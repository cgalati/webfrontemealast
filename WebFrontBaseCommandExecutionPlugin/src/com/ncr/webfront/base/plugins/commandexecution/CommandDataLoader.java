package com.ncr.webfront.base.plugins.commandexecution;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class CommandDataLoader {
	private CommandDataLoader() {
	}
	
	public static List<Command> load(String sourcePath) {
		StringBuilder json = new StringBuilder();
		Type listType = new TypeToken<List<Command>>() {}.getType();
		
		try (BufferedReader reader = new BufferedReader(new FileReader(sourcePath))) {
			String line = "";
			
			while ((line = reader.readLine()) != null) {
				json.append(line);
			}
			
			return new Gson().fromJson(json.toString(), listType);
		} catch (Throwable t) {
			// To also catch com.google.gson.JsonSyntaxException
			logger.error("", t);
			
			return new ArrayList<>();
		}
	}
	
	public static void save(String destinationPath, List<Command> commands) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(destinationPath))) {
			String json = new Gson().toJson(commands);
			
			writer.write(json);
		}
	}
	
	private static Logger logger = WebFrontLogger.getLogger(CommandDataLoader.class);
}
