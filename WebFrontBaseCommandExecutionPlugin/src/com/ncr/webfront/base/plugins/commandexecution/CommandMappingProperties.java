package com.ncr.webfront.base.plugins.commandexecution;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class CommandMappingProperties {
	public static final String commandDataPath = "command.data.path";
	public static final String commandDataPathDefault = PathManager.getInstance().getWebFrontDataDirectory() + File.separator + "commandData.json";
		
	public static synchronized CommandMappingProperties getInstance() {
		if (instance == null) {
			instance = new CommandMappingProperties();
		}
		
		return instance;
	}
	
	private CommandMappingProperties() {
		command = PropertiesLoader.loadOrCreatePropertiesFile(commandPluginPropertiesFileName, getCommandPropertiesMap());
	}
	
	private static Map<String, String> getCommandPropertiesMap() {
		Map<String, String> commandPropertiesMap = new HashMap<>();
		
		commandPropertiesMap.put(commandDataPath, commandDataPathDefault);
		
		return commandPropertiesMap;
	}
	
	public String getProperty(String key) {
		return getProperty(key, false);
	}
	
	public String getProperty(String key, boolean prependWorkingDir) {
		String value = command.getProperty(key);
		
		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}
		
		return value;
	}
	
	private final String commandPluginPropertiesFileName = "command.properties";
	private Properties command;
	
	private static CommandMappingProperties instance;
}
