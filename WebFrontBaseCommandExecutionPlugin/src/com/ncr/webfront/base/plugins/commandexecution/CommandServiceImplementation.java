package com.ncr.webfront.base.plugins.commandexecution;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ncr.webfront.base.commandexecution.CommandServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.commandexecution.CommandLocalRunner;
import com.ncr.webfront.core.utils.commandexecution.CommandRunner;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;

public class CommandServiceImplementation implements CommandServiceInterface {
	public CommandServiceImplementation() {
		String commandDataPath = CommandMappingProperties.getInstance().getProperty(CommandMappingProperties.commandDataPath, true);
		List<Command> commands = CommandDataLoader.load(commandDataPath);
		
		for (Command command : commands) {
			this.commands.put(command.getName(), new RunningInfo(command));
		}
	}

	@Override
	public ErrorObject checkService() {
		try {
			checkIfFileExists(CommandMappingProperties.commandDataPath);
		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
	
	private void checkIfFileExists(String key) {
		String path = CommandMappingProperties.getInstance().getProperty(key, true);
		
		if (!new File(path).exists()) {
			throw new RuntimeException(String.format("File %s inesistente", path));
		}
	}
	
	@Override
	public List<RunningInfo> getAll() {
		List<RunningInfo> list = new ArrayList<>();
		
		cleanupOldRunningInfo();
		for (RunningInfo info : commands.values()) {
			list.add(info);
		}
		
		return list;
	}
	
	private void cleanupOldRunningInfo() {
		for (RunningInfo info : commands.values()) {
			if (info.isFinished()) {
				info.reset();
				runningCommands.remove(info.getCommand().getName());
			}
		}
	}

	@Override
	public List<RunningInfo> getByName(String name) {
		List<RunningInfo> filtered = new ArrayList<>();
		
		cleanupOldRunningInfo();
		for (RunningInfo info : commands.values()) {
			if (info.getCommand().getName().contains(name)) {
				filtered.add(info);
			}
		}
		
		return filtered;
	}
	
	@Override
	public synchronized void execute(String commandName) {
		cleanupOldRunningInfo();
		if (!runningCommands.containsKey(commandName)) {
			CommandRunner runner = getRunner(commands.get(commandName));
			
			runner.execute();
			runningCommands.put(commandName, runner);
		}
	}
	
	private static CommandRunner getRunner(RunningInfo info) {
		CommandRunner runner = null;
		int timeoutInSeconds = WebFrontMappingProperties.getInstance().getCommandExecutionTimeoutInSeconds();
		
		switch (info.getCommand().getRunnerType()) {
			default:
			case LOCAL:
				runner = new CommandLocalRunner();
				break;
			
			case REMOTE_WINDOWS:
				// TODO x RELEASE 2.0
				break;
			
			case REMOTE_LINUX:
				// TODO x RELEASE 2.0
				break;
		}
		runner.setTimeoutInSeconds(Integer.valueOf(timeoutInSeconds));
		runner.setRunningInfo(info);
		
		return runner;
	}
	
	@Override
	public synchronized void cancel(String commandName) {
		CommandRunner runner = runningCommands.get(commandName);
		
		runner.cancel();
	}
	
	@Override
	public synchronized RunningInfo update(String commandName) {
		CommandRunner runner = runningCommands.get(commandName);
		
		runner.update();

		return runner.getRunningInfo();
	}
	
	@Override
	public synchronized void makeCommandAvailabileForExecution(Command command) {
		if (!commands.containsKey(command.getName())) {
			commands.put(command.getName(), new RunningInfo(command));
		}
	}
	
	private Map<String, RunningInfo> commands = new HashMap<>();
	private Map<String, CommandRunner> runningCommands = new HashMap<>();
}
