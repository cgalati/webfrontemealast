package com.ncr.webfront.base.article;

import java.util.Comparator;

public class ArticleComparator implements Comparator<Article> {
	public ArticleComparator(boolean ascending) {
		this.ascending = ascending;
	}
	
	@Override
	public int compare(Article left, Article right) {
		long leftCode = Long.valueOf(left.getArticleCode());
		long rightCode = Long.valueOf(right.getArticleCode());
		int result = Long.compare(leftCode, rightCode);
		
		return ascending ? result : -result;
	}
	
	private boolean ascending;
}
