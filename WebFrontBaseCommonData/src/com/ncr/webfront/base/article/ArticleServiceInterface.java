package com.ncr.webfront.base.article;

import java.util.List;
import java.util.Map;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public interface ArticleServiceInterface {

	public ErrorObject checkService();
	public List<Article> searchByArticleCode (String articleCode);
	public List<Article> searchByStartsWithArticleCode (String articleCode);
	public List<Article> searchByPluCode (String pluCode);
	public List<Article> searchByDescription (String description);
	public ErrorObject deleteArticle(String articleCodeToDelete);
	public ErrorObject addArticle(Article articleToAdd);
	public ErrorObject updateArticle(Article articleToUpdate);
	public Map <String, Boolean> getCapabilities();
	public Article createArticle();
	public List<WebFrontValidationData> getValidationData();
}
