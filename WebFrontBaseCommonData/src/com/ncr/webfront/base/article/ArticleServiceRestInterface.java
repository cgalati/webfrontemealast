package com.ncr.webfront.base.article;

public interface ArticleServiceRestInterface {
	public Object checkService();
	public Object searchByArticleCode(String articleCode);
	public Object searchByStartsWithArticleCode(String articleCode);
	public Object searchByPluCode(String pluCode);
	public Object searchByDescription(String description);
	public Object deleteArticle(String articleJsonString);
	public Object addArticle(String articleJsonString);
	public Object updateArticle(String articleJsonString);
	public Object getCapabilities();
	public Object createArticle();
	public Object getValidationData();
}
