package com.ncr.webfront.base.article;

import java.util.List;

public class Article {

	public String getPosDpt() {
		return posDpt;
	}

	public void setPosDpt(String posDpt) {
		this.posDpt = posDpt;
	}

	public int getVatId() {
		return vatId;
	}

	public void setVatId(int vatId) {
		this.vatId = vatId;
	}

	public boolean isNegativePrice() {
		return negativePrice;
	}

	public void setNegativePrice(boolean negativePrice) {
		this.negativePrice = negativePrice;
	}

	public boolean isDeposit() {
		return deposit;
	}

	public void setDeposit(boolean deposit) {
		this.deposit = deposit;
	}

	public boolean isScalable() {
		return scalable;
	}

	public void setScalable(boolean scalable) {
		this.scalable = scalable;
	}

	public boolean isDecimalQty() {
		return decimalQty;
	}

	public void setDecimalQty(boolean decimalQty) {
		this.decimalQty = decimalQty;
	}

	public int getManualDiscountId() {
		return manualDiscountId;
	}

	public void setManualDiscountId(int manualDiscountId) {
		this.manualDiscountId = manualDiscountId;
	}

	public int getTareId() {
		return tareId;
	}

	public void setTareId(int tareId) {
		this.tareId = tareId;
	}

	public int getMixId() {
		return mixId;
	}

	public void setMixId(int mixId) {
		this.mixId = mixId;
	}

	public String getPackagingType() {
		return packagingType;
	}

	public void setPackagingType(String packagingType) {
		this.packagingType = packagingType;
	}

	public int getUnitsPerPackage() {
		return unitsPerPackage;
	}

	public void setUnitsPerPackage(int unitsPerPackage) {
		this.unitsPerPackage = unitsPerPackage;
	}

	public String getLinkedItem() {
		return linkedItem;
	}

	public void setLinkedItem(String linkedItem) {
		this.linkedItem = linkedItem;
	}

	public int getPrizeCode() {
		return prizeCode;
	}

	public void setPrizeCode(int prizeCode) {
		this.prizeCode = prizeCode;
	}

	public int getZeroPriceBehavior() {
		return zeroPriceBehavior;
	}

	public void setZeroPriceBehavior(int zeroPriceBehavior) {
		this.zeroPriceBehavior = zeroPriceBehavior;
	}

	public boolean isMembersOnly() {
		return membersOnly;
	}

	public void setMembersOnly(boolean membersOnly) {
		this.membersOnly = membersOnly;
	}

	public int getAutomDiscountType() {
		return automDiscountType;
	}

	public void setAutomDiscountType(int automDiscountType) {
		this.automDiscountType = automDiscountType;
	}

	public int getUpbOperationType() {
		return upbOperationType;
	}

	public void setUpbOperationType(int upbOperationType) {
		this.upbOperationType = upbOperationType;
	}

	public int getUpbProviderId() {
		return upbProviderId;
	}

	public void setUpbProviderId(int upbProviderId) {
		this.upbProviderId = upbProviderId;
	}

	public String getArticleCode() {
		return articleCode;
	}

	public void setArticleCode(String articleCode) {
		this.articleCode = articleCode;
	}

	public List<String> getPluCodes() {
		return pluCodes;
	}

	public void setPluCodes(List<String> pluCodes) {
		this.pluCodes = pluCodes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getScalableUnitPrice() {
		return scalableUnitPrice;
	}

	public void setScalableUnitPrice(Integer scalableUnitPrice) {
		this.scalableUnitPrice = scalableUnitPrice;
	}

	public Integer getScalableExtendedPrice() {
		return scalableExtendedPrice;
	}

	public void setScalableExtendedPrice(Integer scalableExtendedPrice) {
		this.scalableExtendedPrice = scalableExtendedPrice;
	}

	public Integer getScalableWeight() {
		return scalableWeight;
	}

	public void setScalableWeight(Integer scalableWeight) {
		this.scalableWeight = scalableWeight;
	}

	public String toString() {
		StringBuffer articleBuffer = new StringBuffer();
		articleBuffer.append("\n");
		articleBuffer.append("articleCode           =" + articleCode + "\n");
		articleBuffer.append("pluCodes              =" + pluCodes + "\n");
		articleBuffer.append("posDpt                =" + posDpt + "\n");
		articleBuffer.append("description           =" + description + "\n");
		articleBuffer.append("vatId                 =" + vatId + "\n");
		articleBuffer.append("negativePrice         =" + negativePrice + "\n");
		articleBuffer.append("deposit               =" + deposit + "\n");
		articleBuffer.append("scalable              =" + scalable + "\n");
		articleBuffer.append("decimalQty            =" + decimalQty + "\n");
		articleBuffer.append("manualDiscountId      =" + manualDiscountId + "\n");
		articleBuffer.append("tareId                =" + tareId + "\n");
		articleBuffer.append("mixId                 =" + mixId + "\n");
		articleBuffer.append("packagingType         =" + packagingType + "\n");
		articleBuffer.append("unitsPerPackage       =" + unitsPerPackage + "\n");
		articleBuffer.append("linkedItem            =" + linkedItem + "\n");
		articleBuffer.append("prizeCode             =" + prizeCode + "\n");
		articleBuffer.append("zeroPriceBehavior     =" + zeroPriceBehavior + "\n");
		articleBuffer.append("membersOnly           =" + membersOnly + "\n");
		articleBuffer.append("automDiscountType     =" + automDiscountType + "\n");
		articleBuffer.append("upbOperationType      =" + upbOperationType + "\n");
		articleBuffer.append("upbProviderId         =" + upbProviderId + "\n");
		articleBuffer.append("price                 =" + price + "\n");
		articleBuffer.append("scalableUnitPrice     =" + scalableUnitPrice + "\n");
		articleBuffer.append("scalableExtendedPrice =" + scalableExtendedPrice + "\n");
		articleBuffer.append("scalableWeight        =" + scalableWeight + "\n");
		return articleBuffer.toString();
	}

	private String articleCode;
	private List<String> pluCodes;
	private String posDpt;
	private String description;
	private int vatId;
	private boolean negativePrice;
	private boolean deposit;
	private boolean scalable;
	private boolean decimalQty;
	private int manualDiscountId;
	private int tareId;
	private int mixId;
	private String packagingType;
	private int unitsPerPackage;
	private String linkedItem;
	private int prizeCode;
	private int zeroPriceBehavior;
	private boolean membersOnly;
	private int automDiscountType;
	private int upbOperationType;
	private int upbProviderId;
	private Integer price;
	private Integer scalableUnitPrice;
	private Integer scalableExtendedPrice;
	private Integer scalableWeight;

}
