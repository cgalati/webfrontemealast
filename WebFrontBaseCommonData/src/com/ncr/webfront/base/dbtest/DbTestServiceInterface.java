package com.ncr.webfront.base.dbtest;

public interface DbTestServiceInterface {
	public Object checkService();

	public Object getAll();
}
