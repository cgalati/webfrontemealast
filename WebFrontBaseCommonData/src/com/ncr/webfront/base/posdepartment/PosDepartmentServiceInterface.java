package com.ncr.webfront.base.posdepartment;

import java.util.List;


import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface PosDepartmentServiceInterface {
	public ErrorObject checkService();	
	public List<PosDepartment> getAll();
	public List<PosDepartment> getDepartmentsOfLevel(int level);
	public PosDepartment getByCode (String dptCode);	
	public ErrorObject deleteDepartment(String departmentCodeToDelete);
	public ErrorObject addDepartment(PosDepartment departmentToAdd);
	public ErrorObject updateDepartment(PosDepartment departmentToUpdate);
	public PosDepartment createDepartment();
}
