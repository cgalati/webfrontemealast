package com.ncr.webfront.base.posdepartment;

public class PosDepartment {
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isNegativePrice() {
		return negativePrice;
	}
	public void setNegativePrice(boolean negativePrice) {
		this.negativePrice = negativePrice;
	}
	public boolean isDeposit() {
		return deposit;
	}
	public void setDeposit(boolean deposit) {
		this.deposit = deposit;
	}
	public boolean isScalable() {
		return scalable;
	}
	public void setScalable(boolean scalable) {
		this.scalable = scalable;
	}
	public boolean isDecimalQty() {
		return decimalQty;
	}
	public void setDecimalQty(boolean decimalQty) {
		this.decimalQty = decimalQty;
	}
	public int getManualDiscountID() {
		return manualDiscountID;
	}
	public void setManualDiscountID(int manualDiscountID) {
		this.manualDiscountID = manualDiscountID;
	}
	public int getVatID() {
		return vatID;
	}
	public void setVatID(int vatID) {
		this.vatID = vatID;
	}
	public int getUpbOperationType() {
		return upbOperationType;
	}
	public void setUpbOperationType(int upbOperationType) {
		this.upbOperationType = upbOperationType;
	}
	public int getUpbProviderId() {
		return upbProviderId;
	}
	public void setUpbProviderId(int upbProviderId) {
		this.upbProviderId = upbProviderId;
	}
	public int getAutomDiscountType() {
		return automDiscountType;
	}
	public void setAutomDiscountType(int automDiscountType) {
		this.automDiscountType = automDiscountType;
	}
	public int getLevel() {
		return code.lastIndexOf('*') + 1;
	}
	
	private String code;
	private String total;
	private String description;
	private boolean negativePrice;
	private boolean deposit;
	private boolean scalable;
	private boolean decimalQty; 
	private int manualDiscountID;
	private int vatID;
	private int upbOperationType;
	private int upbProviderId;
	private int automDiscountType;

}
