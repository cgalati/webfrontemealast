package com.ncr.webfront.base.posdepartment;

public interface PosDepartmentServiceRestInterface {
	public Object checkService();

	public Object getAll();
	public Object getDepartmentsOfLevel(int level);
	public Object getByCode (String dptCode);
	public Object deleteDepartment(String departmentJsonString);
	public Object addDepartment(String departmentJsonString);
	public Object updateDepartment(String departmentJsonString);
	public Object createDepartment();
}
