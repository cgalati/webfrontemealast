package com.ncr.webfront.base.posstatus;

public interface PosStatusServiceRestInterface {
	public Object checkService();
	public Object getAllPosStatus();
	public Object getPosStatus(String posCode);

}
