package com.ncr.webfront.base.posstatus;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.PosStatusObject;

public interface PosStatusServiceInterface {
	public ErrorObject checkService();
	public List<PosStatusObject> getAllPosStatus();
	public PosStatusObject getPosStatus(String posCode);
}
