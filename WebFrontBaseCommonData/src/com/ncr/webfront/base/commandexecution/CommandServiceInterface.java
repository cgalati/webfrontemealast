package com.ncr.webfront.base.commandexecution;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;

public interface CommandServiceInterface {
	public ErrorObject checkService();
	
	public List<RunningInfo> getAll();
	
	public List<RunningInfo> getByName(String commandName);
	
	public void execute(String commandName);
	
	public void cancel(String commandName);
	
	public RunningInfo update(String commandName);
	
	public void makeCommandAvailabileForExecution(Command command);
}
