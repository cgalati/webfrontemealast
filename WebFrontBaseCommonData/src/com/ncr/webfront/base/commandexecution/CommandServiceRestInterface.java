package com.ncr.webfront.base.commandexecution;

public interface CommandServiceRestInterface {
	public Object checkService();

	public Object getAll();
	
	public Object getByName(String commandName);
	
	public Object execute(String commandName);
	
	public Object cancel(String commandName);
	
	public Object update(String commandName);
	
	public Object makeCommandAvailabileForExecution(String commandJsonString);
}
