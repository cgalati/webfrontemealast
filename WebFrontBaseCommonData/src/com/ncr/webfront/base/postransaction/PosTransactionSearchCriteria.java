package com.ncr.webfront.base.postransaction;

import java.util.Date;

public class PosTransactionSearchCriteria {
	
	private Date transactionDate;
	private String operatorCode;
	private String terminalCode;
	private String transactionNumber;

	public PosTransactionSearchCriteria(Date transactionDate,
			String operatorCode, String terminalCode, String transactionNumber) {
		this.transactionDate = transactionDate;
		this.operatorCode = operatorCode;
		this.terminalCode = terminalCode;
		this.transactionNumber = transactionNumber;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

}
