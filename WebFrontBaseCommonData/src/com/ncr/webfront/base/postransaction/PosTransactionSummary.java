package com.ncr.webfront.base.postransaction;

import java.util.Date;

public class PosTransactionSummary {

	private Date transactionDate;
	private String operatorCode;
	private String terminalCode;
	private String transactionNumber;
	private Integer transactionTotal;

	public PosTransactionSummary(Date transactionDate, String operatorCode,
			String terminalCode, String transactionNumber, Integer transactionTotal) {
		this.transactionDate = transactionDate;
		this.operatorCode = operatorCode;
		this.terminalCode = terminalCode;
		this.transactionNumber = transactionNumber;
		this.transactionTotal = transactionTotal;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getTerminalCode() {
		return terminalCode;
	}

	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	public Integer getTransactionTotal() {
		return transactionTotal;
	}

	public void setTransactionTotal(Integer transactionTotal) {
		this.transactionTotal = transactionTotal;
	}
	
	public boolean isRecordEmpty() {
		return (transactionNumber.isEmpty() || transactionNumber == null);
	}

}
