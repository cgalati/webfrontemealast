package com.ncr.webfront.base.postransaction;

import java.util.Date;
import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface PosTransactionServiceInterface {

	public ErrorObject checkService();

	List<Date> getAvailablePosTransactionDates();

	List <PosTransactionSummary> searchTransactions (PosTransactionSearchCriteria posTransactionSearchCriteria);
	
	List <String > getPosTransactionJournal(PosTransactionSummary posTransactionSummary);
	
}
