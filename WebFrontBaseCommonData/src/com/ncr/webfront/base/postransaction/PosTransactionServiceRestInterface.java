package com.ncr.webfront.base.postransaction;

public interface PosTransactionServiceRestInterface {

	public Object checkService();
	
	Object getAvailablePosTransactionDates();

	Object searchTransactions(String posTransactionSearchCriteriaJsonString );
	
	Object getPosTransactionJournal(String posTransactionSummaryJsonString );
}
