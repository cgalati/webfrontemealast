package com.ncr.webfront.base.eod;

import java.util.ArrayList;
import java.util.List;

import com.ncr.webfront.base.posterminal.PosTerminal;

public class PosStatusObject extends PosTerminal {

	public PosStatus getPosStatus() {
		return posStatus;
	}

	public void setPosStatus(PosStatus posStatus) {
		this.posStatus = posStatus;
	}

	public String getPosStatusDescription() {
		return posStatusDescription;
	}

	public void setPosStatusDescription(String posStatusDescription) {
		this.posStatusDescription = posStatusDescription;
	}

	public boolean isDefectiveDeclarable() {
		return defectiveDeclarable;
	}

	public void setDefectiveDeclarable(boolean defectiveDeclarable) {
		this.defectiveDeclarable = defectiveDeclarable;
	}

	public boolean isReadyForPosEndOfDay() {
		return readyForPosEndOfDay;
	}

	public void setReadyForPosEndOfDay(boolean readyForPosEndOfDay) {
		this.readyForPosEndOfDay = readyForPosEndOfDay;
	}	
	
	public boolean isReadyForServerEndOfDay() {
		return readyForServerEndOfDay;
	}

	public void setReadyForServerEndOfDay(boolean readyForServerEndOfDay) {
		this.readyForServerEndOfDay = readyForServerEndOfDay;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDcCounter() {
		return dcCounter;
	}

	public void setDcCounter(String dcCounter) {
		this.dcCounter = dcCounter;
	}

	public String getJrnCounter() {
		return jrnCounter;
	}

	public void setJrnCounter(String jrnCounter) {
		this.jrnCounter = jrnCounter;
	}
	
	public String getMaintenanceCounter() {
		return maintenanceCounter;
	}

	public void setMaintenanceCounter(String maintenanceCounter) {
		this.maintenanceCounter = maintenanceCounter;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionCodeDescription() {
		return actionCodeDescription;
	}

	public void setActionCodeDescription(String actionCodeDescription) {
		this.actionCodeDescription = actionCodeDescription;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getGroupPos() {
		return groupPos;
	}

	public void setGroupPos(String group) {
		this.groupPos = group;
	}
	
	public List<String> getListOperatorCodes() {
		return listOperatorCodes;
	}
	
	public void setListOperatorCodes(List<String> listCodeOperators) {
		this.listOperatorCodes = listCodeOperators;
	}
	
	public List<String> getListOperatorNames() {
		return listOperatorNames;
	}
	
	public void setListOperatorNames(List<String> listNameOperators) {
		this.listOperatorNames = listNameOperators;
	}
	
	

	private PosStatus posStatus;
	private boolean defectiveDeclarable;
	private boolean readyForPosEndOfDay;
	private boolean readyForServerEndOfDay;
	private String posStatusDescription;
	private String operatorCode;
	private String statusCode;
	private String actionCode;
	private String actionCodeDescription;
	private String dcCounter;
	private String jrnCounter;
	private String maintenanceCounter;
	private String date;
	private String time;
	private String dateTime;
	private String groupPos;
	private List<String> listOperatorCodes = new ArrayList<String>();
	private List<String> listOperatorNames = new ArrayList<String>();
	
	public enum PosStatus{
		POS_READY,
		POS_EOD_END_OK,
		POS_DEFECTIVE,
		POS_INUSE,
		POS_OFFLINE,
		POS_EOD_RUNNING,
		POS_EOD_ERROR,
		POS_EOD_WARNING,
		POS_MISMATCH,
		POS_MIRRORING_STATE
	}
	
	public String toString(){
		String string = "";
		string = string + "terminalCode                = " + getTerminalCode()         +  "\n";
		string = string + "description                 = " + getDescription()          +  "\n";
		string = string + "type                        = " + getType()                 +  "\n";
		string = string + "posStatus                   = " + posStatus                 +  "\n";
		string = string + "defectiveDeclarable         = " + defectiveDeclarable       +  "\n";
		string = string + "readyForPosEndOfDay         = " + readyForPosEndOfDay       +  "\n";
		string = string + "readyForServerEndOfDay      = " + readyForServerEndOfDay    +  "\n";
		string = string + "posStatusDescription        = " + posStatusDescription      +  "\n";
		string = string + "operatorCode                = " + operatorCode              +  "\n";
		string = string + "statusCode                  = " + statusCode                +  "\n";
		string = string + "actionCode                  = " + actionCode                +  "\n";
		string = string + "actionCodeDescription       = " + actionCodeDescription     +  "\n";
		string = string + "dcCounter                   = " + dcCounter                 +  "\n";
		string = string + "jrnCounter                  = " + jrnCounter                +  "\n";
		string = string + "maintenanceCounter          = " + maintenanceCounter        +  "\n";
		string = string + "date                        = " + date                      +  "\n";
		string = string + "time                        = " + time                      +  "\n";
		string = string + "dateTime                    = " + dateTime                  +  "\n";
		string = string + "groupPos                    = " + groupPos                  +  "\n";
		
	 return string;	
	 
	}
}
