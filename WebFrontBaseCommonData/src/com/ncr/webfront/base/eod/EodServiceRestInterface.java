package com.ncr.webfront.base.eod;

public interface EodServiceRestInterface {

	public Object checkService();
	public Object getAllPosStatus();
	public Object getPosStatus(String posCode);
	public Object declareDefective(String posCode);
	public Object launchEodAllPos();

	public Object getEodStatus();
	public Object launchEodServer();
	public Object cancel();

	public Object getEodStatusHistory();
	
}
