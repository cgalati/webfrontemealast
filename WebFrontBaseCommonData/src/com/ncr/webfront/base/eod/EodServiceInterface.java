package com.ncr.webfront.base.eod;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface EodServiceInterface {

	public ErrorObject checkService();
	public List<PosStatusObject> getAllPosStatus();
	public PosStatusObject getPosStatus(String posCode);
	public ErrorObject declareDefective(String posCode);
	public ErrorObject launchEodAllPos();

	public ErrorObject launchEodServer();
	public void cancel();
	public EodStatusObject getEodStatus();
	public List<EodStatusObject> getEodStatusHistory();
}
