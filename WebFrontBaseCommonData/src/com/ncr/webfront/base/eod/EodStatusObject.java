package com.ncr.webfront.base.eod;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ncr.webfront.core.utils.commandexecution.RunningInfo;

public class EodStatusObject {

	public EodStatusObject(Date eodStartDateTime, Date eodEndDateTime, EodStatus eodStatus) {
		if (eodStatus == EodStatus.EOD_NOT_STARTED) {
			this.eodEndDateTime = null;
			this.eodStartDateTime = null;
			this.status = EodStatus.EOD_NOT_STARTED;
		} else {
			this.eodStartDateTime = eodStartDateTime;
			this.eodEndDateTime = eodEndDateTime;
			this.status = eodStatus;
		}
		eodSteps = new ArrayList<RunningInfo>();
	}

	public Date getEodEndDateTime() {
		return eodEndDateTime;
	}

	public void setEodEndDateTime(Date eodEndDateTime) {
		this.eodEndDateTime = eodEndDateTime;
	}

	public EodStatus getStatus() {
		return status;
	}

	public void setStatus(EodStatus status) {
		this.status = status;
	}

	public Date getEodStartDateTime() {
		return eodStartDateTime;
	}

	public void setEodStartDateTime(Date eodStartDateTime) {
		this.eodStartDateTime = eodStartDateTime;
	}

	public EodServerPhases getEodServerPhase() {
		return eodServerPhase;
	}

	public void setEodServerPhase(EodServerPhases eodServerPhase) {
		this.eodServerPhase = eodServerPhase;
	}

	public List<RunningInfo> getEodSteps() {
		return eodSteps;
	}

	public int getLastCompletedExecutionStepIndex() {
		return lastCompletedExecutionStepIndex;
	}

	public void setLastCompletedExecutionStepIndex(int lastCompletedExecutionStepIndex) {
		this.lastCompletedExecutionStepIndex = lastCompletedExecutionStepIndex;
	}

	public int getNumberOfCompletedExecutionSteps() {
		return numberOfCompletedExecutionSteps;
	}

	public void setNumberOfCompletedExecutionSteps(int numberOfCompletedExecutionSteps) {
		this.numberOfCompletedExecutionSteps = numberOfCompletedExecutionSteps;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("\n");
		stringBuilder.append("BEGIN dumping EodStatusObject" + "\n");
		stringBuilder.append("   eodStartDateTime:                " + eodStartDateTime + "\n");
		stringBuilder.append("   eodEndDateTime:                  " + eodEndDateTime + "\n");
		stringBuilder.append("   eodServerPhase:                  " + eodServerPhase + "\n");
		stringBuilder.append("   lastCompletedExecutionStepIndex: " + lastCompletedExecutionStepIndex + "\n");
		stringBuilder.append("   numberOfCompletedExecutionSteps: " + numberOfCompletedExecutionSteps + "\n");
		stringBuilder.append("   status:                          " + status + "\n");
		stringBuilder.append("   ----------------------------------------------------------------------------------------\n");
		for (RunningInfo eodStep : eodSteps) {
			stringBuilder.append("   step   : \"" + eodStep.getCommand().getDescription() + "\"\n");
			if (eodStep.getCommand().getFullCommandLine() != null){
				stringBuilder.append("   command: \"" + eodStep.getCommand().getFullCommandLine() + "\"\n");
			}
			stringBuilder.append("   Details:\n");
			for (List<String> outputList: eodStep.getOutput()){
				for (String output: outputList){
					stringBuilder.append("      "+ output+"\n");
				}
			}
			stringBuilder.append("   ----------------------------------------------------------------------------------------\n");
		}
		stringBuilder.append("END   dumping EodStatusObject" + "\n");
		// return ToStringBuilder.reflectionToString(this);
		return stringBuilder.toString();
	}

	private Date eodStartDateTime;
	private Date eodEndDateTime;
	private EodServerPhases eodServerPhase;
	private int lastCompletedExecutionStepIndex;
	private int numberOfCompletedExecutionSteps;
	private EodStatus status;
	private List<RunningInfo> eodSteps;

	public enum EodServerPhases {
		PRE_SERVER_EOD, SERVER_EOD, POST_SERVER_EOD
	}

	public enum EodStatus {
		EOD_NOT_STARTED(false, "H_End of day not started"), 
		EOD_ENDED_OK(false, "H_End of day succesfully completed"), 
		EOD_ENDED_WITH_ERROR(false, "H_End of day succesfully completed with errors"), 
		EOD_ENDED_WITH_WARNINGS(false, "H_End of day succesfully completed with warnings"), 
		EOD_CRASHED(false, "H_End of day failed"), 
		EOD_RUNNING_OK(true, "H_End of day in progress"), 
		EOD_RUNNING_WITH_WARNINGS(true, "H_End of day in progress with warnings");

		private EodStatus(boolean running, String description) {
			this.running = running;
			this.description = description;
		}

		public boolean isRunning() {
			return running;
		}
		
		public String getDescription() {
			return description;
		}

		private boolean running;
		private String description;
	}

}
