package com.ncr.webfront.base.vat;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface VatServiceInterface {
	public ErrorObject checkService();

	public List<Vat> getAll();
	public Vat getVatById(int vatId);
	public Vat getDefault();
	
// TODO x RELEASE 2.0
/*	public Vat createVat();
 *	public ErrorObject addVat(Vat vatToAdd);
 *	public ErrorObject updateVat(Vat vatToUpdate);
 * 	public ErrorObject deleteVat(Vat vatToDelete);
 */
}
