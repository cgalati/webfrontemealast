package com.ncr.webfront.base.vat;

public interface VatServiceRestInterface {
	public Object checkService();
	public Object getAll();
	public Object getVatById(int vatId);
	public Object getDefault();
}
