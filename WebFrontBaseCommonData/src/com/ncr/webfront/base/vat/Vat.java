package com.ncr.webfront.base.vat;

public class Vat {
	public int getVatId() {
		return vatId;
	}
	public void setVatId(int vatId) {
		this.vatId = vatId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	
	private int vatId;
	private String description;
	private int rate;
}
