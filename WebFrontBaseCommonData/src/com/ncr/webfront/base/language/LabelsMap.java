package com.ncr.webfront.base.language;

import java.util.HashMap;

@SuppressWarnings("serial")
public class LabelsMap extends HashMap<String, String> {

	public String get(String key, String defaultValue) {

		String value = get(key);
		if (value == null) {
			value = defaultValue;
		}
		return value;
	}

}
