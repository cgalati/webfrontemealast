package com.ncr.webfront.base.language;

import java.util.Locale;

public interface LanguageServiceInterface {

	public LabelsMap load(Locale locale, String serviceName);
	public LabelsMap load(String serviceName);
}
