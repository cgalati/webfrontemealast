package com.ncr.webfront.base.posidcjrn.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "transaction_idc")
public class DataCollect {    

    private int id;
    private String line;
    private transient DataHeader dataHeader;
    private int sequence;

	public DataCollect(){};

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "LINE")
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    @ManyToOne
    @JoinColumn(name = "transaction_header_ID")
    public DataHeader getDataHeader() {
        return dataHeader;
    }

    public void setDataHeader(DataHeader dataHeader) {
        this.dataHeader = dataHeader;
    }

    @Column(name = "SEQUENCE")
	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
}
