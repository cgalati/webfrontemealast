package com.ncr.webfront.base.posidcjrn.entities;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name = "transaction_header")
public class DataHeader {	
    private int id = 0;
    private String store;
    private String terminal;
    private int receipt; 
    private String receiptDate; 
	private String timestamp; 
    private long amount;
	private String customer = "";
	private String eod = "";
	private String fileName = "";
    private Set<DataCollect> idc = new LinkedHashSet<DataCollect>();
	private Set<Journal> jrn = new LinkedHashSet<Journal>();    

    public DataHeader() {}
    
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "STORE")
	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	@Column(name = "TERMINAL")
	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	@Column(name = "TRANS")
	public int getReceipt() {
		return receipt;
	}

	public void setReceipt(int receipt) {
		this.receipt = receipt;
	}

	@Column(name = "DATE")
	public String getReceiptDate() {
		/*try {
			receiptDate  = new SimpleDateFormat("yyyy-MM-dd").parse(receiptDate.toString());
		} catch (Exception e) {
				
		}*/

		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) {
		/*if (receiptDate.toString().indexOf("-") >= 0) {
			this.receiptDate  = receiptDate;	
		} else {
			try {
				//Date dataReceipt = new SimpleDateFormat("yyyy-MM-dd").parse(receiptDate.toString());
				this.receiptDate  = receiptDate;
			} catch (Exception e) {
				
			}			
		}*/
		
		this.receiptDate  = receiptDate;
	}

	@Column(name = "TIME_STAMP")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		try {			
			this.timestamp = timestamp;
		} catch (Exception e) {
			
		}			
	}

	@Column(name = "TOTAL")
	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	@Column(name = "CUSTOMER")
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	@Column(name = "EOD")
	public String getEod() {
		return eod;
	}

	public void setEod(String eod) {
		this.eod = eod;
	}

	@Column(name = "FILENAME")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
    @OneToMany(mappedBy = "dataHeader", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)    
	public Set<DataCollect> getIdc() {
    	return idc;
	}

	public void setIdc(Set<DataCollect> idc) {
		this.idc = idc;
	}

    @OneToMany(mappedBy = "dataHeader", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)    
	public Set<Journal> getJrn() {
		return jrn;
	}

	public void setJrn(Set<Journal> jrn) {
		this.jrn = jrn;
	}	 
	
	private List<DataCollect> bubbleSortIdc(Set<DataCollect> listIdc) {
    	List<DataCollect> lista = new ArrayList<DataCollect>(listIdc);
    	//Set<DataCollect> listaSet = new HashSet<DataCollect>();
    	    	
    	DataCollect[] strArr = new DataCollect[listIdc.size()];
        listIdc.toArray(strArr);
        
        for(DataCollect str : strArr){
        	lista.add(str);
        }
    	
        for(int i = 0; i < lista.size(); i++) {
            boolean flag = false;
            
            for(int j = 0; j < lista.size()-1; j++) {
                if(lista.get(j).getSequence() > lista.get(j+1).getSequence()) {
                	DataCollect k = lista.get(j);
                	
                    lista.add(j, lista.get(j+1));
                    lista.remove(j+1);
                    lista.add(j+1, k);
                    lista.remove(j+2);
                    
                    flag = true; 
                }
            }

            if(!flag) break; 
        }

        //listaSet.addAll(lista);
        
        return lista;        
    }
    
    private List<Journal> bubbleSortJrn(Set<Journal> listIdc) {
    	List<Journal> lista = new ArrayList<Journal>();
    //	Set<Journal> listaSet = new HashSet<Journal>();
    	
    	Journal[] strArr = new Journal[listIdc.size()];
        listIdc.toArray(strArr);
        
        for(Journal str : strArr){
        	lista.add(str);
        }
    	
        for(int i = 0; i < lista.size(); i++) {
            boolean flag = false;
            
            for(int j = 0; j < lista.size()-1; j++) {
                if(lista.get(j).getSequence() > lista.get(j+1).getSequence()) {
                	Journal k = lista.get(j);
                	
                    lista.add(j, lista.get(j+1));
                    lista.remove(j+1);
                    lista.add(j+1, k);
                    lista.remove(j+2);
                    
                    flag = true; 
                }
            }

            if(!flag) break; 
        }

      //  listaSet.addAll(lista);
        
        return lista;        
    }
}
