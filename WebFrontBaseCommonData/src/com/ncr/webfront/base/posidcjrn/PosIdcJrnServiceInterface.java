package com.ncr.webfront.base.posidcjrn;

import java.util.List;

import com.ncr.webfront.base.posidcjrn.entities.*;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface PosIdcJrnServiceInterface {
	ErrorObject checkService();
	List<DataHeader> getAll();
	ErrorObject addIdcJrn(String dataHeader);	
	ErrorObject updateFileName(String fileName, int idHeader);
	List<DataHeader> getTransactionsByParams(String terminal, String dateFrom, String dateTo, String store, String transaction, String customer);	
	ErrorObject deleteIdcJrn(int idc, String table);
	ErrorObject addIdc(String dataCollect);
	ErrorObject addJrn(String journal);
	ErrorObject deleteAll(int dataHeader);
	List<DataCollect> getLstIdc(int idHeader);
	List<Journal> getLstJrn(int idHeader);
}
