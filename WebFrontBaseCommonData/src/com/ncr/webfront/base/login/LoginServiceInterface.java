package com.ncr.webfront.base.login;

import com.ncr.webfront.core.login.LoginData;

public interface LoginServiceInterface {

	public long doLogin(String userName, String password);
	public boolean doLogout(long userToken);
	public boolean isLoggedIn (long userToken);
	public LoginData getLoggedInUserData (long userToken);
}
