package com.ncr.webfront.base.login;

public interface LoginServiceRestInterface {

	public Object doLogin(String userName, String password);

	public Object doLogout(long userToken);
	
	public Object isLoggedIn (long userToken);
	
	public Object getLoggedInUserData (long userToken);


}
