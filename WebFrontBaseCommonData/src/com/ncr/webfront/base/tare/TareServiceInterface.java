package com.ncr.webfront.base.tare;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.tare.Tare;;

public interface TareServiceInterface {
	public ErrorObject checkService();

	public List<Tare> getAll();
	public Tare getTareById(int tareId);
}
