package com.ncr.webfront.base.tare;

public class Tare {

	public int getTareId() {
		return tareId;
	}

	public void setTareId(int tareId) {
		this.tareId = tareId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private int tareId;
	private String description;

}
