package com.ncr.webfront.base.tare;

public interface TareServiceRestInterface {
	public Object checkService();
	public Object getAll();
	public Object getTareById(int vatId);
}
