package com.ncr.webfront.base.customers;

public enum CustomerCategory {
	ALL_CUSTOMERS, LOYALTY_CUSTOMERS
}