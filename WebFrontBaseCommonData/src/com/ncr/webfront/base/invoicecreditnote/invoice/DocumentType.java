package com.ncr.webfront.base.invoicecreditnote.invoice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wf_documents_types")
public class DocumentType {
	public static final String INVOICE_DOCUMENT_TYPE = "INVOICE";
	public static final String CREDITNOTE_DOCUMENT_TYPE = "CREDITNOTE";
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	private int id;
	private String type;
	private String description;
}
