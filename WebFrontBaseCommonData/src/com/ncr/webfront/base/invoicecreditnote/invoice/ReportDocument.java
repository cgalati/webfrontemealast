package com.ncr.webfront.base.invoicecreditnote.invoice;

import java.util.List;

public class ReportDocument extends Document {
	public List<VatDetail> getVatCastle() {
		return vatCastle;
	}

	public void setVatCastle(List<VatDetail> vatCastle) {
		this.vatCastle = vatCastle;
	}
	
	private List<VatDetail> vatCastle;
}
