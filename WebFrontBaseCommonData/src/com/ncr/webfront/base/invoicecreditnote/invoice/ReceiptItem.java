package com.ncr.webfront.base.invoicecreditnote.invoice;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class ReceiptItem {
	@Column(name = "serial_number")
	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	private int sequenceNumber;
}
