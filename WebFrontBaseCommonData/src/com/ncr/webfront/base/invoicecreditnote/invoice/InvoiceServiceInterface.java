package com.ncr.webfront.base.invoicecreditnote.invoice;

import java.util.List;

public interface InvoiceServiceInterface {
	
	List<Receipt> searchReceipts(ReceiptSearchType receiptSearchType);

	Receipt fillReceipt(ReceiptSearchType receiptSearchType);
	
	Document createInvoice(Receipt receipt);
	
	Document generateInvoice(Document invoice);
	
	byte[] renderHtmlInvoice(Document generatedInvoice);
	
	byte[] renderPdfInvoice(Document generatedInvoice);
	
	byte[] saveInvoiceAndRenderPdf(Document generatedInvoice);
	
	void cancelInvoice(Document invoice);
	
	Document getInvoiceByNumber(int number, boolean mayHaveCreditNote);
	
	List<Document> getInvoicesByVatCode(String vatCode, boolean mayHaveCreditNote);
	
	List<Document> getInvoicesByFiscalCode(String fiscalCode, boolean mayHaveCreditNote);
	
	List<Document> getLastPrintedInvoices(int number, boolean mayHaveCreditNote);
}
