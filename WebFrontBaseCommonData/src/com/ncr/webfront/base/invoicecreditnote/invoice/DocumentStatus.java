package com.ncr.webfront.base.invoicecreditnote.invoice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wf_document_statuses")
public class DocumentStatus {
	public static final String CREATED_DOCUMENT_STATUS = "CREATED";
	public static final String PRINTED_DOCUMENT_STATUS = "PRINTED";
	public static final String CANCELED_DOCUMENT_STATUS = "CANCELED";
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	private int id;
	private String status;
	private String description;
}
