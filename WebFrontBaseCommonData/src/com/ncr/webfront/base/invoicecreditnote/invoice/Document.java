package com.ncr.webfront.base.invoicecreditnote.invoice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import com.ncr.webfront.base.invoicecreditnote.customer.Customer;

@Entity
@Table(name = "wf_documents")
public class Document {
	public boolean isA(String type) {
		return documentType.getType().equals(type);
	}
	
	public boolean isIn(String status) {
		return documentStatus.getStatus().equals(status);
	}
	
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "wf_document_types_id")
	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	@Column(name = "document_number")
	public int getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(int documentNumber) {
		this.documentNumber = documentNumber;
	}

	@Column(name = "document_code")
	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	@Column(name = "creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@ManyToOne
	@JoinColumn(name = "wf_document_statuses_id")
	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	@ManyToOne
	@JoinColumn(name = "wf_customers_id")
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Column(name = "last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name = "amount")
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Column(name = "receipt_number")
	public int getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(int receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	@Column(name = "receipt_terminal")
	public String getReceiptTerminal() {
		return receiptTerminal;
	}

	public void setReceiptTerminal(String receiptTerminal) {
		this.receiptTerminal = receiptTerminal;
	}

	@Column(name = "receipt_store")
	public String getReceiptStore() {
		return receiptStore;
	}

	public void setReceiptStore(String receiptStore) {
		this.receiptStore = receiptStore;
	}

	@Column(name = "receipt_date")
	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "wf_documents_id")
	public List<DocumentDetail> getDocumentDetails() {
		return documentDetails;
	}

	public void setDocumentDetails(List<DocumentDetail> documentDetails) {
		this.documentDetails = documentDetails;
	}
	
	@OneToOne
	@JoinColumn(name = "wf_documents_id")
	public Document getReferenced() {
		return referenced;
	}
	
	public void setReferenced(Document referenced) {
		this.referenced = referenced;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	private int id;
	private DocumentType documentType;
	private int documentNumber;
	private String documentCode;
	private Date creationDate;
	private DocumentStatus documentStatus;
	private Customer customer;
	private Date lastUpdate;
	private int amount;
	private int receiptNumber;
	private String receiptTerminal;
	private String receiptStore;
	private Date receiptDate;
	private Document referenced;
	private List<DocumentDetail> documentDetails = new ArrayList<>();
}
