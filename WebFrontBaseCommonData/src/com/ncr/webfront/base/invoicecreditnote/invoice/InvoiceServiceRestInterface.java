package com.ncr.webfront.base.invoicecreditnote.invoice;

public interface InvoiceServiceRestInterface {
	
	Object searchReceipts(String receiptSearchTypeJsonString );

	Object fillReceipt(String receiptSearchTypeJsonString);
	
	Object createInvoice(String receiptJsonString);
	
	Object generateInvoice(String invoiceJsonString);
	
	Object renderHtmlInvoice(String invoiceJsonString);
	
	Object renderPdfInvoice(String invoiceJsonString);
	
	Object saveInvoiceAndRenderPdf(String invoiceJsonString);
	
	Object cancelInvoice(String invoiceJsonString);
	
	Object getInvoiceByNumber(int number, boolean mayHaveCreditNote);
	
	Object getInvoicesByVatCode(String vatCode, boolean mayHaveCreditNote);
	
	Object getInvoicesByFiscalCode(String fiscalCode, boolean mayHaveCreditNote);
	
	Object getLastPrintedInvoices(int number, boolean mayHaveCreditNote);
}
