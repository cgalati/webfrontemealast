package com.ncr.webfront.base.invoicecreditnote.invoice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wf_document_details")
public class DocumentDetail extends ReceiptSale {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "wf_documents_id")
	public int getDocumentId() {
		return documentId;
	}
	
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	
	public String getDepartment() {
		return department;
	}
	
	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Column(name = "vat_rate")
	public int getVatRate() {
		return vatRate;
	}
	
	public void setVatRate(int vatRate) {
		this.vatRate = vatRate;
	}
	
	@Column(name = "net_price")
	public int getNetPrice() {
		return netPrice;
	}
	
	public void setNetPrice(int netPrice) {
		this.netPrice = netPrice;
	}
	
	@Column(name = "net_discount")
	public int getNetDiscount() {
		return netDiscount;
	}
	
	public void setNetDiscount(int netDiscount) {
		this.netDiscount = netDiscount;
	}
	
	@Column(name = "net_amount")
	public int getNetAmount() {
		return netAmount;
	}
	
	public void setNetAmount(int netAmount) {
		this.netAmount = netAmount;
	}
	
	@Column(name = "exmpt")
	public boolean isDiscountExempt() {
		return discountExempt;
	}
	
	public void setDiscountExempt(boolean discountExempt) {
		this.discountExempt = discountExempt;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	private int id;
	private int documentId;
	private String department;
	private int vatRate;
	private int netPrice;
	private int netDiscount;
	private int netAmount;
	private boolean discountExempt;
}
