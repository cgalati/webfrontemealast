package com.ncr.webfront.base.invoicecreditnote.invoice;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "idc_detail")
public class IdcDetail {
	
	public int getReg() {
		return reg;
	}
	public void setReg(int reg) {
		this.reg = reg;
	}
	
	public int getStore() {
		return store;
	}
	public void setStore(int store) {
		this.store = store;
	}
		
	public Date getDdate() {
		return ddate;
	}
	public void setDdate(Date ddate) {
		this.ddate = ddate;
	}
	
	public String getTtime() {
		return ttime;
	}
	public void setTtime(String ttime) {
		this.ttime = ttime;
	}
	
	@Id
	@Column(name = "sequencenumber")
	public int getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public int getTrans() {
		return trans;
	}
	public void setTrans(int trans) {
		this.trans = trans;
	}
	
	@Column(name = "transstep")
	public int getTransStep() {
		return transStep;
	}
	public void setTransStep(int transStep) {
		this.transStep = transStep;
	}

	@Column(name = "recordtype")
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	@Column(name = "recordcode")
	public String getRecordCode() {
		return recordCode;
	}
	public void setRecordCode(String recordCode) {
		this.recordCode = recordCode;
	}
	
	@Column(name = "userno")
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	
	public String getMisc() {
		return misc;
	}
	public void setMisc(String misc) {
		this.misc = misc;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	@Column(name = "eoddate")
	public String getEodDate() {
		return eodDate;
	}
	public void setEodDate(String eodDate) {
		this.eodDate = eodDate;
	}
	
	@Column(name = "eodtime")
	public String getEodTime() {
		return eodTime;
	}
	public void setEodTime(String eodTime) {
		this.eodTime = eodTime;
	}
	
	@Column(name = "discountableflag")
	public int getDiscountableFlag() {
		return discountableFlag;
	}
	public void setDiscountableFlag(int discountableFlag) {
		this.discountableFlag = discountableFlag;
	}
	
	@Column(name = "vatcode")
	public int getVatCode() {
		return vatCode;
	}
	public void setVatCode(int vatCode) {
		this.vatCode = vatCode;
	}
	
	@Column(name = "salesamount")	
	public int getSalesAmount() {
		return salesAmount;
	}
	public void setSalesAmount(int salesAmount) {
		this.salesAmount = salesAmount;
	}
	
	@Column(name = "itemdiscountamount")	
	public int getItemDiscountAmount() {
		return itemDiscountAmount;
	}
	public void setItemDiscountAmount(int itemDiscountAmount) {
		this.itemDiscountAmount = itemDiscountAmount;
	}
	
	@Column(name = "netamount1")	
	public int getNetAmount1() {
		return netAmount1;
	}
	public void setNetAmount1(int netAmount1) {
		this.netAmount1 = netAmount1;
	}
	
	@Column(name = "transdiscountamount")
	public int getTransDiscountAmount() {
		return transDiscountAmount;
	}
	public void setTransDiscountAmount(int transDiscountAmount) {
		this.transDiscountAmount = transDiscountAmount;
	}
	
	@Column(name = "discountqta")	
	public int getDiscountQta() {
		return discountQta;
	}
	public void setDiscountQta(int discountQta) {
		this.discountQta = discountQta;
	}
	
	@Column(name = "netamount2")
	public int getNetAmount2() {
		return netAmount2;
	}
	public void setNetAmount2(int netAmount2) {
		this.netAmount2 = netAmount2;
	}
	
	private int reg;	
	private int store;	
	private Date ddate;			
	private String ttime;			
	private int sequenceNumber;
	private int trans;			
	private int transStep;			
	private String recordType;			
	private String recordCode;			
	private int userNo;			
	private String misc;		
	private String data;			
	private String eodDate;			
	private String eodTime;
	private int discountableFlag;
	private int vatCode;
	private int salesAmount;
	private int itemDiscountAmount;
	private int netAmount1;
	private int transDiscountAmount;
	private int discountQta;
	private int netAmount2;
	
}
