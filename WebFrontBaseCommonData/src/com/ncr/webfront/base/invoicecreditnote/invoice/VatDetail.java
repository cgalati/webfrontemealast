package com.ncr.webfront.base.invoicecreditnote.invoice;

public class VatDetail {
	public VatDetail(int vatRate) {
		this.vatRate = vatRate;
	}
	
	public long getTaxable() {
		return taxable;
	}
	
	public void setTaxable(long taxable) {
		this.taxable = taxable;
	}
	
	public int getVatRate() {
		return vatRate;
	}
	
	public void setVatRate(int vatRate) {
		this.vatRate = vatRate;
	}
	
	public long getVatAmount() {
		return vatAmount;
	}
	
	public void setVatAmount(long vatAmount) {
		this.vatAmount = vatAmount;
	}
	
	public long getGross() {
		return gross;
	}
	
	public void setGross(long gross) {
		this.gross = gross;
	}
	
	private long taxable;
	private int vatRate;
	private long vatAmount;
	private long gross;
}
