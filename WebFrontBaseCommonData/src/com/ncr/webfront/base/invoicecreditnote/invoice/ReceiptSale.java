package com.ncr.webfront.base.invoicecreditnote.invoice;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ToStringBuilder;

@MappedSuperclass
public class ReceiptSale extends ReceiptItem {
	@Column(name = "ean")
	public String getItemCode() {
		return itemCode;
	}
	
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	@Transient
	public int getDepartmentId() {
		return departmentId;
	}
	
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	
	@Column(name = "vat_code")
	public int getVatCode() {
		return vatCode;
	}
	
	public void setVatCode(int vatCode) {
		this.vatCode = vatCode;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "measure_unit")
	public String getMeasureUnit() {
		return measureUnit;
	}
	
	public void setMeasureUnit(String measureUnit) {
		this.measureUnit = measureUnit;
	}
	
	public double getQuantity() {
		return quantity;
	}
	
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	@Column(name = "decimal_quantity")
	public boolean isDecimal() {
		return decimal;
	}
	
	public void setDecimal(boolean decimal) {
		this.decimal = decimal;
	}
	
	public int getDiscount() {
		return discount;
	}
	
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	private String itemCode;
	private int departmentId;
	private int vatCode;
	private int price;
	private String description;
	private String measureUnit;
	private double quantity;
	private boolean decimal;
	private int discount;
	private int amount;
}
