package com.ncr.webfront.base.invoicecreditnote.invoice;

public class ReceiptSearchType {
	public Integer getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(Integer receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	
	public String getPosTerminalCode() {
		return posTerminalCode;
	}
	public void setPosTerminalCode(String posTerminalCode) {
		this.posTerminalCode = posTerminalCode;
	}
	
	public String getFidelityCode() {
		return fidelityCode;
	}
	public void setFidelityCode(String fidelityCode) {
		this.fidelityCode = fidelityCode;
	}
	
	private Integer receiptNumber;
	private String posTerminalCode;
	private String fidelityCode;

	
}
