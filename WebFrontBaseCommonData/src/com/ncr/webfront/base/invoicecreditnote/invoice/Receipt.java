package com.ncr.webfront.base.invoicecreditnote.invoice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Receipt {
	
	public int getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(int receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	
	public String getPosTerminalCode() {
		return posTerminalCode;
	}
	public void setPosTerminalCode(String posTerminalCode) {
		this.posTerminalCode = posTerminalCode;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(int receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	
	public String getFidelityCode() {
		return fidelityCode;
	}
	public void setFidelityCode(String fidelityCode) {
		this.fidelityCode = fidelityCode;
	}
	
	public List<ReceiptSale> getSaleReceiptItems() {
		return saleReceiptItems;
	}
	public void setSaleReceiptItems(List<ReceiptSale> saleReceiptItems) {
		this.saleReceiptItems = saleReceiptItems;
	} 
	
	private int receiptNumber;
	private String store;
	private String posTerminalCode;
	private Date date;
	private int receiptAmount;
	private String fidelityCode;
	private List<ReceiptSale> saleReceiptItems = new ArrayList<>();

}
