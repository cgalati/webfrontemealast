package com.ncr.webfront.base.invoicecreditnote.customer;

public interface CustomerServiceRestInterface {
	public Object createDefaultCustomer();
	public Object addCustomer(String customerToAdd);
	
	public Object getAllCustomers();
	public Object getCustomersByName(String companyName);
	public Object getCustomersByFiscalOrVatCode(String code);
	public Object getCustomerByFidelityCode(String fidelityCode);
	
	public Object updateCustomer(String customerToUpdate);
	
	public Object deleteCustomer(String customerToDelete);
	
	public Object getAllCustomerTypes();
	public Object getAllExemptions();
	
	public Object getCustomerValidationData();
}
