package com.ncr.webfront.base.invoicecreditnote.customer;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wf_customers")
public class Customer {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "last_name_company_name")
	public String getLastNameCompanyName() {
		return lastNameCompanyName;
	}

	public void setLastNameCompanyName(String lastNameCompanyName) {
		this.lastNameCompanyName = lastNameCompanyName;
	}

	@Column(name = "vat_code")
	public String getVatCode() {
		return vatCode;
	}

	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}

	@Column(name = "fiscal_code")
	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	@Column(name = "fidelity_code")
	public String getFidelityCode() {
		return fidelityCode;
	}

	public void setFidelityCode(String fidelityCode) {
		this.fidelityCode = fidelityCode;
	}

	@ManyToOne
	@JoinColumn(name = "wf_customer_types_id")
	public CustomerType getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "wf_customer_addresses_id")
	public CustomerAddress getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(CustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}

	@ManyToOne
	@JoinColumn(name = "wf_exemptions_id")
	public Exemption getExemption() {
		return exemption;
	}

	public void setExemption(Exemption exemption) {
		this.exemption = exemption;
	}

	@Column(name = "last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	private int id = -1;
	private String lastNameCompanyName;
	private String vatCode;
	private String fiscalCode;
	private String fidelityCode;
	private CustomerType customerType;
	private CustomerAddress customerAddress = new CustomerAddress();
	private Exemption exemption;
	private Date lastUpdate;
}
