package com.ncr.webfront.base.invoicecreditnote.customer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wf_customer_types")
public class CustomerType {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(type).append(description).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
            return false;
		}
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof CustomerType)) {
            return false;
        }
		
		CustomerType other = (CustomerType)obj;
		return new EqualsBuilder().append(id, other.id).append(type, other.type).append(description, other.description)
				.isEquals();
	}
	
	private int id;
	private String type;
	private String description;
}
