package com.ncr.webfront.base.invoicecreditnote.customer;

import java.util.List;

import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public interface CustomerServiceInterface {
	public Customer createDefaultCustomer();
	public int addCustomer(Customer customerToAdd);
	
	public List<Customer> getAllCustomers();
	public List<Customer> getCustomersByName(String companyName);
	public List<Customer> getCustomersByFiscalOrVatCode(String code);
	public Customer getCustomerByFidelityCode(String fidelityCode);
	
	public void updateCustomer(Customer customerToUpdate);
	
	public void deleteCustomer(Customer customerToDelete);
	
	public List<CustomerType> getAllCustomerTypes();
	public List<Exemption> getAllExemptions();
	
	public List<WebFrontValidationData> getCustomerValidationData();
}
