package com.ncr.webfront.base.invoicecreditnote.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wf_exemptions")
public class Exemption {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getRate() {
		return rate;
	}
	
	public void setRate(int rate) {
		this.rate = rate;
	}
	
	public boolean isFacilitated() {
		return facilitated;
	}
	
	public void setFacilitated(boolean facilitated) {
		this.facilitated = facilitated;
	}

	@Column(name = "long_description")
	public String getLongDescription() {
		return longDescription;
	}
	
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(description).append(rate).append(facilitated)
				.append(longDescription).hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
            return false;
		}
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Exemption)) {
            return false;
        }
		
        Exemption other = (Exemption)obj;
		return new EqualsBuilder().append(id, other.id).append(description, other.description)
				.append(rate, other.rate).append(facilitated, other.facilitated)
				.append(longDescription, other.longDescription).isEquals();
	}
	
	private int id;
	private String description;
	private int rate;
	private boolean facilitated = true;
	private String longDescription;
}
