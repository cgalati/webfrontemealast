package com.ncr.webfront.base.invoicecreditnote.sequence;

import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;

public interface SequenceServiceInterface {
	List<MutablePair<DocumentType, Integer>> peekSequences();
	
	void setSequence(String key, int value);
}
