package com.ncr.webfront.base.invoicecreditnote.sequence;

public interface SequenceServiceRestInterface {
	Object peekSequences();
	
	Object setSequence(String key, int value);
}
