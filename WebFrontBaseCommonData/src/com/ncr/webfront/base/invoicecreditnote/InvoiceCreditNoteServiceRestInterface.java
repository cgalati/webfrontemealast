package com.ncr.webfront.base.invoicecreditnote;

import com.ncr.webfront.base.invoicecreditnote.creditnote.CreditNoteServiceRestInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerServiceRestInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.InvoiceServiceRestInterface;
import com.ncr.webfront.base.invoicecreditnote.sequence.SequenceServiceRestInterface;

public interface InvoiceCreditNoteServiceRestInterface extends CustomerServiceRestInterface, InvoiceServiceRestInterface, CreditNoteServiceRestInterface, SequenceServiceRestInterface {
	Object checkService();
	
	Object getPrintedDocumentByNumber(String number);
	
	Object getPrintedDocumentsByVatCode(String vatCode);
	
	Object getPrintedDocumentsByFiscalCode(String fiscalCode);
	
	Object getLastPrintedDocuments(int number);
}
