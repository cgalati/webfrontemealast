package com.ncr.webfront.base.invoicecreditnote.creditnote;

public interface CreditNoteServiceRestInterface {
	Object generateCreditNote(String invoice);
	
	Object renderHtmlCreditNote(String generatedCreditNote);
	
	Object renderPdfCreditNote(String generatedCreditNote);
	
	Object saveCreditNoteAndRenderPdf(String generatedCreditNote);
	
	Object cancelCreditNote(String creditNote);
}
