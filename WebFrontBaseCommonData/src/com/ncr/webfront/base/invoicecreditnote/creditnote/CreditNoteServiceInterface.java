package com.ncr.webfront.base.invoicecreditnote.creditnote;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;

public interface CreditNoteServiceInterface {
	Document generateCreditNote(Document invoice);
	
	byte[] renderHtmlCreditNote(Document generatedCreditNote);
	
	byte[] renderPdfCreditNote(Document generatedCreditNote);
	
	byte[] saveCreditNoteAndRenderPdf(Document generatedCreditNote);
	
	void cancelCreditNote(Document creditNote);
}
