package com.ncr.webfront.base.invoicecreditnote;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.creditnote.CreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.InvoiceServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.sequence.SequenceServiceInterface;

public interface InvoiceCreditNoteServiceInterface extends CustomerServiceInterface, InvoiceServiceInterface, CreditNoteServiceInterface, SequenceServiceInterface {
	ErrorObject checkService();
	
	Document getPrintedDocumentByNumber(int number);
	
	List<Document> getPrintedDocumentsByVatCode(String vatCode);
	
	List<Document> getPrintedDocumentsByFiscalCode(String fiscalCode);
	
	List<Document> getLastPrintedDocuments(int number);
}
