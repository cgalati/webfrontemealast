package com.ncr.webfront.base.posoperator;

public interface PosOperatorServiceRestInterface {
	
	public Object checkService();
	public Object getAll();
	public Object getByCode(String operatorCode);
	public Object searchByCode(String operatorCode);
	public Object searchByName(String name);
	public Object createDefaultCashier();
	public Object createCashierFromRole(String role);
	public Object addPosOperator(String posOperatorJsonString);
	public Object deletePosOperator(String operatorCode);
	public Object updatePosOperator(String posOperatorJsonString);
	public Object getPosOperatorValidationData();
	public Object getPosOperatorRoles();
	public Object getPosOperatorTraineeRole();
}
