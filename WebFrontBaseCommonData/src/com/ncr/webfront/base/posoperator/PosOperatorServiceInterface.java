package com.ncr.webfront.base.posoperator;

import java.util.List;
import java.util.Map;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.validation.PosOperatorValidationData;

public interface PosOperatorServiceInterface {

	public ErrorObject checkService();
	public List<PosOperator> getAll();
	public PosOperator getByCode(String operatorCode);
	public List<PosOperator> searchByCode(String code);
	public List<PosOperator> searchByName(String name);
	public PosOperator createDefaultCashier();
	public PosOperator createCashierFromRole(String role);
	public ErrorObject addPosOperator(PosOperator posOperatorToAdd);
	public ErrorObject deletePosOperator(String operatorCodeToDelete);
	public ErrorObject updatePosOperator(PosOperator posOperatorToUpdate);
	//public Map<String, Boolean> getCapabilities();
	public List<PosOperatorValidationData> getPosOperatorValidationData();
	public Map<Integer, String> getPosOperatorRoles();
	public String getPosOperatorTraineeRole();

}
