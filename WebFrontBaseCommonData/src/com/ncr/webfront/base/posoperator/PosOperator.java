package com.ncr.webfront.base.posoperator;

public class PosOperator {

	public PosOperator(String operatorCode, String name, String personnelNo, String secretNo, String role) {
		this.operatorCode = operatorCode;
		this.name = name;
		this.personnelNo = personnelNo;
		this.secretNo = secretNo;
		this.role = role;
	}

	public PosOperator() {
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getName() {
		return name;
	}

	public String getNameAndCode() {
		String nameAndCode = "";

		if (operatorCode.length() > 3) {
			nameAndCode += operatorCode.substring(operatorCode.length() - 3);
		} else {
			nameAndCode += operatorCode;
		}
		nameAndCode += "-";
		if (name.length() > 11) {
			nameAndCode += name.substring(0, 11);
		} else
			nameAndCode += name;

		return nameAndCode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecretNo() {
		return secretNo;
	}

	public void setSecretNo(String secretNo) {
		this.secretNo = secretNo;
	}

	public String getPersonnelNo() {
		return personnelNo;
	}

	public void setPersonnelNo(String personnelNo) {
		this.personnelNo = personnelNo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof PosOperator)) {
			return false;
		}
		final PosOperator posOperator = (PosOperator) obj;
		return this.getOperatorCode().equals(posOperator.getOperatorCode());
	}

	@Override
	public int hashCode() {
		return operatorCode.hashCode();
	};

	private String operatorCode;
	private String name;
	private String secretNo;
	private String personnelNo;
	private String role;

}
