package com.ncr.webfront.base.posterminal;

public interface PosTerminalServiceRestInterface {
	
	public Object checkService();
	public Object getAll();
	public Object getByCode (String terminalCode);

}
