package com.ncr.webfront.base.posterminal;

public enum PosTerminalType {
	REGISTER, CLIENT, SERVER, REGISTER_POS, REGISTER_SSCO, NONE
}
