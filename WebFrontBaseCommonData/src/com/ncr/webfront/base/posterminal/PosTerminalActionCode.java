package com.ncr.webfront.base.posterminal;

public enum PosTerminalActionCode {
	AC_NEVER_OPENED_TERMINAL("00"),
	AC_02("02"),
	AC_28("28"),
	AC_87("87"),
	AC_88("88"),
	AC_PRINTER_ERROR("97"),
	AC_DEFECTIVE_TERMINAL("98"),
	AC_ZEROED_TERMINAL("99");
	
	private PosTerminalActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	
	@Override
	public String toString() {
		return actionCode;
	}
	
	private String actionCode;
}
