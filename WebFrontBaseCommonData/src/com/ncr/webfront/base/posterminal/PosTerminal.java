package com.ncr.webfront.base.posterminal;

public class PosTerminal {

	public String getTerminalCode() {
		return terminalCode;
	}
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public PosTerminalType getType() {
		return type;
	}
	public void setType(PosTerminalType type) {
		this.type = type;
	}

	private String terminalCode;
	private String description;
	private PosTerminalType type;
}
