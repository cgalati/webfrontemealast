package com.ncr.webfront.base.posterminal;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface PosTerminalServiceInterface {
	
	public ErrorObject checkService();
	public List<PosTerminal> getAll();
	public PosTerminal getByCode (String terminalCode);

	// TODO x RELEASE 2.0
	/*
	* createPosTerminal (crea terminale campi default)
	*addPosTerminal (aggiunge terminale)
	*deletePosTerminal (cancella terminale)
	*updatePosTerminal (aggiorna un terminale esistente)
	*/

}
