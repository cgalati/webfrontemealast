package com.ncr.webfront.base.reports;

import java.util.List;
import java.util.Map;

import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface ReportsServiceInterface {
	public ErrorObject checkService();

	public List<String> getFormats();
		
	public byte[] generatePosOperatorCashReport(Map<PosOperator, String> posOperatorsTerminalCodeMap, String reportFormat);
	
	public byte[] generatePosOperatorReport(ReportOptions reportOptions);
	
	public byte[] generateFiancialReport(ReportOptions reportOptions);
	
	public byte[] generateHourlyActivityReport(ReportOptions reportOptions);
	
	public byte[] generateDepartmentReport(ReportOptions reportOptions);
	
	public ErrorObject generateEodReports(List<String> registerList);
}
