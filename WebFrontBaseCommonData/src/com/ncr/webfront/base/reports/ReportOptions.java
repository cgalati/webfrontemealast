package com.ncr.webfront.base.reports;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.ncr.webfront.base.posdepartment.PosDepartment;

public class ReportOptions {
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<String> getPosTerminals() {
		return posTerminals;
	}

	public void setPosTerminals(List<String> posTerminals) {
		this.posTerminals = posTerminals;
	}

	public List<String> getPosOperators() {
		return posOperators;
	}

	public void setPosOperators(List<String> posOperators) {
		this.posOperators = posOperators;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public boolean isConsolidated() {
		return consolidated;
	}

	public void setConsolidated(boolean daily) {
		this.consolidated = daily;
	}

	public List<PosDepartment> getPosDepartments() {
		return posDepartments;
	}

	public void setPosDepartments(List<PosDepartment> posDepartments) {
		this.posDepartments = posDepartments;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	private Date date;
	private List<String> posTerminals = new ArrayList<>();
	private List<String> posOperators = new ArrayList<>();
	private String format;
	private boolean consolidated;
	private String title;
	private String subTitle;
	private String additionalInfo;

	private List<PosDepartment> posDepartments = new ArrayList<>();
}
