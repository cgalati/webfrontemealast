package com.ncr.webfront.base.reports;

import java.util.List;

public class EndOfDayReportProperties {

	private String type;
	private String format;
	private String filename;
	private String title;
	private String subTitle;
	private boolean summary;
	private List<String> registers;
	
	public EndOfDayReportProperties(String type, String format, String filename, String title, String subTitle, boolean summary, List<String> registers) {
		this.type = type;
		this.format = format;
		this.filename = filename;
		this.title = title;
		this.subTitle=subTitle;
		this.summary = summary;
		this.registers = registers;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public boolean isSummary() {
		return summary;
	}

	public void setSummary(boolean summary) {
		this.summary = summary;
	}

	public List<String> getRegisters() {
		return registers;
	}

	public void setRegisters(List<String> registers) {
		this.registers = registers;
	}
}
