package com.ncr.webfront.base.reports;

public interface ReportsServiceRestInterface {

	public Object checkService();

	public Object getFormats();
	
	public Object generatePosOperatorReport(String reportOptionsJsonString);
	
	public Object generatePosOperatorCashReport(String reportOptionsJsonString);
	
	public Object generateFiancialReport(String reportOptionsJsonString);
	
	public Object generateHourlyActivityReport(String reportOptionsJsonString);
	
	public Object generateDepartmentReport(String reportOptionsJsonString);
	
	public Object generateEodReports(String posTerminalListJsonString);
}
