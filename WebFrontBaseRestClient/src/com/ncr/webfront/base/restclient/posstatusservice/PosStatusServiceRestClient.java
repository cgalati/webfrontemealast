package com.ncr.webfront.base.restclient.posstatusservice;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.posstatus.PosStatusServiceRestInterface;
import com.ncr.webfront.core.utils.common.RestConnector;

public class PosStatusServiceRestClient implements PosStatusServiceRestInterface {

	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public List<PosStatusObject> getAllPosStatus() {
		Type lstType = new TypeToken<List<PosStatusObject>>() {}.getType();
		
		return restConnector.get("getAllPosStatus", lstType);
	}

	@Override
	public PosStatusObject getPosStatus(String posCode) {
		return restConnector.get("getPosStatus/"+posCode, PosStatusObject.class);
	}

	RestConnector restConnector = new RestConnector(serverBaseUrl);
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/posStatusService/";

}
