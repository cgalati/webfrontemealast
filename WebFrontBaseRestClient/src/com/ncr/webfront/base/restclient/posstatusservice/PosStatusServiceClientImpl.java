package com.ncr.webfront.base.restclient.posstatusservice;

import java.util.List;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.posstatus.PosStatusServiceInterface;
import com.ncr.webfront.base.posstatus.PosStatusServiceRestInterface;

@SuppressWarnings("unchecked")
public class PosStatusServiceClientImpl implements PosStatusServiceInterface {

	@Override
	public ErrorObject checkService() {
		return (ErrorObject) posStatusServiceRest.checkService();
	}
	
    @Override
	public List<PosStatusObject> getAllPosStatus() {
		return (List<PosStatusObject>) posStatusServiceRest.getAllPosStatus();
	}

	@Override
	public PosStatusObject getPosStatus(String posCode) {
		return (PosStatusObject) posStatusServiceRest.getPosStatus(posCode);
	}

	private static final PosStatusServiceRestInterface posStatusServiceRest = new PosStatusServiceRestClient();

}
