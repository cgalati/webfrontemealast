package com.ncr.webfront.base.restclient.posdepartmentservice;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceRestInterface;
import com.ncr.webfront.core.utils.common.RestConnector;

public class PosDepartmentServiceRestClient implements PosDepartmentServiceRestInterface {

	public PosDepartmentServiceRestClient()
	{
		restConnector = new RestConnector(serverBaseUrl);
	}
	
	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public Object getAll() {
		
		Type listType = new TypeToken<ArrayList<PosDepartment>>() {
		}.getType();
		return restConnector.get("getAll", listType);
	}

	@Override
	public Object getDepartmentsOfLevel(int level) {
		Type listType = new TypeToken<ArrayList<PosDepartment>>() {
		}.getType();
		return restConnector.get("getDepartmentsOfLevel/"+level, listType);
	}
	
	@Override
	public Object getByCode(String dptCode) {
		return restConnector.get("getByCode/"+dptCode, PosDepartment.class);
	}


	@Override
	public ErrorObject addDepartment(String departmentJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("addDepartment", departmentJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}

	}

	@Override
	public ErrorObject deleteDepartment(String departmentCodeToDelete) {
		
		ErrorObject eo = null;
		eo = restConnector.get("deleteDepartment/" + departmentCodeToDelete.trim(), ErrorObject.class);

		if (eo != null) {
			return eo;
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public ErrorObject updateDepartment(String departmentJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("updateDepartment", departmentJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public PosDepartment createDepartment() {
		return restConnector.get("createDepartment", PosDepartment.class);
	}

	private RestConnector restConnector = null;
																				  
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/posDepartmentService/";
	private static final Gson gson = new Gson();
}
