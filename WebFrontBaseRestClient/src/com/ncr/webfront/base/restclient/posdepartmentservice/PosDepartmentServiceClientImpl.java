package com.ncr.webfront.base.restclient.posdepartmentservice;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.google.gson.Gson;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceRestInterface;
@SuppressWarnings("unchecked")
public class PosDepartmentServiceClientImpl implements PosDepartmentServiceInterface {


	@Override
	public ErrorObject checkService() {
		return (ErrorObject) posDepartmentServiceRest.checkService();
	}
	
	@Override
	public List<PosDepartment> getAll() {
		return (List<PosDepartment>) posDepartmentServiceRest.getAll();
	}

	@Override
	public List<PosDepartment> getDepartmentsOfLevel(int level) {
		return (List<PosDepartment>) posDepartmentServiceRest.getDepartmentsOfLevel(level);
	}

	@Override
	public PosDepartment getByCode(String dptCode) {
		return (PosDepartment) posDepartmentServiceRest.getByCode(dptCode);
	}
	
	@Override
	public ErrorObject deleteDepartment(String departmentCodeToDelete) {
		return (ErrorObject) posDepartmentServiceRest.deleteDepartment(departmentCodeToDelete);
	}

	@Override
	public ErrorObject addDepartment(PosDepartment departmentToAdd) {
		Gson gson = new Gson();
		String jsonDepartment = "";
		jsonDepartment = gson.toJson(departmentToAdd);
		gson = null;
		return (ErrorObject) posDepartmentServiceRest.addDepartment(jsonDepartment);
	}

	@Override
	public ErrorObject updateDepartment(PosDepartment departmentToUpdate) {
		Gson gson = new Gson();
		String jsonDepartment = "";
		jsonDepartment = gson.toJson(departmentToUpdate);
		gson = null;
		return (ErrorObject) posDepartmentServiceRest.updateDepartment(jsonDepartment);
	}

	@Override
	public PosDepartment createDepartment() {
		return (PosDepartment) posDepartmentServiceRest.createDepartment();
	}
	
	private PosDepartmentServiceRestInterface posDepartmentServiceRest = new PosDepartmentServiceRestClient();

}
