package com.ncr.webfront.base.restclient.invoicecreditnoteservice;

import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.google.gson.Gson;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceRestInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerType;
import com.ncr.webfront.base.invoicecreditnote.customer.Exemption;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSearchType;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

@SuppressWarnings("unchecked")
public class InvoiceCreditNoteServiceClientImpl implements InvoiceCreditNoteServiceInterface {

	@Override
	public ErrorObject checkService() {
		return (ErrorObject) invoiceCreditNoteService.checkService();
	}
	
	
	@Override
	public Customer createDefaultCustomer() {
		return (Customer) invoiceCreditNoteService.createDefaultCustomer();
	}

	@Override
	public int addCustomer(Customer customerToAdd) {
		return (int) invoiceCreditNoteService.addCustomer(gson.toJson(customerToAdd));
	}

	@Override
	public List<Customer> getAllCustomers() {
		return (List<Customer>) invoiceCreditNoteService.getAllCustomers();
	}

	@Override
	public List<Customer> getCustomersByName(String companyName) {
		return (List<Customer>) invoiceCreditNoteService.getCustomersByName(companyName);
	}

	@Override
	public List<Customer> getCustomersByFiscalOrVatCode(String code) {
		return (List<Customer>) invoiceCreditNoteService.getCustomersByFiscalOrVatCode(code);
	}
	
	@Override
	public Customer getCustomerByFidelityCode(String fidelityCode) {
		return (Customer) invoiceCreditNoteService.getCustomerByFidelityCode(fidelityCode);
	}

	@Override
	public void updateCustomer(Customer customerToUpdate) {
		invoiceCreditNoteService.updateCustomer(gson.toJson(customerToUpdate));
	}

	@Override
	public void deleteCustomer(Customer customerToDelete) {
		invoiceCreditNoteService.deleteCustomer(gson.toJson(customerToDelete));
	}

	@Override
	public List<CustomerType> getAllCustomerTypes() {
		return (List<CustomerType>) invoiceCreditNoteService.getAllCustomerTypes();
	}

	@Override
	public List<Exemption> getAllExemptions() {
		return (List<Exemption>) invoiceCreditNoteService.getAllExemptions();
	}
	
	@Override
	public List<WebFrontValidationData> getCustomerValidationData() {
		return (List<WebFrontValidationData>) invoiceCreditNoteService.getCustomerValidationData();
	}
	
	@Override
	public List<Receipt> searchReceipts(ReceiptSearchType receiptSearchType) {
		return (List<Receipt>) invoiceCreditNoteService.searchReceipts(gson.toJson(receiptSearchType));
	}
	
	@Override
	public Receipt fillReceipt(ReceiptSearchType receiptSearchType) {
		return (Receipt) invoiceCreditNoteService.fillReceipt(gson.toJson(receiptSearchType));
	}
	
	@Override
	public Document createInvoice(Receipt receipt) {
		return (Document) invoiceCreditNoteService.createInvoice(gson.toJson(receipt));
	}
	
	@Override
	public Document generateInvoice(Document invoice) {
		return (Document) invoiceCreditNoteService.generateInvoice(gson.toJson(invoice));
	}
	
	@Override
	public byte[] renderHtmlInvoice(Document generatedInvoice) {
		return (byte[]) invoiceCreditNoteService.renderHtmlInvoice(gson.toJson(generatedInvoice));
	}
	
	@Override
	public byte[] renderPdfInvoice(Document generatedInvoice) {
		return (byte[]) invoiceCreditNoteService.renderPdfInvoice(gson.toJson(generatedInvoice));
	}
	
	@Override
	public byte[] saveInvoiceAndRenderPdf(Document generatedInvoice) {
		return (byte[]) invoiceCreditNoteService.saveInvoiceAndRenderPdf(gson.toJson(generatedInvoice));
	}
	
	@Override
	public void cancelInvoice(Document invoice) {
		invoiceCreditNoteService.cancelInvoice(gson.toJson(invoice));
	}
	
	@Override
	public Document getInvoiceByNumber(int number, boolean mayHaveCreditNote) {
		return (Document) invoiceCreditNoteService.getInvoiceByNumber(number, mayHaveCreditNote);
	}
	
	@Override
	public List<Document> getInvoicesByFiscalCode(String fiscalCode, boolean mayHaveCreditNote) {
		return (List<Document>) invoiceCreditNoteService.getInvoicesByFiscalCode(fiscalCode, mayHaveCreditNote);
	}
	
	@Override
	public List<Document> getInvoicesByVatCode(String vatCode, boolean mayHaveCreditNote) {
		return (List<Document>) invoiceCreditNoteService.getInvoicesByVatCode(vatCode, mayHaveCreditNote);
	}
	
	@Override
	public List<Document> getLastPrintedInvoices(int number, boolean mayHaveCreditNote) {
		return (List<Document>) invoiceCreditNoteService.getLastPrintedInvoices(number, mayHaveCreditNote);
	}
	
	@Override
	public Document generateCreditNote(Document invoice) {
		return (Document) invoiceCreditNoteService.generateCreditNote(gson.toJson(invoice));
	}


	@Override
	public byte[] renderHtmlCreditNote(Document generatedCreditNote) {
		return (byte[]) invoiceCreditNoteService.renderHtmlCreditNote(gson.toJson(generatedCreditNote));
	}


	@Override
	public byte[] renderPdfCreditNote(Document generatedCreditNote) {
		return (byte[]) invoiceCreditNoteService.renderPdfCreditNote(gson.toJson(generatedCreditNote));
	}


	@Override
	public byte[] saveCreditNoteAndRenderPdf(Document generatedCreditNote) {
		return (byte[]) invoiceCreditNoteService.saveCreditNoteAndRenderPdf(gson.toJson(generatedCreditNote));
	}


	@Override
	public void cancelCreditNote(Document creditNote) {
		invoiceCreditNoteService.cancelCreditNote(gson.toJson(creditNote));
	}
	
	@Override
	public List<MutablePair<DocumentType, Integer>> peekSequences() {
		return (List<MutablePair<DocumentType, Integer>>) invoiceCreditNoteService.peekSequences();
	}
	
	@Override
	public void setSequence(String key, int value) {
		invoiceCreditNoteService.setSequence(key, value);
	}
	
	@Override
	public Document getPrintedDocumentByNumber(int number) {
		return (Document) invoiceCreditNoteService.getPrintedDocumentByNumber(Integer.toString(number));
	}
	
	@Override
	public List<Document> getPrintedDocumentsByFiscalCode(String fiscalCode) {
		return (List<Document>) invoiceCreditNoteService.getPrintedDocumentsByFiscalCode(fiscalCode);
	}
	
	@Override
	public List<Document> getPrintedDocumentsByVatCode(String vatCode) {
		return (List<Document>) invoiceCreditNoteService.getPrintedDocumentsByVatCode(vatCode);
	}
	
	@Override
	public List<Document> getLastPrintedDocuments(int number) {
		return (List<Document>) invoiceCreditNoteService.getLastPrintedDocuments(number);
	}
	
	private InvoiceCreditNoteServiceRestInterface invoiceCreditNoteService = new InvoiceCreditNoteServiceRestClient();
	private Gson gson = new Gson();
}
