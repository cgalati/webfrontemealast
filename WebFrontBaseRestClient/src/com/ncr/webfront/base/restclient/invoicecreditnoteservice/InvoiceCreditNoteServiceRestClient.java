package com.ncr.webfront.base.restclient.invoicecreditnoteservice;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceRestInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerType;
import com.ncr.webfront.base.invoicecreditnote.customer.Exemption;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public class InvoiceCreditNoteServiceRestClient implements InvoiceCreditNoteServiceRestInterface {
	@Override
	public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public Customer createDefaultCustomer() {
		return restConnector.get("createDefaultCustomer", Customer.class);
	}

	@Override
	public Object addCustomer(String customerToAdd) {
		return restConnector.put("addCustomer", customerToAdd, Integer.class);
	}

	@Override
	public List<Customer> getAllCustomers() {
		Type listType = new TypeToken<List<Customer>>() {}.getType();
		
		return restConnector.get("getAllCustomers", listType);
	}

	@Override
	public List<Customer> getCustomersByName(String companyName) {
		Type listType = new TypeToken<List<Customer>>() {}.getType();
		
		return restConnector.get("getCustomersByName/" + companyName, listType);
	}

	@Override
	public List<Customer> getCustomersByFiscalOrVatCode(String code) {
		Type listType = new TypeToken<List<Customer>>() {}.getType();
		
		return restConnector.get("getCustomersByFiscalOrVatCode/" + code, listType);
	}
	
	@Override
	public Object getCustomerByFidelityCode(String fidelityCode) {
		return restConnector.get("getCustomerByFidelityCode/" + fidelityCode, Customer.class);
	}

	@Override
	public Object updateCustomer(String customerToUpdate) {
		restConnector.put("updateCustomer", customerToUpdate);
		
		return null;
	}

	@Override
	public Object deleteCustomer(String customerToDelete) {
		restConnector.put("deleteCustomer", customerToDelete);
		
		return null;
	}

	@Override
	public List<CustomerType> getAllCustomerTypes() {
		Type listType = new TypeToken<List<CustomerType>>() {}.getType();
		
		return restConnector.get("getAllCustomerTypes", listType);
	}

	@Override
	public List<Exemption> getAllExemptions() {
		Type listType = new TypeToken<List<Exemption>>() {}.getType();
		
		return restConnector.get("getAllExemptions", listType);
	}
	
	@Override
	public List<WebFrontValidationData> getCustomerValidationData() {
		Type listType = new TypeToken<List<WebFrontValidationData>>() {}.getType();
		
		return restConnector.get("getCustomerValidationData", listType);
	}
	
	@Override
	public List<Receipt> searchReceipts(String receiptSearchTypeJsonString) {		
		Type listType = new TypeToken<List<Receipt>>() {}.getType();
		
		return restConnector.put("searchReceipts", receiptSearchTypeJsonString, listType);
	}
	
	@Override
	public Object fillReceipt(String receiptSearchTypeJsonString) {
		return restConnector.put("fillReceipt", receiptSearchTypeJsonString, Receipt.class);
	}
	
	@Override
	public Object createInvoice(String receiptJsonString) {
		return restConnector.put("createInvoice", receiptJsonString, Document.class);
	}
	
	@Override
	public Object generateInvoice(String invoiceJsonString) {
		return restConnector.put("generateInvoice", invoiceJsonString, Document.class);
	}
	
	@Override
	public Object renderHtmlInvoice(String invoiceJsonString) {
		Type arrayType = new TypeToken<byte[]>() {}.getType();
		
		return restConnector.put("renderHtmlInvoice", invoiceJsonString, arrayType);
	}
	
	@Override
	public Object renderPdfInvoice(String invoiceJsonString) {
		Type arrayType = new TypeToken<byte[]>() {}.getType();
		
		return restConnector.put("renderPdfInvoice", invoiceJsonString, arrayType);
	}
	
	@Override
	public Object saveInvoiceAndRenderPdf(String invoiceJsonString) {
		Type arrayType = new TypeToken<byte[]>() {}.getType();
		
		return restConnector.put("saveInvoiceAndRenderPdf", invoiceJsonString, arrayType);
	}
	
	@Override
	public Object cancelInvoice(String invoiceJsonString) {
		restConnector.put("cancelInvoice", invoiceJsonString);
		
		return null;
	}
	
	@Override
	public Object getInvoiceByNumber(int number, boolean mayHaveCreditNote) {
		return restConnector.get("getInvoiceByNumber/" + number + "/" + mayHaveCreditNote, Document.class);
	}
	
	@Override
	public Object getInvoicesByFiscalCode(String fiscalCode, boolean mayHaveCreditNote) {
		Type listType = new TypeToken<List<Document>>() {}.getType();
		
		return restConnector.get("getInvoicesByFiscalCode/" + fiscalCode + "/" + mayHaveCreditNote, listType);
	}
	
	@Override
	public Object getInvoicesByVatCode(String vatCode, boolean mayHaveCreditNote) {
		Type listType = new TypeToken<List<Document>>() {}.getType();
		
		return restConnector.get("getInvoicesByVatCode/" + vatCode + "/" + mayHaveCreditNote, listType);
	}
	
	@Override
	public Object getLastPrintedInvoices(int number, boolean mayHaveCreditNote) {
		Type listType = new TypeToken<List<Document>>() {}.getType();
		
		return restConnector.get("getLastPrintedInvoices/" + number + "/" + mayHaveCreditNote, listType);
	}
	
	@Override
	public Object generateCreditNote(String invoice) {
		return restConnector.put("generateCreditNote", invoice, Document.class);
	}

	@Override
	public Object renderHtmlCreditNote(String generatedCreditNote) {
		Type arrayType = new TypeToken<byte[]>() {}.getType();
		
		return restConnector.put("renderHtmlCreditNote", generatedCreditNote, arrayType);
	}
	
	@Override
	public Object renderPdfCreditNote(String generatedCreditNote) {
		Type arrayType = new TypeToken<byte[]>() {}.getType();
		
		return restConnector.put("renderPdfCreditNote", generatedCreditNote, arrayType);
	}

	@Override
	public Object saveCreditNoteAndRenderPdf(String generatedCreditNote) {
		Type arrayType = new TypeToken<byte[]>() {}.getType();
		
		return restConnector.put("saveCreditNoteAndRenderPdf", generatedCreditNote, arrayType);
	}

	@Override
	public Object cancelCreditNote(String creditNote) {
		return restConnector.put("cancelCreditNote", creditNote, Object.class);
	}
	
	@Override
	public Object peekSequences() {
		Type listType = new TypeToken<List<MutablePair<DocumentType, Integer>>>() {}.getType();
		
		return restConnector.get("peekSequences", listType);
	}
	
	@Override
	public Object setSequence(String key, int value) {
		return restConnector.get("setSequence/" + key + "/" + value, Object.class);
	}
	
	@Override
	public Object getPrintedDocumentByNumber(String number) {
		return restConnector.get("getPrintedDocumentByNumber/" + number, Document.class);
	}
	
	@Override
	public Object getPrintedDocumentsByFiscalCode(String fiscalCode) {
		Type listType = new TypeToken<List<Document>>() {}.getType();
		
		return restConnector.get("getPrintedDocumentsByFiscalCode/" + fiscalCode, listType);
	}
	
	@Override
	public Object getPrintedDocumentsByVatCode(String vatCode) {
		Type listType = new TypeToken<List<Document>>() {}.getType();
		
		return restConnector.get("getPrintedDocumentsByVatCode/" + vatCode, listType);
	}
	
	@Override
	public Object getLastPrintedDocuments(int number) {
		Type listType = new TypeToken<List<Document>>() {}.getType();
		
		return restConnector.get("getLastPrintedDocuments/" + number, listType);
	}
	
	private String serviceUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/invoiceCreditNoteService/";
	private RestConnector restConnector = new RestConnector(serviceUrl);
}
