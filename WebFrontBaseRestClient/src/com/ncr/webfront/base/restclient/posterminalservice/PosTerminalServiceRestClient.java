package com.ncr.webfront.base.restclient.posterminalservice;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceRestInterface;
import com.ncr.webfront.core.utils.common.RestConnector;

public class PosTerminalServiceRestClient implements PosTerminalServiceRestInterface {
	
	public PosTerminalServiceRestClient()
	{
		restConnector = new RestConnector(serverBaseUrl);
	}

	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	
	@Override
	public Object getAll() {
		Type listType = new TypeToken<ArrayList<PosTerminal>>() {
		}.getType();
		return restConnector.get("getAll", listType);
	}

	@Override
	public Object getByCode(String terminalCode) {
		return restConnector.get("getByCode/"+terminalCode, PosTerminal.class);
	}
	
	private RestConnector restConnector = null;
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/posTerminalService/";

}
