package com.ncr.webfront.base.restclient.posterminalservice;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.posterminal.PosTerminalServiceRestInterface;
@SuppressWarnings("unchecked")

public class PosTerminalServiceClientImpl implements PosTerminalServiceInterface {


	@Override
	public ErrorObject checkService() {
		return (ErrorObject) posTerminalServiceRest.checkService();
	}
	
	
	@Override
	public List<PosTerminal> getAll() {
		return (List<PosTerminal>) posTerminalServiceRest.getAll();
	}

	@Override
	public PosTerminal getByCode(String terminalCode) {
		return (PosTerminal) posTerminalServiceRest.getByCode(terminalCode);
	}
	
	private PosTerminalServiceRestInterface posTerminalServiceRest = new PosTerminalServiceRestClient();

}
