package com.ncr.webfront.base.restclient.commonlists;

import java.lang.reflect.Type;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonlists.CommonListsRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.common.RestConnector;

public class CommonListsServiceRestClient implements CommonListsRestInterface {

	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
		
	public CommonListsServiceRestClient() {
		restConnector = new RestConnector(serverBaseUrl);
	}
	
	@Override
	public Object getManualDiscountCodeList() {
		Type listType = new TypeToken<List<MutablePair<Integer, String>>>() {}.getType();
		return restConnector.get("getManualDiscountCodeList", listType);
	}

	@Override
	public Object getManualDiscountCodeById(int idManualDiscountCode) {
		Type pairType = new TypeToken<MutablePair<Integer, String>>() {}.getType();
		
		return restConnector.get("getManualDiscountCodeById/" + idManualDiscountCode, pairType);
	}

	@Override
	public Object getPackagingTypeList() {
		Type listType = new TypeToken<List<MutablePair<String, String>>>() {}.getType();
		
		return restConnector.get("getPackagingTypeList", listType);
	}

	@Override
	public Object getPackagingTypeById(String idPackagingType) {
		Type pairType = new TypeToken<MutablePair<Integer, String>>() {}.getType();
		
		return restConnector.get("getPackagingTypeById/" + idPackagingType, pairType);
	}

	@Override
	public Object getPrizeCodeList() {
		Type listType = new TypeToken<List<MutablePair<Integer, String>>>() {}.getType();
		
		return restConnector.get("getPrizeCodeList", listType);
	}

	@Override
	public Object getPrizeCodeById(int idPrizeCode) {
		Type pairType = new TypeToken<MutablePair<Integer, String>>() {}.getType();
		
		return restConnector.get("getPrizeCodeById/" + idPrizeCode, pairType);
	}

	@Override
	public Object getZeroPriceBehaviourList() {
		Type listType = new TypeToken<List<MutablePair<Integer, String>>>() {}.getType();
		
		return restConnector.get("getZeroPriceBehaviourList", listType);
	}

	@Override
	public Object getZeroPriceBehaviourById(int idZeroPriceBehaviour) {
		Type pairType = new TypeToken<MutablePair<Integer, String>>() {}.getType();
		
		return restConnector.get("getZeroPriceBehaviourById/" + idZeroPriceBehaviour, pairType);
	}

	@Override
	public Object getAutoDiscountTypeList() {
		Type listType = new TypeToken<List<MutablePair<Integer, String>>>() {}.getType();
		
		return restConnector.get("getAutoDiscountTypeList", listType);
	}

	@Override
	public Object getAutoDiscountTypeById(int idAutoDiscountType) {
		Type pairType = new TypeToken<MutablePair<Integer, String>>() {}.getType();
		
		return restConnector.get("getAutoDiscountTypeById/" + idAutoDiscountType, pairType);
	}

	@Override
	public Object getUpbOperationTypeList() {
		Type listType = new TypeToken<List<MutablePair<Integer, String>>>() {}.getType();
		
		return restConnector.get("getUpbOperationTypeList", listType);
	}

	@Override
	public Object getUpbOperationTypeById(int idUpbOperationType) {
		Type pairType = new TypeToken<MutablePair<Integer, String>>() {}.getType();
		
		return restConnector.get("getUpbOperationTypeById/" + idUpbOperationType, pairType);
	}
	
	private RestConnector restConnector = null;
	private String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/commonListsService/";
}
