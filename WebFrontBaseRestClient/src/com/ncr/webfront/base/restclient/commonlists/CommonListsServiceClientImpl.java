package com.ncr.webfront.base.restclient.commonlists;

import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;

import com.ncr.webfront.core.utils.commonlists.CommonListsRestInterface;
import com.ncr.webfront.core.utils.commonlists.CommonListsServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

@SuppressWarnings("unchecked")
public class CommonListsServiceClientImpl implements CommonListsServiceInterface {
	
	@Override
	public ErrorObject checkService() {
		return (ErrorObject) commonListsRest.checkService();
	}
	
	@Override
	public List<MutablePair<Integer, String>> getManualDiscountCodeList() {
		return (List<MutablePair<Integer, String>>) commonListsRest.getManualDiscountCodeList();
	}

	@Override
	public MutablePair<Integer, String> getManualDiscountCodeById(
			int idManualDiscountCode) {
		return (MutablePair<Integer, String>) commonListsRest.getManualDiscountCodeById(idManualDiscountCode);
	}

	@Override
	public List<MutablePair<String, String>> getPackagingTypeList() {
		return (List<MutablePair<String, String>>) commonListsRest.getPackagingTypeList();
	}

	@Override
	public MutablePair<String, String> getPackagingTypeById(String idPackagingType) {
		return (MutablePair<String, String>) commonListsRest.getPackagingTypeById(idPackagingType);
	}

	@Override
	public List<MutablePair<Integer, String>> getPrizeCodeList() {
		return (List<MutablePair<Integer, String>>) commonListsRest.getPrizeCodeList();
	}

	@Override
	public MutablePair<Integer, String> getPrizeCodeById(int idPrizeCode) {
		return (MutablePair<Integer, String>) commonListsRest.getPrizeCodeById(idPrizeCode);
	}

	@Override
	public List<MutablePair<Integer, String>> getZeroPriceBehaviourList() {
		return (List<MutablePair<Integer, String>>) commonListsRest.getZeroPriceBehaviourList();
	}

	@Override
	public MutablePair<Integer, String> getZeroPriceBehaviourById(
			int idZeroPriceBehaviour) {
		return (MutablePair<Integer, String>) commonListsRest.getZeroPriceBehaviourById(idZeroPriceBehaviour);
	}

	@Override
	public List<MutablePair<Integer, String>> getAutoDiscountTypeList() {
		return (List<MutablePair<Integer, String>>) commonListsRest.getAutoDiscountTypeList();
	}

	@Override
	public MutablePair<Integer, String> getAutoDiscountTypeById(
			int idAutoDiscountType) {
		return (MutablePair<Integer, String>) commonListsRest.getAutoDiscountTypeById(idAutoDiscountType);
	}

	@Override
	public List<MutablePair<Integer, String>> getUpbOperationTypeList() {
		return (List<MutablePair<Integer, String>>) commonListsRest.getUpbOperationTypeList();
	}

	@Override
	public MutablePair<Integer, String> getUpbOperationTypeById(
			int idUpbOperationType) {
		return (MutablePair<Integer, String>) commonListsRest.getUpbOperationTypeById(idUpbOperationType);
	}
	
	private CommonListsRestInterface commonListsRest = new CommonListsServiceRestClient();
}
