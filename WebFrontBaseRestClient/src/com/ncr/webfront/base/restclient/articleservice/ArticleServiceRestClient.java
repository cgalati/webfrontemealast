package com.ncr.webfront.base.restclient.articleservice;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public class ArticleServiceRestClient implements ArticleServiceRestInterface {

	public ArticleServiceRestClient()
	{
		restConnector = new RestConnector(serverBaseUrl);
		
	}

	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public List<Article> searchByPluCode(String plucode) {
		List<Article> listOfArticles;
		Type listType = new TypeToken<ArrayList<Article>>() {
		}.getType();
		listOfArticles = restConnector.get("searchByPluCode/" + plucode.trim(), listType);
		return listOfArticles;

	}

	@Override
	public List<Article> searchByArticleCode(String internalId) {
		List<Article> listOfArticles;
		Type listType = new TypeToken<ArrayList<Article>>() {
		}.getType();
		listOfArticles = restConnector.get("searchByArticleCode/" + internalId.trim(), listType);
		return listOfArticles;
	}

	@Override
	public List<Article> searchByStartsWithArticleCode(String internalId) {
		List<Article> listOfArticles;
		Type listType = new TypeToken<ArrayList<Article>>() {
		}.getType();								
		listOfArticles = restConnector.get("searchByStartsWithArticleCode/" + internalId.trim(), listType);
		return listOfArticles;
	}

	@Override
	public List<Article> searchByDescription(String description) {
		List<Article> listOfArticles;
		Type listType = new TypeToken<ArrayList<Article>>() {
		}.getType();
		if ((description != null) && ("" != description))
			listOfArticles = restConnector.get("searchByDescription/" + description.trim(), listType);
		else
			listOfArticles = restConnector.get("searchByDescription", listType);

		return listOfArticles;
	}

	@Override
	public ErrorObject addArticle(String articleJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("addArticle", articleJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}

	}

	@Override
	public ErrorObject deleteArticle(String articleCodeToDelete) {
		
		ErrorObject eo = null;
		eo = restConnector.get("deleteArticle/" + articleCodeToDelete.trim(), ErrorObject.class);

		if (eo != null) {
			return eo;
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public ErrorObject updateArticle(String articleJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("updateArticle", articleJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public Article createArticle() {
		return restConnector.get("createArticle", Article.class);
	}

	@Override
	public Map<String, Boolean> getCapabilities() {
		Type mapType = new TypeToken<Map<String, Boolean>>() {
		}.getType();
		return restConnector.get("getCapabilities", mapType);
	}
	
	@Override
    public Object getValidationData() {
		Type listType = new TypeToken<List<WebFrontValidationData>>() {
		}.getType();
		return restConnector.get("getValidationData", listType);
    }

	private RestConnector restConnector = null;
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/articleMaintenance/";
	private static final Gson gson = new Gson();
	
}
