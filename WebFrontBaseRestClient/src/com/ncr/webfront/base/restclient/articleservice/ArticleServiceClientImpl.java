package com.ncr.webfront.base.restclient.articleservice;

import java.util.List;
import java.util.Map;
import com.google.gson.Gson;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.article.ArticleServiceRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

@SuppressWarnings("unchecked")
public class ArticleServiceClientImpl implements ArticleServiceInterface {


	@Override
	public ErrorObject checkService() {
		return (ErrorObject) articleServiceRest.checkService();
	}
	
	@Override
	public List<Article> searchByArticleCode(String articleCode) {
		return (List<Article>) articleServiceRest.searchByArticleCode(articleCode);
	}

	@Override
	public List<Article> searchByStartsWithArticleCode(String articleCode) {
		return (List<Article>) articleServiceRest.searchByStartsWithArticleCode(articleCode);
	}

	@Override
	public List<Article> searchByPluCode(String pluCode) {
		return (List<Article>) articleServiceRest.searchByPluCode(pluCode);
	}

	@Override
	public List<Article> searchByDescription(String description) {
		return (List<Article>) articleServiceRest.searchByDescription(description);
	}

	@Override
	public ErrorObject deleteArticle(String articleCodeToDelete) {
		return (ErrorObject) articleServiceRest.deleteArticle(articleCodeToDelete);
	}

	@Override
	public ErrorObject addArticle(Article articleToAdd) {
		Gson gson = new Gson();
		String jsonArticle = "";
		jsonArticle = gson.toJson(articleToAdd);
		gson = null;
		return (ErrorObject) articleServiceRest.addArticle(jsonArticle);
	}

	@Override
	public ErrorObject updateArticle(Article articleToUpdate) {
		Gson gson = new Gson();
		String jsonArticle = "";
		jsonArticle = gson.toJson(articleToUpdate);
		gson = null;
		return (ErrorObject) articleServiceRest.updateArticle(jsonArticle);
	}

	@Override
	public Article createArticle() {
		return (Article) articleServiceRest.createArticle();
	}

	@Override
	public Map<String, Boolean> getCapabilities() {
		return (Map<String, Boolean>) articleServiceRest.getCapabilities();
	}

	@Override
    public List<WebFrontValidationData> getValidationData(){ 
	    return (List<WebFrontValidationData>) articleServiceRest.getValidationData();
    }
	private static final ArticleServiceRestInterface articleServiceRest = new ArticleServiceRestClient();

	}
