package com.ncr.webfront.base.restclient.postransactionservice;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.postransaction.PosTransactionSearchCriteria;
import com.ncr.webfront.base.postransaction.PosTransactionServiceInterface;
import com.ncr.webfront.base.postransaction.PosTransactionServiceRestInterface;
import com.ncr.webfront.base.postransaction.PosTransactionSummary;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@SuppressWarnings("unchecked")
public class PosTransactionServiceClientImpl implements PosTransactionServiceInterface{

	@Override
	public ErrorObject checkService() {
		log.info("BEGIN");
		try {
			ErrorObject errorObject = (ErrorObject) posTransactionServiceRest.checkService();
			log.info("END");
			return errorObject;
		} catch (Exception e) {
			log.error("Exception!", e);
			log.info("END - " + e.getMessage());
			return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
		}
	}

	@Override
	public List<Date> getAvailablePosTransactionDates() {
		log.info("BEGIN");
		try {
			List<Date> availablePosTransactionDates =  (List<Date>) posTransactionServiceRest.getAvailablePosTransactionDates();
			log.info("END ");
			return availablePosTransactionDates;
		} catch (Exception e) {
			log.error("Exception!", e);
			log.info("END - " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<String> getPosTransactionJournal(PosTransactionSummary posTransactionSummary) {
		log.info("BEGIN");
		try {
			List<String> posTransactionJournal = (List<String>) posTransactionServiceRest.getPosTransactionJournal(gson.toJson(posTransactionSummary));
			log.info("END ");
			return posTransactionJournal;
		} catch (Exception e) {
			log.error("Exception!", e);
			log.info("END - " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<PosTransactionSummary> searchTransactions(PosTransactionSearchCriteria posTransactionSearchCriteria) {
		log.info("BEGIN");
		try {
			List<PosTransactionSummary> transactions = (List<PosTransactionSummary>) posTransactionServiceRest.searchTransactions(gson.toJson(posTransactionSearchCriteria));
			log.info("END ");
			return transactions;
		} catch (Exception e) {
			log.error("Exception!", e);
			log.info("END - " + e.getMessage());
			return null;
		}
	}

	private static final Logger log = WebFrontLogger.getLogger(PosTransactionServiceClientImpl.class);
			private PosTransactionServiceRestInterface posTransactionServiceRest = new PosTransactionServiceRestClientImpl();
	private Gson gson = new Gson();
}
