package com.ncr.webfront.base.restclient.postransactionservice;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.postransaction.PosTransactionServiceRestInterface;
import com.ncr.webfront.base.postransaction.PosTransactionSummary;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class PosTransactionServiceRestClientImpl implements PosTransactionServiceRestInterface {

	public PosTransactionServiceRestClientImpl() {
		restConnector = new RestConnector(serverBaseUrl);
	}

	@Override
	public Object checkService() {
		log.info("BEGIN");
		Object object = restConnector.get("checkService", ErrorObject.class);
		log.info("END");
		return object;
	}

	@Override
	public Object getAvailablePosTransactionDates() {
		log.info("BEGIN");
		Type listType = new TypeToken<ArrayList<Date>>() {
		}.getType();
		Object object = restConnector.get("getAvailablePosTransactionDates", listType);
		log.info("END");
		return object;
	}

	@Override
	public Object getPosTransactionJournal(String posTransactionSummaryJsonString) {
		log.info("BEGIN");
		Type listType = new TypeToken<List<String>>() {
		}.getType();
		Object object = restConnector.put("getPosTransactionJournal", posTransactionSummaryJsonString, listType);
		log.info("END");
		return object;
	}

	@Override
	public Object searchTransactions(String posTransactionSearchCriteriaJsonString) {
		log.info("BEGIN");
		Type listType = new TypeToken<List<PosTransactionSummary>>() {
		}.getType();
		Object object = restConnector.put("searchTransactions", posTransactionSearchCriteriaJsonString, listType);
		log.info("END");
		return object;
	}

	private static final Logger log = WebFrontLogger.getLogger(PosTransactionServiceRestClientImpl.class);
	private RestConnector restConnector = null;
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/posTransactionService/";
}
