package com.ncr.webfront.base.restclient.posoperatorservice;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceRestInterface;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.core.utils.validation.PosOperatorValidationData;

public class PosOperatorServiceRestClientImpl implements
		PosOperatorServiceRestInterface {

	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	public PosOperatorServiceRestClientImpl()
	{
		restConnector = new RestConnector(serverBaseUrl);
	}

	@Override
	public Object getAll() {
		Type listType = new TypeToken<ArrayList<PosOperator>>() {
		}.getType();
		return restConnector.get("getAll", listType);
	}

	@Override
	public PosOperator getByCode(String operatorCode) {
		return restConnector.get("getByCode/"+operatorCode, PosOperator.class);
	}
	
	@Override
	public List<PosOperator> searchByCode(String operatorCode) {
		List<PosOperator> listOfPosOperators;
		Type listType = new TypeToken<ArrayList<PosOperator>>() {
		}.getType();
		listOfPosOperators = restConnector.get("searchByCode/" + operatorCode.trim(), listType);
		return listOfPosOperators;

	}

	@Override
	public List<PosOperator> searchByName(String name) {
		List<PosOperator> listOfPosOperators;
		Type listType = new TypeToken<ArrayList<PosOperator>>() {
		}.getType();
		if ((name != null) && ("" != name))
			// "#" have a special meaning to URLs; so, everything appended to it is discarded from the receiving rest service.
			listOfPosOperators = restConnector.get("searchByName/" + name.trim().replace("#", ""), listType);
		else
			listOfPosOperators = restConnector.get("searchByName", listType);

		return listOfPosOperators;
	}
	
	@Override
	public PosOperator createDefaultCashier() {
		return restConnector.get("createDefaultCashier", PosOperator.class);
	}

	@Override
	public PosOperator createCashierFromRole(String role) {
		return restConnector.get("createCashierFromRole/"+role.trim(), PosOperator.class);
	}

	@Override
	public ErrorObject addPosOperator(String posOperatorJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("addPosOperator", posOperatorJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public ErrorObject deletePosOperator(String posOperatorCodeToDelete) {
		ErrorObject eo = null;
		eo = restConnector.get("deletePosOperator/" + posOperatorCodeToDelete.trim(), ErrorObject.class);

		if (eo != null) {
			return eo;
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public ErrorObject updatePosOperator(String posOperatorJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("updatePosOperator", posOperatorJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}
	
	@Override
	public Object getPosOperatorValidationData() {
		Type listType = new TypeToken<List<PosOperatorValidationData>>() {
		}.getType();
		return restConnector.get("getPosOperatorValidationData", listType);
	}	
	
	@Override
	public Object getPosOperatorRoles() {
		Type mapType = new TypeToken<Map<Integer, String>>() {
		}.getType();
		return restConnector.get("getPosOperatorRoles", mapType);
	}
	
	@Override
	public Object getPosOperatorTraineeRole() {
		return restConnector.get("getPosOperatorTraineeRole", String.class);
	}
	
	private RestConnector restConnector = null;
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/posOperatorMaintenance/";
	private static final Gson gson = new Gson();
	
	

}
