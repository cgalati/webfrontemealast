package com.ncr.webfront.base.restclient.posoperatorservice;

import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.base.posoperator.PosOperatorServiceRestInterface;
import com.ncr.webfront.core.utils.validation.PosOperatorValidationData;

@SuppressWarnings("unchecked")
public class PosOperatorServiceClientImpl implements
		PosOperatorServiceInterface {

	@Override
	public ErrorObject checkService() {
		return (ErrorObject) posOperatorServiceRest.checkService();
	}
	
	@Override
	public List<PosOperator> getAll() {
		return (List<PosOperator>) posOperatorServiceRest.getAll();
	}

	@Override
	public PosOperator getByCode(String operatorCode) {
		return (PosOperator) posOperatorServiceRest.getByCode(operatorCode);
	}
	
	@Override
	public List<PosOperator> searchByCode(String operatorCode) {
		return (List<PosOperator>) posOperatorServiceRest.searchByCode(operatorCode);
	}

	@Override
	public List<PosOperator> searchByName(String name) {
		return (List<PosOperator>) posOperatorServiceRest.searchByName(name);
	}
	
	@Override
	public PosOperator createDefaultCashier() {
		return (PosOperator) posOperatorServiceRest.createDefaultCashier();
	}

	@Override
	public PosOperator createCashierFromRole(String role) {
		return (PosOperator) posOperatorServiceRest.createCashierFromRole(role);
	}

	@Override
	public ErrorObject addPosOperator(PosOperator posOperatorToAdd) {
		Gson gson = new Gson();
		String jsonPosOperator = "";
		jsonPosOperator = gson.toJson(posOperatorToAdd);
		gson = null;
		return (ErrorObject)posOperatorServiceRest.addPosOperator(jsonPosOperator);

	}

	@Override
	public ErrorObject deletePosOperator(String posOperatorCodeToDelete) {
		return (ErrorObject)posOperatorServiceRest.deletePosOperator(posOperatorCodeToDelete);

	}

	@Override
	public ErrorObject updatePosOperator(PosOperator posOperatorToUpdate) {
		Gson gson = new Gson();
		String jsonPosOperator = "";
		jsonPosOperator = gson.toJson(posOperatorToUpdate);
		gson = null;
		return (ErrorObject)posOperatorServiceRest.updatePosOperator(jsonPosOperator);
	}
	
	@Override
	public List<PosOperatorValidationData> getPosOperatorValidationData() {
		return (List<PosOperatorValidationData>) posOperatorServiceRest.getPosOperatorValidationData();
	}
	
	@Override
	public Map<Integer, String> getPosOperatorRoles() {
		return (Map<Integer, String>) posOperatorServiceRest.getPosOperatorRoles();
	}
	
	@Override
	public String getPosOperatorTraineeRole() {
		return (String) posOperatorServiceRest.getPosOperatorTraineeRole();
	}
	
	private PosOperatorServiceRestInterface posOperatorServiceRest = new PosOperatorServiceRestClientImpl();

	



}
