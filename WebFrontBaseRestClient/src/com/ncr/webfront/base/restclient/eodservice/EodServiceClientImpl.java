package com.ncr.webfront.base.restclient.eodservice;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.EodServiceInterface;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.EodServiceRestInterface;
import com.ncr.webfront.base.eod.PosStatusObject;
@SuppressWarnings("unchecked")
public class EodServiceClientImpl implements EodServiceInterface {

	@Override
	public ErrorObject checkService() {
		return (ErrorObject) eodServiceRest.checkService();
	}
	
    @Override
	public List<PosStatusObject> getAllPosStatus() {
		return (List<PosStatusObject>) eodServiceRest.getAllPosStatus();
	}

	@Override
	public PosStatusObject getPosStatus(String posCode) {
		return (PosStatusObject) eodServiceRest.getPosStatus(posCode);
	}

	@Override
	public EodStatusObject getEodStatus() {
		return (EodStatusObject) eodServiceRest.getEodStatus();
	}

	@Override
	public List<EodStatusObject> getEodStatusHistory() {
		return (List<EodStatusObject>) eodServiceRest.getEodStatusHistory();
	}

	@Override
	public ErrorObject launchEodAllPos() {
		return (ErrorObject) eodServiceRest.launchEodAllPos();
	}

	@Override
	public ErrorObject launchEodServer() {
		return (ErrorObject) eodServiceRest.launchEodServer();
	}
	
	@Override
	public void cancel() {
		eodServiceRest.cancel();
	}
	
	@Override
    public ErrorObject declareDefective(String posCode) {
		return (ErrorObject) eodServiceRest.declareDefective(posCode);
    }
	
	private static final EodServiceRestInterface eodServiceRest = new EodServiceRestClient();

}
