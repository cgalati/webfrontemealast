package com.ncr.webfront.base.restclient.eodservice;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.EodServiceRestInterface;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.core.utils.common.RestConnector;

public class EodServiceRestClient implements EodServiceRestInterface {
	
	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public List<PosStatusObject> getAllPosStatus() {
		Type lstType = new TypeToken<List<PosStatusObject>>() {}.getType();
		
		return restConnector.get("getAllPosStatus", lstType);
	}

	@Override
	public PosStatusObject getPosStatus(String posCode) {
		return restConnector.get("getPosStatus/"+posCode, PosStatusObject.class);
	}

	@Override
	public EodStatusObject getEodStatus() {
		return restConnector.get("getEodStatus", EodStatusObject.class);
	}

	@Override
	public List<EodStatusObject> getEodStatusHistory() {
		Type lstType = new TypeToken<List<EodStatusObject>>() {}.getType();
		
		return restConnector.get("getEodStatusHistory", lstType);
	}

	@Override
	public Object launchEodAllPos() {
		return restConnector.get("launchEodAllPos", ErrorObject.class);
	}

	@Override
	public Object launchEodServer() {
		return restConnector.get("launchEodServer", ErrorObject.class);
	}
	
	@Override
	public Object cancel() {
		return restConnector.get("cancel", ErrorObject.class);
	}

	@Override
    public Object declareDefective(String posCode) {
		return restConnector.get("declareDefective/"+posCode, ErrorObject.class);
    }
	
	RestConnector restConnector = new RestConnector(serverBaseUrl);
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/eodService/";
}
