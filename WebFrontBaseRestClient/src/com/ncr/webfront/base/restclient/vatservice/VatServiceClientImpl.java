package com.ncr.webfront.base.restclient.vatservice;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.base.vat.VatServiceRestInterface;

@SuppressWarnings("unchecked")
public class VatServiceClientImpl implements VatServiceInterface {
	
	@Override
	public ErrorObject checkService() {
		return (ErrorObject) vatServiceRest.checkService();
	}
	
	@Override
	public List<Vat> getAll() {
		return (List<Vat>) vatServiceRest.getAll();
	}

	@Override
	public Vat getVatById(int vatId) {
		return (Vat) vatServiceRest.getVatById(vatId);
	}
	
	@Override
	public Vat getDefault() {
		return (Vat) vatServiceRest.getDefault();
	}
	
	private VatServiceRestInterface vatServiceRest = new VatServiceRestClient();
}
