package com.ncr.webfront.base.restclient.vatservice;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceRestInterface;

public class VatServiceRestClient implements VatServiceRestInterface {
	public VatServiceRestClient() {
		restConnector = new RestConnector(serverBaseUrl);
	}
	
	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public Object getAll() {
		Type listType = new TypeToken<List<Vat>>() {}.getType();
		
		return restConnector.get("getAll", listType);
	}

	@Override
	public Object getVatById(int vatId) {
		return restConnector.get("getVatById/" + vatId, Vat.class);
	}
	
	@Override
	public Object getDefault() {
		return restConnector.get("getDefault", Vat.class);
	}
	
	private RestConnector restConnector = null;
	private String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/vatService/";
}
