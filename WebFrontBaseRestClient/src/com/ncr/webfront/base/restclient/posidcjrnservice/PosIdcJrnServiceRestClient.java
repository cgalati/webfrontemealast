package com.ncr.webfront.base.restclient.posidcjrnservice;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;
import com.ncr.webfront.base.posidcjrn.*;

import com.ncr.webfront.base.posidcjrn.entities.DataCollect;
import com.ncr.webfront.base.posidcjrn.entities.DataHeader;
import com.ncr.webfront.base.posidcjrn.entities.Journal;

public class PosIdcJrnServiceRestClient implements PosIdcJrnServiceRestInterface {

	public PosIdcJrnServiceRestClient() {
		restConnector = new RestConnector(serverBaseUrl);		
	}
		
	@Override
	public ErrorObject checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public List<DataHeader> getAll() {
		List<DataHeader> listOfTrans;
		Type listType = new TypeToken<ArrayList<DataHeader>>() {
		}.getType();
		listOfTrans = restConnector.get("getAll", listType);
		return listOfTrans;
	}
	
	@Override
	public ErrorObject addIdcJrn(String idcjrnJsonString) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("addIdcJrn", idcjrnJsonString);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}
	
	@Override
	public List<DataHeader> getTransactionsByParams(String terminal, String dateFrom, String dateTo, String store, String transaction, String customer) {
		List<DataHeader> listOfTrans;
		Type listType = new TypeToken<ArrayList<DataHeader>>() {
		}.getType();
		
		String urlString =  "getTransaction/" + terminal + "/" + dateFrom + "/" + dateTo + "/" + store + "/" + transaction + "/" + customer;
		
		listOfTrans = restConnector.get(urlString, listType);
		return listOfTrans;
	}

	@Override
	public ErrorObject deleteIdcJrn(int id, String table) {
		ErrorObject eo = null;
		eo = restConnector.get("deleteIdcJrn/" + id + "/" + table, ErrorObject.class);

		if (eo != null) {
			return eo;
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}
	
	@Override
	public ErrorObject deleteAll(int idHeader) {
		ErrorObject eo = null;
		eo = restConnector.get("deleteAll/" + idHeader, ErrorObject.class);

		if (eo != null) {
			return eo;
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public List<DataCollect> getLstIdc(int idHeader) {
		List<DataCollect> listOfIdc;
		Type listType = new TypeToken<ArrayList<DataCollect>>() {
		}.getType();
		listOfIdc = restConnector.get("getLstIdc/" + idHeader, listType);
		return listOfIdc;
	}

	@Override
	public List<Journal> getLstJrn(int idHeader) {
		List<Journal> listOfJrn;
		Type listType = new TypeToken<ArrayList<Journal>>() {
		}.getType();
		listOfJrn = restConnector.get("getLstJrn/" + idHeader, listType);
		return listOfJrn;
	
	}
	
	@Override
	public ErrorObject addIdc(String jsonDataCollect) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("addIdc", jsonDataCollect);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	@Override
	public ErrorObject addJrn(String jsonJournal) {
		String returnJsonString = null;
		returnJsonString = restConnector.put("addJrn", jsonJournal);

		if (returnJsonString != null) {
			try {
				return gson.fromJson(returnJsonString, ErrorObject.class);
			} catch (Exception e) {
				e.printStackTrace();
				return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			}
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}
		
	@Override
	public ErrorObject updateFileName(String fileName, int idHeader) {
		ErrorObject eo = null;
		eo = restConnector.get("updateFileName/" + fileName + "/" + idHeader, ErrorObject.class);

		if (eo != null) {
			return eo;
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}

	private RestConnector restConnector = null;
	private static final String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/posIdcJrnService/";
	private static final Gson gson = new Gson();
	
}
