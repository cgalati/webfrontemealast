package com.ncr.webfront.base.restclient.posidcjrnservice;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.article.ArticleServiceRestInterface;
import com.ncr.webfront.base.posidcjrn.PosIdcJrnServiceInterface;
import com.ncr.webfront.base.posidcjrn.PosIdcJrnServiceRestInterface;
import com.ncr.webfront.base.posidcjrn.entities.DataCollect;
import com.ncr.webfront.base.posidcjrn.entities.DataHeader;
import com.ncr.webfront.base.posidcjrn.entities.Journal;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

@SuppressWarnings("unchecked")
public class PosIdcJrnServiceClientImpl implements PosIdcJrnServiceInterface {
	
	@Override
	public ErrorObject checkService() {
		return (ErrorObject) posIdcJrnServiceRest.checkService();
	}
	
	@Override
	public List<DataHeader> getAll() {
		return posIdcJrnServiceRest.getAll();
	}
	
	@Override
	public ErrorObject addIdcJrn(String dataHeader) {
		return (ErrorObject) posIdcJrnServiceRest.addIdcJrn(dataHeader);
	}
	
	@Override
	public List<DataHeader> getTransactionsByParams(String terminal, String dateFrom, String dateTo, String store, String transaction, String customer) {
		return posIdcJrnServiceRest.getTransactionsByParams(terminal, dateFrom, dateTo, store, transaction, customer);
	}
	



	@Override
	public ErrorObject deleteIdcJrn(int id, String table) {
		return (ErrorObject) posIdcJrnServiceRest.deleteIdcJrn(id, table);
	}
	
	@Override
	public ErrorObject deleteAll(int deleteAll) {
		return (ErrorObject) posIdcJrnServiceRest.deleteAll(deleteAll);
	}

	@Override
	public List<DataCollect> getLstIdc(int idHeader) {
		return posIdcJrnServiceRest.getLstIdc(idHeader);
	}

	@Override
	public List<Journal> getLstJrn(int idHeader) {
		return posIdcJrnServiceRest.getLstJrn(idHeader);	
	}
	
	@Override
	public ErrorObject addIdc(String dataCollect) {
		return (ErrorObject) posIdcJrnServiceRest.addIdc(dataCollect);	
	}

	@Override
	public ErrorObject addJrn(String journal) {
		return (ErrorObject) posIdcJrnServiceRest.addJrn(journal);	
	}
		
	@Override
	public ErrorObject updateFileName(String fileName, int idHeader) {
		return (ErrorObject) posIdcJrnServiceRest.updateFileName(fileName, idHeader);	
	}
	
	public List<Journal> getListJournal(int idHdr) {
		return getLstJrn(idHdr);
	}
	
	public List<DataCollect> getListDc(int idHdr) {
		return getLstIdc(idHdr);
	}
		
	public List<DataHeader> getTransByParams(String terminal, String dateFrom, String dateTo, String store, String transaction, String customer) {
		return getTransactionsByParams(terminal, dateFrom, dateTo, store, transaction, customer);
	}
	
	public ErrorObject cancelIdcJrn(int id, String table) {
		return deleteIdcJrn(id, table);
	}
	
	public ErrorObject cancelAll(int header) {
		return deleteAll(header);
	}
	
	public ErrorObject changeFileName(String fileName, int idHeader) {
		return updateFileName(fileName, idHeader);
	}
	
	public ErrorObject addJournal(String journal) {
		return addJrn(journal);
	}
	
	public ErrorObject addDc(String dataCollect) {
		return addIdc(dataCollect);
	}
	

	private static final PosIdcJrnServiceRestInterface posIdcJrnServiceRest = new PosIdcJrnServiceRestClient();

}
