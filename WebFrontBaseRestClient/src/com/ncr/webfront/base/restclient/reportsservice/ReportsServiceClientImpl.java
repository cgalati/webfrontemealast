package com.ncr.webfront.base.restclient.reportsservice;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.common.RestUtils;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.reports.ReportsServiceInterface;
import com.ncr.webfront.base.reports.ReportsServiceRestInterface;

@SuppressWarnings("unchecked")
public class ReportsServiceClientImpl implements ReportsServiceInterface {

	@Override
	public ErrorObject checkService() {
		return (ErrorObject) reportsServiceRest.checkService();
	}
	
	
	@Override
	public List<String> getFormats() {
		return (List<String>) reportsServiceRest.getFormats();
	}
	
	@Override
	public byte[] generatePosOperatorReport(ReportOptions reportOptions) {
		String reportOptionsJsonString = gson.toJson(reportOptions);
		
		return (byte[]) reportsServiceRest.generatePosOperatorReport(reportOptionsJsonString);
	}

	@Override
	public byte[] generateFiancialReport(ReportOptions reportOptions) {
		String reportOptionsJsonString = gson.toJson(reportOptions);
		
		return (byte[]) reportsServiceRest.generateFiancialReport(reportOptionsJsonString);
	}

	@Override
	public byte[] generateHourlyActivityReport(ReportOptions reportOptions) {
		String reportOptionsJsonString = gson.toJson(reportOptions);
		
		return (byte[]) reportsServiceRest.generateHourlyActivityReport(reportOptionsJsonString);
	}

	@Override
	public byte[] generateDepartmentReport(ReportOptions reportOptions) {
		String reportOptionsJsonString = gson.toJson(reportOptions);
		
		return (byte[]) reportsServiceRest.generateDepartmentReport(reportOptionsJsonString);
	}
	
	@Override
	public byte[] generatePosOperatorCashReport(Map<PosOperator, String> posOperatorsTerminalCodeMap, String reportFormat) {
		Type posOperatorTerminalCodeMapType = new TypeToken<Map<PosOperator, String>>(){}.getType();
		String posOperatorsTerminalCodeMapJsonString = gson.toJson(posOperatorsTerminalCodeMap, posOperatorTerminalCodeMapType);
		
		String reportFormatJsonString = gson.toJson(reportFormat);
		
		String posOperatorCashReporJsonString = RestUtils.generateCustomJsonString(posOperatorsTerminalCodeMapJsonString, reportFormatJsonString);
		
		return (byte[]) reportsServiceRest.generatePosOperatorCashReport(posOperatorCashReporJsonString);
	}
	
	@Override
	public ErrorObject generateEodReports(List<String> registerList) {
//		Type posTerminalListType = new TypeToken<List<PosTerminal>>(){}.getType();
		String registerListJsonString = gson.toJson(registerList);
		
		return (ErrorObject) reportsServiceRest.generateEodReports(registerListJsonString);	
	}

	private Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	private ReportsServiceRestInterface reportsServiceRest = new ReportsServiceRestClient();

}
