package com.ncr.webfront.base.restclient.reportsservice;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.reports.ReportsServiceRestInterface;
import com.ncr.webfront.core.utils.common.RestConnector;

//TODO Error handling + logging !!
public class ReportsServiceRestClient implements ReportsServiceRestInterface {
	public ReportsServiceRestClient() {
		restConnector = new RestConnector(serverBaseUrl);
	}
	
	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public Object getFormats() {
		Type listType = new TypeToken<List<String>>() {}.getType();
		
		return restConnector.get("getFormats", listType);
	}
	
	@Override
	public Object generatePosOperatorReport(String reportOptionsJsonString) {
		return put("generatePosOperatorReport", reportOptionsJsonString);
	}

	@Override
	public Object generateFiancialReport(String reportOptionsJsonString) {
		return put("generateFiancialReport", reportOptionsJsonString);
	}

	@Override
	public Object generateHourlyActivityReport(String reportOptionsJsonString) {
		return put("generateHourlyActivityReport", reportOptionsJsonString);
	}

	@Override
	public Object generateDepartmentReport(String reportOptionsJsonString) {
		return put("generateDepartmentReport", reportOptionsJsonString);
	}
	
	private Object put(String service, String reportOptionsJsonString) {
		String returnJsonString = restConnector.put(service, reportOptionsJsonString);
		
		if (returnJsonString != null) {
			Type arrayType = new TypeToken<byte[]>() {}.getType();
			
			return gson.fromJson(returnJsonString, arrayType);
		} else {
			return new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "NO_RESPONSE");
		}
	}
	
	@Override
	public Object generatePosOperatorCashReport(String reportOptionsJsonString) {
		return put("generatePosOperatorCashReport", reportOptionsJsonString);
	}
	
	@Override
	public Object generateEodReports(String registerListJsonString) {
		return put("generateEodReports", registerListJsonString);
	}
	
	private Gson gson = new Gson();
	private RestConnector restConnector = null;
	private String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/reportsService/";


}
