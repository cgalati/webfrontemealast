package com.ncr.webfront.base.restclient.tareservice;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.tare.TareServiceRestInterface;



public class TareServiceRestClient implements TareServiceRestInterface{
	public TareServiceRestClient() {
		restConnector = new RestConnector(serverBaseUrl);
	}
	
	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public Object getAll() {
		Type listType = new TypeToken<List<Tare>>() {}.getType();		
		return restConnector.get("getAll", listType);
	}

	@Override
	public Object getTareById(int tareId) {
		return restConnector.get("getTareById/" + tareId, Tare.class);
	}
	
	private RestConnector restConnector = null;
	private String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/tareService/";

}
