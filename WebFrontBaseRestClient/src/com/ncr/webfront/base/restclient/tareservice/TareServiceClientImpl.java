package com.ncr.webfront.base.restclient.tareservice;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.tare.TareServiceInterface;
import com.ncr.webfront.base.tare.TareServiceRestInterface;


@SuppressWarnings("unchecked")
public class TareServiceClientImpl implements TareServiceInterface {

	@Override
	public ErrorObject checkService() {
		return (ErrorObject) tareServiceRest.checkService();
	}
	
	@Override
	public List<Tare> getAll() {
		return (List<Tare>) tareServiceRest.getAll();
	}

	@Override
	public Tare getTareById(int tareId) {
		return (Tare) tareServiceRest.getTareById(tareId);
	}
	
	private TareServiceRestInterface tareServiceRest = new TareServiceRestClient();

}
