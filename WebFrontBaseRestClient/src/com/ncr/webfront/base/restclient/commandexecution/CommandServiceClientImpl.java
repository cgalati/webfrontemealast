package com.ncr.webfront.base.restclient.commandexecution;

import java.util.List;

import com.google.gson.Gson;
import com.ncr.webfront.base.commandexecution.CommandServiceInterface;
import com.ncr.webfront.base.commandexecution.CommandServiceRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;

@SuppressWarnings("unchecked")
public class CommandServiceClientImpl implements CommandServiceInterface {
	
	@Override
	public ErrorObject checkService() {
		return (ErrorObject) commandServiceRest.checkService();
	}
	
	
	@Override
	public List<RunningInfo> getAll() {
		return (List<RunningInfo>) commandServiceRest.getAll();
	}

	@Override
	public List<RunningInfo> getByName(String fileName) {
		return (List<RunningInfo>) commandServiceRest.getByName(fileName);
	}

	@Override
	public void execute(String commandName) {
		commandServiceRest.execute(commandName);
	}
	
	@Override
	public void cancel(String commandName) {
		commandServiceRest.cancel(commandName);
	}

	@Override
	public RunningInfo update(String commandName) {
		return (RunningInfo) commandServiceRest.update(commandName);
	}
	
	@Override
	public void makeCommandAvailabileForExecution(Command command) {
		commandServiceRest.makeCommandAvailabileForExecution(gson.toJson(command));
	}

	private CommandServiceRestInterface commandServiceRest = new CommandServiceRestClient();
	private Gson gson = new Gson();
}
