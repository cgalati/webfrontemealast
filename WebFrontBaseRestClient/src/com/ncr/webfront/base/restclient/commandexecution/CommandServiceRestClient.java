package com.ncr.webfront.base.restclient.commandexecution;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.commandexecution.CommandServiceRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.common.RestConnector;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;

public class CommandServiceRestClient implements CommandServiceRestInterface {

	@Override
   public Object checkService() {
		return restConnector.get("checkService", ErrorObject.class);
	}
	
	@Override
	public Object getAll() {
		Type listType = new TypeToken<List<RunningInfo>>() {}.getType();
		
		return restConnector.get("getAll", listType);
	}

	@Override
	public Object getByName(String commandName) {
		Type listType = new TypeToken<List<RunningInfo>>() {}.getType();
		
		return restConnector.get("getByName/" + commandName, listType);
	}
	
	@Override
	public Object execute(String commandName) {
		ErrorObject result = (ErrorObject) restConnector.get("execute/" + commandName, ErrorObject.class);
		
		if (result.getErrorCode() != ErrorCode.OK_NO_ERROR) {
			throw new RuntimeException(result.getDescription());
		}
		
		return null;
	}
	
	@Override
	public Object cancel(String commandName) {
		ErrorObject result = (ErrorObject) restConnector.get("cancel/" + commandName, ErrorObject.class);
		
		if (result.getErrorCode() != ErrorCode.OK_NO_ERROR) {
			throw new RuntimeException(result.getDescription());
		}
		
		return null;
	}
	
	@Override
	public Object update(String commandName) {
		ErrorObject result = (ErrorObject) restConnector.get("update/" + commandName, ErrorObject.class);
		
		if (result.getErrorCode() == ErrorCode.OK_NO_ERROR) {
			return gson.fromJson(result.getDescription(), RunningInfo.class);
		} else {
			throw new RuntimeException(result.getDescription());
		}
	}
	
	@Override
	public Object makeCommandAvailabileForExecution(String commandJsonString) {
		String resultJsonString = restConnector.put("makeCommandAvailabileForExecution", commandJsonString);
		ErrorObject result = gson.fromJson(resultJsonString, ErrorObject.class);
		
		if (result.getErrorCode() != ErrorCode.OK_NO_ERROR) {
			throw new RuntimeException(result.getDescription());
		}
		
		return null;
	}
	
	private String serverBaseUrl = RestConnector.getRestBaseUrl() + "/WebFrontBaseRestServer/services/commandService/";
	private RestConnector restConnector = new RestConnector(serverBaseUrl);
	private Gson gson = new Gson();
}
