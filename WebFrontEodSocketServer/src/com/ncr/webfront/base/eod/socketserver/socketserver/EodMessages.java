package com.ncr.webfront.base.eod.socketserver.socketserver;

import java.io.FileInputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.socketserver.Globals;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EodMessages {

	public static EodMessages getInstance() {
		if (eodMessages == null) {
			eodMessages = new EodMessages(Globals.PROPERTIES_FILE_SUBDIR, Globals.EOD_MESSAGES_FILE_NAME);
		}
		return eodMessages;
	}

	public EodMessages(String propertiesFileSubdir, String eodMessagesFileName) {
		logger.debug("BEGIN");
		ResourceBundle propertyFileResourceBundle = null;
		try {
			logger.debug("Begin");

			propertyFileResourceBundle = new PropertyResourceBundle(new FileInputStream(propertiesFileSubdir + eodMessagesFileName));

		} catch (Exception e) {
			logger.error("Exception!", e);
			propertyFileResourceBundle = null;
		}
		logger.debug("END");
		messagesEodStart = readMessages(propertyFileResourceBundle, "messages.eod.start");
		messagesEodEndSuccessfully = readMessages(propertyFileResourceBundle, "messages.eod.end.successfully");
		messagesEodEndWithNonCriticalErrors = readMessages(propertyFileResourceBundle, "messages.eod.end.with.noncritical.errors");
		messagesEodEndWithCriticalErrors = readMessages(propertyFileResourceBundle, "messages.eod.end.with.critical.errors");
		messagesEodContinue = readMessages(propertyFileResourceBundle, "messages.eod.continue");
		messagesStepBegin = readMessages(propertyFileResourceBundle, "messages.eod.step.begin");
		messagesStepEndSuccessfully = readMessages(propertyFileResourceBundle, "messages.eod.step.end.successfully");
		messagesStepEndWithNonCriticalError = readMessages(propertyFileResourceBundle, "messages.eod.step.end.with.noncritical.errors");
		messagesStepEndWithCriticalError = readMessages(propertyFileResourceBundle, "messages.eod.step.end.with.critical.errors");
		messagesStepCancelled = readMessages(propertyFileResourceBundle, "messages.eod.step.cancelled");

	}

	private List<String> readMessages(ResourceBundle resourceBundle, String propertyBaseName) {
		List<String> listResult = new ArrayList<String>();
		if (resourceBundle == null) {
			listResult.add(propertyBaseName);
			return listResult;
		}
		int index = 1;
		do {
			String stringResult = "";
			try {
				stringResult = resourceBundle.getString(propertyBaseName + "." + index);
				logger.debug(propertyBaseName + "." + index + "=" + stringResult);
			} catch (Exception e) {
				break;
			}
			listResult.add(stringResult);
			index++;
		} while (true);
		if (listResult.size() == 0) {
			listResult.add(propertyBaseName);
		}
		return listResult;
	}

	public String[] getMessagesEodStart(Date startingDate) {
		String[] arrayMessages = new String[messagesEodStart.size()];
		int index = 0;
		for (String message : messagesEodStart) {
			message = String.format("%s - " + message, getDateAndTimeFormatted(startingDate));
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesEodEndSuccessfully() {
		String[] arrayMessages = new String[messagesEodEndSuccessfully.size()];
		int index = 0;
		for (String message : messagesEodEndSuccessfully) {
			message = String.format("%s - " + message, getDateAndTimeFormatted());
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesEodEndWithNonCriticalErrors(int numberOfNonCriticalErrors) {
		String[] arrayMessages = new String[messagesEodEndWithNonCriticalErrors.size()];
		int index = 0;
		for (String message : messagesEodEndWithNonCriticalErrors) {
			message = String.format(WARNING_PREFIX + "%s - " + message, getDateAndTimeFormatted(), numberOfNonCriticalErrors);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesEodEndWithCriticalErrors() {
		String[] arrayMessages = new String[messagesEodEndWithCriticalErrors.size()];
		int index = 0;
		for (String message : messagesEodEndWithCriticalErrors) {
			message = String.format(ERROR_PREFIX + "%s - " + message, getDateAndTimeFormatted());
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesEodContinue(int currentStepIndex) {
		String[] arrayMessages = new String[messagesEodContinue.size()];
		int index = 0;
		for (String message : messagesEodContinue) {
			message = String.format("%s - " + message, getDateAndTimeFormatted(), currentStepIndex);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String getMessagesStepBegin(int currentStepIndex, String stepDescription, Date date) {
		String[] arrayMessages = new String[messagesStepBegin.size()];
		int index = 0;
		if (date == null) {
			date = new Date();
			logger.debug("LOG20160531 - getMessagesStepBegin date parameter null!!! using new Date()");
		}
		for (String message : messagesStepBegin) {
			message = String.format("%s - " + message, getDateAndTimeFormatted(date), currentStepIndex, stepDescription);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		logger.debug("Returning only first one.");
		return arrayMessages[0];
	}

	public String getMessageStepEndSuccessfully(int currentStepIndex) {
		String message = "";
		if (messagesStepEndSuccessfully.size() > 0) {
			message = String.format("%s - " + messagesStepEndSuccessfully.get(0), getDateAndTimeFormatted(), currentStepIndex);
		}
		return message;
	}

	public String getMessageStepEndWithNonCriticalErrors(int currentStepIndex) {
		String message = "";
		if (messagesStepEndWithNonCriticalError.size() > 0) {
			message = String.format(WARNING_PREFIX + "%s - " + messagesStepEndWithNonCriticalError.get(0), getDateAndTimeFormatted(), currentStepIndex);
		}
		return message;
	}

	public String getMessageStepEndWithCriticalErrors(int currentStepIndex) {
		String message = "";
		if (messagesStepEndWithCriticalError.size() > 0) {
			message = String.format(ERROR_PREFIX + "%s - " + messagesStepEndWithCriticalError.get(0), getDateAndTimeFormatted(), currentStepIndex);
		}
		return message;
	}

	public String[] getMessagesStepEndSuccessfully(int currentStepIndex) {
		String[] arrayMessages = new String[messagesStepEndSuccessfully.size()];
		int index = 0;
		for (String message : messagesStepEndSuccessfully) {
			message = String.format("%s - " + message, getDateAndTimeFormatted(), currentStepIndex);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesStepEndWithNonCriticalErrors(int currentStepIndex) {
		String[] arrayMessages = new String[messagesStepEndWithNonCriticalError.size()];
		int index = 0;
		for (String message : messagesStepEndWithNonCriticalError) {
			message = String.format("%s - " + message, getDateAndTimeFormatted(), currentStepIndex);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesStepEndWithCriticalErrors(int currentStepIndex) {
		String[] arrayMessages = new String[messagesStepEndWithCriticalError.size()];
		int index = 0;
		for (String message : messagesStepEndWithCriticalError) {
			message = String.format(ERROR_PREFIX + "%s - " + message, getDateAndTimeFormatted(), currentStepIndex);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String[] getMessagesStepCancelled(int currentStepIndex) {
		String[] arrayMessages = new String[messagesStepCancelled.size()];
		int index = 0;
		for (String message : messagesStepCancelled) {
			message = String.format("%s - " + message, getDateAndTimeFormatted(), currentStepIndex);
			logger.debug(message);
			arrayMessages[index] = message;
			index++;
		}
		return arrayMessages;
	}

	public String getDateAndTimeFormatted() {
		return getDateAndTimeFormatted(new Date());
	}

	public String getDateAndTimeFormatted(Date date) {
		Format dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		Format timeFormatter = new SimpleDateFormat("HH.mm.ss");
		String dateString = dateFormatter.format(date);
		String timeString = timeFormatter.format(date);
		return dateString + " " + timeString;
	}

	private static final Logger logger = WebFrontLogger.getLogger(EodMessages.class);
	private static EodMessages eodMessages;

	private String WARNING_PREFIX = "@WARNING@";
	private String ERROR_PREFIX = "@ERROR@";

	private List<String> messagesEodStart;
	private List<String> messagesEodEndSuccessfully;
	private List<String> messagesEodEndWithNonCriticalErrors;
	private List<String> messagesEodEndWithCriticalErrors;
	private List<String> messagesEodContinue;
	private List<String> messagesStepBegin;
	private List<String> messagesStepEndSuccessfully;
	private List<String> messagesStepEndWithNonCriticalError;
	private List<String> messagesStepEndWithCriticalError;
	private List<String> messagesStepCancelled;

}
