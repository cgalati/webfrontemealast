package com.ncr.webfront.base.eod.socketserver.engine.steps;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.filesystem.FileHelper;
import com.ncr.webfront.core.utils.filesystem.ZipHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class ExecutionStepDirectoryFilesRetention extends AbstractExecutionStep {
	public ExecutionStepDirectoryFilesRetention(String sourceDir, String destDir, String destFileName,
			String numberOfRetentionFiles) {

		this(sourceDir, destDir, destFileName, numberOfRetentionFiles, "0");
	}

	public ExecutionStepDirectoryFilesRetention(String sourceDir, String destDir, String destFileName,
			String numberOfRetentionFiles, String numberOfFilesToKeep) {

		this.sourceDir = PathManager.getInstance().normalizePath(sourceDir, true);
		this.destDir = PathManager.getInstance().normalizePath(destDir, true);
		this.destFileName = destFileName;
		this.numberOfRetentionFiles = NumberUtils.toInt(numberOfRetentionFiles, 30);
		this.numberOfFilesToKeep = NumberUtils.toInt(numberOfFilesToKeep, 0);
	}

	@Override
	public void run() {
		log.debug("Begin (stepNumber = " + stepNumber + ")");
		log.info("Retention - sourceDir              = " + sourceDir);
		log.info("Retention - destDir                = " + destDir);
		log.info("Retention - destFileName           = " + destFileName);
		log.info("Retention - numberOfRetentionFiles = " + numberOfRetentionFiles);
		log.info("Retention - numberOfJsonFiles = " + numberOfFilesToKeep);

		doCreateCurrentRetentionFile();
		doMoveOldRetentionFiles();

		log.debug("End   (stepNumber = " + stepNumber + ") - returning " + info.isOk());
	}
	
	@Override
	public RunningInfo getRunningInfo() {
		return info;
	}

	private void doCreateCurrentRetentionFile() {

		File fileSourceDir = new File(sourceDir);
		if (!fileSourceDir.isDirectory()) {
			String message = "The source directory is not a directory (" + sourceDir + ")!";

			log.error(message);
			info.appendError(message);

			return;
		}

		// Check if there's at least one file...
		int filesCount = 0;
		for (int index = 0; index < fileSourceDir.listFiles().length; index++) {
			File fileSourceFile = fileSourceDir.listFiles()[index];
			if (fileSourceFile.isFile()) {
				filesCount++;
			}
		}
		if (filesCount == 0) {
			String message = "The source directory has no files in it (" + sourceDir + ")!";

			log.warn(message);
			info.appendError(message);

			return;
		}
		log.info("The source directory has " + filesCount + " files in it (" + sourceDir + ")!");

		File fileDestDir = new File(destDir);
		if (!fileDestDir.isDirectory()) {
			String message = "The destination directory is not a directory (" + destDir + ")!";

			log.error(message);
			info.appendError(message);

			return;
		}

		try {
			List<File> files = FileHelper.getFilesByDir(sourceDir);
			
			ZipHelper.zipDirectory(destDir, destFileName, files);
			deleteFiles(files);
		} catch (Exception e) {
			String message = "Errors during the compression of the directory (" + sourceDir + ")!";

			log.error(message);
			info.appendError(message);
		}
	}
	
	private void deleteFiles(List<File> files) {
		files = FileHelper.sortFilesByName(files, true);
		for (int i = 0; i < numberOfFilesToKeep; i++) {
			files.get(i).delete();
		}
	}

	private void doMoveOldRetentionFiles() {

		File firstFile = new File(destDir + destFileName);
		if (!firstFile.exists()) {
			String message = "Client file not present (" + destDir + destFileName
					+ "). Retention not needed for directory " + destDir + ".";

			log.info(message);
			info.appendError(message);

			return;
		}

		File lastFile = new File(destDir + destFileName + "." + numberOfRetentionFiles);
		if (lastFile.exists()) {
			lastFile.delete();
		}

		for (int index = numberOfRetentionFiles - 1; index > 0; index--) {
			File currFile = new File(destDir + destFileName + "." + index);
			File newFile = new File(destDir + destFileName + "." + (index + 1));

			if (currFile.exists()) {
				currFile.renameTo(newFile);
			}
		}

		File currFile = new File(destDir + destFileName);
		File newFile = new File(destDir + destFileName + "." + 1);
		if (currFile.exists()) {
			currFile.renameTo(newFile);
		}
	}

	private String sourceDir;
	private String destDir;
	private String destFileName;
	private int numberOfRetentionFiles;
	private int numberOfFilesToKeep;
	private RunningInfo info = new RunningInfo();

	private static final Logger log = WebFrontLogger.getLogger(ExecutionStepDirectoryFilesRetention.class);
}
