package com.ncr.webfront.base.eod.socketserver.engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.EodStatusObject.EodStatus;
import com.ncr.webfront.base.eod.socketserver.Globals;
import com.ncr.webfront.base.eod.socketserver.socketserver.EodMessages;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.filesystem.FileHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EndOfDayStatusReader {

	public static EndOfDayStatusReader getInstance() {
		if (endOfDayStatusReader == null) {
			endOfDayStatusReader = new EndOfDayStatusReader();
		}
		return endOfDayStatusReader;

	}

	private EndOfDayStatusReader() {

	}

	public EodStatusObject readLastEndOfDayStatus() {
		File eodStatusSubdir = new File(Globals.EOD_STATUS_FILES_SUBDIR);
		List<File> jsons = null;

		createDirIfNotExists(eodStatusSubdir);
		jsons = FileHelper.getFilesByDirAndName(eodStatusSubdir.getAbsolutePath(), Globals.EOD_STATUS_FILE_NAME + ".+json");

		log.debug(String.format("jsons size [%d]", jsons.size()));

		if (!jsons.isEmpty()) {
			jsons = FileHelper.sortFilesByName(jsons, true);

			log.debug("sorted jsons");

			return gson.fromJson(read(jsons.get(0)), EodStatusObject.class);
		} else {
			log.debug("No previous eods");

			return new EodStatusObject(null, null, EodStatus.EOD_NOT_STARTED);
		}
	}

	public List<EodStatusObject> readEndOfDayHistory() {
		File eodStatusSubdir = new File(Globals.EOD_STATUS_FILES_SUBDIR);
		List<File> jsons = null;

		createDirIfNotExists(eodStatusSubdir);
		jsons = FileHelper.getFilesByDirAndName(eodStatusSubdir.getAbsolutePath(), Globals.EOD_STATUS_FILE_NAME + ".+json");

		log.debug(String.format("jsons size [%d]", jsons.size()));

		List<EodStatusObject> endOfDayHistory = new ArrayList<EodStatusObject>();

		if (!jsons.isEmpty()) {
			jsons = FileHelper.sortFilesByName(jsons, true);

			log.debug("sorted jsons");

			for (File json : jsons) {
				EodStatusObject eodStatus = gson.fromJson(read(json), EodStatusObject.class);
				endOfDayHistory.add(eodStatus);
			}
		} else {
			log.debug("No previous eods");

			endOfDayHistory.add(new EodStatusObject(null, null, EodStatus.EOD_NOT_STARTED));
		}

		return endOfDayHistory;
	}

	public void performEndOfDayHistoryRetention(int numberOfDays) {
		log.debug("BEGIN (numberOfDays = " + numberOfDays + ")");

		if (numberOfDays == 0) {
			log.warn("END (Since numberOfDays = " + numberOfDays + ", there's nothing to do)");
			return;
		}

		File eodStatusSubdir = new File(Globals.EOD_STATUS_FILES_SUBDIR);
		int eodStatusFilesCount = FileHelper.countFilesByDirAndName(eodStatusSubdir.getAbsolutePath(), Globals.EOD_STATUS_FILE_NAME + ".+json");
		if (eodStatusFilesCount <= 1) {
			log.warn("END (Since eodStatusFilesCount = " + eodStatusFilesCount + ", I'm skipping the retention)");
			return;
		}

		try {

			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.add(Calendar.DATE, -1 * numberOfDays);
			Date dateLastToKeep = cal.getTime();

			String[] files = eodStatusSubdir.list(new AgeFileFilter(dateLastToKeep));
			log.warn("Number of files to purge: " + files.length);
			for (int i = 0; i < files.length; i++) {
				String fileFullpath = eodStatusSubdir.getAbsolutePath() + File.separator + files[i];
				log.warn("The file \"" + fileFullpath + "\" is older than " + dateLastToKeep + " days");
				File file = new File(fileFullpath);
				if (file.exists()) {
					log.warn("Deleting file \"" + fileFullpath + "\"...");
					file.delete();
					log.warn("File \"" + fileFullpath + "\" deleted.");
				} else {
					log.warn("File \"" + fileFullpath + "\" does not exist!!");
				}
				file = null;
			}
		} catch (Exception e) {
			log.error("Exception!", e);
		}
		log.warn("END");
	}

	private void createDirIfNotExists(File dir) {
		if (!dir.exists()) {
			log.debug(String.format("creating [%s]", dir.getAbsolutePath()));

			dir.mkdir();
		}

		log.debug(String.format("[%s] present", dir.getAbsolutePath()));
	}

	private String read(File json) {
		StringBuilder sb = new StringBuilder();

		try (BufferedReader reader = new BufferedReader(new FileReader(json))) {
			String line = "";

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			reader.close();
			log.debug("reader.close() called");

		} catch (Exception e) {
			log.error("", e);
		}

		return sb.toString();
	}

	public synchronized EodStatusObject createCurrentEodStatus(boolean createFile) {
		currentEodStatus = new EodStatusObject(null, null, EodStatus.EOD_NOT_STARTED);
		currentEodStatus.setLastCompletedExecutionStepIndex(-1);
		currentEodStatus.setNumberOfCompletedExecutionSteps(0);

		if (createFile) {

			currentEodStatus.setEodStartDateTime(new Date());
			for (String message : EodMessages.getInstance().getMessagesEodStart(currentEodStatus.getEodStartDateTime())) {
				RunningInfo startingInfo = new RunningInfo();
				startingInfo.getCommand().setDescription(message);
				currentEodStatus.getEodSteps().add(startingInfo);
			}
			currentEodStatus.setStatus(EodStatus.EOD_RUNNING_OK);

			save();
		}

		return currentEodStatus;
	}

	private void save() {
		save(currentEodStatus);
	}

	public void save(EodStatusObject eodStatus) {
		Date date = eodStatus.getEodStartDateTime();
		String path = getFile(date);

		log.debug(String.format("path [%s]", path));
		log.debug(String.format("eodStatus [%s]", eodStatus));

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
			writer.write(gson.toJson(eodStatus));
		} catch (IOException e) {
			log.error("", e);
		}
	}

	private String getFile(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

		return String.format("%s%s%s%s.json", Globals.EOD_STATUS_FILES_SUBDIR, File.separator, Globals.EOD_STATUS_FILE_NAME, dateFormat.format(date));
	}

	public synchronized EodStatusObject createCurrentEodStatusFromExistingEodStatus(EodStatusObject existingEodStatus, ExecutionStepsReader stepReader) {
		Date oldDate = existingEodStatus.getEodStartDateTime();
		// existingEodStatus.setEodStartDateTime(new Date());
		existingEodStatus.setEodEndDateTime(null);

		log.debug(String.format("oldDate [%s], newDate [%s]", oldDate, existingEodStatus.getEodStartDateTime()));

		int nextStepIndex = stepReader.nextExecutionStep(existingEodStatus.getLastCompletedExecutionStepIndex());
		for (String message : EodMessages.getInstance().getMessagesEodContinue(nextStepIndex)) {
			RunningInfo restartingInfo = new RunningInfo();
			restartingInfo.getCommand().setDescription(message);
			existingEodStatus.getEodSteps().add(restartingInfo);
		}

		log.debug("forcing new state to be with warnings");
		existingEodStatus.setStatus(EodStatus.EOD_RUNNING_WITH_WARNINGS);

		log.debug("ready to create new file and delete old one");

		currentEodStatus = existingEodStatus;
		save();

		log.debug("new file created, old one deleted");

		return currentEodStatus;
	}

	private synchronized EodStatusObject updateCurrentEodStatus(int currentStepIndex, RunningInfo newInfo, String stepCompletionMessage, boolean updateSteps) {
		log.debug(String.format("currentStepIndex [%d]", currentStepIndex));

		if (updateSteps) {
			currentEodStatus.setNumberOfCompletedExecutionSteps(currentEodStatus.getNumberOfCompletedExecutionSteps() + 1);
			currentEodStatus.setLastCompletedExecutionStepIndex(currentStepIndex);
		}
		currentEodStatus.getEodSteps().add(newInfo);
		if (stepCompletionMessage != null & !stepCompletionMessage.isEmpty()) {
			Command command = new Command();
			command.setDescription(stepCompletionMessage);
			RunningInfo newRunningInfo = new RunningInfo(command);
			currentEodStatus.getEodSteps().add(newRunningInfo);
		}

		save();
		log.debug("currentEodStatus saved");

		return currentEodStatus;
	}

	public EodStatusObject updateCurrentEodStatus(EodStatus status, boolean successful) {
		if (successful) {
			return updateCurrentEodStatus(status, EodMessages.getInstance().getMessagesEodEndSuccessfully());
		} else {
			return updateCurrentEodStatus(status, EodMessages.getInstance().getMessagesEodEndWithCriticalErrors());
		}
	}

	public EodStatusObject updateCurrentEodStatus(EodStatus status, int numberOfNonCriticalErrors) {
		return updateCurrentEodStatus(status, EodMessages.getInstance().getMessagesEodEndWithNonCriticalErrors(numberOfNonCriticalErrors));
	}

	private synchronized EodStatusObject updateCurrentEodStatus(EodStatus status, String... messages) {
		currentEodStatus.setStatus(status);

		Date date = new Date();
		for (String message : messages) {
			RunningInfo closingInfo = new RunningInfo();

			log.debug("message: " + message);
			closingInfo.setStart(date);
			closingInfo.setEnd(date);
			closingInfo.getCommand().setDescription(message);
			currentEodStatus.getEodSteps().add(closingInfo);
		}

		save();
		log.debug("currentEodStatus saved");

		return currentEodStatus;
	}

	public synchronized EodStatusObject updateCurrentEodStatus(int currentStepIndex, RunningInfo newInfo, EodStatus status) {
		currentEodStatus.setStatus(status);

		switch (status) {
		default:
		case EOD_RUNNING_OK:
			dumpNewInfo(newInfo);
			newInfo.appendOutput(EodMessages.getInstance().getMessagesStepEndSuccessfully(currentStepIndex));
			updateCurrentEodStatus(currentStepIndex, newInfo, EodMessages.getInstance().getMessageStepEndSuccessfully(currentStepIndex), true);
			break;

		case EOD_ENDED_WITH_ERROR:
			dumpNewInfo(newInfo);
			newInfo.appendOutput(EodMessages.getInstance().getMessagesStepEndWithCriticalErrors(currentStepIndex));
			updateCurrentEodStatus(currentStepIndex, newInfo, EodMessages.getInstance().getMessageStepEndWithCriticalErrors(currentStepIndex), false);
			break;

		case EOD_RUNNING_WITH_WARNINGS:
			dumpNewInfo(newInfo);
			newInfo.appendOutput(EodMessages.getInstance().getMessagesStepEndWithNonCriticalErrors(currentStepIndex));
			updateCurrentEodStatus(currentStepIndex, newInfo, EodMessages.getInstance().getMessageStepEndWithNonCriticalErrors(currentStepIndex), true);
			break;
		}
		log.debug("currentEodStatus saved");

		return currentEodStatus;
	}

	private void dumpNewInfo(RunningInfo newInfo) {
		log.debug("BEGIN");
		log.debug("newInfo      = " + newInfo);
		if (newInfo == null) {
			log.debug("END");
			return;
		}
		log.debug("command      = " + newInfo.getCommand());
		log.debug("start        = " + newInfo.getStart());
		log.debug("end          = " + newInfo.getEnd());
		log.debug("output       = " + newInfo.getOutput());
		if (newInfo.getOutput() != null) {
			log.debug("output.size  = " + newInfo.getOutput().size());
			int index1 = 0;
			for (List<String> list : newInfo.getOutput()) {
				if (list != null) {
					log.debug("output[" + index1 + "].size  = " + list.size());
					int index2 = 0;
					for (String element : list) {
						log.debug("output[" + index1 + "][" + index2 + "]  = " + element);
						index2++;
					}
				}
				index1++;
			}
		}
		log.debug("error        = " + newInfo.getError());
		log.debug("ok           = " + newInfo.isOk());

		log.debug("END");
	}

	private static EndOfDayStatusReader endOfDayStatusReader;

	private EodStatusObject currentEodStatus;
	private Gson gson = new Gson();

	private static final Logger log = WebFrontLogger.getLogger(EndOfDayStatusReader.class);
}
