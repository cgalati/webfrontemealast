package com.ncr.webfront.base.eod.socketserver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EndOfDayStepsPropertyReader {

	public static EndOfDayStepsPropertyReader getInstance() {
		if (endOfDayStepsPropertyReader == null) {

			try {
				endOfDayStepsPropertyReader = new EndOfDayStepsPropertyReader(Globals.PROPERTIES_FILE_SUBDIR, Globals.getExecutionStepsPropertyFileName());

			} catch (IOException ioe) {
				log.error("Could not find " + Globals.getExecutionStepsPropertyFileName());
				log.debug("End (returning false)");
				return null;
			}
		}
		return endOfDayStepsPropertyReader;
	}

	protected EndOfDayStepsPropertyReader(String propertyFilesPath, String appPropertyFileName) throws FileNotFoundException, IOException {

		log.debug("Begin");

		log.info("Checking if " + appPropertyFileName + " exists...");
		propertyFileResourceBundle = new PropertyResourceBundle(new FileInputStream(propertyFilesPath + appPropertyFileName));

		log.debug("End (returning true)");

	}

	public String readProperty(String fieldName) {
		return readProperty(fieldName, true);
	}

	public int readIntProperty(String fieldName, int defaultValue) {
		int intValue = defaultValue;
		String stringValue = readProperty(fieldName);
		try {
			intValue = Integer.parseInt(stringValue);
		} catch (Exception e) {
			intValue = defaultValue;
		}
		return intValue;
	}

	public String readProperty(String fieldName, boolean silent) {
		String value = "";
		try {
			value = propertyFileResourceBundle.getString(fieldName);

		} catch (Exception e) {
			if (!silent) {
				log.error("Exception reading field \"" + fieldName);
			}
			return "";
		}

		if (!silent) {
			log.info(fieldName + " = " + value);
		}
		return value;
	}

	private static final Logger log = WebFrontLogger.getLogger(EndOfDayStepsPropertyReader.class);
	private static EndOfDayStepsPropertyReader endOfDayStepsPropertyReader;
	private ResourceBundle propertyFileResourceBundle;
}
