package com.ncr.webfront.base.eod.socketserver.socketserver;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.socketserver.Globals;
import com.ncr.webfront.base.eod.socketserver.engine.EndOfDayStatusReader;
import com.ncr.webfront.base.eod.socketserver.engine.ExecutionStepsEngine;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class WebFrontEodSocketServer implements Runnable {
	public static final String CHARSET = System.getProperty("file.encoding");
	public static final int SOCKET_READ_BUFFER_SIZE = 1024000;

	public WebFrontEodSocketServer(Socket clientSocket) {
		this.clientSocket = clientSocket;
		logger.debug(String.format("Read buffer size = %d", SOCKET_READ_BUFFER_SIZE));
	}

	public void run() {
		initializeExecutionStepEngine();

		execute();
	}

	private synchronized void initializeExecutionStepEngine() {
		logger.debug(String.format("eodThread null? [%b]", eodThread == null));

		if (eodThread != null) {
			logger.debug(String.format("eodThread alive? [%b]", eodThread.isAlive()));

			if (!eodThread.isAlive()) {
				eodThread = null;
			}
		}
		if (eodThread == null) {
			logger.debug(String.format("executionStepsEngine null? [%b]", executionStepsEngine == null));

			if (executionStepsEngine == null) {
				executionStepsEngine = ExecutionStepsEngine.create(Globals.PROPERTIES_FILE_SUBDIR, Globals.getExecutionStepsPropertyFileName());
				executionStepsEngine.initialize();
			}

			eodThread = new Thread(executionStepsEngine);
		}
	}

	public void execute() {
		InputStream in = null;
		OutputStream out = null;

		logger.debug("ServerSocket created");

		try {
			in = clientSocket.getInputStream();
			out = clientSocket.getOutputStream();

			logger.debug("client connection accepted");

			execute(out, in);

		} catch (Exception e) {
			logger.error("try", e);
		} finally {
			try {
				if (clientSocket != null) {
					logger.debug("Calling clientSocket.close()..");
					clientSocket.close();
				}
				logger.debug("Calling clientSocket = null..");
				clientSocket = null;

			} catch (Exception e) {
				logger.error("finally", e);
			}

		}
	}

	private void execute(OutputStream out, InputStream in) throws Exception {
		byte[] bytes = new byte[SOCKET_READ_BUFFER_SIZE];
		String command = "";

		int readLength = in.read(bytes);
		logger.debug(String.format("read [%d] bytes in buffer of size [%d]", readLength, bytes.length));

		command = new String(bytes, CHARSET).trim();

		logger.debug(String.format("read command [%s]", command));

		try {
			switch (command) {
			case "launchEodServer":
				launchEodServer(out);
				break;

			case "getEodStatus":
				getEodStatus(out);
				break;

			case "getEodStatusHistory":
				getEodStatusHistory(out);
				break;

			case "cancel":
				cancel(out);
				break;

			default:
				break;
			}
		} catch (SocketTimeoutException e) {
			throw e;
		} catch (Exception e) {
			send(out, new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage()));

			throw e;
		}
	}

	private synchronized void launchEodServer(OutputStream out) throws Exception {
		if (!eodThread.isAlive()) {
			executionStepsEngine.preStart();
			eodThread.start();
		}

		send(out, new ErrorObject(ErrorCode.OK_NO_ERROR));

	}

	private synchronized void getEodStatus(OutputStream out) throws Exception {
		EndOfDayStatusReader eodStatusReader = EndOfDayStatusReader.getInstance();
		EodStatusObject lastStatus = eodStatusReader.readLastEndOfDayStatus();
		RunningInfo updatedInfo = executionStepsEngine.getUpdatedRunningInfo();
		logger.debug("lastStatus = " + lastStatus);
		if (lastStatus != null && lastStatus.getEodSteps() != null) {
			int lastStepIndex = lastStatus.getEodSteps().size() - 1;

			if (lastStepIndex != -1) {
				RunningInfo lastInfo = lastStatus.getEodSteps().get(lastStepIndex);

				// When the step has already been saved and the next one has not
				// already started, lastInfo and updatedInfo are the same.
				// Hence, the latter must not be added.
				if (!lastInfo.equals(updatedInfo) && (lastStatus.getEodEndDateTime() == null)) {
					lastStatus.getEodSteps().add(updatedInfo);
				}
			}
		}
		logger.debug(String.format("last eod status [%s]", lastStatus));

		send(out, lastStatus);
	}

	private void getEodStatusHistory(OutputStream out) throws Exception {
		EndOfDayStatusReader eodStatusReader = EndOfDayStatusReader.getInstance();
		List<EodStatusObject> history = eodStatusReader.readEndOfDayHistory();

		logger.debug(String.format("EndOfDayHistory size = %d", history.size()));

		send(out, history);
	}

	private void cancel(OutputStream out) throws Exception {
		executionStepsEngine.cancel();

		send(out, "OK");
	}

	private <T> void send(OutputStream out, T object) throws Exception {
		String json = gson.toJson(object);

		logger.debug("Sending a json with length = " + json.length());
		logger.debug("json: \"" + json + "\"");
		out.write(json.getBytes(CHARSET));
		out.flush();
	}

	private Socket clientSocket;
	private Gson gson = new Gson();

	private static ExecutionStepsEngine executionStepsEngine;
	private static Thread eodThread;

	private static final Logger logger = WebFrontLogger.getLogger(WebFrontEodSocketServer.class);
}
