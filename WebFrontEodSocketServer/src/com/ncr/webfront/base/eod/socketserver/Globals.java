package com.ncr.webfront.base.eod.socketserver;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.paths.PathManager;

public class Globals {
	public static final String PROPERTIES_FILE_SUBDIR = "conf" + File.separator;
	public static final String EOD_STATUS_FILES_SUBDIR = "eodstatus" + File.separator;
	public static final String EOD_STATUS_FILE_NAME = "eodstatus";
	public static final String EOD_MESSAGES_FILE_NAME = "eodmessages.properties";
	private static final String EXECUTIONSTEPS_FILE_NAME_WINDOWS = "executionsteps_windows.properties";
	private static final String EXECUTIONSTEPS_FILE_NAME_LINUX = "executionsteps_linux.properties";
	public static final int APPLICATION_ERROR_EXIT_CODE = 1;
	public static final String PERSISTENCE_UNIT = "com.ncr.webfront.eod";

	public static String getExecutionStepsPropertyFileName() {
		if (SystemUtils.IS_OS_WINDOWS) {
			return EXECUTIONSTEPS_FILE_NAME_WINDOWS;
		}
		return EXECUTIONSTEPS_FILE_NAME_LINUX;
	}
	
	public static String getRootDir(Logger logger) {
		String rootDir = PathManager.getInstance().normalizePath(PathManager.getInstance().getHomeFolder(), true) + "webfront-endofday";
		if (logger != null){
			logger.debug("rootDir [" + rootDir+"]");
		}
		
		return rootDir;
	}

	public static String getTempDir(Logger logger) {
		return getRootDir(logger) + "/tmp";
	}

	public static void cleanupTempDir(Logger logger) {
		try{
			
		String tempDir = getTempDir(logger);
		File dir = new File (tempDir);
		for(File file: dir.listFiles()) {
			logger.debug("Globals.cleanupTempDir() - deleting file \"" + file.getName() +"\"");
			file.delete();
		}
		}
		catch (Exception e){
			logger.warn("Globals.cleanupTempDir() - Warning! Exception!", e);
		}
   }
}
