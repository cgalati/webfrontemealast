package com.ncr.webfront.base.eod.socketserver.engine;

import java.lang.reflect.Constructor;
import java.util.Date;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.EodStatusObject.EodStatus;
import com.ncr.webfront.base.eod.socketserver.Globals;
import com.ncr.webfront.base.eod.socketserver.engine.steps.AbstractExecutionStep;
import com.ncr.webfront.base.eod.socketserver.socketserver.EodMessages;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ExecutionStepsEngine implements Runnable {
	protected ExecutionStepsEngine(String propertiesFilesSubfolder, String executionStepsPropertyFileName) {
		executionStepsReader = ExecutionStepsReader.getInstance(propertiesFilesSubfolder, executionStepsPropertyFileName);
		endOfdayStatusReader = EndOfDayStatusReader.getInstance();
		endOfdayStatusReader.performEndOfDayHistoryRetention(executionStepsReader.readEodStatusHistoryRetentionDays());
	}

	public static ExecutionStepsEngine create(String propertiesFilesSubfolder, String executionStepsPropertyFileName) {
		if (executionStepsEngine == null) {
			log.info("Creating new instance of " + ExecutionStepsEngine.class.getSimpleName());

			executionStepsEngine = new ExecutionStepsEngine(propertiesFilesSubfolder, executionStepsPropertyFileName);
		}

		return executionStepsEngine;
	}

	public void preStart() {
		eodStatus = endOfdayStatusReader.readLastEndOfDayStatus();

		log.debug(String.format("eodStatus pre [%s]", eodStatus));

		if ((eodStatus.getStatus() == EodStatus.EOD_CRASHED) || (eodStatus.getStatus() == EodStatus.EOD_ENDED_WITH_ERROR)) {
			endOfdayStatusReader.createCurrentEodStatusFromExistingEodStatus(eodStatus, executionStepsReader);
		} else {
			eodStatus = endOfdayStatusReader.createCurrentEodStatus(true);
		}

		log.debug(String.format("eodStatus post [%s]", eodStatus));
	}

	@Override
	public void run() {

		Globals.cleanupTempDir(log);

		Pair<Integer, Integer> loopResult = loopUntillAllPresentStepsAreExecuted();

		if (eodStatus.getStatus() == EodStatus.EOD_ENDED_OK) {
			Globals.cleanupTempDir(log);
		}
		closeEodStatus(loopResult);
	}

	/*
	 * This is invoked once when the WebFrontEodSocketServer program starts (not
	 * each time the thread starts).
	 */
	public void initialize() {
		eodStatus = endOfdayStatusReader.readLastEndOfDayStatus();

		log.debug(String.format("eodStatus [%s]", eodStatus));

		if (eodStatus == null) {
			// Last eod not found.
			// First time ever.
			eodStatus = endOfdayStatusReader.createCurrentEodStatus(false);

			log.debug(String.format("eodStatus == null -> new eodStatus [%s]", eodStatus));
		} else {
			if (eodStatus.getStatus().isRunning()) {
				// Last eod running -> crashed.
				// Program stated and found a crashed eod.
				eodStatus.setStatus(EodStatus.EOD_CRASHED);

				log.debug("status set to EOD_CRASHED");

				endOfdayStatusReader.save(eodStatus);

				log.debug("status EOD_CRASHED saved to file");
			} else {
				// Last eod was either completed or failed.

				log.debug("done nothing");
			}
		}
	}

	private Pair<Integer, Integer> loopUntillAllPresentStepsAreExecuted() {
		// *****************************************************************************************
		// Count the number of steps in the property file.
		// Note it is possible some steps are missing or commented
		// *****************************************************************************************
		int totalNumberOfSteps = executionStepsReader.getTotalNumberOfExecutionSteps();
		log.info("totalNumberOfSteps (calculated from property file): " + totalNumberOfSteps);

		int currentStepIndex = eodStatus.getLastCompletedExecutionStepIndex() + 1;
		int numberOfStepsDone = eodStatus.getNumberOfCompletedExecutionSteps();
		int numberOfCriticalErrors = 0;
		int numberOfNonCriticalErrors = 0;
		do {
			log.info("currentStepIndex  : " + currentStepIndex);
			log.info("numberOfStepsDone : " + numberOfStepsDone);
			log.info("totalNumberOfSteps: " + totalNumberOfSteps);

			if (!executionStepsReader.isExecutionStepAvailable(currentStepIndex)) {
				log.info("Skipping step with index " + currentStepIndex + " because not present");
				currentStepIndex++;
				continue;
			}
			boolean isCriticalStepFlag = executionStepsReader.readIsCriticalStepFlag(currentStepIndex);
			log.info("BEFORE runExecutionStep(..., " + currentStepIndex + ")...");
			RunningInfo resultInfo = runExecutionStep(eodStatus, currentStepIndex);
			log.info("AFTER  runExecutionStep(..., " + currentStepIndex + ")...");
			log.info("totalNumberOfSteps: " + totalNumberOfSteps);
			if (resultInfo.isOk()) {
				log.info("Result of runExecutionStep(..., " + currentStepIndex + ") = true.");
				eodStatus = endOfdayStatusReader.updateCurrentEodStatus(currentStepIndex, resultInfo, EodStatus.EOD_RUNNING_OK);
			} else {
				if (isCriticalStepFlag) {
					numberOfCriticalErrors++;
					log.error("Result of runExecutionStep(..., " + currentStepIndex + ") = false (critical step).");
					log.fatal("Failure for critical step #" + currentStepIndex + "!!!");
					log.warn(String.format("cancelled? [%b]", cancelled));
					eodStatus = endOfdayStatusReader.updateCurrentEodStatus(currentStepIndex, resultInfo, EodStatus.EOD_ENDED_WITH_ERROR);
					break;
				} else {
					numberOfNonCriticalErrors++;
					log.error("Result of runExecutionStep(..., " + currentStepIndex + ") = false (non-critical step).");
					log.error("Failure for non-critical step #" + currentStepIndex + "!!!");
					log.warn(String.format("cancelled? [%b]", cancelled));
					eodStatus = endOfdayStatusReader.updateCurrentEodStatus(currentStepIndex, resultInfo, EodStatus.EOD_RUNNING_WITH_WARNINGS);
				}
			}
			currentStepIndex++;
			numberOfStepsDone++;
		} while (numberOfStepsDone < totalNumberOfSteps);

		return Pair.of(numberOfCriticalErrors, numberOfNonCriticalErrors);
	}

	private void closeEodStatus(Pair<Integer, Integer> loopResult) {
		int numberOfCriticalErrors = loopResult.getLeft();
		int numberOfNonCriticalErrors = loopResult.getRight();

		eodStatus.setEodEndDateTime(new Date());
		if (numberOfCriticalErrors > 0) {
			log.error("Ended with CRITICAL ERROR");
			eodStatus = endOfdayStatusReader.updateCurrentEodStatus(EodStatus.EOD_ENDED_WITH_ERROR, false);
		} else if (numberOfNonCriticalErrors > 0) {
			log.warn("Ended with WARNINGS");
			eodStatus = endOfdayStatusReader.updateCurrentEodStatus(EodStatus.EOD_ENDED_WITH_WARNINGS, numberOfNonCriticalErrors);
		} else {
			log.info("Ended SUCCESSFULLY");
			eodStatus = endOfdayStatusReader.updateCurrentEodStatus(EodStatus.EOD_ENDED_OK, true);
		}
	}

	private RunningInfo runExecutionStep(EodStatusObject eodStatus, int stepNumber) {
		int numberOfCompletedExecutionSteps = eodStatus.getNumberOfCompletedExecutionSteps();
		int totalNumberOfExecutionSteps = executionStepsReader.getTotalNumberOfExecutionSteps();

		String stepClassName = executionStepsReader.readStepClassName(stepNumber);
		String[] stepArguments = executionStepsReader.readStepArguments(stepNumber);

		log.info("BEGIN executing step #" + stepNumber + " (remaining steps: " + (totalNumberOfExecutionSteps - numberOfCompletedExecutionSteps) + ")");
		log.info("runEodStep - stepClassName = " + stepClassName);

		Class<?> partypes[] = new Class[stepArguments.length];
		for (int index = 0; index < stepArguments.length; index++) {
			partypes[index] = String.class;
		}

		RunningInfo resultInfo = new RunningInfo(); // Instantiated for the
													// catches.
		try {

			Class<?> clazz = Class.forName(stepClassName);

			Constructor<?> ct = clazz.getConstructor(partypes);

			synchronized (CURRENT_STEP_EXECUTING_LOCK) {
				currentStepExecuting = (AbstractExecutionStep) ct.newInstance(stepArguments);
				currentStepNumber = stepNumber;
				// Here there's a small vulnerability window between setting
				// currentStepNumber and actually starting that step (in the
				// next line).
			}
			currentStepExecuting.run(stepNumber);
			resultInfo = getUpdatedRunningInfo();
		} catch (ClassNotFoundException classNotFoundException) {
			log.error("Exception executing step #" + stepNumber + "! ClassNotFoundException!", classNotFoundException);

			resultInfo.appendError(classNotFoundException.getMessage());
		} catch (Exception e) {
			log.error("Exception executing step #" + stepNumber + "! Exception!", e);

			resultInfo.appendError(e.getMessage());
		}

		log.info("END executing step #" + stepNumber + "; result [" + resultInfo + "]");
		return resultInfo;
	}

	public void cancel() {
		synchronized (CURRENT_STEP_EXECUTING_LOCK) {
			cancelled = true;

			log.debug("cancelling");

			currentStepExecuting.cancel();
		}

		log.debug("cancelled");
	}

	public RunningInfo getUpdatedRunningInfo() {
		synchronized (CURRENT_STEP_EXECUTING_LOCK) {
			RunningInfo info = null;
			String stepDescription = executionStepsReader.readStepDescription(currentStepNumber);

			log.debug(String.format("reading updated RunningInfo for step #%d, description [%s]", currentStepNumber, stepDescription));

			if (currentStepExecuting != null) {
				try {
					info = currentStepExecuting.getRunningInfo();
				} catch (Exception e) {
					log.error("Exception in currentStepExecuting.getRunningInfo()!", e);
					log.warn("LOG20160531 - re-creating RunningInfo() because Exception!");
					info = new RunningInfo();
				}
			} else {
				info = new RunningInfo();
				log.debug("LOG20160531 - re-creating RunningInfo() because currentStepExecuting == null!");
			}

			log.debug("updated info retrieved.");

			log.debug("LOG20160531 - Setting MessagesStepBegin but use info.startDate()!");
			info.getCommand().setDescription(EodMessages.getInstance().getMessagesStepBegin(currentStepNumber, stepDescription, info.getStart()));
			return info;
		}
	}

	private ExecutionStepsReader executionStepsReader;
	private EndOfDayStatusReader endOfdayStatusReader;
	private EodStatusObject eodStatus;
	private AbstractExecutionStep currentStepExecuting;
	private int currentStepNumber;
	private boolean cancelled = false;

	private static ExecutionStepsEngine executionStepsEngine;

	private static final Logger log = WebFrontLogger.getLogger(ExecutionStepsEngine.class);
	private static final Object CURRENT_STEP_EXECUTING_LOCK = new Object();
}
