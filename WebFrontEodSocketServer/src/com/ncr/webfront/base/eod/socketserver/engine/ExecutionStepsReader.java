package com.ncr.webfront.base.eod.socketserver.engine;

import java.util.ArrayList;
import java.util.List;

import com.ncr.webfront.base.eod.socketserver.EndOfDayStepsPropertyReader;

public class ExecutionStepsReader {

	public static ExecutionStepsReader getInstance (String propertiesFilesSubfolder, String executionStepsPropertyFileName) {
		if (executionStepsReader == null){
			executionStepsReader = new ExecutionStepsReader( propertiesFilesSubfolder,  executionStepsPropertyFileName);
		}
		return executionStepsReader;
	}

	private ExecutionStepsReader(String propertiesFilesSubfolder, String executionStepsPropertyFileName) {
		endOfDayStepsPropertyReader = EndOfDayStepsPropertyReader.getInstance();
		totalNumberOfExecutionSteps = calculateNumberOfExecutionSteps(10000);
	}

	public boolean isExecutionStepAvailable(int currentStepIndex) {
		String className = readStepClassName(currentStepIndex);
		
		return !className.trim().isEmpty();
	}
	
	public int nextExecutionStep(int currentStepIndex) {
		do {
			currentStepIndex++;
		} while (!isExecutionStepAvailable(currentStepIndex));
		
		return currentStepIndex;
	}

	public int calculateNumberOfExecutionSteps(int currentExecutionStep) {
		int numberOfSteps = 0;

		for (int index = 0; index <= currentExecutionStep; index++) {
			if (isExecutionStepAvailable(index)) {
				numberOfSteps++;
			}
		}
		return numberOfSteps;
	}

	public String readStepClassName(int stepNumber) {
		String fieldName = "execution.step." + stepNumber + ".classname";
		return endOfDayStepsPropertyReader.readProperty(fieldName, true);
	}
	
	public String readStepDescription(int stepNumber) {

		String fieldName = "execution.step." + stepNumber + ".description";
		String value = endOfDayStepsPropertyReader.readProperty(fieldName, true);
		return value;
	}

	public boolean readIsCriticalStepFlag(int stepNumber) {
		String fieldName = "execution.step." + stepNumber + ".critical";
		String value = endOfDayStepsPropertyReader.readProperty(fieldName, true);
		
		return value.trim().equalsIgnoreCase("true");
	}

	public String[] readStepArguments(int stepNumber) {

		List<String> argumentList = new ArrayList<String>();
		int argumentNumber = 1;
		do {

			String fieldName = "execution.step." + stepNumber + ".constructor.argument." + argumentNumber;
			String value = endOfDayStepsPropertyReader.readProperty(fieldName, true);
			if (value.length() == 0) {
				break;
			}

			argumentList.add(value);
			argumentNumber++;
		} while (true);

		return argumentList.toArray(new String[argumentList.size()]);
	}

	public int getTotalNumberOfExecutionSteps() {
		return totalNumberOfExecutionSteps;
	}


	public int readEodStatusHistoryRetentionDays() {
		String fieldName = "eodstatus.history.retention.days";
		int value = endOfDayStepsPropertyReader.readIntProperty(fieldName, 0);
		return value;
	}
	
	private static ExecutionStepsReader executionStepsReader;
	private EndOfDayStepsPropertyReader endOfDayStepsPropertyReader;
	private int totalNumberOfExecutionSteps;
}
