package com.ncr.webfront.base.eod.socketserver;

import com.ncr.webfront.core.utils.versioning.ComponentVersionInterface;

public class VersionWebFrontEodSocketServer implements ComponentVersionInterface {

	private final static String VERSION_NUMBER = "0.18.0819";
	private final static String VERSION_DATE = "18/08/2018";
	private final static String COMPONENT_NAME = "WebFront EMEA EndOfDay Socket Server";

	@Override
	public String getVersionNumber() {
		return VERSION_NUMBER;
	}

	@Override
	public String getVersionDate() {
		return VERSION_DATE;
	}

	@Override
	public String getComponentName() {
		return COMPONENT_NAME;
	}

	@Override
	public String getComponentNameAndVersion() {
		return COMPONENT_NAME + " v. " + VERSION_NUMBER + " (" + VERSION_DATE + ")";
	}
}
