package com.ncr.webfront.base.eod.socketserver.engine.steps;

import java.io.File;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.socketserver.Globals;
import com.ncr.webfront.base.eod.socketserver.socketserver.EodMessages;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.commandexecution.CommandLocalRunner;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;

public class ExecutionStepCmdCommand extends AbstractExecutionStep {
	public ExecutionStepCmdCommand(String commandLine) {
		this(commandLine, "");
	}

	public ExecutionStepCmdCommand(String commandLine, String stringTimeoutInSeconds) {
		logger.debug("BEGIN");

		int timeoutInSeconds = detectTimeoutInSeconds(stringTimeoutInSeconds);
		logger.info("commandLine     = " + commandLine);
		logger.info("timeoutInSeconds= " + timeoutInSeconds);

		localRunner = new CommandLocalRunner(Globals.getTempDir(logger));
		localRunner.setTimeoutInSeconds(timeoutInSeconds);
		Command command = new Command();

		logger.debug(String.format("commandLine [%s] before normalizePath:", commandLine));
		commandLine = normalizePath(commandLine);
		logger.debug(String.format("commandLine [%s] after  normalizePath:", commandLine));

		command.setFullCommandLine(commandLine);
		info = new RunningInfo(command);
		logger.debug("END");
	}

	private int detectTimeoutInSeconds(String stringTimeoutInSeconds) {
		logger.debug("BEGIN");
		logger.debug("stringTimeoutInSeconds:" + stringTimeoutInSeconds);
		if (stringTimeoutInSeconds == null || stringTimeoutInSeconds.isEmpty()) {
			logger.debug("Getting TimeoutInSeconds from global settings (webfront-endofday.properties file, field command.execution.timeout.in.seconds)");
			stringTimeoutInSeconds = "" + WebFrontMappingProperties.getInstance().getCommandExecutionTimeoutInSeconds();
			logger.debug("stringTimeoutInSeconds:" + stringTimeoutInSeconds);
		}
		int timeoutInSeconds = 0;
		logger.debug("Converting TimeoutInSeconds string into integer...");
		try {
			timeoutInSeconds = Integer.valueOf(stringTimeoutInSeconds);
		} catch (Exception e) {
			logger.error("Exception", e);
			timeoutInSeconds = 0;
		}
		if (timeoutInSeconds <= 0) {
			timeoutInSeconds = 60;
			logger.debug("Forcing hardcoded default for TimeoutInSeconds :" + timeoutInSeconds);
		}
		logger.debug("END(" + timeoutInSeconds + ")");
		return timeoutInSeconds;
	}

	private String normalizePath(String commandLine) {
		logger.debug("BEGIN");
		if (commandLine.startsWith("/")) {
			logger.debug("END (" + commandLine + ")");
			return commandLine;
		}
		if (SystemUtils.IS_OS_WINDOWS) {
			if (commandLine.length() >= 2 && commandLine.charAt(1) == ':') {
				logger.debug("END (" + commandLine + ")");
				return commandLine;
			}
		}

		commandLine = Globals.getRootDir(logger) + File.separator + commandLine;
		logger.debug("END (" + commandLine + ")");
		return commandLine;
	}

	@Override
	public void run() {
		logger.info("BEGIN");
		localRunner.setRunningInfo(info);
		localRunner.run();

		logger.debug(String.format("command ended", stepNumber));
		logger.info("END");
	}

	@Override
	public void cancel() {
		localRunner.cancel();

		super.cancel();
	}

	@Override
	public RunningInfo getRunningInfo() {
		localRunner.update();

		return info;
	}

	private CommandLocalRunner localRunner;
	private RunningInfo info = new RunningInfo();

	private static final Logger logger = WebFrontLogger.getLogger(ExecutionStepCmdCommand.class);
}
