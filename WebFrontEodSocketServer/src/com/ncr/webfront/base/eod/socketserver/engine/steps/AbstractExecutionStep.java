package com.ncr.webfront.base.eod.socketserver.engine.steps;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.socketserver.socketserver.EodMessages;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class AbstractExecutionStep implements Runnable {
	public void run(int stepNumber) {
		this.stepNumber = stepNumber;
		executing = new Thread(this);
		
		log.debug(String.format("step #[%d]", stepNumber));
		
		executing.start();
		do {
			try {
				Thread.sleep(1_000);
			} catch (InterruptedException e) {
				log.warn("", e);
			}
		} while (executing.isAlive() && !executing.isInterrupted());
	}
	
	public void cancel() {
		log.debug("before cancelling execution");
		
		executing.interrupt();
		getRunningInfo().appendError(EodMessages.getInstance().getMessagesStepCancelled(stepNumber));
		
		log.debug("execution cancelled");
	}
	
	public abstract RunningInfo getRunningInfo();
	
	protected int stepNumber;
	
	private Thread executing;
	
	private Logger log = WebFrontLogger.getLogger(AbstractExecutionStep.class);
}
