package com.ncr.webfront.base.eod.socketserver;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.socketserver.socketserver.WebFrontEodSocketServer;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.core.utils.versioning.VersionManager;

public class Main {
	public static final int SERVER_SOCKET_TIMEOUT = 10_000;
	public static final int CLIENT_SOCKET_TIMEOUT = 60_000;

	public static void main(String[] args) throws IOException {

		WebFrontMappingProperties.changeWebFrontConfigByReadingItFromWar();
		WebFrontMappingProperties.changeName("webfront-endofday");
		logger = WebFrontLogger.getLogger(Main.class);

		int port = Integer.parseInt(readParamFromArgs(args, "-p", "8888"));
		String terminationPath = readParamFromArgs(args, "-t", "");

		if (!initialize(Globals.PROPERTIES_FILE_SUBDIR, Globals.getExecutionStepsPropertyFileName())) {
			System.out.println("Exiting the program with return code " + Globals.APPLICATION_ERROR_EXIT_CODE);
			System.exit(Globals.APPLICATION_ERROR_EXIT_CODE);
		}

		listen(port, terminationPath);
	}

	private static void listen(int port, String terminationPath) {
		while (true) {
			logger.debug("pre serverSocket");

			try (ServerSocket serverSocket = new ServerSocket(port)) {
				Socket clientSocket = null;
				WebFrontEodSocketServer webFrontEodSocketServer = null;

				logger.debug("post serverSocket");

				if (StringUtils.isNotBlank(terminationPath)) {
					serverSocket.setSoTimeout(SERVER_SOCKET_TIMEOUT);
				}
				clientSocket = serverSocket.accept();
				clientSocket.setSoTimeout(CLIENT_SOCKET_TIMEOUT);
				clientSocket.setSoLinger(false, 0);

				logger.debug("connection accepted");

				webFrontEodSocketServer = new WebFrontEodSocketServer(clientSocket);
				new Thread(webFrontEodSocketServer).run();

				logger.debug("thread started");
			} catch (SocketTimeoutException e) {
				if (StringUtils.isNotBlank(terminationPath) && new File(terminationPath).exists()) {
					logger.info(String.format("Exiting because termination file [%s] detected", terminationPath));

					return;
				} else {
					// No termination file found or set: listen for new and
					// future connections.
				}
			} catch (Exception e) {
				logger.error("", e);
			}
		}
	}

	private static String readParamFromArgs(String[] args, String prefix, String defaultValue) {
		for (String arg : args) {
			if (arg.startsWith(prefix)) {
				String value = arg.replace(prefix, "");

				if (StringUtils.isBlank(value)) {
					value = defaultValue;
				}
				System.out.println(String.format("parameter [%s] found with value [%s]", prefix, value));

				return value;
			}
		}

		return defaultValue;
	}

	private static boolean initialize(String propertiesFilesPath, String executionStepsPropertyFileName) {
		String filePath;
		File file;

		System.out.println(Main.class.getName() + ".initialize BEGIN");
		System.out.println("propertiesFilesPath   			= " + propertiesFilesPath);
		System.out.println("executionStepsPropertyFileName = " + executionStepsPropertyFileName);

		filePath = propertiesFilesPath + executionStepsPropertyFileName;
		file = new File(filePath);
		if (!file.exists()) {
			System.out.println("Error - File " + filePath + " does not exist!");
			System.out.println(Main.class.getName() + ".initialize END (returning false");
			return false;
		}

		VersionManager.addVersionOnlyForLogVersion(new VersionWebFrontEodSocketServer());
		VersionManager.logVersions(logger);
		return true;
	}

	private static Logger logger;
}
