package com.ncr.webfront.base.eod.socketserver.engine.steps;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ExecutionStepDelay extends AbstractExecutionStep {
	public ExecutionStepDelay(String milliseconds) {
		this.milliseconds = NumberUtils.toInt(milliseconds, 1000);
	}

	@Override
	public void run() {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			logger.debug("interrupted");
		}
	}
	
	@Override
	public RunningInfo getRunningInfo() {
		return new RunningInfo();
	}

	private int milliseconds;

	private static final Logger logger = WebFrontLogger.getLogger(ExecutionStepDelay.class);
}
