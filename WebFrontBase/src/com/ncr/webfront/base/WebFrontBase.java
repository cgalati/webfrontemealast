package com.ncr.webfront.base;

import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.core.utils.versioning.VersionManager;
import com.ncr.webfront.eventlogger.VersionWebFrontEventLogger;
import com.ncr.webfront.eventlogger.WebFrontEventLogger;
import com.ncr.webfront.eventlogger.plugin.EventLoggerService;
import com.ncr.webfront.base.VersionWebFrontBase;
import com.ncr.webfront.base.commondata.VersionWebFrontBaseCommonData;
import com.ncr.webfront.base.restclient.VersionWebFrontBaseRestClient;

public class WebFrontBase {

	public static void initialize() {
		VersionManager.addVersionOnlyForLogVersion(new VersionWebFrontBaseCommonData());
		VersionManager.addVersionOnlyForLogVersion(new VersionWebFrontBaseRestClient());
		VersionManager.addVersionOnlyForLogVersion(new VersionWebFrontEventLogger());
		VersionManager.addVersion(new VersionWebFrontBase());
		VersionManager.logVersions();
		
		EventLoggerService.initialize(WebFrontMappingProperties.getInstance().getEventsRetentionDays());
	}
}
