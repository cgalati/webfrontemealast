package com.ncr.webfront.base.utils;

import org.apache.log4j.Logger;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.Label;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EndOfDayStatusTreeRenderer implements TreeitemRenderer<DefaultTreeNode<String>> {

	public EndOfDayStatusTreeRenderer(String normalLabelStyle, String warningLabelStyle, String errorLabelStyle) {
		super();
		this.normalLabelStyle = normalLabelStyle;
		this.warningLabelStyle = warningLabelStyle;
		this.errorLabelStyle = errorLabelStyle;
		logger.debug("normalLabelStyle = \"" + normalLabelStyle + "\"");
		logger.debug("warningLabelStyle = \"" + warningLabelStyle + "\"");
		logger.debug("errorLabelStyle = \"" + errorLabelStyle + "\"");
	}

	@Override
	public void render(final Treeitem treeItem, DefaultTreeNode<String> treeNode, int index) throws Exception {
		DefaultTreeNode<String> treenode = treeNode;
		String data = (String) treenode.getData();
		Treerow dataRow = new Treerow();
		dataRow.setParent(treeItem);
		treeItem.setValue(data);

		Label labelData = new Label(data);
		labelData.setSclass("ss_label");
		logger.debug("data = \"" + data + "\"");
		if (normalLabelStyle != null && !normalLabelStyle.isEmpty()) {
			if (data != null && data.startsWith("@WARNING@")) {
				labelData.setValue(data.substring("@WARNING@".length()));
				labelData.setStyle(warningLabelStyle);
			} else if (data != null && data.startsWith("@ERROR@")) {
				labelData.setValue(data.substring("@ERROR@".length()));
				labelData.setStyle(errorLabelStyle);
			} else {
				labelData.setStyle(normalLabelStyle);
			}
		}

		// TODO AAAA Se non ha nodi figli, togliere icona x espandere
		Treecell treeCell = new Treecell();
		treeCell.appendChild(labelData);
		dataRow.appendChild(treeCell);
	}

	private static final Logger logger = WebFrontLogger.getLogger(EndOfDayStatusTreeRenderer.class);
	private String normalLabelStyle;
	private String warningLabelStyle;
	private String errorLabelStyle;
}
