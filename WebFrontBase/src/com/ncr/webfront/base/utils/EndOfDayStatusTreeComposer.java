package com.ncr.webfront.base.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.TreeModel;
import org.zkoss.zul.TreeNode;

import sun.util.logging.resources.logging;

import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EndOfDayStatusTreeComposer {
	public static final String OUTPUT_TREE_LABEL = "Output";
	public static final String ERROR_TREE_LABEL = "Error";

	private EndOfDayStatusTreeComposer() {
	}

	public static TreeModel<TreeNode<String>> composeOneLevel(TreeModel<TreeNode<String>> tree, RunningInfo data, boolean flat) {
		List<TreeNode<String>> checkpoints = (tree != null) ? tree.getRoot().getChildren() : new ArrayList<TreeNode<String>>();

		populate(checkpoints, data, flat);

		if (tree == null) {
			tree = new DefaultTreeModel<>(new DefaultTreeNode<>("root", checkpoints));
		}

		return tree;
	}

	public static TreeModel<TreeNode<String>> composeTwoLevels(TreeModel<TreeNode<String>> tree, List<RunningInfo> data, boolean flat) {
		List<TreeNode<String>> steps = null;

		if (tree == null) {
			tree = new DefaultTreeModel<String>(new DefaultTreeNode<String>("root", new ArrayList<TreeNode<String>>()));
		}
		steps = tree.getRoot().getChildren();

		if (data != null) {
			for (int i = previous(steps.size()); i < data.size(); i++) {
				TreeNode<String> step = (i < steps.size()) ? steps.get(i) : null;
				List<TreeNode<String>> checkpoints = (i < steps.size()) ? steps.get(i).getChildren() : new ArrayList<TreeNode<String>>();
				RunningInfo stepInfo = data.get(i);
				populate(checkpoints, stepInfo, flat);

				if (step == null) {
					step = new DefaultTreeNode<>(stepInfo.getCommand().getDescription(), checkpoints);
					steps.add(step);
				}
			}
		}

		return tree;
	}

	private static void populate(List<TreeNode<String>> checkpoints, RunningInfo info, boolean flat) {
		if (flat) {
			checkpoints.clear();
			for (List<String> messages : info.getOutput()) {
				addLeafFlat(checkpoints, messages);
			}
			for (List<String> messages : info.getError()) {
				addLeafFlat(checkpoints, messages);
			}
		} else {
			addCheckpoint(checkpoints, info);
		}
	}

	private static void addLeafFlat(List<TreeNode<String>> checkpoints, List<String> messages) {
		logger.debug("messages: " + messages);
		for (String message : messages) {
			logger.debug("   message: " + message);
			if (message == null) {
				logger.warn("message == null!");
			}
			if (message != null && (!message.equals(RunningInfo.DEFAULT_CHECKPOINT))) {
				checkpoints.add(new DefaultTreeNode<>(message));
			}
		}
	}

	private static void addCheckpoint(List<TreeNode<String>> checkpoints, RunningInfo data) {
		int outputSections = data.getOutput().size();
		int errorSections = data.getError().size();

		for (int i = previous(checkpoints.size()); i < Math.max(outputSections, errorSections); i++) {
			TreeNode<String> checkpoint = (i < checkpoints.size()) ? checkpoints.get(i) : null;
			String checkpointName = "";
			List<TreeNode<String>> rows = (checkpoint != null) ? checkpoint.getChildren() : new ArrayList<TreeNode<String>>();

			checkpointName = addSection(rows, OUTPUT_TREE_LABEL, data.getOutput(), i, checkpointName);
			checkpointName = addSection(rows, ERROR_TREE_LABEL, data.getError(), i, checkpointName);

			if (checkpoint == null) {
				checkpoint = new DefaultTreeNode<>(checkpointName, rows);
				checkpoints.add(checkpoint);
			}
		}
	}

	private static int previous(int index) {
		return (index == 0) ? 0 : index - 1;
	}

	/*
	 * A "section" is an output or error node, plus its children.
	 */
	private static String addSection(List<TreeNode<String>> rows, String nodeName, List<List<String>> checkpoints, int checkpoint, String checkpointName) {
		if (checkpoint < checkpoints.size()) {
			if (!checkpoints.get(checkpoint).isEmpty()) {
				checkpointName = checkpoints.get(checkpoint).get(0);
			}
			addRowsToSection(rows, nodeName, checkpoints.get(checkpoint));
		}

		return checkpointName;
	}

	private static void addRowsToSection(List<TreeNode<String>> rows, String nodeName, List<String> messages) {
		int sectionNumber = nodeName.equals(OUTPUT_TREE_LABEL) ? 0 : 1;
		List<TreeNode<String>> leaves = (rows.size() > sectionNumber) ? rows.get(sectionNumber).getChildren() : new ArrayList<TreeNode<String>>();
		int added = 0;

		for (int i = leaves.size() + 1; i < messages.size(); i++) {
			TreeNode<String> leaf = new DefaultTreeNode<>(messages.get(i));

			leaves.add(leaf);
			added++;
		}

		if ((added > 0) && (rows.size() <= sectionNumber)) {
			rows.add(new DefaultTreeNode<>(nodeName, leaves));
		}
	}

	private static final Logger logger = WebFrontLogger.getLogger(EndOfDayStatusTreeComposer.class);
}
