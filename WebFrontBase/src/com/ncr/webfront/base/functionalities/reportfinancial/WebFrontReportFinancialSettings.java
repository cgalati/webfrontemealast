package com.ncr.webfront.base.functionalities.reportfinancial;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontReportFinancialSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontReportFinancialSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontReportFinancialSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontReportFinancialSettings("reports.properties");
		}
		return instance;
	}

	protected WebFrontReportFinancialSettings(String propertyFileName) {
		super(propertyFileName);
	}

	
	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportfinancial.title", "Financial Report");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getConsolidated() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportfinancial.consolidated", "Consolidated");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDateLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportfinancial.date", "Date");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getTerminals() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportfinancial.terminals", "Terminals");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getFormatLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportfinancial.format", "Format");
		logger.debug("END (" + value + ")");
		return value;
	}
}
