package com.ncr.webfront.base.functionalities.endofdayreport;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.reports.EndOfDayReportProperties;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class EndOfDayReportViewModel {
	@GlobalCommand
	public void onDesktopResize(@BindingParam("width") int width, @BindingParam("height") int height, @BindingParam("orientation") String orientation) {
		logger.debug("orientation = \"" + Executions.getCurrent().getDesktop().getAttribute("orientation") + "\"");
		logger.debug("width       = \"" + Executions.getCurrent().getDesktop().getAttribute("width") + "\"");
		logger.debug("height      = \"" + Executions.getCurrent().getDesktop().getAttribute("height") + "\"");
	}

	@Init
	public void init() {
		logger.info("Init");
		getDirectoriesList();
		if (directories.size() > 0) {
			eodReportsProperties(directories.get(0));
			logger.info("Last dir: " + directories.get(directories.size() - 1));
		}
	}

	@Command
	public List<String> getDirectoriesList() {
		directories = new ArrayList<String>();
		directoriesMap = new ArrayList<String>();
		String tmp;
		File file = new File(reportspath);
		String[] names = file.list();
		for (String name : names) {
			directories.add(formatData(name));
			directoriesMap.add(name);
		}
		for (int i = 0; i < directoriesMap.size() - 1; i++) {
			for (int k = 0; k < directoriesMap.size() - 1; k++) {
				if (directoriesMap.get(k).compareTo(directoriesMap.get(k + 1)) < 0) {
					tmp = directories.get(k);
					directories.set(k, directories.get(k + 1));
					directories.set(k + 1, tmp);
					tmp = directoriesMap.get(k);
					directoriesMap.set(k, directoriesMap.get(k + 1));
					directoriesMap.set(k + 1, tmp);
				}
			}
		}
		return new ArrayList<String>(directories);
	}

	public List<EndOfDayReportProperties> eodReportsProperties(String dir) {
		ArrayList<EndOfDayReportProperties> eodReportPropertiesList = new ArrayList<EndOfDayReportProperties>();
		Gson gson = new Gson();
		Type endOfDayReportPropertiesListType = new TypeToken<List<EndOfDayReportProperties>>() {
		}.getType();
		String eodReportPropertiesJsonString = "";
		try {
			if (directories.indexOf(dir) > -1) {
				dirMap = directoriesMap.get(directories.indexOf(dir));
			}
			eodReportPropertiesJsonString = FileUtils.readFileToString(new File(reportspath + File.separator + dirMap + File.separator
					+ "eod_reports_properties.json"));
			directory = dir;
		} catch (IOException e) {
			logger.error(e);
		}
		eodReportPropertiesList = gson.fromJson(eodReportPropertiesJsonString, endOfDayReportPropertiesListType);
		setEodReportsProperties(eodReportPropertiesList);
		if (eodReportPropertiesList != null) {
			System.out.println("Files: " + eodReportPropertiesList.size());
			File chk;
			ListIterator<EndOfDayReportProperties> iter = eodReportPropertiesList.listIterator();
			while (iter.hasNext()) {
				EndOfDayReportProperties item = iter.next();
				chk = new File(reportspath + File.separator + dirMap + File.separator + item.getFilename());
				if (!chk.exists()) {
					System.out.println("Removing file \"" + item.getFilename() + "\"");
					iter.remove();
				} else {
					System.out.println("Keeping  file \"" + item.getFilename() + "\"");
				}
			}
		}
		logger.info("Properties: " + eodReportPropertiesList);
		return eodReportPropertiesList;
	}

	public ArrayList<EndOfDayReportProperties> getEodReportsProperties(String dir) {
		eodReportsProperties(dir);
		return eodReportPropertiesList;
	}

	public void setEodReportsProperties(ArrayList<EndOfDayReportProperties> eodReportsPropertiesList) {
		this.eodReportPropertiesList = eodReportPropertiesList;
	}

	public String getLastDirectory() {
		return "last";
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getDirectory() {
		return directory;
	}

	@Command
	public void download(@BindingParam("filename") String filename) {
		try {
			File report = new File(reportspath + File.separator + directoriesMap.get(directories.indexOf(directory)) + File.separator + filename);
			Filedownload.save(report, "application/pdf");
		} catch (FileNotFoundException e) {
			logger.error(e);
		}
	}

	@Command
	public String formatData(@BindingParam("name") String name) {
		String giorno = "";
		String mese = "";
		String anno = "";
		String after = "";
		if (name.charAt(0) > '9') {
			try {
				Path p = Paths.get((PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + "eodreports" + File.separator + name));
				BasicFileAttributes folder = Files.getFileAttributeView(p, BasicFileAttributeView.class).readAttributes();
				if (name.indexOf('.') != -1) {
					after = name.substring(name.indexOf('.') + 1);
				}
				giorno = folder.creationTime().toString().substring(8, 10);
				mese = folder.creationTime().toString().substring(5, 7);
				anno = folder.creationTime().toString().substring(0, 4);
			} catch (IOException e) {
				Messagebox.show("Directory not found");
			}
		} else {
			giorno = name.substring(6, 8);
			mese = name.substring(4, 6);
			anno = name.substring(0, 4);
		}
		if (name.length() > 8 || after != "") {
			after = name.substring(name.indexOf('.') + 1);
			return giorno + "/" + mese + "/" + anno + " " + after;
		}
		return giorno + "/" + mese + "/" + anno;
	}

	private List<String> directories;
	private List<String> directoriesMap;
	private String directory;
	private String dirMap;
	private ArrayList<EndOfDayReportProperties> eodReportPropertiesList;
	private final Logger logger = WebFrontLogger.getLogger(EndOfDayReportViewModel.class);
	private String reportspath = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + "eodreports";
}
