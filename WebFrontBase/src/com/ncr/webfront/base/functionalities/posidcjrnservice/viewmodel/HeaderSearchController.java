package com.ncr.webfront.base.functionalities.posidcjrnservice.viewmodel;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;


public class HeaderSearchController extends SelectorComposer<Component>{
	@Wire
    private Component btnExport;
    private static final long serialVersionUID = 1L;
    
    @Listen("onSelect = #headerListBox")
	public void selectDataHeader() {

		btnExport.setVisible(true);

	}
}
