package com.ncr.webfront.base.functionalities.posidcjrnservice;

import com.ncr.webfront.core.utils.versioning.ComponentVersionInterface;

public class VersionWebFrontEmea implements ComponentVersionInterface {

	private final static String VERSION_NUMBER = "0.18.0821";
	private final static String VERSION_DATE = "21/08/2018";
	private final static String COMPONENT_NAME = "WebFrontEmeaHQ";
	private final static String APPLICATION_NAME = "Webfront EMEA HQ";

	@Override
	public String getVersionNumber() {
		return VERSION_NUMBER;
	}

	@Override
	public String getVersionDate() {
		return VERSION_DATE;
	}

	@Override
	public String getComponentName() {
		return COMPONENT_NAME;
	}

	@Override
	public String getComponentNameAndVersion() {
		return COMPONENT_NAME + " v. " + VERSION_NUMBER + " (" + VERSION_DATE + ")";
	}

	public String getApplicationName() {
		return APPLICATION_NAME;
	}
}
