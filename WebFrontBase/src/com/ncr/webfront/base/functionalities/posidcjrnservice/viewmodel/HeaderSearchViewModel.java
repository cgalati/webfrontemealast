package com.ncr.webfront.base.functionalities.posidcjrnservice.viewmodel;

import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;

import org.zkoss.zk.ui.util.Clients;


import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.language.WebFrontZkLabelsLocator;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.zkconverters.ZkCentsToStringCurrencyConverter;
import com.ncr.webfront.base.functionalities.posidcjrnservice.WebFrontEmeaSettings;
import com.ncr.webfront.base.plugins.dbtest.PosIdcJrnServiceImplementation;
//import com.ncr.webfront.base.plugins.dbtest.PosIdcJrnServiceImplementation;
import com.ncr.webfront.base.posidcjrn.PosIdcJrnServiceInterface;
import com.ncr.webfront.base.posidcjrn.entities.*;
import com.ncr.webfront.base.restclient.posidcjrnservice.PosIdcJrnServiceClientImpl;
import com.google.gson.*;


public class HeaderSearchViewModel {	
	private static final Logger log = WebFrontLogger.getLogger(HeaderSearchViewModel.class);	
	private RetentionHeaders retentionHeaders = new RetentionHeaders();	
	private List<DataHeader> listDataHeader = new ArrayList<DataHeader>();
	private Set<DataCollect>  listDataCollect;
	private Set<Journal> listJournal;
	private Gson gson = new Gson();
	private String terminal = "";
	private String store = "";
	private String receipt = "";
	private String customer = "";
	private DataHeader selectedDataHeader;
	private Date dateFrom;
	private Date dateTo;
	private String msgExportIdc = "";
	private String msgExportJrn = "";
	private String today = "";
	private String buttonExport = "";
    //private PrintJournal print = new PrintJournal();
	private PosIdcJrnServiceInterface invoiceService;
	//private PosIdcJrnServiceInterface invoiceService = new PosIdcJrnServiceImplementation();

	
	public HeaderSearchViewModel() {
		invoiceService = new PosIdcJrnServiceClientImpl();
	}
	
	
	
	public PosIdcJrnServiceInterface getInvoiceService() {
		return invoiceService;
	}

	public void setInvoiceService(PosIdcJrnServiceInterface invoiceService) {
		this.invoiceService = invoiceService;
	}

	@Init
	public void init() {
		ErrorObject errorObject = invoiceService.checkService();
		
		if ((errorObject == null) || errorObject.getErrorCode().isOk()) {
			//postInit();
		} else {
			MainComposer.notifyServiceProblems("PosIdcJrnSearchService", errorObject.getDescription());
			MainComposer.goToHome();
		}
	}
    
	public List<DataHeader> getListDataHeader() {
		return listDataHeader;
	}

	public void setListDataHeader(List<DataHeader> listDataHeader) {
		this.listDataHeader = listDataHeader;
	}
	
	public Date getDateFrom() {
		return dateFrom;
	}
	
	@NotifyChange({ "dateFrom" })
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
		BindUtils.postNotifyChange(null, null, this, "dateFrom");
	}
	
	public Date getDateTo() {
		return dateTo;
	}
	
	@NotifyChange({ "dateTo" })
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
		BindUtils.postNotifyChange(null, null, this, "dateTo");
	}

	//cassa
	public String getTerminal() {
		return terminal;
	}
	
	@NotifyChange({ "terminal" })
	public void setTerminal(String terminal) {
		this.terminal = terminal;
		BindUtils.postNotifyChange(null, null, this, "terminal");
	}
		
	//transazione
	public String getReceipt() {
		return receipt;
	}
	
	@NotifyChange({ "receipt" })
	public void setReceipt(String receipt) {
		this.receipt = receipt;
		BindUtils.postNotifyChange(null, null, this, "receipt");
	}
	
	//customer
	public String getCustomer() {
		return customer;
	}

	@NotifyChange({ "customer" })
	public void setCustomer(String customer) {
		this.customer = customer;
		BindUtils.postNotifyChange(null, null, this, "customer");
	}

	//negozio
	public String getStore() {
		return store;
	}

	@NotifyChange({ "store" })
	public void setStore(String store) {
		this.store = store;
		BindUtils.postNotifyChange(null, null, this, "store");
	}
	
	@NotifyChange({ "listDataHeader", "selectedDataHeader" })
	@Command
	public void printRowsJrn() {		
		//Set<Journal> lstJrn = new LinkedHashSet<Journal>(invoiceService.getLstJrn(selectedDataHeader.getId()));
		//Set<Journal> lstJrn = new HashSet<Journal>(invoice.getListJournal(selectedDataHeader.getId()));
		
		/*boolean success = print.printJrn(lstJrn);
				
		if (success) {
			setMsgExportIdc(WebFrontEmeaSettings.getInstance().getPrinterNoSuccess());
		} else {
			setMsgExportIdc(WebFrontEmeaSettings.getInstance().getPrinterSuccess());	
		}*/
	}
	
	@NotifyChange({ "listDataHeader", "selectedDataHeader" })
	@Command
	public void searchHeaders() {
		Format dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		String dataBeg = "";
		String dataEnd = "";
		int maxTransaction = Integer.parseInt(WebFrontZkLabelsLocator.getLabel("web.base.functionalities.transactions.max.rows"));
		
		try {
			if (dateFrom != null && !dateFrom.equals("")) {				
				dataBeg = dateFormatter.format(dateFrom);						
			}
			
			if (dateTo != null && !dateTo.equals("")) {								
				dataEnd = dateFormatter.format(dateTo);				
			}
			
			if (terminal.equals("")) {
				terminal = "%20";
			}
			if (dataBeg.equals("")) {
				dataBeg = "%20";
			}
			if (dataEnd.equals("")) {
				dataEnd = "%20";
			}
			if (store.equals("")) {
				store = "%20";
			}
			if (receipt.equals("")) {
				receipt = "%20";
			}
			if (customer.equals("")) {
				customer = "%20";
			}

			
			List<DataHeader> lstHeaders = invoiceService.getTransactionsByParams(terminal, dataBeg, dataEnd, store, receipt, customer);
			//List<DataHeader> lstHeaders = invoice.getTransByParams(terminal, dataBeg, dataEnd, store, receipt, customer);
			
			if (lstHeaders.size() > maxTransaction) {
				Clients.showNotification(WebFrontEmeaSettings.getInstance().getMaxTransactionMessage());
			} else {
				listDataHeader = lstHeaders;
			}
		} catch (Exception e){
			log.error("", e);
			
			Clients.showNotification("Error during the search.\n" + e.getMessage());
		}
		
		BindUtils.postNotifyChange(null, null, this, "listDataHeader");
		
		selectedDataHeader = null;
	}
	
	@Command
	@NotifyChange({ "selectedDataHeader", "listDataCollect", "listJournal" })
	public void selectDataHeader() {
		selectedDataHeader.getIdc().clear();
		selectedDataHeader.getJrn().clear();
		
		setMsgExportIdc("");	
		setMsgExportJrn("");	
		
		if (selectedDataHeader.getFileName() != null && !selectedDataHeader.getFileName().trim().equals("")) {
			retentionHeaders.restoreIdcJrn(selectedDataHeader);
		}
		
		listDataCollect = new LinkedHashSet<DataCollect>(invoiceService.getLstIdc(selectedDataHeader.getId()));
		listJournal = new LinkedHashSet<Journal>(invoiceService.getLstJrn(selectedDataHeader.getId()));
		
		//listDataCollect = new LinkedHashSet<DataCollect>(invoice.getListDc(selectedDataHeader.getId()));
		//listJournal = new LinkedHashSet<Journal>(invoice.getListJournal(selectedDataHeader.getId()));

		//selectedDataHeader.setIdc(listDataCollect);
		//selectedDataHeader.setJrn(listJournal);
		
		
		List<Journal> lstJrn = new ArrayList<Journal>();		
		
		for (Journal jrn : listJournal) {
			Journal journal = new Journal();
			
			String row = recognizeArabicRow(jrn.getLine());
			
			journal.setId(jrn.getId());
			journal.setDataHeader(jrn.getDataHeader());
			journal.setLine(row);
			journal.setSequence(jrn.getSequence());
			
			lstJrn.add(journal);
		}
		
		listJournal = new LinkedHashSet<Journal>(lstJrn);
		
		selectedDataHeader.setIdc(listDataCollect);
		selectedDataHeader.setJrn(listJournal);
	}
	
	private String recognizeArabicRow(String row) {
		boolean isArabic = false;
		
	    for (int i = 0; i < row.length();) {
	        int c = row.codePointAt(i);
	        if (c >= 0x0600 && c <= 0x06E0) {
	            isArabic = true;
	            break;
	        }
	        
	        i += Character.charCount(c);            
	    }
	    
	    if (isArabic) {
	       row = leftFill(row, 39, "\u00a0", true);
	    }
	    
	    return row;
	}
	
    private static String leftFill(String s, int len, String prefix, boolean firstChars) {        
        int ind = s.trim().length() - len;

        if (ind >= 0) {
            if (firstChars) {                
                return s.substring(ind, ind + len);
            } else {                
                return s.substring(0, len);
            }
        }
        
        StringBuffer sb = new StringBuffer(len);

        while (ind++ < 0) {
            sb.append(prefix);
        }
        
        return sb.append(s).toString();
    }

	@Command
	@NotifyChange({ "selectedDataHeader" })
	public void retentionIdcJrn() {
	   retentionHeaders.readDbForRetention();
	}
	
	@NotifyChange({ "listDataHeader", "selectedDataHeader" })
	@Command
	public void exportIdcJrn() throws Exception {						
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss.SSS");
		Calendar cal = Calendar.getInstance();		 
		
		Set<DataCollect> lstIdc = new HashSet<DataCollect>(invoiceService.getLstIdc(selectedDataHeader.getId()));
		Set<Journal> lstJrn = new HashSet<Journal>(invoiceService.getLstJrn(selectedDataHeader.getId()));
		
		//Set<DataCollect> lstIdc = new HashSet<DataCollect>(invoice.getListDc(selectedDataHeader.getId()));
		//Set<Journal> lstJrn = new HashSet<Journal>(invoice.getListJournal(selectedDataHeader.getId()));

		today = dateFormat.format(cal.getTime());
		String fileIdcName = writeIdcFile(lstIdc, today);
		String fileJrnName = writeJrnFile(lstJrn, today);		
				
	    ArrayList<String> listEntry = new ArrayList<>();
	    listEntry.add(fileIdcName);
	    listEntry.add(fileJrnName);	 
	}

	private String writeIdcFile(Set<DataCollect> lstIdc, String cal) {
		String directoryFilesIdc = WebFrontEmeaSettings.getInstance().getPathIdcDirectory();
		BufferedWriter bw = null;
		
		String fileName = directoryFilesIdc + File.separator + "HOCIDC-" + cal + ".DAT";
		
		try {
			File fileIdc = new File(fileName);
			bw = new BufferedWriter(new FileWriter(fileIdc));
			
			for (DataCollect idc : lstIdc) {
				bw.write(idc.getLine() + "\r\n");
			}
			
			bw.close();
						
			setMsgExportIdc(WebFrontZkLabelsLocator.getLabel("web.base.functionalities.transactions.export.success.idc"));
		} catch(IOException e) {
			e.printStackTrace();
			setMsgExportIdc(WebFrontZkLabelsLocator.getLabel("web.base.functionalities.transactions.export.nosuccess.idc") + " " + e.getMessage());
			return "";
		}
		
		return fileName;
	}	

	private String writeJrnFile(Set<Journal> lstJrn, String cal) {
		String directoryFilesJrn = WebFrontEmeaSettings.getInstance().getPathJrnDirectory();
		BufferedWriter bw = null;
		String fileName = directoryFilesJrn + File.separator + "HOCJRN-" + cal + ".DAT";
				
		try {
			File fileJrn = new File(fileName);
			bw = new BufferedWriter(new FileWriter(fileJrn));
			
			for (Journal jrn : lstJrn) {
				bw.write(jrn.getLine() + "\r\n");
			}
			
			bw.close();
			
			setMsgExportJrn(WebFrontZkLabelsLocator.getLabel("web.base.functionalities.transactions.export.success.jrn"));			
		} catch(IOException e) {
			e.printStackTrace();
			setMsgExportJrn(WebFrontZkLabelsLocator.getLabel("web.base.functionalities.transactions.export.nosuccess.jrn") + " " + e.getMessage());			
			return "";
		} 	
		
		return fileName;
	}

	public Converter<String, Integer, Component> getCentsToStringCurrencyConverter() {
		return new ZkCentsToStringCurrencyConverter();
	}

	
	//Filtri
	/*
	public String getTitleFilter() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterTitle();
	}
	
	public String getLblStore() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterStore();
	}
	
	public String getLblTerminal() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterTerminal();
	}

	public String getLblReceipt() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterReceipt();
	}
	
	public String getLblCustomer() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterCustomer();
	}

	public String getLblDateFrom() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterDateFrom();
	}
	
	public String getLblDateTo() {
		return WebFrontEmeaSettings.getInstance().getHeadersFilterDateTo();
	}
	
	
	//Campi tabella
	public String getTblStore() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableStore();
	}
	
	public String getTblTerminal() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableTerminal();
	}

	public String getTblReceipt() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableReceipt();
	}
	
	public String getTblDate() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableDate();
	}
	
	public String getTblTime() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableTime();
	}

	public String getTblTotal() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableTotal();
	}

	public String getTblCustomer() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableCustomer();
	}

	public String getTblEod() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableEod();
	}

	public String getTblFileName() {
		return WebFrontEmeaSettings.getInstance().getHeadersTableFileName();
	}

	//tasti
	public String getButtonSearch() {
		return WebFrontEmeaSettings.getInstance().getHeadersButtonSearch();
	}
		
	public String getButtonExport() {		
		return WebFrontEmeaSettings.getInstance().getHeadersButtonExport();
	}
	
	public String getPrintJrn() {		
		return WebFrontEmeaSettings.getInstance().getHeadersButtonPrint();
	}
	
	public String getNoResultHeaders() {
		return WebFrontEmeaSettings.getInstance().getMsgNoResultHeaders();
	}

	public String getNoResultIdc() {		
		return WebFrontEmeaSettings.getInstance().getMsgNoResultIdc();	
	}
		
	public String getNoResultJournal() {	
		return WebFrontEmeaSettings.getInstance().getMsgNoResultJrn();
	}
		
	public String getTblJrn() {	
		return WebFrontEmeaSettings.getInstance().getTitleTableJrn();
	}
	
	public String getTblIdc() {	
		return WebFrontEmeaSettings.getInstance().getTitleTableIdc();
	}
	*/
	
	public DataHeader getSelectedDataHeader() {
		return selectedDataHeader;
	}

	public void setSelectedDataHeader(DataHeader selectedDataHeader) {
		this.selectedDataHeader = selectedDataHeader;
	}
	
	public String getMsgExportIdc() {
		return msgExportIdc;
	}
	
	@NotifyChange({ "msgExportIdc" })
	public void setMsgExportIdc(String msgExportIdc) {
		this.msgExportIdc = msgExportIdc;
		BindUtils.postNotifyChange(null, null, this, "msgExportIdc");
	}

	public String getMsgExportJrn() {
		return msgExportJrn;
	}
	
	@NotifyChange({ "msgExportJrn" })
	public void setMsgExportJrn(String msgExportJrn) {
		this.msgExportJrn = msgExportJrn;
		BindUtils.postNotifyChange(null, null, this, "msgExportJrn");
	}
}
