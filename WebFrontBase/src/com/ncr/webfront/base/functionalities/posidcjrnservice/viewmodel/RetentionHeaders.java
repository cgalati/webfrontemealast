package com.ncr.webfront.base.functionalities.posidcjrnservice.viewmodel;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.base.functionalities.posidcjrnservice.WebFrontEmeaSettings;

//import com.ncr.webfront.base.restservices.posidcjrnservice.PosIdcJrnServiceImplementation;
//import com.ncr.webfront.base.restservices.posidcjrnservice.PosIdcJrnServiceInterface;
import com.ncr.webfront.base.plugins.dbtest.PosIdcJrnServiceImplementation;
import com.ncr.webfront.base.posidcjrn.PosIdcJrnServiceInterface;
import com.ncr.webfront.base.posidcjrn.entities.*;
import com.ncr.webfront.base.restclient.posidcjrnservice.PosIdcJrnServiceClientImpl;
import com.google.gson.Gson;
import com.ncr.webfront.base.functionalities.posidcjrnservice.*;

public class RetentionHeaders {
	private final Logger logger = WebFrontLogger.getLogger(RetentionHeaders.class);	
	private PosIdcJrnServiceInterface service;	
	private final String COMPRESSED_FILEEXT = ".zip";
	private String fileNameZip = "";
	private String filePathCompressed = "";
	
	public RetentionHeaders() {
		service = new PosIdcJrnServiceClientImpl();
	}
	
	
	public PosIdcJrnServiceInterface getService() {
		return service;
	}

	public void setService(PosIdcJrnServiceInterface service) {
		this.service = service;
	}

	private String getToday() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss.SSS");
		Calendar cal = Calendar.getInstance();		 
		
		return dateFormat.format(cal.getTime());
	}	
	
	//cerco tutti i DATAHEADERS pi� vecchi di 60 gg per la retention

	public void readDbForRetention() {		
		int deleteIdcJrnDays = Integer.parseInt(WebFrontEmeaSettings.getInstance().getDeleteIdcJrnDays());
		int deleteHeadersDays = Integer.parseInt(WebFrontEmeaSettings.getInstance().getDeleteHeadersDays());	

		deleteOnDb(deleteHeadersDays, true);
		deleteOnDb(deleteIdcJrnDays, false);
	}	
	
	private boolean deleteOnDb(int days, boolean cancelAll) {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();		 
			cal.add(Calendar.DATE, -days); 
			
			List<DataHeader> lstDataHeader = service.getTransactionsByParams("", "", dateFormat.format(cal.getTime()), "", "", "");
			//List<DataHeader> lstDataHeader = service.getTransByParams("", "", dateFormat.format(cal.getTime()), "", "", "");
	
			for (DataHeader currentHeader : lstDataHeader) {
				if (cancelAll) {
					deleteDbIdcJrn(currentHeader.getIdc(), currentHeader.getJrn());
					
					service.deleteAll(currentHeader.getId());
					//service.cancelAll(currentHeader);
					if (currentHeader.getFileName() != null && !currentHeader.getFileName().trim().equals("")) {
						deleteFile(WebFrontEmeaSettings.getInstance().getPathZipDirectory() + File.separator + currentHeader.getFileName());	
					}					
				} else {
					writeFiles(currentHeader.getId());
				}				
			}
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	//scrive i relativi files idc e jrn
	private void writeFiles(int idDataHeader) {
		Set<DataCollect> lstIdc = new HashSet<DataCollect>(service.getLstIdc(idDataHeader));
		Set<Journal> lstJrn = new HashSet<Journal>(service.getLstJrn(idDataHeader));

		//Set<DataCollect> lstIdc = new HashSet<DataCollect>(service.getListDc(idDataHeader));
		//Set<Journal> lstJrn = new HashSet<Journal>(service.getListJournal(idDataHeader));

		
		String fileIdcName = writeIdcFile(lstIdc, getToday());
		String fileJrnName = writeJrnFile(lstJrn, getToday());		
				
	    ArrayList<String> listEntry = new ArrayList<>();
	    listEntry.add(fileIdcName);
	    listEntry.add(fileJrnName);	 	    
	    
	    //una volta scritti i files, questi verranno messi nella directory zippata
	    boolean success = retentionIdcJrn(listEntry);
	    
	    //cancello i dati dalle tabelle idc e jrn
	    //inserisco nel campo FILENAME il nome della directory zippata
	    if (success) {
	    	deleteDbIdcJrn(lstIdc, lstJrn);	
	    	service.updateFileName(fileNameZip, idDataHeader);
	    	//service.changeFileName(fileNameZip, idDataHeader);
	    }	    
	}
	
	//cancello i dati dalle tabelle idc e jrn
	private void deleteDbIdcJrn(Set<DataCollect> lstIdc, Set<Journal> lstJrn) {
		for (DataCollect idc : lstIdc) {
			service.deleteIdcJrn(idc.getId(), "DataCollect");
			//service.cancelIdcJrn(idc.getId(), "DataCollect");
		}

		for (Journal jrn : lstJrn) {
			service.deleteIdcJrn(jrn.getId(), "Journal");
			//service.cancelIdcJrn(jrn.getId(), "Journal");
		}
	}
		
	private String writeIdcFile(Set<DataCollect> lstIdc, String cal) {
		String directoryFilesIdc = WebFrontEmeaSettings.getInstance().getPathIdcDirectory();
		BufferedWriter bw = null;
		
		String fileName = directoryFilesIdc + File.separator + "HOCIDC-" + cal + ".DAT";
		
		try {
			File fileIdc = new File(fileName);
			bw = new BufferedWriter(new FileWriter(fileIdc));
			
			for (DataCollect idc : lstIdc) {
				bw.write(idc.getLine() + "\r\n");
			}
			
			bw.close();
		} catch(IOException e) {
			e.printStackTrace();
			return "";
		}
		
		return fileName;
	}
	

	private String writeJrnFile(Set<Journal> lstJrn, String cal) {
		String directoryFilesJrn = WebFrontEmeaSettings.getInstance().getPathJrnDirectory();
		BufferedWriter bw = null;
		String fileName = directoryFilesJrn + File.separator + "HOCJRN-" + cal + ".DAT";
				
		try {
			File fileJrn = new File(fileName);
			bw = new BufferedWriter(new FileWriter(fileJrn));
			
			for (Journal jrn : lstJrn) {
				bw.write(jrn.getLine() + "\r\n");
			}
			
			bw.close();
		} catch(IOException e) {
			e.printStackTrace();
			
			return "";
		} 	
		
		return fileName;
	}

	public boolean retentionIdcJrn(ArrayList<String> lstFilesToCompress) {
		String compressedDirectory = WebFrontEmeaSettings.getInstance().getPathZipDirectory();
		
		if (compressedDirectory != null) {
			File dirZip = new File(compressedDirectory);
			
			if (!dirZip.exists() || !dirZip.isDirectory()) {
				dirZip.mkdir();
			}
			
			return creaZip(compressedDirectory, lstFilesToCompress);			
		}
		
		return false;
	}	

	//creo la cartella zippata con i files idc e jrn, che poi andranno cancellati
	private boolean creaZip(String pathDirectoryZip, final ArrayList<String> listaEntry) {
		fileNameZip = "HOCIDCJRN-" + getToday() + COMPRESSED_FILEEXT;
		filePathCompressed = pathDirectoryZip + File.separator + fileNameZip;
		File f = new File(filePathCompressed);
		
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(f);
			ZipOutputStream out = new ZipOutputStream(fileOutputStream);
			//GZIPOutputStream out = new GZIPOutputStream(fileOutputStream);
			
			for (String zipEntry : listaEntry) {
			    addToZipFile(zipEntry, out);
			    deleteFile(zipEntry);
			}
			out.close();
			fileOutputStream.close();
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	private static void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {
		byte[] bytes = new byte[1024];
		int length;
		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		
		zos.putNextEntry(zipEntry);
		
		while ((length = fis.read(bytes)) >= 0) {
		    zos.write(bytes, 0, length);
		}
				
		zos.closeEntry();
		fis.close();
    }	
	
	private void deleteFile(String zipEntry) {
		try {
			File file = new File(zipEntry);
			file.delete();			
		} catch(Exception e) {
			
		}
	}

	//questa viene richiamata quando riseleziono su un DATAHEADER con campo fileName valorizzato (quindi � stata fatta la retention)
	public void restoreIdcJrn(DataHeader selectedDataHeader) {
		String compressedDirectory = WebFrontEmeaSettings.getInstance().getPathZipDirectory();
		String dirZip = compressedDirectory + File.separator + selectedDataHeader.getFileName();
		String dirUnzip = compressedDirectory + File.separator + "UNZIP";
		
		//unzippo la directory
		boolean successUnzip = unzipDirectory(dirZip, dirUnzip);
		
		if (successUnzip) {
			//leggo il contenuto dei files, che andranno a ripopolare le tabelle
			readFileToAddInTable(dirZip, dirUnzip, selectedDataHeader);			
		}
	}

	public static boolean unzipDirectory(String dirZip, String dirUnzip) {		
		int BUFSIZE = 2048;
		ZipInputStream zipFile = null;
		BufferedOutputStream bos = null;
		
		File src = new File(dirZip);
		File dirOut = new File(dirUnzip);
		
		try {	
			if (!dirOut.exists() || !dirOut.isDirectory()) {
				if (!dirOut.mkdirs()) {
					return false;
				}
			}
	
			zipFile = new ZipInputStream(new FileInputStream(src));	
			ZipEntry entry = null;
			
			while((entry = zipFile.getNextEntry()) != null) {
				String entryName = entry.getName();				

				int numBytes = 0;
				byte[] data = new byte[BUFSIZE];
				File entryOut = new File(dirOut, entryName);
				FileOutputStream fos = new FileOutputStream(entryOut);
				bos = new BufferedOutputStream(fos, BUFSIZE);
	
				while((numBytes = zipFile.read(data, 0, BUFSIZE)) != -1) {
					bos.write(data, 0, numBytes);
				}
				
				bos.flush();
				bos.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
			
			return false;
		} finally {
			if (zipFile != null) {
				try {
					zipFile.close();
				} catch (IOException ioe) {					
					ioe.printStackTrace(); 
					return false;
				}
			}
		}
		
		return true;
	}
	
	private boolean readFileToAddInTable(String zipDirectory, String unzipDirectory, DataHeader selectedDataHeader) {
		try {
			File dirUnzip = new File(unzipDirectory);
			
			if (!dirUnzip.exists() || !dirUnzip.isDirectory()) {
				return false;
			}
			
			for(File file : dirUnzip.listFiles()) {
				String fileName = file.getName();

				BufferedReader br = new BufferedReader(new FileReader(file));
				String line = "";
				int i = 1;
				
				while((line = br.readLine()) != null) {
					if (fileName.indexOf("HOCIDC") >= 0) {
						DataCollect dataCollect = new DataCollect();
						
						dataCollect.setDataHeader(selectedDataHeader);
						dataCollect.setLine(line);
						dataCollect.setSequence(i);
						
						Gson gson = new Gson();
						String jsonIdc = gson.toJson(dataCollect);
						
						service.addIdc(jsonIdc);
						//service.addDc(dataCollect);
					} else {
						Journal journal = new Journal();
						
						journal.setDataHeader(selectedDataHeader);
						journal.setLine(line);
						journal.setSequence(i);
						
						Gson gson = new Gson();
						String jsonJrn = gson.toJson(journal);
						
						service.addJrn(jsonJrn);
						//service.addJournal(journal);
					}
					
					i++;
				}
				
				br.close();
				file.delete();
				service.updateFileName("", selectedDataHeader.getId());
				//service.changeFileName("", selectedDataHeader.getId());
			}
		} catch(Exception e) {
			return false;
		}
		
		deleteFile(zipDirectory);
		return true;
	}
}
