package com.ncr.webfront.base.functionalities.posidcjrnservice.viewmodel;



import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.event.PrintJobEvent;
import javax.print.event.PrintJobListener;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/*import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
*/
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.base.functionalities.posidcjrnservice.WebFrontEmeaSettings;
import com.ncr.webfront.base.posidcjrn.entities.Journal;


public class PrintJournal {
	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private boolean failed = false;
	private PrintService[] printServices;
	private PrintService designatedService;
	private PrintJobListener listener;
	private static final int WIDTH = 595; 
	private static final int FONT_SIZE = 10; 
	private static final int LINE_HEIGHT = 16; 	
	private PrinterJob job1;
	private String directoryFilesJrn = "";
	private String fileTempName = "";
	private String filePdfName = "";

	
	public boolean printJrn(Set<Journal> lstJrn) {
		logger.info("Enter function print");
		String printerNameDefault = WebFrontEmeaSettings.getInstance().getPrinterName();
		
		try {
			directoryFilesJrn = WebFrontEmeaSettings.getInstance().getPathJrnDirectory();
			
			logger.info("Creo il file temporaneo");
			createFileTemp(lstJrn);
	
			logger.info("Ricopio il file temporaneo in un file pdf, per poterlo stampare");
			createFilePdfTemp();
			
			DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
			designatedService = null;
			printServices = PrintServiceLookup.lookupPrintServices(flavor, null);

			for (int i = 0; i < printServices.length; i++) {
				if (printServices[i].getName().equalsIgnoreCase(printerNameDefault)) {
					logger.info("La stampante di default � tra quelle riconosciute dal sistema");
					designatedService = printServices[i];

					break;
				}
			}

			listener = new PrintJobListener() {

				public void printDataTransferCompleted(PrintJobEvent pje) {
					logger.info("listener - Enter function printDataTransferCompleted");
					failed = false;
				}

				public void printJobCompleted(PrintJobEvent pje) {
					logger.info("listener - Enter function printJobCompleted");
					failed = false;
				}

				public void printJobFailed(PrintJobEvent pje) {
					logger.info("listener - Enter function printJobFailed");
					failed = true;
				}

				public void printJobCanceled(PrintJobEvent pje) {
					logger.info("listener - Enter function printJobCanceled");
					failed = true;
				}

				public void printJobNoMoreEvents(PrintJobEvent pje) {
					logger.info("listener - Enter function printJobNoMoreEvents");
					failed = false;
				}

				public void printJobRequiresAttention(PrintJobEvent pje) {
					logger.info("listener - Enter function printJobRequiresAttention");
					failed = false;
				}
			};
			
			if (designatedService != null) { 		
				PDDocument doc = PDDocument.load(directoryFilesJrn + File.separator + "temp" + File.separator + "JRNTOPRINT.pdf");
				
				job1 = PrinterJob.getPrinterJob();
				job1.setPrintService(designatedService);
				
				doc.silentPrint(job1);	
			} else {
				logger.info("Stampa comande FALLITA");
				failed = true;
			}
		} catch (Exception e) {
			logger.error("Eccezione durante la stampa delle comande - ripulisco la coda di stampa - " + e.getMessage());
			
			failed = true;
			job1.cancel();			
		}

		deleteFile(fileTempName);
		deleteFile(filePdfName);
				
		logger.info("Exit function print - failed = " + failed);
		
		return failed;
	}

	
	private String createFileTemp(Set<Journal> lstJrn) {		
		BufferedWriter bw = null;
		String directoryTemp = directoryFilesJrn + File.separator + "temp";
	    fileTempName = directoryTemp + File.separator + "JRNTOPRINT.tmp";				
		
		try {
			File dirTemp = new File(directoryTemp);
			
			if (!dirTemp.exists()) {
				dirTemp.mkdirs();
			}
			
			File fileJrn = new File(fileTempName);
			bw = new BufferedWriter(new FileWriter(fileJrn));
			
			for (Journal jrn : lstJrn) {
				bw.write(jrn.getLine() + "\r\n");
			}
			
			bw.close();
		} catch(IOException e) {			
			e.printStackTrace();
			return "";
		} 	
		
		return fileTempName;
	}
	
	private void createFilePdfTemp() {
        logger.info("Enter function createFilePdfTemp");
        
        filePdfName = directoryFilesJrn + File.separator + "temp" + File.separator + "JRNTOPRINT.tmp";

		try {
			FileReader fileTmp = new FileReader(new File(filePdfName));
			BufferedReader br = new BufferedReader(fileTmp);
			List<String> listLine = new ArrayList<String>();
			String line = "";

			while((line = br.readLine()) != null) {
				listLine.add(line);
			}
			
			br.close();
			fileTmp.close();
				
			int height = 842;
			
			PDRectangle rec = new PDRectangle(WIDTH, height);
			PDPage page = new PDPage(rec);

			PDDocument doc = new PDDocument();
	        doc.addPage(page);
           
            PDFont fontNormal = PDType1Font.HELVETICA;

            PDPageContentStream content = new PDPageContentStream(doc, page);
            content.beginText();
            content.setFont(fontNormal, FONT_SIZE);
            height-= 120;
            content.moveTextPositionByAmount(30, height+70);
                      
            for (String lineFile : listLine) {
        		content.setFont(fontNormal, FONT_SIZE);            	
            	content.drawString(lineFile);
            	content.moveTextPositionByAmount(0, -LINE_HEIGHT);
			}         
            
            content.endText();
            content.close();
            
            String filename = directoryFilesJrn + File.separator + "temp" + File.separator + "JRNTOPRINT.pdf";
            
			doc.save(filename);

			doc.close();			
		} catch (Exception e) {
			logger.error("Eccezione durante la scrittura nel file temporaneo in pdf - "
					+ e.getMessage());
			System.out.println(e);			
		}
	}
	
	private void deleteFile(String zipEntry) {
		try {
			File file = new File(zipEntry);
			file.delete();			
		} catch(Exception e) {
			
		}
	}
}







