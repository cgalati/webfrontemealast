package com.ncr.webfront.base.functionalities.endofdayhistory;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.TreeModel;
import org.zkoss.zul.TreeNode;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.EodServiceInterface;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.EodStatusObject.EodStatus;
import com.ncr.webfront.base.functionalities.endofday.EndOfDayMappingProperties;
import com.ncr.webfront.base.restclient.eodservice.EodServiceClientImpl;
import com.ncr.webfront.base.utils.EndOfDayStatusTreeComposer;
import com.ncr.webfront.base.utils.EndOfDayStatusTreeRenderer;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.language.WebFrontZkLabelsLocator;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.zkconverters.DateConverter;

public class EndOfDayHistoryViewModel {

	@Init
	@NotifyChange({ "selectedEodStatusHistory", "eodStatusHistoryRecordSelected", "eodTree" })
	public void init(@ContextParam(ContextType.COMPONENT) Component component, @ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {

		List<EodStatusObject> history = null;

		if (renderer == null) {
			String endOfDayTreeNodeStyle = EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeStyle();
			String endOfDayTreeNodeWarningStyle = EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeWarninngStyle();
			String endOfDayTreeNodeErrorStyle = EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeErrorStyle();
			logger.info("EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeStyle() = \"" + endOfDayTreeNodeStyle + "\"");
			logger.info("EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeWarningStyle() = \"" + endOfDayTreeNodeWarningStyle + "\"");
			logger.info("EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeErrorStyle() = \"" + endOfDayTreeNodeErrorStyle + "\"");
			renderer = new EndOfDayStatusTreeRenderer(endOfDayTreeNodeStyle, endOfDayTreeNodeWarningStyle,endOfDayTreeNodeErrorStyle);
		}


		ErrorObject errorObject = eodService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("EodService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}

		logger.debug("retrieving eod status history");
		try {
			history = eodService.getEodStatusHistory();
		} catch (Exception e) {
			history = new ArrayList<>();
		}
		logger.debug(String.format("[%d] eod found", history.size()));

		if (history.isEmpty()) {
			Clients.showNotification("Connection problems with the EOD socket server");
			return;
		}

		setSelectedEodStatusHistory(history.get(0));
		showSelectedEod();
	}

	public List<EodStatusObject> getEodStatusHistoryList() {
		return eodService.getEodStatusHistory();
	}

	public boolean isEodStatusHistoryRecordSelected() {
		return (selectedEodStatusHistory != null);
	}

	@NotifyChange({ "selectedEodStatusHistory", "eodStatusHistoryRecordSelected", "eodTree" })
	public void setSelectedEodStatusHistory(EodStatusObject selectedEod) {
		logger.debug("Selected!!");
		System.out.println("Selected!!");
		this.selectedEodStatusHistory = selectedEod;
		showSelectedEod();
	}

	public EodStatusObject getSelectedEodStatusHistory() {
		return selectedEodStatusHistory;
	}
	public String getSelectedEodStatusHistoryDescription() {
		return getEodStatusLabel (selectedEodStatusHistory.getStatus());
	}

	public void showSelectedEod() {
		if (selectedEodStatusHistory == null) {
			return;
		}
		try {
			eodTree = EndOfDayStatusTreeComposer.composeTwoLevels(null, selectedEodStatusHistory.getEodSteps(), true);
		} catch (Exception e) {
			logger.error("", e);
			Clients.showNotification("Connection problems with the EOD socket server");
		}
	}

	public TreeModel<TreeNode<String>> getEodTree() {
		return eodTree;
	}

	public DateConverter getDateConverter() {
		return new DateConverter();
	}

	public EndOfDayStatusTreeRenderer getTreeRenderer() {
		return renderer;
	}
	
	public String getTreeHeight() {
		final int DEFAULT_HEIGTH = 410;
		int treeHeight = DEFAULT_HEIGTH;
		try {
			int screenHeight = (int) Executions.getCurrent().getDesktop().getAttribute("height");
			logger.info("screenHeight = " + screenHeight);
			treeHeight = screenHeight - 200;
			logger.info("treeHeight = " + treeHeight);
		} catch (Exception e) {
			logger.info("exception!", e);
			treeHeight = DEFAULT_HEIGTH;
		}

		logger.info("treeHeight = " + treeHeight);
		return treeHeight + "px";
	}
	private String getEodStatusLabel(EodStatus eodStatus) {
		try {
			String label = WebFrontZkLabelsLocator.getLabel("com.ncr.webfront.base.eod.EodStatusObject.EodStatus." + eodStatus.name());
			return label;
		} catch (Exception e) {
			return eodStatus.name();
		} 
	}	
	
	private TreeModel<TreeNode<String>> eodTree;
	private EodServiceInterface eodService = new EodServiceClientImpl();
	private EodStatusObject selectedEodStatusHistory;
	private EndOfDayStatusTreeRenderer renderer;


	private static final Logger logger = WebFrontLogger.getLogger(EndOfDayHistoryViewModel.class);
}
