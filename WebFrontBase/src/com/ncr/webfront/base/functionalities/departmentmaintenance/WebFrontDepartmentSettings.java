package com.ncr.webfront.base.functionalities.departmentmaintenance;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontDepartmentSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontDepartmentSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontDepartmentSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontDepartmentSettings("departmentmaintenance.properties");
		}
		return instance;
	}

	protected WebFrontDepartmentSettings(String propertyFileName) {
		super(propertyFileName);
	}

	// TODO test settings

	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.list-title", "department list");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getCode() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.code", "Code");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getMainCode() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.maincode", "Main Code");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDescription() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.description", "Description");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDetails() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.details", "Details");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getSearch() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.search", "Search");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getAdd() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.add", "Add");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getEdit() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.edit", "Edit");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDelete() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.delete", "Delete");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDepartment() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.department", "Department");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getConfirm() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.confirm", "Confirm");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getAnnul() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.annul", "Annul");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDeleteAll() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.deleteall", "Delete All");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getNoPlu() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.noplu", "No Plu");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getAction() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.action", "Action");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getNoResults() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.departmentmaintenance.noresults", "No department in the results list");
		logger.debug("END (" + value + ")");
		return value;
	}	
}
