package com.ncr.webfront.base.functionalities.departmentmaintenance;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.XulElement;

import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.functionalities.posidcjrnservice.WebFrontEmeaSettings;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.restclient.articleservice.ArticleServiceClientImpl;
import com.ncr.webfront.base.restclient.commonlists.CommonListsServiceClientImpl;
import com.ncr.webfront.base.restclient.posdepartmentservice.PosDepartmentServiceClientImpl;
import com.ncr.webfront.base.restclient.tareservice.TareServiceClientImpl;
import com.ncr.webfront.base.restclient.vatservice.VatServiceClientImpl;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.WebFrontCore;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.login.LoginManagerInterface;
import com.ncr.webfront.core.utils.commonlists.CommonListsServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.zkconverters.ZkCentsToStringCurrencyConverter;
import com.ncr.webfront.eventlogger.commondata.data.WebFrontEvent;
import com.ncr.webfront.eventlogger.plugin.EventLoggerService;



public class DepartmentMaintenanceViewModel {
	private final Logger logger = WebFrontLogger.getLogger(DepartmentMaintenanceViewModel.class);
	
	private Properties propDepartment = new Properties();
	private String departmentCodeSearchField = "";	
	private String mainCodeSearchField = "";
	private String descriptionSearchField = "";
	private String departmentCodeField;
	private List<String> tempDepartmentCodes = new ArrayList<String>();
	private List<PosDepartment> departmentList = new ArrayList<PosDepartment>();
	private PosDepartment selectedDepartment;
	
	private ErrorObject eo;
	private boolean readOnly = false;
	private boolean displayEdit = false;
	private boolean displayAdd = false;
	private boolean displayDetailsEdit = false;
	private PosDepartmentServiceInterface departmentService = new PosDepartmentServiceClientImpl();
	private Map<String, Boolean> capabilities;
	private String code="";
	private String maincode="";
	private String description="";

	
	
	
	/*@Init
	public void init(@ContextParam(ContextType.COMPONENT) Component component, @ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {

		logger.info("firstArgument = \"" + firstArgument + "\"");
		if (firstArgument != null && firstArgument.equals("#readOnly")) {
			this.readOnly = true;
			MainComposer.setFunctionTitle((XulElement) component, "Search Department");
		}
		
		ErrorObject errorObject = departmentService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("ArticleService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}		
	}*/
	
	public List<PosDepartment> getDepartmentList() {	
		return departmentList;
	}

	public String getAdd() {
		return WebFrontDepartmentSettings.getInstance().getAdd();
	}

	//@NotifyChange({ "departmentCodeSearchField", "mainCodeSearchField", "descriptionSearchField" })
	@NotifyChange({ "departmentCodeSearchField" })
	public void setDepartmentCodeSearchField(String departmentCodeSearchField) {
		this.departmentCodeSearchField = departmentCodeSearchField;
		//mainCodeSearchField = 0L;
		//descriptionSearchField = "";		
	}

	public String getDepartmentCodeSearchField() {
		return departmentCodeSearchField;
	}
	
	//@NotifyChange({ "departmentCodeSearchField", "mainCodeSearchField", "descriptionSearchField" })
	@NotifyChange({ "mainCodeSearchField" })
	public void setMainCodeSearchField(String mainCodeSearchField) {
		this.mainCodeSearchField = mainCodeSearchField;
		//departmentCodeSearchField = 0L;
		//descriptionSearchField = "";		
	}

	public String getMainCodeSearchField() {
		return mainCodeSearchField;
	}

	//@NotifyChange({ "departmentCodeSearchField", "mainCodeSearchField", "descriptionSearchField" })
	@NotifyChange({ "descriptionSearchField" })
	public void setDescriptionSearchField(String descriptionSearchField) {
		this.descriptionSearchField = descriptionSearchField;
		//departmentCodeSearchField = 0L;
		//mainCodeSearchField = 0L;		
	}

	public String getDescriptionSearchField() {
		return descriptionSearchField;
	}

	private void reset() {
		departmentCodeSearchField = "";
		mainCodeSearchField = "";
		descriptionSearchField = "";
		//departmentList = new ArrayList<PosDepartment>();
	}

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
		BindUtils.postNotifyChange(null, null, this, "displayEdit");
		WebFrontCore.setCurrentFunctionModfied(displayEdit || displayAdd);
	}

	public boolean isDisplayAdd() {
		return displayAdd;
	}

	public void setDisplayAdd(boolean displayAdd) {
		this.displayAdd = displayAdd;
		BindUtils.postNotifyChange(null, null, this, "displayAdd");
		WebFrontCore.setCurrentFunctionModfied(displayEdit || displayAdd);
	}

	public ErrorObject createDepartment() {
		logger.debug("Begin createDepartment");

		PosDepartment department = new PosDepartment();

		department.setCode("");
		department.setDescription("");
		
		boolean find = false;
		
		if (code != null) {
			List<PosDepartment> lstDep = departmentService.getAll();

			for (PosDepartment dep : lstDep) {
				if (dep.getCode().equals(code)) {
					find = true;
					break;
				}
			}
		
			if (!find) {
				if (code.length() > 4) {
					return new ErrorObject(ErrorCode.GENERIC_ERROR, "the code has too many characters");
				}
				if (maincode.length() > 4) {
					return new ErrorObject(ErrorCode.GENERIC_ERROR, "the maincode has too many characters");
				}
				if (description.length() > 20) {
					return new ErrorObject(ErrorCode.GENERIC_ERROR, "the description has too many characters");
				}
				
				department.setCode(code);
				department.setDescription(description);
				department.setTotal(maincode);
				
				departmentService.addDepartment(department);
				return new ErrorObject(ErrorCode.OK_NO_ERROR, "Department successfully inserted");
			}
		}

		logger.debug("End createDepartment");
		return new ErrorObject(ErrorCode.GENERIC_ERROR, "The code of department already exists");
	}
	
	@NotifyChange({ "code" })
	public void setCode(String code) {
		this.code = code;
	}
	
	@NotifyChange({ "maincode" })
	public void setMaincode(String maincode) {
		this.maincode = maincode;
	}
	
	@NotifyChange({ "description" })
	public void setDescription(String description) {
		this.description = description;
	}


	public String getCode() {
		return code;
	}
	
	public String getMaincode() {
		return maincode;
	}
	
	public String getDescription() {
		return description;
	}

	@Command
	@NotifyChange("departmentList")
	public void search() {		
		// Cleaning
		setSelectedDepartment(null);
		departmentList.clear();
		
		List<PosDepartment> lstDepartment = departmentService.getAll();

		try {
			if (departmentCodeSearchField.length() == 0 && mainCodeSearchField.length() == 0 && descriptionSearchField.length() == 0) {
				departmentList = lstDepartment;
				return;
			}
		} catch (Exception e) {
			departmentList = lstDepartment;
			return;
		}
				
		if (departmentCodeSearchField != null && departmentCodeSearchField.length() > 0) {
			for (PosDepartment dep : lstDepartment) {
				if (dep.getCode().trim().startsWith(departmentCodeSearchField.trim())) {
					departmentList.add(dep);	
				}
			}
			reset();
			return;
		}

		if (mainCodeSearchField != null && mainCodeSearchField.length() > 0) {
			for (PosDepartment dep : lstDepartment) {
				if (String.valueOf(dep.getLevel()).startsWith(mainCodeSearchField.trim())) {
					departmentList.add(dep);					
				}
			}
			reset();
			return;
		}

		if (descriptionSearchField != null && descriptionSearchField.length() > 0) {
			for (PosDepartment dep : lstDepartment) {
				if (dep.getDescription().trim().startsWith(descriptionSearchField.trim())) {
					departmentList.add(dep);
				}
			}
			reset();
			return;
		}
	}

	/*@Command
	public void edit() {
		logger.debug("Starting update to the following department: " + selectedDepartment.toString());
		
		final Object _this = this;

		Messagebox.show("Do you confirm editing of item with code: " + selectedDepartment.getCode() + " ?", "Confirm editing", Messagebox.YES
		      | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
			public void onEvent(Event evt) throws InterruptedException {
				if (evt.getName().equals("onYes")) {
					eo = updateDep();
					if (!showError()) {
						BindUtils.postNotifyChange(null, null, _this, "departmentList");

						// Cleaning
						setSelectedDepartment(null);
						BindUtils.postNotifyChange(null, null, _this, "selectedDepartment");
					}
				}
			}
		});
				
		setDisplayEdit(true);
		setDisplayAdd(false);
	}*/
	
	private ErrorObject updateDep() {
		if (selectedDepartment != null) {
			
			List<PosDepartment> lstDep = departmentService.getAll();

			for (PosDepartment dep : lstDep) {
				if (dep.getCode().equals(String.valueOf(selectedDepartment.getCode()))) {
		
					if (selectedDepartment.getDescription() != null) {
						if (selectedDepartment.getDescription().trim().length() > 20) {
							return new ErrorObject(ErrorCode.GENERIC_ERROR, "the description has too many characters");
						}
						dep.setDescription(selectedDepartment.getDescription());	
					}
					
					if (selectedDepartment.getTotal() != null) {
						if (selectedDepartment.getTotal().trim().length() > 4) {
							return new ErrorObject(ErrorCode.GENERIC_ERROR, "the maincode has too many characters");
						}
						dep.setTotal(String.valueOf(selectedDepartment.getTotal()));	
					}
					
					return departmentService.updateDepartment(dep);					
				}
			}
		}
		
		return new ErrorObject(ErrorCode.GENERIC_ERROR, "The department code is wrong");
	}

	@Command
	public void delete() {
		final Object _this = this;

		Messagebox.show("confirm the deletion of item with code: " + selectedDepartment.getCode() + " ?", "Confirm deletion", Messagebox.YES
		      | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
			public void onEvent(Event evt) throws InterruptedException {
				if (evt.getName().equals("onYes")) {
					eo = departmentService.deleteDepartment(selectedDepartment.getCode());
					if (!showError()) {
						// Refreshing view
						if (departmentList.indexOf(selectedDepartment) > -1) {
							departmentList.remove(departmentList.indexOf(selectedDepartment));
						}
						BindUtils.postNotifyChange(null, null, _this, "departmentList");

						// Cleaning
						setSelectedDepartment(null);
						BindUtils.postNotifyChange(null, null, _this, "selectedDepartment");
					}
				}
			}
		});
	}
	
	@Command
	@NotifyChange({ "selectedDepartment", "departmentList" })
	public void confirm() {
		if (displayEdit) {
			logger.debug("Confirming the change of the following department: " + selectedDepartment.toString());
			eo = updateDep();
		} else {
			logger.debug("Confirming insertion of the following department: " + selectedDepartment.toString());
			eo = createDepartment();
		}
		//if (!showError()) {
			setDisplayEdit(false);
			setDisplayAdd(false);
		//}
	}
	
	@Command
	public void cancel() {
		if (displayEdit) {
			setDisplayEdit(false);
			setSelectedDepartment(departmentService.getByCode(selectedDepartment.getCode()));
		} else {
			setDisplayAdd(false);
			setSelectedDepartment(null);
		}
	}
	

	private boolean showError() {
		boolean isInError = false;
		if (eo != null && eo.getDescription() != null && eo.getDescription().length() > 0) {
			if (eo.getErrorCode().equals(ErrorCode.OK_NO_ERROR)) {
				Messagebox.show(eo.getDescription(), "Information", Messagebox.OK, Messagebox.INFORMATION);
			} else if (eo.getErrorCode().equals(ErrorCode.OK_WARNING)) {
				Messagebox.show(eo.getDescription(), "Attention", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				Messagebox.show(eo.getDescription(), "Error", Messagebox.OK, Messagebox.ERROR);
				isInError = true;
			}
		}
		return isInError;
	}
	
	public PosDepartment getSelectedDepartment() {
		//setDisplayEdit(true);
		return selectedDepartment;
	}
	
	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

/*	
	public String getTitlePage() {
		return WebFrontDepartmentSettings.getInstance().getTitlePage();
	}
	
	public String getCode() {
		return WebFrontDepartmentSettings.getInstance().getCode();	
	}

	public String getMainCode() {
		return WebFrontDepartmentSettings.getInstance().getMainCode();	
	}

	public String getDescription() {
		return WebFrontDepartmentSettings.getInstance().getDescription();
	}

	public String getSearch() {
		return WebFrontDepartmentSettings.getInstance().getSearch();
	}
	
	public String getNoResults() {
		return WebFrontDepartmentSettings.getInstance().getNoResults();
	}
	
	public String getDetails() {
		return WebFrontDepartmentSettings.getInstance().getDetails();
	}

	public String getDepartment() {
		return WebFrontDepartmentSettings.getInstance().getDepartment();
	}

	public String getEdit() {
		return WebFrontDepartmentSettings.getInstance().getEdit();
	}

	public String getDelete() {
		return WebFrontDepartmentSettings.getInstance().getDelete();
	}

	public String getConfirm() {
		return WebFrontDepartmentSettings.getInstance().getConfirm();
	}
	
	public String getAnnul() {
		return WebFrontDepartmentSettings.getInstance().getAnnul();
	}
	
	public String getAction() {
		return WebFrontDepartmentSettings.getInstance().getAction();
	}

	public String getDeleteAll() {
		return WebFrontDepartmentSettings.getInstance().getDeleteAll();
	}
*/
	
	@Command
	@NotifyChange("departmentList")
	public void add() {
		setSelectedDepartment(departmentService.createDepartment());

		setDisplayEdit(false);
		setDisplayAdd(true);
	}

	@NotifyChange({ "setSelectedDepartment" })
	public void setSelectedDepartment(PosDepartment selectedDepartment) {
		this.selectedDepartment = selectedDepartment;
		BindUtils.postNotifyChange(null, null, this, "selectedDepartment");
	}
	
	@Command
	public void edit() {
		setDisplayEdit(true);
		setDisplayAdd(false);
	}
}
