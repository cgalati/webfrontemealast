package com.ncr.webfront.base.functionalities.invoicecustomermaintenance;

import java.util.List;

import org.zkoss.bind.ValidationContext;

import com.ncr.webfront.base.restclient.invoicecreditnoteservice.InvoiceCreditNoteServiceClientImpl;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;
import com.ncr.webfront.core.utils.validation.WebFrontValidator;

public class CustomerValidator extends WebFrontValidator {
	@Override
	protected Object getBean(ValidationContext context) {
		return context.getValidatorArg("customerToValidate");
	}
	
	@Override
	protected List<WebFrontValidationData> getValidationData() {
		return new InvoiceCreditNoteServiceClientImpl().getCustomerValidationData();
	}
}
