package com.ncr.webfront.base.functionalities.invoicecustomermaintenance;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.XulElement;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerType;
import com.ncr.webfront.base.invoicecreditnote.customer.Exemption;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.restclient.invoicecreditnoteservice.InvoiceCreditNoteServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.WebFrontCore;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class InvoiceCustomerMaintenanceViewModel {
	@Init
	public void init(@ContextParam(ContextType.COMPONENT) Component component) {
		ErrorObject errorObject = invoiceCreditNoteService.checkService();
		
		if ((errorObject == null) || errorObject.getErrorCode().isOk()) {
			postInit(component);
		} else {
			MainComposer.notifyServiceProblems("InvoiceCustomerService", errorObject.getDescription());
			MainComposer.goToHome();
		}
	}
	
	public void postInit(Component component) {
		try {
			customerTypes = invoiceCreditNoteService.getAllCustomerTypes();
			exemptions = invoiceCreditNoteService.getAllExemptions();
			
			invoice = (Document) Sessions.getCurrent().removeAttribute("invoice");
			if (invoice != null) {
				WebFrontCore.setCurrentFunctionModfied(true);
				MainComposer.setFunctionTitle((XulElement) component, "Create Invoice");
			}
			fidelityCodeSearchField = (String) Sessions.getCurrent().removeAttribute("fidelityCode");
			
			if (StringUtils.isNotBlank(fidelityCodeSearchField)) {
				safeSearch();
			}
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred loading the page.\n" + e.getMessage() + "\nRetry");
		}
	}
	
	@Command
	@NotifyChange({"customerList", "selectedCustomer"})
	public void search() {
		try {
			safeSearch();
			
			log.debug(String.format("[%d] customers found for companyName [%s], fiscalOrVatCodeSearchField [%s]",
					customerList.size(), lastNameCompanyNameSearchField, fiscalOrVatCodeSearchField));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred during search.\n" + e.getMessage());
		}
	}
	
	private void safeSearch() {
		if (StringUtils.isNotBlank(lastNameCompanyNameSearchField)) {
			customerList = invoiceCreditNoteService.getCustomersByName(lastNameCompanyNameSearchField);
		} else if (StringUtils.isNotBlank(fiscalOrVatCodeSearchField)) {
			customerList = invoiceCreditNoteService.getCustomersByFiscalOrVatCode(fiscalOrVatCodeSearchField);
		} else if (StringUtils.isNotBlank(fidelityCodeSearchField)) {
			Customer customer = invoiceCreditNoteService.getCustomerByFidelityCode(fidelityCodeSearchField);
			
			fidelityCodeSearchField = "";
			customerList.clear();
			if (customer != null) {
				customerList.add(customer);
			}
		} else {
			customerList = invoiceCreditNoteService.getAllCustomers();
		}
		
		if (customerList.size() == 1) {
			selectedCustomer = customerList.get(0);
		} else {
			selectedCustomer = null;
		}
	}
	
	@Command
	@NotifyChange({"inEditMode", "selectedCustomer"})
	public void add() {
		try {
			setInEditMode(true);
			selectedCustomer = invoiceCreditNoteService.createDefaultCustomer();
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred creating a new customer");
		}
	}
	
	@Command
	@NotifyChange("inEditMode")
	public void edit() {
		setInEditMode(true);
	}
	
	@Command
	@NotifyChange({"customerList", "inEditMode", "selectedCustomer"})
	public void confirm() {
		try {
			if (selectedCustomer.getId() == -1) {
				int id = invoiceCreditNoteService.addCustomer(selectedCustomer);
				
				log.debug(String.format("Customer [%s] added with id [%d]", selectedCustomer, id));
				
				selectedCustomer.setId(id);
			} else {
				invoiceCreditNoteService.updateCustomer(selectedCustomer);
				
				log.debug(String.format("Customer [%s] updated", selectedCustomer));
			}
			Clients.showNotification(String.format("Customer %s successfully saved", selectedCustomer.getLastNameCompanyName()));
			
			resetDetails();
			setInEditMode(false);
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred during saving.\n" + e.getMessage());
		}
	}
	
	@Command
	@NotifyChange({"customerList", "selectedCustomer", "inEditMode"})
	public void cancel() {
		setInEditMode(false);
		if (selectedCustomer.getId() == -1) {
			selectedCustomer = null;
		}
	}
	
	@Command
	@NotifyChange({"customerList", "selectedCustomer", "inEditMode"})
	public void delete() {
		final InvoiceCustomerMaintenanceViewModel _this = this;
		
		Messagebox.show("Confirm the deletion of customer " + selectedCustomer.getLastNameCompanyName() + "?", "Confirm deletion", Messagebox.YES
				| Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (event.getName().equals("onYes")) {
					try {
						invoiceCreditNoteService.deleteCustomer(selectedCustomer);
						Clients.showNotification(String.format("Customer %s successfully deleted", selectedCustomer.getLastNameCompanyName()));
						
						log.debug(String.format("Customer [%s] deleted", selectedCustomer));
						
						_this.resetDetails();
						BindUtils.postNotifyChange(null, null, _this, "customerList");
						BindUtils.postNotifyChange(null, null, _this, "selectedCustomer");
						BindUtils.postNotifyChange(null, null, _this, "inEditMode");
					} catch (Exception e) {
						log.error("", e);
						
						Clients.showNotification("An error has occurred during customer deletion.\n" + e.getMessage());
					}
				}
			}
		});
	}
	
	@Command
	public void back() {
		Messagebox.show("Quit the generation of this document?", "Confirm quit", Messagebox.YES
				| Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (event.getName().equals("onYes")) {
					goBack();
				}
			}
		});
	}
	
	private void goBack() {
		try {
			log.debug("about to discarding invoice");
			
			invoiceCreditNoteService.cancelInvoice(invoice);
			
			log.debug(String.format("invoice [%s] discarted", invoice));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred.\n" + e.getMessage());
		}
		
		WebFrontCore.setCurrentFunctionModfied(false);
		WebFrontCore.setCurrentFunctionBlocking(false);
		MainComposer.goTo("invoicereceiptsearch.zul");
	}
	
	@Command
	@NotifyChange("generatedInvoice")
	public void backToUserSelection() {
		try {
			log.debug("about to discarding invoice");
			
			invoiceCreditNoteService.cancelInvoice(invoice);
			
			log.debug(String.format("invoice [%s] discarted", invoice));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred.\n" + e.getMessage());
		}
		
		WebFrontCore.setCurrentFunctionBlocking(false);
		generatedInvoice = null;
		skipInvoiceGeneration = true;
	}
	
	@Command
	@NotifyChange("generatedInvoice")
	public void generateInvoice() {
		try {
			WebFrontCore.setCurrentFunctionBlocking(true);
			
			invoice.setCustomer(selectedCustomer);
			
			if (!skipInvoiceGeneration) {
				log.debug(String.format("generating invoice [%s]", invoice));
				
				invoice = invoiceCreditNoteService.generateInvoice(invoice);
				
				log.debug("invoice generated. Redirecting");
			}
			
			byte[] bytes = invoiceCreditNoteService.renderHtmlInvoice(invoice);
			generatedInvoice = new AMedia(String.format("%s.html", invoice.getDocumentNumber()), "html", "text/html", bytes);
			
			log.debug(String.format("html is [%d] bytes", bytes.length));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred during invoice generation.\n" + e.getMessage());
		}
	}
	
	@Command
	@NotifyChange("generatedInvoice")
	public void saveAndPrintInvoice() {
		try {
			log.debug(String.format("saving and printing invoice [%s]", invoice));
			
			byte[] bytes = invoiceCreditNoteService.saveInvoiceAndRenderPdf(invoice);
			generatedInvoice = new AMedia(String.format("%s.pdf", invoice.getDocumentCode()), "pdf", "application/pdf", bytes);
			
			log.debug(String.format("pdf is [%d] bytes", bytes.length));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred during invoice saving.\n" + e.getMessage());
		}
		
		WebFrontCore.setCurrentFunctionModfied(false);
		WebFrontCore.setCurrentFunctionBlocking(false);
	}
	
	private void resetDetails() {
		selectedCustomer = null;
		setInEditMode(false);
		safeSearch();
	}
	
	public String getLastNameCompanyNameSearchField() {
		return lastNameCompanyNameSearchField;
	}

	public void setLastNameCompanyNameSearchField(String lastNameCompanyNameSearchField) {
		this.lastNameCompanyNameSearchField = lastNameCompanyNameSearchField;
	}

	public String getFiscalOrVatCodeSearchField() {
		return fiscalOrVatCodeSearchField;
	}

	public void setFiscalOrVatCodeSearchField(String fiscalOrVatCodeSearchField) {
		this.fiscalOrVatCodeSearchField = fiscalOrVatCodeSearchField;
	}
	
	public List<Customer> getCustomerList() {
		return customerList;
	}
	
	public Customer getSelectedCustomer() {
		return selectedCustomer;
	}

	public void setSelectedCustomer(Customer selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}
	
	public List<CustomerType> getCustomerTypes() {
		return customerTypes;
	}
	
	public List<Exemption> getExemptions() {
		return exemptions;
	}
	
	public boolean isInEditMode() {
		return inEditMode;
	}
	
	public void setInEditMode(boolean inEditMode) {
		this.inEditMode = inEditMode;
		
		if (generatedInvoice == null) {
			WebFrontCore.setCurrentFunctionModfied(inEditMode);
		} else {
			WebFrontCore.setCurrentFunctionBlocking(inEditMode);
		}
	}
	
	public Document getInvoice() {
		return invoice;
	}
	
	public AMedia getGeneratedInvoice() {
		
		log.info("Entered in getGeneratedInvoice");
		log.info("generatedInvoice = " + generatedInvoice);
		if (generatedInvoice != null){
			log.info("generatedInvoice.toString() = " + generatedInvoice.toString());
		}
		return generatedInvoice;
	}

	private String lastNameCompanyNameSearchField;
	private String fiscalOrVatCodeSearchField;
	private String fidelityCodeSearchField;
	private List<Customer> customerList = new ArrayList<>();
	private Customer selectedCustomer;
	private List<CustomerType> customerTypes;
	private List<Exemption> exemptions;
	private boolean inEditMode = false;
	private boolean skipInvoiceGeneration = false;
	private Document invoice;
	private AMedia generatedInvoice;
	
	private InvoiceCreditNoteServiceInterface invoiceCreditNoteService = new InvoiceCreditNoteServiceClientImpl();
	
	private static final Logger log = WebFrontLogger.getLogger(InvoiceCustomerMaintenanceViewModel.class);
}
