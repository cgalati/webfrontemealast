package com.ncr.webfront.base.functionalities.reporthourlyactivity;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontReportHourlyActivitySettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontReportHourlyActivitySettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontReportHourlyActivitySettings getInstance() {
		if (instance == null) {
			instance = new WebFrontReportHourlyActivitySettings("reports.properties");
		}
		return instance;
	}

	protected WebFrontReportHourlyActivitySettings(String propertyFileName) {
		super(propertyFileName);
	}

	
	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reporthourlyactivity.title", "Hourly Activity Report");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getConsolidated() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reporthourlyactivity.consolidated", "Consolidated");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDateLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reporthourlyactivity.date", "Date");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getTerminals() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reporthourlyactivity.terminals", "Terminals");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getFormatLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reporthourlyactivity.format", "Format");
		logger.debug("END (" + value + ")");
		return value;
	}
}
