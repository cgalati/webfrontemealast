package com.ncr.webfront.base.functionalities.reporthourlyactivity;

import org.zkoss.bind.annotation.Init;

import com.ncr.webfront.base.functionalities.reportfinancial.WebFrontReportFinancialSettings;
import com.ncr.webfront.base.functionalities.reports.ReportBaseViewModel;
import com.ncr.webfront.base.reports.ReportOptions;

public class ReportHourlyActivityViewModel extends ReportBaseViewModel {
	@Override
	@Init
	// Sadly needed.
	public void init() {
		super.init();
	}
	
	@Override
	protected byte[] generateReport(ReportOptions reportOptions) {
		return reportsService.generateHourlyActivityReport(reportOptions);
	}
	
	
	public String getTitlePage() {
		return WebFrontReportFinancialSettings.getInstance().getTitlePage();
	}

	public String getConsolidated() {
		return WebFrontReportFinancialSettings.getInstance().getConsolidated();
	}
	
	public String getDateLbl() {
		return WebFrontReportFinancialSettings.getInstance().getDateLbl();
	}

	public String getTerminals() {
		return WebFrontReportFinancialSettings.getInstance().getTerminals();
	}

	public String getFormatLbl() {
		return WebFrontReportFinancialSettings.getInstance().getFormatLbl();
	}
}
