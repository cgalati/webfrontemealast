package com.ncr.webfront.base.functionalities.reportdepartment;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontReportDepartmentSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontReportDepartmentSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontReportDepartmentSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontReportDepartmentSettings("reports.properties");
		}
		return instance;
	}

	protected WebFrontReportDepartmentSettings(String propertyFileName) {
		super(propertyFileName);
	}

	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.title", "Department Report");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getConsolidated() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.consolidated", "Consolidated");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDateLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.date", "Date");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getTerminals() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.terminals", "Terminals");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getFormatLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.format", "Format");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getAll() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.all", "All");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDepartments() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.departments", "Departments");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDetail() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportdepartment.detail", "Detail");
		logger.debug("END (" + value + ")");
		return value;
	}
}
