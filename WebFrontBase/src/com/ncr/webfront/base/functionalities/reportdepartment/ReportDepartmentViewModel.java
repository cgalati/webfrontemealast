package com.ncr.webfront.base.functionalities.reportdepartment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

import com.google.gson.Gson;
import com.ncr.webfront.base.functionalities.articlemaintenance.WebFrontArticleSettings;
import com.ncr.webfront.base.functionalities.reports.ReportBaseViewModel;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.restclient.posdepartmentservice.PosDepartmentServiceClientImpl;

public class ReportDepartmentViewModel extends ReportBaseViewModel {
	@Override
	@Init
	// Sadly needed.
	public void init() {
		super.init();
		PosDepartmentServiceInterface posdepartmentService = new PosDepartmentServiceClientImpl();
		posDepartmentList = posdepartmentService.getDepartmentsOfLevel(MAJOR);
//		selectedPosDepartmentList = new HashSet<>(posDepartmentList);
//		allDepartments = true;
		selectedPosDepartmentList = new HashSet<>();
		allDepartments = false;
		detail = true;
	}
	
	@Override
	protected void loadOptions(ReportOptions reportOptions) {
		super.loadOptions(reportOptions);
		reportOptions.setPosDepartments(new ArrayList<PosDepartment>(selectedPosDepartmentList));
		reportOptions.setAdditionalInfo(gson.toJson(detail).toString());
	}
	
	@Override
	protected byte[] generateReport(ReportOptions reportOptions) {
		return reportsService.generateDepartmentReport(reportOptions);
	}

	public List<PosDepartment> getPosDepartmentList() {
		return posDepartmentList;
	}

	public void setPosDepartmentList(List<PosDepartment> posDepartmentList) {
		this.posDepartmentList = posDepartmentList;
	}

	public Set<PosDepartment> getSelectedPosDepartmentList() {
		return selectedPosDepartmentList;
	}

	public void setSelectedPosDepartmentList(Set<PosDepartment> selectedPosDepartmentList) {
		this.selectedPosDepartmentList = selectedPosDepartmentList;
	}

	public boolean isAllDepartments() {
		return allDepartments;
	}

	public void setAllDepartments(boolean allDepartments) {
		this.allDepartments = allDepartments;
	}
	
	public boolean isDetail() {
		return detail;
	}

	public void setDetail(boolean detail) {
		this.detail = detail;
	}

	@Command
	@NotifyChange({ "allDepartments", "selectedPosDepartmentList"})
	public void selectAll() {
		if (allDepartments) {
			selectedPosDepartmentList = new HashSet<>(posDepartmentList);
		}else{
			selectedPosDepartmentList = new HashSet<>();
		}			
	}

	@Command
	@NotifyChange({ "allDepartments", "selectedPosDepartmentList"})
	public void selectDepartments() {
		allDepartments = selectedPosDepartmentList.size() == posDepartmentList.size();
	}
	/*
	public String getTitlePage() {
		return WebFrontReportDepartmentSettings.getInstance().getTitlePage();
	}

	public String getConsolidated() {
		return WebFrontReportDepartmentSettings.getInstance().getConsolidated();
	}
	
	public String getDateLbl() {
		return WebFrontReportDepartmentSettings.getInstance().getDateLbl();
	}

	public String getTerminals() {
		return WebFrontReportDepartmentSettings.getInstance().getTerminals();
	}

	public String getFormatLbl() {
		return WebFrontReportDepartmentSettings.getInstance().getFormatLbl();
	}

	public String getAll() {
		return WebFrontReportDepartmentSettings.getInstance().getAll();
	}

	public String getDepartments() {
		return WebFrontReportDepartmentSettings.getInstance().getDepartments();
	}

	public String getDetail() {
		return WebFrontReportDepartmentSettings.getInstance().getDetail();
	}*/

	
	private List<PosDepartment> posDepartmentList;
	private Set<PosDepartment> selectedPosDepartmentList;
	private boolean allDepartments;
	private boolean detail;
	private Gson gson = new Gson();
	
	public static final int DEPARTMENT = 0;
	public static final int MAJOR = 1;
}
