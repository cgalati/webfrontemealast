package com.ncr.webfront.base.functionalities.documentsearch;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.util.Clients;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentDetail;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.base.restclient.invoicecreditnoteservice.InvoiceCreditNoteServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.WebFrontCore;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.zkconverters.ZkCentsToStringCurrencyConverter;
import com.ncr.webfront.core.zkconverters.DateConverter;

public class DocumentSearchViewModel {
	private static final String REPRINTING_ARG_VALUE = "reprint";
	private static int LAST_PRINTED_LIMIT = 15;
	
	@Init
	public void init(@ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {
		ErrorObject errorObject = invoiceCreditNoteService.checkService();
		
		if ((errorObject != null) && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("InvoiceCustomerService", errorObject.getDescription());
			MainComposer.goToHome();
		}
		
		postInit(firstArgument);
	}
	
	private void postInit(String firstArgument) {
		reprint = StringUtils.equals(firstArgument, REPRINTING_ARG_VALUE);
	}
	
	@Command
	@NotifyChange({"documentList", "selectedDocument", "selectedInvoiceItems"})
	public void search() {
		try {
			if (!reprint) {
				safeSearch();
			} else {
				reprintSafeSearch();
			}
			
			log.debug(String.format("[%d] documents found for number [%d], vatCode [%s], fiscalCode [%s]", documentList.size(),
					documentNumberSearchField, vatCodeSearchField, fiscalCodeSearchField));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred during search.\n" + e.getMessage());
		}
	}
	
	private void safeSearch() {
		if (documentNumberSearchField != null) {
			Document document = invoiceCreditNoteService.getInvoiceByNumber(documentNumberSearchField, false);
			
			documentList.clear();
			if (document != null) {
				documentList.add(document);
			}
		} else if (StringUtils.isNotBlank(vatCodeSearchField)) {
			documentList = invoiceCreditNoteService.getInvoicesByVatCode(vatCodeSearchField, false);
		} else if (StringUtils.isNotBlank(fiscalCodeSearchField)) {
			documentList = invoiceCreditNoteService.getInvoicesByFiscalCode(fiscalCodeSearchField, false);
		} else {
			documentList = invoiceCreditNoteService.getLastPrintedInvoices(LAST_PRINTED_LIMIT, false);
		}
		
		updateSelectedDocumentsAndItems();
	}
	
	private void reprintSafeSearch() {
		if (documentNumberSearchField != null) {
			Document document = invoiceCreditNoteService.getPrintedDocumentByNumber(documentNumberSearchField);
			
			documentList.clear();
			if (document != null) {
				documentList.add(document);
			}
		} else if (StringUtils.isNotBlank(vatCodeSearchField)) {
			documentList = invoiceCreditNoteService.getPrintedDocumentsByVatCode(vatCodeSearchField);
		} else if (StringUtils.isNotBlank(fiscalCodeSearchField)) {
			documentList = invoiceCreditNoteService.getPrintedDocumentsByFiscalCode(fiscalCodeSearchField);
		} else {
			documentList = invoiceCreditNoteService.getLastPrintedDocuments(LAST_PRINTED_LIMIT);
		}
		
		updateSelectedDocumentsAndItems();
	}
	
	private void updateSelectedDocumentsAndItems() {
		selectedInvoiceItems.clear();
		if (documentList.size() == 1) {
			selectedDocument = documentList.get(0);
			selectDocument();
		} else {
			selectedDocument = null;
		}
	}
	
	@Command
	@NotifyChange("selectedInvoiceItems")
	public void selectDocument() {
		selectedInvoiceItems.clear();
		if (reprint) {
			for (DocumentDetail detail : selectedDocument.getDocumentDetails()) {
				selectedInvoiceItems.add(detail.getSequenceNumber());
			}
		}
	}
	
	@Command
	@NotifyChange("selectedInvoiceItemsEmpty")
	public void toggleItemSelection(@BindingParam("sequenceNumber") int sequenceNumber) {
		if (selectedInvoiceItems.contains(sequenceNumber)) {
			selectedInvoiceItems.remove((Object) sequenceNumber);
		} else {
			selectedInvoiceItems.add(sequenceNumber);
		}
	}
	
	@Command
	@NotifyChange("generated")
	public void generate() {
		if (reprint) {
			if (selectedDocument.isA(DocumentType.INVOICE_DOCUMENT_TYPE)) {
				reprintInvoice();
			} else {
				reprintCreditNote();
			}
		} else {
			generateCreditNote();
		}
	}
	
	private void reprintInvoice() {
		try {
			byte[] bytes = invoiceCreditNoteService.renderPdfInvoice(selectedDocument);
			generated = new AMedia(String.format("%s.pdf", selectedDocument.getDocumentNumber()), "pdf", "application/pdf", bytes);
			
			log.debug(String.format("pdf is [%d] bytes", bytes.length));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred while showing the invoice.\n" + e.getMessage());
		}
	}
	
	private void reprintCreditNote() {
		try {
			byte[] bytes = invoiceCreditNoteService.renderPdfCreditNote(selectedDocument);
			generated = new AMedia(String.format("%s.pdf", selectedDocument.getDocumentNumber()), "pdf", "application/pdf", bytes);
			
			log.debug(String.format("pdf is [%d] bytes", bytes.length));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred while showing the credit note.\n" + e.getMessage());
		}
	}
	
	private void generateCreditNote() {
		WebFrontCore.setCurrentFunctionBlocking(true);
		
		try {
			Document invoice = removeUnselectedDetailsFromSelectedInvoice();
			
			log.debug("generating credit note");
			
			creditNote = invoiceCreditNoteService.generateCreditNote(invoice);
			
			log.debug("credit note generated. Redirecting");
			
			byte[] bytes = invoiceCreditNoteService.renderHtmlCreditNote(creditNote);
			generated = new AMedia(String.format("%s.html", creditNote.getDocumentNumber()), "html", "text/html", bytes);
			
			log.debug(String.format("html is [%d] bytes", bytes.length));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred while generating note.\n" + e.getMessage());
		}
	}
	
	private Document removeUnselectedDetailsFromSelectedInvoice() throws Exception {
		Document invoice = new Document();
		
		PropertyUtils.copyProperties(invoice, selectedDocument);
		
		invoice.setDocumentDetails(new ArrayList<DocumentDetail>());
		for (DocumentDetail detail : selectedDocument.getDocumentDetails()) {
			if (selectedInvoiceItems.contains(detail.getSequenceNumber())) {
				invoice.getDocumentDetails().add(detail);
				
				log.debug(String.format("adding detail [%d] to invoice for credit note", detail.getSequenceNumber()));
			}
		}
		
		return invoice;
	}
	
	@Command
	@NotifyChange({"generated", "printDisabled"})
	public void back() {
		try {
			if (!reprint) {
				log.debug("about to discarding credit note");
				
				invoiceCreditNoteService.cancelCreditNote(creditNote);
				
				log.debug(String.format("credit note [%s] discarted", generated));
			}
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred.\n" + e.getMessage());
		}
		
		creditNote = null;
		generated = null;
		printDisabled = false;
		WebFrontCore.setCurrentFunctionBlocking(false);
	}
	
	@Command
	@NotifyChange({"generated", "printDisabled"})
	public void saveAndPrintCreditNote() {
		try {
			printDisabled = true;
			
			log.debug(String.format("saving and printing credit note [%s]", creditNote));
			
			byte[] bytes = invoiceCreditNoteService.saveInvoiceAndRenderPdf(creditNote);
			generated = new AMedia(String.format("%s.pdf", creditNote.getDocumentCode()), "pdf", "application/pdf", bytes);
			
			log.debug(String.format("pdf is [%d] bytes", bytes.length));
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred while saving the credit note.\n" + e.getMessage());
		}
		
		WebFrontCore.setCurrentFunctionBlocking(false);
	}
	
	public Integer getDocumentNumberSearchField() {
		return documentNumberSearchField;
	}
	
	public void setDocumentNumberSearchField(Integer documentNumberSearchField) {
		this.documentNumberSearchField = documentNumberSearchField;
	}
	
	public String getVatCodeSearchField() {
		return vatCodeSearchField;
	}
	
	public void setVatCodeSearchField(String vatCodeSearchField) {
		this.vatCodeSearchField = vatCodeSearchField;
	}
	
	public String getFiscalCodeSearchField() {
		return fiscalCodeSearchField;
	}
	
	public void setFiscalCodeSearchField(String fiscalCodeSearchField) {
		this.fiscalCodeSearchField = fiscalCodeSearchField;
	}
	
	public List<Document> getDocumentList() {
		return documentList;
	}
	
	public void setDocumentList(List<Document> documentList) {
		this.documentList = documentList;
	}
	
	public Document getSelectedDocument() {
		return selectedDocument;
	}
	
	public void setSelectedDocument(Document selectedDocument) {
		this.selectedDocument = selectedDocument;
	}
	
	public AMedia getGenerated() {
		return generated;
	}
	
	public void setGenerated(AMedia generated) {
		this.generated = generated;
	}
	
	public ZkCentsToStringCurrencyConverter getCentsToStringCurrencyConverter() {
		return new ZkCentsToStringCurrencyConverter();
	}
	
	public DateConverter getDateConverter() {
		return new DateConverter();
	}
	
	public boolean isSelectedInvoiceItemsEmpty() {
		return selectedInvoiceItems.isEmpty();
	}
	
	public boolean isPrintDisabled() {
		return printDisabled;
	}

	public void setPrintDisabled(boolean printDisabled) {
		this.printDisabled = printDisabled;
	}
	
	public boolean isReprint() {
		return reprint;
	}

	public void setReprint(boolean reprint) {
		this.reprint = reprint;
	}
	
	private Integer documentNumberSearchField;
	private String vatCodeSearchField;
	private String fiscalCodeSearchField;
	private List<Document> documentList = new ArrayList<>();
	private Document selectedDocument;
	private List<Integer> selectedInvoiceItems = new ArrayList<>();
	private Document creditNote;
	private AMedia generated;
	private boolean printDisabled = false;
	private boolean reprint = false;
	
	private InvoiceCreditNoteServiceInterface invoiceCreditNoteService = new InvoiceCreditNoteServiceClientImpl();
	
	private static final Logger log = WebFrontLogger.getLogger(DocumentSearchViewModel.class);
}
