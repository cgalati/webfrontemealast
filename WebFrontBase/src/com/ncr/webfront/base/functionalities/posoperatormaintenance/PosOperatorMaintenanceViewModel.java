package com.ncr.webfront.base.functionalities.posoperatormaintenance;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.XulElement;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.functionalities.postransactionssearch.PosTransactionsSearchViewModel;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.WebFrontCore;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;


public class PosOperatorMaintenanceViewModel {

	@GlobalCommand
   public void onDesktopResize(@BindingParam("width") int width, @BindingParam("height") int height, @BindingParam("orientation") String orientation ){
		logger.debug("orientation = \"" +Executions.getCurrent().getDesktop().getAttribute("orientation")+"\"");
		logger.debug("width       = \"" +Executions.getCurrent().getDesktop().getAttribute("width")+"\"");
		logger.debug("height      = \"" +Executions.getCurrent().getDesktop().getAttribute("height")+"\"");
   }

	public PosOperatorMaintenanceViewModel() {
		this.operatorService = new PosOperatorServiceClientImpl();
		
		if(posOperatorRoleList == null) {
			setPosOperatorRoleList(new ArrayList<String>(operatorService.getPosOperatorRoles().values()));
			traineeRole = operatorService.getPosOperatorTraineeRole();
		}
		logger.debug("orientation = \"" +Executions.getCurrent().getDesktop().getAttribute("orientation")+"\"");
		logger.debug("width       = \"" +Executions.getCurrent().getDesktop().getAttribute("width")+"\"");
		logger.debug("height      = \"" +Executions.getCurrent().getDesktop().getAttribute("height")+"\"");
				
		reset();
	}

	private void reset() {
		codeSearchField = null;
		nameSearchField = "";
		posOperatorList = new ArrayList<PosOperator>();
		setEditingPosOperator(null);
	}
	
	@Init
	public void init(@ContextParam(ContextType.COMPONENT) Component component, @ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {
		logger.info("firstArgument = \"" + firstArgument + "\"");
		String ro = "#readOnly";
		if (firstArgument != null && firstArgument.equals(ro)) {
			this.setReadOnly(true);
			MainComposer.setFunctionTitle((XulElement)component, "Operators List");
		}
		ErrorObject errorObject = operatorService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("OperatorService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}
		if(readOnly) {
			//search();
			// Cleaning
			setSelectedPosOperator(null);
			setEditingPosOperator(null);
			posOperatorList = operatorService.searchByName(ro);
			BindUtils.postNotifyChange(null, null, this, "posOperatorList");
		}
	}

	public String concat(String string1, String string2) {
		return string1 + string2;
	}

	@Command
	@NotifyChange("posOperatorList")
	public void search() {
		// Cleaning
		setSelectedPosOperator(null);
		setEditingPosOperator(null);
		if (codeSearchField != null) {
			posOperatorList = operatorService.searchByCode(encodeSearchString(codeSearchField.toString()));
			return;
		}
		if (!nameSearchField.isEmpty()) {
			posOperatorList = operatorService.searchByName(encodeSearchString(nameSearchField));
			return;
		}

		posOperatorList = operatorService.searchByName(nameSearchField);
	}

	private String encodeSearchString(String stringSearch) {
		stringSearch = stringSearch.trim();
		try {
			stringSearch = URLEncoder.encode(stringSearch, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
		}
		return stringSearch;
	}

	@Command
	public void add() {
		setEditingPosOperator(operatorService.createDefaultCashier());
		setDisplayEdit(false);
		setDisplayAdd(true);
	}

	@Command
	public void edit() {
		setEditingPosOperator(new PosOperator(selectedPosOperator.getOperatorCode(), selectedPosOperator.getName(), selectedPosOperator.getPersonnelNo(), selectedPosOperator.getSecretNo(), selectedPosOperator.getRole()));
		setDisplayEdit(true);
		setDisplayAdd(false);
	}

	@Command
	public void delete() {
		final Object _this = this;
		
		Messagebox.show("Confirm deletion of the cashier " + selectedPosOperator.getOperatorCode() + " ?", "Confirm deleting ", Messagebox.YES
		      | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
			public void onEvent(Event evt) throws InterruptedException {
				if (evt.getName().equals("onYes")) {
					eo = operatorService.deletePosOperator(selectedPosOperator.getOperatorCode());
					if (!showError()) {
						// Refreshing view
						if (posOperatorList.indexOf(selectedPosOperator) > -1) {
							posOperatorList.remove(posOperatorList.indexOf(selectedPosOperator));
						}
						BindUtils.postNotifyChange(null, null, _this, "posOperatorList");

						// Cleaning
						setSelectedPosOperator(null);
						setEditingPosOperator(null);
						BindUtils.postNotifyChange(null, null, _this, "selectedPosOperator");
						BindUtils.postNotifyChange(null, null, _this, "editingPosOperator");
					}
				}
			}
		});
	}

	@Command
	public void confirm() {
		if (displayEdit) {
			eo = operatorService.updatePosOperator(editingPosOperator);
		} else {
			eo = operatorService.addPosOperator(editingPosOperator);
			setSelectedPosOperator(null);
		}
		if (!showError()) {
			setDisplayEdit(false);
			setDisplayAdd(false);
		}
		search();
		BindUtils.postNotifyChange(null, null, this, "*");
	}

	@Command
	public void cancel() {
		if (displayEdit) {
			setDisplayEdit(false);
		} else {
			setDisplayAdd(false);
			setSelectedPosOperator(null);
		}
		setEditingPosOperator(null);
	}
	
	@Command
	public void changeRole() {
		setEditingPosOperator(operatorService.createCashierFromRole(editingPosOperator.getRole()));
	}

	private boolean showError() {
		boolean isInError = false;
		if (eo != null && eo.getDescription() != null && eo.getDescription().length() > 0) {
			if (eo.getErrorCode().equals(ErrorCode.OK_NO_ERROR)) {
				Messagebox.show(eo.getDescription(), "Information", Messagebox.OK, Messagebox.INFORMATION);
			} else if (eo.getErrorCode().equals(ErrorCode.OK_WARNING)) {
				Messagebox.show(eo.getDescription(), "Attention", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				Messagebox.show(eo.getDescription(), "Error", Messagebox.OK, Messagebox.ERROR);
				isInError = true;
			}
		}
		return isInError;
	}
	
	@NotifyChange({ "codeSearchField", "nameSearchField" })
	public void setCodeSearchField(Integer codeSearchField) {
		this.codeSearchField = codeSearchField;
		nameSearchField = "";
	}

	public Integer getCodeSearchField() {
		return codeSearchField;
	}

	@NotifyChange({ "codeSearchField", "nameSearchField" })
	public void setNameSearchField(String nameSearchField) {
		codeSearchField = null;
		this.nameSearchField = nameSearchField;
	}

	public String getNameSearchField() {
		return nameSearchField;
	}

	public List<PosOperator> getPosOperatorList() {
		logger.info("getPosOperatorList - posOperatorList.size() size = " + posOperatorList.size());
		return posOperatorList;
	}

	@NotifyChange({ "selectedPosOperator" })
	public void setSelectedPosOperator(PosOperator selectedPosOperator) {
		this.selectedPosOperator = selectedPosOperator;
		BindUtils.postNotifyChange(null, null, this, "selectedPosOperator");
	}

	public PosOperator getSelectedPosOperator() {
		return selectedPosOperator;
	}

	public PosOperator getEditingPosOperator() {
		return editingPosOperator;
	}

	@NotifyChange({ "editingPosOperator" })
	public void setEditingPosOperator(PosOperator editingPosOperator) {
		this.editingPosOperator = editingPosOperator;
		BindUtils.postNotifyChange(null, null, this, "editingPosOperator");
	}

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
		BindUtils.postNotifyChange(null, null, this, "displayEdit");
		WebFrontCore.setCurrentFunctionModfied(displayEdit || displayAdd);
	}

	public boolean isDisplayAdd() {
		return displayAdd;
	}

	public void setDisplayAdd(boolean displayAdd) {
		this.displayAdd = displayAdd;
		BindUtils.postNotifyChange(null, null, this, "displayAdd");
		WebFrontCore.setCurrentFunctionModfied(displayEdit || displayAdd);
	}

	public List<String> getPosOperatorRoleList() {
		return posOperatorRoleList;
	}

	public void setPosOperatorRoleList(List<String> posOperatorRoleList) {
		this.posOperatorRoleList = posOperatorRoleList;
	}
	
	public String getTraineeRole() {
		return traineeRole;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	private final Logger logger = WebFrontLogger.getLogger(PosOperatorMaintenanceViewModel.class);

	private Integer codeSearchField;
	private String nameSearchField;

	private List<PosOperator> posOperatorList;
	private PosOperator selectedPosOperator;
	private PosOperator editingPosOperator;
	
	private List<String> posOperatorRoleList;
	private String traineeRole;

	private ErrorObject eo;

	private boolean readOnly = false;
	private boolean displayEdit = false;
	private boolean displayAdd = false;

	private PosOperatorServiceInterface operatorService;

}
