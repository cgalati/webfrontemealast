package com.ncr.webfront.base.functionalities.posoperatormaintenance;

import java.util.List;

import org.zkoss.bind.ValidationContext;

import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;
import com.ncr.webfront.core.utils.validation.PosOperatorValidationData;
import com.ncr.webfront.core.utils.validation.WebFrontValidator;

public class PosOperatorValidator extends WebFrontValidator {
	@Override
	protected Object getBean(ValidationContext context) {
		return context.getValidatorArg("posOperatorToValidate");
	}
	
	@Override
	protected List<PosOperatorValidationData> getValidationData() {
		return new PosOperatorServiceClientImpl().getPosOperatorValidationData();
	}
}
