package com.ncr.webfront.base.functionalities.invoicingparametersmaintenance;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.base.restclient.invoicecreditnoteservice.InvoiceCreditNoteServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class InvoicingParametersMaintenanceViewModel {
	@Init
	public void init() {
		ErrorObject errorObject = invoiceService.checkService();
		
		if ((errorObject == null) || errorObject.getErrorCode().isOk()) {
			postInit();
		} else {
			MainComposer.notifyServiceProblems("InvoicingParametersService", errorObject.getDescription());
			MainComposer.goToHome();
		}
	}
	
	public void postInit() {
		reload();
	}

	@Command
	@NotifyChange({"editing", "sequences"})
	public void toggleEditing(@BindingParam("sequence") EditingDecoratedMutablePair<DocumentType, Integer> sequence) {
		sequence.setEditing(!sequence.isEditing());
		reload();
	}
	
	@Command
	@NotifyChange({"editing", "sequences"})
	public void confirm(@BindingParam("sequence") EditingDecoratedMutablePair<DocumentType, Integer> sequence) {
		String key = sequence.getKey().getType();
		int value = sequence.getValue();
		
		log.debug(String.format("setting [%s] as [%d]", key, value));
		
		invoiceService.setSequence(key, value);
		sequence.setEditing(false);
		reload();
	}
	
	@Command
	@NotifyChange({"editing", "sequences"})
	public void reload() {
		if (!isEditing()) {
			sequences.clear();
			for (MutablePair<DocumentType, Integer> sequence : invoiceService.peekSequences()) {
				sequences.add(new EditingDecoratedMutablePair<DocumentType, Integer>(sequence));
			}
		}
	}
	
	public List<EditingDecoratedMutablePair<DocumentType, Integer>> getSequences() {
		return sequences;
	}
	
	public void setSequences(List<EditingDecoratedMutablePair<DocumentType, Integer>> sequences) {
		this.sequences = sequences;
	}
	
	public boolean isEditing() {
		for (EditingDecoratedMutablePair<DocumentType, Integer> sequence : sequences) {
			if (sequence.isEditing()) {
				return true;
			}
		}
		
		return false;
	}
	
	private List<EditingDecoratedMutablePair<DocumentType, Integer>> sequences = new ArrayList<>();
	
	private InvoiceCreditNoteServiceInterface invoiceService = new InvoiceCreditNoteServiceClientImpl();
	
	private static final Logger log = WebFrontLogger.getLogger(InvoicingParametersMaintenanceViewModel.class);
}
