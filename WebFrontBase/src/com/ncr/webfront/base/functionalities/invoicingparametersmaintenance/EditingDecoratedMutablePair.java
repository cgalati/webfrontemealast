package com.ncr.webfront.base.functionalities.invoicingparametersmaintenance;

import org.apache.commons.lang3.tuple.MutablePair;

public class EditingDecoratedMutablePair<L, R> extends MutablePair<L, R> {
	private static final long serialVersionUID = 2524855699997636661L;

	public EditingDecoratedMutablePair(MutablePair<L, R> pair) {
		left = pair.left;
		right = pair.right;
	}
	
	public boolean isEditing() {
		return editing;
	}
	
	public void setEditing(boolean editable) {
		this.editing = editable;
	}
	
	private boolean editing = false;
}
