package com.ncr.webfront.base.functionalities.articlemaintenance;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.impl.XulElement;

import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.functionalities.posidcjrnservice.WebFrontEmeaSettings;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.restclient.articleservice.ArticleServiceClientImpl;
import com.ncr.webfront.base.restclient.commonlists.CommonListsServiceClientImpl;
import com.ncr.webfront.base.restclient.posdepartmentservice.PosDepartmentServiceClientImpl;
import com.ncr.webfront.base.restclient.tareservice.TareServiceClientImpl;
import com.ncr.webfront.base.restclient.vatservice.VatServiceClientImpl;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.WebFrontCore;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.login.LoginManagerInterface;
import com.ncr.webfront.core.utils.commonlists.CommonListsServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.zkconverters.ZkCentsToStringCurrencyConverter;
import com.ncr.webfront.eventlogger.commondata.data.WebFrontEvent;
import com.ncr.webfront.eventlogger.plugin.EventLoggerService;


public class ArticleMaintenanceViewModel {
	private Properties propArticle = new Properties();
	
	public ArticleMaintenanceViewModel() {
		this.articleService = new ArticleServiceClientImpl();
		
		this.posDptService = new PosDepartmentServiceClientImpl();
		this.vatService = new VatServiceClientImpl();
		this.tareService = new TareServiceClientImpl();
		this.commonListsService = new CommonListsServiceClientImpl();

		// Initialize Capabilities
		setCapabilities(articleService.getCapabilities());

		// Initialize Lovs
		setPosDptList(posDptService.getAll());
		setVatList(vatService.getAll());
		setManualDicountCodeList(commonListsService.getManualDiscountCodeList());
		setTareList(tareService.getAll());
		setPackagingTypeList(commonListsService.getPackagingTypeList());
		setPrizeCodeList(commonListsService.getPrizeCodeList());
		setZeroPriceBehaviorList(commonListsService.getZeroPriceBehaviourList());
		setAutoDiscountTypeList(commonListsService.getAutoDiscountTypeList());
		setUpbOperationTypeList(commonListsService.getUpbOperationTypeList());

		reset();
	}

	public ArticleMaintenanceViewModel(ArticleServiceInterface articleService) {
		this.articleService = articleService;
		this.posDptService = new PosDepartmentServiceClientImpl();
		this.vatService = new VatServiceClientImpl();
		this.commonListsService = new CommonListsServiceClientImpl();

		// Initialize Lovs
		setPosDptList(posDptService.getAll());
		setVatList(vatService.getAll());
		setManualDicountCodeList(commonListsService.getManualDiscountCodeList());
		setTareList(tareService.getAll());
		setPackagingTypeList(commonListsService.getPackagingTypeList());
		setZeroPriceBehaviorList(commonListsService.getZeroPriceBehaviourList());
		setAutoDiscountTypeList(commonListsService.getAutoDiscountTypeList());
		setUpbOperationTypeList(commonListsService.getUpbOperationTypeList());

		reset();
	}

	
	@Init
	public void init(@ContextParam(ContextType.COMPONENT) Component component, @ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {
		logger.info("firstArgument = \"" + firstArgument + "\"");
		if (firstArgument != null && firstArgument.equals("#readOnly")) {
			this.readOnly = true;
			MainComposer.setFunctionTitle((XulElement) component, "Search Article");
		}
		ErrorObject errorObject = articleService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("ArticleService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}
		errorObject = posDptService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("PosDepartmentService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}
		errorObject = vatService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("VatService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}
		errorObject = commonListsService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("CommonListsService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}
		errorObject = tareService.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("TareService", errorObject.getDescription());
			MainComposer.goToHome();
			return;
		}
	}

	private void reset() {
		articleCodeSearchField = null;
		pluCodeSearchField = "";
		descriptionSearchField = "";
		articleCodeTextSearchField = "";
		articleList = new ArrayList<Article>();
	}

	private void initializeObjects() {
		int i = 0;
		int objectId;
		String objectCode;

		// Pos Department
		objectCode = selectedArticle.getPosDpt();
		if (posDptList == null) {
			logger.warn("PosDptList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < posDptList.size(); i++) {
				if (((PosDepartment) posDptList.get(i)).getCode().equals(objectCode)) {
					setPosDpt((PosDepartment) posDptList.get(i));
					break;
				}
			}
		}

		// Vat
		objectId = selectedArticle.getVatId();
		if (vatList == null) {
			logger.warn("VatList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < vatList.size(); i++) {
				if (((Vat) vatList.get(i)).getVatId() == objectId) {
					setVat((Vat) vatList.get(i));
					break;
				}
			}
		}

		// Manual Discount Code
		objectId = selectedArticle.getManualDiscountId();
		if (manualDicountCodeList == null) {
			logger.warn("ManualDiscountCodeList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < manualDicountCodeList.size(); i++) {
				if (((MutablePair<Integer, String>) manualDicountCodeList.get(i)).getKey().intValue() == objectId) {
					setManualDicountCode((MutablePair<Integer, String>) manualDicountCodeList.get(i));
					break;
				}
			}
		}

		// Tare
		objectId = selectedArticle.getTareId();
		if (tareList == null) {
			logger.warn("TareList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < tareList.size(); i++) {
				if (((Tare) tareList.get(i)).getTareId() == objectId) {
					setTare((Tare) tareList.get(i));
					break;
				}
			}
		}

		// Packaging Type
		objectCode = selectedArticle.getPackagingType();
		if (packagingTypeList == null) {
			logger.warn("PackagingTypeList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < packagingTypeList.size(); i++) {
				if (((MutablePair<String, String>) packagingTypeList.get(i)).getKey().equals(objectCode)) {
					setPackagingType((MutablePair<String, String>) packagingTypeList.get(i));
					break;
				}
			}
		}

		// Prize Code
		objectId = selectedArticle.getPrizeCode();
		if (prizeCodeList == null) {
			logger.warn("PrizeCodeList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < prizeCodeList.size(); i++) {
				if (((MutablePair<Integer, String>) prizeCodeList.get(i)).getKey().intValue() == objectId) {
					setPrizeCode((MutablePair<Integer, String>) prizeCodeList.get(i));
					break;
				}
			}
		}

		// Zero Price
		objectId = selectedArticle.getZeroPriceBehavior();
		if (zeroPriceBehaviorList == null) {
			logger.warn("ZeroPriceBehaviorList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < zeroPriceBehaviorList.size(); i++) {
				if (((MutablePair<Integer, String>) zeroPriceBehaviorList.get(i)).getKey().intValue() == objectId) {
					setZeroPriceBehavior((MutablePair<Integer, String>) zeroPriceBehaviorList.get(i));
					break;
				}
			}
		}

		// Auto Discount Type
		objectId = selectedArticle.getAutomDiscountType();
		if (autoDiscountTypeList == null) {
			logger.warn("AutoDiscountTypeList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < autoDiscountTypeList.size(); i++) {
				if (((MutablePair<Integer, String>) autoDiscountTypeList.get(i)).getKey().intValue() == objectId) {
					setAutoDiscountType((MutablePair<Integer, String>) autoDiscountTypeList.get(i));
					break;
				}
			}
		}

		// Upb Operation Type
		objectId = selectedArticle.getUpbOperationType();
		if (upbOperationTypeList == null) {
			logger.warn("UpbOperationTypeList is null for article with code " + selectedArticle.getArticleCode());
		} else {
			for (i = 0; i < upbOperationTypeList.size(); i++) {
				if (((MutablePair<Integer, String>) upbOperationTypeList.get(i)).getKey().intValue() == objectId) {
					setUpbOperationType((MutablePair<Integer, String>) upbOperationTypeList.get(i));
					break;
				}
			}
		}

		BindUtils.postNotifyChange(null, null, this, "formattedPrice");
	}

	@Command
	@NotifyChange("articleList")
	public void search() {		
		// Cleaning
		setSelectedArticle(null);
		if (!articleCodeTextSearchField.isEmpty()) {
			if (!articleCodeTextSearchField.startsWith("=")) {
				articleList = articleService.searchByStartsWithArticleCode(encodeSearchString(articleCodeTextSearchField));
			} else {
				articleList = articleService.searchByArticleCode(encodeSearchString(articleCodeTextSearchField.substring(1)));
			}
			return;
		}
		if (articleCodeSearchField != null) {
			articleList = articleService.searchByArticleCode(encodeSearchString(articleCodeSearchField.toString()));
			return;
		}
		if (!pluCodeSearchField.isEmpty()) {
			articleList = articleService.searchByPluCode(encodeSearchString(pluCodeSearchField));
			return;
		}
		if (!descriptionSearchField.isEmpty()) {
			articleList = articleService.searchByDescription(encodeSearchString(descriptionSearchField));
			return;
		}

		articleList = articleService.searchByDescription(descriptionSearchField);
	}

	private String encodeSearchString(String stringSearch) {
		stringSearch = stringSearch.trim();
		try {
			stringSearch = URLEncoder.encode(stringSearch, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("", e);
		}
		return stringSearch;
	}

	@Command
	public void add() {
		setSelectedArticle(articleService.createArticle());
		logger.debug("Starting insertion for the following article: " + selectedArticle.toString());
		setDisplayEdit(false);
		setDisplayAdd(true);
	}

	@Command
	public void edit() {
		logger.debug("Starting update to the following article: " + selectedArticle.toString());
		previousArticlePrice = selectedArticle.getPrice();
		setDisplayEdit(true);
		setDisplayAdd(false);
	}

	@Command
	public void delete() {
		final Object _this = this;

		Messagebox.show("Confirm the deletion of article " + selectedArticle.getArticleCode() + " ?", "Confirm the deletion", Messagebox.YES
		      | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
			public void onEvent(Event evt) throws InterruptedException {
				if (evt.getName().equals("onYes")) {
					eo = articleService.deleteArticle(selectedArticle.getArticleCode());
					if (!showError()) {
						// Refreshing view
						if (articleList.indexOf(selectedArticle) > -1) {
							articleList.remove(articleList.indexOf(selectedArticle));
						}
						BindUtils.postNotifyChange(null, null, _this, "articleList");

						// Cleaning
						setSelectedArticle(null);
						BindUtils.postNotifyChange(null, null, _this, "selectedArticle");
					}
				}
			}
		});
	}

	@Command
	@NotifyChange({ "selectedArticle", "articleList" })
	public void confirm() {
		if (displayEdit) {
			logger.debug("Confirming the change to the following article: " + selectedArticle.toString());
			eo = articleService.updateArticle(selectedArticle);
			logEventArticlePriceChanged();
		} else {
			logger.debug("Confirming insertion for the following article: " + selectedArticle.toString());
			eo = articleService.addArticle(selectedArticle);
		}
		if (!showError()) {
			setDisplayEdit(false);
			setDisplayAdd(false);
		}
	}

	@Command
	public void cancel() {
		if (displayEdit) {
			setDisplayEdit(false);
			setSelectedArticle(articleService.searchByArticleCode(selectedArticle.getArticleCode()).get(0));
		} else {
			setDisplayAdd(false);
			setSelectedArticle(null);
		}
	}

	@Command
	public void editDetails() {
		// Cleaning
		setPluCodeField(null);
		tempPluCodes = new ArrayList<String>();
		for (String pc : selectedArticle.getPluCodes()) {
			tempPluCodes.add(pc);
		}
		BindUtils.postNotifyChange(null, null, this, "tempPluCodes");

		// Opening edit details
		setDisplayDetailsEdit(true);
	}

	@Command
	public void cancelPlu() {
		// Closing edit details without saving
		setDisplayDetailsEdit(false);
	}

	@Command
	public void addPlu() {
		// Adding a plu to view
		if (pluCodeField == null || pluCodeField.length() == 0) {
			return;
		}

		if (tempPluCodes.indexOf(pluCodeField) > -1) {
			Messagebox.show("Plu duplicato");
		} else {
			tempPluCodes.add(pluCodeField);
			BindUtils.postNotifyChange(null, null, this, "tempPluCodes");
		}

		// Cleaning
		setPluCodeField(null);
	}

	@Command
	public void deletePlu(@BindingParam("plu") final String plu) {
		// Removing a plu from view
		tempPluCodes.remove(plu);
		BindUtils.postNotifyChange(null, null, this, "tempPluCodes");
	}

	@Command
	public void deleteAllPlu() {
		// Removing all plus from view
		setTempPluCodes(new ArrayList<String>());
	}

	@Command
	public void confirmPlu() {
		// Closing edit details saving
		selectedArticle.setPluCodes(new ArrayList<String>());
		for (String pc : tempPluCodes) {
			selectedArticle.getPluCodes().add(pc);
		}
		setDisplayDetailsEdit(false);
	}

	public Converter<String, Integer, Component> getCentsToStringCurrencyConverter() {
		return new ZkCentsToStringCurrencyConverter();
	}

	private boolean showError() {
		boolean isInError = false;
		if (eo != null && eo.getDescription() != null && eo.getDescription().length() > 0) {
			if (eo.getErrorCode().equals(ErrorCode.OK_NO_ERROR)) {
				Messagebox.show(eo.getDescription(), "Information", Messagebox.OK, Messagebox.INFORMATION);
			} else if (eo.getErrorCode().equals(ErrorCode.OK_WARNING)) {
				Messagebox.show(eo.getDescription(), "Attention", Messagebox.OK, Messagebox.INFORMATION);
			} else {
				Messagebox.show(eo.getDescription(), "Error", Messagebox.OK, Messagebox.ERROR);
				isInError = true;
			}
		}
		return isInError;
	}

	@NotifyChange({ "articleCodeTextSearchField", "articleCodeSearchField", "pluCodeSearchField", "descriptionSearchField" })
	public void setArticleCodeSearchField(Long articleCodeSearchField) {
		this.articleCodeSearchField = articleCodeSearchField;
		pluCodeSearchField = "";
		descriptionSearchField = "";
		articleCodeTextSearchField = "";
	}

	public Long getArticleCodeSearchField() {
		return articleCodeSearchField;
	}

	@NotifyChange({ "articleCodeTextSearchField", "articleCodeSearchField", "pluCodeSearchField", "descriptionSearchField" })
	public void setArticleCodeTextSearchField(String articleCodeTextSearchField) {
		articleCodeSearchField = null;
		this.articleCodeTextSearchField = articleCodeTextSearchField;
		descriptionSearchField = "";
		pluCodeSearchField = "";
	}

	public String getArticleCodeTextSearchField() {
		return articleCodeTextSearchField;
	}

	@NotifyChange({ "articleCodeTextSearchField", "articleCodeSearchField", "pluCodeSearchField", "descriptionSearchField" })
	public void setPluCodeSearchField(String pluCodeSearchField) {
		articleCodeSearchField = null;
		this.pluCodeSearchField = pluCodeSearchField;
		descriptionSearchField = "";
		articleCodeTextSearchField = "";
	}

	public String getPluCodeSearchField() {
		return pluCodeSearchField;
	}

	@NotifyChange({ "articleCodeTextSearchField", "articleCodeSearchField", "pluCodeSearchField", "descriptionSearchField" })
	public void setDescriptionSearchField(String descriptionSearchField) {
		articleCodeSearchField = null;
		pluCodeSearchField = "";
		articleCodeTextSearchField = "";
		this.descriptionSearchField = descriptionSearchField;
	}

	public String getDescriptionSearchField() {
		return descriptionSearchField;
	}

	public String getPluCodeField() {
		return pluCodeField;
	}

	@NotifyChange({ "pluCodeField" })
	public void setPluCodeField(String pluCodeField) {
		this.pluCodeField = pluCodeField;
		BindUtils.postNotifyChange(null, null, this, "pluCodeField");
	}

	public List<String> getTempPluCodes() {
		return tempPluCodes;
	}

	public void setTempPluCodes(List<String> tempPluCodes) {
		this.tempPluCodes = tempPluCodes;
		BindUtils.postNotifyChange(null, null, this, "tempPluCodes");
	}

	public double getFormattedPrice() {
		return (selectedArticle != null) ? selectedArticle.getPrice() / ((double) 100.0) : -1;
	}

	public void setFormattedPrice(double formattedPrice) {
		selectedArticle.setPrice((int) (formattedPrice * ((double) 100.0)));
	}

	public List<Article> getArticleList() {
		logger.info("getArticleList - articleList.size() size = " + articleList.size());
		return articleList;
	}

	public void setSelectedArticle(Article selectedArticle) {
		this.selectedArticle = selectedArticle;
		BindUtils.postNotifyChange(null, null, this, "selectedArticle");

		// Initialize selected objects related to the article
		if (selectedArticle != null) {
			initializeObjects();
		}
	}

	public Article getSelectedArticle() {
		return selectedArticle;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public boolean isDisplayEdit() {
		return displayEdit;
	}

	public void setDisplayEdit(boolean displayEdit) {
		this.displayEdit = displayEdit;
		BindUtils.postNotifyChange(null, null, this, "displayEdit");
		WebFrontCore.setCurrentFunctionModfied(displayEdit || displayAdd);
	}

	public boolean isDisplayAdd() {
		return displayAdd;
	}

	public void setDisplayAdd(boolean displayAdd) {
		this.displayAdd = displayAdd;
		BindUtils.postNotifyChange(null, null, this, "displayAdd");
		WebFrontCore.setCurrentFunctionModfied(displayEdit || displayAdd);
	}

	public boolean isDisplayDetailsEdit() {
		return displayDetailsEdit;
	}

	public void setDisplayDetailsEdit(boolean displayDetailsEdit) {
		this.displayDetailsEdit = displayDetailsEdit;
		BindUtils.postNotifyChange(null, null, this, "displayDetailsEdit");
	}

	public Map<String, Boolean> getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Map<String, Boolean> capabilities) {
		this.capabilities = capabilities;
	}

	public PosDepartment getPosDpt() {
		return posDpt;
	}

	@NotifyChange({ "posDpt" })
	public void setPosDpt(PosDepartment posDpt) {
		this.posDpt = posDpt;
		selectedArticle.setPosDpt(posDpt.getCode());
		posDpt.setDescription(posDpt.getDescription());
		BindUtils.postNotifyChange(null, null, this, "posDpt");
	}

	public List<PosDepartment> getPosDptList() {
		return posDptList;
	}

	public void setPosDptList(List<PosDepartment> posDptList) {
		this.posDptList = posDptList;
	}

	public Vat getVat() {
		return vat;
	}

	@NotifyChange({ "vat" })
	public void setVat(Vat vat) {
		this.vat = vat;
		selectedArticle.setVatId(vat.getVatId());
		BindUtils.postNotifyChange(null, null, this, "vat");
	}

	public List<Vat> getVatList() {
		return vatList;
	}

	public void setVatList(List<Vat> vatList) {
		this.vatList = vatList;
	}

	public MutablePair<Integer, String> getManualDicountCode() {
		return manualDicountCode;
	}

	@NotifyChange({ "manualDicountCode" })
	public void setManualDicountCode(MutablePair<Integer, String> manualDicountCode) {
		this.manualDicountCode = manualDicountCode;
		selectedArticle.setManualDiscountId(manualDicountCode.getKey());
		BindUtils.postNotifyChange(null, null, this, "manualDicountCode");
	}

	public List<MutablePair<Integer, String>> getManualDicountCodeList() {
		return manualDicountCodeList;
	}

	public void setManualDicountCodeList(List<MutablePair<Integer, String>> manualDicountCodeList) {
		this.manualDicountCodeList = manualDicountCodeList;
	}

	public Tare getTare() {
		return tare;
	}

	@NotifyChange({ "tare" })
	public void setTare(Tare tare) {
		this.tare = tare;
		selectedArticle.setTareId(tare.getTareId());
		BindUtils.postNotifyChange(null, null, this, "tare");
	}

	public List<Tare> getTareList() {
		return tareList;
	}

	public void setTareList(List<Tare> tareList) {
		this.tareList = tareList;
	}

	public MutablePair<String, String> getPackagingType() {
		return packagingType;
	}

	@NotifyChange({ "packagingType" })
	public void setPackagingType(MutablePair<String, String> packagingType) {
		this.packagingType = packagingType;
		selectedArticle.setPackagingType(packagingType.getKey());
		BindUtils.postNotifyChange(null, null, this, "packagingType");
	}

	public List<MutablePair<String, String>> getPackagingTypeList() {
		return packagingTypeList;
	}

	public void setPackagingTypeList(List<MutablePair<String, String>> packagingTypeList) {
		this.packagingTypeList = packagingTypeList;
	}

	public MutablePair<Integer, String> getPrizeCode() {
		return prizeCode;
	}

	@NotifyChange({ "prizeCode" })
	public void setPrizeCode(MutablePair<Integer, String> prizeCode) {
		this.prizeCode = prizeCode;
		selectedArticle.setPrizeCode(prizeCode.getKey());
		BindUtils.postNotifyChange(null, null, this, "prizeCode");
	}

	public List<MutablePair<Integer, String>> getPrizeCodeList() {
		return prizeCodeList;
	}

	public void setPrizeCodeList(List<MutablePair<Integer, String>> prizeCodeList) {
		this.prizeCodeList = prizeCodeList;
	}

	public MutablePair<Integer, String> getZeroPriceBehavior() {
		return zeroPriceBehavior;
	}

	@NotifyChange({ "zeroPriceBehavior" })
	public void setZeroPriceBehavior(MutablePair<Integer, String> zeroPriceBehavior) {
		this.zeroPriceBehavior = zeroPriceBehavior;
		selectedArticle.setZeroPriceBehavior(zeroPriceBehavior.getKey());
		BindUtils.postNotifyChange(null, null, this, "zeroPriceBehavior");
	}

	public List<MutablePair<Integer, String>> getZeroPriceBehaviorList() {
		return zeroPriceBehaviorList;
	}

	public void setZeroPriceBehaviorList(List<MutablePair<Integer, String>> zeroPriceBehaviorList) {
		this.zeroPriceBehaviorList = zeroPriceBehaviorList;
	}

	public MutablePair<Integer, String> getAutoDiscountType() {
		return autoDiscountType;
	}

	@NotifyChange({ "autoDiscountType" })
	public void setAutoDiscountType(MutablePair<Integer, String> autoDiscountType) {
		this.autoDiscountType = autoDiscountType;
		selectedArticle.setAutomDiscountType(autoDiscountType.getKey());
		BindUtils.postNotifyChange(null, null, this, "autoDiscountType");
	}

	public List<MutablePair<Integer, String>> getAutoDiscountTypeList() {
		return autoDiscountTypeList;
	}

	public void setAutoDiscountTypeList(List<MutablePair<Integer, String>> autoDiscountTypeList) {
		this.autoDiscountTypeList = autoDiscountTypeList;
	}

	public MutablePair<Integer, String> getUpbOperationType() {
		return upbOperationType;
	}

	@NotifyChange({ "upbOperationType" })
	public void setUpbOperationType(MutablePair<Integer, String> upbOperationType) {
		this.upbOperationType = upbOperationType;
		selectedArticle.setUpbOperationType(upbOperationType.getKey());
		BindUtils.postNotifyChange(null, null, this, "upbOperationType");
	}

	public List<MutablePair<Integer, String>> getUpbOperationTypeList() {
		return upbOperationTypeList;
	}

	public void setUpbOperationTypeList(List<MutablePair<Integer, String>> upbOperationTypeList) {
		this.upbOperationTypeList = upbOperationTypeList;
	}

	private String getLoggedInUserName() {
		LoginManagerInterface loginManager = WebFrontCore.getLoginManager();
		String loggedInUserName = String.valueOf(WebFrontCore.getLoginDataLoader().getLoginData(loginManager.getLoggedInUserId()).getUserName());
		return loggedInUserName;
	}
	
	
	
	/*
	public String getTitlePage() {
		return WebFrontArticleSettings.getInstance().getTitlePage();
	}
	
	public String getCode() {
		return WebFrontArticleSettings.getInstance().getCode();	
	}
	
	public String getDescription() {
		return WebFrontArticleSettings.getInstance().getDescription();
	}

	public String getCodePlu() {
		return WebFrontArticleSettings.getInstance().getCodePlu();
	}

	public String getDetails() {
		return WebFrontArticleSettings.getInstance().getDetails();
	}

	public String getArticle() {
		return WebFrontArticleSettings.getInstance().getArticle();
	}

	public String getSearch() {
		return WebFrontArticleSettings.getInstance().getSearch();
	}

	public String getAdd() {
		return WebFrontArticleSettings.getInstance().getAdd();
	}

	public String getPrice() {
		return WebFrontArticleSettings.getInstance().getPrice();
	}

	public String getEdit() {
		return WebFrontArticleSettings.getInstance().getEdit();
	}

	public String getDelete() {
		return WebFrontArticleSettings.getInstance().getDelete();
	}

	public String getDepartment() {
		return WebFrontArticleSettings.getInstance().getDepartment();
	}

	public String getCodeVat() {
		return WebFrontArticleSettings.getInstance().getCodeVat();
	}

	public String getDepository() {
		return WebFrontArticleSettings.getInstance().getDepository();
	}

	public String getManagePlu() {
		return WebFrontArticleSettings.getInstance().getManagePlu();
	}

	public String getWeight() {
		return WebFrontArticleSettings.getInstance().getWeight();
	}

	public String getDecimal() {
		return WebFrontArticleSettings.getInstance().getDecimal();
	}
	
	public String getManualDiscount() {
		return WebFrontArticleSettings.getInstance().getManualDiscount();
	}

	public String getTareLbl() {
		return WebFrontArticleSettings.getInstance().getTare();
	}

	public String getMixCode() {
		return WebFrontArticleSettings.getInstance().getMixCode();
	}

	public String getPkg() {
		return WebFrontArticleSettings.getInstance().getPkg();
	}

	public String getPieces() {
		return WebFrontArticleSettings.getInstance().getPieces();
	}

	public String getLkdArt() {
		return WebFrontArticleSettings.getInstance().getLinkedArticle();
	}

	public String getConfirm() {
		return WebFrontArticleSettings.getInstance().getConfirm();
	}

	public String getAnnul() {
		return WebFrontArticleSettings.getInstance().getAnnul();
	}
	
	public String getDeleteAll() {
		return WebFrontArticleSettings.getInstance().getDeleteAll();	
	}
	
	public String getNoPlu() {
		return WebFrontArticleSettings.getInstance().getNoPlu();
	}
	
	public String getAction() {
		return WebFrontArticleSettings.getInstance().getAction();
	}
*/
	public String getNoResults() {
		return WebFrontArticleSettings.getInstance().getNoResults();
	}
	
	public String getNegPrice() {
		return WebFrontArticleSettings.getInstance().getNegPrice();
	}


	private void logEventArticlePriceChanged() {
		logger.info("BEGIN");
		if (previousArticlePrice != selectedArticle.getPrice()) {
			String area = "ArticleMaintenance";
			String previousPrice = new ZkCentsToStringCurrencyConverter().coerceToUi(previousArticlePrice, null, null);
			String newPrice = new ZkCentsToStringCurrencyConverter().coerceToUi(selectedArticle.getPrice(), null, null);
			String description = "Modifica al prezzo dell'articolo " + selectedArticle.getArticleCode() + " (" + selectedArticle.getDescription() + ") da "
			      + previousPrice + " a " + newPrice;
			WebFrontEvent event = new WebFrontEvent(getLoggedInUserName(), area, description, "detailType", "detail");
			EventLoggerService.getInstance().logEvent(event);
		}
		logger.info("END");
	}

	private final Logger logger = WebFrontLogger.getLogger(ArticleMaintenanceViewModel.class);

	private Long articleCodeSearchField;
	private String pluCodeSearchField;
	private String descriptionSearchField;
	private String articleCodeTextSearchField;

	private String pluCodeField;
	private List<String> tempPluCodes = new ArrayList<String>();

	private List<Article> articleList;
	private Article selectedArticle;
	private int previousArticlePrice;

	private ErrorObject eo;

	private boolean readOnly = false;
	private boolean displayEdit = false;
	private boolean displayAdd = false;
	private boolean displayDetailsEdit = false;

	private Map<String, Boolean> capabilities;

	private PosDepartment posDpt;
	private List<PosDepartment> posDptList;
	private Vat vat;
	private List<Vat> vatList;
	private Tare tare;
	private List<Tare> tareList;
	private MutablePair<Integer, String> manualDicountCode;
	private List<MutablePair<Integer, String>> manualDicountCodeList;
	private MutablePair<String, String> packagingType;
	private List<MutablePair<String, String>> packagingTypeList;
	private MutablePair<Integer, String> prizeCode;
	private List<MutablePair<Integer, String>> prizeCodeList;
	private MutablePair<Integer, String> zeroPriceBehavior;
	private List<MutablePair<Integer, String>> zeroPriceBehaviorList;
	private MutablePair<Integer, String> autoDiscountType;
	private List<MutablePair<Integer, String>> autoDiscountTypeList;
	private MutablePair<Integer, String> upbOperationType;
	private List<MutablePair<Integer, String>> upbOperationTypeList;

	private ArticleServiceInterface articleService;
	private PosDepartmentServiceInterface posDptService;
	private VatServiceInterface vatService;
	private TareServiceClientImpl tareService;
	private CommonListsServiceInterface commonListsService;

}
