package com.ncr.webfront.base.functionalities.articlemaintenance;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontArticleSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontArticleSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontArticleSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontArticleSettings("articlemaintenance.properties");
		}
		return instance;
	}

	protected WebFrontArticleSettings(String propertyFileName) {
		super(propertyFileName);
	}

	// TODO test settings

	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.list-title", "Article list");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getCode() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.code", "Code");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDescription() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.description", "Description");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getCodePlu() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.codeplu", "Code plu");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDetails() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.details", "Details");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getArticle() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.article", "Article");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getSearch() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.search", "Search");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getAdd() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.add", "Add");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPrice() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.price", "Price");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getEdit() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.edit", "Edit");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDelete() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.delete", "Delete");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDepartment() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.department", "Department");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getCodeVat() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.codevat", "Code VAT");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDepository() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.depository", "Depository");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getManagePlu() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.manageplu", "Manage PLU");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getWeight() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.weight", "Weight");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDecimal() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.decimal", "Decimal");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getManualDiscount() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.manualdiscount", "Manual Discount");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getTare() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.tare", "Tare");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getMixCode() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.mixcode", "Mix code");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPkg() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.pkg", "Package");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPieces() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.pieces", "Pieces");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getLinkedArticle() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.linkedarticle", "Linked article");		
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getConfirm() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.confirm", "Confirm");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getAnnul() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.annul", "Annul");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDeleteAll() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.deleteall", "Delete All");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getNoPlu() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.noplu", "No Plu");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getAction() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.action", "Action");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getNoResults() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.noresults", "No article in the results list");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getNegPrice() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.articlemaintenance.negprice", "Negative Price");
		logger.debug("END (" + value + ")");
		return value;
	}
}
