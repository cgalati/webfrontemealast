package com.ncr.webfront.base.functionalities.articlemaintenance;

import java.util.List;

import org.zkoss.bind.ValidationContext;

import com.ncr.webfront.base.restclient.articleservice.ArticleServiceClientImpl;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;
import com.ncr.webfront.core.utils.validation.WebFrontValidator;

public class ArticleValidator extends WebFrontValidator {
	@Override
	protected Object getBean(ValidationContext context) {
		return context.getValidatorArg("articleToValidate");
	}
	
	@Override
	protected List<WebFrontValidationData> getValidationData() {
		return new ArticleServiceClientImpl().getValidationData();
	}
}
