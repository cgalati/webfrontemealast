package com.ncr.webfront.base.functionalities.reportposoperator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.Init;

import com.ncr.webfront.base.functionalities.reportfinancial.WebFrontReportFinancialSettings;
import com.ncr.webfront.base.functionalities.reports.ReportBaseViewModel;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;

public class ReportPosOperatorViewModel extends ReportBaseViewModel {
	@Override
	@Init
	// Sadly needed.
	public void init() {
		super.init();
	}
	
	@Override
	public void postInit() {
		super.postInit();
		
		PosOperatorServiceInterface posOperatorService = new PosOperatorServiceClientImpl();
		
		posOperatorList = posOperatorService.getAll();
		selectedPosOperatorList = new HashSet<>();
	}
	
	@Override
	protected void loadOptions(ReportOptions reportOptions) {
		super.loadOptions(reportOptions);
		for (PosOperator posOperator : selectedPosOperatorList) {
			reportOptions.getPosOperators().add(posOperator.getOperatorCode());
		}
	}
	
	@Override
	protected byte[] generateReport(ReportOptions reportOptions) {
		return reportsService.generatePosOperatorReport(reportOptions);
	}
	
	public List<PosOperator> getPosOperatorList() {
		return posOperatorList;
	}

	public void setPosOperatorList(List<PosOperator> posOperatorList) {
		this.posOperatorList = posOperatorList;
	}
	
	public Set<PosOperator> getSelectedPosOperatorList() {
		return selectedPosOperatorList;
	}

	public void setSelectedPosOperatorList(Set<PosOperator> selectedPosOperatorList) {
		this.selectedPosOperatorList = selectedPosOperatorList;
	}
	
	
	/*
	public String getTitlePage() {
		return WebFrontReportOperatorSettings.getInstance().getTitlePage();
	}

	public String getConsolidated() {
		return WebFrontReportOperatorSettings.getInstance().getConsolidated();
	}
	
	public String getDateLbl() {
		return WebFrontReportOperatorSettings.getInstance().getDateLbl();
	}

	public String getTerminals() {
		return WebFrontReportOperatorSettings.getInstance().getTerminals();
	}

	public String getOperators() {
		return WebFrontReportOperatorSettings.getInstance().getOperators();
	}

	public String getFormatLbl() {
		return WebFrontReportOperatorSettings.getInstance().getFormatLbl();
	}*/

	private List<PosOperator> posOperatorList;
	private Set<PosOperator> selectedPosOperatorList;
}
