package com.ncr.webfront.base.functionalities.reportposoperator;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontReportOperatorSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontReportOperatorSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontReportOperatorSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontReportOperatorSettings("reports.properties");
		}
		return instance;
	}

	protected WebFrontReportOperatorSettings(String propertyFileName) {
		super(propertyFileName);
	}
			
	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperator.title", "Operators Report");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getConsolidated() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperator.consolidated", "Consolidated");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDateLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperator.date", "Date");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getOperators() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperator.operators", "Operators");
		logger.debug("END (" + value + ")");
		return value;
	}	

	public String getTerminals() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperator.terminals", "Terminals");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getFormatLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperator.format", "Format");
		logger.debug("END (" + value + ")");
		return value;
	}
}
