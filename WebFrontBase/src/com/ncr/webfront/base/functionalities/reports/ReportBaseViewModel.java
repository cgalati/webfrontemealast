package com.ncr.webfront.base.functionalities.reports;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.functionalities.reportposoperator.ReportPosOperatorViewModel;
import com.ncr.webfront.base.functionalities.reportposoperatorcash.WebFrontReportOperatorCashSettings;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.reports.ReportsServiceInterface;
import com.ncr.webfront.base.restclient.posterminalservice.PosTerminalServiceClientImpl;
import com.ncr.webfront.base.restclient.reportsservice.ReportsServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class ReportBaseViewModel {
	public void init() {
		ErrorObject errorObject = reportsService.checkService();

		if ((errorObject == null) || errorObject.getErrorCode().isOk()) {
			postInit();
		} else {
			MainComposer.notifyServiceProblems("ReportsService", errorObject.getDescription());
			MainComposer.goToHome();
		}
	}

	public void postInit() {
		PosTerminalServiceInterface posTerminalService = new PosTerminalServiceClientImpl();

		adjustDate();
		posTerminalList = posTerminalService.getAll();
		selectedPosTerminalList = new HashSet<>();
		formatList = reportsService.getFormats();
		selectedFormat = formatList.get(1); // pdf preselection
	}

	@Command
	@NotifyChange({ "date", "dateConstraint", "dateDisabled" })
	public void adjustDate() {
		if (consolidated) {
			setDateConstraint("no future");
		} else {
			setDateConstraint("no future, no past");
		}
		date = new Date();
		dateDisabled = !consolidated;
	}

	@Command
	@NotifyChange("report")
	public void generate() {
		ReportOptions reportOptions = new ReportOptions();

		loadOptions(reportOptions);
		
		logger.debug(String.format("BEGIN report with options [%s]", reportOptions));
		byte[] reportResult = generateReport(reportOptions);
		if (selectedFormat.equalsIgnoreCase("html")) {
			report = new AMedia("report.html", "html", "text/html", reportResult);
		} else {
			// AMZ-XLS#BEG
			if (selectedFormat.equalsIgnoreCase("pdf")) {
				// AMZ-XLS#END
				report = new AMedia("report.pdf", "pdf", "application/pdf", reportResult);
				// AMZ-XLS#BEG
			}
			if (selectedFormat.equalsIgnoreCase("xls")) {
				report = new AMedia("report.xls", "xls", "application/file", reportResult);
			}
			if (selectedFormat.equalsIgnoreCase("csv")) {
				report = new AMedia("report.csv", "csv", "application/file", reportResult);
			}
			if (selectedFormat.equalsIgnoreCase("excel")) {
				report = new AMedia("report.xls", "xls", "application/file", reportResult);
			}
			// AMZ-XLS#END
		}
		logger.debug(String.format("END report with options [%s]", reportOptions));
	}

	protected void loadOptions(ReportOptions reportOptions) {
		reportOptions.setDate(date);
		for (PosTerminal posTerminal : selectedPosTerminalList) {
			reportOptions.getPosTerminals().add(posTerminal.getTerminalCode());
		}
		reportOptions.setFormat(selectedFormat);
		reportOptions.setConsolidated(consolidated);
	}

	protected abstract byte[] generateReport(ReportOptions reportOptions);

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDateConstraint() {
		return dateConstraint;
	}

	public void setDateConstraint(String dateConstraint) {
		this.dateConstraint = dateConstraint;
	}

	public boolean isDateDisabled() {
		return dateDisabled;
	}

	public void setDateDisabled(boolean dateDisabled) {
		this.dateDisabled = dateDisabled;
	}

	public List<PosTerminal> getPosTerminalList() {
		return posTerminalList;
	}

	public void setPosTerminalList(List<PosTerminal> posTerminalList) {
		this.posTerminalList = posTerminalList;
	}

	public Set<PosTerminal> getSelectedPosTerminalList() {
		return selectedPosTerminalList;
	}

	public void setSelectedPosTerminalList(Set<PosTerminal> selectedPosTerminalList) {
		this.selectedPosTerminalList = selectedPosTerminalList;
	}

	public boolean isConsolidated() {
		return consolidated;
	}

	public void setConsolidated(boolean consolidated) {
		this.consolidated = consolidated;
	}

	public List<String> getFormatList() {
		return formatList;
	}

	public void setFormatList(List<String> formatList) {
		this.formatList = formatList;
	}

	public String getSelectedFormat() {
		return selectedFormat;
	}

	public void setSelectedFormat(String selectedFormat) {
		this.selectedFormat = selectedFormat;
	}

	public AMedia getReport() {
		return report;
	}

	public void setReport(AMedia report) {
		this.report = report;
	}

	public String getReportHeight() {
		final int DEFAULT_HEIGTH = 410;
		int reportHeight = DEFAULT_HEIGTH;
		try {
			int screenHeight = (int) Executions.getCurrent().getDesktop().getAttribute("height");
			logger.info("screenHeight = " + screenHeight);
			reportHeight = screenHeight - 200;
			logger.info("reportHeight = " + reportHeight);
		} catch (Exception e) {
			logger.info("exception!", e);
			reportHeight = DEFAULT_HEIGTH;
		}

		logger.info("reportHeight = " + reportHeight);
		return reportHeight + "px";
	}

	
	public String getTitlePage() {
		return WebFrontReportBaseSettings.getInstance().getTitlePage();
	}

	public String getFromLbl() {
		return WebFrontReportBaseSettings.getInstance().getFromLbl();
	}
	
	public String getToLbl() {
		return WebFrontReportBaseSettings.getInstance().getToLbl();
	}

	public String getTypeLbl() {
		return WebFrontReportBaseSettings.getInstance().getTypeLbl();
	}

	public String getDetail() {
		return WebFrontReportBaseSettings.getInstance().getDetail();
	}

	public String getNoPromotions() {
		return WebFrontReportBaseSettings.getInstance().getNoPromotions();
	}
	
	
	
	protected ReportsServiceInterface reportsService = new ReportsServiceClientImpl();

	private Date date;
	private String dateConstraint;
	private boolean dateDisabled = true;
	private List<PosTerminal> posTerminalList;
	private Set<PosTerminal> selectedPosTerminalList;
	private boolean consolidated;
	private List<String> formatList;
	private String selectedFormat;
	private AMedia report;

	private final Logger logger = WebFrontLogger.getLogger(ReportPosOperatorViewModel.class);
}
