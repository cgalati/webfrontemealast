package com.ncr.webfront.base.functionalities.reports;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontReportBaseSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontReportBaseSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontReportBaseSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontReportBaseSettings("reports.properties");
		}
		return instance;
	}

	protected WebFrontReportBaseSettings(String propertyFileName) {
		super(propertyFileName);
	}
			
	
	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportpromotions.title", "Operators Report");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getFromLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportpromotions.from", "From");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getToLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportpromotions.to", "To");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getTypeLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportpromotions.type", "Type");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getDetail() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportpromotions.detail", "Detail");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getNoPromotions() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportpromotions.nopromotions", "No promotions in the results list");
		logger.debug("END (" + value + ")");
		return value;
	}

}
