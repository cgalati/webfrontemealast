package com.ncr.webfront.base.functionalities.invoicereceiptsearch;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.ValidationContext;

import com.ncr.webfront.core.utils.validation.InvoiceReceiptSearchValidationData;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;
import com.ncr.webfront.core.utils.validation.WebFrontValidator;

public class InvoiceReceiptSearchValidator extends WebFrontValidator {
	@Override
	protected Object getBean(ValidationContext context) {
		return context.getValidatorArg("fieldToValidate");
	}
	
	@Override
	protected List<InvoiceReceiptSearchValidationData> getValidationData() {
		List<InvoiceReceiptSearchValidationData> rules = new ArrayList<>();
		WebFrontValidationData fidelityCodeUsed = new WebFrontValidationData().notBlank();
		
		rules.add((InvoiceReceiptSearchValidationData) new InvoiceReceiptSearchValidationData().length(12).number().when(fidelityCodeUsed));
		
		return rules;
	}
	
	@Override
	protected String getPropertyName(WebFrontValidationData validation) {
		return "fidelityCode";
	}
}
