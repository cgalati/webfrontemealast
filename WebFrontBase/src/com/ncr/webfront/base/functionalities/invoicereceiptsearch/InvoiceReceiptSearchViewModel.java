package com.ncr.webfront.base.functionalities.invoicereceiptsearch;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSale;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSearchType;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.restclient.invoicecreditnoteservice.InvoiceCreditNoteServiceClientImpl;
import com.ncr.webfront.base.restclient.posterminalservice.PosTerminalServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.zkconverters.ZkCentsToStringCurrencyConverter;


public class InvoiceReceiptSearchViewModel {
	@Init
	public void init() {
		ErrorObject errorObject = invoiceService.checkService();
		
		if ((errorObject == null) || errorObject.getErrorCode().isOk()) {
			postInit();
		} else {
			MainComposer.notifyServiceProblems("InvoiceReceiptSearchService", errorObject.getDescription());
			MainComposer.goToHome();
		}
	}
	
	public void postInit() {
		terminalList = posTerminalService.getAll();
		selectedTerminal = terminalList.get(0);
	}
	
	public List<PosTerminal> getTerminalList() {
		return terminalList;
	}
	
	public void setTerminalList(List<PosTerminal> terminalList) {
		//add an empty terminal
		this.terminalList = new ArrayList<PosTerminal>();
		this.terminalList.add(new PosTerminal());
		this.terminalList.addAll(terminalList);
		BindUtils.postNotifyChange(null, null, this, "terminalList");
	}
	
	public PosTerminal getSelectedTerminal() {
		return selectedTerminal;
	}

	@NotifyChange({ "selectedTerminal" })
	public void setSelectedTerminal(PosTerminal selectedTerminal) {
		this.selectedTerminal = selectedTerminal;
		BindUtils.postNotifyChange(null, null, this, "selectedTerminal");
	}
	
	public String getReceiptSearchField() {
		return receiptSearchField;
	}

	@NotifyChange({ "receiptSearchField" })
	public void setReceiptSearchField(String receiptSearchField) {
		this.receiptSearchField = receiptSearchField;
		BindUtils.postNotifyChange(null, null, this, "receiptSearchField");
	}
	
	public String getFidelitySearchField() {
		return fidelitySearchField;
	}

	@NotifyChange({ "fidelitySearchField" })
	public void setFidelitySearchField(String fidelitySearchField) {
		this.fidelitySearchField = fidelitySearchField;
		BindUtils.postNotifyChange(null, null, this, "fidelitySearchField");
	}
	
	public List<Receipt> getReceiptList() {
		return receiptList;
	}

	public void setReceiptList(List<Receipt> receiptList) {
		this.receiptList = receiptList;
	}

	public Receipt getSelectedReceipt() {
		return selectedReceipt;
	}

	public void setSelectedReceipt(Receipt selectedReceipt) {
		this.selectedReceipt = selectedReceipt;
	}
	
	@NotifyChange({"receiptList", "selectedReceipt", "selectedReceiptItems"})
	@Command
	public void searchReceipt() {
		//check if at least one search parameter is defined
		String localReceiptSearchField = receiptSearchField == null ? "0":receiptSearchField;
		String localFidelityCode = fidelitySearchField == null ? "" : fidelitySearchField;
		if (selectedTerminal.getTerminalCode() == null && localReceiptSearchField.equals("0") && localFidelityCode.equals("") ){
			Clients.alert("Please insert at least one search criteria");
		} else {
			try {
				ReceiptSearchType receiptSearchType = new ReceiptSearchType();
				receiptSearchType.setPosTerminalCode(selectedTerminal.getTerminalCode());
				receiptSearchType.setReceiptNumber(new Integer(localReceiptSearchField));
				receiptSearchType.setFidelityCode(localFidelityCode);
				receiptList = invoiceService.searchReceipts(receiptSearchType);	
				log.debug(String.format("[%d] Receipts found for terminal code [%s], receipt number [%s]", receiptList.size(),
						selectedTerminal.getTerminalCode(), localReceiptSearchField));
			} catch (Exception e){
				log.error("", e);
				
				Clients.showNotification("An error has occurred during search.\n" + e.getMessage());
			}
			BindUtils.postNotifyChange(null, null, this, "receiptList");
		}
		
		selectedReceipt = null;
		selectedReceiptItems.clear();
	}
	
	@Command
	@NotifyChange({"selectedReceipt", "selectedReceiptItems", "selectedReceiptItemsEmpty"})
	public void selectReceipt() {
		ReceiptSearchType receiptSearchType = new ReceiptSearchType();
		
		receiptSearchType.setPosTerminalCode(selectedReceipt.getPosTerminalCode());
		receiptSearchType.setReceiptNumber(selectedReceipt.getReceiptNumber());
		
		selectedReceipt.getSaleReceiptItems().clear();
		for (ReceiptSale item : invoiceService.fillReceipt(receiptSearchType).getSaleReceiptItems()) {
			// Modifing the currently selected receipt we prevent the selection from disappearing.
			// Substituting the receipt will make it so.
			selectedReceipt.getSaleReceiptItems().add(item);
			
			selectedReceiptItems.add(item.getSequenceNumber());
		}
	}
	
	@Command
	@NotifyChange("selectedReceiptItemsEmpty")
	public void toggleItemSelection(@BindingParam("sequenceNumber") int sequenceNumber) {
		if (selectedReceiptItems.contains(sequenceNumber)) {
			selectedReceiptItems.remove((Object) sequenceNumber);
		} else {
			selectedReceiptItems.add(sequenceNumber);
		}
	}
	
	@Command
	public void goToCustomer() {
		try {
			Receipt receipt = new Receipt();
			Document invoice = new Document();
			
			PropertyUtils.copyProperties(receipt, selectedReceipt);
			receipt.setSaleReceiptItems(new ArrayList<ReceiptSale>());
			for (ReceiptSale item : selectedReceipt.getSaleReceiptItems()) {
				if (selectedReceiptItems.contains(item.getSequenceNumber())) {
					receipt.getSaleReceiptItems().add(item);
					
					log.debug(String.format("adding sale receipt item [%d] to invoice", item.getSequenceNumber()));
				}
			}
			
			invoice = invoiceService.createInvoice(receipt);
			
			log.debug(String.format("invoice created [%s]", invoice));
			
			Sessions.getCurrent().setAttribute("fidelityCode", selectedReceipt.getFidelityCode());
			Sessions.getCurrent().setAttribute("invoice", invoice);
			MainComposer.goTo("invoicecustomermaintenance.zul");
		} catch (Exception e) {
			log.error("", e);
			
			Clients.showNotification("An error has occurred.\n" + e.getMessage());
		}
	}
	
	public Converter<String, Integer, Component> getCentsToStringCurrencyConverter() {
		return new ZkCentsToStringCurrencyConverter();
	}
	
	public boolean isSelectedReceiptItemsEmpty() {
		return selectedReceiptItems.isEmpty();
	}
	
	private InvoiceCreditNoteServiceInterface invoiceService = new InvoiceCreditNoteServiceClientImpl();
	private PosTerminalServiceInterface posTerminalService = new PosTerminalServiceClientImpl();
	private String receiptSearchField;
	private String fidelitySearchField;

	private List<PosTerminal> terminalList;
	private PosTerminal selectedTerminal;
	
	private List<Receipt> receiptList;
	private Receipt selectedReceipt;
	private List<Integer> selectedReceiptItems = new ArrayList<>();

	private static final Logger log = WebFrontLogger.getLogger(InvoiceReceiptSearchViewModel.class);
}
