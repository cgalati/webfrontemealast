package com.ncr.webfront.base.functionalities.posstatus;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GroupTerminal {
	String codeGroup;
	String descriptionGroup;
	Map<String, List<PosStatusDecoratedObject>> posStatusMap;  //lista di terminali per gruppo (contiene la lista per righe)
	int index;
	int line;
	String type;
	
	public GroupTerminal(String codeGroup, String descriptionGroup) {
		this.codeGroup = codeGroup;
		this.descriptionGroup = descriptionGroup;
		index = 1;
		line = 1;
		posStatusMap = new TreeMap<String, List<PosStatusDecoratedObject>>();
		type = "";
	}

	public String getCodeGroup() {
		return codeGroup;
	}

	public void setCodeGroup(String codeGroup) {
		this.codeGroup = codeGroup;
	}

	public String getDescriptionGroup() {
		return descriptionGroup;
	}

	public void setDescriptionGroup(String descriptionGroup) {
		this.descriptionGroup = descriptionGroup;
	}

	public Map<String, List<PosStatusDecoratedObject>> getPosStatusMap() {
		return posStatusMap;
	}

	public void setPosStatusMap(Map<String, List<PosStatusDecoratedObject>> map) {
		this.posStatusMap = map;
	}

	public void addPosStatusMap(String key, List<PosStatusDecoratedObject> value) {
		this.posStatusMap.put(key, value);		
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}

	public void addIndex() {
		this.index++;
	}	
	
	public int getLine() {
		return line;
	}

	public void addLine() {
		this.line++;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
