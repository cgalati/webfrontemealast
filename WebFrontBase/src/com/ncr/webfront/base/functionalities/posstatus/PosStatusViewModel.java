package com.ncr.webfront.base.functionalities.posstatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.util.Clients;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.posstatus.PosStatusServiceInterface;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.base.restclient.posstatusservice.PosStatusServiceClientImpl;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class PosStatusViewModel {
	
	@Init
	public void init() {
		logger.debug("Init");

		listGroup = new ArrayList<GroupTerminal>();	
		
		ErrorObject errorObject = posStatusservice.checkService();
		if (errorObject != null && !errorObject.getErrorCode().isOk()) {
			MainComposer.notifyServiceProblems("PosStatusService", errorObject.getDescription());
			MainComposer.goToHome();

			return;
		}
		
		
		numberOfGroups = loadListGroup();
	}

	private int loadListGroup() {
		GroupTerminal groupTerminal;
		List<PosStatusObject> serviceList = posStatusservice.getAllPosStatus();
		if (serviceList == null || serviceList.size() == 0) {
			logger.warn("Error reading POS status from server!");
			logger.warn("Clients.showNotification(\"Errore nella lettura dello stato delle casse\"...)");
			Clients.showNotification("error during the reading of registers status", Clients.NOTIFICATION_TYPE_WARNING, null, "middle_center", 3000);
			
			return numberOfGroups;
		}

		int groupSize = Integer.parseInt(PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.smallIconSizeThreshold));
		listGroup.clear();
		
		Map<String,ArrayList<PosStatusObject>> supportMap = new TreeMap<String, ArrayList<PosStatusObject>>();
		for (PosStatusObject object : serviceList) {
			ArrayList<PosStatusObject> supportList = supportMap.get(object.getGroupPos());
			if (supportList == null) {
				supportList = new ArrayList<PosStatusObject>();
				supportMap.put(object.getGroupPos(), supportList);
			}
			supportList.add(object);
		}
		
		for (String key : supportMap.keySet()) {
			ArrayList<PosStatusObject> supportList = supportMap.get(key);
			
			int size = Integer.parseInt(PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.iconsPerLine));
			String title = PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.groupTitleStub + key);
			groupTerminal = new GroupTerminal(key, title);
			int delta = size - (supportList.size() % size);
			List<PosStatusDecoratedObject> innerList = new ArrayList<PosStatusDecoratedObject>();
			
			for (int index = 0; index < supportList.size() + delta; index++) {				
				if (index % size == 0) {
					innerList = new ArrayList<PosStatusDecoratedObject>();
					groupTerminal.addPosStatusMap("group " + key  + " line " + index / size + 1, innerList);					
				}
				PosStatusDecoratedObject decorated;
				boolean small = supportList.size() + delta > groupSize;
				//groupTerminal.setSmallStoreSize(supportList.size() + delta <= groupSize);
				if (index < supportList.size()) {
					decorated = new PosStatusDecoratedObject(supportList.get(index), small);
				} else {
					PosStatusObject emptyObject = new PosStatusObject();
					emptyObject.setType(PosTerminalType.NONE);
					decorated = new PosStatusDecoratedObject(emptyObject, small);
				}
				innerList.add(decorated);
			}
			listGroup.add(groupTerminal);
		}
		
		for (int index = supportMap.keySet().size(); index <= 10; index++) {
			String key = String.format("%02d", index);
			groupTerminal = new GroupTerminal(key, "");
			List<PosStatusDecoratedObject> innerList = new ArrayList<PosStatusDecoratedObject>();
			groupTerminal.addPosStatusMap("group " + key  + " line " + 1, innerList);					
			listGroup.add(groupTerminal);
		}
		
		return supportMap.keySet().size();
	}
		
	@Command
	@NotifyChange({ "listGroup" })
	public List<GroupTerminal> reload() {
		logger.debug("Reload Data");
		
		loadListGroup();
		
		return listGroup;
	}
	
	public List<GroupTerminal> getListGroup() {
		return listGroup;
	}
	
	public int getNumberOfGroups(){
		return numberOfGroups;
	}
	
	private int numberOfGroups = 1;
	private final Logger logger = WebFrontLogger.getLogger(PosStatusViewModel.class);
	private PosStatusServiceInterface posStatusservice = new PosStatusServiceClientImpl();
	private List<GroupTerminal> listGroup;	  
}
