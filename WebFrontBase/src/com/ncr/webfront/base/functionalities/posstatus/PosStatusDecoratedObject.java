package com.ncr.webfront.base.functionalities.posstatus;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class PosStatusDecoratedObject {
	
	public PosStatusDecoratedObject(PosStatusObject object, boolean small) {
		this.object = object;
		this.small = small;
		buildInfo();
	}
	
	private void buildInfo() {				
		String status = SCLASS_NONE;		
		infoText = TEXT_NONE;
		minutesStatus = 0L;
		String time = "";
		String[] fastlanes = PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.fastlanes).split(",");
		minutesMaxPause = Integer.parseInt(PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.pauseFlashThreshold));
		minutesMaxOpen = Integer.parseInt(PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.inactiveFlashThreshold));
		
		maxMinutes = Integer.parseInt(PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.timeDisplayMaxMinutes));
		maxHours = Integer.parseInt(PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.timeDisplayMaxHours));
		
		String size = small ? SMALL_TERM_IMAGE : BIG_TERM_IMAGE;
		
		initTerminalStatus();		
		
		if (object.getDateTime() != null) {
			minutesStatus = compareTime(object.getDateTime());
		}
		
		if (!object.getType().equals(PosTerminalType.NONE)) {
			if (statusTerminal.equals("open")) {
				if (Arrays.asList(fastlanes).contains(object.getTerminalCode())) {
					status = SCLASS_APERTA_SELF;
				} else {
					status = SCLASS_APERTA;
					if (minutesStatus >= minutesMaxOpen) {
						status = SCLASS_APERTA_VUOTA;
						time = TIME;								
					}
				}				
				infoText = TEXT_APERTA;
			} else if (statusTerminal.equals("error")) {
				status = SCLASS_GUASTA;
				infoText = TEXT_GUASTA;
			} else if (statusTerminal.equals("closed")) {
				status = SCLASS_CHIUSA;
				infoText = TEXT_CHIUSA;
			} else if (statusTerminal.equals("pause")) {
				status = SCLASS_PAUSA;
				infoText = TEXT_PAUSA;
				if (minutesStatus >= minutesMaxPause) {
					time = TIME;
				}
			} else if (statusTerminal.equals("offline")) {
				status = SCLASS_OFFLINE;
				infoText = TEXT_OFFLINE;		
			}
		}		
		
		if (time.length() > 0) {
			sclass = BUTTON + status + time + size;
		}
		
		background = BUTTON + status + size;
		modalSclass = BUTTON + status + FRAME;
		
		PosOperatorServiceClientImpl posOperatorService = new PosOperatorServiceClientImpl();
		posOperatorService.getPosOperatorRoles();
		
		for (String code : object.getListOperatorCodes()) {
			PosOperator operator = posOperatorService.getByCode(code);
			if (operator != null) {
				object.getListOperatorNames().add(operator.getNameAndCode());
			} else {
				object.getListOperatorNames().add("");
			}
		}

		if (object.getOperatorCode() != null) {
			PosOperator temp = posOperatorService.getByCode(object.getOperatorCode());
			if (temp == null) {
				operator = new PosOperator("", "", "", "", "");
			} else {
				operator = new PosOperator(temp.getOperatorCode(), temp.getName(), temp.getPersonnelNo(), temp.getSecretNo(), temp.getRole());
			}
		}		
	}	
		
	public void initTerminalStatus() {				
		typeError = "";
		statusTerminal = "";
		
		if (object.getStatusCode() != null) {
			if (object.getStatusCode().equals("01")) {
				statusTerminal = "offline";
				if (object.getActionCode().equals("98")) {
					typeError = "Excluded from EOD";					
				}
			} else if (object.getStatusCode().equals("02")) { 
				statusTerminal = "error";
				typeError = "Total mismatch";				
			} else if (object.getActionCode() != null) { 
				if (object.getActionCode().equals("97")) {
					statusTerminal = "error";
					typeError = "Printer error";				
				} else if (object.getActionCode().equals("02") || object.getActionCode().equals("99") || (object.getActionCode().equals("00") && object.getOperatorCode().equals(NO_OPERATOR))) {
					statusTerminal = "closed";		
				} else if (object.getActionCode().equals("08")) {
					statusTerminal = "pause";		
				} else {
					statusTerminal = "open";
				}		
			}
		}		
	}

	private long compareTime(String dataTime) {
		
		
		String pattern = "yyMMddkkmm";
		long minuti = 0L;
		long ore = 0L;
		long giorni = 0L;
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String today = new SimpleDateFormat(pattern).format(new Date());
		
		try {			
			Date currentDate = simpleDateFormat.parse(today);
			Date startPause = simpleDateFormat.parse(dataTime);

			
			long diff = Math.abs(currentDate.getTime() - startPause.getTime());
			logger.debug("201404-POSSTATUS: currentDate = " + currentDate);
			logger.debug("201404-POSSTATUS: startPause  = " + startPause);
	
			minuti = diff / (60 * 1000);
		} catch (java.text.ParseException ex) {
		
		}

		setStringShow("");

		if (minuti >= 1) {
			setMinutesStatusShow("since " + minuti + " minute" + (minuti > 1 ? "s" : " "));
		} else {
			setMinutesStatusShow("less than a minute");
		}
		
		if (minuti >= maxMinutes) {
			ore = minuti / 60;
			setMinutesStatusShow("since " + ore + " hour" + (ore > 1 ? "s" : " "));
		}
		
		if ((minuti / 60) >= maxHours) {
			giorni = (minuti / 60) / 24;
			setMinutesStatusShow("since " + giorni + " day" + (giorni > 1 ? "s" : " "));
		}
		
		setStringShow("");
		
		if (statusTerminal.equals("pause")) {
			;
		} else {
			if (statusTerminal.equals("open")) {
				if (minuti > minutesMaxOpen) {
					minuti = minutesMaxOpen;
					
					setStringShow("Not used");
				} /*else {
					setStringShow("Aperta");
				}*/
			} else {
				setMinutesStatusShow("");
			}
		}
				
		return minuti;
	}
	
	public String getBackground() {
		return background;
	}

	public String getSclass() {
		return sclass;
	}

	public PosStatusObject getObject() {
		return object;
	}

	public void setObject(PosStatusObject object) {
		this.object = object;
	}

	public String getInfoText() {
		return infoText;
	}

	public void setInfoText(String infoText) {
		this.infoText = infoText;
	}

	public String getModalSclass() {
		return modalSclass;
	}

	public void setModalSclass(String modalSclass) {
		this.modalSclass = modalSclass;
	}

	public PosOperator getOperator() {
		return operator;
	}

	public void setOperator(PosOperator operator) {
		this.operator = operator;
	}
	
	public void setMinutesStatusShow(String minutesStatusShow) {
		this.minutesStatusShow = minutesStatusShow;
	}
	
	public String getMinutesStatusShow() {
		return minutesStatusShow;
	}
	
	public long getMinutesStatus() {
		return minutesStatus;
	}
	
	public String getStringShow() {
		return stringShow;
	}

	public void setStringShow(String stringShow) {
		this.stringShow = stringShow;
	}
	
	public String getTypeError() {
		return typeError;
	}
	
	public boolean getSmall() {
		return small;
	}
	private final Logger logger = WebFrontLogger.getLogger(PosStatusDecoratedObject.class);

	private boolean small;
	private PosStatusObject object;
	private String infoText;
	private String sclass;
	private String background;
	private String modalSclass;
	private PosOperator operator;
	private String typeError = "";
	private String statusTerminal = "";
	private long minutesStatus;
	private String minutesStatusShow;
	private int minutesMaxPause = 0;
	private int minutesMaxOpen = 0;
	private int maxMinutes = 0;
	private int maxHours = 0;
	private String stringShow = "";
	private static final String BUTTON = "button ";
	private static final String SCLASS_APERTA = "aperta-";
	private static final String SCLASS_APERTA_VUOTA = "aperta-vuota-";
	private static final String SCLASS_APERTA_SELF = "aperta-self-";
	private static final String SCLASS_GUASTA = "guasta-";
	private static final String SCLASS_OFFLINE = "offline-";
	private static final String SCLASS_CHIUSA = "chiusa-";
	private static final String SCLASS_PAUSA = "pausa-";
	private static final String SCLASS_NONE = "none-";
	private static final String TIME = "time-";
	
	private static final String TEXT_APERTA = "Open";
	private static final String TEXT_GUASTA = "Error";
	private static final String TEXT_OFFLINE = "Offline";
	private static final String TEXT_CHIUSA = "Closed";
	private static final String TEXT_PAUSA = "Pause";
	private static final String TEXT_NONE = "";

	private static final String FRAME = "frame";

	private static final String BIG_TERM_IMAGE = "100x100";  
	private static final String SMALL_TERM_IMAGE = "100x50";  		
	
	public static final String NO_OPERATOR = "000";
}
