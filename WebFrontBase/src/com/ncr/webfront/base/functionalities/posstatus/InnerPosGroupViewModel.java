package com.ncr.webfront.base.functionalities.posstatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zul.Window;

import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class InnerPosGroupViewModel {

	@Init
	public void init(@ExecutionArgParam("posGroupMap") Map<String, List<PosStatusDecoratedObject>> posGroupMap) {
		this.posGroupMap = posGroupMap;
	}

	public Map<String, List<PosStatusDecoratedObject>> getPosGroupMap() {
		return posGroupMap;
	}

	public void setPosGroupMap(Map<String, List<PosStatusDecoratedObject>> posGroupMap) {
		this.posGroupMap = posGroupMap;
	}

	public PosStatusDecoratedObject getSelectedPosTerminal() {
		return selectedPosTerminal;
	}

	public void setSelectedPosTerminal(PosStatusDecoratedObject selectedPosTerminal) {
		this.selectedPosTerminal = selectedPosTerminal;
	}

	@Command
	public void showModal(@BindingParam("terminal") PosStatusDecoratedObject term) {
		selectedPosTerminal = term;

		if (!term.getObject().getType().equals(PosTerminalType.NONE)) {
			try {
				final HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("selectedPosTerminal", term);

				Window modalDialog = (Window) Executions.createComponents("~./base/functionalities/posstatus/dialog.zul", null, map);
				modalDialog.doModal();
			} catch (Exception e) {
				logger.error(e);
			}

		}
	}

	private final Logger logger = WebFrontLogger.getLogger(InnerPosGroupViewModel.class);
	private PosStatusDecoratedObject selectedPosTerminal;
	private Map<String, List<PosStatusDecoratedObject>> posGroupMap;
}
