package com.ncr.webfront.base.functionalities.posstatus;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class CurrentTerminalViewModel {
	@Init
	public void init(
			@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("selectedPosTerminal") PosStatusDecoratedObject selectedPosTerminal) {

		Selectors.wireComponents(view, this, false);
		this.selectedPosTerminal = selectedPosTerminal;
	}
	
	@Command
	public void hideDialog() {		
		modalDialog.detach();
	}
	
	@Command
	@NotifyChange({ "detailVisible" })
	public void showList() {
		detailVisible = !detailVisible;		
	}

	public PosStatusDecoratedObject getSelectedPosTerminal() {
		return selectedPosTerminal;
	}

	public void setSelectedTerminal(PosStatusDecoratedObject selectedPosTerminal) {
		this.selectedPosTerminal = selectedPosTerminal;
	}

	public boolean getDetailVisible() {
		return detailVisible;
	}
	
	@Wire
	private Window modalDialog;
	private final Logger logger = WebFrontLogger
			.getLogger(CurrentTerminalViewModel.class);
	private PosStatusDecoratedObject selectedPosTerminal;	
	private boolean detailVisible = false;
	
}
