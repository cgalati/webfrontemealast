package com.ncr.webfront.base.functionalities.posstatus;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class PosStatusMappingProperties {
	
	public static final String smallIconSizeThreshold = "icon.SmallIconSize.threshold";
	public static final String smallIconSizeThresholdDefault = "10";
	
	public static final String fastlanes = "fastlanes";
	public static final String fastlanesDefault = "001";	

	public static final String pauseFlashThreshold = "pauseFlash.thresholdMinutes";
	public static final String pauseFlashThresholdDefault = "20";
	
	public static final String inactiveFlashThreshold = "inactiveFlash.thresholdMinutes";
	public static final String inactiveFlashThresholdDefault = "20";

	public static final String iconsPerLine = "icon.IconsPerLine";
	public static final String iconsPerLineDefault = "5";
	
	public static final String timeDisplayMaxMinutes = "timeDisplay.inMinutes.max";
	public static final String timeDisplayMaxMinutesDefault = "60";

	public static final String timeDisplayMaxHours = "timeDisplay.inHours.max";
	public static final String timeDisplayMaxHoursDefault = "24";
	

	public static final String groupTitleStub = "title.group";
		
	public static synchronized PosStatusMappingProperties getInstance() {
		if (instance == null) {
			instance = new PosStatusMappingProperties();
		}
		
		return instance;
	}
	
	private PosStatusMappingProperties() {
		posstatus = PropertiesLoader.loadOrCreatePropertiesFile(posStatusPropertiesFileName, getPosstatusPropertiesMap());
	}
	
	private static Map<String, String> getPosstatusPropertiesMap() {
		Map<String, String> posstatusPropertiesMap = new HashMap<>();
		
		posstatusPropertiesMap.put(smallIconSizeThreshold, smallIconSizeThresholdDefault);
		posstatusPropertiesMap.put(fastlanes, fastlanesDefault);
		posstatusPropertiesMap.put(pauseFlashThreshold, pauseFlashThresholdDefault);
		posstatusPropertiesMap.put(inactiveFlashThreshold, inactiveFlashThresholdDefault);
		posstatusPropertiesMap.put(iconsPerLine, iconsPerLineDefault);
		posstatusPropertiesMap.put(timeDisplayMaxMinutes, timeDisplayMaxMinutesDefault);
		posstatusPropertiesMap.put(timeDisplayMaxHours, timeDisplayMaxHoursDefault);
		
		return posstatusPropertiesMap;
	}
	
	public String getProperty(String key) {
		return getProperty(key, false);
	}
	
	public String getProperty(String key, boolean prependWorkingDir) {
		String value = posstatus.getProperty(key);
		
		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}
		
		return value;
	}
	
	private final String posStatusPropertiesFileName= "posstatus.properties";
	private Properties posstatus;
	
	private static PosStatusMappingProperties instance;
}
