package com.ncr.webfront.base.functionalities.postransactionssearch;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;

import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;

import com.google.common.collect.Ordering;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.postransaction.PosTransactionSearchCriteria;
import com.ncr.webfront.base.postransaction.PosTransactionServiceInterface;
import com.ncr.webfront.base.postransaction.PosTransactionSummary;
import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;
import com.ncr.webfront.base.restclient.posterminalservice.PosTerminalServiceClientImpl;
import com.ncr.webfront.base.restclient.postransactionservice.PosTransactionServiceClientImpl;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.core.zkconverters.DateConverter;
import com.ncr.webfront.core.zkconverters.ZkCentsToStringCurrencyConverter;

public class PosTransactionsSearchViewModel {

	private final Logger logger = WebFrontLogger.getLogger(PosTransactionsSearchViewModel.class);

	private List<PosTransactionSummary> listPosTransactionSummaries;
	private PosTransactionServiceInterface posTransactionService;
	private PosOperatorServiceInterface posOperatorServiceInterface;
	private PosTerminalServiceInterface posTerminalServiceInterface;
	private PosTransactionSearchCriteria posTransactionSearchCriteria;
	private PosTransactionSummary selectedPosTransactionSummary;
	private String selectedTransactionDateString;
	private boolean searchTransactionButtonPressed;
	private List<PrintService> printServicesList;
	private PrintService selectedPrintService;

	private int height;
	private int contentHeightReduction;
	private int transactionDivHeightReduction;

	private static final String ALL_OPERATORS = "Tutti";
	private static final String ALL_TERMINALS = "Tutte";

	private List<String> formattedAvailablePosTransactionDates;

	public PosTransactionsSearchViewModel() {
		this.posTransactionService = new PosTransactionServiceClientImpl();
		this.posOperatorServiceInterface = new PosOperatorServiceClientImpl();
		this.posTerminalServiceInterface = new PosTerminalServiceClientImpl();
		this.posTransactionSearchCriteria = new PosTransactionSearchCriteria(null, "", "", "");
		searchTransactionButtonPressed = false;
		contentHeightReduction = Integer.parseInt(WebFrontMappingProperties.getInstance()
				.getProperty("webfront.postransactionssearch.content.height.reduction"));
		transactionDivHeightReduction = Integer.parseInt(WebFrontMappingProperties.getInstance().getProperty(
				"webfront.postransactionssearch.transaction.div.height.reduction"));
		getAvailablePosTransactionDates();
	}

	@Init
	public void init(@ContextParam(ContextType.COMPONENT) Component component, @ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {
		logger.info("BEGIN");

		posTransactionSearchCriteria.setOperatorCode(ALL_OPERATORS);
		posTransactionSearchCriteria.setTerminalCode(ALL_TERMINALS);
		retrieveSystemPrinters();

		logger.info("END");
	}

	@GlobalCommand
	@NotifyChange({ "height", "transactionDivHeight" })
	public void onDesktopResize(@BindingParam("width") int width, @BindingParam("height") int height, @BindingParam("orientation") String orientation) {
		final Object _this = this;
		this.height = height - contentHeightReduction;
		BindUtils.postNotifyChange(null, null, _this, "height");
	}

	public String getHeight() {
		return height + "px";
	}

	public String getTransactionDivHeight() {
		return (height - transactionDivHeightReduction) + "px";
	}

	private void dumpPosTransactionJournal(List<String> posTransactionJournal) {
		logger.debug("BEGIN posTransactionJournal - size = " + posTransactionJournal.size());
		for (String line : posTransactionJournal) {
			logger.debug("    " + line);
		}
		logger.debug("END   posTransactionJournal - size = " + posTransactionJournal.size());
	}

	private void dumpPosTransactions(List<PosTransactionSummary> posTransactions) {
		logger.debug("BEGIN posTransactions - size = " + posTransactions.size());
		for (PosTransactionSummary posTransactionSummary : posTransactions) {
			DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mms");
			logger.debug(formatter.format(posTransactionSummary.getTransactionDate()) + "    " + posTransactionSummary.getOperatorCode() + "    "
					+ posTransactionSummary.getTerminalCode() + "    " + posTransactionSummary.getTransactionNumber() + "    "
					+ posTransactionSummary.getTransactionTotal());
		}
		logger.debug("END   posTransactions - size = " + posTransactions.size());
	}

	public PosTransactionSearchCriteria getPosTransactionSearchCriteria() {
		return posTransactionSearchCriteria;
	}

	public void setPosTransactionSearchCriteria(PosTransactionSearchCriteria posTransactionSearchCriteria) {
		this.posTransactionSearchCriteria = posTransactionSearchCriteria;
	}

	public List<String> getAvailablePosTransactionDates() {
		logger.info("BEGIN");

		if (formattedAvailablePosTransactionDates != null) {
			logger.info("END");
			return formattedAvailablePosTransactionDates;
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		formattedAvailablePosTransactionDates = new ArrayList<String>();
		List<Date> availablePosTransactionDates = posTransactionService.getAvailablePosTransactionDates();
		// show dates in descending order
		availablePosTransactionDates = Ordering.natural().reverse().sortedCopy(availablePosTransactionDates);

		for (Date availablePosTransactionDate : availablePosTransactionDates) {
			formattedAvailablePosTransactionDates.add(dateFormatter.format(availablePosTransactionDate));
		}

		logger.info("END");
		return formattedAvailablePosTransactionDates;
	}

	public List<String> getPosOperators() {
		List<String> posOperatorCodes = new ArrayList<String>();
		posOperatorCodes.add(ALL_OPERATORS);
		for (PosOperator posOperator : posOperatorServiceInterface.getAll()) {
			posOperatorCodes.add(posOperator.getOperatorCode());
		}
		return posOperatorCodes;
	}

	public List<String> getPosTerminals() {
		List<String> posTerminalCodes = new ArrayList<String>();
		posTerminalCodes.add(ALL_TERMINALS);
		for (PosTerminal posTerminal : posTerminalServiceInterface.getAll()) {
			posTerminalCodes.add(posTerminal.getTerminalCode());
		}
		return posTerminalCodes;
	}

	public String getSelectedTransactionDateString() {
		return selectedTransactionDateString;
	}

	public void setSelectedTransactionDateString(String selectedTransactionDateString) {
		this.selectedTransactionDateString = selectedTransactionDateString;
	}

	public List<PosTransactionSummary> getListPosTransactionSummaries() {
		return listPosTransactionSummaries;
	}

	public void setListPosTransactionSummaries(List<PosTransactionSummary> listPosTransactionSummaries) {
		this.listPosTransactionSummaries = listPosTransactionSummaries;
	}

	public boolean isSearchTransactionButtonPressed() {
		return searchTransactionButtonPressed;
	}

	public void setSearchTransactionButtonPressed(boolean searchTransactionButtonPressed) {
		this.searchTransactionButtonPressed = searchTransactionButtonPressed;
	}

	public PosTransactionSummary getSelectedPosTransactionSummary() {
		return selectedPosTransactionSummary;
	}

	public void setSelectedPosTransactionSummary(PosTransactionSummary selectedPosTransactionSummary) {
		this.selectedPosTransactionSummary = selectedPosTransactionSummary;
	}

	public Converter<String, Integer, Component> getCentsToStringCurrencyConverter() {
		return new ZkCentsToStringCurrencyConverter();
	}

	public DateConverter getDateConverter() {
		DateConverter dateConverter = new DateConverter();
		dateConverter.setDatePattern("dd/MM/yyyy HH:mm");
		return dateConverter;
	}

	public String getSelectedPosTransactionJournal() {
		logger.info("BEGIN");

		if (selectedPosTransactionSummary == null) {
			return "";
		}

		StringBuilder selectedPosTransactionJournal = new StringBuilder();

		for (String posTransactionJournalLine : posTransactionService.getPosTransactionJournal(selectedPosTransactionSummary)) {
			logger.debug("posTransactionJournalLine: " + posTransactionJournalLine);
			selectedPosTransactionJournal.append(posTransactionJournalLine);
			selectedPosTransactionJournal.append("\n");
		}

		if (selectedPosTransactionJournal.length() < 1) {
			return "\nScontrino non disponibile";
		}

		logger.info("END");
		return selectedPosTransactionJournal.toString();
	}

	@Command
	@NotifyChange({ "selectedPosTransactionJournal", "printJournalButtonEnabled" })
	public void doSelectPosTransactionSummary() {

	}

	private void retrieveSystemPrinters() {
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
		printServicesList = Arrays.asList(printServices);
	}

	public PrintService getSelectedPrintService() {
		return selectedPrintService;
	}

	public void setSelectedPrintService(PrintService selectedPrintService) {
		this.selectedPrintService = selectedPrintService;
	}

	public List<PrintService> getPrintServices() {
		return printServicesList;
	}

	@Command
	public void doPrintPosTransactionJournal() {
		logger.info("BEGIN");

		try {
			StringWriter sw = new StringWriter();
			BufferedWriter bw = new BufferedWriter(sw);
			for (String posTransactionJournalLine : posTransactionService.getPosTransactionJournal(selectedPosTransactionSummary)) {
				bw.write(posTransactionJournalLine);
				bw.newLine(); // add a new line according to the operating
								// system format
			}
			bw.flush();
			bw.close();

			InputStream is = new ByteArrayInputStream(sw.getBuffer().toString().getBytes());
			DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
			SimpleDoc doc = new SimpleDoc(is, flavor, null);
			DocPrintJob job = selectedPrintService.createPrintJob();

			PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
			job.print(doc, pras);

			is.close();
		} catch (PrintException e) {
			logger.error(e.getStackTrace());
		} catch (IOException e) {
			logger.error(e);
		}

		logger.info("END");
	}

	@Command
	public void doSelectTransactionDate() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date selectedTransactionDate = dateFormatter.parse(selectedTransactionDateString);
			posTransactionSearchCriteria.setTransactionDate(selectedTransactionDate);
		} catch (ParseException e) {
			logger.error(e);
		}
	}

	@Command
	@NotifyChange({ "listPosTransactionSummaries", "searchTransactionButtonPressed", "selectedPosTransactionJournal", "printJournalButtonEnabled" })
	public void doSearchTransaction() {
		if (posTransactionSearchCriteria.getOperatorCode().equals(ALL_OPERATORS)) {
			posTransactionSearchCriteria.setOperatorCode("");
		}
		if (posTransactionSearchCriteria.getTerminalCode().equals(ALL_TERMINALS)) {
			posTransactionSearchCriteria.setTerminalCode("");
		}
		listPosTransactionSummaries = posTransactionService.searchTransactions(posTransactionSearchCriteria);
		searchTransactionButtonPressed = true;
		selectedPosTransactionSummary = null;
	}

	public boolean isPosTransactionDatesAvailable() {
		return getAvailablePosTransactionDates().size() > 0;
	}

	@Command
	@NotifyChange({ "printJournalButtonEnabled" })
	public void checkPrintJournalButtonState() {

	}

	public boolean isPrintJournalButtonEnabled() {
		return (selectedPrintService == null || selectedPosTransactionSummary == null) ? false : true;
	}
}
