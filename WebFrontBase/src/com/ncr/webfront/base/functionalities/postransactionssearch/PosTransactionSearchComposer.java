package com.ncr.webfront.base.functionalities.postransactionssearch;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindComposer;
import org.zkoss.bind.BindUtils;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.ClientInfoEvent;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Window;

public class PosTransactionSearchComposer extends BindComposer<Window> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doAfterCompose(final Window window) throws Exception {
		super.doAfterCompose(window);
		window.addEventListener(Events.ON_CLIENT_INFO,
				new EventListener<ClientInfoEvent>() {
					public void onEvent(final ClientInfoEvent event)
							throws Exception {
						Executions
								.getCurrent()
								.getDesktop()
								.setAttribute("orientation",
										event.getOrientation());
						Executions.getCurrent().getDesktop()
								.setAttribute("width", event.getDesktopWidth());
						Executions
								.getCurrent()
								.getDesktop()
								.setAttribute("height",
										event.getDesktopHeight());
						@SuppressWarnings("serial")
						Map<String, Object> args = new HashMap<String, Object>() {
							{
								put("width", event.getDesktopWidth());
								put("height", event.getDesktopHeight());
								put("orientation", event.getOrientation());
							}
						};
						BindUtils.postGlobalCommand(null, null,
								"onDesktopResize", args);
					}
				});
	}

}
