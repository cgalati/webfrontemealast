package com.ncr.webfront.base.functionalities.reportposoperatorcash;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontReportOperatorCashSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontReportOperatorCashSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontReportOperatorCashSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontReportOperatorCashSettings("reports.properties");
		}
		return instance;
	}

	protected WebFrontReportOperatorCashSettings(String propertyFileName) {
		super(propertyFileName);
	}
			
	
	public String getTitlePage() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperatorcash.title", "Operator cash Report");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getFormatLbl() {
		logger.debug("BEGIN");
		String value = getStringValue("web.base.functionalities.reportposoperatorcash.format", "Format");
		logger.debug("END (" + value + ")");
		return value;
	}
}
