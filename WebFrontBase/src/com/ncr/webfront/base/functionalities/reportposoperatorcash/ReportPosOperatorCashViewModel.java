package com.ncr.webfront.base.functionalities.reportposoperatorcash;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.Init;

import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.functionalities.reportposoperator.WebFrontReportOperatorSettings;
import com.ncr.webfront.base.functionalities.reports.ReportBaseViewModel;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.base.posstatus.PosStatusServiceInterface;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;
import com.ncr.webfront.base.restclient.posstatusservice.PosStatusServiceClientImpl;

public class ReportPosOperatorCashViewModel  extends ReportBaseViewModel {
	private List<PosOperator> posOperatorList;
	private List<PosStatusObject> posStatusList;
	private PosStatusServiceInterface posStatusservice;
	
	@Init
	public void init() {
		super.init();
		super.postInit();
		
		PosOperatorServiceInterface posOperatorService = new PosOperatorServiceClientImpl();
		posOperatorList = posOperatorService.getAll();
		posStatusservice = new PosStatusServiceClientImpl();
		
	}

	@Override
	protected byte[] generateReport(ReportOptions reportOptions) {
		posStatusList = posStatusservice.getAllPosStatus(); // get the updated posStatusObjects
		return reportsService.generatePosOperatorCashReport(getPosOperatorsTerminalCodeMap(), getSelectedFormat());
	}
	
	private Map<PosOperator, String> getPosOperatorsTerminalCodeMap() {
		Map<PosOperator, String> posOperatorsTerminalCodeMap = new LinkedHashMap<PosOperator, String>();
		for ( PosOperator posOperator : posOperatorList) {
			// skip operators with opcode greater than 699
			if (Integer.parseInt(posOperator.getOperatorCode()) > 699) {
				break;
			}
			posOperatorsTerminalCodeMap.put(posOperator, "");
			for ( PosStatusObject posStatusObject : posStatusList) {
				// posOperator.getOperatorCode() format = xxxx
				// posStatusObject.getOperatorCode() format = xxx
				// so they are converted to integer before comparing
				if (Integer.parseInt(posOperator.getOperatorCode()) == Integer.parseInt(posStatusObject.getOperatorCode())) {
					posOperatorsTerminalCodeMap.put(posOperator, posStatusObject.getTerminalCode()); 
					break;
				}
			}
		}
		return posOperatorsTerminalCodeMap;
	}
	/*
	public String getTitlePage() {
		return WebFrontReportOperatorCashSettings.getInstance().getTitlePage();
	}

	public String getFormatLbl() {
		return WebFrontReportOperatorCashSettings.getInstance().getFormatLbl();
	}*/
}
