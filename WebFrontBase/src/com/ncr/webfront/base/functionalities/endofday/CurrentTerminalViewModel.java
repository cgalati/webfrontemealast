package com.ncr.webfront.base.functionalities.endofday;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class CurrentTerminalViewModel {
	@Init
	public void init(
			@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("selectedPosTerminal") EndOfDayDecoratedObject selectedPosTerminal) {

		Selectors.wireComponents(view, this, false);
		this.selectedPosTerminal = selectedPosTerminal;
		
		posStatus = new EndOfDayViewModel();			
	}
	
	
	@Command
	public void hideDialog() {		
		modalDialog.detach();
	}
	/*
	@Command
	@NotifyChange({ "detailVisible" })
	public void showList() {
		detailVisible = !detailVisible;		
	}
	*/
	@Command
	@NotifyChange({"posStatusList", "posStatusListMap"})
	public void callDeclareDefective(@BindingParam("terminalCode") String terminalCode) {
		posStatus.declareDefective(terminalCode);	
		
		hideDialog();
	}
	
	public EndOfDayDecoratedObject getSelectedPosTerminal() {
		return selectedPosTerminal;
	}

	public void setSelectedTerminal(EndOfDayDecoratedObject selectedTerminal) {
		this.selectedPosTerminal = selectedPosTerminal;
	}

	/*public boolean getDetailVisible() {
		return detailVisible;
	}*/
	
	public EndOfDayViewModel getPosStatus() {
		return posStatus;
	}
	
	
	@Wire
	private Window modalDialog;
	private EndOfDayViewModel posStatus;
	private final Logger logger = WebFrontLogger.getLogger(CurrentTerminalViewModel.class);
	private EndOfDayDecoratedObject selectedPosTerminal;	
	//private boolean detailVisible = false;
	
}
