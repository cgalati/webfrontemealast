package com.ncr.webfront.base.functionalities.endofday;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.impl.XulElement;
import org.zkoss.zul.TreeModel;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.Window;

import com.ncr.webfront.base.eod.EodServiceInterface;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.eod.EodStatusObject.EodServerPhases;
import com.ncr.webfront.base.eod.EodStatusObject.EodStatus;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.reports.ReportsServiceInterface;
import com.ncr.webfront.base.restclient.eodservice.EodServiceClientImpl;
import com.ncr.webfront.base.restclient.posterminalservice.PosTerminalServiceClientImpl;
import com.ncr.webfront.base.restclient.reportsservice.ReportsServiceClientImpl;
import com.ncr.webfront.base.utils.EndOfDayStatusTreeComposer;
import com.ncr.webfront.base.utils.EndOfDayStatusTreeRenderer;
import com.ncr.webfront.core.MainComposer;
import com.ncr.webfront.core.WebFrontCore;
import com.ncr.webfront.core.functionality.FunctionalityData;
import com.ncr.webfront.core.language.WebFrontZkLabelsLocator;
import com.ncr.webfront.core.utils.commandexecution.CommandLocalRunner;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.core.zkconverters.DateConverter;

public class EndOfDayViewModel {

	private boolean posStatusOnly;

	@Init
	@NotifyChange({ "posStatusList", "posStatusListMap", "readyForPosEndOfDay", "readyForServerEndOfDay", "current", "eodTree", "last" })
	public void init(@ContextParam(ContextType.COMPONENT) Component component, @ExecutionArgParam(FunctionalityData.FIRST_ARGUMENT_KEY) String firstArgument) {
		logger.debug("BEGIN");
		try {
			showNotificationPopups = EndOfDayMappingProperties.getInstance().isShowNotificationPopups();

			List<EodStatusObject> history = null;
			logger.info("firstArgument = \"" + firstArgument + "\"");
			if (firstArgument != null && firstArgument.equalsIgnoreCase("#posstatusonly")) {
				logger.debug("posStatusOnly = true");
				this.posStatusOnly = true;
				MainComposer.setFunctionTitle((XulElement) component, "Barrier status");
			}
			logger.debug("-----------------------------------------------------------------------");
			logger.debug("ENTERING THE END OF DAY PAGE");
			logger.debug("-----------------------------------------------------------------------");
			if (renderer == null) {
				String endOfDayTreeNodeStyle = EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeStyle();
				String endOfDayTreeNodeWarningStyle = EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeWarninngStyle();
				String endOfDayTreeNodeErrorStyle = EndOfDayMappingProperties.getInstance().getEndOfDayTreeNodeErrorStyle();
				renderer = new EndOfDayStatusTreeRenderer(endOfDayTreeNodeStyle, endOfDayTreeNodeWarningStyle, endOfDayTreeNodeErrorStyle);
			}

			ErrorObject errorObject = eodService.checkService();
			if (errorObject != null && !errorObject.getErrorCode().isOk()) {
				MainComposer.notifyServiceProblems("EodService", errorObject.getDescription());
				logger.error("ERROR! " + errorObject.getDescription());
				MainComposer.goToHome();
				logger.debug("-----------------------------------------------------------------------");
				logger.debug("EXITING  THE END OF DAY PAGE BECAUSE OF THE ERROR");
				logger.debug("-----------------------------------------------------------------------");
				logger.debug("END");
				return;
			}
			if (posStatusOnly) {
				EodStatusObject eodStatusObject = new EodStatusObject(new Date(), new Date(), EodStatus.EOD_NOT_STARTED);
				history = new ArrayList<EodStatusObject>();
				history.add(eodStatusObject);
			} else {

				logger.debug("retrieving eod status history");
				try {
					history = eodService.getEodStatusHistory();
				} catch (Exception e) {
					logger.error("Exception calling eodService.getEodStatusHistory()!", e);
					history = new ArrayList<>();
				}
				logger.debug(String.format("history.size() = [%d] ", history.size()));
			}
			if (history == null || history.isEmpty()) {
				logger.error("ERROR! history == null or empty! Should never happen!");
				logger.error("Clients.showNotification(\"Problemi di connessione con l'EOD socket server\")");
				Clients.showNotification("Connection errors with the EOD socket server");
				logger.debug("END");
				return;
			}

			EodStatusObject prevEod = history.get(0);

			if (history.get(0).getStatus().isRunning()) {
				setUpdateStepsAndNotifyTimerEnabled(true);
				setEodTreeVisible(true);
				// TODO CHECK WHY IT IS COMMENTED (PERCHE' NON C'E'?)
				// setLastEodStatusObject(history.get(0)); ???
				updateStepsAndNotify();

				logger.debug("listening to a running eod");
			} else if (history.get(0).getStatus().equals(EodStatus.EOD_ENDED_WITH_ERROR)) {
				setEodTreeVisible(true);
				setStartRetryEndOfDayButtonEnabled(true);
				showNotificationPopups = false;
				updateStepsAndNotify();
				showNotificationPopups = EndOfDayMappingProperties.getInstance().isShowNotificationPopups();
			} else {
				setUpdatePosStatusListTimerEnabled(true);
				setPosStatusListVisible(true);
				setLastEodStatusObject(history.get(0));
				updatePosStatusList();
				logger.debug("showing the last eod");
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}

		restartEndOfDayButtonVisible = false;
		logger.debug("END");
	}

	public boolean isPosStatusOnly() {
		return posStatusOnly;
	}

	@Command
	@NotifyChange({ "posStatusList", "posStatusListMap", "readyForPosEndOfDay", "readyForServerEndOfDay", "progressPercentage", "startEndOfDayButtonEnabled" })
	public void updatePosStatusList() {
		logger.debug("BEGIN");
		try {
			if ((progressPercentage += 10) > 100) {
				progressPercentage = 0;
			}

			List<PosStatusObject> posStatusList = eodService.getAllPosStatus();
			dumpPosStatusList("updatePosStatusList", posStatusList);
			if (posStatusList == null || posStatusList.size() == 0) {
				logger.warn("posStatusList will NOT be used since null or empty!");
				logger.warn("Error reading POS status from server!");
				logger.warn("Clients.showNotification(\"Errore nella lettura dello stato delle casse\"...)");
				Clients.showNotification("Error in the reading of register status", Clients.NOTIFICATION_TYPE_WARNING, null, "middle_center",
						getUpdatePosStatuslistTimeoutMilliseconds() / 3);
				setStartEndOfDayButtonEnabled(false);
				logger.debug("END");
				return;
			}
			logger.debug("posStatusList will be used since not null and not empty");
			setPosStatusList(posStatusList);

			if (!startEndOfDayButtonAlreadyPressed) {
				setStartEndOfDayButtonEnabled(isReadyForPosEndOfDay());
			}

		} catch (Exception e) {
			logger.error("Exception!", e);

			posStatusListMap = new TreeMap<String, ArrayList<EndOfDayDecoratedObject>>(); // CGA
			logger.error("Clients.showNotification(\"Problemi di connessione con l'EOD socket server\")");
			Clients.showNotification("Connection problems with the EOD socket server");
		}
		logger.debug("END");
	}

	public int getProgressPercentage() {
		return progressPercentage;
	}

	@Command
	@NotifyChange({ "posStatusList", "posStatusListMap" })
	public void declareDefective(String terminalCode) { // CGA A
		logger.debug("BEGIN");
		try {
			eodService.declareDefective(terminalCode);
			updatePosStatusList();
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	@Command
	@NotifyChange({ "updatePosStatusListTimerEnabled", "posStatusList", "posStatusListMap", "readyForPosEndOfDay", "readyForServerEndOfDay", "current",
			"eodTree", "last" })
	public void startEndOfDay() {
		logger.debug("BEGIN");
		try {

			logger.debug("START EOD OF DAY (Avvia fine giorno) BUTTON PRESSED");

			boolean doIt = true;
			if (isEodAlreadyExecutedTodayCheck()) {
				doIt = checkEodAlreadyExecutedToday();
			}
			if (doIt) {
				if (isEodPasswordRequired()) {
					doOpenPasswordCheckDialog(START_EOD);
				} else {
					doStartEndOfDay();
				}
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private boolean checkEodAlreadyExecutedToday() {
		logger.debug("BEGIN");
		try {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
			EventListener<ClickEvent> clickListener = new EventListener<Messagebox.ClickEvent>() {
				public void onEvent(ClickEvent event) throws Exception {
					if (Messagebox.Button.YES.equals(event.getButton())) {
						if (isEodPasswordRequired()) {
							doOpenPasswordCheckDialog(START_EOD);
						} else {
							doStartEndOfDay();
						}
					}
				}
			};

			if (lastEodStatusObject != null && (lastEodStatusObject.getEodStartDateTime() != null)) {
				if (fmt.format(lastEodStatusObject.getEodStartDateTime()).equals(fmt.format(new Date()))) {
					logger.debug("Asking confirmation to rerun EOD on the same day");
					Messagebox.show("The EOD was executed today. Confirm the execution?", "Confirm the execution EOD", new Messagebox.Button[] {
							Messagebox.Button.YES, Messagebox.Button.NO }, Messagebox.QUESTION, clickListener);
					logger.debug("END (false)");
					return false;
				}
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END (true)");
		return true;
	}

	private void doStartEndOfDay() {
		logger.debug("BEGIN");
		try {

			startEndOfDayButtonAlreadyPressed = true;
			setStartEndOfDayButtonEnabled(false);

			updatePosStatusList();

			if (isReadyForPosEndOfDay()) {
				endOfDay();
			} else {
				if (showNotificationPopups) {
					showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.poseod.error.1"), EndOfDayMappingProperties
							.getInstance().getProperty("endofday.notification.popup.poseod.error.2"),
							EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.end.phase.timeout"));
				}
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	@Command
	@NotifyChange({ "posStatusList", "posStatusListMap", "readyForPosEndOfDay", "readyForServerEndOfDay", "current", "eodTree", "last",
			"startRetryEndOfDayButtonEnabled" })
	public void restartEndOfDay() {
		logger.debug("BEGIN");
		try {
			logger.debug("RETRY START EOD OF DAY (Ritenta fine giorno) BUTTON PRESSED");
			if (isEodPasswordRequired()) {
				doOpenPasswordCheckDialog(RESTART_EOD);
			} else {
				doRestartOfDay();
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	@Command
	@NotifyChange({ "posStatusList", "posStatusListMap", "readyForPosEndOfDay", "readyForServerEndOfDay", "current", "eodTree", "last" })
	public void restartEndOfDayFromStart() {
		logger.debug("BEGIN");
		try {
			logger.debug("RETRY START EOD OF DAY (Riavvia fine giorno) BUTTON PRESSED");

			if (isEodPasswordRequired()) {
				doOpenPasswordCheckDialog(RESTART_EOD_FROMSTART);
			} else {
				lastEodStatusObject = null;
				currentEodStatusObject = null;
				eodTree = null;
				deleteLastStatus();
				doStartEndOfDay();
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private void deleteLastStatus() {
		logger.debug("BEGIN");
		try {
			File statusDir = new File(PathManager.getInstance().getHomeFolder() + File.separator + "webfront-endofday" + File.separator + "eodstatus");
			logger.debug("statusDir");
			File[] files = statusDir.listFiles(new FileFilter() {
				public boolean accept(File file) {
					return file.isFile();
				}
			});
			long lastMod = Long.MIN_VALUE;
			File choice = null;
			for (File file : files) {
				if (file.lastModified() > lastMod) {
					choice = file;
					lastMod = file.lastModified();
				}
			}
			choice.delete();
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private void doRestartOfDay() {
		logger.debug("BEGIN");
		try {
			startEndOfDayButtonAlreadyPressed = true;
			setStartRetryEndOfDayButtonEnabled(false);
			setUpdatePosStatusListTimerEnabled(false);
			setPosStatusListVisible(false);
			setCheckAllPosCompletedEodTimerEnabled(false);

			logger.debug("RUNNING eodService.launchEodServer()");
			ErrorObject serverErrorObject = eodService.launchEodServer();

			WebFrontCore.setCurrentFunctionModfied(false);

			logger.debug(String.format("launchEodServer -> [%s]", serverErrorObject.getErrorCode()));

			setUpdateStepsAndNotifyTimerEnabled(true);
			setStartRetryEndOfDayButtonEnabled(false);
			setEodTreeVisible(true);
			// eodTree = null;
			updateStepsAndNotify();

			// if (showNotificationPopups) {
			// Clients.showNotification("Rilancio fine giornata");
			// }
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private void doOpenPasswordCheckDialog(String callingMethod) {
		logger.debug("BEGIN");
		try {

			final Map<String, Object> map = new HashMap<String, Object>();
			map.put("callingMethod", callingMethod);
			Window window = (Window) Executions.createComponents("~./base/functionalities/endofday/eodpasswordcheckdialog.zul", null, map);
			window.doModal();
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private void executeScriptBeforeEod() {
		logger.debug("BEGIN");
		try {
			int timeoutInSeconds = WebFrontMappingProperties.getInstance().getCommandExecutionTimeoutInSeconds();

			String scriptPath = EndOfDayMappingProperties.getInstance().getProperty("endofday.preexecution.script");
			String eodHomeFolder = System.getenv("WF_ENDOFDAY_HOMEDIR");

			logger.debug("scriptPath " + scriptPath);
			if (scriptPath != null && scriptPath.trim().length() > 0) {
				if (scriptPath.startsWith(".")) {
					scriptPath = eodHomeFolder + scriptPath.substring(1);
				}
				logger.debug("scriptPath " + scriptPath);

				CommandLocalRunner localRunner = new CommandLocalRunner();
				localRunner.setTimeoutInSeconds(timeoutInSeconds);
				com.ncr.webfront.core.utils.commandexecution.Command command = new com.ncr.webfront.core.utils.commandexecution.Command();

				command.setFullCommandLine(scriptPath);
				RunningInfo info = new RunningInfo(command);
				localRunner.setRunningInfo(info);
				localRunner.run();
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private boolean isEodPasswordRequired() {
		boolean result = false;
		try {
			result = Boolean.valueOf(EndOfDayMappingProperties.getInstance().getProperty("endofday.button.askpassword"));
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	private int getLastCriticalPhaseStep() {
		int last = 0;
		try {
			last = currentEodStatusObject.getEodSteps().size();
			last = EndOfDayMappingProperties.getInstance().getIntProperty("endofday.critical.last_step", 30);
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		return last;
	}

	private boolean isEodAlreadyExecutedTodayCheck() {
		boolean result = false;
		try {
			result = Boolean.parseBoolean(EndOfDayMappingProperties.getInstance().getProperty("endofday.already.executed.today.askconfirmation"));
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	@GlobalCommand
	public void doCheckPasswordState(@BindingParam("callingMethod") final String callingMethod,
			@BindingParam("isEodPasswordChecked") final boolean isEodPasswordChecked) {
		logger.debug("BEGIN");
		try {
			if (isEodPasswordChecked) {
				if (callingMethod.equals(START_EOD)) {
					doStartEndOfDay();
				} else if (callingMethod.equals(RESTART_EOD)) {
					doRestartOfDay();
				} else if (callingMethod.equals(RESTART_EOD_FROMSTART)) {
					eodTree = null;
					deleteLastStatus();
					doStartEndOfDay();
				}

			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	private void endOfDay() {
		logger.debug("Start EOD");
		try {
			modalDialog = null;
			if (showNotificationPopups) {
				showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.poseod.phase.1"), EndOfDayMappingProperties
						.getInstance().getProperty("endofday.notification.popup.poseod.phase.2"), "");
			}
			executeScriptBeforeEod();
			WebFrontCore.setCurrentFunctionModfied(true);
			logger.debug("RUNNING eodService.launchEodAllPos()");

			ErrorObject posErrorObject = new ErrorObject(ErrorCode.OK_NO_ERROR);

			// ALBTEMP TEST ONLY BEGIN
			String testOnlyFlagFilePath = "/home/NCR/webfront/TESTONLY_SKIPPOSEODCMD.FLG";
			File file = new File(testOnlyFlagFilePath);
			if (file.exists()) {
				logger.warn("TESTONLY - IL FLAG FILE \"" + testOnlyFlagFilePath + "\" ESISTE -> SALTO COMANDO POS EOD");
				logger.warn("Skipping eodService.launchEodAllPos()");
			} else {
				logger.info("Calling  eodService.launchEodAllPos()");
				posErrorObject = eodService.launchEodAllPos();
			}
			// ALBTEMP TEST ONLY END

			ErrorCode errorCode = posErrorObject.getErrorCode();
			logger.debug(String.format("launchEodAllPos -> [%s]", errorCode));

			if ((errorCode == ErrorCode.OK_NO_ERROR) || (errorCode == ErrorCode.OK_WARNING)) {
				checkAllPosCompletedEodTimerCurrentCount = 0;
				setCheckAllPosCompletedEodTimerEnabled(true);
			} else {
				showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.poseod.error.1"), EndOfDayMappingProperties
						.getInstance().getProperty("endofday.notification.popup.poseod.error.2"),
						EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.end.phase.timeout"));
				WebFrontCore.setCurrentFunctionModfied(false);
				startEndOfDayButtonAlreadyPressed = false;
				setUpdatePosStatusListTimerEnabled(true);
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	@Command
	@NotifyChange({ "posStatusList", "posStatusListMap", "readyForPosEndOfDay", "readyForServerEndOfDay", "startEndOfDayButtonEnabled" })
	public void checkAllPosCompletedEod() {
		logger.debug("BEGIN");
		try {
			checkAllPosCompletedEodTimerMaxRetries = WebFrontMappingProperties.getInstance().getIntProperty("endofday.checkallposcompletedeod.max.retries", 15);

			if (checkAllPosCompletedEodTimerCurrentCount < checkAllPosCompletedEodTimerMaxRetries) {
				try {
					List<PosStatusObject> posStatusList = eodService.getAllPosStatus();
					dumpPosStatusList("checkAllPosCompletedEod", posStatusList);
					if (posStatusList == null || posStatusList.size() == 0) {
						logger.warn("posStatusList will NOT be used since null or empty!");
						logger.warn("Error reading POS status from server!");
						logger.warn("Clients.showNotification(\"Errore nella lettura dello stato delle casse\"...)");
						Clients.showNotification("Error during the reading of register status", Clients.NOTIFICATION_TYPE_WARNING, null, "middle_center",
								getCheckAllPosCompletedEodTimeoutMilliseconds() / 3);
						checkAllPosCompletedEodTimerCurrentCount++;
						logger.debug("END");
						return;
					}

					logger.debug("posStatusList will be used since not null and not empty");
					setPosStatusList(posStatusList);

				} catch (Exception e) {
					logger.error("Exception!", e);

					posStatusListMap = new TreeMap<String, ArrayList<EndOfDayDecoratedObject>>(); // CGA
					logger.error("Clients.showNotification(\"Problemi di connessione con l'EOD socket server\")");
					Clients.showNotification("Connection problems with the EOD socket server");
				}

				if (isReadyForServerEndOfDay()) {
					setUpdatePosStatusListTimerEnabled(false);
					setPosStatusListVisible(false);

					setCheckAllPosCompletedEodTimerEnabled(false);

					setUpdateStepsAndNotifyTimerEnabled(true);
					setEodTreeVisible(true);

					if (EndOfDayMappingProperties.getInstance().getEndOfDayReportsEnabled()) {
						logger.debug("BEGIN producing report...");
						PosTerminalServiceInterface posTerminalService = new PosTerminalServiceClientImpl();
						ReportsServiceInterface reportService = new ReportsServiceClientImpl();

						List<String> registerList = new ArrayList<String>();
						for (PosTerminal posTerminal : posTerminalService.getAll()) {
							registerList.add(posTerminal.getTerminalCode());
						}

						if (showNotificationPopups) {
							showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.generateReports.phase.1"),
									EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.generateReports.phase.2"), "");
						}
						reportService.generateEodReports(registerList);
						logger.debug("END   producing report...");
					}

					logger.debug("RUNNING eodService.launchEodServer()");
					ErrorObject serverErrorObject = eodService.launchEodServer();

					WebFrontCore.setCurrentFunctionModfied(false);

					logger.debug(String.format("launchEodServer -> [%s]", serverErrorObject.getErrorCode()));

					updateSteps();

					if (showNotificationPopups) {
						showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.critical.phase.1"),
								EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.critical.phase.2"), "");
					}
				}
			} else {
				setCheckAllPosCompletedEodTimerEnabled(true);
				if (showNotificationPopups) {
					showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.poseod.error.1"), EndOfDayMappingProperties
							.getInstance().getProperty("endofday.notification.popup.poseod.error.2"),
							EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.end.phase.timeout"));
				}
				WebFrontCore.setCurrentFunctionModfied(false);
				setStartEndOfDayButtonEnabled(isReadyForPosEndOfDay());
				startEndOfDayButtonAlreadyPressed = false;
				setUpdatePosStatusListTimerEnabled(true);
				logger.debug("END");
				return;
			}

			checkAllPosCompletedEodTimerCurrentCount++;
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	@Command
	@NotifyChange({ "current", "eodTree", "last" })
	public void cancel() {
		logger.debug("BEGIN");
		try {
			eodService.cancel();

			updateStepsAndNotify();
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	@Command
	@NotifyChange({ "current", "eodTree", "last", "startRetryEndOfDayButtonEnabled" })
	public void updateStepsAndNotify() {
		logger.debug("BEGIN");
		try {
			updateSteps();

			if (currentEodStatusObject != null && currentEodStatusObject.getEodSteps() != null) {
				logger.debug("currentEodStatusObject.getEodSteps().size() = " + currentEodStatusObject.getEodSteps().size());
				if (currentEodStatusObject.getEodSteps().size() > 0) {
					logger.debug("currentEodStatusObject.getNumberOfCompletedExecutionSteps():" + currentEodStatusObject.getNumberOfCompletedExecutionSteps());
					logger.debug("currentEodStatusObject.getLastCompletedExecutionStepIndex():" + currentEodStatusObject.getLastCompletedExecutionStepIndex());
				}
			}

			if (currentEodStatusObject == null || currentEodStatusObject.getStatus() == null) {
				logger.warn("(currentEodStatusObject == null || currentEodStatusObject.getStatus() == null)");
				logger.debug("END");
				return;
			}

			if (!postEod && (currentEodStatusObject.getEodServerPhase() == EodServerPhases.POST_SERVER_EOD)) {
				postEod = true;

				logger.debug("in post eod phase");

				if (showNotificationPopups) {
					Clients.showNotification("Start of next step to EOD of server"); 
				}
			}

			if (currentEodStatusObject.getStatus().equals(EodStatus.EOD_ENDED_WITH_ERROR)) {
				// setEodTreeVisible(true);
				setStartRetryEndOfDayButtonEnabled(true);
			}
			if (showNotificationPopups) {
				if (currentEodStatusObject.getLastCompletedExecutionStepIndex() >= getLastCriticalPhaseStep()
						&& currentEodStatusObject.getEodEndDateTime() == null) {
					showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.noncritical.phase.1"),
							EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.noncritical.phase.2"), "");
				}
			}
			if (currentEodStatusObject.getEodEndDateTime() != null) {
				logger.debug("eod finished");

				logger.debug("showNotificationPopups = " + showNotificationPopups);

				if (showNotificationPopups) {
					showMessage(EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.end.phase.1"), EndOfDayMappingProperties
							.getInstance().getProperty("endofday.notification.popup.end.phase.2"),
							EndOfDayMappingProperties.getInstance().getProperty("endofday.notification.popup.end.phase.timeout"));
				}
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}

	public void updateSteps() {
		try {
			lastEodStatusObject = currentEodStatusObject = eodService.getEodStatus();
			if (lastEodStatusObject != null && (!lastEodStatusObject.getStatus().isRunning())) {

				logger.debug("-----------------------------------------------------------------------");
				logger.debug("END OF DAY COMPLETED.");
				logger.debug("lastEodStatusObject.getStatus() = " + lastEodStatusObject.getStatus());
				logger.debug("-----------------------------------------------------------------------");
				logger.debug("lastEodStatusObject:" + lastEodStatusObject);

				setUpdateStepsAndNotifyTimerEnabled(false);
			}

			if (lastEodStatusObject != null) {
				eodTree = EndOfDayStatusTreeComposer.composeTwoLevels(eodTree, currentEodStatusObject.getEodSteps(), true);
			}
		} catch (Exception e) {
			logger.error("Exception!", e);

			logger.error("Clients.showNotification(\"Problemi di connessione con l'EOD socket server\")");
			Clients.showNotification("Connection problems with the EOD socket server");
		}
	}

	public EodStatusObject getLastEodStatusObject() {
		return lastEodStatusObject;
	}

	public String getLastEodStatusDescription() {
		return getEodStatusLabel(lastEodStatusObject.getStatus());
	}

	public void setLastEodStatusObject(EodStatusObject last) {
		this.lastEodStatusObject = last;
	}

	public EodStatusObject getCurrent() {
		return currentEodStatusObject;
	}

	public void setCurrent(EodStatusObject current) {
		this.currentEodStatusObject = current;
	}

	public Map<String, ArrayList<EndOfDayDecoratedObject>> getPosStatusListMap() {
		return posStatusListMap;
	}

	@Command
	@NotifyChange({ "posStatusList", "posStatusListMap" })
	public void setPosStatusList(List<PosStatusObject> posStatusList) {
		try {

			this.posStatusList = posStatusList;

			sizeLine = EndOfDayMappingProperties.getInstance().getPosStatusIconsPerLine();
			numTerminal = EndOfDayMappingProperties.getInstance().getPosStatusSmallIconSizeThreshold();
			ArrayList<EndOfDayDecoratedObject> supportList1 = new ArrayList<EndOfDayDecoratedObject>();
			ArrayList<EndOfDayDecoratedObject> supportList = supportList1;
			EndOfDayDecoratedObject decorated;
			int cont = 0;
			int indexTerminal = 0;
			int key = 1;
			boolean small = false;

			posStatusListMap.clear();

			if (posStatusList.size() > numTerminal) {
				small = true;
			}

			for (PosStatusObject object : posStatusList) {
				decorated = new EndOfDayDecoratedObject(object, small);

				supportList.add(decorated);
				cont++;
				indexTerminal++;

				if (cont == sizeLine || indexTerminal == posStatusList.size()) {
					posStatusListMap.put("list" + key, supportList);

					supportList = new ArrayList<EndOfDayDecoratedObject>();
					key++;
					cont = 0;
				}
			}

		} catch (Exception e) {
			logger.error("Exception!", e);
		}
	}

	private void dumpPosStatusList(String callerName, List<PosStatusObject> posStatusList) {
		logger.debug("--------- BEGIN dumpPosStatusList (" + callerName + ") ---------");
		if (posStatusList == null) {
			logger.debug("posStatusList == null");
			logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
			return;
		}
		if (posStatusList.size() == 0) {
			logger.debug("posStatusList.size() == 0");
			logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
			return;
		}

		for (PosStatusObject posStatusObject : posStatusList) {
			logger.debug("Terminal code: " + posStatusObject.getTerminalCode() + " - isDefectiveDeclarable: " + posStatusObject.isDefectiveDeclarable()
					+ " - isReadyForPosEndOfDay: " + posStatusObject.isReadyForPosEndOfDay() + " - isReadyForServerEndOfDay: "
					+ posStatusObject.isReadyForServerEndOfDay() + " - ActionCode: " + posStatusObject.getActionCode() + " - ActionCodeDescription: "
					+ posStatusObject.getActionCodeDescription() + " - PosStatus: " + posStatusObject.getPosStatus());
		}
		logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
	}

	public boolean isReadyForPosEndOfDay() {
		boolean readyForPosEndOfDay = true;
		try {

			if (posStatusListMap != null && posStatusList != null && posStatusList.size() > 0) {
				for (PosStatusObject status : posStatusList) {
					readyForPosEndOfDay &= status.isReadyForPosEndOfDay();
				}
			} else {
				readyForPosEndOfDay = false;
			}

		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug(String.format("readyForPosEndOfDay = %b", readyForPosEndOfDay));
		return readyForPosEndOfDay;
	}

	public boolean isReadyForServerEndOfDay() {
		logger.debug("BEGIN");
		boolean readyForServerEndOfDay = true;
		try {

			if (posStatusListMap != null) {
				for (PosStatusObject posStatusObject : posStatusList) {
					readyForServerEndOfDay &= posStatusObject.isReadyForServerEndOfDay();
				}
			} else {
				readyForServerEndOfDay = false;
			}

			logger.debug(String.format("readyForServerEndOfDay = %b", readyForServerEndOfDay));

		} catch (Exception e) {
			logger.error("Exception!", e);
		}

		logger.debug("END (readyForServerEndOfDay = " + readyForServerEndOfDay + ")");
		return readyForServerEndOfDay;
	}

	public TreeModel<TreeNode<String>> getEodTree() {
		return eodTree;
	}

	public EndOfDayStatusTreeRenderer getTreeRenderer() {
		return renderer;
	}

	public String getTreeHeight() {
		logger.debug("BEGIN");
		final int DEFAULT_HEIGTH = 410;
		int treeHeight = DEFAULT_HEIGTH;
		try {

			try {
				int screenHeight = (int) Executions.getCurrent().getDesktop().getAttribute("height");
				logger.info("screenHeight = " + screenHeight);
				treeHeight = screenHeight - 200;
				logger.info("treeHeight = " + treeHeight);
			} catch (Exception e) {
				logger.info("Exception!", e);
				treeHeight = DEFAULT_HEIGTH;
			}

			logger.info("treeHeight = " + treeHeight);
		} catch (Exception e) {
			logger.error("Exception!", e);
		}

		logger.debug("END (" + treeHeight + "px)");
		return treeHeight + "px";
	}

	public boolean isUpdatePosStatusListTimerEnabled() {
		return updatePosStatusListTimerEnabled;
	}

	public int getUpdatePosStatuslistTimeoutMilliseconds() {
		int value = EndOfDayMappingProperties.getInstance().getIntProperty("endofday.updateposstatuslist.timeout.milliseconds", 5);
		logger.info("endofday.updateposstatuslist.timeout.milliseconds = " + value);
		return value;
	}

	public int getInvalidateMapTimeoutMilliseconds() {
		int value = EndOfDayMappingProperties.getInstance().getIntProperty("endofday.invalidatemap.timeout.milliseconds", 5);
		logger.info("endofday.invalidatemap.timeout.milliseconds= " + value);
		return value;
	}

	public int getCheckAllPosCompletedEodTimeoutMilliseconds() {
		int value = EndOfDayMappingProperties.getInstance().getIntProperty("endofday.checkallposcompletedeod.timeout.milliseconds", 5);
		logger.info("endofday.checkallposcompletedeod.timeout.milliseconds= " + value);
		return value;
	}

	public int getUpdateStepsAndNotifyTimeoutMilliseconds() {
		int value = EndOfDayMappingProperties.getInstance().getIntProperty("endofday.updatestepsandnotify.timeout.milliseconds", 5);
		logger.info("endofday.updatestepsandnotify.timeout.milliseconds= " + value);
		return value;
	}

	public void setUpdatePosStatusListTimerEnabled(boolean updatePosStatusListTimerEnabled) {
		this.updatePosStatusListTimerEnabled = updatePosStatusListTimerEnabled;
		BindUtils.postNotifyChange(null, null, this, "updatePosStatusListTimerEnabled");
	}

	public boolean isUpdateStepsAndNotifyTimerEnabled() {
		return updateStepsAndNotifyTimerEnabled;
	}

	public void setUpdateStepsAndNotifyTimerEnabled(boolean updateStepsAndNotifyTimerEnabled) {
		this.updateStepsAndNotifyTimerEnabled = updateStepsAndNotifyTimerEnabled;
		BindUtils.postNotifyChange(null, null, this, "updateStepsAndNotifyTimerEnabled");
	}

	public boolean isPosStatusListVisible() {
		return posStatusListVisible;
	}

	public void setPosStatusListVisible(boolean posStatusListVisible) {
		this.posStatusListVisible = posStatusListVisible;
		BindUtils.postNotifyChange(null, null, this, "posStatusListVisible");
	}

	public boolean isEodTreeVisible() {
		return eodTreeVisible;
	}

	public void setEodTreeVisible(boolean eodTreeVisible) {
		this.eodTreeVisible = eodTreeVisible;
		BindUtils.postNotifyChange(null, null, this, "eodTreeVisible");
	}

	public boolean isStartEndOfDayButtonEnabled() {
		return startEndOfDayButtonEnabled;
	}

	@NotifyChange("startEndOfDayButtonEnabled")
	public void setStartEndOfDayButtonEnabled(boolean startEndOfDayButtonEnabled) {
		this.startEndOfDayButtonEnabled = startEndOfDayButtonEnabled;
		logger.debug("setStartEndOfDayButtonEnabled ->" + startEndOfDayButtonEnabled);
		BindUtils.postNotifyChange(null, null, this, "startEndOfDayButtonEnabled");
	}

	public boolean isStartRetryEndOfDayButtonEnabled() {
		return startRetryEndOfDayButtonEnabled;
	}

	public boolean isRestartEndOfDayButtonVisible() {
		return restartEndOfDayButtonVisible;
	}

	public void setStartRetryEndOfDayButtonEnabled(boolean startRetryEndOfDayButtonEnabled) {
		this.startRetryEndOfDayButtonEnabled = startRetryEndOfDayButtonEnabled;
		BindUtils.postNotifyChange(null, null, this, "startRetryEndOfDayButtonEnabled");
	}

	public boolean isCheckAllPosCompletedEodTimerEnabled() {
		return checkAllPosCompletedEodTimerEnabled;
	}

	public void setCheckAllPosCompletedEodTimerEnabled(boolean checkAllPosCompletedEodTimerEnabled) {
		this.checkAllPosCompletedEodTimerEnabled = checkAllPosCompletedEodTimerEnabled;
		BindUtils.postNotifyChange(null, null, this, "checkAllPosCompletedEodTimerEnabled");
	}

	public DateConverter getDateConverter() {
		return new DateConverter();
	}

	private void showMessage(final String mainMessage, final String subMessage, String timeout) {
		final HashMap<String, Object> map = new HashMap<String, Object>();
		try {

			map.put("mainMessage", mainMessage);
			map.put("subMessage", subMessage);
			map.put("timeout", timeout);
			if (modalDialog == null) {
				logger.debug("showing modalDialog with message = \"" + mainMessage + "\" and submessage =\"" + subMessage + "\"");
				modalDialog = (Window) Executions.createComponents("~./base/functionalities/endofday/eodwarnings.zul", null, map);
				modalDialog.doModal();
			} else {
				logger.debug("modalDialog = null - skipping modalDialog with message = \"" + mainMessage + "\" and submessage =\"" + subMessage + "\"");
				BindUtils.postGlobalCommand(null, null, "modalMessage", map);
			}
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
	}

	private String getEodStatusLabel(EodStatus eodStatus) {
		try {
			String label = WebFrontZkLabelsLocator.getLabel("com.ncr.webfront.base.eod.EodStatusObject.EodStatus." + eodStatus.name());
			return label;
		} catch (Exception e) {
			return eodStatus.name();
		}
	}

	private Window modalDialog = null;
	private EodStatusObject lastEodStatusObject;
	private EodStatusObject currentEodStatusObject;
	private Map<String, ArrayList<EndOfDayDecoratedObject>> posStatusListMap = new TreeMap<String, ArrayList<EndOfDayDecoratedObject>>();
	private List<PosStatusObject> posStatusList = new ArrayList<PosStatusObject>();
	private TreeModel<TreeNode<String>> eodTree;
	private boolean postEod;
	private int progressPercentage = 0;

	int sizeLine = 0;
	int numTerminal = 0;

	private boolean startEndOfDayButtonAlreadyPressed = false;
	private boolean startEndOfDayButtonEnabled = false;
	private boolean startRetryEndOfDayButtonEnabled = false;
	private boolean restartEndOfDayButtonVisible = false;

	private boolean updatePosStatusListTimerEnabled = false;

	private boolean checkAllPosCompletedEodTimerEnabled = false;
	private int checkAllPosCompletedEodTimerMaxRetries = 15;
	private int checkAllPosCompletedEodTimerCurrentCount = 0;

	private boolean updateStepsAndNotifyTimerEnabled = false;

	private boolean posStatusListVisible = false;
	private boolean eodTreeVisible = false;

	private EodServiceInterface eodService = new EodServiceClientImpl();

	private EndOfDayStatusTreeRenderer renderer;

	private static final String START_EOD = "start eod";
	private static final String RESTART_EOD = "restart eod";
	private static final String RESTART_EOD_FROMSTART = "restart eod from start";

	private boolean showNotificationPopups = false;

	private static final Logger logger = WebFrontLogger.getLogger(EndOfDayViewModel.class);
}
