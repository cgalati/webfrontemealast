package com.ncr.webfront.base.functionalities.endofday;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class EndOfDayMappingProperties {
	public static final String posStatusSmallIconSizeThreshold = "posstatus.smalliconsize.threshold";
	public static final int posStatusSmallIconSizeThresholdDefault = 10;

	public static final String posStatusIconsPerLine = "posstatus.iconsperline";
	public static final int posStatusIconsPerLineDefault = 4;

	public static final String showNotificationPopups = "endofday.notification.popups.show";
	public static final boolean showNotificationPopupsDefault = true;

	public static final String endOfDayTreeNodeStyle = "endofday.tree.node.style";
	public static final String endOfDayTreeNodeStyleDefault = "";

	public static final String endOfDayTreeNodeWarningStyle = "endofday.tree.node.warning.style";
	public static final String endOfDayTreeNodeWarningStyleDefault = "";

	public static final String endOfDayTreeNodeErrorStyle = "endofday.tree.node.error.style";
	public static final String endOfDayTreeNodeErrorStyleDefault = "";

	public static final String endOfDayReportsEnabled = "endofday.reports.enabled";
	public static final String endOfDayReportsEnabledDefault = "false";
	
	public static synchronized EndOfDayMappingProperties getInstance() {
		if (instance == null) {
			instance = new EndOfDayMappingProperties();
		}

		return instance;
	}

	private EndOfDayMappingProperties() {
		endOfDayProperties = PropertiesLoader.loadOrCreatePropertiesFile(endOfDayPropertiesFileName, getEndOfDayPropertiesMap());
	}

	private static Map<String, String> getEndOfDayPropertiesMap() {
		Map<String, String> posstatusPropertiesMap = new HashMap<>();

		posstatusPropertiesMap.put(posStatusSmallIconSizeThreshold, "" + posStatusSmallIconSizeThresholdDefault);
		posstatusPropertiesMap.put(posStatusIconsPerLine, "" + posStatusIconsPerLineDefault);
		posstatusPropertiesMap.put(showNotificationPopups, "" + showNotificationPopupsDefault);
		posstatusPropertiesMap.put(endOfDayTreeNodeStyle, "" + endOfDayTreeNodeStyleDefault);
		posstatusPropertiesMap.put(endOfDayTreeNodeWarningStyle, "" + endOfDayTreeNodeWarningStyleDefault);
		posstatusPropertiesMap.put(endOfDayTreeNodeErrorStyle, "" + endOfDayTreeNodeErrorStyleDefault);
		
	
		return posstatusPropertiesMap;
	}

	public String getProperty(String key) {
		return endOfDayProperties.getProperty(key);
	}
	

	public int getIntProperty(String key, int defaultValue) {
		int intValue = defaultValue;
		try {
			intValue = Integer.parseInt(getProperty(key));
		} catch (Exception e) {
			intValue = defaultValue;
		}
		return intValue;
	}
	
	public int getPosStatusSmallIconSizeThreshold() {
		int intValue = 0;
		try {
			String value = endOfDayProperties.getProperty(posStatusSmallIconSizeThreshold, "" + posStatusSmallIconSizeThresholdDefault);
			intValue = Integer.parseInt(value);
		} catch (Exception e) {
			intValue = posStatusSmallIconSizeThresholdDefault;
		}
		return intValue;
	}	
	public int getPosStatusIconsPerLine() {
		int intValue = 0;
		try {
			String value = endOfDayProperties.getProperty(posStatusIconsPerLine, "" + posStatusIconsPerLineDefault);
			intValue = Integer.parseInt(value);
		} catch (Exception e) {
			intValue = posStatusIconsPerLineDefault;
		}
		return intValue;
	}

	public boolean isShowNotificationPopups() {
		boolean bolValue = true;
		try {
			String value = endOfDayProperties.getProperty(showNotificationPopups, "" + showNotificationPopupsDefault);
			value = value.trim();
			if (value.equalsIgnoreCase("false")){
				bolValue = false;
			}
		} catch (Exception e) {
			bolValue= showNotificationPopupsDefault;
		}
		return bolValue;
	}

	public String getEndOfDayTreeNodeStyle() {
		String value="";
		try {
			value = endOfDayProperties.getProperty(endOfDayTreeNodeStyle,  endOfDayTreeNodeStyleDefault);
		} catch (Exception e) {
			value= endOfDayTreeNodeStyleDefault;
		}
		return value;
	}
	
	public String getEndOfDayTreeNodeErrorStyle() {
		String value="";
		try {
			value = endOfDayProperties.getProperty(endOfDayTreeNodeErrorStyle,  endOfDayTreeNodeErrorStyleDefault);
		} catch (Exception e) {
			value= endOfDayTreeNodeErrorStyleDefault;
		}
		return value;
	}
	
	public String getEndOfDayTreeNodeWarninngStyle() {
		String value="";
		try {
			value = endOfDayProperties.getProperty(endOfDayTreeNodeWarningStyle,  endOfDayTreeNodeWarningStyleDefault);
		} catch (Exception e) {
			value= endOfDayTreeNodeWarningStyleDefault;
		}
		return value;
	}
	
	public boolean getEndOfDayReportsEnabled() {
		String value="";
		try {
			value = endOfDayProperties.getProperty(endOfDayReportsEnabled, endOfDayReportsEnabledDefault);
		} catch (Exception e) {
			value= endOfDayReportsEnabledDefault;
		}
		return Boolean.valueOf(value);
	}		

	private final String endOfDayPropertiesFileName = "endofday.properties";

	private Properties endOfDayProperties;
	private static EndOfDayMappingProperties instance;
}
