package com.ncr.webfront.base.functionalities.endofday;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

import com.ncr.webfront.base.functionalities.posstatus.CurrentTerminalViewModel;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class InnerTerminalViewModel {
	@Init
    public void init(@ExecutionArgParam("posGroupMap") Map<String, List<EndOfDayDecoratedObject>> posGroupMap) {
        this.posGroupMap = posGroupMap;
    }
    
	public Map<String, List<EndOfDayDecoratedObject>> getPosGroupMap() {
		return posGroupMap;
	}
	
	public void setPosGroupMap(Map<String, List<EndOfDayDecoratedObject>> posGroupMap) {
		this.posGroupMap = posGroupMap;
	}
	
	public EndOfDayDecoratedObject getSelectedPosTerminal() {
		return selectedPosTerminal;
	}

	public void setSelectedPosTerminal(EndOfDayDecoratedObject selectedPosTerminal) {
		this.selectedPosTerminal = selectedPosTerminal;
	}
	
    @Command
    public void showModal(@BindingParam("terminal")EndOfDayDecoratedObject term) {      	
		selectedPosTerminal = term;		

		if (!term.getObject().getType().equals(PosTerminalType.NONE)) {
			try{
			final HashMap<String, Object> map = new HashMap<String, Object>();
	        map.put("selectedPosTerminal", term );
	    	Window modalDialog = (Window)Executions.createComponents("~./base/functionalities/endofday/dialogframe.zul", null, map);
	    	modalDialog.doModal();
			}catch (Exception e)
			{
				logger.error(e);
			}
		}
    }
	private final Logger logger = WebFrontLogger
			.getLogger(InnerTerminalViewModel.class);
	private EndOfDayDecoratedObject selectedPosTerminal;
	private Map<String, List<EndOfDayDecoratedObject>> posGroupMap;
}
