package com.ncr.webfront.base.functionalities.endofday;

import org.apache.log4j.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EodWarningsViewModel {
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("mainMessage") String mainMessage,
			@ExecutionArgParam("subMessage") String subMessage,
			@ExecutionArgParam("timeout") String timeout) {

		this.mainMessage = mainMessage;
		this.subMessage = subMessage;
		this.timeout = -1;
		if (timeout.length() > 0) {
			try {
				this.timeout = Integer.parseInt(timeout);
			} catch (Exception e) {
			}
		}

		Selectors.wireComponents(view, this, false);
	}
		
	@Command
	public void checkHideDialog() {
		if (timeout == -1) {
			return;
		}
		
		if (--timeout <= 0) {			
			eodWarningsDialog.detach();
		}
	}	
	
	@Command
	public void hideDialog() {
		if (timeout == -1) {
			return;
		}
		
		eodWarningsDialog.detach();
	}	
	
	@GlobalCommand
	@NotifyChange({ "mainMessage", "subMessage" })
	public void modalMessage(@BindingParam("mainMessage") String mainMessage, 
			@BindingParam("subMessage") String subMessage, 
			@BindingParam("timeout") String timeout) {
		final Object _this = this;
		this.mainMessage = mainMessage;
		BindUtils.postNotifyChange(null, null, _this, "mainMessage");
		this.subMessage = subMessage;
		BindUtils.postNotifyChange(null, null, _this, "subMessage");
		this.timeout = -1;
		if (timeout.length() > 0) {
			try {
				this.timeout = Integer.parseInt(timeout);
			} catch (Exception e) {
			}
		}
	}

	public String getMainMessage() {
		return mainMessage;
	}

	public void setMainMessage(String mainMessage) {
		this.mainMessage = mainMessage;
	}

	public String getSubMessage() {
		return subMessage;
	}

	public void setSubMessage(String subMessage) {
		this.subMessage = subMessage;
	}

	@Wire
	private Window eodWarningsDialog;
	private String mainMessage = "";
	private String subMessage = "";
	private int timeout = 0;
	private final Logger logger = WebFrontLogger.getLogger(EodWarningsViewModel.class);
}
