package com.ncr.webfront.base.functionalities.endofday;

import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.functionalities.posstatus.PosStatusMappingProperties;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.base.restclient.posoperatorservice.PosOperatorServiceClientImpl;

public class EndOfDayDecoratedObject {

	public EndOfDayDecoratedObject(PosStatusObject object, boolean small) {
		this.object = object;
		this.small = small;
		buildInfo();
	}

	private void buildInfo() {
		String status = SCLASS_NONE;
		infoText = TEXT_NONE;

		String time = "";
		String[] fastlanes = PosStatusMappingProperties.getInstance().getProperty(PosStatusMappingProperties.fastlanes).split(",");

		String size = small ? SMALL_TERM_IMAGE : BIG_TERM_IMAGE;

		initTerminalStatus();

		infoText = "Ready";

		if (!object.getType().equals(PosTerminalType.NONE)) {
			if (statusTerminal.equals("open")) {
				status = SCLASS_APERTA;
				infoText = TEXT_APERTA;
			} else if (statusTerminal.equals("error")) {
				status = SCLASS_GUASTA;
				infoText = TEXT_GUASTA;
			} else if (statusTerminal.equals("offline")) {
				status = SCLASS_OFFLINE;
				infoText = TEXT_OFFLINE;
			} else if (statusTerminal.equals("closed")) {
				if (object.getActionCode().equals("98")) {
					infoText = "Excluded";
				} /*
					 * else { //infoText = TEXT_CHIUSA; infoText = "Pronta"; }
					 */

				status = SCLASS_CHIUSA;
			}
		}

		if (object.getActionCode().equals("99")) {
			infoText = "Esecuted";
		}

		sclass = BUTTON + status + size;

		background = BUTTON + status + size;
		modalSclass = BUTTON + status + FRAME;

		PosOperatorServiceClientImpl posOperatorService = new PosOperatorServiceClientImpl();
		posOperatorService.getPosOperatorRoles();

		for (String code : object.getListOperatorCodes()) {
			PosOperator operator = posOperatorService.getByCode(code);
			if (operator != null) {
				object.getListOperatorNames().add(operator.getNameAndCode());
			} else {
				object.getListOperatorNames().add("");
			}
		}

		if (object.getOperatorCode() != null) {
			PosOperator temp = posOperatorService.getByCode(object.getOperatorCode());
			if (temp == null) {
				operator = new PosOperator("", "", "", "", "");
			} else {
				operator = new PosOperator(temp.getOperatorCode(), temp.getName(), temp.getPersonnelNo(), temp.getSecretNo(), temp.getRole());
			}
		}
	}

	public void initTerminalStatus() {
		typeError = "";
		statusTerminal = "";

		if (object.getActionCode().equals("98")) {
			typeError = "Excluded from EOD";
		}

		if (object.isDefectiveDeclarable()) {
			statusTerminal = "offline";
		} else {
			if (object.isReadyForPosEndOfDay()) {
				statusTerminal = "closed";
			} else {
				if (object.getStatusCode().equals("02")) {
					statusTerminal = "error";
					typeError = "Total mismatch";
				} else {
					if (object.getActionCode() != null) {
						if (object.getActionCode().equals("97")) {
							statusTerminal = "error";
							typeError = "Printer Error";
						} else {
							statusTerminal = "open";
						}
					}
				}
			}
		}
	}

	public String getBackground() {
		return background;
	}

	public String getSclass() {
		return sclass;
	}

	public PosStatusObject getObject() {
		return object;
	}

	public void setObject(PosStatusObject object) {
		this.object = object;
	}

	public String getInfoText() {
		return infoText;
	}

	public void setInfoText(String infoText) {
		this.infoText = infoText;
	}

	public String getModalSclass() {
		return modalSclass;
	}

	public void setModalSclass(String modalSclass) {
		this.modalSclass = modalSclass;
	}

	public PosOperator getOperator() {
		return operator;
	}

	public void setOperator(PosOperator operator) {
		this.operator = operator;
	}

	public void setMinutesStatusShow(String minutesStatusShow) {
		this.minutesStatusShow = minutesStatusShow;
	}

	public String getMinutesStatusShow() {
		return minutesStatusShow;
	}

	public long getMinutesStatus() {
		return minutesStatus;
	}

	public String getStringShow() {
		return stringShow;
	}

	public void setStringShow(String stringShow) {
		this.stringShow = stringShow;
	}

	public String getTypeError() {
		return typeError;
	}

	public boolean getSmall() {
		return small;
	}

	private boolean small;
	private PosStatusObject object;
	private String infoText;
	private String sclass;
	private String background;
	private String modalSclass;
	private PosOperator operator;
	private String typeError = "";
	private String statusTerminal = "";
	private long minutesStatus;
	private String minutesStatusShow;
	private String stringShow = "";
	private static final String BUTTON = "button ";
	private static final String SCLASS_APERTA = "aperta-";
	private static final String SCLASS_APERTA_VUOTA = "aperta-vuota-";
	private static final String SCLASS_APERTA_SELF = "aperta-self-";
	private static final String SCLASS_GUASTA = "guasta-";
	private static final String SCLASS_OFFLINE = "offline-";
	private static final String SCLASS_CHIUSA = "chiusa-";
	private static final String SCLASS_PAUSA = "pausa-";
	private static final String SCLASS_NONE = "none-";
	private static final String TIME = "time-";

	private static final String TEXT_APERTA = "Open";
	private static final String TEXT_GUASTA = "Error";
	private static final String TEXT_OFFLINE = "Offline";
	private static final String TEXT_CHIUSA = "Closed";
	private static final String TEXT_PAUSA = "Pause";
	private static final String TEXT_ESCLUSA_DA_EOD = "Excluded from EOD";
	private static final String TEXT_NONE = "";

	private static final String FRAME = "frame";

	private static final String BIG_TERM_IMAGE = "100x100";
	private static final String SMALL_TERM_IMAGE = "100x50";
}
