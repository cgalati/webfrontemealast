package com.ncr.webfront.base.functionalities.endofday;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

public class EndOfDayPasswordCheckViewModel {

	private String callingMethod;
	private String password;
	private String eodPassword;
	
	@Wire("#eodPasswordCheck")
	private Window eodPasswordCheckWindow;
		
	public EndOfDayPasswordCheckViewModel() {
		eodPassword = EndOfDayMappingProperties.getInstance().getProperty("endofday.button.password");
	}

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		final Execution execution = Executions.getCurrent();
		callingMethod = (String) execution.getArg().get("callingMethod");
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Command
	public void doCheckPassword() {
		if (password.equals(eodPassword)) {
			
		    final Map<String, Object> map = new HashMap<String, Object>();
		    map.put("callingMethod", callingMethod);
			map.put("isEodPasswordChecked", true);
			BindUtils.postGlobalCommand(null, null, "doCheckPasswordState", map);
			
		    doCloseDialog();
			
		} else {
			Messagebox.show("The password is invalid!", "Check the password of end of day", Messagebox.OK, Messagebox.ERROR);
		}
	}
	
	@Command
	public void doCloseDialog() {
		eodPasswordCheckWindow.detach();
	}
}
