package com.ncr.webfront.base;

import org.apache.log4j.Logger;
import javax.servlet.ServletContextEvent;
import com.ncr.webfront.core.WebFrontCoreInitializer;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.versioning.VersionManager;

public class WebFrontBaseInitializer extends WebFrontCoreInitializer {

	public void contextInitialized(ServletContextEvent contextEvent) {
		super.contextInitialized(contextEvent);
		VersionManager.changeApplicationName(new VersionWebFrontBase().getComponentName());

		logger = WebFrontLogger.getLogger(WebFrontBaseInitializer.class);
		logger.info("BEGIN");
		WebFrontBase.initialize();
		logger.info("END");

	}

	public void contextDestroyed(ServletContextEvent contextEvent) {
	}

	private Logger logger;
}
