package com.ncr.webfront.base.plugins.ars.article;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestArsPluRecordTemplate {
	
	@Test
	public void testNormalPluRow(){
//		String pluRow = "111222222222222233334567889900001111222222222222222222223456789999999900000000";
		String pluRow = "   80183840461650261001300  00100000513G6 2 PAIA CALZE I0003          00000100";
		
		ArsPluRowTemplate arsPluRowTemplate = CommonArticleTestUtils.createArsPluRowTemplate();
		
		assertEquals ("   ", arsPluRowTemplate.getFiller1(pluRow));
		assertEquals ("8018384046165", arsPluRowTemplate.getArticleCode(pluRow));
		assertEquals ("0261", arsPluRowTemplate.getPosDpt(pluRow));
		assertEquals ("0", arsPluRowTemplate.getCode1(pluRow));
		assertEquals ("0", arsPluRowTemplate.getCode2(pluRow));
		assertEquals ("1", arsPluRowTemplate.getManualDiscountId(pluRow));
		assertEquals ("3", arsPluRowTemplate.getVatId(pluRow));
		assertEquals ("00", arsPluRowTemplate.getCode3(pluRow));
		assertEquals ("  ", arsPluRowTemplate.getPackagingType(pluRow));
		assertEquals ("0010", arsPluRowTemplate.getUnitsPerPackage(pluRow));
		assertEquals ("0000", arsPluRowTemplate.getLinkedItem(pluRow));
		assertEquals ("513G6 2 PAIA CALZE I", arsPluRowTemplate.getDescription(pluRow));
		assertEquals ("0", arsPluRowTemplate.getPrizeCode(pluRow));
		assertEquals ("0", arsPluRowTemplate.getZeroPriceBehavior(pluRow));
		assertEquals ("0", arsPluRowTemplate.getMembersOnly(pluRow));
		assertEquals ("3", arsPluRowTemplate.getAutomDiscountType(pluRow));
		assertEquals (" ", arsPluRowTemplate.getUpbOperationType(pluRow));
		assertEquals (" ", arsPluRowTemplate.getUpbProviderId(pluRow));
		assertEquals ("        ", arsPluRowTemplate.getFiller2(pluRow));
		assertEquals ("00000100", arsPluRowTemplate.getPrice(pluRow));
	}

	@Test
	public void testNonDiscountablePluRow(){
//		String pluRow = "111222222222222233334567889900001111222222222222222222223456789999999900000000";
		String pluRow = "   97719749203040241000000  00100000ABB.CORRIERE 2 CD DV0000          00000999";
		
		ArsPluRowTemplate arsPluRowTemplate = CommonArticleTestUtils.createArsPluRowTemplate();
		
		assertEquals ("   ", arsPluRowTemplate.getFiller1(pluRow));
		assertEquals ("9771974920304", arsPluRowTemplate.getArticleCode(pluRow));
		assertEquals ("0241", arsPluRowTemplate.getPosDpt(pluRow));
		assertEquals ("0", arsPluRowTemplate.getCode1(pluRow));
		assertEquals ("0", arsPluRowTemplate.getCode2(pluRow));
		assertEquals ("0", arsPluRowTemplate.getManualDiscountId(pluRow));
		assertEquals ("0", arsPluRowTemplate.getVatId(pluRow));
		assertEquals ("00", arsPluRowTemplate.getCode3(pluRow));
		assertEquals ("  ", arsPluRowTemplate.getPackagingType(pluRow));
		assertEquals ("0010", arsPluRowTemplate.getUnitsPerPackage(pluRow));
		assertEquals ("0000", arsPluRowTemplate.getLinkedItem(pluRow));
		assertEquals ("ABB.CORRIERE 2 CD DV", arsPluRowTemplate.getDescription(pluRow));
		assertEquals ("0", arsPluRowTemplate.getPrizeCode(pluRow));
		assertEquals ("0", arsPluRowTemplate.getZeroPriceBehavior(pluRow));
		assertEquals ("0", arsPluRowTemplate.getMembersOnly(pluRow));
		assertEquals ("0", arsPluRowTemplate.getAutomDiscountType(pluRow));
		assertEquals (" ", arsPluRowTemplate.getUpbOperationType(pluRow));
		assertEquals (" ", arsPluRowTemplate.getUpbProviderId(pluRow));
		assertEquals ("        ", arsPluRowTemplate.getFiller2(pluRow));
		assertEquals ("00000999", arsPluRowTemplate.getPrice(pluRow));
	}

	@Test
	public void testUpbPluRow(){
//		String pluRow = "111222222222222233334567889900001111222222222222222222223456789999999900000000";
		String pluRow = "   80329933107760241001000  00100000RIC.10EU TIM VIRTUAL000221        00001000";
		
		ArsPluRowTemplate arsPluRowTemplate = CommonArticleTestUtils.createArsPluRowTemplate();
		
		assertEquals ("   ", arsPluRowTemplate.getFiller1(pluRow));
		assertEquals ("8032993310776", arsPluRowTemplate.getArticleCode(pluRow));
		assertEquals ("0241", arsPluRowTemplate.getPosDpt(pluRow));
		assertEquals ("0", arsPluRowTemplate.getCode1(pluRow));
		assertEquals ("0", arsPluRowTemplate.getCode2(pluRow));
		assertEquals ("1", arsPluRowTemplate.getManualDiscountId(pluRow));
		assertEquals ("0", arsPluRowTemplate.getVatId(pluRow));
		assertEquals ("00", arsPluRowTemplate.getCode3(pluRow));
		assertEquals ("  ", arsPluRowTemplate.getPackagingType(pluRow));
		assertEquals ("0010", arsPluRowTemplate.getUnitsPerPackage(pluRow));
		assertEquals ("0000", arsPluRowTemplate.getLinkedItem(pluRow));
		assertEquals ("RIC.10EU TIM VIRTUAL", arsPluRowTemplate.getDescription(pluRow));
		assertEquals ("0", arsPluRowTemplate.getPrizeCode(pluRow));
		assertEquals ("0", arsPluRowTemplate.getZeroPriceBehavior(pluRow));
		assertEquals ("0", arsPluRowTemplate.getMembersOnly(pluRow));
		assertEquals ("2", arsPluRowTemplate.getAutomDiscountType(pluRow));
		assertEquals ("2", arsPluRowTemplate.getUpbOperationType(pluRow));
		assertEquals ("1", arsPluRowTemplate.getUpbProviderId(pluRow));
		assertEquals ("        ", arsPluRowTemplate.getFiller2(pluRow));
		assertEquals ("00001000", arsPluRowTemplate.getPrice(pluRow));
	}

}
