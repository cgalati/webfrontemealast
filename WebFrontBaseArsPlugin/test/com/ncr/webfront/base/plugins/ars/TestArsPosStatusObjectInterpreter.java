package com.ncr.webfront.base.plugins.ars;

import static org.junit.Assert.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.ncr.webfront.base.posterminal.PosTerminalActionCode;

public class TestArsPosStatusObjectInterpreter {
	private static final int ONLINE = 0x00;
	private static final int OFFLINE = 0x01;
	private static final int STANDALONE = 0x02;
	private static final int SPC_ONLINE = 0x04;
	private static final int EOD_SELECTED = 0x08;
	private static final int PICKUP_NECESSARY = 0x20;
	private static final int MIRRORING_STATE = 0x80;

	@Test
	public void testHexStringToInt() {
		int expectedStatus;
		String statusCode;
		int status;

		expectedStatus = 0;
		statusCode = "00";
		status = convertHexStringToInt(statusCode);
		System.out.println(statusCode + " -> " + status);
		assertEquals(expectedStatus, status);
		check(status, ONLINE);

		expectedStatus = 1;
		statusCode = "01";
		status = convertHexStringToInt(statusCode);
		System.out.println(statusCode + " -> " + status);
		assertEquals(expectedStatus, status);
		check(status, OFFLINE);

		expectedStatus = 10;
		statusCode = "0A";
		status = convertHexStringToInt(statusCode);
		System.out.println(statusCode + " -> " + status);
		assertEquals(expectedStatus, status);
		check(status, STANDALONE);
		check(status, EOD_SELECTED);

		expectedStatus = 12;
		statusCode = "0C";
		status = convertHexStringToInt(statusCode);
		System.out.println(statusCode + " -> " + status);
		assertEquals(expectedStatus, status);
		check(status, SPC_ONLINE);
		check(status, EOD_SELECTED);

	}

	private void check(int status, int value) {
		if ((status & value) == value) {
			return;
		}
		fail();
	}

	public int convertHexStringToInt(String statusCode) {
		int status = -1;
		try {
			status = Integer.valueOf(statusCode, 16);
		} catch (Exception e) {
			status = -1;
		}
		return status;
	}
}
