package com.ncr.webfront.base.plugins.ars.article;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.junit.Test;

import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.base.plugins.ars.article.ArsArticleServiceImplementation;
import com.ncr.webfront.base.plugins.ars.article.ArsPluHashFileActuatorInterface;
import com.ncr.webfront.base.plugins.ars.article.ArsPluRowTemplate;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.tare.TareServiceInterface;
import com.ncr.webfront.base.vat.VatServiceInterface;

public class TestArsArticleServiceImplementation {

	@Test
	public void testSearchByArticleCodeSuccess() {

		VatServiceInterface vatService = null;
		TareServiceInterface tareService = null;
		PosDepartmentServiceInterface posDepartmentService = null;
		ArsPluHashFileActuatorInterface arsPluHashFileActuatorMock = new ArsPluHashFileActuatorMock();
		ArsPluRowTemplate arsPluRowTemplate = CommonArticleTestUtils.createArsPluRowTemplate();
		ArticleServiceInterface articleService = new ArsArticleServiceImplementation(arsPluHashFileActuatorMock, posDepartmentService, vatService, tareService,
		      arsPluRowTemplate);

		String articleCode = "8300434230045";
		List<Article> articles = articleService.searchByArticleCode(articleCode);
		assertEquals(1, articles.size());

		assertEquals(articleCode, articles.get(0).getArticleCode());

	}

	private class ArsPluHashFileActuatorMock implements ArsPluHashFileActuatorInterface {
		@Override
		public List<String> searchByArticleCode(String articleCode) {
			List<String> articleList = new ArrayList<String>();
			articleList.add("   83004342300450263001300  00100000SMART BOX EXAMPLE   000361        00000300");
			return articleList;
		}

		@Override
		public List<String> searchByStartsWithArticleCode(String articleCode) {
			List<String> articleList = new ArrayList<String>();
			articleList.add("   83004342300450263001300  00100000SMART BOX EXAMPLE   000361        00000300");
			return articleList;
		}

		@Override
		public List<String> searchByDescription(String description, int maxSearchRowsNumber) {
			return null;
		}

		@Override
		public ErrorObject deleteArticle(String articleCode) {
			return null;
		}

		@Override
		public ErrorObject addArticle(String articleCode, String linePlu) {
			return null;
		}

		@Override
		public ErrorObject updateArticle(String articleCode, String linePlu) {
			return null;
		}

		@Override
		public String findArticle(String articleCode) {
			return null;
		}

	}

	private List<String> eanXlines;

	@Test
	public void testPriceEmbedded() {

		eanXlines = new ArrayList<String>();

		eanXlines.add("EANX0:P$0:   20?????-----%P$0:   21?????-----%");
		eanXlines.add("EANX1:P$0:   22?????-----%P$0:   23?????-----%");
		eanXlines.add("EANX2:P$0:   24?????-----%P$0:   25?????-----%");
		eanXlines.add("EANX3:P$0:   26?????-----%P$0:   271????-----%");
		eanXlines.add("EANX4:P$0:   28?????abcde%P$0:   278????-----%");
		eanXlines.add("EANX5:P$0:    2?sss?-----%P$0:   02?sss?-----%");
		eanXlines.add("EANX6:C 0:   04701NNNNNNN%C 0:   04702NNNNNNN%");
		eanXlines.add("EANX7:C 0:   04703NNNNNNN%  :                 ");
		eanXlines.add("EANX8:P$0:        8??----%F 0:       0NNNNNNN%");
		eanXlines.add("EANXD:UA1:     ???????????UE1:         0??????");
		eanXlines.add("EANXE:S 1:   0???????????%S 4:    0000???????%");
		eanXlines.add("EANXF:   :                ZE4:        0??????%");

		String articleCode;
		String expePattern;
		String expCleanCod;
		int expectedPrice;

		articleCode = "2780880006788";
		expePattern = "278????-----%";
		expCleanCod = "2780880000007";
		expectedPrice = 678;
		assertTrue(ArsPluUtils.isPriceEmbedded(eanXlines, articleCode));
		assertEquals(expePattern, ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode));
		assertEquals(expectedPrice, ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode));
		assertEquals(expCleanCod, ArsPluUtils.clearEmbeddedPrice(eanXlines, articleCode));

		articleCode = "8780880006788";
		assertFalse(ArsPluUtils.isPriceEmbedded(eanXlines, articleCode));

		articleCode = "2780889123458";
		expePattern = "278????-----%";
		expectedPrice = 12345;
		assertTrue(ArsPluUtils.isPriceEmbedded(eanXlines, articleCode));
		assertEquals(expePattern, ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode));
		assertEquals(expectedPrice, ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode));

		articleCode = "2890889123458";
		expePattern = "28?????abcde%";
		assertTrue(ArsPluUtils.isPriceEmbedded(eanXlines, articleCode));
		assertEquals(expePattern, ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode));
		assertNull(ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode));

		articleCode = "2712348678919";
		expePattern = "271????-----%";
		expCleanCod = "2712348000007";
		expectedPrice = 67891;
		assertTrue(ArsPluUtils.isPriceEmbedded(eanXlines, articleCode));
		assertEquals(expePattern, ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode));
		assertEquals(expectedPrice, ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode));
		assertEquals(expCleanCod, ArsPluUtils.clearEmbeddedPrice(eanXlines, articleCode));

		articleCode = "84512345";
		expePattern = "8??----%";
		expCleanCod = "84500007";
		expectedPrice = 1234;
		assertTrue(ArsPluUtils.isPriceEmbedded(eanXlines, articleCode));
		assertEquals(expePattern, ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode));
		assertEquals(expectedPrice, ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode));
		assertEquals(expCleanCod, ArsPluUtils.clearEmbeddedPrice(eanXlines, articleCode));
	
	}
}
