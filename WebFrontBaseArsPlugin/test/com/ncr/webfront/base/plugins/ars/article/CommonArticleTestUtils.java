package com.ncr.webfront.base.plugins.ars.article;

import java.util.HashMap;
import java.util.Map;

public class CommonArticleTestUtils {

	public static ArsPluRowTemplate createArsPluRowTemplate() {
		ArsPluRecordField arsPluRecordField;
		Map<String, ArsPluRecordField> pluRowFields = new HashMap <String, ArsPluRecordField> ();
		
		arsPluRecordField = new ArsPluRecordField(1,3);
		pluRowFields.put ("filler1", arsPluRecordField);
		
		arsPluRecordField = new ArsPluRecordField(4,13);
		pluRowFields.put ("articleCode", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(17,4);
		pluRowFields.put ("posDpt", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(21,1);
		pluRowFields.put ("code1", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(22,1);
		pluRowFields.put ("code2", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(23,1);
		pluRowFields.put ("manualDiscountId", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(24,1);
		pluRowFields.put ("vatId", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(25,2);
		pluRowFields.put ("code3", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(27,2);
		pluRowFields.put ("packagingType", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(29,4);
		pluRowFields.put ("unitsPerPackage", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(33,4);
		pluRowFields.put ("linkedItem", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(37,20);
		pluRowFields.put ("description", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(57,1);
		pluRowFields.put ("prizeCode", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(58,1);
		pluRowFields.put ("zeroPriceBehavior", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(59,1);
		pluRowFields.put ("membersOnly", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(60,1);
		pluRowFields.put ("automDiscountType", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(61,1);
		pluRowFields.put ("upbOperationType", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(62,1);
		pluRowFields.put ("upbProviderId", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(63,8);
		pluRowFields.put ("filler2", arsPluRecordField);

		arsPluRecordField = new ArsPluRecordField(71,8);
		pluRowFields.put ("price", arsPluRecordField);

		return new ArsPluRowTemplate(pluRowFields);
   }


}
