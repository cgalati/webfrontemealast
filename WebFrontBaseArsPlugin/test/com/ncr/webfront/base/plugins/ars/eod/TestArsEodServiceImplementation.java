package com.ncr.webfront.base.plugins.ars.eod;

import org.junit.Ignore;
import org.junit.Test;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public class TestArsEodServiceImplementation {

	@SuppressWarnings("null")
   @Test
	@Ignore
	public void test() throws Exception {

		
		ArsEodServiceImplementation arsEodServiceImplementation = null;

		if (!arsEodServiceImplementation.getEodStatus().getStatus().isRunning()) {

			ErrorObject errorObject;
			do {
				// Fill list of POs states based on
				// arsEodServiceImplementation.getAllPosStatus()
				// The list has three columns: checkbox x difettosa / N. cassa
				// /descrizione stato getPosStatusDescription()
				// NOTE: the checkbox on each row should be enabled only if the
				// status file has isDefectiveDeclarable() set to true
				// if all POs have status flag isReadyForPosEndOfday() set to true
				// -> enable "Avvia fine giorno" button
				// if at least one POs has status flag isDefectiveDeclarable() set
				// to true -> enable "Dichiara difettoso" button

				// NOTE: refresh this POS status list each xx seconds (es. 10
				// seconds)

				// when "Avvia fine giorno" button is pressed:
				errorObject = arsEodServiceImplementation.launchEodAllPos();
			} while (errorObject.getErrorCode() != ErrorObject.ErrorCode.OK_NO_ERROR && errorObject.getErrorCode() != ErrorObject.ErrorCode.OK_WARNING);

			errorObject = arsEodServiceImplementation.launchEodServer();
		}

		boolean isRunning = arsEodServiceImplementation.getEodStatus().getStatus().isRunning();
		do {

			// Hide the list of POs states if screen space is needed
			
			// Show the tree with the EOD execution status

			// NOTE: refresh this tree each xx seconds (es. 10 seconds)
			isRunning = arsEodServiceImplementation.getEodStatus().getStatus().isRunning();
		} while (isRunning);
	}

}
