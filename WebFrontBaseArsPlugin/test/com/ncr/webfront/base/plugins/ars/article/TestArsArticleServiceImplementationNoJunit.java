package com.ncr.webfront.base.plugins.ars.article;

import java.util.ArrayList;
import java.util.List;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.plugins.ars.article.ArsArticleServiceImplementation;
import com.ncr.webfront.base.plugins.ars.article.ArsPluRowTemplate;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.tare.TareServiceInterface;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceInterface;

public class TestArsArticleServiceImplementationNoJunit {

	public static void main(String[] args) {

		PosDepartmentServiceInterface mockPosDepartmentService = new MockPosDepartmentService();
		VatServiceInterface mockVatService = new MockVatService();
		TareServiceInterface mockTareService = new MockTareService();
		ArsPluRowTemplate arsPluRowTemplate = CommonArticleTestUtils.createArsPluRowTemplate();
		ArticleServiceInterface articleService = new ArsArticleServiceImplementation(mockPosDepartmentService, mockVatService, mockTareService, arsPluRowTemplate, null);
		ErrorObject errorObject;

		/////////////////////////////////////////////////////////////////////////////////
		
		System.out.println("searchByArticleCode-------------------------------");
		
		String articleCode = "   2150";
		List<Article> articles = articleService.searchByArticleCode(articleCode);
		
		if (articles.size() == 0){
			System.out.println("article not found");			
		}
		
		for(Article articlesFound:articles){
			System.out.println(articlesFound.getArticleCode());
			
		}
		
		/////////////////////////////////////////////////////////////////////////////////

		System.out.println("searchByDescription-------------------------------");
		
		String description = "POLLO";
		articles = articleService.searchByDescription(description);
		
		for(Article articlesFound:articles){
			System.out.println(articlesFound.getArticleCode());
			
		}
		
		/////////////////////////////////////////////////////////////////////////////////

		System.out.println("deleteArticle"+articles.get(0).getArticleCode() +"-------------------------------");
		
		
		errorObject = articleService.deleteArticle(articles.get(0).getArticleCode());
		
		System.out.println(errorObject.getDescription()+" " + errorObject.getErrorCode());
		

				
		/////////////////////////////////////////////////////////////////////////////////
		
//		System.out.println("deleteArticle"+articles.get(0).getArticleCode() +" again-------------------------");
//
//		errorObject = articleService.deleteArticle(articles.get(0));
//		
//		System.out.println(errorObject.getDescription()+" " + errorObject.getErrorCode());
//		
		

		/////////////////////////////////////////////////////////////////////////////////

//		System.out.println("deleteArticle not present 9999999999999");
//		
//		articles.get(0).setArticleCode("9999999999999");
//
//		errorObject = articleService.deleteArticle(articles.get(0).getArticleCode());
//		
//		System.out.println(errorObject.getDescription()+" " + errorObject.getErrorCode());
				
		
		/////////////////////////////////////////////////////////////////////////////////

		System.out.println("add article 20140960");
		
		articles.get(0).setArticleCode("20140960");

		errorObject = articleService.addArticle(articles.get(0));
		
		System.out.println(errorObject.getDescription()+" " + errorObject.getErrorCode());

		/////////////////////////////////////////////////////////////////////////////////
		
		System.out.println("update price in article 20140960");
		
		articles.get(0).setPrice(1234);

		errorObject = articleService.updateArticle(articles.get(0));
		
		System.out.println(errorObject.getDescription()+" " + errorObject.getErrorCode());

	}

	static class MockPosDepartmentService implements PosDepartmentServiceInterface {


		@Override
	   public ErrorObject checkService() {
		   return new ErrorObject(ErrorCode.OK_NO_ERROR, "");
	   }

		
		@Override
		public List<PosDepartment> getAll() {

			if (dptList == null)
				buildDptList();
			return dptList;
		}

		@Override
		public List<PosDepartment> getDepartmentsOfLevel(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public PosDepartment getByCode(String dptCode) {
			if (dptList == null)
				buildDptList();

			for (PosDepartment posDepartment : dptList) {
				if (posDepartment.getCode().equals(dptCode))
					return posDepartment;
			}

			return null;
		}
		
		@Override
		public ErrorObject deleteDepartment(String departmentCodeToDelete) {
			return new ErrorObject(ErrorCode.OK_NO_ERROR, "OK");
		}
		
		@Override
		public ErrorObject addDepartment(PosDepartment departmentToAdd){
			return new ErrorObject(ErrorCode.OK_NO_ERROR, "OK");
		}
		
		@Override
		public ErrorObject updateDepartment(PosDepartment departmentToUpdate){
			return new ErrorObject(ErrorCode.OK_NO_ERROR, "OK");
		}
		
		@Override
		public PosDepartment createDepartment(){
			return new PosDepartment();
		}

		private void buildDptList() {

			dptList = new ArrayList<PosDepartment>();

			PosDepartment posDepartment = new PosDepartment();
			posDepartment.setCode("0010");
			posDepartment.setDescription("PROVA");
			dptList.add(posDepartment);
			posDepartment = new PosDepartment();
			posDepartment.setCode("0001");
			posDepartment.setDescription("ORTOFRUTTA");
			dptList.add(posDepartment);
			posDepartment = new PosDepartment();
			posDepartment.setCode("0224");
			posDepartment.setDescription("POLLAME");
			dptList.add(posDepartment);
		}

		
		
		
		
		private List<PosDepartment> dptList = null;
	}

	static class MockVatService implements VatServiceInterface {

		public MockVatService() {
			vatList = new ArrayList<Vat>();

			addVat(vatList, 400);
			addVat(vatList, 1000);
			addVat(vatList, 2100);
		}

		private void addVat(List<Vat> vatList, int rate) {
			Vat vat = new Vat();

			vat.setVatId(vatList.size());
			vat.setDescription(String.format("IVA %d%%", rate / 100));
			vat.setRate(rate);

			vatList.add(vat);
		}

		@Override
	   public ErrorObject checkService() {
		   return new ErrorObject(ErrorCode.OK_NO_ERROR, "");
	   }
		
		@Override
		public List<Vat> getAll() {
			return vatList;
		}

		@Override
		public Vat getVatById(int vatId) {
			try {
				return vatList.get(vatId);
			} catch (IndexOutOfBoundsException e) {
				return null;
			}
		}
		
		@Override
		public Vat getDefault() {
			return vatList.get(2);
		}

		private List<Vat> vatList;

	}

	static class MockTareService implements TareServiceInterface {

		public MockTareService() {
			tareList = new ArrayList<Tare>();
				
			addTare(tareList, 0, "Nessuna tara");
			addTare(tareList, 1, "Tara 1");
			addTare(tareList, 2, "Tara 2");
			addTare(tareList, 3, "Tara 3");
			addTare(tareList, 4, "Tara 4");
			addTare(tareList, 5, "Tara 5");
			addTare(tareList, 6, "Tara 6");
			addTare(tareList, 7, "Tara 7");
			addTare(tareList, 8, "Tara 8");
			addTare(tareList, 9, "Tara 9");
			
		}

		@Override
	   public ErrorObject checkService() {
		   return new ErrorObject(ErrorCode.OK_NO_ERROR, "");
	   }
		
		@Override
		public List<Tare> getAll() {
			return tareList;
		}

		@Override
		public Tare getTareById(int tareId) {
			return tareList.get(tareId);
		}
		
		private void addTare(List<Tare> tareList, int tareId, String desc) {
			Tare tare = new Tare();
			
			tare.setTareId(tareId);
			tare.setDescription(desc);
			tareList.add(tare);
		}
		

		private List<Tare> tareList;

	}


}
