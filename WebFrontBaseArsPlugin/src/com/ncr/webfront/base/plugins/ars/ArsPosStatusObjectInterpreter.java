package com.ncr.webfront.base.plugins.ars;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.eod.PosStatusObject.PosStatus;
import com.ncr.webfront.base.posterminal.PosTerminalActionCode;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ArsPosStatusObjectInterpreter {
	private static final String SEPARATOR = ":";
	public static final String NO_OPERATOR = "000";
	public static final String POS_ONLINE = "00";
	private static final int CODE_SECTION = 0;
	private static final int TERMINAL_CODE_SECTION = 1;
	private static final int GROUP_TERMINAL = 2;
	private static final int OPERATOR_CODE_SECTION = 7;
	private static final int ACTION_CODE_SECTION = 8;
	private static final int DC_COUNTER_SECTION = 9;
	private static final int JRN_COUNTER_SECTION = 10;
	private static final int MAINTENANCE_COUNTER_SECTION = 11;
	private static final int DATE_SECTION = 15;
	private static final int TIME_SECTION = 16;
	private static final int STATUS_CODE_SECTION = 21;
	private static final int LIST_OPERATOR_CODE = 22;

	private static final int ONLINE = 0x00;
	private static final int OFFLINE = 0x01;
	private static final int STANDALONE = 0x02;
	private static final int SPC_ONLINE = 0x04;
	private static final int EOD_SELECTED = 0x08;
	private static final int PICKUP_NECESSARY = 0x20;
	private static final int MIRRORING_STATE = 0x80;

	private ArsPosStatusObjectInterpreter() {
	}

	public static PosStatusObject interpret(String mlanRegisterLine) {
		Logger log = WebFrontLogger.getLogger(ArsPosStatusObjectInterpreter.class);

		log.debug("BEGIN");
		ArsPosStatusObjectInterpreter interpreter = new ArsPosStatusObjectInterpreter();
		PosStatusObject result = new PosStatusObject();

		try {
			interpreter.sections = mlanRegisterLine.split(SEPARATOR);
			PropertyUtils.copyProperties(result, interpreter);
		} catch (Exception e) {
			log.error("Exception!", e);
		}

		log.debug("END");
		return result;
	}

	public String getCode() {
		log.debug("Code [" + sections[CODE_SECTION] + "]");
		return sections[CODE_SECTION];
	}

	public String getTerminalCode() {
		log.debug("Terminal Code [" + sections[TERMINAL_CODE_SECTION] + "]");
		return sections[TERMINAL_CODE_SECTION];
	}

	public String getGroupPos() {
		log.debug("Group Terminal [" + sections[GROUP_TERMINAL] + "]");
		String temp = sections[GROUP_TERMINAL];
		if (temp.equals("00")) {
			temp = "01";
		}
		return temp;
	}

	public List<String> getListOperatorCodes() {
		List<String> temp = new ArrayList<String>();
		for (int index = 0; index < sections[LIST_OPERATOR_CODE].length(); index += 3) {
			if (sections[LIST_OPERATOR_CODE].substring(index, index + 3).equals("000"))
				continue;
			temp.add("0" + sections[LIST_OPERATOR_CODE].substring(index, index + 3));
		}
		return temp;
	}

	public String getOperatorCode() {
		log.debug("Operator Code [" + sections[OPERATOR_CODE_SECTION] + "]");
		return sections[OPERATOR_CODE_SECTION];
	}

	public String getActionCode() {
		log.debug("Action Code [" + sections[ACTION_CODE_SECTION] + "]");
		return sections[ACTION_CODE_SECTION];
	}

	public String getStatusCode() {
		log.debug("Status Code [" + sections[STATUS_CODE_SECTION] + "]");
		return sections[STATUS_CODE_SECTION];
	}

	public String getDcCounter() {
		return sections[DC_COUNTER_SECTION];
	}

	public String getJrnCounter() {
		return sections[JRN_COUNTER_SECTION];
	}

	public String getMaintenanceCounter() {
		return sections[MAINTENANCE_COUNTER_SECTION];
	}

	public String getDate() {
		return sections[DATE_SECTION];
	}

	public String getTime() {
		return sections[TIME_SECTION];
	}

	public String getDateTime() {
		return sections[DATE_SECTION] + sections[TIME_SECTION];
	}

	public String getDateTimeOld() {
		String yyMMdd = sections[DATE_SECTION];
		String hhmm = sections[TIME_SECTION];
		DateFormat formatter = new SimpleDateFormat("yyMMddkkmm");
		Date convertedDate = new Date();
		try {
			convertedDate = (Date) formatter.parse(yyMMdd + hhmm);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(convertedDate);
	}

	public String getDescription() {
		Map<String, String> map = readMapWithStringKey(ArsMappingProperties.arsPosDescriptionData);

		return String.format(map.get(getCode()), getTerminalCode());
	}

	public PosTerminalType getType() {
		Map<String, String> map = readMapWithStringKey(ArsMappingProperties.arsPosTerminalTypeData);

		return PosTerminalType.valueOf(map.get(getCode()));
	}

	public PosStatus getPosStatus() {
		Map<Integer, String> map = readMapWithIntegerKey(ArsMappingProperties.arsPosStatusData);

		return PosStatus.valueOf(map.get(index()));
	}

	public boolean isDefectiveDeclarable() {
		Map<Integer, Boolean> map = readMapWithIntegerKey(ArsMappingProperties.arsDefectiveDeclarableData);

		return map.get(index());
	}

	public boolean isReadyForPosEndOfDay() {
		Map<Integer, Boolean> map = readMapWithIntegerKey(ArsMappingProperties.arsReadyPosEodData);

		return map.get(index());
	}

	public boolean isReadyForServerEndOfDay() {
		Map<Integer, Boolean> map = readMapWithIntegerKey(ArsMappingProperties.arsReadyServerEodData);

		return map.get(index());
	}

	public String getPosStatusDescription() {
		Map<String, String> map = readMapWithStringKey(ArsMappingProperties.arsPosStatusDescriptionData);

		return map.get(getPosStatus().toString());
	}

	public String getActionCodeDescription() {
		String actionCodePropertyKey = ArsActionCodeMappingProperties.arsActionCodeDefaultKeyPrefix + getActionCode();
		return ArsActionCodeMappingProperties.getInstance().getProperty(actionCodePropertyKey);
	}

	private int index() {
		String ac = getActionCode();
		boolean noOperator = StringUtils.equals(getOperatorCode(), NO_OPERATOR);
		int index = 0;
		int status = 0;
		status = convertHexStringToInt(getStatusCode(), ONLINE);

		log.debug(String.format("pos #%s", getTerminalCode()));
		log.debug(String.format("ac [%s]", ac));
		log.debug(String.format("noOperator [%b]", noOperator));
		log.debug(String.format("status [%s]", status));

		// The order is important and meaningful.
		if (StringUtils.equals(ac, PosTerminalActionCode.AC_ZEROED_TERMINAL.toString())) {
			index = 200;
		} else if (StringUtils.equals(ac, PosTerminalActionCode.AC_DEFECTIVE_TERMINAL.toString())) {
			index = 300;
		} else if ((status & OFFLINE) == OFFLINE && !noOperator) {
			index = 400;
		} else if (StringUtils.equals(ac, PosTerminalActionCode.AC_PRINTER_ERROR.toString())) {
			index = 100;
		} else if (StringUtils.equals(ac, PosTerminalActionCode.AC_87.toString()) || StringUtils.equals(ac, PosTerminalActionCode.AC_88.toString())) {
			index = 500;
		} else if (!StringUtils.equals(ac, PosTerminalActionCode.AC_02.toString()) && !noOperator) {
			index = 600;
		} else if ((status & STANDALONE) == STANDALONE && !noOperator) {
			index = 700;
		} else if ((status & MIRRORING_STATE) == MIRRORING_STATE) {
			index = 800;
		} else {
			index = 900;
		}

		log.debug(String.format("index [%d]", index));

		return index;
	}

	public int convertHexStringToInt(String statusCode, int defaultValue) {
		log.debug("BEGIN (" + statusCode + ")");
		int status = defaultValue;
		try {
			status = Integer.valueOf(statusCode, 16);
		} catch (Exception e) {
			log.error("Exception! Using default", e);
			status = defaultValue;
		}
		log.debug("END (" + status + ")");
		return status;
	}

	// These two methods, apparently useless, are necesserly because of the way
	// Java and TypeToken handles generics.
	// It's possible to write compiling code without these two, but that code
	// won't behave as intended at runtime.
	// To be more specific: the map will be filled, but it won't recognize any
	// key.
	private <T> Map<Integer, T> readMapWithIntegerKey(String propertyName) {
		Type mapType = new TypeToken<Map<Integer, T>>() {
		}.getType();

		return readMap(propertyName, mapType);
	}

	private <T> Map<String, T> readMapWithStringKey(String propertyName) {
		Type mapType = new TypeToken<Map<String, T>>() {
		}.getType();

		return readMap(propertyName, mapType);
	}

	private <K, V> Map<K, V> readMap(String propertyName, Type mapType) {
		String jsonFileName = ArsMappingProperties.getInstance().getProperty(propertyName, true);
		StringBuilder json = new StringBuilder();

		try (BufferedReader reader = new BufferedReader(new FileReader(jsonFileName))) {
			String line = "";

			while ((line = reader.readLine()) != null) {
				json.append(line);
			}
		} catch (Exception e) {
			log.error("", e);
		}

		return gson.fromJson(json.toString(), mapType);
	}

	private String[] sections;

	private static final Gson gson = new Gson();
	private static final Logger log = WebFrontLogger.getLogger(ArsPosStatusObjectInterpreter.class);
}
