package com.ncr.webfront.base.plugins.ars;

import java.util.List;

public interface ArsPRegDatFileActuatorInterface {
	
	public String readLine(String id);
	
	public List <String> readAll();	
	
	public List <String> readLines(String id);

}
