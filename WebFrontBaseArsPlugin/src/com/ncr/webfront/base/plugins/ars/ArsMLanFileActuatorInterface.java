package com.ncr.webfront.base.plugins.ars;

import java.util.List;

public interface ArsMLanFileActuatorInterface {
	
	public String readLine(int id);
	
	public String readLine(String idStr);
	
	public List <String> readAll();	
	
	public List <String> readAllPosTerminals();	

}
