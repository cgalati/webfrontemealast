package com.ncr.webfront.base.plugins.ars.eod;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.eod.EodServiceInterface;
import com.ncr.webfront.base.eod.EodStatusObject;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class EodServiceAbstractImplementation implements EodServiceInterface {
	public static final String CHARSET = System.getProperty("file.encoding");
	public static final int SOCKET_READ_BUFFER_SIZE = 16384;

	@Override
	public EodStatusObject getEodStatus() {
		EodStatusObject eodStatusObject = null;
		String json = "";
		try {
			json = request("getEodStatus");
			eodStatusObject = gson.fromJson(json, EodStatusObject.class);
		} catch (Exception e) {
			logger.error("json: \"" + json + "\"");
			logger.error("Exception in request or in gson.fromJson!", e);
			eodStatusObject = null;
		}
		return eodStatusObject;
	}

	@Override
	public List<EodStatusObject> getEodStatusHistory() {
		List<EodStatusObject> eodStatusHistory;
		String json = "";
		try {
			Type listType = new TypeToken<List<EodStatusObject>>() {
			}.getType();

			json = request("getEodStatusHistory");
			eodStatusHistory = gson.fromJson(json, listType);
		} catch (Exception e) {
			logger.error("json: \"" + json + "\"");
			if (json != null) {
				logger.error("json length = " + json.length());
			}
			logger.error("Exception in request or in gson.fromJson!", e);
			eodStatusHistory = new ArrayList<EodStatusObject>();
		}
		return eodStatusHistory;
	}

	@Override
	public ErrorObject launchEodServer() {
		try {
			return gson.fromJson(request("launchEodServer"), ErrorObject.class);
		} catch (Exception e) {
			logger.error("", e);

			return new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
		}
	}

	@Override
	public void cancel() {
		request("cancel");
	}

	/*
	 * Only one communication at a time: this ensures that no new communication
	 * can disrupt any running one. In particular it can be the case that,
	 * without synchronized, a timer-launched getEodStatus and a user-launched
	 * launchEodServer overlaps each other.
	 */
	private synchronized String request(String service) {
		String address = ArsMappingProperties.getInstance().getProperty(ArsMappingProperties.arsEodServerAddress);
		String port = ArsMappingProperties.getInstance().getProperty(ArsMappingProperties.arsEodServerPort);
		int trials = 3;
		String receivedString = "";

		logger.debug(String.format("connecting to %s:%s", address, port));

		logger.debug(String.format("Read buffer size = %d", SOCKET_READ_BUFFER_SIZE));

		Socket socket = null;
		DataInputStream is;

		do {
			try {
				socket = new Socket(address, Integer.parseInt(port));
				OutputStream out = socket.getOutputStream();
				is = new DataInputStream(socket.getInputStream());

				logger.debug("Socket created with success");

				out.write(service.getBytes(CHARSET));
				out.flush();

				logger.debug(String.format("written [%s]", service));

				int bufferSize = socket.getReceiveBufferSize();
				byte[] buffer = new byte[bufferSize];
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

				byte c[] = outputStream.toByteArray();
				int totalReadLength = 0;
				int currentReadLength;
				while ((currentReadLength = is.read(buffer)) != -1) {
					totalReadLength = totalReadLength + currentReadLength;
					logger.debug("Current read :" + currentReadLength + " bytes in buffer of size " + buffer.length);
					logger.debug("Partial read :" + totalReadLength + "bytes");
					outputStream.write(buffer, 0, currentReadLength);
				}
				receivedString = outputStream.toString();
				logger.debug("Total   read :" + totalReadLength + "bytes");
				logger.debug("receivedString.length():" + receivedString.length());
				logger.debug("receivedString =\"" + receivedString + "\"");

				return receivedString;
			} catch (Exception e) {
				trials--;

				if (trials > 0) {
					logger.warn(e);
					logger.info(String.format("retrying connection, [%d] trials remaining", trials));
				} else {
					logger.error("", e);

					throw new RuntimeException(e);
				}
			} finally {
				try {
					if (socket != null) {
						logger.debug("Calling socket.close()..");
						socket.close();
					}
					logger.debug("Calling socket = null...");
					socket = null;

				} catch (Exception e) {
					logger.error("finally", e);
				}

			}
		} while (trials > 0);

		// Unreachable.
		return "";
	}

	private Gson gson = new Gson();

	private static Logger logger = WebFrontLogger.getLogger(EodServiceAbstractImplementation.class);
}
