package com.ncr.webfront.base.plugins.ars.posstatus;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.plugins.ars.ArsMLanFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsPosStatusObjectInterpreter;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.posstatus.PosStatusServiceInterface;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ArsPosStatusServiceImplementation implements PosStatusServiceInterface {

	@Override
	public ErrorObject checkService() {

		try {
			List<PosStatusObject> posStatuses = getAllPosStatus();

			if (posStatuses == null || posStatuses.size() == 0) {
				logger.error("posStatusList is null or empty!");
				return new ErrorObject(ErrorCode.SETUP_ERROR, "It's impossible to get the registers (OFFLINE?) list from server!");
				
			}

		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}

		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	@Override
	public List<PosStatusObject> getAllPosStatus() {
		logger.debug("BEGIN");

		List<PosStatusObject> posStatuses = new ArrayList<PosStatusObject>();

		int rec = 0;
		try {
			while (true) {
				StringBuffer message = new StringBuffer();

				message.append("R:L").append(String.format("%04d", ++rec)).append(":")
						.append(ArsServerEnvParameters.getInstance().getSupervisorNumberForServerEod()).append(':').append(EMPTY);

				logger.debug(String.format("message [%s]", message));
				String answer = send(message.toString());
				// if (answer.startsWith("?")) {
				// break;
				// }
				if (answer.trim().length() == 12 + EMPTY.length()) {
					String posTerminalLine = answer.substring(12);
					PosStatusObject object = ArsPosStatusObjectInterpreter.interpret(posTerminalLine);
					if (object.getType().equals(PosTerminalType.REGISTER)) {
						posStatuses.add(object);
					}
				} else {
					break;
				}
			}
		} catch (Exception e) {
			logger.error("Exception!, e");
		}

		if (!checkPosStatusAgainstMLan(posStatuses)) {
			logger.warn("checkPosStatusAgainstMLan returned false!");
			posStatuses.clear();
		} else {
			logger.debug("checkPosStatusAgainstMLan returned true");
		}
		logger.debug("END (posStatuses.size() == " + posStatuses.size() + ")");
		return posStatuses;
	}

	private boolean checkPosStatusAgainstMLan(List<PosStatusObject> posStatuses) {
		logger.debug("BEGIN");
		if (posStatuses.size() == 0) {
			logger.warn("END (false because posStatuses.size() == 0)");
			return false;
		}

		String mlanFilePath = ArsMappingProperties.getInstance().getMlanPath();
		String serverNumber = ArsServerEnvParameters.getInstance().getServerNumber();
		ArsMLanFileActuator mlanActuator = new ArsMLanFileActuator(mlanFilePath, serverNumber);
		List<String> listMlanTerminals = mlanActuator.readAllPosTerminals();
		if (listMlanTerminals.size() != posStatuses.size()) {
			dumpMlanList("checkPosStatusAgainstMLan", listMlanTerminals);
			dumpPosStatusList("checkPosStatusAgainstMLan", posStatuses);
			logger.warn("END (false because listMlanTerminals.size() != posStatuses.size())");
			return false;
		}
		for (PosStatusObject posStatusObject : posStatuses) {
			boolean found = false;
			for (String mlanRow : listMlanTerminals) {
				if (mlanRow.length() > 6 && mlanRow.startsWith("R")) {
					String mlanRowTerminalCode = mlanRow.substring(2, 2 + 3);
					logger.debug("mlanRow                           = \"" + mlanRow + "\"");
					logger.debug("mlanRowTerminalCode               = \"" + mlanRowTerminalCode + "\"");
					logger.debug("posStatusObject.getTerminalCode() = \"" + posStatusObject.getTerminalCode() + "\"");
					if (mlanRowTerminalCode.equals(posStatusObject.getTerminalCode())) {
						found = true;
					}
				}

			}
			if (!found) {
				dumpMlanList("checkPosStatusAgainstMLan", listMlanTerminals);
				dumpPosStatusList("checkPosStatusAgainstMLan", posStatuses);
				logger.warn("END (false because element to element compare failed)");
				return false;
			}
		}
		logger.debug("END (true)");
		return true;
	}

	private void dumpMlanList(String callerName, List<String> mlanTerminalList) {
		logger.debug("--------- BEGIN dumpMlanList (" + callerName + ") ---------");
		if (mlanTerminalList == null) {
			logger.debug("mlanTerminalList == null");
			logger.debug("--------- END   dumpMlanList (" + callerName + ") ---------");
			return;
		}
		if (mlanTerminalList.size() == 0) {
			logger.debug("mlanTerminalList.size() == 0");
			logger.debug("--------- END   dumpMlanList (" + callerName + ") ---------");
			return;
		}

		for (String mlanRow : mlanTerminalList) {
			logger.debug(mlanRow);
		}
		logger.debug("--------- END   dumpMlanList (" + callerName + ") ---------");
	}

	private void dumpPosStatusList(String callerName, List<PosStatusObject> posStatusList) {
		logger.debug("--------- BEGIN dumpPosStatusList (" + callerName + ") ---------");
		if (posStatusList == null) {
			logger.debug("posStatusList == null");
			logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
			return;
		}
		if (posStatusList.size() == 0) {
			logger.debug("posStatusList.size() == 0");
			logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
			return;
		}

		for (PosStatusObject posStatusObject : posStatusList) {
			logger.debug("Terminal code: " + posStatusObject.getTerminalCode() + " - isDefectiveDeclarable: " + posStatusObject.isDefectiveDeclarable()
					+ " - isReadyForPosEndOfDay: " + posStatusObject.isReadyForPosEndOfDay() + " - isReadyForServerEndOfDay: "
					+ posStatusObject.isReadyForServerEndOfDay() + " - ActionCode: " + posStatusObject.getActionCode() + " - ActionCodeDescription: "
					+ posStatusObject.getActionCodeDescription() + " - PosStatus: " + posStatusObject.getPosStatus());
		}
		logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
	}

	@Override
	public PosStatusObject getPosStatus(String posCode) {
		// TODO check...
		return null;
	}

	/*
	 * Only one communication at a time: this ensures that no new communication
	 * can disrupt any running one.
	 */
	private synchronized String send(String... messages) {
		logger.debug("BEGIN");

		ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR);
		ArsServerEnvParameters arsServerEnvParameters = ArsServerEnvParameters.getInstance();
		String answer = "";

		try {
			InetAddress serverName = InetAddress.getByName("SRV" + arsServerEnvParameters.getServerNumber());
			int serverPort = 20000 + Integer.parseInt(arsServerEnvParameters.getServerNumber());
			int localPort = 20000 + Integer.parseInt(arsServerEnvParameters.getClientNumberForServerEod());

			logger.debug(String.format("serverName [%s]", serverName));
			logger.debug(String.format("serverPort [%d]", serverPort));
			logger.debug(String.format("localPort [%d]", localPort));

			try (Socket socket = new Socket(serverName, serverPort, InetAddress.getLocalHost(), localPort)) {
				socket.setSoTimeout(8000);
				socket.setSoLinger(true, 0);

				answer = send2(socket, messages);
			}
		} catch (Exception e) {
			logger.error("Exception!", e);

			errorObject = new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			answer = errorObject.toString();
		}

		logger.debug("END");
		return answer;
	}

	private String send2(Socket socket, String... messages) throws Exception {
		logger.debug("BEGIN");
		InputStream in = socket.getInputStream();
		OutputStream out = socket.getOutputStream();
		String answer = "";

		hello(socket, in, out);

		for (String message : messages) {
			byte[] bytes = message.getBytes(CHARSET);
			int outLength = 0;

			out.write(bytes);
			out.flush();
			outLength = bytes.length;

			logger.debug(String.format("outLength [%d]", outLength));

			if ((outLength != in.read(bytes)) && (message.charAt(2) != 'D')) {
				logger.debug("END (SocketException)");
				throw new SocketException("The size is invalid for the response.");
			}

			logger.debug(String.format("response [%s]", new String(bytes, CHARSET)));
			answer = new String(bytes, CHARSET);
		}
		logger.debug("END (no exception)");
		return answer;
	}

	private void hello(Socket socket, InputStream in, OutputStream out) throws Exception {
		logger.debug("BEGIN");
		try {
			String clientToRunEod = ArsServerEnvParameters.getInstance().getClientNumberForServerEod();
			byte[] bytes = new byte[SOCKET_READ_BUFFER];

			logger.debug("sending hello...");

			out.write(String.format("!:REG%s:0090:0000:00000000:00000000:00000000:00000000:00000000:00000000", clientToRunEod).getBytes(CHARSET));
			out.flush();

			logger.debug("...hello sent");

			int readCount = in.read(bytes);
			logger.debug("readCount = " + readCount);
			logger.debug("bytes.length = " + bytes.length);
			if (bytes.length > 0) {
				logger.debug("bytes[0] = '" + bytes[0] + "'");
			}

			if (readCount < 1 || bytes[0] != 0x21) {
				logger.debug("END (SocketException)");
				throw new SocketException("No answer");
			}

		} catch (Exception e) {
			logger.debug("END (Exception)");
			throw e;
		}
		logger.debug("END (no exception)");
	}

	private static Logger logger = WebFrontLogger.getLogger(ArsPosStatusServiceImplementation.class);
	public static final String CHARSET = System.getProperty("file.encoding");
	//                                  0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
	//                                  0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567
	public static final String EMPTY = "                                                                                                                                                                                                                ";
	public static final int SOCKET_READ_BUFFER = 10240;
}
