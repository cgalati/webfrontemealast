package com.ncr.webfront.base.plugins.ars.article;

import java.util.Map;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ArsPluRowTemplate {

	private Logger logger = WebFrontLogger.getLogger(ArsPluRowTemplate.class);

	public ArsPluRowTemplate(Map<String, ArsPluRecordField> pluRowFields) {
		this.pluRowFields = pluRowFields;
	}

	public String getFiller1(String pluRow) {
		return getField(pluRow, "filler1");
	}

	public String getArticleCode(String pluRow) {
		return getField(pluRow, "articleCode");
	}

	public String getPosDpt(String pluRow) {
		return getField(pluRow, "posDpt");
	}

	public String getCode1(String pluRow) {
		return getField(pluRow, "code1");
	}

	public String getCode2(String pluRow) {
		return getField(pluRow, "code2");
	}

	public String getManualDiscountId(String pluRow) {
		return getField(pluRow, "manualDiscountId");
	}

	public String getVatId(String pluRow) {
		return getField(pluRow, "vatId");
	}

	public String getCode3(String pluRow) {
		return getField(pluRow, "code3");
	}

	public String getPackagingType(String pluRow) {
		return getField(pluRow, "packagingType");
	}

	public String getUnitsPerPackage(String pluRow) {
		return getField(pluRow, "unitsPerPackage");
	}

	public String getLinkedItem(String pluRow) {
		return getField(pluRow, "linkedItem");
	}

	public String getDescription(String pluRow) {
		return getField(pluRow, "description");
	}

	public String getPrizeCode(String pluRow) {
		return getField(pluRow, "prizeCode");
	}

	public String getZeroPriceBehavior(String pluRow) {
		return getField(pluRow, "zeroPriceBehavior");
	}

	public String getMembersOnly(String pluRow) {
		return getField(pluRow, "membersOnly");
	}

	public String getAutomDiscountType(String pluRow) {
		return getField(pluRow, "automDiscountType");
	}

	public String getUpbOperationType(String pluRow) {
		return getField(pluRow, "upbOperationType");
	}

	public String getUpbProviderId(String pluRow) {
		return getField(pluRow, "upbProviderId");
	}

	public String getFiller2(String pluRow) {
		return getField(pluRow, "filler2");
	}

	public String getPrice(String pluRow) {
		return getField(pluRow, "price");
	}

	private String getField(String pluRow, String fieldName) {
		String result="";
		try{
			ArsPluRecordField arsPluRecordField = pluRowFields.get(fieldName);
			result = pluRow.substring(arsPluRecordField.getOffset() - 1, arsPluRecordField.getOffset() + arsPluRecordField.getLength() - 1);
		}catch (Exception e){
			logger.debug("pluRow    = \"" + pluRow + "\"");
			logger.debug("fieldName = \"" + fieldName + "\"");
		}
		return result;

	}

	private Map<String, ArsPluRecordField> pluRowFields;

}
