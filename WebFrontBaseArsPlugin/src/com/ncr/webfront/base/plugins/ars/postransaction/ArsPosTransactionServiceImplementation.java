package com.ncr.webfront.base.plugins.ars.postransaction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.postransaction.PosTransactionSearchCriteria;
import com.ncr.webfront.base.postransaction.PosTransactionServiceInterface;
import com.ncr.webfront.base.postransaction.PosTransactionSummary;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.db.DbHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class ArsPosTransactionServiceImplementation implements PosTransactionServiceInterface {

	private final Logger logger = WebFrontLogger.getLogger(ArsPosTransactionServiceImplementation.class);
	private Map<String, List<String>> mapDateFiles = new HashMap<String, List<String>>();

	private final String CURRENT_DAY_JRN_FILENAME = "S_JRNXXX.DAT";
	private String currentDayJrnPath;

	private boolean isPreviousDayCompressed = false;
	private final String PREVIOUS_DAY_JRN_FILENAME = "HOCJRN";
	private final String COMPRESSED_PREVIOUS_DAY_JRN_FILENAME = "HOCJRN";
	private final String COMPRESSED_PREVIOUS_DAY_JRN_FILEEXT = ".gz";
	private String previousDayJrnPath;
	private String previousDayTempPath = null;

	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

	public ArsPosTransactionServiceImplementation() {
		logger.debug("BEGIN Constructor");

		currentDayJrnPath = PathManager.getInstance().buildNormalizedFullFilePath(ArsServerEnvParameters.getInstance().getServerHomeDir(), "data");
		previousDayJrnPath = PathManager.getInstance().buildNormalizedFullFilePath(ArsServerEnvParameters.getInstance().getServerHomeDir(), "safe");

		// Enable compressed journal folder only if compressedJournalFolder parameter present and folder exists
		String compressedJrnFolder = ArsMappingProperties.getInstance().getProperty(ArsMappingProperties.compressedJournalFolder);
		if (compressedJrnFolder != null) {
			File file = new File(compressedJrnFolder);
			if (file.exists() && file.isDirectory()) {
				isPreviousDayCompressed = true;
				previousDayJrnPath = compressedJrnFolder;
				previousDayTempPath = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + "temp";
				logger.debug("Using compressed journal folder");
			}
		}
		logger.debug("END   Constructor (isPreviousDayCompressed == " + isPreviousDayCompressed + ")");
	}

	@Override
	public ErrorObject checkService() {
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	@Override
	public List<Date> getAvailablePosTransactionDates() {
		logger.debug("BEGIN (isPreviousDayCompressed == " + isPreviousDayCompressed + ")");

		mapDateFiles = new HashMap<String, List<String>>();
		List<Date> posTransactionDatesFromFiles = new ArrayList<Date>();
		List<Date> posTransactionDatesFromDatabase = new ArrayList<Date>();
		if (isPreviousDayCompressed) {
			appendPreviousDayAvailableDatesCompressed(posTransactionDatesFromFiles, mapDateFiles);
		} else {
			appendPreviousDayAvailableDatesUncompressed(posTransactionDatesFromFiles, mapDateFiles);
		}
		appendCurrentDayAvailableDates(posTransactionDatesFromFiles, mapDateFiles);

		if (!isIdcTableEmpty()) {
			appendTransactionDatesFromDatabase(posTransactionDatesFromDatabase, "idc");
		}
		appendTransactionDatesFromDatabase(posTransactionDatesFromDatabase, "idc_eod");
		logger.info("posTransactionDatesFromFiles.size()   =" + posTransactionDatesFromFiles.size());
		logger.info("posTransactionDatesFromDatabase.size()=" + posTransactionDatesFromDatabase.size());
		dumpListDates("posTransactionDatesFromFiles", posTransactionDatesFromFiles);
		dumpListDates("posTransactionDatesFromDatabase", posTransactionDatesFromDatabase);
		dumpMapDates("mapDateFile", mapDateFiles);
		posTransactionDatesFromFiles.retainAll(posTransactionDatesFromDatabase);
		dumpListDates("posTransactionDatesFromFiles", posTransactionDatesFromFiles);
		logger.info("returned list size                    =" + posTransactionDatesFromFiles.size());
		logger.debug("END   (isPreviousDayCompressed == " + isPreviousDayCompressed + ")");
		return posTransactionDatesFromFiles;
	}

	private void dumpMapDates(String string, Map<String, List<String>> mapDateFile) {
		logger.debug("BEGIN - " + string);
		for (String key : mapDateFile.keySet()) {
			logger.debug("   key:" + key + "; value =" + mapDateFile.get(key));
		}
		logger.debug("END   - " + string);
	}

	private void dumpListDates(String string, List<Date> posTransactionDates) {
		logger.debug("BEGIN - " + string);
		for (Date date : posTransactionDates) {
			logger.debug("   DATE:" + date);
		}
		logger.debug("END   - " + string);
	}

	private void appendTransactionDatesFromDatabase(List<Date> posTransactionDatesFromDatabase, String table) {
		logger.info("BEGIN");

		Connection connection = null;
		Statement statement = null;
		try {

			String query;
			ResultSet rs;

			connection = DbHelper.getConnection();
			statement = connection.createStatement();

			query = "select distinct  ddate from " + table + " where recordtype = 'F' and recordcode in ('100','200','800')";
			// set the query conditions according to
			// posTransactionSearchCriteria
			rs = statement.executeQuery(query);
			while (rs.next()) {
				logger.debug("rs.getDate(1) = \""+ rs.getDate(1) +"\"");
				String transactionDate = dateFormatter.format(rs.getDate(1));
				logger.debug("transactionDate = \""+ transactionDate +"\"");
				posTransactionDatesFromDatabase.add(dateFormatter.parse(transactionDate));
			}
		} catch (Exception e) {
			logger.error("Error: ", e);
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				logger.error("Finally error: ", e);
			}
		}

		logger.info("END");
	}

	private void appendPreviousDayAvailableDatesCompressed(List<Date> availablePosTransactionDates, Map<String, List<String>> mapDateFile) {
		for (File safeDirFile : new File(previousDayJrnPath).listFiles()) {
			// check if the file is a journal
			if (safeDirFile.getName().startsWith(COMPRESSED_PREVIOUS_DAY_JRN_FILENAME) && safeDirFile.getName().endsWith(COMPRESSED_PREVIOUS_DAY_JRN_FILEEXT)) {
				String dateString;
				String[] split = safeDirFile.getName().split("\\.");
				split = split[1].split("-");
				dateString = split[0] + "-" + split[1] + "-" + split[2];
				logger.debug("dateString = \"" + dateString+"\"");
				List<String> journalFilenames;
				if (mapDateFile.containsKey(dateString)) {
					journalFilenames = mapDateFile.get(dateString);
					journalFilenames.add(safeDirFile.getName());
				} else {
					journalFilenames = new ArrayList<String>();
					journalFilenames.add(safeDirFile.getName());
					try {
						availablePosTransactionDates.add(dateFormatter.parse(dateString));
					} catch (ParseException e) {
						logger.error("Error: ", e);
					}
				}
				mapDateFile.put(dateString, journalFilenames);
			}
		}
	}

	private void appendPreviousDayAvailableDatesUncompressed(List<Date> availablePosTransactionDates, Map<String, List<String>> mapDateFile) {
		for (File filePreviousDayJrn : new File(previousDayJrnPath).listFiles()) {
			// check if the file is a journal
			if (filePreviousDayJrn.getName().startsWith(PREVIOUS_DAY_JRN_FILENAME)) {
				String lastModifiedDateRecordFormatted = dateFormatter.format(new Date(filePreviousDayJrn.lastModified()));
				List<String> journalFilenames;
				if (mapDateFile.containsKey(lastModifiedDateRecordFormatted)) {
					journalFilenames = mapDateFile.get(lastModifiedDateRecordFormatted);
					journalFilenames.add(filePreviousDayJrn.getName());
				} else {
					journalFilenames = new ArrayList<String>();
					journalFilenames.add(filePreviousDayJrn.getName());

					try {
						Date date = new Date(filePreviousDayJrn.lastModified());
						String dateString = dateFormatter.format(date);
						date = dateFormatter.parse(dateString);
						availablePosTransactionDates.add(date);
					} catch (Exception e) {
						logger.error("Exception!", e);
					}

				}
				mapDateFile.put(lastModifiedDateRecordFormatted, journalFilenames);
			}
		}
	}

	private void appendCurrentDayAvailableDates(List<Date> availablePosTransactionDates, Map<String, List<String>> mapDateFiles) {
		// se oggi ho fatto transazioni, aggiungere i files S_JRN
		if (!isIdcTableEmpty()) {
			List<String> idcDateList = retrieveIdcDates();
			List<String> journalFilenames;
			for (String idcDate : idcDateList) {
				if (mapDateFiles.containsKey(idcDate)) {
					journalFilenames = mapDateFiles.get(idcDate);
					journalFilenames.add(CURRENT_DAY_JRN_FILENAME);
				} else {
					journalFilenames = new ArrayList<String>();
					journalFilenames.add(CURRENT_DAY_JRN_FILENAME);
					try {
						availablePosTransactionDates.add(dateFormatter.parse(idcDate));
					} catch (ParseException e) {
						logger.error("Error: ", e);
					}
				}
				mapDateFiles.put(idcDate, journalFilenames);
			}
		}

		logger.debug("mapDateFile: " + mapDateFiles.toString());

	}

	private List<String> retrieveIdcDates() {
		List<String> idcDates = new ArrayList<String>();
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DbHelper.getConnection();
			statement = connection.createStatement();
			String query = "SELECT distinct ddate FROM idc;";
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				idcDates.add(rs.getString(1));
			}
		} catch (Exception e) {
			logger.error("Error: ", e);
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				logger.error("Finally error: ", e);
			}
		}
		return idcDates;
	}

	@Override
	public List<String> getPosTransactionJournal(PosTransactionSummary posTransactionSummary) {
		logger.debug("BEGIN (isPreviousDayCompressed == " + isPreviousDayCompressed + ")");

		if (posTransactionSummary == null) {
			logger.debug("END   (posTransactionSummary == null)");
			return new ArrayList<String>();
		}
		logger.debug("posTransactionSummary.getTransactionDate()   = " + posTransactionSummary.getTransactionDate());
		logger.debug("posTransactionSummary.getOperatorCode()      = " + posTransactionSummary.getOperatorCode());
		logger.debug("posTransactionSummary.getTerminalCode()      = " + posTransactionSummary.getTerminalCode());
		logger.debug("posTransactionSummary.getTransactionNumber() = " + posTransactionSummary.getTransactionNumber());

		List<String> listJournalFileNames = new ArrayList<String>();

		if (isPreviousDayCompressed) {
			listJournalFileNames = mapDateFiles.get(dateFormatter.format(posTransactionSummary.getTransactionDate()));
			logger.debug("recordDateFormat: " + dateFormatter.format(posTransactionSummary.getTransactionDate()));
			logger.debug("listJournalFileNames: " + listJournalFileNames);
			if (listJournalFileNames != null && listJournalFileNames.get(0).endsWith(".gz")) {
				String comp = previousDayJrnPath + File.separator + listJournalFileNames.get(0);
				String uncomp = previousDayTempPath + File.separator + listJournalFileNames.get(0).substring(0, listJournalFileNames.get(0).length() - 3);
				logger.debug("comp: " + comp);
				logger.debug("uncomp: " + uncomp);
				boolean exists = new File(uncomp).exists();
				if (!exists) {
					File f = new File(previousDayTempPath);
					// rimuovo il file unzip sotto tmp se esiste
					if (f.exists() && f.isDirectory()) {
						if (f.listFiles() != null) {
							for (File safeDirFile : f.listFiles()) {
								if (safeDirFile.getName().startsWith("HOCJRN"))
									safeDirFile.delete();
							}
						}
					} else
						f.mkdirs();

					byte[] b = new byte[1024];
					try {
						FileInputStream fileIn = new FileInputStream(comp);
						GZIPInputStream gZIPInputStream = new GZIPInputStream(fileIn);
						FileOutputStream fileOutputStream = new FileOutputStream(uncomp);

						int bytes_read;
						while ((bytes_read = gZIPInputStream.read(b)) > 0) {
							fileOutputStream.write(b, 0, bytes_read);
						}
						// System.out.println("bytes: " + bytes_read);
						gZIPInputStream.close();
						fileIn.close();
						fileOutputStream.close();

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			listJournalFileNames = mapDateFiles.get(dateFormatter.format(posTransactionSummary.getTransactionDate()));
		}

		logger.debug("listJournalFileNames: " + listJournalFileNames);

		List<String> posTransactionJournal = new ArrayList<String>();
		List<String> posTransactionJournalNext = new ArrayList<String>();
		try {
			boolean isTransactionNumberPresent = false;

			String lineToCheckBackwards = "";
			outer: for (String journalFileName : listJournalFileNames) {
				File journalFile = null;

				if (journalFileName.equals(CURRENT_DAY_JRN_FILENAME)) {
					journalFile = new File(PathManager.getInstance().buildNormalizedFullFilePath(currentDayJrnPath,
							buildJrnlFileName(posTransactionSummary.getTerminalCode())));
				} else {
					if (isPreviousDayCompressed) {
						journalFile = new File(PathManager.getInstance().buildNormalizedFullFilePath(previousDayTempPath,
								journalFileName.substring(0, journalFileName.length() - 3)));
					} else {
						journalFile = new File(PathManager.getInstance().buildNormalizedFullFilePath(previousDayJrnPath, journalFileName));
					}
				}
				logger.debug("buildJrnlFileName(posTransactionSummary.getTerminalCode() = " + buildJrnlFileName(posTransactionSummary.getTerminalCode()));
				logger.debug("journalFilePath: " + journalFile.getPath());
				Scanner journalScanner = new Scanner(journalFile);

				while (journalScanner.hasNext()) {
					String nextJournalLine = journalScanner.nextLine();

					// search for pattern (*transactionNumber
					// ----/terminalCode/operatorCode) in the journal files
					// associated to
					// the summary transactionDate
					if (isTransactionNumberPresent) {
						posTransactionJournalNext.add(nextJournalLine);
						// continuo a leggere per cercare possibili voucher
						if (nextJournalLine.startsWith("*")
								&& nextJournalLine.substring(10, 18).equals(
										"/" + formatCode(posTransactionSummary.getTerminalCode()) + "/" + formatCode(posTransactionSummary.getOperatorCode()))) {
							if (nextJournalLine.substring(1, 5).equals(formatTransactionNumber(posTransactionSummary.getTransactionNumber()))) {

								posTransactionJournal.addAll(posTransactionJournalNext);
								posTransactionJournalNext.clear();
							} else {
								posTransactionJournal.add(posTransactionJournalNext.get(0));
								journalScanner.close();
								break outer;
							}
						}

					} else {
						posTransactionJournal.add(nextJournalLine);

						if (nextJournalLine.startsWith("*" + formatTransactionNumber(posTransactionSummary.getTransactionNumber()))
								&& nextJournalLine.substring(10, 18).equals(
										"/" + formatCode(posTransactionSummary.getTerminalCode()) + "/" + formatCode(posTransactionSummary.getOperatorCode()))) {

							lineToCheckBackwards = nextJournalLine.substring(6, 18); // used
																						// to
																						// find
																						// the
																						// last
																						// transaction
																						// (pattern:
																						// *----
																						// storeCode/terminalCode/operatorCode)
							logger.debug("lineToCheckBackwards=" + lineToCheckBackwards);
							logger.debug("nextJournalLine.substring(10, 17) = " + nextJournalLine.substring(10, 18));
							logger.debug("equals = " + "/" + formatCode(posTransactionSummary.getTerminalCode()) + "/"
									+ formatCode(posTransactionSummary.getOperatorCode()));
							// add the line after *transactionNumber
							// ----/terminalCode/storeCode
							// posTransactionJournal.add(journalScanner.nextLine());
							isTransactionNumberPresent = true;
							// journalScanner.close();
							logger.debug("Transaction: " + posTransactionSummary.getTransactionNumber() + " found in " + journalFileName);
						}
					}
				}
				journalScanner.close();
			}

			if (!isTransactionNumberPresent) {
				return new ArrayList<String>();
			}

			// get the iterator from the end except the last two lines (where
			// the current transaction number is stored)
			ListIterator<String> journalIterator = posTransactionJournal.listIterator(posTransactionJournal.size() - 2);
			// look for the last transaction
			while (journalIterator.hasPrevious()) {
				String journalLine = journalIterator.previous();
				// look for pattern
				if (journalLine.startsWith("*") && journalLine.substring(6, 18).equals(lineToCheckBackwards)
						&& !journalLine.substring(1, 5).equals(formatTransactionNumber(posTransactionSummary.getTransactionNumber()))) {

					// two lines after *transactionNumber, will be removed
					journalIterator.next();
					journalIterator.next();

					// remove all lines before the target journal
					while (journalIterator.hasPrevious()) {
						journalIterator.previous();
						journalIterator.remove();
					}
				}
			}
		} catch (IOException e) {
			logger.error("Error:", e);
		}

		logger.debug("END   (isPreviousDayCompressed == " + isPreviousDayCompressed + ")");
		return posTransactionJournal;
	}

	private String buildJrnlFileName(String terminalCode) {
		return CURRENT_DAY_JRN_FILENAME.replace("XXX", formatCode(terminalCode));
	}

	@Override
	public List<PosTransactionSummary> searchTransactions(PosTransactionSearchCriteria posTransactionSearchCriteria) {

		List<PosTransactionSummary> posTransactionSummaries = new ArrayList<PosTransactionSummary>();

		if (isToday(posTransactionSearchCriteria.getTransactionDate()) && !isIdcTableEmpty()) {
			posTransactionSummaries = searchTransactionsQuery(posTransactionSummaries, "idc", posTransactionSearchCriteria);

		}
		posTransactionSummaries = searchTransactionsQuery(posTransactionSummaries, "idc_eod", posTransactionSearchCriteria);

		return posTransactionSummaries;
	}

	private boolean isToday(Date date) {
		Date today = new Date();

		// if the selected date is today
		if (dateFormatter.format(date).equals(dateFormatter.format(today))) {
			return true;

		}
		return false;
	}

	private List<PosTransactionSummary> searchTransactionsQuery(List<PosTransactionSummary> posTransactionSummaries, String table,
			PosTransactionSearchCriteria posTransactionSearchCriteria) {
		logger.info("BEGIN");
		SimpleDateFormat summaryDateFormat = new SimpleDateFormat("yyyy-MM-dd HHmmss");

		logger.debug("posTransactionSearchCriteria: transaction date " + posTransactionSearchCriteria.getTransactionDate() + " operatorCode "
				+ posTransactionSearchCriteria.getOperatorCode() + " terminalCode " + posTransactionSearchCriteria.getTerminalCode() + " transaction number "
				+ posTransactionSearchCriteria.getTransactionNumber());

		Connection connection = null;
		Statement statement = null;
		try {

			String query;
			ResultSet rs;

			connection = DbHelper.getConnection();
			statement = connection.createStatement();

			query = "select distinct reg, ddate, ttime, trans, userno, data from " + table + " where recordtype = 'F' and recordcode in ('100','200','800')";

			// set the query conditions according to
			// posTransactionSearchCriteria
			if (posTransactionSearchCriteria.getTransactionDate() != null) {
				query += " and ddate = '" + dateFormatter.format(posTransactionSearchCriteria.getTransactionDate()) + "'";
			}
			if (!posTransactionSearchCriteria.getTerminalCode().isEmpty() && posTransactionSearchCriteria.getTerminalCode() != null) {
				query += " and reg = " + posTransactionSearchCriteria.getTerminalCode();
			}
			if (!posTransactionSearchCriteria.getOperatorCode().isEmpty() && posTransactionSearchCriteria.getOperatorCode() != null) {
				query += " and userno = " + posTransactionSearchCriteria.getOperatorCode();
			}
			if (!posTransactionSearchCriteria.getTransactionNumber().isEmpty() && posTransactionSearchCriteria.getTransactionNumber() != null) {
				query += " and trans = " + posTransactionSearchCriteria.getTransactionNumber();
			}

			rs = statement.executeQuery(query);
			while (rs.next()) {
				String transactionDate = dateFormatter.format(rs.getDate(2)) + " " + rs.getString(3);
				String operatorCode = String.valueOf(rs.getInt(5));
				String terminalCode = String.valueOf(rs.getInt(1));
				String transactionNumber = String.valueOf(rs.getInt(4));
				int transactionTotal = getCentsFromDataField(rs.getString(6));

				PosTransactionSummary posTransactionSummary = new PosTransactionSummary(summaryDateFormat.parse(transactionDate), operatorCode, terminalCode,
						transactionNumber, transactionTotal);

				posTransactionSummaries.add(posTransactionSummary);
			}
		} catch (Exception e) {
			logger.error("Error: ", e);
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				logger.error("Finally error: ", e);
			}
		}

		logger.info("END");
		return posTransactionSummaries;
	}

	private boolean isIdcTableEmpty() {
		boolean isIdcTableEmpty = false;
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DbHelper.getConnection();
			statement = connection.createStatement();
			String query = "select count(*) from idc";
			ResultSet rs = statement.executeQuery(query);
			if (rs.next()) {
				isIdcTableEmpty = rs.getInt(1) > 0 ? false : true;
			}
		} catch (Exception e) {
			logger.error("Error: ", e);
		} finally {
			try {
				connection.close();
				statement.close();
			} catch (Exception e) {
				logger.error("Finally error: ", e);
			}
		}
		return isIdcTableEmpty;
	}

	private int getCentsFromDataField(String dataField) {
		if (dataField.isEmpty() || dataField == null) {
			return 0;
		}

		String[] dataFieldSplitted = dataField.split("\\+");
		if (dataFieldSplitted.length < 3) {
			return 0;
		}

		return Integer.parseInt(dataFieldSplitted[2]);
	}

	private String formatCode(String code) {
		if (code.length() == 1) {
			return "00" + code;
		}
		if (code.length() == 2) {
			return "0" + code;
		}
		return code;
	}

	private String formatTransactionNumber(String transactionNumber) {
		if (transactionNumber.length() == 1) {
			return "000" + transactionNumber;
		}
		if (transactionNumber.length() == 2) {
			return "00" + transactionNumber;
		}
		if (transactionNumber.length() == 3) {
			return "0" + transactionNumber;
		}
		return transactionNumber;
	}

}
