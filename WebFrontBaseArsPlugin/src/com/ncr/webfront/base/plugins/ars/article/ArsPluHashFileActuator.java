package com.ncr.webfront.base.plugins.ars.article;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ArsPluHashFileActuator implements ArsPluHashFileActuatorInterface {

	public ArsPluHashFileActuator(ArsPluRowTemplate arsPluRowTemplate, String pluHashFilePathAndName) {
		this.arsPluRowTemplate = arsPluRowTemplate;
		logger.debug("Begin ArsPluHashFileActuator Constructor");
		this.pluHashFilePathAndName = pluHashFilePathAndName;
		logger.debug("End ArsPluHashFileActuator Constructor");
	}

	@Override
	/*
	 * Get just one line
	 * 
	 * @see
	 * com.ncr.webfront.base.plugins.ars.article.ArsPluHashFileActuatorInterface
	 * #searchByArticleCode(java.lang.String)
	 */
	public List<String> searchByArticleCode(String articleCode) {
		logger.debug("Begin  searchByArticleCode " + articleCode);
		List<String> pluHashFileLines = new ArrayList<String>();

		String foundArticle = findArticle(articleCode);
		if (foundArticle.trim().equals("")) {
			logger.info(articleCode + " not found in " + pluHashFilePathAndName);
		} else {
			logger.info(articleCode + " found in " + pluHashFilePathAndName);
		}
		pluHashFileLines.add(foundArticle);
		logger.debug("End  searchByArticleCode " + articleCode);
		return pluHashFileLines;
	}

	/**
	 * not used yet
	 * 
	 * @param articleCode
	 * @return
	 */
	public List<String> searchByStartsWithArticleCode(String articleCode) {
		logger.debug("Begin searchByStartsWithArticleCode: " + articleCode);
		List<String> pluHashFileLines = new ArrayList<String>();
		try {
			File filePlu = new File(pluHashFilePathAndName);
			BufferedReader bufPlu = new BufferedReader(new FileReader(filePlu));
			String line;

			articleCode = articleCode.trim();
			if (articleCode.length() >= 1) {
				while ((line = bufPlu.readLine()) != null) {

					String cod = arsPluRowTemplate.getArticleCode(line).trim();
					if (cod.startsWith(articleCode)) {
						pluHashFileLines.add(line);
					}
				}
			}
			bufPlu.close();

		} catch (FileNotFoundException fileNotFoundException) {
			// hash file not found
			logger.error("Hash file not found", fileNotFoundException);

		} catch (IOException iOException) {
			// io exception
			logger.error("IO Exception", iOException);
		}
		logger.debug("End searchByStartsWithArticleCode: " + articleCode);
		return pluHashFileLines;
	}

	@Override
	public List<String> searchByDescription(String description, int maxSearchRowsNumber) {
		logger.debug("Begin  searchByDescription: " + description);
		List<String> pluHashFileLines = new ArrayList<String>();
		try {
			File filePlu = new File(pluHashFilePathAndName);
			BufferedReader bufPlu = new BufferedReader(new FileReader(filePlu));
			String line;
			int counter = 0;

			description = description.trim().toUpperCase();
			if (description.length() >= 1) {
				while ((line = bufPlu.readLine()) != null && counter < maxSearchRowsNumber) {
					if (line.length() >= 78) {
						String descInFile = arsPluRowTemplate.getDescription(line).trim().toUpperCase();
						if (descInFile.indexOf(description) >= 0) {
							counter++;
							pluHashFileLines.add(line);
						}
					}else{
						logger.error("wrong line length:" + line.length() + "");
						logger.error("line = \"" + line + "\"");
					}
				}
			} else {
				while ((line = bufPlu.readLine()) != null && counter < maxSearchRowsNumber) {
					if (!(line.trim().equals("") || line.charAt(0) == '-')) {
						counter++;
						pluHashFileLines.add(line);
					}
				}
			}
			bufPlu.close();

		} catch (FileNotFoundException fileNotFoundException) {
			logger.error("File Not Found", fileNotFoundException);
		} catch (IOException iOException) {
			logger.error("Error IO", iOException);
		}
		logger.debug("End searchByDescription: " + description);
		return pluHashFileLines;
	}

	@Override
	public ErrorObject deleteArticle(String articleCode) {
		logger.debug("Begin  deleteArticle " + articleCode);
		File filePlu = new File(pluHashFilePathAndName);
		long ptr = hashFind(articleCode, filePlu);

		// error searching articleCode in hsh file
		if (ptr < 0) {
			logger.error("Error during article search " + articleCode);
			return new ErrorObject(ErrorCode.GENERIC_ERROR, "Error during article search " + articleCode);
		}

		if (!lastHashFound) {
			logger.error("Errore: articolo " + articleCode + " non trovato in Hash File");
			return new ErrorObject(ErrorCode.BUSINESS_LOGIC_ERROR, "Error during article search " + articleCode);
		}

		try {
			// use the file pointer to delete row in hash file
			logger.trace("use the file pointer to delete row in hash file pointer =" + ptr);
			RandomAccessFile file = new RandomAccessFile(filePlu, "rwd");
			file.seek(ptr);

			for (int i = 0; i < 78; i++) {
				file.writeByte(' ');
			}
			file.close();

		} catch (IOException ioe) {
			logger.error("I/O Exception", ioe);
			return new ErrorObject(ErrorCode.GENERIC_ERROR, ioe.getLocalizedMessage());
		}

		// article successfully deleted
		logger.trace("Article successfully deleted");
		return new ErrorObject(ErrorCode.OK_NO_ERROR, "Article successfully deleted");
	}

	/**
	 * 
	 * @param key
	 * @param file
	 *            plu
	 * @return pointer to the file where record going to -
	 * 
	 *         return -2 if hashfile is full
	 */
	private long hashFind(String key, File ffile) {
		logger.debug("Entering hashFind " + key);

		int recno;
		int fixSize = 16;
		RandomAccessFile file = null;

		lastHashFound = false;

		try {

			file = new RandomAccessFile(ffile, "r");

			int top = 0, del = 0, end = (int) (ffile.length() / 80);

			String pb;
			if (end < 8) {
				logger.error("Errore dimensione file");
				file.close();
				return -4;
			}

			key = "                " + key;
			key = key.substring(key.length() - 16);
			int ind = fixSize + 5 & ~7, val = key.charAt(1) & 0x0f;
			String s = key;

			while (val-- > 0) {
				s = s.substring(1) + s.charAt(0);
			}
			for (char tmp[] = new char[8]; ind-- > 0;) {
				val = s.charAt(ind) & 0x0f;
				tmp[ind & 7] = (char) (val - (val > 9 ? 6 : 0) + '0');
				if ((ind & 7) > 0) {
					continue;
				}
				top += Integer.parseInt(new String(tmp));
			}
			top %= end >> 3;
			recno = top <<= 3;

			file.seek(recno * 80);

			while ((pb = file.readLine()) != null) {
				recno++;
				char c = pb.charAt(fixSize - 1);

				if (c < '0') {
					if (del == 0) {
						del = recno;
					}
					if (c == ' ') {
						break;
					}
				} else {
					if (pb.startsWith(key)) {
						long ret = file.getFilePointer() - 80;
						logger.debug("Articolo presente - ptr=" + ret);
						lastHashFound = true;
						file.close();
						return ret;
					}
				}
				if (recno == end) {
					recno = 0;
					file.seek(0);
					break;
				}

				if (recno == top) {
					break;
				}
			}
			if ((recno = del) > 0) {
				long ret = file.getFilePointer() - 80;
				logger.debug("Articolo non presente - empty ptr=" + ret);
				file.close();
				return ret;
			}

			file.close();
			logger.debug("File Hash Pieno");
			return -2;
		} catch (Exception e) {

			try {
				if (file != null)
					file.close();
			} catch (Exception ee) {

			}
		}
		logger.error("Errore I/O");
		return -3;
	}

	@Override
	public ErrorObject addArticle(String articleCode, String linePlu) {
		logger.debug("Begin addArticle");
		// search article in hsh file
		File filePlu = new File(pluHashFilePathAndName);
		long ptr = hashFind(articleCode, filePlu);

		if (ptr == -2) {
			logger.error("Hash File Pieno");
			return new ErrorObject(ErrorCode.GENERIC_ERROR, "Space not available, Hash File Full");
		}

		if (ptr < 0) {
			logger.error("Errore durante ricerca in Hash File");
			return new ErrorObject(ErrorCode.GENERIC_ERROR, "Error during search in Hash File");
		}

		if (lastHashFound) {
			logger.error("Articolo " + articleCode + " già esistente");
			return new ErrorObject(ErrorCode.BUSINESS_LOGIC_ERROR, "Article " + articleCode + " already existing");
		}

		try {
			RandomAccessFile file = new RandomAccessFile(filePlu, "rwd");
			file.seek(ptr);
			file.writeBytes(linePlu);
			file.close();

		} catch (FileNotFoundException fne) {
			logger.error("Errore durante scrittura, Hash File non trovato");
			return new ErrorObject(ErrorCode.GENERIC_ERROR, "Hash File not Found");
		} catch (IOException ioe) {
			logger.error("Errore durante scrittura, Errore I/O");
			return new ErrorObject(ErrorCode.GENERIC_ERROR, "System Error");
		}

		logger.debug("End addArticle");
		return new ErrorObject(ErrorCode.OK_NO_ERROR, "Article successfully inserted");

	}

	@Override
	public ErrorObject updateArticle(String articleCode, String linePlu) {
		logger.debug("Begin updateArticle " + articleCode);
		File filePlu = new File(pluHashFilePathAndName);
		long ptr = hashFind(articleCode, filePlu);

		if (ptr < 0) {
			logger.error("Errore durante ricerca in Hash File");
			return new ErrorObject(ErrorCode.GENERIC_ERROR, "Error during search in Hash File");
		}

		if (lastHashFound) {
			try {
				RandomAccessFile file = new RandomAccessFile(filePlu, "rwd");
				file.seek(ptr);
				file.writeBytes(linePlu);
				file.close();

			} catch (FileNotFoundException fne) {
				logger.error("Errore durante scrittura, Hash File non trovato");
				return new ErrorObject(ErrorCode.GENERIC_ERROR, "Hash File not Found");
			} catch (IOException ioe) {
				logger.error("Errore durante scrittura, Errore I/O");
				return new ErrorObject(ErrorCode.GENERIC_ERROR, "System Error");
			}

		}

		logger.debug("End updateArticle " + articleCode);
		return new ErrorObject(ErrorCode.OK_NO_ERROR, "Article successfully changed");
	}

	@Override
	public String findArticle(String articleCode) {
		File filePlu = new File(pluHashFilePathAndName);
		long ptr = hashFind(articleCode, filePlu);
		byte[] buff = new byte[78];
		try {
			RandomAccessFile file = new RandomAccessFile(filePlu, "rwd");
			file.seek(ptr);
			file.read(buff, 0, 78);
			file.close();
		} catch (IOException ioe) {
			logger.error("I/O Exception", ioe);
			return "";
		}
		return new String(buff);
	}

	private ArsPluRowTemplate arsPluRowTemplate;
	private String pluHashFilePathAndName;
	private boolean lastHashFound;
	private Logger logger = WebFrontLogger.getLogger(ArsPluHashFileActuator.class);

}
