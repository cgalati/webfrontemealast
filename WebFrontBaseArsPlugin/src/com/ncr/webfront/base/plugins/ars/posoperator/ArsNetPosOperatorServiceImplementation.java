package com.ncr.webfront.base.plugins.ars.posoperator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.plugins.ars.ArsCtlFileActuatorInterface;
import com.ncr.webfront.base.plugins.ars.ArsMCtlFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsSCtlFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;
import com.ncr.webfront.core.utils.validation.PosOperatorValidationData;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public class ArsNetPosOperatorServiceImplementation implements
PosOperatorServiceInterface {
	
	public ArsNetPosOperatorServiceImplementation() {
		logger.debug("Begin Constructor");

		arsPosOperatorServiceProperties = PropertiesLoader.loadOrCreatePropertiesFile(ArsMappingProperties.arsPluginPropertiesFileName,
			      ArsMappingProperties.getArsPropertiesMap());
		
		//For maintenance
		arsSCtlOrgFileActuator = new ArsSCtlFileActuator(ArsServerEnvParameters.getInstance().getServerHomeDir());
		//Only view
		String mCtlDatPath = ArsMappingProperties.getInstance().getSCtlPath();
		String serverNumber = ArsServerEnvParameters.getInstance().getServerNumber();
		arsMCtlDatFileActuator = new ArsMCtlFileActuator(mCtlDatPath, serverNumber);
		
		logger.debug("End Constructor");
		
		// build role map
		roleMap = new HashMap<Integer, String>();
		roleMap.put(0, "NORMAL");
		roleMap.put(1, "SUPERVISOR");
		roleMap.put(2, "TRAINING");
	}
	
	/*public ArsPosOperatorServiceImplementation(ArsSCtlFileActuatorInterface arsSCtlOrgFileActuator, ArsSCtlFileActuatorInterface arsSCtlDatFileActuator){
		this.arsSCtlOrgFileActuator = arsSCtlOrgFileActuator;
		this.arsSCtlDatFileActuator = arsSCtlDatFileActuator;
	}*/

	@Override
	public ErrorObject checkService() {

		try {
			List<PosOperator> tempPosOperatorList = getAll();
			
			if (tempPosOperatorList == null || tempPosOperatorList.size() ==0 ){
				logger.error("PosOperatorList is null or empty!");
				return new ErrorObject(ErrorCode.SETUP_ERROR, "The operators list is empty!");
			}
			
		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
	
	@Override
	public List<PosOperator> getAll() {
		logger.debug("Begin getAll");
		List<PosOperator> returnList = new ArrayList<PosOperator>();
		
		String operatorCode;
		String name;
		String secretNo;
		String personnelNo;
		String role;
		
		for(String sCtlLine : arsSCtlOrgFileActuator.readAll()) {
			String fields[] =  sCtlLine.split(":");
			operatorCode = fields[0].trim();
			name = fields[1].trim();
			personnelNo = fields[2].trim();
			secretNo = fields[4].trim();
			role = buildRole(operatorCode, personnelNo);
			
			returnList.add(new PosOperator(operatorCode, name, personnelNo, secretNo, role));
		}
		
		logger.debug("End getAll");
		return returnList;
	}

	/* Customization of roles rules definition
	 * Operator code in [801, 899] (and personnelNo != 0) => SUPERVISORE
	 * Operator code in [  1, 799]  and personnelNo != 0  => NORMALE
	 * Operator code in [  1, 799]  and personnelNo  = 0  => ADDESTRAMENTO
	 */
	private String buildRole(String operatorCode, String personnelNo) {
		if(Integer.parseInt(operatorCode) > 800 && Integer.parseInt(operatorCode) <=899) {
			return roleMap.get(1);
		} else if (Integer.parseInt(operatorCode) < 800) {
			if(Integer.parseInt(personnelNo) != 0) {
				return roleMap.get(0);
			} else {
				return roleMap.get(2);
			}
		}
		return roleMap.get(0);
	}

	@Override
	public PosOperator getByCode(String operatorCode) {
		String sCtlLine = arsSCtlOrgFileActuator.readLine(Integer.parseInt(operatorCode));
		
		if(!StringUtils.isEmpty(sCtlLine)) {
			String fields[] =  sCtlLine.split(":");
	
			String name = fields[1];
			String personnelNo = fields[2];
			String secretNo = fields[4];
			String role = buildRole(operatorCode, personnelNo);
		
			return new PosOperator(operatorCode, name, personnelNo, secretNo, role);
		}
		
		return null;
	}
	
	@Override
	public List<PosOperator> searchByCode(String code) {
		return searchByProperty(arsSCtlOrgFileActuator.searchByCode(code));
	}
	
	@Override
	public List<PosOperator> searchByName(String name) {
		if(name.equalsIgnoreCase("readOnly")) {
			return searchByProperty(arsMCtlDatFileActuator.searchByName(""));
		}
		return searchByProperty(arsSCtlOrgFileActuator.searchByName(name));
	}
	
	private List<PosOperator> searchByProperty(List<String> sCtlList) {
		List<PosOperator> returnList = new ArrayList<PosOperator>();
		
		String operatorCode;
		String name;
		String secretNo;
		String personnelNo;
		String role;
		
		for(String sCtlLine : sCtlList) {
			String fields[] =  sCtlLine.split(":");
			operatorCode = fields[0].trim();
			name = fields[1].trim();
			personnelNo = fields[2].trim();
			secretNo = fields[4].trim();
			role = buildRole(operatorCode, personnelNo);
			
			returnList.add(new PosOperator(operatorCode, name, personnelNo, secretNo, role));
		}
		
		return returnList;
	}

	@Override
	public PosOperator createDefaultCashier() {
		return createCashierFromRole(roleMap.get(0));
	}

	@Override
	public PosOperator createCashierFromRole(String role) {
		if(StringUtils.isEmpty(role)) {
			role = roleMap.get(0);
		}
		
		String operatorCode = getFirstFreeCode(role);
		String secretNo = "00";
		String personnelNo = role.equals(roleMap.get(2)) ? "0" : operatorCode;
		
		PosOperator posOperator = new PosOperator(operatorCode, "", personnelNo, secretNo, role);
		
		return posOperator;
	}
	
	private String getFirstFreeCode(String role) {
		List<Integer> codeFilteredList = new ArrayList<Integer>();
		for(PosOperator posOperator : getAll()) {
			if(posOperator.getRole().equals(role)) {
				codeFilteredList.add(Integer.parseInt(posOperator.getOperatorCode()));
			}
		}
				
		Collections.sort(codeFilteredList);
		Integer previousCode = codeFilteredList.get(0);
		Integer actualCode;
		for(int i = 1; i < codeFilteredList.size(); i++) {
			actualCode = codeFilteredList.get(i);
			if((previousCode + 1) < actualCode.intValue()) {
				actualCode = previousCode + 1;
				return StringUtils.leftPad(String.valueOf(actualCode), 4, "0");
			} else if((previousCode + 1) == actualCode.intValue()) {
				previousCode = actualCode;
			}
		}
		return StringUtils.leftPad(String.valueOf(previousCode + 1), 4, "0");
	}

	@Override
	public ErrorObject addPosOperator(PosOperator posOperatorToAdd) {
		logger.debug("AddPosOperator");
		
		int codeToAdd = Integer.parseInt(posOperatorToAdd.getOperatorCode());
		String oldSCtlLine = arsSCtlOrgFileActuator.readLine(codeToAdd);	
		if(!StringUtils.isEmpty(oldSCtlLine)) {
			return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Operator with code " + posOperatorToAdd.getOperatorCode() + " already existing");
		}
		
		String newSCtlLine = mountNewSCtlLineFromObj(posOperatorToAdd);

		if (arsSCtlOrgFileActuator.insertLine(newSCtlLine)){
			return new ErrorObject(ErrorCode.OK_NO_ERROR, "Operator successfully inserted");
		} else {
			return new ErrorObject(ErrorCode.GENERIC_ERROR,"Error during the insertion, check the file S_CTLXXX.ORG");
		}
	}

	@Override
	public ErrorObject deletePosOperator(String posOperatorCodeToDelete) {
		logger.debug("Begin deletePosOperator");

		if (!posOperatorCodeToDelete.trim().equals("")) {
			if (arsSCtlOrgFileActuator.deleteLine(posOperatorCodeToDelete)){
				return new ErrorObject(ErrorCode.OK_NO_ERROR, "Operator successfully deleted");
			} else {
				return new ErrorObject(ErrorCode.GENERIC_ERROR,"Error during the deletion, check the file S_CTLXXX.ORG");
			}
		}

		// posOperator code is empty
		logger.debug("End deletePosOperator");
		return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Operator code invalid");
	}

	@Override
	public ErrorObject updatePosOperator(PosOperator posOperatorToUpdate) {
		logger.debug("Entering updatePosOperator");
		String sCtlLine = mountSCtlLineFromObj(posOperatorToUpdate);
		
		if (sCtlLine.length() != 62){
			logger.error("Errore durante preparazione record S_CTL");
			return new ErrorObject(ErrorCode.BUSINESS_LOGIC_ERROR,"Error during the editing");
		}
		
		if (arsSCtlOrgFileActuator.writeLine(sCtlLine)){
			return new ErrorObject(ErrorCode.OK_NO_ERROR, "Operator successfully updated");
		} else {
			return new ErrorObject(ErrorCode.GENERIC_ERROR,"Error during the saving, check the file S_CTLXXX.ORG");
		}
	}
	
	private String mountSCtlLineFromObj(PosOperator posOperator){
		logger.debug("Begin mountSCtlLineFromObj");		
		int codeToChange = Integer.parseInt(posOperator.getOperatorCode());
		
		String oldSCtlLine = arsSCtlOrgFileActuator.readLine(codeToChange);	
		
		StringBuilder sCtlLine = new StringBuilder();
		sCtlLine.append(oldSCtlLine.substring(0, 5));
		sCtlLine.append(StringUtils.rightPad(posOperator.getName(), 20, ' '));
		sCtlLine.append(":");
		sCtlLine.append(StringUtils.leftPad(posOperator.getPersonnelNo(), 8, '0'));
		sCtlLine.append(oldSCtlLine.substring(34, 41));
		sCtlLine.append(posOperator.getSecretNo());
		sCtlLine.append(oldSCtlLine.substring(43));
		
		logger.debug("End mountSCtlLineFromObj");			
		return sCtlLine.toString();
	}
	
	private String mountNewSCtlLineFromObj(PosOperator posOperatorToAdd){
		logger.debug("Begin mountNewSCtlLineFromObj");		
		
		StringBuilder sCtlLine = new StringBuilder();
		sCtlLine.append(StringUtils.leftPad(posOperatorToAdd.getOperatorCode(), 4, '0'));
		sCtlLine.append(":");
		sCtlLine.append(StringUtils.rightPad(posOperatorToAdd.getName(), 20, ' '));
		sCtlLine.append(":");
		sCtlLine.append(StringUtils.leftPad(posOperatorToAdd.getPersonnelNo(), 8, '0'));
		sCtlLine.append(":0000:");
		sCtlLine.append(posOperatorToAdd.getSecretNo());
		sCtlLine.append(":00:000000:0000:0000");
		
		logger.debug("End mountNewSCtlLineFromObj");			
		return sCtlLine.toString();
	}
	
	@Override
	public List<PosOperatorValidationData> getPosOperatorValidationData() {
		List<PosOperatorValidationData> returnList = new ArrayList<>();
		WebFrontValidationData normal = new WebFrontValidationData("role").notBlank().is("NORMAL");
		WebFrontValidationData supervisor = new WebFrontValidationData("role").notBlank().is("SUPERVISOR");
		WebFrontValidationData trainee = new WebFrontValidationData("role").notBlank().is("TRAINING");
		
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("name").notBlank().length(1, 20));
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("personnelNo").notBlank().length(1, 8));
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("secretNo").notBlank().length(2, 2));
		
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("operatorCode").rangeForRole(1, 799, normal));
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("personnelNo").rangeForRole(1, normal));
		
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("operatorCode").rangeForRole(801, 899, supervisor));
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("personnelNo").rangeForRole(1, supervisor));
		
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("operatorCode").rangeForRole(1, 799, trainee));
		returnList.add((PosOperatorValidationData) new PosOperatorValidationData("personnelNo").valueForRole(0, trainee));
		
		return returnList;
	}
	
	@Override
	public Map<Integer, String> getPosOperatorRoles() {
		return roleMap;
	}
	
	@Override
	public String getPosOperatorTraineeRole() {
		return roleMap.get(2);
	}
	
	public Map<Integer, String> roleMap = null;
	
	private Properties arsPosOperatorServiceProperties;
	private ArsCtlFileActuatorInterface arsSCtlOrgFileActuator;
	private ArsCtlFileActuatorInterface arsMCtlDatFileActuator;
	
	private Logger logger = WebFrontLogger.getLogger(ArsPosOperatorServiceImplementation.class);
}
