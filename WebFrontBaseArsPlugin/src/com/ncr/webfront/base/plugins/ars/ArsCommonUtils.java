package com.ncr.webfront.base.plugins.ars;

import org.apache.log4j.Logger;

public class ArsCommonUtils {

	/**
	 * 
	 * @param valueToParse
	 * @param defaultValue
	 *            (used when error occurs during parsing )
	 * @return
	 */
	public static int parseIntDefault(String valueToParse, int defaultValue, Logger logger, String logFieldName) {
		try {
			int returnValue;
			returnValue = Integer.parseInt(valueToParse);
			return returnValue;
		} catch (NumberFormatException exc) {
			logger.warn("WARNING!!! Invalid valueToParse (\"" + valueToParse + "\" for field \""+logFieldName + "\". Default value will be returned (\""+defaultValue+"\")");
			return defaultValue;
		}
	}

	/**
	 * 
	 * @param valueToParse
	 * @param defaultValue
	 *            (used when error occurs during parsing )
	 * @return
	 */
	public static int parseIntDefault(String valueToParse, int defaultValue,
			int radix, Logger logger, String logFieldName) {
		try {
			int returnValue;
			returnValue = Integer.parseInt(valueToParse, radix);
			return returnValue;
		} catch (NumberFormatException exc) {
			logger.warn("WARNING!!! Invalid valueToParse (\"" + valueToParse + "\" for field \""+logFieldName + "\". Default value will be returned (\""+defaultValue+"\")");
			return defaultValue;
		}

	}

}
