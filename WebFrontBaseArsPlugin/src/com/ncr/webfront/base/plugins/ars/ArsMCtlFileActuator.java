package com.ncr.webfront.base.plugins.ars;

import java.util.List;

public class ArsMCtlFileActuator extends ArsCtlFileActuator {
	public ArsMCtlFileActuator(String sCtlFilePath) {
		super(sCtlFilePath, mCtlOrgFileName);
	}
	
	public ArsMCtlFileActuator(String sCtlFilePath, String serverNumber) {
		super(sCtlFilePath, serverNumber, mCtlDatFileName);
	}
	
	@Override
	public List<String> searchByName(String name) {
		return searchByName(name, true);
	}
	
	private static final String mCtlOrgFileName = "M_CTLXXX.ORG";
	private static final String mCtlDatFileName = "M_CTLnnn.DAT";
}
