package com.ncr.webfront.base.plugins.ars.article;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;

public interface ArsPluHashFileActuatorInterface {

	public List <String> searchByArticleCode(String articleCode);

	public List <String> searchByStartsWithArticleCode(String articleCode);

	public List<String> searchByDescription(String description, int maxSearchRowsNumber);

	public ErrorObject deleteArticle(String articleCode);

	public ErrorObject addArticle(String articleCode, String linePlu);
	
	public ErrorObject updateArticle(String articleCode, String linePlu);

	public String findArticle (String articleCode);
}
