package com.ncr.webfront.base.plugins.ars;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class ArsActionCodeMappingProperties {
	public static final String arsActionCodeDefaultKeyPrefix = "key.";
	public static final String arsActionCodeDefaultValuePrefix = "Action Code ";
	
	
	public static synchronized ArsActionCodeMappingProperties getInstance() {
		if (instance == null) {
			instance = new ArsActionCodeMappingProperties();
		}

		return instance;
	}

	private ArsActionCodeMappingProperties() {
		arsActionCode = PropertiesLoader.loadOrCreatePropertiesFile(ArsMappingProperties.arsPluginActionCodePropertiesFileName, getArsActionCodePropertiesMap());
	}

	private static Map<String, String> getArsActionCodePropertiesMap() {
		Map<String, String> arsActionCodePropertiesMap = new HashMap<String, String>();

		for (int i=0; i<100; i++){
			String keyNumber=StringUtils.leftPad(i+"", 2, '0');
			arsActionCodePropertiesMap.put(arsActionCodeDefaultKeyPrefix+keyNumber, arsActionCodeDefaultValuePrefix + keyNumber);
		}

		return arsActionCodePropertiesMap;
	}

	
	public String getProperty(String key) {
		return getProperty(key, false);
	}
	
	public String getProperty(String key, boolean prependWorkingDir) {
		String value = arsActionCode.getProperty(key);
		
		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}
		
		return value;
	}
	
	
	private Properties arsActionCode;
	private static ArsActionCodeMappingProperties instance;
}
