package com.ncr.webfront.base.plugins.ars;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class ArsServerEnvParameters {
	
	public static ArsServerEnvParameters getInstance(){
		if (instance == null){
			instance = new ArsServerEnvParameters();
		}
		return instance;
	}

	private ArsServerEnvParameters() {

		Properties properties = new Properties();
		String filePath = buildArsServerEnvPropertyFilePath();
		File propertiesFile = new File(filePath);

		logger.info("filePath = \"" + filePath + "\"");
		if (propertiesFile.exists()) {
			try {
				logger.info("File \"" + filePath + "\" exists.");
				FileInputStream fisProperties = new FileInputStream(propertiesFile);
				properties.load(fisProperties);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			logger.error("File \"" + filePath + "\"does not exist!");
		}

		serverHomeDir = properties.getProperty("WF_ARSSERVER_HOMEDIR");
		logger.info("serverHomeDir =\"" + serverHomeDir + "\"");
		if (serverHomeDir == null) {
			serverHomeDir = "";
		}
		serverHomeDir = serverHomeDir.trim();
		logger.info("serverHomeDir after nullcheck and trim = \"" + serverHomeDir + "\"");

		serverNumber = properties.getProperty("WF_ARSSERVER_SRV");
		logger.info("serverNumber =\"" + serverNumber + "\"");
		if (serverNumber == null) {
			serverNumber = "";
		}
		serverNumber = serverNumber.trim();
		logger.info("serverNumber after nullcheck and trim = \"" + serverNumber + "\"");

		supervisorNumberForServerEod = properties.getProperty("WF_ARSSERVER_SUP");
		logger.info("supervisorNumberForServerEod =\"" + supervisorNumberForServerEod + "\"");
		if (supervisorNumberForServerEod == null) {
			supervisorNumberForServerEod = "";
		}
		supervisorNumberForServerEod = supervisorNumberForServerEod.trim();
		logger.info("supervisorNumberForServerEod after nullcheck and trim = \"" + supervisorNumberForServerEod + "\"");

		clientNumberForServerEod = properties.getProperty("WF_ARSSERVER_CRTEOD");
		logger.info("clientNumberForServerEod =\"" + clientNumberForServerEod + "\"");
		if (clientNumberForServerEod == null) {
			clientNumberForServerEod = "";
		}
		clientNumberForServerEod = clientNumberForServerEod.trim();
		logger.info("clientNumberForServerEod after nullcheck and trim = \"" + clientNumberForServerEod + "\"");

		mntApplyHomeDir = properties.getProperty("WF_MNTAPPLY_HOMEDIR");
		logger.info("mntApplyHomeDir =\"" + mntApplyHomeDir + "\"");
		if (mntApplyHomeDir == null) {
			mntApplyHomeDir = "";
		}
		mntApplyHomeDir = mntApplyHomeDir.trim();
		logger.info("mntApplyHomeDir after nullcheck and trim = \"" + mntApplyHomeDir + "\"");

		mntApplyMntSourceDir = properties.getProperty("WF_MNTAPPLY_MNTSRCDIR");
		logger.info("mntApplyMntSourceDir =\"" + mntApplyMntSourceDir + "\"");
		if (mntApplyMntSourceDir == null) {
			mntApplyMntSourceDir = "";
		}
		mntApplyMntSourceDir = mntApplyMntSourceDir.trim();
		logger.info("mntApplyMntSourceDir after nullcheck and trim = \"" + mntApplyMntSourceDir + "\"");

		fourDigitsChecker = ("true".equals(properties.getProperty("WF_FOURDIGITS")));
		logger.info("fourDigitsChecker =\"" + fourDigitsChecker + "\"");
	}

	private String buildArsServerEnvPropertyFilePath() {
		String baseFilePath = PathManager.getInstance().getWebFrontConfigDirectory() + File.separatorChar + ARS_SERVER_ENV_PROPERTIES_BASEFILENAME;
		// Try windows
		if (SystemUtils.IS_OS_WINDOWS) {
			String filePath = baseFilePath + "_windows.properties";
			File file = new File(filePath);
			if (file.exists()) {
				logger.info("File \"" + filePath + "\" exists. Using this file.");
				return filePath;
			}
			filePath = baseFilePath + ".properties";
			logger.info("Using file \"" + filePath + "\".");
			return filePath;
		}
		String filePath = baseFilePath + "_linux.properties";
		File file = new File(filePath);
		if (file.exists()) {
			logger.info("File \"" + filePath + "\" exists. Using this file.");
			return filePath;
		}

		filePath = baseFilePath + ".properties";
		logger.info("Using file \"" + filePath + "\".");
		return filePath;

	}

	public String getServerHomeDir() {
		return serverHomeDir;
	}

	public String getMntApplyHomeDir() {
		return mntApplyHomeDir;
	}

	public String getMntApplyMntSourceDir() {
		return mntApplyMntSourceDir;
	}

	public String getClientNumberForServerEod() {
		return clientNumberForServerEod;
	}

	public String getServerNumber() {
		return serverNumber;
	}

	public String getSupervisorNumberForServerEod() {
		return supervisorNumberForServerEod;
	}

	public boolean isForDigitsChecker() {
		return fourDigitsChecker;
	}
	
	private Logger logger = WebFrontLogger.getLogger(ArsServerEnvParameters.class);
	private final String ARS_SERVER_ENV_PROPERTIES_BASEFILENAME = "arsserverenv";
	private static ArsServerEnvParameters instance;
	private String serverHomeDir;
	private String serverNumber;
	private String supervisorNumberForServerEod;
	private String clientNumberForServerEod;
	private String mntApplyHomeDir;
	private String mntApplyMntSourceDir;
	private boolean fourDigitsChecker;
}
