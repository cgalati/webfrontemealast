package com.ncr.webfront.base.plugins.ars;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class ArsMappingProperties {

	public static final String arsPluginPropertiesFileName = "ars.properties";
	public static final String arsPluginActionCodePropertiesFileName = "arsactioncode.properties";

	public static final String arsDefaultPluLine = "default.plu.line";
	public static final String arsDefaultPluLineDefaultValue = "   99999999999990001001300PZ00100000ARTICOLO DEFAULT    0003          00000100";

	public static final String arsUpdateOnHashFile = "update.on.hashfile";
	public static final String arsUpdateOnHashFileDefaultValue = "false";

	public static final String arsMaximumSearchRowsNumber = "maximum.search.rows.number";
	public static final String arsMaximumSearchRowsNumberDefaultValue = "1000";

	public static final String arsEodServerAddress = "eod.server.address";
	public static final String arsEodServerAddressDefaultValue = "127.0.0.1";

	public static final String arsEodServerPort = "eod.server.port";
	public static final String arsEodServerPortDefaultValue = "8888";

	public static final String arsPosDescriptionData = "pos.description.data";
	public static final String arsPosDescriptionDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator
			+ "posDescriptionData.json";

	public static final String arsPosTerminalTypeData = "pos.terminal.type.data";
	public static final String arsPosTerminalTypeDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator
			+ "posTerminalTypeData.json";

	public static final String arsPosStatusData = "pos.status.data";
	public static final String arsPosStatusDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator + "posStatusData.json";

	public static final String arsDefectiveDeclarableData = "defective.declarable.data";
	public static final String arsDefectiveDeclarableDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator
			+ "defectiveDeclarableData.json";

	public static final String arsReadyPosEodData = "ready.pos.eod.data";
	public static final String arsReadyPosEodDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator + "readyPosEodData.json";

	public static final String arsReadyServerEodData = "ready.server.eod.data";
	public static final String arsReadyServerEodDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator
			+ "readyServerEodData.json";

	public static final String arsPosStatusDescriptionData = "pos.status.description.data";
	public static final String arsPosStatusDescriptionDataDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator
			+ "posStatusDescriptionData.json";

	public static final String arsVatDefaultId = "vat.default.id";
	public static final String arsVatDefaultIdDefaultValue = "705";

	public static final String arsExchangeRateSwitch = "exchange.rates.switch";
	public static final String arsExchangeRateSwitchDefaultValue = "false";

	public static final String arsInvokeMntApplyCommand = "invoke.mntapply.command";
	public static final String arsInvokeMntApplyCommandDefaultValue = "";

	public static final String arsPosCommand = "ars.pos.command";

	public static final String compressedJournalFolder = "postransactionservice.compressed.journal.file.folder";
	public static final String compressedJournalFolderDefaultValue = PathManager.getInstance().getWebFrontDataDirectory() + File.separator + "backup";

	public static synchronized ArsMappingProperties getInstance() {
		if (instance == null) {
			instance = new ArsMappingProperties();
		}

		return instance;
	}

	private ArsMappingProperties() {
		ars = PropertiesLoader.loadOrCreatePropertiesFile(arsPluginPropertiesFileName, getArsPropertiesMap());
	}

	public static Map<String, String> getArsPropertiesMap() {
		Map<String, String> arsPropertiesMap = new HashMap<String, String>();

		arsPropertiesMap.put(arsDefaultPluLine, arsDefaultPluLineDefaultValue);
		arsPropertiesMap.put(arsUpdateOnHashFile, arsUpdateOnHashFileDefaultValue);
		arsPropertiesMap.put(arsMaximumSearchRowsNumber, arsMaximumSearchRowsNumberDefaultValue);
		arsPropertiesMap.put(arsEodServerAddress, arsEodServerAddressDefaultValue);
		arsPropertiesMap.put(arsEodServerPort, arsEodServerPortDefaultValue);
		arsPropertiesMap.put(arsPosDescriptionData, arsPosDescriptionDataDefaultValue);
		arsPropertiesMap.put(arsPosTerminalTypeData, arsPosTerminalTypeDataDefaultValue);
		arsPropertiesMap.put(arsPosStatusData, arsPosStatusDataDefaultValue);
		arsPropertiesMap.put(arsDefectiveDeclarableData, arsDefectiveDeclarableDataDefaultValue);
		arsPropertiesMap.put(arsReadyPosEodData, arsReadyPosEodDataDefaultValue);
		arsPropertiesMap.put(arsReadyServerEodData, arsReadyServerEodDataDefaultValue);
		arsPropertiesMap.put(arsPosStatusDescriptionData, arsPosStatusDescriptionDataDefaultValue);
		arsPropertiesMap.put(arsVatDefaultId, arsVatDefaultIdDefaultValue);
		arsPropertiesMap.put(arsExchangeRateSwitch, arsExchangeRateSwitchDefaultValue);
		arsPropertiesMap.put(arsInvokeMntApplyCommand, arsInvokeMntApplyCommandDefaultValue);
		arsPropertiesMap.put(compressedJournalFolder, compressedJournalFolderDefaultValue);

		return arsPropertiesMap;
	}

	public String getProperty(String key) {
		return getProperty(key, false);
	}

	public String getProperty(String key, boolean prependWorkingDir) {
		String value = ars.getProperty(key);

		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}

		return value;
	}

	public String getMlanPath() {
		// This path is always tied to arsPosServerPath: adding it to the
		// properties file is usless and can lead to a misalignment.
		return PathManager.getInstance().normalizePath(ArsServerEnvParameters.getInstance().getServerHomeDir(), true) + "data";
	}

	public String getSCtlPath() {
		// This path is always tied to arsPosServerPath: adding it to the
		// properties file is usless and can lead to a misalignment.
		return PathManager.getInstance().normalizePath(ArsServerEnvParameters.getInstance().getServerHomeDir(), true) + "data";
	}

	private Properties ars;

	private static ArsMappingProperties instance;
}
