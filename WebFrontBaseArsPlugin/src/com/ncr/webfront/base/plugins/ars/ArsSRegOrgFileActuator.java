package com.ncr.webfront.base.plugins.ars;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class ArsSRegOrgFileActuator implements ArsSRegOrgFileActuatorInterface {
	
	public ArsSRegOrgFileActuator(String sRegFilePath){
		sRegFilePath = PathManager.getInstance().buildNormalizedFullFilePath(sRegFilePath, sRegFileName);
		regFile = new File(sRegFilePath);

		if (! regFile.exists()){
			logger.error("File \""+ regFile+"\" does not exist!");
		}

		sRegFileLines = this.readAll();
		
	}

	@Override
	public String readLine(int id) {
		logger.debug("Begin readLine");
		String returnLine = "";
		
		for (String sRegLine : sRegFileLines) {
				int lineId = Integer.parseInt(sRegLine.substring(0,4));
				if (id == lineId ){
					returnLine = sRegLine;
					break;
				}
			}
		
		logger.debug("End readLine");
		return returnLine;
	}

	@Override
	public boolean writeLine(String line) {
		logger.debug("Begin writeLine");		
		try {		
			BufferedReader regReader = new BufferedReader(new FileReader(regFile));
			String oldline = "";
			StringBuffer newRegFileText = new StringBuffer();
			
			while((oldline = regReader.readLine()) != null){
				
				if (oldline.substring(0, 4).equals(line.substring(0, 4))){
					newRegFileText.append(line).append("\r\n");
				} else {
					newRegFileText.append(oldline).append("\r\n");
				}
            }
			regReader.close();
       
			FileWriter regWriter = new FileWriter(regFile);
			regWriter.write(newRegFileText.toString());
			regWriter.close();
			sRegFileLines = this.readAll();
			logger.debug("End writeLine");
			return true;
        
		} catch (FileNotFoundException fnf){
			logger.error("", fnf);
		} catch (IOException ioe){
			logger.error("", ioe);
		}	
		logger.debug("End writeLine");
		return false;
		
	}

	@Override
	public List<String> readAll() {
		logger.debug("Begin readAll()");
		List<String> sRegOrgFileLines = new ArrayList<String>();
		try {		
			BufferedReader regReader = new BufferedReader(new FileReader(regFile));
			String line;
			while ((line = regReader.readLine()) != null) {
				sRegOrgFileLines.add(line);
			}
			regReader.close();
		} catch (FileNotFoundException fnf){
			logger.error("", fnf);
		} catch (IOException ioe){
			logger.error("", ioe);
		}		
		logger.debug("End readAll()");
		return sRegOrgFileLines;
	}
	
	private Logger logger = WebFrontLogger.getLogger(ArsSRegOrgFileActuator.class);
	private static final String sRegFileName = "S_REGXXX.ORG";
	private File regFile;
	private List<String> sRegFileLines;

}
