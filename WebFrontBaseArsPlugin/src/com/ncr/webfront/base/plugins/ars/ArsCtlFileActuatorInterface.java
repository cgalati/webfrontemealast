package com.ncr.webfront.base.plugins.ars;

import java.io.File;
import java.util.List;

public interface ArsCtlFileActuatorInterface {
	public String readLine(int id);
	
	public boolean writeLine(String line);
	
	public boolean deleteLine(String operatorCode);
	
	public boolean insertLine(String newSCtlLine);
	
	public List <String> readAll();
	
	public List <String> readAll(File sCtlFile);	

	public List <String> searchByCode(String operatorCode);

	public List<String> searchByName(String name);

	public List<String> searchByName(String name, boolean readOnly);
}
