package com.ncr.webfront.base.plugins.ars;

import java.util.List;

public interface ArsSRegOrgFileActuatorInterface {

	public String readLine(int id);
	
	public boolean writeLine(String line);
	
	public List <String> readAll();	

}
