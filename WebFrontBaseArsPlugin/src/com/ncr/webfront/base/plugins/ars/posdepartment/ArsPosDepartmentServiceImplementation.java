package com.ncr.webfront.base.plugins.ars.posdepartment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.plugins.ars.ArsCommonUtils;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.plugins.ars.article.ArsPluHashFileActuator;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class ArsPosDepartmentServiceImplementation implements PosDepartmentServiceInterface {

	public ArsPosDepartmentServiceImplementation() {

		arsPosDepartmentServiceProperties = PropertiesLoader.loadOrCreatePropertiesFile(ArsMappingProperties.arsPluginPropertiesFileName,
				ArsMappingProperties.getArsPropertiesMap());

		serverPath = ArsServerEnvParameters.getInstance().getServerHomeDir();
		logger.info("serverPath = \"" + serverPath + "\"");
		dptFileFullPath = PathManager.getInstance().buildNormalizedFullFilePath(serverPath, dptFileName);
		logger.info("dptFileFullPath = \"" + dptFileFullPath + "\"");		
	}

	@Override
	public ErrorObject checkService() {

		logger.info("dptFileFullPath = \"" + dptFileFullPath + "\"");
		File file = new File(dptFileFullPath);
		if (!file.exists()) {
			logger.error("File \"" + dptFileFullPath + "\" does not exist!");
			return new ErrorObject(ErrorCode.SETUP_ERROR, "Il file \"" + dptFileFullPath + "\" non esiste!");
		}

		try {
			List<PosDepartment> tempDeptList = buildDeptList();

			if (tempDeptList == null || tempDeptList.size() == 0) {
				logger.error("DeptList is null or empty!");
				return new ErrorObject(ErrorCode.SETUP_ERROR, "La lista dei reparti è vuota!");
			}

		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}

		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	@Override
	public List<PosDepartment> getAll() {
		//if (deptList == null) {
			deptList = buildDeptList();
		//}
		return deptList;
	}

	@Override
	public List<PosDepartment> getDepartmentsOfLevel(int level) {
		List<PosDepartment> filteredDeptList = new ArrayList<PosDepartment>();
		
		if (deptList == null) {
			deptList = buildDeptList();
		}
		for (PosDepartment department: deptList) {
			if (department.getLevel() == level) {
				filteredDeptList.add(department);
			}
		}
		return filteredDeptList;
	}
	
	@Override
	public PosDepartment getByCode(String dptCode) {

		logger.debug("Begin getByCode");
		if (deptList == null) {
			deptList = buildDeptList();
		}

		for (PosDepartment posDepartment : deptList) {
			if (compareNormalizedDepartmentCodes(posDepartment.getCode(), dptCode)) {
				logger.debug("End getByCode");
				return posDepartment;
			}
		}
		logger.error("Department " + dptCode + " not found");
		logger.debug("End getByCode");
		return null;
	}

	private boolean compareNormalizedDepartmentCodes(String dptCode1, String dptCode2) {

		logger.debug("dptCode1 before normalize: " + dptCode1);
		try {
			int tempInt = Integer.parseInt(dptCode1);
			dptCode1 = "" + tempInt;
		} catch (Exception e) {
		}
		logger.debug("dptCode1 after  normalize: " + dptCode1);
		logger.debug("dptCode2 before normalize: " + dptCode2);
		try {
			int tempInt = Integer.parseInt(dptCode2);
			dptCode2 = "" + tempInt;
		} catch (Exception e) {
		}
		logger.debug("dptCode2 after  normalize: " + dptCode2);
		return dptCode1.equals(dptCode2);
	}

	private List<PosDepartment> buildDeptList() {
		logger.debug("Begin buildDeptList");
		List<PosDepartment> returnList = new ArrayList<PosDepartment>();

		File dptFile = new File(serverPath + File.separator + dptFileName);
		if (!dptFile.exists()) {
			logger.error("File \"" + dptFile + "\" does not exist!");
			return returnList;
		}

		try (BufferedReader dptReader = new BufferedReader(new FileReader(dptFile))) {
			String line = "";

			while ((line = dptReader.readLine()) != null) {
				String fields[] = line.split(":");
				PosDepartment posDept = new PosDepartment();

				posDept.setCode(fields[0]);
				posDept.setTotal(fields[1]);

				// code1 parsing
				int code1 = ArsCommonUtils.parseIntDefault(fields[2].substring(0, 1), 0, 16, logger, "code1");
				posDept.setNegativePrice((code1 & 8) == 8);
				posDept.setDeposit((code1 & 4) == 4);
				posDept.setScalable((code1 & 2) == 2);

				// code2 parsing
				int code2 = ArsCommonUtils.parseIntDefault(fields[2].substring(1, 2), 0, 16, logger, "code2");
				posDept.setDecimalQty((code2 & 1) == 1);

				// TODO x RELEASE 2.0 - verify if needed - Discount Allowed
				// (fields[3].substring(0, 1)

				posDept.setVatID(ArsCommonUtils.parseIntDefault(fields[3].substring(1, 2), 1, logger, "vat id"));

				// TODO x RELEASE 2.0 - verify if needed - Category
				// checkVoidString(fields[3].substring(2, 4)

				posDept.setDescription(checkVoidString(fields[6].substring(0, 15)));

				//posDept.setManualDiscountID(ArsCommonUtils.parseIntDefault(fields[4].substring(15, 16), 0, logger, "manual discount id"));

				//posDept.setUpbOperationType(ArsCommonUtils.parseIntDefault(fields[4].substring(16, 17), 0, logger, "upb operation type"));

				//posDept.setUpbProviderId(ArsCommonUtils.parseIntDefault(fields[4].substring(17, 18), 0, logger, "upb provider id"));

				//posDept.setAutomDiscountType(ArsCommonUtils.parseIntDefault(fields[4].substring(19, 20), 3, logger, "dp discount type"));

				// if (isLeaf(posDept)) {
				logger.info("Adding " + posDept.getCode() + ":" + posDept.getDescription() + " to posDeptList");

				returnList.add(posDept);
				// }
			}

			logger.debug("End buildDeptList");
			return returnList;

		} catch (FileNotFoundException fnfe) {
			logger.error("File Reparti non trovato", fnfe);
			return null;

		} catch (IOException ioe) {
			logger.error("I/O Exception", ioe);
			return null;
		}
	}

	private boolean isLeaf(PosDepartment posDepartment) {
		return StringUtils.isNumeric(posDepartment.getCode());
	}

	private String checkVoidString(String field) {
		if (field.trim().length() == 0)
			return "-";
		return field;
	}
	
	
	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 */
	public ErrorObject deleteDepartment(String departmentCodeToDelete) {
		logger.debug("Begin deleteArticle");
		
		try {
			File dptFile = new File(serverPath + File.separator + dptFileName);
			
			if (!dptFile.exists()) {
				logger.error("File \"" + dptFile + "\" does not exist!");
				
				return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "File \"" + dptFile + "\" does not exist!");
			}
			
			File tempFile = new File(serverPath + File.separator + "myTempFile.txt");

			BufferedReader reader = new BufferedReader(new FileReader(dptFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;

			while((currentLine = reader.readLine()) != null) {
			    String trimmedLine = currentLine.trim();
			    String fields[] = currentLine.split(":");
			    
			    if (fields[0].equals(departmentCodeToDelete)) {
			    	continue;
			    }

			    writer.write(currentLine + "\r\n");
			    
			}
			writer.close(); 
			reader.close(); 
			
		    if (!dptFile.delete()) {
		       System.out.println("Could not delete file");
		       return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Could not delete file");
		    }
			      
		    if (!tempFile.renameTo(dptFile))
		    	return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Could not delete file");

		} catch(Exception e) {
			
		}
		
		ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR, "Department successfully deleted");
		return errorObject;
	}
	

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 */
	public ErrorObject addDepartment(PosDepartment departmentToAdd) {
		logger.debug("Begin addArticle");

		try {
			File dptFile = new File(serverPath + File.separator + dptFileName);
			
			if (!dptFile.exists()) {
				logger.error("File \"" + dptFile + "\" does not exist!");
				
				return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "File \"" + dptFile + "\" does not exist!");
			}
			
			File tempFile = new File(serverPath + File.separator + "myTempFile.txt");

			BufferedReader reader = new BufferedReader(new FileReader(dptFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;
			
			String newRow = newRow(departmentToAdd);

			//1000:*100:00:0002:0000:0000:BABY CAREs          xxxxxxxxxx+000000+0000000+0000000000+000000+0000000+0000000000+000000+0000000+0000000000
			
			boolean writed = false;
			while((currentLine = reader.readLine()) != null) {			    
			    String fields[] = currentLine.split(":");
			    
			    if (currentLine.equals("") && !writed) {
			    	writer.write(newRow + "\r\n");
			    	writed = true;
			    } else {
			    	if (fields[0].compareTo(departmentToAdd.getCode()) < 0) {
			    		writer.write(currentLine + "\r\n");			    		
			    	} else {
			    		if (fields[0].compareTo(departmentToAdd.getCode()) == 0) {
			    			return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "the Department code already exists");
			    		} else {	
			    			if (!writed) {
			    				writer.write(newRow + "\r\n");
			    				writed = true;
			    			}

					    	writer.write(currentLine + "\r\n");						    	
			    		}
			    	}
			    }
			}
			
			if (!writed) {
				writer.write(newRow + "\r\n");				
			}
			
			writer.close(); 
			reader.close(); 
			
		    if (!dptFile.delete()) {
		       System.out.println("Could not delete file");
		       return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Could not delete file");
		    }
			      
		    if (!tempFile.renameTo(dptFile))
		    	return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Could not delete file");

		} catch(Exception e) {
			
		}
		
		ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR, "Department successfully add");
		return errorObject;

	}

	
	private String newRow(PosDepartment departmentToAdd) {
		String line = "";		
			
		// 1000:*100:00:0002:0000:0000:BABY CAREs          xxxxxxxxxx+000000+0000000+0000000000+000000+0000000+0000000000+000000+0000000+0000000000
		String code = StringUtils.leftPad(departmentToAdd.getCode(), 4, "*"); //String.format("%04d", departmentToAdd.getCode());
		String total = StringUtils.leftPad(departmentToAdd.getTotal(), 4, "*"); 
	    String level = StringUtils.leftPad(String.valueOf(departmentToAdd.getLevel()), 2, "0"); 
	    String vat = StringUtils.leftPad(String.valueOf(departmentToAdd.getVatID()), 4, "0"); 
	    
			
		line = code + ":" + 
				total + ":" + 
				level + ":" +
				vat + ":0000:0000:" +
				StringUtils.rightPad(departmentToAdd.getDescription(), 20, " ") + "xxxxxxxxxx" +
				"+000000+0000000+0000000000+000000+0000000+0000000000+000000+0000000+0000000000";
		
		return line;
	}


	@Override
	/*
	 * (non-Javadoc)
	 * 
	 */
	public ErrorObject updateDepartment(PosDepartment departmentToUpdate) {
		logger.debug("Begin updateDepartment");
		boolean writed = false;
		
		try {
			File dptFile = new File(serverPath + File.separator + dptFileName);
			
			if (!dptFile.exists()) {
				logger.error("File \"" + dptFile + "\" does not exists!");
				
				return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "File \"" + dptFile + "\" does not exists!");
			}
			
			File tempFile = new File(serverPath + File.separator + "myTempFile.txt");

			BufferedReader reader = new BufferedReader(new FileReader(dptFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

			String currentLine;

			while((currentLine = reader.readLine()) != null) {			    
			    String fields[] = currentLine.split(":");
			    
				if (fields[0].equals(departmentToUpdate.getCode())) {
					String updateRow = updateRow(departmentToUpdate, currentLine);
					writer.write(updateRow + "\r\n");	
					writed = true;
				} else {
					writer.write(currentLine + "\r\n");	
				}
			}
			
			writer.close(); 
			reader.close(); 
			
		    if (!dptFile.delete()) {
		       System.out.println("Could not update file");
		       return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Could not update file");
		    }
			      
		    if (!tempFile.renameTo(dptFile))
		    	return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "Could not update file");
		    
		} catch (Exception e) {
			
		}

		if (!writed) {
			return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, "The code department does not exists in the file!");
		}				
		    
		return new ErrorObject(ErrorCode.OK_NO_ERROR, "Department successfully update");
	}
	
	private String updateRow(PosDepartment departmentToUpdate, String row) {
		String line = "";
		
		line = row.substring(0, 28) + StringUtils.rightPad(departmentToUpdate.getDescription(), 20, " ") + row.substring(48);
		
		return line;
	}


	@Override
	/*
	 * (non-Javadoc)
	 * 
	 */
	public PosDepartment createDepartment() {
		logger.debug("Begin createArticle");
		PosDepartment dep = new PosDepartment();
		
		dep.setCode("");
		dep.setDescription("");

		logger.debug("End createArticle");
		return dep;

	}


	private Logger logger = WebFrontLogger.getLogger(ArsPosDepartmentServiceImplementation.class);
	private Properties arsPosDepartmentServiceProperties;
	private String serverPath;
	private final String dptFileName = "S_DPTXXX.ORG";
	private String dptFileFullPath;
	private List<PosDepartment> deptList;

}
