package com.ncr.webfront.base.plugins.ars.article;

public class ArsPluRecordField {

	public ArsPluRecordField() {
   }
	public ArsPluRecordField(int offset, int length) {
		this.offset = offset;
		this.length = length;
   }

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	private int offset;
	private int length;
}
