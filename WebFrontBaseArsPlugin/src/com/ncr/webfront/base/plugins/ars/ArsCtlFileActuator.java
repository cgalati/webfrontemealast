package com.ncr.webfront.base.plugins.ars;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public abstract class ArsCtlFileActuator implements ArsCtlFileActuatorInterface {
	protected ArsCtlFileActuator(String sCtlFilePath, String orgFileName){
		sCtlFilePath = PathManager.getInstance().buildNormalizedFullFilePath(sCtlFilePath, orgFileName);
		orgFile = new File(sCtlFilePath);
		if(!orgFile.exists()) {
			logger.error("Error! The file \"" + sCtlFilePath + "\" does not exist!");
		}
	}
	
	protected ArsCtlFileActuator(String sCtlFilePath, String serverNumber, String datFileName){
		sCtlFilePath = PathManager.getInstance().buildNormalizedFullFilePath(sCtlFilePath, datFileName.replaceAll("nnn", serverNumber));
		datFile = new File(sCtlFilePath);
		if(!datFile.exists()) {
			logger.error("Error! The file \"" + sCtlFilePath + "\" does not exist!");
		}
	}
	
	@Override
	public String readLine(int id) {
		logger.debug("Begin readLine");
		String returnLine = "";
		
		for (String orgFileLine : readAll(orgFile)) {
				int lineId = Integer.parseInt(orgFileLine.substring(0,4));
				if (id == lineId ){
					returnLine = orgFileLine;
					break;
				}
			}
		
		logger.debug("End readLine");
		return returnLine;
	}

	@Override
	public boolean writeLine(String line) {
		logger.debug("Begin writeLine");		
		try {		
			BufferedReader ctlReader = new BufferedReader(new FileReader(orgFile));
			String oldline = "";
			StringBuffer newCtlFileText = new StringBuffer();
			
			while((oldline = ctlReader.readLine()) != null){
				
				if (oldline.substring(0, 4).equals(line.substring(0, 4))){
					newCtlFileText.append(line).append("\r\n");
				} else {
					newCtlFileText.append(oldline).append("\r\n");
				}
            }
			ctlReader.close();
       
			FileWriter regWriter = new FileWriter(orgFile);
			regWriter.write(newCtlFileText.toString());
			regWriter.close();
			logger.debug("End writeLine");
			return true;
        
		} catch (FileNotFoundException fnf){
			logger.error("", fnf);
		} catch (IOException ioe){
			logger.error("", ioe);
		}	
		logger.debug("End writeLine");
		return false;
		
	}
	
	@Override
	public boolean insertLine(String newLine) {
		boolean entered = false;
		logger.debug("Begin insertLine");		
		try {		
			Integer codeToSave = Integer.parseInt(newLine.substring(0, 4));
			BufferedReader ctlReader = new BufferedReader(new FileReader(orgFile));
			String savedLine = "";
			Integer savedCode;
			StringBuffer newCtlFileText = new StringBuffer();
			
			while((savedLine = ctlReader.readLine()) != null){
				savedCode = Integer.parseInt(savedLine.substring(0, 4));
				if(!entered && codeToSave.intValue() < savedCode) {
					entered = true;
					newCtlFileText.append(newLine).append("\r\n").append(savedLine).append("\r\n");
				} else {
					newCtlFileText.append(savedLine).append("\r\n");
				}
				
            }
			ctlReader.close();
       
			FileWriter regWriter = new FileWriter(orgFile);
			regWriter.write(newCtlFileText.toString());
			regWriter.close();
			logger.debug("End writeLine");
			return true;
        
		} catch (FileNotFoundException fnf){
			logger.error("", fnf);
		} catch (IOException ioe){
			logger.error("", ioe);
		}	
		logger.debug("End insertLine");
		return false;
		
	}

	@Override
	public List<String> readAll() {
		return readAll(orgFile);
	}
	
	@Override
	public List<String> readAll(File ctlFile) {
		logger.debug("Begin readAll from file " + ctlFile.getName());
		List<String> orgFileLines = new ArrayList<String>();
		try {		
			BufferedReader ctlReader = new BufferedReader(new FileReader(ctlFile));
			String line;
			while ((line = ctlReader.readLine()) != null) {
				orgFileLines.add(line);
			}
			ctlReader.close();
		} catch (FileNotFoundException fnf){
			logger.error("", fnf);
		} catch (IOException ioe){
			logger.error("", ioe);
		}		
		logger.debug("End readAll()");
		return orgFileLines;
	}
	
	@Override
	public List<String> searchByCode(String operatorCode) {
		logger.debug("Begin searchByCode");
		List<String> returnList = new ArrayList<String>();
		
		for (String orgFileLine : readAll(orgFile)) {
				if (StringUtils.stripStart(orgFileLine, "0").startsWith(operatorCode)){
					returnList.add(orgFileLine);
				}
			}
		
		logger.debug("End searchByCode");
		return returnList;
	}

	@Override
	public List<String> searchByName(String name, boolean readOnly) {
		logger.debug("Begin searchByName");
		List<String> returnList = new ArrayList<String>();
		
		name = name.toUpperCase();
		String datFileLineName;
		if(readOnly) {
			for (String datFileLine : readAll(datFile)) {
				datFileLineName = datFileLine.substring(17, 32).trim();
				//datFileLineName = datFileLine.substring(5, 20).trim();
				if(datFileLineName.startsWith(name)) {
					returnList.add(datFileLine);
				}
			}
		} else {
			for (String orgFileLine : readAll(orgFile)) {
				//datFileLineName = orgFileLine.substring(5, 20).trim();
				datFileLineName = orgFileLine.substring(17, 32).trim();
				if(datFileLineName.startsWith(name)) {
					returnList.add(orgFileLine);
				}
			}
		}
		
		logger.debug("End searchByName");
		return returnList;
	}

	@Override
	public boolean deleteLine(String operatorCode) {
		logger.debug("Begin deleteLine");		
		try {		
			BufferedReader ctlReader = new BufferedReader(new FileReader(orgFile));
			String oldline = "";
			StringBuffer newCtlFileText = new StringBuffer();
			
			while((oldline = ctlReader.readLine()) != null){
				
				if (!oldline.substring(0, 4).equals(operatorCode)){
					newCtlFileText.append(oldline).append("\r\n");
				}
            }
			ctlReader.close();
       
			FileWriter regWriter = new FileWriter(orgFile);
			regWriter.write(newCtlFileText.toString());
			regWriter.close();
			logger.debug("End deleteLine");
			return true;
        
		} catch (FileNotFoundException fnf){
			logger.error("", fnf);
		} catch (IOException ioe){
			logger.error("", ioe);
		}	
		logger.debug("End writeLine");
		return false;
	}
	
	private File datFile;
	private File orgFile;
	
	private static final Logger logger = WebFrontLogger.getLogger(ArsCtlFileActuator.class);
}
