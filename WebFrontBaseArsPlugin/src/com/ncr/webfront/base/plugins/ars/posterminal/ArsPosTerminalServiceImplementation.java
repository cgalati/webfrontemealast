package com.ncr.webfront.base.plugins.ars.posterminal;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.plugins.ars.ArsMLanFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsMLanFileActuatorInterface;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class ArsPosTerminalServiceImplementation implements PosTerminalServiceInterface {
	
	public ArsPosTerminalServiceImplementation(){
		logger.debug("Begin Constructor");
		arsPosTerminalProperties = PropertiesLoader.loadOrCreatePropertiesFile(ArsMappingProperties.arsPluginPropertiesFileName,
			      ArsMappingProperties.getArsPropertiesMap());
		
		String mLanPath = ArsMappingProperties.getInstance().getMlanPath();
		String serverNumber = ArsServerEnvParameters.getInstance().getServerNumber();
		arsMLanFileActuator = new ArsMLanFileActuator(mLanPath,serverNumber);
		logger.debug("End Constructor");
	}

	@Override
	public ErrorObject checkService() {
		try {
			buildPosTerminalList();
		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
	

	@Override
	public List<PosTerminal> getAll() {
		logger.debug("Begin getAll");
		if (posTerminalList == null){
			buildPosTerminalList();
		}
		logger.debug("End getAll");
		return posTerminalList;	
	}

	
	@Override
	public PosTerminal getByCode(String terminalCode) {
		logger.debug("Begin getByCode");
		if (posTerminalList == null){
			buildPosTerminalList();
		}
		
		for(PosTerminal posTerminal:posTerminalList)
		{
			if(posTerminal.getTerminalCode().equals(terminalCode)){
				logger.debug("End getByCode");
				return posTerminal;
			}
		}		
		logger.error("PosTerminal "+ terminalCode + " non trovato.");
		logger.debug("End getByCode");
		return null;
	}
	
	private void buildPosTerminalList(){
		logger.debug("Begin buildPosTerminalList");
		posTerminalList = new ArrayList<PosTerminal>();
		List<String> posTerminalLines =  arsMLanFileActuator.readAllPosTerminals();
		
		for(String posTerminalLine:posTerminalLines){
			PosTerminal posTerminal = new PosTerminal();
			posTerminal.setType(PosTerminalType.REGISTER);
			posTerminal.setTerminalCode(posTerminalLine.substring(2, 5));
			posTerminal.setDescription("Cassa "+posTerminalLine.substring(2, 5));
			posTerminalList.add(posTerminal);
		}	
		logger.debug("End buildPosTerminalList");
	}
	
	
	private Logger logger = WebFrontLogger.getLogger(ArsPosTerminalServiceImplementation.class);
	private Properties arsPosTerminalProperties;
	private ArsMLanFileActuatorInterface arsMLanFileActuator;
	private List<PosTerminal> posTerminalList;

}
