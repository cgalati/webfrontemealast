package com.ncr.webfront.base.plugins.ars;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.commandexecution.CommandLocalRunner;
import com.ncr.webfront.core.utils.commandexecution.RunningInfo;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class GmRecMntActuator {
	
	public GmRecMntActuator(String pluMaintFileFolder){
	this.pluMaintFilePathOnly = pluMaintFileFolder;
	}

	
	public boolean createMaintFileForDelete(String articleCode) {
		logger.debug("Begin createMaintFileForDelete");
		try {
			// create tmp file for mntapply (delete)
			String filename = pluMaintFilePathOnly + File.separator + tmpFileName + System.currentTimeMillis();
			RandomAccessFile file;
			file = new RandomAccessFile(filename, "rwd");
			int i;
			for (i = 0; i < 3; i++) {
				file.writeByte(' ');
			}

			file.writeBytes(StringUtils.leftPad(articleCode, 13));
			i += 13;
			for (; i < 78; i++) {
				file.writeByte('-');
			}
			file.writeByte(0x0d);
			file.writeByte(0x0a);
			file.close();
			
			// copy tmp file in his right name for mntapply
			renameFileFromTmpToDat(filename);					
		} catch (FileNotFoundException e) {
			logger.error("File Not Found Exception", e);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			logger.error("I/O Exception", e);
			e.printStackTrace();
			return false;
		}
		logger.debug("End createMaintFileForDelete");
		return true;
	}

	
	public boolean createMaintFileForAddUpdateCommand(String line) {
		logger.debug("Begin createMaintFileForAddUpdate");
		try {
			// create tmp file for mntapply (add, update or command)
			String fileName = pluMaintFilePathOnly + File.separator + tmpFileName + System.currentTimeMillis();
			RandomAccessFile file = new RandomAccessFile(fileName, "rwd");

			file.writeBytes(line);
			file.writeByte(0x0d);
			file.writeByte(0x0a);
			file.close();
			
			// rename tmp file
			renameFileFromTmpToDat(fileName);		
		} catch (FileNotFoundException e) {
			logger.error("File Not Found Exception", e);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			logger.error("I/O Exception", e);
			e.printStackTrace();
			return false;
		}
		invokeInvokeMntApplyIfNeeded();
		logger.debug("End createMaintFileForAddUpdate");
		return true;
	}

	private void invokeInvokeMntApplyIfNeeded() {
		logger.debug("BEGIN");
		String scriptPath = ArsMappingProperties.getInstance().getProperty(ArsMappingProperties.arsInvokeMntApplyCommand);
		logger.debug("scriptPath " + scriptPath);
		if (scriptPath == null || scriptPath.isEmpty()) {
			logger.debug("END (mntApply not to invoke)");
			return;
		}
		try {
			int timeoutInSeconds = 120;

			CommandLocalRunner localRunner = new CommandLocalRunner();
			localRunner.setTimeoutInSeconds(timeoutInSeconds);
			com.ncr.webfront.core.utils.commandexecution.Command command = new com.ncr.webfront.core.utils.commandexecution.Command();

			command.setFullCommandLine(scriptPath);
			RunningInfo info = new RunningInfo(command);
			localRunner.setRunningInfo(info);
			localRunner.run();
		} catch (Exception e) {
			logger.error("Exception!", e);
		}
		logger.debug("END");
	}
	
	public boolean createMaintFileForAddUpdateCommand(List<String> lines) {
		logger.debug("Begin createMaintFileForAddUpdate");
		try {
			// create tmp file for mntapply (add, update or command)
			String filename = pluMaintFilePathOnly + File.separator + tmpFileName + System.currentTimeMillis();

			RandomAccessFile file = new RandomAccessFile(filename, "rwd");

			for (String line:lines){
				file.writeBytes(line);
				file.writeByte(0x0d);
				file.writeByte(0x0a);
			}
			file.close();
			// rename tmp file
			renameFileFromTmpToDat(filename);
					
		} catch (FileNotFoundException e) {
			logger.error("File Not Found Exception", e);
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			logger.error("I/O Exception", e);
			e.printStackTrace();
			return false;
		}
		invokeInvokeMntApplyIfNeeded();
		logger.debug("End createMaintFileForAddUpdate");
		return true;
	}

	private void renameFileFromTmpToDat(String sourceFileNameAndPath) throws IOException{
		Path source = Paths.get(sourceFileNameAndPath);
		Path target = Paths.get(pluMaintFilePathOnly + File.separator + maintFileName + System.currentTimeMillis() + maintFileExtension);
		Path targetCtl = Paths.get(pluMaintFilePathOnly + File.separator + maintFileName + System.currentTimeMillis() + ctlFileExtension);
		Files.move(source, target, REPLACE_EXISTING);
		Files.copy(target, targetCtl, REPLACE_EXISTING);
	}
	
	private Logger logger = WebFrontLogger.getLogger(GmRecMntActuator.class);
	private String pluMaintFilePathOnly;
	private static final String maintFileName = "GMRECMNT-";
	private static final String tmpFileName = "TMPMNT-";
	private static final String maintFileExtension = ".DAT";
	private static final String ctlFileExtension = ".CTL";
}
