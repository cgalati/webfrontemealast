package com.ncr.webfront.base.plugins.ars;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class ArsMLanFileActuator implements ArsMLanFileActuatorInterface {
	
	/**
	 * Constructor to be used for MLANXXX.ORG 
	 * @param mLanFilePath
	 */
	public ArsMLanFileActuator(String mLanFilePath){
		mLanFilePath = PathManager.getInstance().buildNormalizedFullFilePath(mLanFilePath, mLanOrgFileName);
		mLanFile =  new File(mLanFilePath);
		if (! mLanFile.exists()){
			logger.error("Error! The file \"" + PathManager.getInstance().buildNormalizedFullFilePath(mLanFilePath, mLanOrgFileName) + "\" does not exist!");
		}
	}
	
	/**
	 * Constructor to be used for MLANnnn.DAT 
	 * @param mLanFilePath
	 * @param serverNumber
	 */
	public ArsMLanFileActuator(String mLanFilePath, String serverNumber){
		mLanFilePath = PathManager.getInstance().buildNormalizedFullFilePath(mLanFilePath, mLanDatFileName.replaceAll("nnn", serverNumber));
		mLanFile =  new File(mLanFilePath);		
		if (! mLanFile.exists()){
			logger.error("Error! The file \"" + PathManager.getInstance().buildNormalizedFullFilePath(mLanFilePath, mLanDatFileName.replaceAll("nnn", serverNumber)) + "\" does not exist!");
		}
	}
	
	/**
	 * Constructor to be used for MLANnnn.DAT 
	 * @param mLanFilePath
	 * @param serverNumber
	 */
	public ArsMLanFileActuator(String mLanFilePath, int serverNumber){
		String serverNumberStr = StringUtils.leftPad(String.valueOf(serverNumber), 3, '0');
		mLanFilePath = PathManager.getInstance().buildNormalizedFullFilePath(mLanFilePath, mLanDatFileName.replaceAll("nnn", serverNumberStr));
		mLanFile =  new File(mLanFilePath);		
	}
	
	@Override
	public String readLine(int id) {
		String idStr = StringUtils.leftPad(String.valueOf(id), 3, '0');
		
		return readLine(idStr);
	}
	
	@Override
	public String readLine(String idStr) {
		String mLanLine = "";
		
		logger.debug("Begin readLine()");
		
		try (BufferedReader regReader = new BufferedReader(new FileReader(mLanFile))) {		
			String line = "";
			
			while ((line = regReader.readLine()) != null) {
				if (line.indexOf(idStr) == 2){
					mLanLine = line;
					break;
				}
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		logger.debug("End readLine()");
		
		return mLanLine;
	}

	@Override
	public List<String> readAll() {
		List<String> mLanFileLines = new ArrayList<String>();
		
		logger.debug("Begin readAll()");
		
		try (BufferedReader regReader = new BufferedReader(new FileReader(mLanFile))) {		
			String line = "";
			
			while ((line = regReader.readLine()) != null) {
				mLanFileLines.add(line);
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		logger.debug("End readAll()");
		
		return mLanFileLines;
	}
	
	@Override
	public List<String> readAllPosTerminals() {
		List<String> mLanPosTerminalLines = new ArrayList<String>();
		
		logger.debug("Begin readAllPosTerminals()");
		
		try (BufferedReader regReader = new BufferedReader(new FileReader(mLanFile))) {		
			String line = "";
			
			logger.debug("-----------------------------------------------------------------------");
			logger.debug("BEGIN READING " + mLanFile + " file");
			while ((line = regReader.readLine()) != null) {
				if (line.startsWith("R")) {
					logger.debug("R record: " + line);
					mLanPosTerminalLines.add(line);
				}
			}
			logger.debug("END   READING " + mLanFile + " file");
			logger.debug("-----------------------------------------------------------------------");
		} catch (Exception e) {
			logger.error("", e);
		}
		
		logger.debug("End readAllPosTerminals()");
		
		return mLanPosTerminalLines;
	}	
	
	private Logger logger = WebFrontLogger.getLogger(ArsMLanFileActuator.class);
	private static final String mLanOrgFileName = "M_LANXXX.ORG";
	private static final String mLanDatFileName = "M_LANnnn.DAT";
	private File mLanFile;

}
