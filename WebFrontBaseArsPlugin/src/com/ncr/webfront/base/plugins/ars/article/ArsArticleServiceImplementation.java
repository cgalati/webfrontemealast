	package com.ncr.webfront.base.plugins.ars.article;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleComparator;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.language.LabelsMap;
import com.ncr.webfront.base.language.LanguageServiceInterface;
import com.ncr.webfront.base.plugins.ars.ArsCommonListsServiceImplementation;
import com.ncr.webfront.base.plugins.ars.ArsCommonUtils;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsPRegDatFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.plugins.ars.GmRecMntActuator;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.tare.TareServiceInterface;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public class ArsArticleServiceImplementation implements ArticleServiceInterface {

	public ArsArticleServiceImplementation(PosDepartmentServiceInterface posDepartmentService, VatServiceInterface vatService, TareServiceInterface tareService,
	      ArsPluRowTemplate arsPluRowTemplate, LanguageServiceInterface languageService) {
		
		initialize(posDepartmentService, vatService, tareService, arsPluRowTemplate);

		logger.info("arsArticleServiceProperties:" + arsArticleServiceProperties);

		pluHashFileFullPath = PathManager.getInstance().normalizePath(ArsServerEnvParameters.getInstance().getServerHomeDir(), true) + "inq" + File.separator
		      + ARS_SERVER_PLU_HASH_FILE_NAME;
		this.arsPluHashFileActuator = new ArsPluHashFileActuator(arsPluRowTemplate, pluHashFileFullPath);

		labelsMap = languageService.load("arsarticleservice");
		for (String key : labelsMap.keySet()) {
			logger.info("   " + key + "=" + labelsMap.get(key));
		}

	}

	public ArsArticleServiceImplementation(ArsPluHashFileActuatorInterface arsPluHashFileActuator, PosDepartmentServiceInterface posDepartmentService,
	      VatServiceInterface vatService, TareServiceInterface tareService, ArsPluRowTemplate arsPluRowTemplate) {
		initialize(posDepartmentService, vatService, tareService, arsPluRowTemplate);

		logger.info("arsArticleServiceProperties:" + arsArticleServiceProperties);

		this.arsPluHashFileActuator = arsPluHashFileActuator;
	}

	@Override
	public ErrorObject checkService() {

		File file = new File(pluHashFileFullPath);
		if (!file.exists()) {
			String defaultMsg = "File \"" + pluHashFileFullPath + "\" does not exist!";
			logger.error(defaultMsg);
			String msg = labelsMap.get("checkservice.error.pluhashfile.not.found", defaultMsg);
			msg = String.format(msg, pluHashFileFullPath);
			return new ErrorObject(ErrorCode.SETUP_ERROR, msg);
		}

		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#searchByArticleCode
	 * (java.lang.String)
	 */
	public List<Article> searchByArticleCode(String articleCode) {
		logger.debug("Begin searchByArticleCode");
		List<Article> returnList = new ArrayList<Article>();
		Article tmpArticle = null;

		int priceFromBarcode = 0;
		if (ArsPluUtils.isPriceEmbedded(eanXlines, articleCode)) {
			if (articleCode.length() == 13) {
				priceFromBarcode = ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode);
				articleCode = ArsPluUtils.clearEmbeddedPrice(eanXlines, articleCode);
			} else if (articleCode.length() == 8) {
				priceFromBarcode = ArsPluUtils.getEmbeddedPrice(eanXlines, articleCode);
				articleCode = ArsPluUtils.clearEmbeddedPrice(eanXlines, articleCode);
			}
		}

		List<String> pluHashFileLines = arsPluHashFileActuator.searchByArticleCode(articleCode);
		for (String pluHashFileLine : pluHashFileLines) {
			tmpArticle = createArticleFromArsPluRecord(pluHashFileLine, priceFromBarcode);
			if (tmpArticle != null) {
				returnList.add(tmpArticle);
			}
		}

		logger.debug("End searchByArticleCode");
		return returnList;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#searchByArticleCode
	 * (java.lang.String)
	 */
	public List<Article> searchByStartsWithArticleCode(String articleCode) {
		logger.debug("Begin searchByStartsWithArticleCode");
		List<Article> returnList = new ArrayList<Article>();
		Article tmpArticle = null;

		int priceFromBarcode = 0;

		List<String> pluHashFileLines = arsPluHashFileActuator.searchByStartsWithArticleCode(articleCode);
		for (String pluHashFileLine : pluHashFileLines) {
			tmpArticle = createArticleFromArsPluRecord(pluHashFileLine, priceFromBarcode);
			if (tmpArticle != null) {
				returnList.add(tmpArticle);
			}
		}

		logger.debug("End searchByStartsWithArticleCode");
		return returnList;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#searchByPluCode(
	 * java.lang.String)
	 */
	public List<Article> searchByPluCode(String pluCode) {
		return null;
	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#searchByDescription
	 * (java.lang.String)
	 */
	public List<Article> searchByDescription(String description) {
		logger.debug("Begin searchByDescription");
		List<Article> returnList = new ArrayList<Article>();
		Article tmpArticle;

		try {
			description = URLDecoder.decode(description, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Error during decode of string: " + description);
		}

		List<String> pluHashFileLines = arsPluHashFileActuator.searchByDescription(description, maxSearchRowsNumber);
		for (String pluHashFileLine : pluHashFileLines) {
			tmpArticle = createArticleFromArsPluRecord(pluHashFileLine, null);
			if (tmpArticle != null) {
				returnList.add(tmpArticle);
			}
		}

		logger.debug("sorting by description");

		Collections.sort(returnList, new ArticleComparator(true));

		logger.debug("End searchByDescription");
		return returnList;

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#deleteArticle(com
	 * .ncr.webfront.base.article.Article)
	 */
	public ErrorObject deleteArticle(String articleCodeToDelete) {
		logger.debug("Begin deleteArticle");

		if (!articleCodeToDelete.trim().equals("")) {

			// during delete operation there are some issues with ARS Pos Server to
			// update directly hash file
			// so i create just the maint file
			boolean result = gmRecMntActuator.createMaintFileForDelete(articleCodeToDelete);
			if (!result) {
				String defaultMsg = "Problems while creating the maintenance file for article deletion";
				logger.error(defaultMsg);
				String msg = labelsMap.get("article.delete.error.maintenance", defaultMsg);
				logger.debug("End deleteArticle");
				return new ErrorObject(ErrorCode.GENERIC_ERROR, msg);
			}

			String defaultMsg = "Article successfully deleted";
			logger.info(defaultMsg);
			String msg = labelsMap.get("article.delete.success", defaultMsg);
			ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR, msg);
			logger.debug("End deleteArticle");
			return errorObject;
		}

		// article code is empty
		String defaultMsg = "Invalid article code";
		logger.error(defaultMsg);
		String msg = labelsMap.get("article.delete.error.invalidarticlecode", defaultMsg);
		logger.debug("End deleteArticle");
		return new ErrorObject(ErrorCode.DATA_VALIDATION_ERROR, msg);

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#addArticle(com.ncr
	 * .webfront.base.article.Article)
	 */
	public ErrorObject addArticle(Article articleToAdd) {
		logger.debug("Begin addArticle");

		ErrorObject errorObject;

		// create hash line with fields from articleToAdd
		String linePlu = createArsPluRecordFromArticleObject(articleToAdd);
		logger.info("PLU hash file row =\"" + linePlu + "\"");

		if (updateOnHashFile) {
			errorObject = arsPluHashFileActuator.addArticle(articleToAdd.getArticleCode(), linePlu);

			if (errorObject.getErrorCode() == ErrorCode.OK_NO_ERROR) {
				boolean result = gmRecMntActuator.createMaintFileForAddUpdateCommand(linePlu);
				if (!result) {
					String defaultMsg = "Article successfully added to hash file but problems creating maintenance file";
					logger.error(defaultMsg);
					String msg = labelsMap.get("article.add.error.partial.maintfile", defaultMsg);
					return new ErrorObject(ErrorCode.GENERIC_ERROR, msg);
				}
			}
		} else {
			boolean result = gmRecMntActuator.createMaintFileForAddUpdateCommand(linePlu);
			if (!result) {
				String defaultMsg = "Problems while creating the maintenance file for article insertion";
				logger.error(defaultMsg);
				String msg = labelsMap.get("article.add.error.maintfile", defaultMsg);
				return new ErrorObject(ErrorCode.GENERIC_ERROR, msg);
			}
		}
		String defaultMsg = "Article insertion correctly sent to ARS server";
		logger.info(defaultMsg);
		String msg = labelsMap.get("article.add.success", defaultMsg);
		errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR, msg);
		logger.debug("End addArticle");
		return errorObject;

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#updateArticle(com
	 * .ncr.webfront.base.article.Article)
	 */
	public ErrorObject updateArticle(Article articleToUpdate) {
		logger.debug("Begin updateArticle");

		// create hash line with fields from articleToUpdate
		String linePluOri = arsPluHashFileActuator.findArticle(articleToUpdate.getArticleCode());
		logger.info("Plu line before modification: \"" + linePluOri + "\"");
		String linePlu = createArsPluRecordFromArticleObject(articleToUpdate);
		logger.info("Plu line after  modification: \"" + linePlu + "\"");

		ErrorObject errorObject;

		if (updateOnHashFile) {
			errorObject = arsPluHashFileActuator.updateArticle(articleToUpdate.getArticleCode(), linePlu);

			if (errorObject.getErrorCode() == ErrorCode.OK_NO_ERROR) {
				boolean result = gmRecMntActuator.createMaintFileForAddUpdateCommand(linePlu);
				if (!result) {
					String defaultMsg = "Article successfully modified to hash file but problems creating maintenance file";
					logger.error(defaultMsg);
					String msg = labelsMap.get("article.update.error.partial.maintfile", defaultMsg);
					return new ErrorObject(ErrorCode.GENERIC_ERROR, msg);
				}
			}
		} else {
			boolean result = gmRecMntActuator.createMaintFileForAddUpdateCommand(linePlu);
			if (!result) {
				String defaultMsg = "Problems while creating the maintenance file for article update";
				logger.error(defaultMsg);
				String msg = labelsMap.get("article.update.error.maintfile", defaultMsg);
				return new ErrorObject(ErrorCode.GENERIC_ERROR, msg);
			}

		}

		String defaultMsg = "Article update correctly sent to ARS server";
		logger.info(defaultMsg);
		String msg = labelsMap.get("article.update.success", defaultMsg);
		errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR, msg);
		logger.debug("End updateArticle");
		return errorObject;

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ncr.webfront.base.article.ArticleServiceInterface#getCapabilities()
	 */
	public Map<String, Boolean> getCapabilities() {
		return capabilities;

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ncr.webfront.base.article.ArticleServiceInterface#createArticle()
	 */
	public Article createArticle() {
		logger.debug("Begin createArticle");

		Article article = new Article();

		article.setArticleCode("");
		article.setPluCodes(new ArrayList<String>());

		if (posDepartmentService != null) {
			List<PosDepartment> tmpPosDepartmentList = posDepartmentService.getAll();
			if (tmpPosDepartmentList != null && tmpPosDepartmentList.size() > 0) {
				article.setPosDpt(tmpPosDepartmentList.get(0).getCode());
			}
		}

		article.setDescription("");

		if (vatService != null) {
			List<Vat> tmpVatList = vatService.getAll();
			if (tmpVatList != null && tmpVatList.size() > 0) {
				article.setVatId(tmpVatList.get(0).getVatId());
				int preferredVatId = 1;
				for (Vat currVat : tmpVatList) {
					if (currVat.getVatId() == preferredVatId) {
						article.setVatId(currVat.getVatId());
					}
				}
			}
		}

		article.setNegativePrice(false);
		article.setDeposit(false);
		article.setScalable(false);
		article.setDecimalQty(false);

		List<MutablePair<Integer, String>> tmpManualDiscountList = arsCommonListsServiceImplementation.getManualDiscountCodeList();
		if (tmpManualDiscountList != null && tmpManualDiscountList.size() > 0) {
			article.setManualDiscountId(tmpManualDiscountList.get(0).getLeft());
		}

		if (tareService != null) {
			List<Tare> tmpTareList = tareService.getAll();
			if (tmpTareList != null && tmpTareList.size() > 0) {
				article.setTareId(tmpTareList.get(0).getTareId());
			}
		}

		article.setMixId(0);

		List<MutablePair<String, String>> tmpPackagingTypeList = arsCommonListsServiceImplementation.getPackagingTypeList();
		if (tmpPackagingTypeList != null && tmpPackagingTypeList.size() > 0) {
			article.setPackagingType(tmpPackagingTypeList.get(0).getLeft());
		}

		article.setUnitsPerPackage(10);
		article.setLinkedItem("0000");

		List<MutablePair<Integer, String>> tmpPrizeCodeList = arsCommonListsServiceImplementation.getPrizeCodeList();
		if (tmpPrizeCodeList != null && tmpPrizeCodeList.size() > 0) {
			article.setPrizeCode(tmpPrizeCodeList.get(0).getLeft());
		}

		List<MutablePair<Integer, String>> tmpZeroPriceBehaviorList = arsCommonListsServiceImplementation.getZeroPriceBehaviourList();
		if (tmpZeroPriceBehaviorList != null && tmpZeroPriceBehaviorList.size() > 0) {
			article.setZeroPriceBehavior(tmpZeroPriceBehaviorList.get(0).getLeft());
		}

		article.setMembersOnly(false);

		List<MutablePair<Integer, String>> tmpAutoDiscountTypeList = arsCommonListsServiceImplementation.getAutoDiscountTypeList();
		if (tmpAutoDiscountTypeList != null && tmpAutoDiscountTypeList.size() > 0) {
			article.setAutomDiscountType(tmpAutoDiscountTypeList.get(0).getLeft());
			int preferredDiscountType = 3;
			for (MutablePair<Integer, String> currDiscountType : tmpAutoDiscountTypeList) {
				if (currDiscountType.getLeft() == preferredDiscountType) {
					article.setAutomDiscountType(currDiscountType.getLeft());
				}
			}
		}

		List<MutablePair<Integer, String>> tmpUpbOperationTypeList = arsCommonListsServiceImplementation.getUpbOperationTypeList();
		if (tmpUpbOperationTypeList != null && tmpUpbOperationTypeList.size() > 0) {
			article.setUpbOperationType(tmpUpbOperationTypeList.get(0).getLeft());
		}

		article.setUpbProviderId(0);
		article.setPrice(0);

		logger.debug("End createArticle");
		return article;

	}

	@Override
	/**
	 * returns validation fields list
	 */
	public List<WebFrontValidationData> getValidationData() {
		List<WebFrontValidationData> returnList = new ArrayList<WebFrontValidationData>();

		returnList.add(new WebFrontValidationData("articleCode").notBlank().length(1, 13));
		returnList.add(new WebFrontValidationData("description").notBlank().length(1, 20));
		returnList.add(new WebFrontValidationData("mixId").notEmpty().number().range(0, 99));
		returnList.add(new WebFrontValidationData("unitsPerPackage").notEmpty().number().range(0, 9999));
		returnList.add(new WebFrontValidationData("linkedItem").length(0, 4));
		returnList.add(new WebFrontValidationData("upbProviderId").notEmpty().number().range(0, 4));
		returnList.add(new WebFrontValidationData("price").notEmpty().number().range(0, 99999999));

		return returnList;
	}

	private void initialize(PosDepartmentServiceInterface posDepartmentService, VatServiceInterface vatService, TareServiceInterface tareService,
	      ArsPluRowTemplate arsPluRowTemplate) {
		logger.debug("Begin initialize");
		createCapabilities();
		this.arsCommonListsServiceImplementation = new ArsCommonListsServiceImplementation();
		this.posDepartmentService = posDepartmentService;
		this.vatService = vatService;
		this.tareService = tareService;
		this.arsPluRowTemplate = arsPluRowTemplate;

		arsArticleServiceProperties = PropertiesLoader.loadOrCreatePropertiesFile(ArsMappingProperties.arsPluginPropertiesFileName,
		      ArsMappingProperties.getArsPropertiesMap());
		try {
			updateOnHashFile = Boolean.parseBoolean(arsArticleServiceProperties.getProperty(ArsMappingProperties.arsUpdateOnHashFile));
		} catch (Exception e) {
			logger.warn("Error converting to boolean the property" + ArsMappingProperties.arsUpdateOnHashFile, e);
			updateOnHashFile = false;
		}
		try {
			maxSearchRowsNumber = Integer.parseInt(arsArticleServiceProperties.getProperty(ArsMappingProperties.arsMaximumSearchRowsNumber));
		} catch (Exception e) {
			logger.warn("Error converting to integer the property" + ArsMappingProperties.arsMaximumSearchRowsNumber, e);
			maxSearchRowsNumber = 1000;
		}

		this.gmRecMntActuator = new GmRecMntActuator(ArsServerEnvParameters.getInstance().getMntApplyMntSourceDir());
		this.pRegDatFileActuator = new ArsPRegDatFileActuator(ArsServerEnvParameters.getInstance().getServerHomeDir());

		eanXlines = (ArrayList<String>) pRegDatFileActuator.readLines("EANX");

		logger.info("updateOnHashFile    = " + updateOnHashFile);
		logger.info("maxSearchRowsNumber = " + maxSearchRowsNumber);

		logger.debug("End initialize");
	}

	private void createCapabilities() {
		logger.debug("Begin createCapabilities");

		capabilities = new HashMap<String, Boolean>();
		capabilities.put("articleCode", true);
		capabilities.put("pluCodes", false);
		capabilities.put("posDpt", true);
		capabilities.put("description", true);
		capabilities.put("vatId", true);
		capabilities.put("negativePrice", true);
		capabilities.put("deposit", true);
		capabilities.put("scalable", true);
		capabilities.put("decimalQty", true);
		capabilities.put("manualDiscountId", true);
		capabilities.put("tareId", true);
		capabilities.put("mixId", true);
		capabilities.put("packagingType", true);
		capabilities.put("unitsPerPackage", true);
		capabilities.put("linkedItem", true);
		capabilities.put("prizeCode", true);
		capabilities.put("zeroPriceBehavior", true);
		capabilities.put("membersOnly", true);
		capabilities.put("automDiscountType", true);
		capabilities.put("upbOperationType", true);
		capabilities.put("upbProviderId", true);
		capabilities.put("price", true);

		logger.debug("End createCapabilities");
	}

	/**
	 * 
	 * @param articleToManage
	 * @return String containing hash file line
	 */
	private String createArsPluRecordFromArticleObject(Article articleToManage) {
		logger.debug("Begin createArsPluRecordFromArticleObject");
		StringBuilder pluRecord = new StringBuilder();

		// filler1
		pluRecord.append("   ");

		// article code
		pluRecord.append(StringUtils.leftPad(articleToManage.getArticleCode(), 13));

		// department
		pluRecord.append(articleToManage.getPosDpt());

		// code1
		StringBuilder code = new StringBuilder();
		code.append(articleToManage.isNegativePrice() ? '1' : '0');
		code.append(articleToManage.isDeposit() ? '1' : '0');
		code.append(articleToManage.isScalable() ? '1' : '0');
		code.append('0');

		pluRecord.append(Integer.toHexString(Integer.parseInt(code.toString(), 2)));

		// code2
		pluRecord.append(articleToManage.isDecimalQty() ? '1' : '0');

		// discount code
		pluRecord.append(articleToManage.getManualDiscountId());

		// vat id
		pluRecord.append(articleToManage.getVatId());

		// code3
		if (articleToManage.isScalable()) {
			pluRecord.append(StringUtils.leftPad(articleToManage.getTareId() + "", 2, '0'));
		} else {
			pluRecord.append(StringUtils.leftPad(articleToManage.getMixId() + "", 2, '0'));
		}

		// pack type
		pluRecord.append(articleToManage.getPackagingType());

		// pack unit
		pluRecord.append(StringUtils.leftPad(articleToManage.getUnitsPerPackage() + "", 4, '0'));

		// deposit link
		pluRecord.append(StringUtils.leftPad(articleToManage.getLinkedItem() + "", 4, '0'));

		// description
		pluRecord.append(StringUtils.rightPad(articleToManage.getDescription(), 20));

		// prizeCod AKA prize item type
		pluRecord.append(articleToManage.getPrizeCode());

		// zero price
		pluRecord.append(articleToManage.getZeroPriceBehavior());

		// members only
		pluRecord.append(articleToManage.isMembersOnly() ? '1' : '0');

		// automatic discount type AKA dp discount type
		pluRecord.append(articleToManage.getAutomDiscountType());

		// upb operation type
		pluRecord.append(articleToManage.getUpbOperationType());

		// upb provider id
		pluRecord.append(articleToManage.getUpbProviderId());

		// filler (taken from default plu line defined in properties file)
		pluRecord.append(arsPluRowTemplate.getFiller2(arsArticleServiceProperties.getProperty(ArsMappingProperties.arsDefaultPluLine)));

		// price
		pluRecord.append(StringUtils.leftPad(articleToManage.getPrice() + "", 8, '0'));

		logger.debug("pluRecord.toString() =\"" + pluRecord.toString() + "\"");
		logger.debug("End createArsPluRecordFromArticleObject");
		return pluRecord.toString();
	}

	/**
	 * 
	 * @param line
	 * @return
	 */
	private Article createArticleFromArsPluRecord(String line, Integer priceFromBarcode) {
		logger.debug("Begin createArticleFromArsPluRecord");

		if (line.length() != 78) {
			logger.error("ERRORE!!! Dimensione errata riga file Hash ");
			return null;
		}

		// CREATE returnArticle with default values
		Article returnArticle = createArticle();

		// article code
		returnArticle.setArticleCode(arsPluRowTemplate.getArticleCode(line).trim());
		if (returnArticle.getArticleCode().equals("")) {
			logger.error("ERRORE!!! Codice Articolo Vuoto");
			return null;
		}

		// plu code not use in ars
		returnArticle.setPluCodes(null);

		// department
		String tmpDpt = arsPluRowTemplate.getPosDpt(line);
		if (posDepartmentService == null || posDepartmentService.getByCode(tmpDpt) != null) {
			returnArticle.setPosDpt(tmpDpt);
		} else {
			logger.warn("WARNING!!! Per l'articolo " + returnArticle.getArticleCode() + " verrà impostato il Reparto di Default");
		}

		// code1 parsing
		int code1 = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getCode1(line), 0, 16, logger, "code1");
		returnArticle.setNegativePrice((code1 & 8) == 8);
		returnArticle.setDeposit((code1 & 4) == 4);
		returnArticle.setScalable((code1 & 2) == 2);

		// code2 parsing
		int code2 = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getCode2(line), 0, 16, logger, "code2");
		returnArticle.setDecimalQty((code2 & 1) == 1);

		// discount code
		int tmpDiscountCode = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getManualDiscountId(line), returnArticle.getManualDiscountId(), logger,
		      "discount code");
		if (arsCommonListsServiceImplementation.getManualDiscountCodeById(tmpDiscountCode) != null) {
			returnArticle.setManualDiscountId(tmpDiscountCode);
		}

		// vat id
		int tmpVatId = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getVatId(line), returnArticle.getVatId(), logger, "vat id");
		if (vatService == null || vatService.getVatById(tmpVatId) != null) {
			returnArticle.setVatId(tmpVatId);
		} else {
			logger.warn("WARNING!!! Per l'articolo " + returnArticle.getArticleCode() + " verrà impostata il VatId di Default");
		}

		// code3 parsing
		String code3 = arsPluRowTemplate.getCode3(line);
		if (returnArticle.isScalable()) {
			int tmpTareId = ArsCommonUtils.parseIntDefault(code3, returnArticle.getTareId(), logger, "code3");
			if (tareService == null || tareService.getTareById(tmpTareId) != null) {
				returnArticle.setTareId(tmpTareId);
			}
		} else {
			returnArticle.setMixId(ArsCommonUtils.parseIntDefault(code3, returnArticle.getMixId(), logger, "mix code"));
		}

		// pack type
		String tmpPackagingType = arsPluRowTemplate.getPackagingType(line);
		if (arsCommonListsServiceImplementation.getPackagingTypeById(tmpPackagingType) != null) {
			returnArticle.setPackagingType(tmpPackagingType);
		}

		// pack unit
		returnArticle.setUnitsPerPackage(ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getUnitsPerPackage(line), returnArticle.getUnitsPerPackage(), logger,
		      "packing unit"));

		// deposit link
		returnArticle.setLinkedItem(StringUtils.strip(arsPluRowTemplate.getLinkedItem(line)));

		// description
		returnArticle.setDescription(StringUtils.strip(arsPluRowTemplate.getDescription(line)));

		// prizeCod AKA prize item type
		int tmpPrizecode = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getPrizeCode(line), returnArticle.getPrizeCode(), logger, "prize code");
		if (arsCommonListsServiceImplementation.getPrizeCodeById(tmpPrizecode) != null) {
			returnArticle.setPrizeCode(tmpPrizecode);
		}

		// zero price
		int tmpZeroPriceBehavior = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getZeroPriceBehavior(line), 0, logger, "zero price");
		if (arsCommonListsServiceImplementation.getZeroPriceBehaviourById(tmpZeroPriceBehavior) != null) {
			returnArticle.setZeroPriceBehavior(tmpZeroPriceBehavior);
		}

		// members only
		returnArticle.setMembersOnly(Boolean.parseBoolean(arsPluRowTemplate.getMembersOnly(line)));

		// automatic discount type AKA dp discount type
		int tmpDiscountType = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getAutomDiscountType(line), returnArticle.getAutomDiscountType(), logger,
		      "dp discount type");
		if (arsCommonListsServiceImplementation.getAutoDiscountTypeById(tmpDiscountType) != null) {
			returnArticle.setAutomDiscountType(tmpDiscountType);
		}

		// upb operation type
		int tmpUpbOperationType = ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getUpbOperationType(line), returnArticle.getUpbOperationType(), logger,
		      "upb operation type");
		if (arsCommonListsServiceImplementation.getUpbOperationTypeById(tmpUpbOperationType) != null) {
			returnArticle.setUpbOperationType(tmpUpbOperationType);
		}

		// upb provider id
		returnArticle.setUpbProviderId(ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getUpbProviderId(line), returnArticle.getUpbProviderId(), logger,
		      "upb provider id"));

		// filler (not used yet)

		// ticket (not used yet)

		// code4 (not used yet)

		// price
		returnArticle.setPrice(ArsCommonUtils.parseIntDefault(arsPluRowTemplate.getPrice(line), returnArticle.getPrice(), logger, "price"));

		if (returnArticle.isScalable()) {
			returnArticle.setScalableExtendedPrice(priceFromBarcode);
			returnArticle.setScalableUnitPrice(returnArticle.getPrice());
			if (priceFromBarcode !=  null && returnArticle.getScalableUnitPrice() != 0) {
				returnArticle.setScalableWeight((returnArticle.getScalableExtendedPrice() * 1000) / returnArticle.getScalableUnitPrice());
			}
		}
		logger.debug("End createArticleFromArsPluRecord");
		return returnArticle;

	}

	private LanguageServiceInterface languageService;
	private LabelsMap labelsMap;

	private final String ARS_SERVER_PLU_HASH_FILE_NAME = "M_HSHPLU.DAT";

	private ArsCommonListsServiceImplementation arsCommonListsServiceImplementation;
	private String pluHashFileFullPath;
	private PosDepartmentServiceInterface posDepartmentService;
	private VatServiceInterface vatService;
	private TareServiceInterface tareService;
	private ArsPluRowTemplate arsPluRowTemplate;
	private HashMap<String, Boolean> capabilities = null;
	private Properties arsArticleServiceProperties;
	private ArsPluHashFileActuatorInterface arsPluHashFileActuator;
	private boolean updateOnHashFile;
	private int maxSearchRowsNumber;
	private GmRecMntActuator gmRecMntActuator;
	private ArsPRegDatFileActuator pRegDatFileActuator;
	private ArrayList<String> eanXlines;
	private Logger logger = WebFrontLogger.getLogger(ArsArticleServiceImplementation.class);

}
