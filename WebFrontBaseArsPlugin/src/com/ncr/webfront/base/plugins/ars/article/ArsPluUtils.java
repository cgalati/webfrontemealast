package com.ncr.webfront.base.plugins.ars.article;

import java.util.List;
import java.util.regex.Pattern;

public class ArsPluUtils {

	public static boolean isPriceEmbedded(List<String> eanXlines, String articleCode) {
		if (ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode).isEmpty()) {
			return false;
		}
		return true;
	}

	public static String getPriceEmbeddedPattern(List<String> eanXlines, String articleCode) {

		for (String line : eanXlines) {
			// split lines in 2 parts to get "P$0:" fields
			String cleanLine = line.substring(6, 26);
			if (cleanLine.startsWith("P$0:")) {
				String pattern = cleanLine.substring(4).trim();
				String regExp = cleanLine.substring(4).trim().replaceAll("[^0-9]", ".");
				if (Pattern.matches(regExp, articleCode)) {
					return pattern;
				}
			}
			cleanLine = line.substring(26, 46);
			if (cleanLine.startsWith("P$0:")) {
				String pattern = cleanLine.substring(4).trim();
				String regExp = cleanLine.substring(4).trim().replaceAll("[^0-9]", ".");
				if (Pattern.matches(regExp, articleCode)) {
					return pattern;
				}
			}
		}
		return "";
	}

	public static int getEmbeddedPrice(List<String> eanXlines, String articleCode) {
		String pattern = ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode);
		if (pattern.isEmpty()) {
			return 0;
		}

		int beginIndex = pattern.indexOf("-");
		int endIndex = pattern.lastIndexOf("-");
		if (beginIndex < 0 || endIndex < 0 || endIndex >= articleCode.length()) {
			return 0;
		}
		String priceString = articleCode.substring(beginIndex, endIndex + 1);
		try {
			int price = Integer.parseInt(priceString);
			return price;
		} catch (Exception e) {
			return 0;
		}
	}

	public static String clearEmbeddedPrice(List<String> eanXlines, String articleCode) {
		String pattern = ArsPluUtils.getPriceEmbeddedPattern(eanXlines, articleCode);
		if (pattern.isEmpty()) {
			return null;
		}
		int beginIndex = pattern.indexOf("-");
		int endIndex = pattern.lastIndexOf("-");
		if (beginIndex < 0 || endIndex < 0 || endIndex >= articleCode.length()) {
			return null;
		}
		String zerosString = "000000000000000000000000000000000";
		zerosString = zerosString.substring(beginIndex, endIndex + 1);
		articleCode = articleCode.substring(0, beginIndex) + zerosString + articleCode.substring(endIndex, articleCode.length() - 1);
		articleCode = ArsPluUtils.appendCheckDigit(articleCode);
		return articleCode;
	}

	public static String appendCheckDigit(String data) {
		data = data.substring(0, data.length() - 1);
		return data + (10 - cdg_check(data + 0, "31313131313131313131", 10)) % 10;
	}

	/**
	 * **************************************************************** check
	 * digit verification
	 * *****************************************************************
	 */
	private static int cdg_check(String data, String weight, int mod) {
		int cdg = 0, ind = data.length();

		for (int pos = weight.length() - ind; ind-- > 0;) {
			cdg += (data.charAt(ind) & 15) * (weight.charAt(pos + ind) & 15);
		}
		return cdg % mod;
	}

}
