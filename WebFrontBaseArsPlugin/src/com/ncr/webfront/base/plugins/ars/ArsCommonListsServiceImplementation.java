package com.ncr.webfront.base.plugins.ars;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.commonlists.CommonListsServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ArsCommonListsServiceImplementation implements CommonListsServiceInterface {

	public ArsCommonListsServiceImplementation() {
		manualDiscountCodeList = new ArrayList<MutablePair<Integer,String>>();
		tareList = new ArrayList<MutablePair<Integer,String>>();
		packagingTypeList = new ArrayList<MutablePair<String,String>>();
		prizeCodeList = new ArrayList<MutablePair<Integer,String>>();
		zeroPriceBehaviourList = new ArrayList<MutablePair<Integer,String>>();
		autoDiscountTypeList = new ArrayList<MutablePair<Integer,String>>();
		upbOperationTypeList = new ArrayList<MutablePair<Integer,String>>();

		//TODO Read from P_REGPAR.DAT/S_REGXXX.ORG
		add(manualDiscountCodeList, 0, "No manual discount");
		add(manualDiscountCodeList, 1, "Manual discount 1");
		add(manualDiscountCodeList, 2, "Manual discount 2");
		add(manualDiscountCodeList, 3, "Manual discount 3");
		add(manualDiscountCodeList, 4, "Manual discount 4");
		add(manualDiscountCodeList, 5, "Manual discount 5");
		add(manualDiscountCodeList, 6, "Manual discount 6");
		add(manualDiscountCodeList, 7, "Manual discount 7");
		add(manualDiscountCodeList, 8, "Manual discount 8");
		add(manualDiscountCodeList, 9, "Manual discount 9");
		
		//TODO Read from P_REGPAR.DAT/S_REGXXX.ORG
		add(tareList, 0, "No tare");
		add(tareList, 1, "Tare 1");
		add(tareList, 2, "Tare 2");
		add(tareList, 3, "Tare 3");
		add(tareList, 4, "Tare 4");
		add(tareList, 5, "Tare 5");
		add(tareList, 6, "Tare 6");
		add(tareList, 7, "Tare 7");
		add(tareList, 8, "Tare 8");
		add(tareList, 9, "Tare 9");
		
		add(packagingTypeList, "PZ", "Pieces");
		add(packagingTypeList, "KG", "Kilograms");
		add(packagingTypeList, "UN", "Unit ");
		
		add(prizeCodeList, 0, "Normal article");
		add(prizeCodeList, 1, "Premium not salable");
		add(prizeCodeList, 2, "Premium salable");

		add(zeroPriceBehaviourList, 0, "Price different to sero obligatory");
		add(zeroPriceBehaviourList, 1, "Permit sales to zero price");
		
		add(autoDiscountTypeList, 0, "No discount - No points");
		add(autoDiscountTypeList, 1, "Yes discount - No points");
		add(autoDiscountTypeList, 2, "No discount - Yes points");
		add(autoDiscountTypeList, 3, "Yes discount - Yes points");
		
		add(upbOperationTypeList, 0, "0");
		add(upbOperationTypeList, 1, "1");
		add(upbOperationTypeList, 2, "2");
		add(upbOperationTypeList,0, "0 - UPB Disaabled");
		add(upbOperationTypeList,1, "1 - Recharge Online");
		add(upbOperationTypeList,2, "2 - Recharge Offline");
		add(upbOperationTypeList,3, "3 - Services Online");
		add(upbOperationTypeList,4, "4 - ");
		add(upbOperationTypeList,5, "5 - Gift Card");
		add(upbOperationTypeList,6, "6 - ");
		add(upbOperationTypeList,7, "7 - ");
		add(upbOperationTypeList,8, "8 - ");
		add(upbOperationTypeList,9, "9 - ");
	}
	
	
	@Override
	public List<MutablePair<Integer, String>> getManualDiscountCodeList() {
		return manualDiscountCodeList;
	}

	@Override
	public ErrorObject checkService() {
		try {
			//TODO Activate together with P_REGPAR.DAT/S_REGXXX.ORG reading
//			String regFilePath = "";
//			checkIfFileExists(regFilePath);
		} catch (Exception e) {
			logger.error("", e);
			
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	/*
	private void checkIfFileExists(String path) throws Exception {
		if (!new File(path).exists()) {
			throw new Exception(String.format("File %s inesistente", path));
		}
	}
	*/
	@Override
	public MutablePair<Integer, String> getManualDiscountCodeById(int idManualDiscountCode) {
		try {
			return manualDiscountCodeList.get(idManualDiscountCode);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public List<MutablePair<String, String>> getPackagingTypeList() {
		return packagingTypeList;
	}

	@Override
	public MutablePair<String, String> getPackagingTypeById(String idPackagingType) {
		try {
			for (MutablePair<String, String>packagingType : packagingTypeList){
				if (packagingType.getLeft().equals(idPackagingType)){
					return packagingType;
				}
			}
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
		return null;
	}

	@Override
	public List<MutablePair<Integer, String>> getPrizeCodeList() {
		return prizeCodeList;
	}

	@Override
	public MutablePair<Integer, String> getPrizeCodeById(int idPrizeCode) {
		try {
			return prizeCodeList.get(idPrizeCode);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public List<MutablePair<Integer, String>> getZeroPriceBehaviourList() {
		return zeroPriceBehaviourList;
	}

	@Override
	public MutablePair<Integer, String> getZeroPriceBehaviourById(int idZeroPriceBehaviour) {
		try {
			return zeroPriceBehaviourList.get(idZeroPriceBehaviour);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public List<MutablePair<Integer, String>> getAutoDiscountTypeList() {
		return autoDiscountTypeList;
	}

	@Override
	public MutablePair<Integer, String> getAutoDiscountTypeById(int idAutoDiscountType) {
		try {
			return autoDiscountTypeList.get(idAutoDiscountType);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public List<MutablePair<Integer, String>> getUpbOperationTypeList() {
		return upbOperationTypeList;
	}

	@Override
	public MutablePair<Integer, String> getUpbOperationTypeById(int idUpbOperationType) {
		try {
			return upbOperationTypeList.get(idUpbOperationType);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	private static void add(List<MutablePair<Integer, String>> list, int id, String description) {
		list.add(MutablePair.of(id, description));
	}
	
	private static void add(List<MutablePair<String, String>> list, String id, String description) {
		list.add(MutablePair.of(id, description));
	}

	private List<MutablePair<Integer, String>> manualDiscountCodeList;
	private List<MutablePair<Integer, String>> tareList;
	private List<MutablePair<String, String>> packagingTypeList;
	private List<MutablePair<Integer, String>> prizeCodeList;
	private List<MutablePair<Integer, String>> zeroPriceBehaviourList;
	private List<MutablePair<Integer, String>> autoDiscountTypeList;
	private List<MutablePair<Integer, String>> upbOperationTypeList;
	
	private static final Logger logger = WebFrontLogger.getLogger(ArsCommonListsServiceImplementation.class);
}
