package com.ncr.webfront.base.plugins.ars.eod;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.eod.PosStatusObject;
import com.ncr.webfront.base.plugins.ars.ArsMLanFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsPosStatusObjectInterpreter;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.posterminal.PosTerminalActionCode;
import com.ncr.webfront.base.posterminal.PosTerminalType;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ArsEodServiceImplementation extends EodServiceAbstractImplementation {
	@Override
	public ErrorObject checkService() {
		try {
			getEodStatusHistory();

			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsPosDescriptionData);
			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsPosTerminalTypeData);
			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsPosStatusData);
			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsDefectiveDeclarableData);
			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsReadyPosEodData);
			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsReadyServerEodData);
			checkIfFileExists(ArsMappingProperties.getInstance(), ArsMappingProperties.arsPosStatusDescriptionData);
			// checkIfFileExists(ArsMappingProperties.getInstance().getMlanPath());
		} catch (Exception e) {
			logger.error("", e);

			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}

		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	private void checkIfFileExists(ArsMappingProperties properties, String key) throws Exception {
		String path = ArsMappingProperties.getInstance().getProperty(key, true);

		checkIfFileExists(path);
	}

	private void checkIfFileExists(String path) throws Exception {
		if (!new File(path).exists()) {
			throw new Exception(String.format("File already existing", path));
		}
	}

	@Override
	public List<PosStatusObject> getAllPosStatus() {
		List<PosStatusObject> posStatuses = new ArrayList<PosStatusObject>();

		int rec = 0;
		try {
			while (true) {
				StringBuffer message = new StringBuffer();

				message.append("R:L").append(String.format("%04d", ++rec)).append(":")
						.append(ArsServerEnvParameters.getInstance().getSupervisorNumberForServerEod()).append(':').append(EMPTY);

				logger.debug(String.format("message [%s]", message));
				String answer = sendMessage(message.toString()); // CGA

				if (answer.toString().trim().length() == 12 + EMPTY.length()) {
					String posTerminalLine = answer.toString().substring(12);
					PosStatusObject object = ArsPosStatusObjectInterpreter.interpret(posTerminalLine);
					if (object.getType().equals(PosTerminalType.REGISTER)) {
						posStatuses.add(object);
					}
				} else {
					break;
				}
			}
		} catch (Exception e) {
			logger.error("Exception!, e");
		}
		if (!checkPosStatusAgainstMLan(posStatuses)) {
			logger.warn("checkPosStatusAgainstMLan returned false!");
			posStatuses.clear();
		} else {
			logger.debug("checkPosStatusAgainstMLan returned true");
		}
		logger.debug("END (posStatuses.size() == " + posStatuses.size() + ")");
		return posStatuses;
	}

	private boolean checkPosStatusAgainstMLan(List<PosStatusObject> posStatuses) {
		logger.debug("BEGIN");
		if (posStatuses.size() == 0) {
			logger.warn("END (false because posStatuses.size() == 0)");
			return false;
		}

		String mlanFilePath = ArsMappingProperties.getInstance().getMlanPath();
		String serverNumber = ArsServerEnvParameters.getInstance().getServerNumber();
		ArsMLanFileActuator mlanActuator = new ArsMLanFileActuator(mlanFilePath, serverNumber);
		List<String> listMlanTerminals = mlanActuator.readAllPosTerminals();
		if (listMlanTerminals.size() != posStatuses.size()) {
			dumpMlanList("checkPosStatusAgainstMLan", listMlanTerminals);
			dumpPosStatusList("checkPosStatusAgainstMLan", posStatuses);
			logger.warn("END (false because listMlanTerminals.size() != posStatuses.size())");
			return false;
		}
		for (PosStatusObject posStatusObject : posStatuses) {
			boolean found = false;
			for (String mlanRow : listMlanTerminals) {
				if (mlanRow.length() > 6 && mlanRow.startsWith("R")) {
					String mlanRowTerminalCode = mlanRow.substring(2, 2 + 3);
					logger.debug("mlanRow                           = \"" + mlanRow + "\"");
					logger.debug("mlanRowTerminalCode               = \"" + mlanRowTerminalCode + "\"");
					logger.debug("posStatusObject.getTerminalCode() = \"" + posStatusObject.getTerminalCode() + "\"");
					if (mlanRowTerminalCode.equals(posStatusObject.getTerminalCode())) {
						found = true;
					}
				}

			}
			if (!found) {
				dumpMlanList("checkPosStatusAgainstMLan", listMlanTerminals);
				dumpPosStatusList("checkPosStatusAgainstMLan", posStatuses);
				logger.warn("END (false because element to element compare failed)");
				return false;
			}
		}
		logger.debug("END (true)");
		return true;
	}

	private void dumpMlanList(String callerName, List<String> mlanTerminalList) {
		logger.debug("--------- BEGIN dumpMlanList (" + callerName + ") ---------");
		if (mlanTerminalList == null) {
			logger.debug("mlanTerminalList == null");
			logger.debug("--------- END   dumpMlanList (" + callerName + ") ---------");
			return;
		}
		if (mlanTerminalList.size() == 0) {
			logger.debug("mlanTerminalList.size() == 0");
			logger.debug("--------- END   dumpMlanList (" + callerName + ") ---------");
			return;
		}

		for (String mlanRow : mlanTerminalList) {
			logger.debug(mlanRow);
		}
		logger.debug("--------- END   dumpMlanList (" + callerName + ") ---------");
	}

	private void dumpPosStatusList(String callerName, List<PosStatusObject> posStatusList) {
		logger.debug("--------- BEGIN dumpPosStatusList (" + callerName + ") ---------");
		if (posStatusList == null) {
			logger.debug("posStatusList == null");
			logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
			return;
		}
		if (posStatusList.size() == 0) {
			logger.debug("posStatusList.size() == 0");
			logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
			return;
		}

		for (PosStatusObject posStatusObject : posStatusList) {
			logger.debug("Terminal code: " + posStatusObject.getTerminalCode() + " - isDefectiveDeclarable: " + posStatusObject.isDefectiveDeclarable()
					+ " - isReadyForPosEndOfDay: " + posStatusObject.isReadyForPosEndOfDay() + " - isReadyForServerEndOfDay: "
					+ posStatusObject.isReadyForServerEndOfDay() + " - ActionCode: " + posStatusObject.getActionCode() + " - ActionCodeDescription: "
					+ posStatusObject.getActionCodeDescription() + " - PosStatus: " + posStatusObject.getPosStatus());
		}
		logger.debug("--------- END   dumpPosStatusList (" + callerName + ") ---------");
	}

	@Override
	public PosStatusObject getPosStatus(String posCode) {

		String mlanFilePath = ArsMappingProperties.getInstance().getMlanPath();
		String serverNumber = ArsServerEnvParameters.getInstance().getServerNumber();
		ArsMLanFileActuator mlanActuator = new ArsMLanFileActuator(mlanFilePath, serverNumber);

		// if posCode is "SRV" search for Server data
		if (posCode.equals("SRV")) {
			posCode = serverNumber;
		}

		return ArsPosStatusObjectInterpreter.interpret(mlanActuator.readLine(posCode));
	}

	@Override
	public ErrorObject declareDefective(String posCode) {
		StringBuffer message = new StringBuffer();

		message.append("E:").append(posCode).append(":").append(ArsServerEnvParameters.getInstance().getSupervisorNumberForServerEod()).append(':')
				.append(PosTerminalActionCode.AC_DEFECTIVE_TERMINAL);

		logger.debug(String.format("message [%s]", message));

		return send(message.toString());
	}

	@Override
	public ErrorObject launchEodAllPos() {
		logger.debug("BEGIN");
		ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR);

		try {
			List<PosStatusObject> posStatuses = getAllPosStatus();

			// ALBTEMP TEST ONLY BEGIN
			String testOnlyFlagFilePath = "/home/NCR/webfront/TESTONLY_SIMULATEOD_POSSTATUSKO.FLG";
			File file = new File(testOnlyFlagFilePath);
			if (file.exists()) {
				logger.warn("TESTONLY - IL FLAG FILE \"" + testOnlyFlagFilePath + "\" ESISTE -> SIMULO LISTA POSSTATUS VUOTA");
				posStatuses = new ArrayList<PosStatusObject>();
				logger.warn("posStatuses = new ArrayList<PosStatusObject>()");
			}
			// ALBTEMP TEST ONLY END

			if (posStatuses == null || posStatuses.size() == 0) {
				logger.error("Error - getAllPosStatus null or empty (means cannot get all POS statuses from server)");
				errorObject = new ErrorObject(ErrorCode.COMMUNICATION_ERROR, "Cannot get POS statuses from server");
				logger.debug("END (" + errorObject.getErrorCode() + ")");
				return errorObject;
			}

			String clientToRunEod = ArsServerEnvParameters.getInstance().getClientNumberForServerEod();
			List<String> commandsToSend = new ArrayList<>();

			logger.debug(String.format("[%d] pos terminals", posStatuses.size()));

			for (PosStatusObject posStatus : posStatuses) {
				String command = launchEodAllPosBuildCommandFor(posStatus, clientToRunEod);

				if (command != null) {
					commandsToSend.add(command);
				}
			}

			logger.debug("commandsToSend built");

			errorObject = send(commandsToSend.toArray(new String[commandsToSend.size()]));
		} catch (Exception e) {
			logger.error("", e);
			errorObject = new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
		}

		logger.debug("END (" + errorObject.getErrorCode() + ")");
		return errorObject;
	}

	private String launchEodAllPosBuildCommandFor(PosStatusObject posStatus, String clientToRunEod) {
		if (!posStatus.getTerminalCode().equals(clientToRunEod)) {
			PosTerminalActionCode payload;

			if (!posStatus.getOperatorCode().equals(ArsPosStatusObjectInterpreter.NO_OPERATOR)) {
				payload = PosTerminalActionCode.AC_28;
			} else if (posStatus.getStatusCode().equals(ArsPosStatusObjectInterpreter.POS_ONLINE)) {
				payload = PosTerminalActionCode.AC_28;
			} else {
				payload = PosTerminalActionCode.AC_ZEROED_TERMINAL;
			}

			return launchEodAllPosBuildCommandFor(posStatus.getTerminalCode(), payload);
		} else {
			return null;
		}
	}

	private String launchEodAllPosBuildCommandFor(String posCode, PosTerminalActionCode payload) {
		StringBuffer message = new StringBuffer();
		String fourthDigit = ArsServerEnvParameters.getInstance().isForDigitsChecker() ? "9" : "";
		message.append("E:").append(posCode).append(":").append(fourthDigit).append(ArsServerEnvParameters.getInstance().getSupervisorNumberForServerEod()).append(":")
				.append(payload);

		logger.debug(String.format("message [%s]", message));

		return message.toString();
	}

	/*
	 * Only one communication at a time: this ensures that no new communication
	 * can disrupt any running one.
	 */
	private synchronized ErrorObject send(String... messages) {
		ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR);
		ArsServerEnvParameters arsServerEnvParameters = ArsServerEnvParameters.getInstance();

		try {
			InetAddress serverName = InetAddress.getByName("SRV" + arsServerEnvParameters.getServerNumber());
			int serverPort = 20000 + Integer.parseInt(arsServerEnvParameters.getServerNumber());
			int localPort = 20000 + Integer.parseInt(arsServerEnvParameters.getClientNumberForServerEod());
			if (serverName == null) {
				logger.error("serverName == null!");
			} else {
				logger.debug("serverName.getHostAddress() = \"" + serverName.getHostAddress() + "\"");
				logger.debug("serverName.getHostName()    = \"" + serverName.getHostName() + "\"");
			}
			logger.debug("serverPort                  = \"" + serverPort + "\"");
			logger.debug("localPort                   = \"" + localPort + "\"");
			logger.debug("InetAddress.getLocalHost()  = \"" + InetAddress.getLocalHost() + "\"");

			Socket socket = null;
			try {
				logger.debug("Before socket = new Socket(" + serverName + ", " + serverPort + ", " + InetAddress.getLocalHost() + ", " + localPort + ");");
				socket = new Socket(serverName, serverPort, InetAddress.getLocalHost(), localPort);
				logger.debug("After  socket = new Socket(" + serverName + ", " + serverPort + ", " + InetAddress.getLocalHost() + ", " + localPort + ");");
				socket.setSoTimeout(8000);
				socket.setSoLinger(true, 0);

				sendAnswer(socket, messages);
			} catch (Exception e) {
				logger.error("", e);
			} finally {
				try {
					if (socket != null) {
						logger.debug("Calling clientSocket.close()..");
						socket.close();
					}
					logger.debug("Calling clientSocket = null..");
					socket = null;

				} catch (Exception e) {
					logger.error("finally", e);
				}
			}

		} catch (Exception e) {
			logger.error("", e);

			errorObject = new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
		}

		return errorObject;
	}

	// CGA
	private synchronized String sendMessage(String... messages) {
		ErrorObject errorObject = new ErrorObject(ErrorCode.OK_NO_ERROR);
		ArsServerEnvParameters arsServerEnvParameters = ArsServerEnvParameters.getInstance();
		String answer = "";

		try {
			InetAddress serverName = InetAddress.getByName("SRV" + arsServerEnvParameters.getServerNumber());
			int serverPort = 20000 + Integer.parseInt(arsServerEnvParameters.getServerNumber());
			int localPort = 20000 + Integer.parseInt(arsServerEnvParameters.getClientNumberForServerEod());
			if (serverName == null) {
				logger.error("serverName == null!");
			} else {
				logger.debug("serverName.getHostAddress() = \"" + serverName.getHostAddress() + "\"");
				logger.debug("serverName.getHostName()    = \"" + serverName.getHostName() + "\"");
			}
			logger.debug("serverPort                  = \"" + serverPort + "\"");
			logger.debug("localPort                   = \"" + localPort + "\"");
			logger.debug("InetAddress.getLocalHost()  = \"" + InetAddress.getLocalHost() + "\"");

			Socket socket = null;
			try {
				logger.debug("Before socket = new Socket(" + serverName + ", " + serverPort + ", " + InetAddress.getLocalHost() + ", " + localPort + ");");
				socket = new Socket(serverName, serverPort, InetAddress.getLocalHost(), localPort);
				logger.debug("After  socket = new Socket(" + serverName + ", " + serverPort + ", " + InetAddress.getLocalHost() + ", " + localPort + ");");
				socket.setSoTimeout(8000);
				socket.setSoLinger(true, 0);

				answer = sendAnswer(socket, messages);
			} catch (Exception e) {
				logger.error("", e);
			} finally {
				try {
					if (socket != null) {
						logger.debug("Calling clientSocket.close()..");
						socket.close();
					}
					logger.debug("Calling clientSocket = null..");
					socket = null;

				} catch (Exception e) {
					logger.error("finally", e);
				}
			}

		} catch (Exception e) {
			logger.error("", e);

			errorObject = new ErrorObject(ErrorCode.GENERIC_ERROR, e.getMessage());
			answer = errorObject.toString();
		}

		return answer;
	}

	// private void send(Socket socket, String... messages) throws Exception {
	private String sendAnswer(Socket socket, String... messages) throws Exception {
		InputStream in = socket.getInputStream();
		OutputStream out = socket.getOutputStream();
		String answer = ""; // CGA

		hello(socket, in, out);

		for (String message : messages) {
			byte[] bytes = message.getBytes(CHARSET);
			int outLength = 0;

			out.write(bytes);
			out.flush();
			outLength = bytes.length;

			logger.debug(String.format("outLength [%d]", outLength));

			if ((outLength != in.read(bytes)) && (message.charAt(2) != 'D')) {
				throw new SocketException("Dimensione invalida per la risposta.");
			}

			logger.debug(String.format("response [%s]", new String(bytes, CHARSET)));
			answer = new String(bytes, CHARSET); // CGA
		}

		return answer;// CGA
	}

	private void hello(Socket socket, InputStream in, OutputStream out) throws Exception {
		String clientToRunEod = ArsServerEnvParameters.getInstance().getClientNumberForServerEod();
		byte[] bytes = new byte[SOCKET_READ_BUFFER_SIZE];

		logger.debug("sending hello...");

		out.write(String.format("!:REG%s:0090:0000:00000000:00000000:00000000:00000000:00000000:00000000", clientToRunEod).getBytes(CHARSET));
		out.flush();

		logger.debug("hello sent");

		if (in.read(bytes) < 1 || bytes[0] != 0x21) {
			throw new SocketException("No answer");
		}

		logger.debug("hello received");
	}

	private static Logger logger = WebFrontLogger.getLogger(ArsEodServiceImplementation.class);
	//                                  0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
	//                                  0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567
	public static final String EMPTY = "                                                                                                                                                                                                                ";
}
