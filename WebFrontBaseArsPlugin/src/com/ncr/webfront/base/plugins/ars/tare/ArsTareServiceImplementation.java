package com.ncr.webfront.base.plugins.ars.tare;

import java.util.List;

import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.tare.Tare;
import com.ncr.webfront.base.tare.TareServiceInterface;

public class ArsTareServiceImplementation implements TareServiceInterface {

	@Override
	public ErrorObject checkService() {
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
	
	@Override
	public List<Tare> getAll() {
		//TODO Read from P_REGPAR.DAT/S_REGXXX.ORG
		return null;
	}

	@Override
	public Tare getTareById(int tareId) {
		//TODO Read from P_REGPAR.DAT/S_REGXXX.ORG
		return null;
	}

}
