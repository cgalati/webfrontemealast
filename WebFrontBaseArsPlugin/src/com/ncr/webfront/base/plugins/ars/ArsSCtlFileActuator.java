package com.ncr.webfront.base.plugins.ars;

import java.util.List;

public class ArsSCtlFileActuator extends ArsCtlFileActuator {
	public ArsSCtlFileActuator(String sCtlFilePath) {
		super(sCtlFilePath, sCtlOrgFileName);
	}
	
	public ArsSCtlFileActuator(String sCtlFilePath, String serverNumber) {
		super(sCtlFilePath, serverNumber, sCtlDatFileName);
	}
	
	@Override
	public List<String> searchByName(String name) {
		return searchByName(name, false);
	}
	
	private static final String sCtlOrgFileName = "S_CTLXXX.ORG";
	private static final String sCtlDatFileName = "S_CTLnnn.DAT";
}
