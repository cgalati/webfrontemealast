package com.ncr.webfront.base.plugins.ars;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class ArsPRegDatFileActuator implements ArsPRegDatFileActuatorInterface {
	
	/**
	 * Constructor
	 * @param pRegFilePath
	 */
	public ArsPRegDatFileActuator(String pRegFilePath){
		pRegFilePath = PathManager.getInstance().buildNormalizedFullFilePath(pRegFilePath, pRegParFileName);
	    pRegParFile =  new File(pRegFilePath);
		if (! pRegParFile.exists()){
			logger.error("Error! The file \"" + PathManager.getInstance().buildNormalizedFullFilePath(pRegFilePath, pRegParFileName) + "\" does not exist!");
		}
	}
	
	
	@Override
	public String readLine(String id) {
		
		String pRegFileLine="";
		
		logger.debug("Begin readLine() Id="+id);
		
		try (BufferedReader regReader = new BufferedReader(new FileReader(pRegParFile))) {		
			String line = "";
			
			while ((line = regReader.readLine()) != null) {
				if (line.startsWith(id)){
					if (pRegFileLine.isEmpty()){
						pRegFileLine = line;
					} else {
						logger.error("P_REGPAR.DAT contains more than a line starting with "+id);
					}
				}
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		logger.debug("End readLine()");
		
		return pRegFileLine;
	}

	@Override
	public List<String> readAll() {
		List<String> pRegFileLines = new ArrayList<String>();
		
		logger.debug("Begin readAll()");
		
		try (BufferedReader regReader = new BufferedReader(new FileReader(pRegParFile))) {		
			String line = "";
			
			while ((line = regReader.readLine()) != null) {
				pRegFileLines.add(line);
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		logger.debug("End readAll()");
		
		return pRegFileLines;
	}
	
	@Override
	public List<String> readLines(String id) {
		List<String> pRegFileLines = new ArrayList<String>();
		
		logger.debug("Begin readLines() Id="+id);
		
		try (BufferedReader regReader = new BufferedReader(new FileReader(pRegParFile))) {		
			String line = "";
			
			while ((line = regReader.readLine()) != null) {
				if (line.startsWith(id)){
					pRegFileLines.add(line);
				}
			}
		} catch (Exception e) {
			logger.error("", e);
		}
		
		logger.debug("End readLines()");
		
		return pRegFileLines;
	}
	
	private Logger logger = WebFrontLogger.getLogger(ArsPRegDatFileActuator.class);
	private static final String pRegParFileName = "P_REGPAR.DAT";
	private File pRegParFile;

}
