package com.ncr.webfront.base.plugins.ars.vat;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.plugins.ars.ArsMappingProperties;
import com.ncr.webfront.base.plugins.ars.ArsSRegOrgFileActuator;
import com.ncr.webfront.base.plugins.ars.ArsSRegOrgFileActuatorInterface;
import com.ncr.webfront.base.plugins.ars.ArsServerEnvParameters;
import com.ncr.webfront.base.vat.Vat;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class ArsVatServiceImplementation implements VatServiceInterface {

	public ArsVatServiceImplementation() {
		arsVatServiceProperties = PropertiesLoader.loadOrCreatePropertiesFile(ArsMappingProperties.arsPluginPropertiesFileName,ArsMappingProperties.getArsPropertiesMap());
		arsSRegOrgFileActuator = new ArsSRegOrgFileActuator(ArsServerEnvParameters.getInstance().getServerHomeDir());
	}

	@Override
	public ErrorObject checkService() {
		
		try {
			List<Vat> tempVatList = buildVatList();
			
			if (tempVatList == null || tempVatList.size() ==0 ){
				logger.error("VatList is null or empty!");
				return new ErrorObject(ErrorCode.SETUP_ERROR, "The code vat list is empty!");
			}
		} catch (Exception e) {
			logger.error("", e);
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
	
	@Override
	public List<Vat> getAll() {
		if (vatList == null) {
			vatList = buildVatList();
		}

		logger.debug("Entering buildVatList");
		return vatList;
	}

	@Override
	public Vat getVatById(int vatId) {
		if (vatList == null) {
			vatList = buildVatList();
		}
		logger.debug("Begin getVatById");
		for (Vat vat : vatList) {
			if (vat.getVatId() == vatId) {
				logger.debug("End getVatById");
				return vat;
			}
		}
		logger.error("Vat code " + vatId + " not found.");
		logger.debug("End getVatById");
		return null;
	}
	
	@Override
	public Vat getDefault() {
		String vatDefaultId = ArsMappingProperties.getInstance().getProperty(ArsMappingProperties.arsVatDefaultId);
		
		logger.debug(String.format("loading default vat for id [%s]", vatDefaultId));
		
		return getVatById(Integer.parseInt(vatDefaultId));
	}

	private List<Vat> buildVatList() {
		logger.debug("Begin buildVatList");
		List<Vat> returnList = new ArrayList<Vat>();

		for (int id = 701; id < 710; id++) {
			Vat vat = new Vat();
			String line, desc;
			line = arsSRegOrgFileActuator.readLine(id);
			String fields[] = line.split(":");
			desc = fields[1].trim();
			vat.setVatId(id - 701);
			vat.setDescription(desc);
			try {
				vat.setRate(Integer.parseInt(fields[3]) / 10);
			} catch (NumberFormatException nfe) {
				logger.warn("Rate errato in file S_REGXXX.ORG");
				continue;
			}
			logger.trace("Adding " + vat.getVatId() + ":"
					+ vat.getDescription() + " to vatList");
			returnList.add(vat);

		}

		logger.debug("End buildVatList");
		return returnList;

	}

	private Logger logger = WebFrontLogger
			.getLogger(ArsVatServiceImplementation.class);
	private Properties arsVatServiceProperties;
	private ArsSRegOrgFileActuatorInterface arsSRegOrgFileActuator;
	private List<Vat> vatList;

}
