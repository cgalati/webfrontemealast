package com.ncr.webfront.base.plugins.dbtest;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;


import javax.persistence.EntityTransaction;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.google.gson.Gson;
import com.ncr.webfront.base.posidcjrn.PosIdcJrnServiceInterface;
import com.ncr.webfront.base.posidcjrn.entities.*;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;


public class PosIdcJrnServiceImplementation implements PosIdcJrnServiceInterface  {
	
	
    private EntityManager manager;

	private static final Logger log = WebFrontLogger.getLogger(PosIdcJrnServiceImplementation.class);

	public PosIdcJrnServiceImplementation() {
    	setupEntityManager();	
	}
	
	@Override
	public ErrorObject checkService() {
		try {
			return new ErrorObject(ErrorCode.OK_NO_ERROR);
		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
	}
	

	@Override
	public List<DataHeader> getAll() {
		log.info("BEGIN");
	    List<DataHeader> dataTransactions = null;
	    setupEntityManager();
	    
        try {
            manager.getTransaction().begin();
            dataTransactions = manager.createQuery("SELECT t FROM DataHeader t", DataHeader.class).getResultList();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
		log.info("END");
        return dataTransactions;
	}

	@Override
	public ErrorObject addIdcJrn(String jsondataHeader) {
		log.info("Begin");
		Gson gson = new Gson();
		DataHeader dataHeader = gson.fromJson(jsondataHeader, DataHeader.class);  //id = 832
		
		if (checkAlreadyExist(dataHeader)) {
			return new ErrorObject(ErrorCode.OK_NO_ERROR);
		}
		
		setupEntityManager();
		
		try {
	        try {
	            for (DataCollect idc : dataHeader.getIdc()) {  // id_header=" " 
	            	idc.setDataHeader(dataHeader);
	            }
	            for (Journal jrn : dataHeader.getJrn()) {  //id_header=817
	            	jrn.setDataHeader(dataHeader);
	            }
	            
	            manager.getTransaction().begin();
	            manager.persist(dataHeader);
            	manager.getTransaction().commit();
	        } catch (Exception ex) {	        	
	            if (manager.getTransaction() != null) {
	                manager.getTransaction().rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
		} catch (Exception e) {
			log.error("", e);
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		log.info("END");
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	private boolean checkAlreadyExist(DataHeader dataHeader) {
		List<DataHeader> lstTransaction = null;
		
		Format dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		String dataTrans = "";
			
		dataTrans = dataHeader.getReceiptDate(); //dateFormatter.format(dataHeader.getReceiptDate());						

		lstTransaction = getTransactionsExist(dataHeader.getTerminal(), dataTrans, dataHeader.getStore(), dataHeader.getReceipt());
		
		if (lstTransaction != null && lstTransaction.size() > 0) {
			return true;
		}
		
		return false;
	}
	private List<DataHeader> getTransactionsExist(String terminal, String dateTrans, String store, int transaction) {
		log.info("BEGIN");

		List<DataHeader> dataTransactions = null;

		setupEntityManager();

		try {
			manager.getTransaction().begin();

			String query = "SELECT t FROM DataHeader t WHERE ";


			query += ("DATE = '" + dateTrans + "' AND ");

			query += ("TERMINAL = '" + terminal + "' AND ");
			
			query += ("STORE = '" + store + "' AND ");
			
			query += ("TRANS = '" + transaction + "' ");

			dataTransactions = manager.createQuery(query, DataHeader.class).getResultList();
			
			manager.getTransaction().commit();
		} catch (Exception ex) {
			if (manager.getTransaction() != null) {
				manager.getTransaction().rollback();
			}
			ex.printStackTrace();
		} finally {
			manager.close();
		}

		log.info("END");
		return dataTransactions;
	}
	
	@Override
	public List<DataHeader> getTransactionsByParams(String terminal, String dateFrom, String dateTo, String store, String transaction, String customer) {
		log.info("BEGIN");

		
		if (terminal.equals("%20")) {
			terminal = "";
		}
		
		if (dateFrom.equals("%20")) {
			dateFrom = "";
		}
		
		if (dateTo.equals("%20")) {
			dateTo = "";
		}

		if (store.equals("%20")) {
			store = "";
		}

		if (transaction.equals("%20")) {
			transaction = "";
		}

		if (customer.equals("%20")) {
			customer = "";
		}
		
	    List<DataHeader> dataTransactions = null;
	    	  
    	setupEntityManager();	
	    
        try {        	
            manager.getTransaction().begin();
            
			String query = "SELECT t from DataHeader t where ";
			
			if (dateFrom != null && !dateFrom.trim().equals("") && dateTo != null && !dateTo.trim().equals("")) {	            
				query += ("DATE BETWEEN '" + dateFrom + "' AND '" + dateTo + "' AND ");
			} else if (dateFrom != null && !dateFrom.trim().equals("")){
				query += ("DATE >= '" + dateFrom + "' AND ");
			} else if (dateTo != null && !dateTo.trim().equals("")) {
				query += ("DATE <= '" + dateTo + "' AND ");
			} 
			
			if (terminal != null && !terminal.trim().equals("")) {
				query += ("TERMINAL = " + terminal + " AND ");
			}
			
			if (store != null && !store.trim().equals("")) {
				query += ("STORE = " + store + " AND ");
			}

			if (transaction != null && !transaction.trim().equals("")) {
				query += ("TRANS = " + transaction + " AND ");
			}

			if (customer != null && !customer.trim().equals("")) {
				query += ("CUSTOMER = " + customer + " AND ");
			}
			
			if (query.endsWith(" AND ")) {
				query = query.substring(0, query.length() - 5);	
			} else if (query.endsWith(" where ")) {
				query = query.substring(0, query.length() - 6);
			}
			
			System.out.println("QUERYYYYY: " + query);
			
            dataTransactions = manager.createQuery(query, DataHeader.class).getResultList();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
        	
            ex.printStackTrace();
        } finally {
            manager.close();
        }
		log.info("END");
        return dataTransactions;
	}

	@Override
	public ErrorObject deleteIdcJrn(int id, String table) {
		log.info("BEGIN");

		setupEntityManager();
	    
        try {
            manager.getTransaction().begin();
                        
            Query query = manager.createQuery("DELETE FROM " + table + " AS t where t.id=:id");
            	
        	query.setParameter("id", id);
        	query.executeUpdate();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
            ex.printStackTrace();
            
            return new ErrorObject(ErrorCode.SETUP_ERROR, ex.getLocalizedMessage());
        } finally {
            manager.close();
        }
        
		log.info("END");
		return new ErrorObject(ErrorCode.OK_NO_ERROR, "");
	}
	
	@Override
	public ErrorObject deleteAll(int dataHeader) {
		log.info("BEGIN");
		
		setupEntityManager();
	    
        try {
            manager.getTransaction().begin();
                        
            Query query = manager.createQuery("DELETE FROM DataHeader AS t where t.id=:id");
            	
        	query.setParameter("id", dataHeader);
        	query.executeUpdate();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
            ex.printStackTrace();
            
            return new ErrorObject(ErrorCode.SETUP_ERROR, ex.getLocalizedMessage());
        } finally {
            manager.close();
        }
        
		log.info("END");
		return new ErrorObject(ErrorCode.OK_NO_ERROR, "");
	}

	@Override
	public List<DataCollect> getLstIdc(int idHeader) {
		log.info("BEGIN");
		List<DataCollect> listDataCollect = null;
	    setupEntityManager();
	    
        try {
            manager.getTransaction().begin();
            
			String query = "SELECT t from DataCollect t where transaction_header_ID = " + idHeader + " order by SEQUENCE ASC";
						
			listDataCollect = manager.createQuery(query, DataCollect.class).getResultList();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
		
        log.info("END");
        return listDataCollect;
	}

	@Override
	public List<Journal> getLstJrn(int idHeader) {
		log.info("BEGIN");
	    List<Journal> listJrn = null;
	    setupEntityManager();
	    
        try {
            manager.getTransaction().begin();
            
			String query = "SELECT t from Journal t where transaction_header_ID = " + idHeader  + " order by SEQUENCE ASC";
						
            listJrn = manager.createQuery(query, Journal.class).getResultList();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
		
        log.info("END");
        return listJrn;
	}
	
	@Override
	public ErrorObject addIdc(String dataCollect) {
		log.info("BEGIN");
		setupEntityManager();
		
		try {
	        try {
	            manager.getTransaction().begin();
	            manager.persist(dataCollect);

            	manager.getTransaction().commit();
	        } catch (Exception ex) {	        	
	            if (manager.getTransaction() != null) {
	                manager.getTransaction().rollback();
	            }
	            
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
		} catch (Exception e) {
			log.info("END");
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		log.info("END");
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}

	@Override
	public ErrorObject addJrn(String journal) {
		log.info("BEGIN");
		setupEntityManager();
		
		try {
	        try {
	            manager.getTransaction().begin();
	            manager.persist(journal);

            	manager.getTransaction().commit();
	        } catch (Exception ex) {	        	
	            if (manager.getTransaction() != null) {
	                manager.getTransaction().rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
		} catch (Exception e) {
			log.info("END");
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		log.info("END");
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
		
	@Override
	public ErrorObject updateFileName(String fileName, int idHeader) {
		log.info("BEGIN");
	    DataHeader dataTransaction = new DataHeader();
	    setupEntityManager();
	    
        try {
            manager.getTransaction().begin();
            
			String query = "SELECT t from DataHeader t where ID = " + idHeader;
						
            dataTransaction = manager.createQuery(query, DataHeader.class).getSingleResult();

            manager.getTransaction().commit();
        } catch (Exception ex) {
            if (manager.getTransaction() != null) {
                manager.getTransaction().rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
		
        dataTransaction.setFileName(fileName);
        
        //modifico il databese con il nuovo campo
		setupEntityManager();
		
		try {
	        try {
	            manager.getTransaction().begin();
	            manager.merge(dataTransaction);

            	manager.getTransaction().commit();
	        } catch (Exception ex) {	        	
	            if (manager.getTransaction() != null) {
	                manager.getTransaction().rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
		} catch (Exception e) {
			log.error("", e);
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		log.info("END");
		return new ErrorObject(ErrorCode.OK_NO_ERROR);		
	}
	
	public List<Journal> getListJournal(int idHdr) {
		return getLstJrn(idHdr);
	}
	
	public List<DataCollect> getListDc(int idHdr) {
		return getLstIdc(idHdr);
	}
		
	public List<DataHeader> getTransByParams(String terminal, String dateFrom, String dateTo, String store, String transaction, String customer) {
		return getTransactionsByParams(terminal, dateFrom, dateTo, store, transaction, customer);
	}
	
	public ErrorObject cancelIdcJrn(int id, String table) {
		return deleteIdcJrn(id, table);
	}
	
	public ErrorObject cancelAll(DataHeader header) {
		return deleteAll(header.getId());
	}
	
	public ErrorObject changeFileName(String fileName, int idHeader) {
		return updateFileName(fileName, idHeader);
	}
	
	public ErrorObject addJournal(Journal journal) {
		Gson gson = new Gson();
		String jsonJrn = gson.toJson(journal);
		
		return addJrn(jsonJrn);
	}
	
	public ErrorObject addDc(DataCollect dataCollect) {
		Gson gson = new Gson();
		String jsonIdc = gson.toJson(dataCollect);
		return addIdc(jsonIdc);
	}
	
	
	private synchronized void setupEntityManager() {
		try {
			String unitName = WebFrontMappingProperties.webFrontPersistenceUnitName;
			//unitName = "com.ncr.webfront";
			EntityManagerFactory factory = Persistence.createEntityManagerFactory(unitName);
			manager = factory.createEntityManager();
		} catch (Exception e) {
			log.error("", e);
			log.info("eccezione: " + e.getMessage());
			
			throw e;
		}
	}
}
