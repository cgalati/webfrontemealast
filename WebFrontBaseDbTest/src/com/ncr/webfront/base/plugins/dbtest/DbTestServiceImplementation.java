package com.ncr.webfront.base.plugins.dbtest;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import com.ncr.webfront.base.dbtest.DbTestServiceInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.base.dbtest.entities.DbTest;

public class DbTestServiceImplementation implements DbTestServiceInterface {
	private static final Logger logger = WebFrontLogger.getLogger(DbTestServiceImplementation.class);
	private EntityManagerFactory factory;

	public DbTestServiceImplementation() {
		getFactory();
	}
	
	private synchronized EntityManagerFactory getFactory() {
		try {
				factory = Persistence.createEntityManagerFactory(WebFrontMappingProperties.webFrontPersistenceUnitName);
				if (factory == null) {
			}

			return factory;
		} catch (Exception e) {
			logger.error("", e);

			throw e;
		}
	}

	@Override
	public Object checkService() {
		logger.info("BEGIN");
		// TODO Auto-generated method stub
		logger.info("END");
		return null;
	}


	@Override
	public Object getAll() {
		logger.info("BEGIN");
	     List<DbTest> tests = null;

        // Create an EntityManager
        EntityManager manager = factory.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Get a List of DbTests
            tests = manager.createQuery("SELECT t FROM DbTest t",
                    DbTest.class).getResultList();

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
		logger.info("END");
        return tests;
	}
}
