#!/bin/sh 

doinstallservice() {
  if hash insserv 2>/dev/null; then
       echo calling insserv WebFront-EndOfDay
       insserv WebFront-EndOfDay
  else
     #AZL CENTOS#BEG
     echo calling chkconfig WebFront-EndOfDay on
     chkconfig WebFront-EndOfDay on
     #AZL CENTOS#END
  fi
}

echo Inizio webfront-endofday setup 
FOUND=`id | grep "root"	`
if [ -n "$FOUND" ]
then
   echo ' '  >/dev/null
else
   echo ' ' 
   echo "ATTENZIONE:"
   echo "   lo script NON puo' essere eseguito da un utente diverso da root!!!"
   echo "   Fare il logoff dell'utente corrente e fare il login con l'utente root"
   echo ' ' 
   echo Fine   webfront-endofday setup 
   exit -1 
fi

if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi


if [ "${WF_HOMEDIR}" == "" ]
then
  echo 'Missing env variable WF_HOMEDIR.'
  echo 'Setup cancelled.'
  echo Fine   webfront-endofday setup 
  exit -1
fi
if [ ! -d ${WF_HOMEDIR} ]
then
  echo 'Missing webfront home folder: '${WF_HOMEDIR}
  echo 'Setup cancelled.'
  echo Fine   webfront-endofday setup 
  exit -1
fi

if [ "${WF_ENDOFDAY_HOMEDIR}" == "" ]
then
  echo 'Missing env variable WF_ENDOFDAY_HOMEDIR.'
  echo 'Setup cancelled.'
  echo Fine   webfront-endofday setup 
  exit -1
fi
if [ ! -d ${WF_ENDOFDAY_HOMEDIR} ]
then
  echo 'Missing endofday home folder: '${WF_ENDOFDAY_HOMEDIR}
  echo 'Setup cancelled.'
  echo Fine   webfront-endofday setup 
  exit -1
fi


echo '   Changing '${WF_ENDOFDAY_HOMEDIR}' user and group...'
if [ ! "${WF_ENDOFDAY_HOMEDIR}" == "" ]
then
   chmod -R 777 ${WF_ENDOFDAY_HOMEDIR}
fi
echo '   Changing '${WF_ENDOFDAY_HOMEDIR}' permissions...'
chown -R webfront:asr ${WF_ENDOFDAY_HOMEDIR}

echo '   Installazione servizio...'
chown root:root ${WF_ENDOFDAY_HOMEDIR}/sh/_startup_endofday.sh
rm -f /etc/init.d/WebFront-EndOfDay
ln -s ${WF_ENDOFDAY_HOMEDIR}/sh/_startup_endofday.sh /etc/init.d/WebFront-EndOfDay
chown root:root /etc/init.d/WebFront-EndOfDay

doinstallservice

echo Fine   webfront-endofday setup 
              
