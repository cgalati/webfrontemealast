##############################################################################
#
#       Filename:      50_EodIBCFlag.sh
#
#       Description:    
#
#       Parameters:
#
#       Modify:		15/05/2013 Creazione script
##############################################################################
#!/bin/sh


##############################################################################
# To enable debug for this script you must set DBG_MODE=1
##############################################################################
DBG_MODE=0

echo "Rebooting the server in five minutes..."

echo SKIPPING ${WF_ARSSERVER_HOMEDIR}/bin/troy -c "/sbin/shutdown -r 5"
echo ${WF_ARSSERVER_HOMEDIR}/bin/troy -c "/sbin/shutdown -r 5"
