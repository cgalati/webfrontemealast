##############################################################################
#
#       Filename:      20_EodStepIDCEod.sh
#
#       Description:    
#
#       Parameters:
#
#       Modify:		15/05/2013 Creazione script
##############################################################################
#!/bin/sh

##############################################################################
# To enable debug for this script you must set DBG_MODE=1
##############################################################################
DBG_MODE=0

pushd /home/NCR/idcupdate/IDC

echo "Before ./idc_update eod"
./idc_update eod 
echo "After ./idc_update eod"

popd

exit 0

