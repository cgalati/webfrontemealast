@ECHO off

rem #############################################################################
rem
rem       Filename:      10_EodStepIDCStop.bat
rem
rem       Description:   Terminazione del processo idc_update 
rem
rem       Parameters:
rem
rem       Modify:		15/04/2011 Creazione script
rem #############################################################################

rem #############################################################################
echo 10_EodStepIDCStop.bat - BEGIN

PUSHD "%ProgramFiles%\webfront\sh"
call webfront_setenv.cmd NOPAUSE >NUL
POPD

IF NOT EXIST "%WF_IDC_UPDATE_HOMEDIR%" GOTO :END

echo 10_EodStepIDCStop.bat - pushd "%WF_IDC_UPDATE_HOMEDIR%"
pushd "%WF_IDC_UPDATE_HOMEDIR%"

echo 10_EodStepIDCStop.bat - idc_update stop
idc_update stop

echo 10_EodStepIDCStop.bat - popd
popd

echo 10_EodStepIDCStop.bat - END

:END
exit 0

rem #############################################################################
