##############################################################################
#
#       Filename:      00_EodStepCurrentTerminalEod.sh
#
#       Description:    
#
#       Parameters:
#
#       Modify:		15/05/2013 Creazione script
##############################################################################
#!/bin/sh

echo Begin of 00_EodStepCurrentTerminalEod.sh

##############################################################################
# Environment variable below are needed for pos_eod program
##############################################################################
export SRV=${WF_ARSSERVER_SRV}
export REG=${WF_ARSSERVER_CRTEOD}
export USER=${WF_ARSSERVER_SUP}
export PROTOCOL=tcp

##############################################################################
# To enable debug for this script you must set DBG_MODE=1
##############################################################################
DBG_MODE=0

if [ ${DBG_MODE} = "1" ]
then
  echo
  echo SRV=${SRV}
  echo REG=${REG}
  echo USER=${USER}
  echo PROTOCOL=${PROTOCOL}
  echo
fi
pushd /home/NCR/server/bin

echo "Before ./pos_eod -A99 -R${REG} -S${USER}"
./pos_eod -A99 -R${REG} -S${USER}
echo "After  ./pos_eod -A99 -R${REG} -S${USER}"

popd 

exit 0
