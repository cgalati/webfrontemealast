@ECHO OFF

rem #############################################################################
rem #
rem #       Filename:      40_EodStepPromoInterface.sh
rem #
rem #       Description:   EOD Interfaccia Promozioni 
rem #
rem #       Parameters:
rem #
rem #       Modify:		15/04/2011 Creazione script
rem ##############################################################################

rem ##############################################################################
echo 40_EodStepPromoInterface.bat - BEGIN

PUSHD "%ProgramFiles%\webfront\sh"
call webfront_setenv.cmd NOPAUSE >NUL
POPD

echo 40_EodStepPromoInterface.bat - pushd "%WF_PROMO_INTERFACE_HOMEDIR%\sh"
pushd "%WF_PROMO_INTERFACE_HOMEDIR%\sh" 

echo 40_EodStepPromoInterface.bat CALL EodInterfacciaPromozioni.bat
CALL EodInterfacciaPromozioni.bat

echo 40_EodStepPromoInterface.bat - popd
popd

echo 40_EodStepPromoInterface.bat - END
exit 0
rem ##############################################################################
