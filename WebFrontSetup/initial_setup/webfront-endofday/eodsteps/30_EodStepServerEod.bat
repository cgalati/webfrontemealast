@ECHO off

rem #############################################################################
rem
rem       Filename:      30_EodStepServerEod.bat
rem
rem       Description:   EOD Server As@r 
rem
rem       Parameters:
rem
rem       Modify:   15/04/2011 Creazione script
rem #############################################################################

echo 30_EodStepServerEod.bat - BEGIN

PUSHD "%ProgramFiles%\webfront\sh"
call webfront_setenv.cmd NOPAUSE >NUL
POPD

CD

IF NOT EXIST "%WF_IDC_UPDATE_HOMEDIR%" Echo Skip calling 10_EodStepIDCStop.bat
IF NOT EXIST "%WF_IDC_UPDATE_HOMEDIR%" GOTO :SKIPIDC
echo 30_EodStepServerEod.bat - Begin calling 10_EodStepIDCStop.bat

CALL 10_EodStepIDCStop.bat
echo 30_EodStepServerEod.bat - End   calling 10_EodStepIDCStop.bat
:SKIPIDC 

rem Numero di tentativi
echo 30_EodStepServerEod.bat - ARG 1 = %1 
if "%1" == "" goto WRONG_PARAM
set waitNumber=%1

echo waitNumber=%waitNumber%

rem Tempo di attesa tra un tentativo e l'altro
if "%2" == "" goto WRONG_PARAM
set sleepTime=%2
echo sleepTime=%sleepTime%


SET SRV=%WF_ARSSERVER_SRV%
SET REG=%WF_ARSSERVER_CRTEOD%
SET USER=%WF_ARSSERVER_SUP%
SET PROTOCOL=tcp

rem ############################################################################# 
echo waitNumber=%waitNumber%

if exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL ECHO %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL esiste!
if NOT exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL ECHO %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL NON esiste!
del /f %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL

echo 30_EodStepServerEod.bat - pushd %WF_ARSSERVER_HOMEDIR%\bin
pushd %WF_ARSSERVER_HOMEDIR%\bin

rem per verificare la fine del fine girno 
rem � possibile verificare l'esistenza del file SRV_EOD.CTL
rem quindi lo cancello prima del fine giorno e poi verifico 
rem se viene scritto, in caso contrario esco con errore 1

echo 30_EodStepServerEod.bat - pos_eod -A99 -R000 -S%USER%

pos_eod -A99 -R000 -S%USER%

echo 30_EodStepServerEod.bat - popd
popd


SET intCounter=1
echo 30_EodStepServerEod.bat - Entering while cycle

:while
REM test condition

echo 30_EodStepServerEod.bat - intCounter = %intCounter%
IF %intCounter% == %waitNumber% (GOTO wend)

echo 30_EodStepServerEod.bat - ping -n %sleepTime% 127.0.0.1
echo before ping -n %sleepTime% 127.0.0.1...
ping -n %sleepTime% 127.0.0.1 >NUL
echo after ping -n %sleepTime% 127.0.0.1...

echo before set...
SET /a intCounter=intCounter+1
echo after set...

if exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL ECHO %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL exists!
if NOT exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL ECHO %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL doesn't exist!
if exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL goto EOD_END

echo before GOTO while
GOTO while
:wend
echo 30_EodStepServerEod.bat - Exiting from while cycle

if exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL goto EOD_END
if not exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL ECHO File %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL DOES NOT EXIST 
if not exist %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL goto NOT_END
echo 30_EodStepServerEod.bat - Server end of day NOT ENDED
echo 30_EodStepServerEod.bat - END
exit 1


:NOT_END
echo 30_EodStepServerEod.bat - File %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL DOES NOT EXIST
echo 30_EodStepServerEod.bat - Server end of day NOT ENDED
echo 30_EodStepServerEod.bat - END
exit 1
rem fi

:EOD_END
echo 30_EodStepServerEod.bat -  File %WF_ARSSERVER_HOMEDIR%\SRV_EOD.CTL EXISTS
echo 30_EodStepServerEod.bat - Server end of day SUCCESSFULLY ENDED
echo 30_EodStepServerEod.bat - END
exit 0

:WRONG_PARAM
echo 30_EodStepServerEod.bat - Wrong parameters
echo 30_EodStepServerEod.bat - END
exit 1


rem ############################################################################# 
