##############################################################################
#
#       Filename:      30_EodStepServerEod.sh
#
#       Description:   EOD Server As@r 
#
#       Parameters:
#
#       Modify:		15/04/2011 Creazione script
##############################################################################
#!/bin/sh

##############################################################################
# To enable debug for this script you must set DBG_MODE=1
##############################################################################
DBG_MODE=0

##############################################################################
# Environment variable below are needed for pos_eod program
##############################################################################
export SRV=${WF_ARSSERVER_SRV}
export REG=${WF_ARSSERVER_CRTEOD}
export USER=${WF_ARSSERVER_SUP}
export PROTOCOL=tcp

if [ ${DBG_MODE} = "1" ]
then
    echo
    echo SRV=${SRV}
    echo REG=${REG}
    echo USER=${USER}
    echo PROTOCOL=${PROTOCOL}
    echo
fi

#Numero di tentativi
waitNumber=$1

#Tempo di attesa tra un tentativo e l'altro
sleepTime=$2
i=0

rm -f /home/NCR/server/SRV_EOD.CTL

pushd /home/NCR/server/bin

echo "Before ./pos_eod -A99 -R0 -S${SRV}"
./pos_eod -A99 -R0 -S${SRV}
echo "After  ./pos_eod -A99 -R0 -S${SRV}"

popd

echo "Entering the while loop..."  

while [ ${i} -lt ${waitNumber} ]
do
	if [ ! -f /home/NCR/server/SRV_EOD.CTL ]
  then
      if [ ${DBG_MODE} = "1" ]
      then
        echo "Sleeping ${sleepTime} seconds..."  
      fi
		  sleep ${sleepTime}
	else
      echo "File /home/NCR/server/SRV_EOD.CTL not present, exiting while loop"  
		break
	fi
	i=`expr ${i} + 1`
done

if [ ${i} -eq ${waitNumber} -a ! -f /home/NCR/server/SRV_EOD.CTL ]
then
  echo "File /home/NCR/server/SRV_EOD.CTL still present after while loop finished"  
  echo "exiting script with code 1 (PROBLEMS)"
	exit 1
fi

echo "exiting script with code 0 (OK)"
exit 0
