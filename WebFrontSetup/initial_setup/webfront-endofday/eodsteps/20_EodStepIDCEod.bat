@ECHO off

rem #############################################################################
rem
rem       Filename:      20_EodStepIDCEod.bat
rem
rem       Description:   Step per eseguire fine giorno del programma IDC 
rem
rem       Parameters:
rem
rem       Modify:		15/04/2011 Creazione script
rem #############################################################################

rem #############################################################################
echo 20_EodStepIDCEod.bat - BEGIN

PUSHD "%ProgramFiles%\webfront\sh"
call webfront_setenv.cmd NOPAUSE >NUL
POPD

IF NOT EXIST "%WF_IDC_UPDATE_HOMEDIR%" GOTO :END

echo 20_EodStepIDCEod.bat - pushd "%WF_IDC_UPDATE_HOMEDIR%"
pushd "%WF_IDC_UPDATE_HOMEDIR%"

echo 20_EodStepIDCEod.bat - .idc_update eod
idc_update eod 

echo 20_EodStepIDCEod.bat - popd
popd

echo 20_EodStepIDCEod.bat - END

:END
exit 0
rem #############################################################################
