@ECHO OFF

rem #############################################################################
rem
rem       Filename:      00_EodStepCurrentTerminalEod.bat
rem
rem       Description:   EOD Terminal solo nodo EOD clientr 
rem
rem       Parameters:
rem
rem       Modify:		15/04/2011 Creazione script
rem #############################################################################

PUSHD "%ProgramFiles%\webfront\sh"
call webfront_setenv.cmd NOPAUSE >NUL
POPD

SET SRV=%WF_ARSSERVER_SRV%
SET REG=%WF_ARSSERVER_CRTEOD%
SET USER=%WF_ARSSERVER_SUP%
SET PROTOCOL=TCP

rem ############################################################################# 
echo 00_EodStepCurrentTerminalEod.bat - BEGIN

echo 00_EodStepCurrentTerminalEod.bat - pushd C:\server\bin
pushd "%WF_ARSSERVER_HOMEDIR%\bin"

echo 00_EodStepCurrentTerminalEod.bat - pos_eod -A99 -R%REG% -S%USER%
pos_eod -A99 -R%REG% -S%USER%

echo 00_EodStepCurrentTerminalEod.bat - popd 
popd 

echo 00_EodStepCurrentTerminal.bat - END
exit 0
rem #############################################################################
