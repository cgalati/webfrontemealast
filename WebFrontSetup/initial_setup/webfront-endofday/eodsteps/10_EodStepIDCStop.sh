##############################################################################
#
#       Filename:      10_EodStepIDCStop.sh
#
#       Description:    
#
#       Parameters:
#
#       Modify:		15/05/2013 Creazione script
##############################################################################
#!/bin/sh

##############################################################################
# To enable debug for this script you must set DBG_MODE=1
##############################################################################
DBG_MODE=0

pushd /home/NCR/idcupdate/IDC

echo "Before ./idc_update stop"  
./idc_update stop
echo "After  ./idc_update stop"  

popd

exit 0
