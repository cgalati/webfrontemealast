@echo off
TITLE WebFront-EndOfDay

PUSHD "%ProgramFiles%\webfront\sh"
call webfront_setenv.cmd NOPAUSE 
POPD

CD "%WF_ENDOFDAY_HOMEDIR%"

SET EOD_HOME=%CD%

set EOD_LIB=%EOD_HOME%\jar
set EOD_JAR=%EOD_HOME%\jar

set CLASSPATH=
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\commons-lang.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\commons-logging.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\gson.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\log4j.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\postgresql-9.2.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\antlr.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\dom4j.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\hibernate-commons-annotations.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\hibernate-core.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\hibernate-entitymanager.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\hibernate-jpa-2.0-api.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\javassist.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\jboss-logging.jar
set CLASSPATH=%CLASSPATH%;%EOD_LIB%\jboss-transaction-api.jar
set CLASSPATH=%CLASSPATH%;%EOD_JAR%\WebFrontCommonUtils.jar
set CLASSPATH=%CLASSPATH%;%EOD_JAR%\WebFrontBaseCommonData.jar
set CLASSPATH=%CLASSPATH%;%EOD_JAR%\WebFrontEodSocketServer.jar

echo %CLASSPATH%

if "%1" == "start" goto start
if "%1" == "stop" goto stop

:start
cd %EOD_HOME%

java com.ncr.webfront.base.eod.socketserver.Main

goto end

:stop

wmic process where "commandline like '%%asarfronteod%%'" call terminate

if exist EODSTATUS DEL EODSTATUS

:end

PAUSE
