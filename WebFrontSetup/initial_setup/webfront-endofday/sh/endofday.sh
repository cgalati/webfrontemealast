if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi

EOD_HOME=${WF_ENDOFDAY_HOMEDIR}

EOD_LIB=${EOD_HOME}/jar
EOD_JAR=${EOD_HOME}/jar
LOG_DIR=${WF_ENDOFDAY_LOGSDIR}
EODSERVICELOG=${LOG_DIR}/WebFront_EodService.log


echo $$ > ${WF_ENDOFDAY_HOMEDIR}/sh/_endofday.pid

CLASSPATH=${EOD_LIB}/commons-lang.jar                           
CLASSPATH=${CLASSPATH}:${EOD_LIB}/commons-logging.jar
CLASSPATH=${CLASSPATH}:${EOD_LIB}/commons-io.jar                  
CLASSPATH=${CLASSPATH}:${EOD_LIB}/gson.jar                             
CLASSPATH=${CLASSPATH}:${EOD_LIB}/log4j.jar                            
CLASSPATH=${CLASSPATH}:${EOD_LIB}/postgresql-9.2.jar                   
CLASSPATH=${CLASSPATH}:${EOD_LIB}/antlr.jar                            
CLASSPATH=${CLASSPATH}:${EOD_LIB}/dom4j.jar                            
CLASSPATH=${CLASSPATH}:${EOD_LIB}/hibernate-commons-annotations.jar    
CLASSPATH=${CLASSPATH}:${EOD_LIB}/hibernate-core.jar                   
CLASSPATH=${CLASSPATH}:${EOD_LIB}/hibernate-entitymanager.jar          
CLASSPATH=${CLASSPATH}:${EOD_LIB}/hibernate-jpa-2.0-api.jar            
CLASSPATH=${CLASSPATH}:${EOD_LIB}/javassist.jar                        
CLASSPATH=${CLASSPATH}:${EOD_LIB}/jboss-logging.jar                    
CLASSPATH=${CLASSPATH}:${EOD_LIB}/jboss-transaction-api.jar            
CLASSPATH=${CLASSPATH}:${EOD_JAR}/WebFrontCommonUtils.jar              
CLASSPATH=${CLASSPATH}:${EOD_JAR}/WebFrontBaseCommonData.jar           
CLASSPATH=${CLASSPATH}:${EOD_JAR}/WebFrontEodSocketServer.jar          

echo ${CLASSPATH}

cd ${EOD_HOME}

echo ' '    >> ${EODSERVICELOG} 
echo ' '    >> ${EODSERVICELOG} 
echo '****************************************************************'    >> ${EODSERVICELOG} 
echo BEGIN of $0  >> ${EODSERVICELOG} 
echo '****************************************************************'    >> ${EODSERVICELOG} 

rm -f ${WF_ENDOFDAY_HOMEDIR}/tmp/* >>${EODSERVICELOG} 2>&1 

echo java -cp ${CLASSPATH} com.ncr.webfront.base.eod.socketserver.Main -p8888 -t${WF_ENDOFDAY_STOP_FLAG_FILE} >>${EODSERVICELOG}
java -cp ${CLASSPATH} com.ncr.webfront.base.eod.socketserver.Main  -p8888 -t${WF_ENDOFDAY_STOP_FLAG_FILE} >>${EODSERVICELOG} 2>&1 

rm -f  ${WF_ENDOFDAY_HOMEDIR}/sh/_endofday.pid
rm -f ${WF_ENDOFDAY_STOP_FLAG_FILE}

echo ' '    >> ${EODSERVICELOG} 
echo '****************************************************************'    >> ${EODSERVICELOG} 
echo END   of $0  >> ${EODSERVICELOG} 
echo '****************************************************************'    >> ${EODSERVICELOG} 
