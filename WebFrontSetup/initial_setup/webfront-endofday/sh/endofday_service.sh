#!/bin/bash

if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi

status(){
cd ${WF_ENDOFDAY_HOMEDIR}/sh
if [ -f _endofday.pid ]
then
        EODPID=`cat _endofday.pid`
        # il processo e' ancora in esecuzione?
        if ps -p $EODPID >/dev/null 2>/dev/null
        then
                if [ "$FUNC" == "status" ]
                then
                   echo "EndOfDay service active"
                fi
	        STATUS=1
        else
                if  [ "$FUNC" == "status" ]
                then
                   echo "EndOfDay service not active"
                fi
	        STATUS=0
          rm -f _endofday.pid
        fi
else
	if [ "$FUNC" == "status" ]
	then
           echo "EndOfDay service not active"
        fi
	STATUS=0
fi
}

start() {

	#echo -n $"Starting WebFront-EndOfDay service: "
	echo "Starting WebFront-EndOfDay service: "
	cd ${WF_ENDOFDAY_HOMEDIR}/sh

        FUNC=start
        status
        if [ "$STATUS" -ne "0" ]
        then
           echo "EndOfDay service already active"
           exit
        fi
	
 	/usr/bin/nohup ${WF_ENDOFDAY_HOMEDIR}/sh/endofday.sh endofday >/dev/null  2>&1 &
	sleep 2

        FUNC=status
        status
	
 	RETVAL=$?
	RETVAL=0
}

stop() {
	#echo -n $"Stopping WebFront-EndOfDay service: "
	echo "Stopping WebFront-EndOfDay service: "

  FUNC=stop

	cd ${WF_ENDOFDAY_HOMEDIR}/sh

  status 
  if [ "$STATUS" -ne "0" ]
  then
     if [ -f _endofday.pid ]
     then
	      echo stop >${WF_ENDOFDAY_STOP_FLAG_FILE}

        TIMEOUT=20
        for ((i=1; i<=$TIMEOUT; i++))
        do
           sleep 1
           if [ ! -f ${WF_ENDOFDAY_STOP_FLAG_FILE} ]
           then
                 break
           fi
        done

        if [ -f ${WF_ENDOFDAY_STOP_FLAG_FILE} ]
        then
#          if [ -f _endofday.pid ]
#          then
#             EODPID=`cat _endofday.pid`
#             kill -15 $EODPID
#             rm _endofday.pid
#          fi
         echo "Cannot stop EndOfDay service!" 
        fi
#        rm -f _endofday.pid
        rm -f ${WF_ENDOFDAY_STOP_FLAG_FILE}
     fi
  fi

	sleep 2

        FUNC=status
        status

 	RETVAL=$?
	RETVAL=0

}

FUNC=$1


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
	status
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo "Usage: $0 {start|stop|status|restart}"
	exit 1
esac

exit $RETVAL
