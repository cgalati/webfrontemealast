#!/bin/bash
#AZL CENTOS START
# chkconfig: 35 80 30
# description: WebFront-EndOfDay
#
#AZL CENTOS END

export SUBSYSNAME='WebFront-EndOfDay'

if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi

status() {
   if [ ! "${WF_ENDOFDAY_HOMEDIR}" == "" ]
   then
        chown -R webfront:asr ${WF_ENDOFDAY_HOMEDIR}/
        chmod -R 777  ${WF_ENDOFDAY_HOMEDIR}/
        chmod -R o-wx ${WF_ENDOFDAY_HOMEDIR}/
   fi
   su - webfront -c "${WF_ENDOFDAY_HOMEDIR}/sh/endofday_service.sh status"
	RETVAL=0
}

start() {

   if [ ! "${WF_ENDOFDAY_HOMEDIR}" == "" ]
   then
        chown -R webfront:asr ${WF_ENDOFDAY_HOMEDIR}/
        chmod -R 777  ${WF_ENDOFDAY_HOMEDIR}/
        chmod -R o-wx ${WF_ENDOFDAY_HOMEDIR}/
   fi
   su - webfront -c "${WF_ENDOFDAY_HOMEDIR}/sh/endofday_service.sh start"
	RETVAL=0
}

stop() {
   if [ ! "${WF_ENDOFDAY_HOMEDIR}" == "" ]
   then
        chown -R webfront:asr ${WF_ENDOFDAY_HOMEDIR}/
        chmod -R 777  ${WF_ENDOFDAY_HOMEDIR}/
        chmod -R o-wx ${WF_ENDOFDAY_HOMEDIR}/
   fi
   su - webfront -c "${WF_ENDOFDAY_HOMEDIR}/sh/endofday_service.sh stop"
	RETVAL=0

}

FUNC=$1



case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
	status
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo "Usage: $0 {start|stop|status|restart}"
	exit 1
esac

exit $RETVAL
