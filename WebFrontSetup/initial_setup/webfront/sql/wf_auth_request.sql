DROP TABLE wf_auth_requests CASCADE;
CREATE TABLE wf_auth_requests (
	id INT NOT NULL,
	pos_request_id VARCHAR(30) NOT NULL,
	store_code VARCHAR(10) NOT NULL,
	terminal_code VARCHAR(10) NOT NULL,
	transaction_number INT NOT NULL,
	operator_code VARCHAR(10) NOT NULL,
	creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
	operation_type VARCHAR(20) NOT NULL,
	additional_info VARCHAR(40),
	reason_code INT,
	wf_auth_responses_id INT,
	wf_auth_cancel_notes_id INT,
	CONSTRAINT wf_auth_requests_pk PRIMARY KEY (id)
);


DROP TABLE wf_auth_responses CASCADE;
CREATE TABLE wf_auth_responses (
	id INT NOT NULL,
	user_name VARCHAR(10) NOT NULL,
	creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
	response_type VARCHAR(20) NOT NULL,
	CONSTRAINT wf_auth_responses_pk PRIMARY KEY (id)
);

DROP TABLE wf_auth_cancel_notes CASCADE;
CREATE TABLE wf_auth_cancel_notes (
	id INT NOT NULL,
	creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
	cancel_type VARCHAR(20) NOT NULL,
	CONSTRAINT wf_cancel_notes_pk PRIMARY KEY (id)
);

ALTER TABLE wf_auth_requests ADD CONSTRAINT wf_auth_requests_auth_responses_fk
FOREIGN KEY (wf_auth_responses_id)
REFERENCES wf_auth_responses (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_auth_requests ADD CONSTRAINT wf_auth_requests_auth_cancel_notes_fk
FOREIGN KEY (wf_auth_cancel_notes_id)
REFERENCES wf_auth_cancel_notes (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
