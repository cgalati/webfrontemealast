ALTER TABLE wf_document_details DROP CONSTRAINT wf_documents_wf_document_details_fk;
ALTER TABLE wf_documents DROP CONSTRAINT wf_customers_wf_documents_fk;
ALTER TABLE wf_customers DROP CONSTRAINT wf_customer_types_wf_customers_fk;
ALTER TABLE wf_customers DROP CONSTRAINT wf_customer_addresses_wf_customers_fk;
ALTER TABLE wf_customers DROP CONSTRAINT wf_exemptions_wf_customers_fk;
ALTER TABLE wf_documents DROP CONSTRAINT wf_documents_wf_invoices_fk;
ALTER TABLE wf_documents DROP CONSTRAINT wf_invoice_types_wf_invoices_fk;
ALTER TABLE wf_documents DROP CONSTRAINT wf_document_statuses_wf_documents_fk;

DROP TABLE wf_document_statuses;
CREATE TABLE wf_document_statuses (
                id INT NOT NULL,
                status VARCHAR(10) NOT NULL,
                description VARCHAR(20) NOT NULL,
                CONSTRAINT wf_document_statuses_pk PRIMARY KEY (id)
);

INSERT INTO wf_document_statuses (id, status, description) VALUES (1, 'CREATED', 'CREATED');
INSERT INTO wf_document_statuses (id, status, description) VALUES (2, 'PRINTED', 'PRINTED');
INSERT INTO wf_document_statuses (id, status, description) VALUES (3, 'CANCELED', 'CANCELED');

DROP TABLE wf_documents_types;
CREATE TABLE wf_documents_types (
                id INT NOT NULL,
                type VARCHAR(10) NOT NULL,
                description VARCHAR(20) NOT NULL,
                CONSTRAINT wf_documents_types_pk PRIMARY KEY (id)
);

INSERT INTO wf_documents_types (id, type, description) VALUES (1, 'INVOICE', 'INVOICE');
INSERT INTO wf_documents_types (id, type, description) VALUES (2, 'CREDITNOTE', 'CREDIT NOTE');

DROP TABLE wf_exemptions;
CREATE TABLE wf_exemptions (
                id INT NOT NULL,
                description VARCHAR(20) NOT NULL,
                rate INT NOT NULL,
                facilitated BIT NOT NULL DEFAULT 'true',
                long_description VARCHAR(255),
                CONSTRAINT wf_exemptions_pk PRIMARY KEY (id)
);

INSERT INTO wf_exemptions (id, description, rate, facilitated, long_description) VALUES (0, 'NONE', 21, 'false', '');

DROP TABLE wf_customer_addresses;
CREATE TABLE wf_customer_addresses (
                id INT NOT NULL,
                street VARCHAR(255) NOT NULL,
                city VARCHAR(100) NOT NULL,
                country VARCHAR(100) NOT NULL,
                postal_code VARCHAR(10) NOT NULL,
                CONSTRAINT wf_customer_addresses_pk PRIMARY KEY (id)
);

DROP TABLE wf_customer_types;
CREATE TABLE wf_customer_types (
                id INT NOT NULL,
                type VARCHAR(10) NOT NULL,
                description VARCHAR(20) NOT NULL,
                CONSTRAINT wf_customer_types_pk PRIMARY KEY (id)
);

INSERT INTO wf_customer_types (id, type, description) VALUES (1, 'COMPANY', 'COMPANY');
INSERT INTO wf_customer_types (id, type, description) VALUES (2, 'PRIVATE', 'PRIVATE SUBJECT');

DROP TABLE wf_customers;
CREATE TABLE wf_customers (
                id INT NOT NULL,
                last_name_company_name VARCHAR(255),
                vat_code VARCHAR(20),
                fiscal_code VARCHAR(20),
                fidelity_code VARCHAR(13),
                last_update DATETIME NOT NULL,
                wf_customer_types_id INT NOT NULL,
                wf_customer_addresses_id INT NOT NULL,
                wf_exemptions_id INT NOT NULL,
                CONSTRAINT wf_customers_pk PRIMARY KEY (id)
);

DROP TABLE wf_documents;
CREATE TABLE wf_documents (
                id INT NOT NULL,
                wf_document_types_id INT NOT NULL,
                document_number INT NOT NULL,
                document_code VARCHAR(20) NOT NULL,
                creation_date DATETIME NOT NULL,
                wf_document_statuses_id INT NOT NULL,
                wf_customers_id INT NOT NULL,
                last_update DATETIME NOT NULL,
                amount INT NOT NULL,
                receipt_number INT NOT NULL,
                receipt_terminal VARCHAR(20) NOT NULL,
                receipt_store VARCHAR(20) NOT NULL,
                receipt_date DATETIME NOT NULL,
                wf_documents_id INT,
                CONSTRAINT wf_documents_pk PRIMARY KEY (id)
);

DROP TABLE wf_document_details;
CREATE TABLE wf_document_details (
                id INT NOT NULL,
                wf_documents_id INT NOT NULL,
                serial_number INT NOT NULL,
                ean VARCHAR(20),
                department VARCHAR(20) NOT NULL,
                description VARCHAR(50),
                measure_unit VARCHAR(5),
                decimal_quantity BOOLEAN,
                vat_code VARCHAR(5) NOT NULL,
                vat_rate INT NOT NULL,
                price INT NOT NULL,
                net_price INT NOT NULL,
                quantity DECIMAL(10,4) NOT NULL,
                discount INT NOT NULL,
                amount INT NOT NULL,
                net_discount INT NOT NULL,
                net_amount INT NOT NULL,
                exmpt BOOLEAN NOT NULL,
                CONSTRAINT wf_document_details_pk PRIMARY KEY (id)
);

ALTER TABLE wf_documents ADD CONSTRAINT wf_document_statuses_wf_documents_fk
FOREIGN KEY (wf_document_statuses_id)
REFERENCES wf_document_statuses (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_documents ADD CONSTRAINT wf_invoice_types_wf_invoices_fk
FOREIGN KEY (wf_document_types_id)
REFERENCES wf_documents_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_documents ADD CONSTRAINT wf_documents_wf_invoices_fk
FOREIGN KEY (wf_documents_id)
REFERENCES wf_documents (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_customers ADD CONSTRAINT wf_exemptions_wf_customers_fk
FOREIGN KEY (wf_exemptions_id)
REFERENCES wf_exemptions (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_customers ADD CONSTRAINT wf_customer_addresses_wf_customers_fk
FOREIGN KEY (wf_customer_addresses_id)
REFERENCES wf_customer_addresses (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_customers ADD CONSTRAINT wf_customer_types_wf_customers_fk
FOREIGN KEY (wf_customer_types_id)
REFERENCES wf_customer_types (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_documents ADD CONSTRAINT wf_customers_wf_documents_fk
FOREIGN KEY (wf_customers_id)
REFERENCES wf_customers (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE wf_document_details ADD CONSTRAINT wf_documents_wf_document_details_fk
FOREIGN KEY (wf_documents_id)
REFERENCES wf_documents (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
