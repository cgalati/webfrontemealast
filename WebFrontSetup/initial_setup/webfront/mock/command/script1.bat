@echo off

echo pre-checkpoint

echo checkpoint 1

echo prova1
echo prova1 error 1>&2
mkdir a

echo checkpoint 2

cd a
cd ..
ping 192.0.2.2 -n 1 -w 20000 > nul
echo prova2
echo prova2 error 1>&2

echo checkpoint 3

rmdir a
rmdir a
echo prova3
echo prova3 error 1>&2

echo checkpoint 4
