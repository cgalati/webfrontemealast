status() {
  echo /etc/init.d/tomcat status
  /etc/init.d/tomcat status
  
  echo /etc/init.d/Ars-Server status
  /etc/init.d/Ars-Server status
  
  echo /etc/init.d/WebFront-MntApply status
  /etc/init.d/WebFront-MntApply status
  
  echo /etc/init.d/WebFront-EndOfDay status
  /etc/init.d/WebFront-EndOfDay status
  RETVAL=0
}

start() {
  echo /etc/init.d/tomcat start
  /etc/init.d/tomcat start
  
  echo /etc/init.d/Ars-Server start
  /etc/init.d/Ars-Server start
  
  echo /etc/init.d/WebFront-MntApply start
  /etc/init.d/WebFront-MntApply start
  
  echo /etc/init.d/WebFront-EndOfDay start
  /etc/init.d/WebFront-EndOfDay start
  RETVAL=0
}

stop() {
  echo /etc/init.d/tomcat stop
  /etc/init.d/tomcat stop
  
  echo /etc/init.d/Ars-Server stop
  /etc/init.d/Ars-Server stop
  
  echo /etc/init.d/WebFront-MntApply stop
  /etc/init.d/WebFront-MntApply stop
  
  echo /etc/init.d/WebFront-EndOfDay stop
  /etc/init.d/WebFront-EndOfDay stop

	RETVAL=0

}
  
FUNC=$1

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
	status
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo "Usage: $0 {start|stop|status|restart}"
	exit 1
esac

echo
echo
exit $RETVAL
