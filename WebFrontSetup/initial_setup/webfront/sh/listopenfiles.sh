DATE=$(date +"%Y%m%d%H%M%S")
mkdir ../logs/{$DATE}
output=`ps aux|grep org.apache.juli.ClassLoaderLogManager`
set -- $output
pid=$2
echo
ps aux|grep org.apache.juli.ClassLoaderLogManager
echo
echo TOMCAT pid = $pid
echo ls -l /proc/$pid/fd in ../logs/{$DATE}/TOMCAT_FILES.log
ls -l /proc/$pid/fd >../logs/{$DATE}/TOMCAT_FILES.log


output=`ps aux|grep WebFrontEodSocketServer.jar`
set -- $output
pid=$2
echo
ps aux|grep WebFrontEodSocketServer.jar
echo
echo WebFrontEodSocketServer pid = $pid
echo ls -l /proc/$pid/fd in ../logs/{$DATE}/WEBFRONTEODSOCKETSERVER_FILES.log
ls -l /proc/$pid/fd >../logs/{$DATE}/WEBFRONTEODSOCKETSERVER_FILES.log

chmod 777 ../logs/{$DATE}
chmod -R 777 ../logs/{$DATE}/
