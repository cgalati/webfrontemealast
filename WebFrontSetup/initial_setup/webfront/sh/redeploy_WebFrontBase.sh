
TOMCATWEBAPPSDIR=
if [ -d /usr/share/tomcat/webapps ] 
then
   TOMCATWEBAPPSDIR=/usr/share/tomcat/webapps
fi

if [ -d /opt/WebFront/webapps ] 
then
   TOMCATWEBAPPSDIR=/opt/WebFront/webapps
fi

if [ "$TOMCATWEBAPPSDIR" == "" ]
then
   echo "unable to detect where tomcat is!"
   echo "exiting script!"
   exit
fi

/home/NCR/webfront/sh/all_services.sh stop

cd ${TOMCATWEBAPPSDIR}/


rm -f WebFrontBaseRestServer.war
rm -f WebFrontBase.war
rm -Rf WebFrontBaseRestServer/
rm -Rf WebFrontBase/
rm -f WebFrontBaseRemAuthRestServer.war
rm -f WebFrontBaseRemAuth.war
rm -Rf WebFrontBaseRemAuthRestServer/
rm -Rf WebFrontBaseRemAuth/

/home/NCR/webfront/sh/all_services.sh start
sleep 2
cd ${TOMCATWEBAPPSDIR}/
echo cp /home/NCR/webfront/WebFrontBaseRemAuthRestServer.war .
cp /home/NCR/webfront/WebFrontBaseRemAuthRestServer.war .
sleep 5
ls -lA
sleep 5
ls -lA

echo cp /home/NCR/webfront/WebFrontBaseRemAuth.war .
cp /home/NCR/webfront/WebFrontBaseRemAuth.war .
sleep 5
ls -lA
sleep 5
ls -lA
sleep 5

sleep 2
cd ${TOMCATWEBAPPSDIR}/
echo cp /home/NCR/webfront/WebFrontBaseRestServer.war .
cp /home/NCR/webfront/WebFrontBaseRestServer.war .
sleep 5
ls -lA
sleep 5
ls -lA

echo cp /home/NCR/webfront/WebFrontBase.war .
cp /home/NCR/webfront/WebFrontBase.war .
sleep 5
ls -lA
sleep 5
ls -lA
sleep 5
ls -lA
sleep 5
ls -lA
sleep 5
ls -lA
