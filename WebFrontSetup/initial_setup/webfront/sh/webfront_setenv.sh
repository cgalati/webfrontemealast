#!/bin/sh 

##############################################################################
#IMPORTANT: 
#   The following variables are read from the ARS server's server's scrpit 
#   SERV_ENV.BAT  and STO_ENV.BAT 
##############################################################################
export WF_ARSSERVER_SRV=
export WF_ARSSERVER_SUP=
export WF_STORE_CODE=

##############################################################################
#IMPORTANT:
#   The following ARS node numbers must have a corresponding row 
#   in ARS server's M_LANXXX.ORG file (and M_LANnnn.DAT file) 
##############################################################################
export WF_ARSSERVER_CRTEOD=810
export WF_ARSSERVER_CRTIDC=811
export WF_ARSSERVER_CRTMNT=812

##############################################################################
#   ARS server home folder, subfolders and other info
##############################################################################
export WF_ARSSERVER_HOMEDIR=/home/NCR/server
export WF_ARSSERVER_PMT_IN_DIR=/home/NCR/server/dp
export WF_ARSSERVER_PLUDRV_FULLPATH=${WF_ARSSERVER_HOMEDIR}/bin/pludrv

##############################################################################
#   ARS Promo-Interface home folder, subfolders and other info
##############################################################################
export WF_PROMO_INTERFACE_HOMEDIR=/home/NCR/promointerface
export WF_PROMO_INTERFACE_IN_DIR=/home/NCR/promointerface/inputfile
export WF_PROMO_INTERFACE_MNT_OUT_DIR=/home/NCR/promointerface/hoc
export WF_PROMO_INTERFACE_PMT_OUT_DIR=/home/NCR/promointerface/dp

##############################################################################
#   ARS IDC-Update home folder, subfolders and other info
##############################################################################
export WF_IDC_UPDATE_HOMEDIR=/home/NCR/idcupdate/IDC

##############################################################################
#   WebFront home folder, subfolders and other infosubfolders
##############################################################################
export WF_HOMEDIR=/home/NCR/webfront
export WF_LOGSDIR=${WF_HOMEDIR}/logs

##############################################################################
#   WebFront MntApply home folder, subfolders and other info
##############################################################################
export WF_MNTAPPLY_HOMEDIR=/home/NCR/webfront-mntapply
export WF_MNTAPPLY_LOGSDIR=${WF_MNTAPPLY_HOMEDIR}/logs
export WF_MNTAPPLY_MNTSRCDIR=${WF_MNTAPPLY_HOMEDIR}/hoc
export WF_MNTAPPLY_BACKDIR=${WF_MNTAPPLY_HOMEDIR}/backup
export WF_MNTAPPLY_WORKDIR=${WF_MNTAPPLY_HOMEDIR}/data
export WF_MNTAPPLY_STOP_FLAG_FILE=${WF_MNTAPPLY_HOMEDIR}/sh/_mntapply_stop_cmd.tmp
export WF_MNTAPPLY_MNTFILES_MASK=?MRECMNT*.DAT
export WF_MNTAPPLY_PROMOFILES_MASK=PROM*.DAT

##############################################################################
#   WebFront EndOfDay home folder, subfolders and other info
##############################################################################
export WF_ENDOFDAY_HOMEDIR=/home/NCR/webfront-endofday
export WF_ENDOFDAY_LOGSDIR=${WF_HOMEDIR}/logs
export WF_ENDOFDAY_STOP_FLAG_FILE=${WF_ENDOFDAY_HOMEDIR}/sh/_endofday_stop_cmd.tmp

          
##############################################################################
#   BEGIN reading and setting variables from the ARS server's server's script 
##############################################################################
TMP_SRV=
TMP_SUP=
TMP_STO=
if [ -f  ${WF_ARSSERVER_HOMEDIR}/sh/SERV_ENV.BAT ] 
then
  TMP_SRV=`sed '/^\#/d' ${WF_ARSSERVER_HOMEDIR}/sh/SERV_ENV.BAT | grep "SRV"  | cut -c22-24`
  TMP_SUP=`sed '/^\#/d' ${WF_ARSSERVER_HOMEDIR}/sh/SERV_ENV.BAT | grep "SUP"  | cut -c20-22`
fi
if [ -f  ${WF_ARSSERVER_HOMEDIR}/sh/STO_ENV.BAT ] 
then
  export TMP_STO=`sed '/^\#/d' ${WF_ARSSERVER_HOMEDIR}/sh/STO_ENV.BAT | grep "STO"  | cut -c21-24`
fi
if [ "${TMP_SRV}" != "" ]
then
#   echo '   lettura della variabile SRV dal file SERV_ENV.BAT'
   export WF_ARSSERVER_SRV=${TMP_SRV}
   export SRV=${TMP_SRV}
fi
if [ "${TMP_SUP}" != "" ]
then
#   echo '   lettura della variabile SUP dal file SERV_ENV.BAT'
   export WF_ARSSERVER_SUP=${TMP_SUP}
   export SUP=${TMP_SUP}
fi
if [ "${TMP_STO}" != "" ]
then
#   echo '   lettura della variabile STO dal file SERV_ENV.BAT'
   export WF_STORE_CODE=${TMP_STO}
   export STO=${TMP_STO}
fi
##############################################################################
#   END   reading and setting variables from the ARS server's server's script 
##############################################################################

##############################################################################
#   BEGIN writing the arsserverenv.properties file (used by WebFront web apps) 
##############################################################################
WF_SRVENVCONF=${WF_HOMEDIR}/conf/arsserverenv.properties
rm -f ${WF_SRVENVCONF} 
echo '# This file is generated by ' $0 ' every reboot'           >>${WF_SRVENVCONF}
echo 'WF_ARSSERVER_HOMEDIR       ='${WF_ARSSERVER_HOMEDIR}       >>${WF_SRVENVCONF}
echo 'WF_ARSSERVER_SUP           ='${WF_ARSSERVER_SUP}           >>${WF_SRVENVCONF}
echo 'WF_ARSSERVER_SRV           ='${WF_ARSSERVER_SRV}           >>${WF_SRVENVCONF}
echo 'WF_ARSSERVER_CRTEOD        ='${WF_ARSSERVER_CRTEOD}        >>${WF_SRVENVCONF}
echo 'WF_MNTAPPLY_HOMEDIR        ='${WF_MNTAPPLY_HOMEDIR}        >>${WF_SRVENVCONF}
echo 'WF_MNTAPPLY_MNTSRCDIR      ='${WF_MNTAPPLY_MNTSRCDIR}      >>${WF_SRVENVCONF}
echo >>${WF_SRVENVCONF}
chmod 777 ${WF_SRVENVCONF}
##############################################################################
#   END   writing the arsserverenv.properties file (used by WebFront web apps) 
##############################################################################

##############################################################################
#   BEGIN writing the webfrontenv.properties file (used by WebFront web apps) 
##############################################################################
WF_ENVCONF=${WF_HOMEDIR}/conf/webfrontenv.properties
rm -f ${WF_ENVCONF} 
echo '# This file is generated by ' $0 ' every reboot'           >>${WF_ENVCONF}
echo 'WF_STORE_CODE           ='${WF_STORE_CODE}           >>${WF_ENVCONF}
echo >>${WF_ENVCONF}
chmod 777 ${WF_ENVCONF}
##############################################################################
#   END   writing the webfrontenv.properties file (used by WebFront web apps) 
##############################################################################

if [ "$1" == "status" ]
then
  echo Ambiente WebFront:
  cat ${WF_ENVCONF}
fi

FOUND=`id | grep "root"	`
if [ -n "$FOUND" ]
then
   if [ ! -d ${WF_LOGSDIR} ] 
   then
      mkdir ${WF_LOGSDIR}
   fi
   chown webfront:asr ${WF_LOGSDIR} >/dev/null 2>/dev/null
   chown webfront:asr ${WF_LOGSDIR}/* >/dev/null 2>/dev/null
   chmod 777 ${WF_LOGSDIR} >/dev/null 2>/dev/null
   chmod 777 ${WF_LOGSDIR}/* >/dev/null 2>/dev/null

   if [ ! -d ${WF_MNTAPPLY_LOGSDIR} ] 
   then
      mkdir ${WF_MNTAPPLY_LOGSDIR}
   fi
   chown webfront:asr ${WF_MNTAPPLY_LOGSDIR} >/dev/null 2>/dev/null
   chown webfront:asr ${WF_MNTAPPLY_LOGSDIR}/* >/dev/null 2>/dev/null
   chmod 777 ${WF_MNTAPPLY_LOGSDIR} >/dev/null 2>/dev/null
   chmod 777 ${WF_MNTAPPLY_LOGSDIR}/* >/dev/null 2>/dev/null

   if [ ! -d ${WF_ENDOFDAY_LOGSDIR} ] 
   then
      mkdir ${WF_ENDOFDAY_LOGSDIR}
   fi
   chown webfront:asr ${WF_ENDOFDAY_LOGSDIR} >/dev/null 2>/dev/null
   chown webfront:asr ${WF_ENDOFDAY_LOGSDIR}/* >/dev/null 2>/dev/null
   chmod 777 ${WF_ENDOFDAY_LOGSDIR} >/dev/null 2>/dev/null
   chmod 777 ${WF_ENDOFDAY_LOGSDIR}/* >/dev/null 2>/dev/null

fi
