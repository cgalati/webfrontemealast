@ECHO OFF

REM ##############################################################################
REM IMPORTANT: 
REM    The following variables are read from the ARS server's server's scrpit 
REM    SERV_ENV.BAT  and STO_ENV.BAT 
REM ##############################################################################
SET WF_ARSSERVER_SRV=
SET WF_ARSSERVER_SUP=
SET WF_STORE_CODE=

REM ##############################################################################
REM IMPORTANT:
REM    The following ARS node numbers must have a corresponding row 
REM    in ARS server's M_LANXXX.ORG file (and M_LANnnn.DAT file) 
REM ##############################################################################
SET WF_ARSSERVER_CRTEOD=810
SET WF_ARSSERVER_CRTIDC=811
SET WF_ARSSERVER_CRTMNT=812

REM ##############################################################################
REM    ARS server home folder, subfolders and other info
REM ##############################################################################
SET WF_ARSSERVER_HOMEDIR=C:\server
SET WF_ARSSERVER_PMT_IN_DIR=C:\server\dp
SET WF_ARSSERVER_PLUDRV_FULLPATH=%WF_ARSSERVER_HOMEDIR%\bin\pludrv.exe

REM ##############################################################################
REM    ARS Promo-Interface home folder, subfolders and other info
REM ##############################################################################
SET WF_PROMO_INTERFACE_HOMEDIR=%PROGRAMFILES%\promointerface
SET WF_PROMO_INTERFACE_IN_DIR=%WF_PROMO_INTERFACE_HOMEDIR%\inputfile
SET WF_PROMO_INTERFACE_MNT_OUT_DIR=%WF_PROMO_INTERFACE_HOMEDIR%\hoc
SET WF_PROMO_INTERFACE_PMT_OUT_DIR=%WF_PROMO_INTERFACE_HOMEDIR%\dp

REM ##############################################################################
REM    ARS IDC-Update home folder, subfolders and other info
REM ##############################################################################
SET WF_IDC_UPDATE_HOMEDIR=%WF_ARSSERVER_HOMEDIR%\idc

REM ##############################################################################
REM    WebFront home folder, subfolders and other infosubfolders
REM ##############################################################################
SET WF_HOMEDIR=%ProgramFiles%\webfront
SET WF_LOGSDIR=%WF_HOMEDIR%\logs

REM ##############################################################################
REM    WebFront MntApply home folder, subfolders and other info
REM ##############################################################################
SET WF_MNTAPPLY_HOMEDIR=%ProgramFiles%\webfront-mntapply
SET WF_MNTAPPLY_LOGSDIR=%WF_MNTAPPLY_HOMEDIR%\logs
SET WF_MNTAPPLY_MNTSRCDIR=%WF_MNTAPPLY_HOMEDIR%\hoc
SET WF_MNTAPPLY_BACKDIR=%WF_MNTAPPLY_HOMEDIR%\backup
SET WF_MNTAPPLY_WORKDIR=%WF_MNTAPPLY_HOMEDIR%\data
SET WF_MNTAPPLY_STOP_FLAG_FILE=%WF_MNTAPPLY_HOMEDIR%\sh\_mntapply_stop_cmd.tmp
SET WF_MNTAPPLY_MNTFILES_MASK=?MRECMNT*.DAT
SET WF_MNTAPPLY_PROMOFILES_MASK=PROM*.DAT

REM ##############################################################################
REM   WebFront EndOfDay home folder, subfolders and other info
REM ##############################################################################
SET WF_ENDOFDAY_HOMEDIR=%ProgramFiles%\webfront-endofday
SET WF_ENDOFDAY_LOGSDIR=%WF_ENDOFDAY_HOMEDIR%\logs
SET WF_ENDOFDAY_STOP_FLAG_FILE=%WF_ENDOFDAY_HOMEDIR%\sh\_endofday_stop_cmd.tmp

REM ##############################################################################
REM    reading and setting variables from the ARS server's server's script 
REM ##############################################################################
CALL C:\Server\sh\SERV_ENV.BAT
SET WF_ARSSERVER_SRV=%SRV%
SET WF_ARSSERVER_SUP=%SUP%
SET WF_STORE_CODE=%STO%


REM ##############################################################################
REM    SKIP writing the arsserverenv.properties file 
REM ##############################################################################

IF "%1"=="NOECHO" GOTO :NOECHO
ECHO.
ECHO.
ECHO WF_ARSSERVER_CRTEOD            =%WF_ARSSERVER_CRTEOD%
ECHO WF_ARSSERVER_CRTIDC            =%WF_ARSSERVER_CRTIDC%
ECHO WF_ARSSERVER_CRTMNT            =%WF_ARSSERVER_CRTMNT%
ECHO WF_ARSSERVER_HOMEDIR           =%WF_ARSSERVER_HOMEDIR%
ECHO WF_ARSSERVER_PMT_IN_DIR        =%WF_ARSSERVER_PMT_IN_DIR%
ECHO WF_ARSSERVER_PLUDRV_FULLPATH   =%WF_ARSSERVER_PLUDRV_FULLPATH%
ECHO WF_PROMO_INTERFACE_HOMEDIR     =%WF_PROMO_INTERFACE_HOMEDIR%
ECHO WF_PROMO_INTERFACE_IN_DIR      =%WF_PROMO_INTERFACE_IN_DIR%
ECHO WF_PROMO_INTERFACE_MNT_OUT_DIR =%WF_PROMO_INTERFACE_MNT_OUT_DIR%
ECHO WF_PROMO_INTERFACE_PMT_OUT_DIR =%WF_PROMO_INTERFACE_PMT_OUT_DIR%
ECHO WF_IDC_UPDATE_HOMEDIR          =%WF_IDC_UPDATE_HOMEDIR%
ECHO WF_HOMEDIR                     =%WF_HOMEDIR%
ECHO WF_LOGSDIR                     =%WF_LOGSDIR%
ECHO WF_MNTAPPLY_HOMEDIR            =%WF_MNTAPPLY_HOMEDIR%
ECHO WF_MNTAPPLY_LOGSDIR            =%WF_MNTAPPLY_LOGSDIR%
ECHO WF_MNTAPPLY_MNTSRCDIR          =%WF_MNTAPPLY_MNTSRCDIR%
ECHO WF_MNTAPPLY_BACKDIR            =%WF_MNTAPPLY_BACKDIR%
ECHO WF_MNTAPPLY_WORKDIR            =%WF_MNTAPPLY_WORKDIR%
ECHO WF_MNTAPPLY_STOP_FLAG_FILE     =%WF_MNTAPPLY_STOP_FLAG_FILE%
ECHO WF_MNTAPPLY_MNTFILES_MASK      =%WF_MNTAPPLY_MNTFILES_MASK%
ECHO WF_MNTAPPLY_PROMOFILES_MASK    =%WF_MNTAPPLY_PROMOFILES_MASK%
ECHO WF_ENDOFDAY_HOMEDIR            =%WF_ENDOFDAY_HOMEDIR%
ECHO WF_ENDOFDAY_LOGSDIR            =%WF_ENDOFDAY_LOGSDIR%
ECHO WF_ENDOFDAY_STOP_FLAG_FILE     =%WF_ENDOFDAY_STOP_FLAG_FILE%
ECHO WF_ARSSERVER_SRV               =%WF_ARSSERVER_SRV%
ECHO WF_ARSSERVER_SUP               =%WF_ARSSERVER_SUP%
ECHO WF_STORE_CODE                  =%WF_STORE_CODE%

:NOECHO

IF "%1"=="NOPAUSE" GOTO :NOPAUSE
PAUSE


:NOPAUSE

                                    