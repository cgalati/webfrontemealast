#!/bin/sh 

doinstallservice() {
  if hash insserv 2>/dev/null; then
       echo calling insserv WebFront-MntApply
       insserv WebFront-MntApply
  else
     #AZL CENTOS#BEG
     echo calling chkconfig WebFront-MntApply on
     chkconfig WebFront-MntApply on
     #AZL CENTOS#END
  fi
}

echo Inizio WebFront-MntApply setup 
FOUND=`id | grep "root"	`
if [ -n "$FOUND" ]
then
   echo ' '  >/dev/null
else
   echo ' ' 
   echo "ATTENZIONE:"
   echo "   lo script NON puo' essere eseguito da un utente diverso da root!!!"
   echo "   Fare il logoff dell'utente corrente e fare il login con l'utente root"
   echo ' ' 
   echo Fine   WebFront-MntApply setup 
   exit -1 
fi

if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi


if [ "${WF_HOMEDIR}" == "" ]
then
  echo 'Missing env variable WF_HOMEDIR.'
  echo 'Setup cancelled.'
  echo Fine   WebFront-MntApply setup 
  exit -1
fi
if [ ! -d ${WF_HOMEDIR} ]
then
  echo 'Missing webfront home folder: '${WF_HOMEDIR}
  echo 'Setup cancelled.'
  echo Fine   WebFront-MntApply setup 
  exit -1
fi

if [ "${WF_MNTAPPLY_HOMEDIR}" == "" ]
then
  echo 'Missing env variable WF_MNTAPPLY_HOMEDIR.'
  echo 'Setup cancelled.'
  echo Fine   WebFront-MntApply setup 
  exit -1
fi
if [ ! -d ${WF_MNTAPPLY_HOMEDIR} ]
then
  echo 'Missing mntapply home folder: '${WF_MNTAPPLY_HOMEDIR}
  echo 'Setup cancelled.'
  echo Fine   WebFront-MntApply setup 
  exit -1
fi

FOLDERTOCREATE=${WF_MNTAPPLY_WORKDIR}
if [ ! -d ${FOLDERTOCREATE} ]
then
   echo '   Creating '${FOLDERTOCREATE}' directory...'
   mkdir ${FOLDERTOCREATE}
fi

FOLDERTOCREATE=${WF_MNTAPPLY_DATADIR}
if [ ! -d ${FOLDERTOCREATE} ]
then
   echo '   Creating '${FOLDERTOCREATE}' directory...'
   mkdir ${FOLDERTOCREATE}
fi

FOLDERTOCREATE=${WF_MNTAPPLY_MNTSRCDIR}
if [ ! -d ${FOLDERTOCREATE} ]
then
   echo '   Creating '${FOLDERTOCREATE}' directory...'
   mkdir ${FOLDERTOCREATE}
fi

FOLDERTOCREATE=${WF_MNTAPPLY_HOMEDIR}/sh
if [ ! -d ${FOLDERTOCREATE} ]
then
   echo '   Creating '${FOLDERTOCREATE}' directory...'
   mkdir ${FOLDERTOCREATE}
fi

echo '   Changing '${WF_MNTAPPLY_HOMEDIR}' user and group...'
if [ ! "${WF_MNTAPPLY_HOMEDIR}" == "" ]
then
   chmod -R 777 ${WF_MNTAPPLY_HOMEDIR}
fi


echo '   Changing '${WF_MNTAPPLY_HOMEDIR}' permissions...'
chown -R webfront:asr ${WF_MNTAPPLY_HOMEDIR}

echo '   Installazione servizio...'
chown root:root ${WF_MNTAPPLY_HOMEDIR}/sh/_startup_mntapply.sh
rm -f /etc/init.d/WebFront-MntApply
ln -s ${WF_MNTAPPLY_HOMEDIR}/sh/_startup_mntapply.sh /etc/init.d/WebFront-MntApply
chown root:root /etc/init.d/WebFront-MntApply

doinstallservice

echo Fine   WebFront-MntApply setup 
              
