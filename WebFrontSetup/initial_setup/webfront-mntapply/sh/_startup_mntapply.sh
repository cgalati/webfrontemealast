#!/bin/bash
#AZL CENTOS START
# chkconfig: 35 80 30
# description: WebFront-MntApply
#
#AZL CENTOS END
export SUBSYSNAME='WebFront-MntApply'

if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi

status() {

   if [ ! "${WF_MNTAPPLY_HOMEDIR}" == "" ]
   then
      chmod -R 777 ${WF_MNTAPPLY_HOMEDIR}
   fi
   su - webfront -c "${WF_MNTAPPLY_HOMEDIR}/sh/mntapply_service.sh status"
	 RETVAL=0
}

start() {

   if [ ! "${WF_MNTAPPLY_HOMEDIR}" == "" ]
   then
      chmod -R 777 ${WF_MNTAPPLY_HOMEDIR}
   fi
   su - webfront -c "${WF_MNTAPPLY_HOMEDIR}/sh/mntapply_service.sh start"
   RETVAL=0
}

stop() {
   if [ ! "${WF_MNTAPPLY_HOMEDIR}" == "" ]
   then
      chmod -R 777 ${WF_MNTAPPLY_HOMEDIR}
   fi
   su - webfront -c "${WF_MNTAPPLY_HOMEDIR}/sh/mntapply_service.sh stop"
   rm -f /var/lock/subsys/$SUBSYSNAME
	RETVAL=0

}

FUNC=$1



case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
	status
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo "Usage: $0 {start|stop|status|restart}"
	exit 1
esac

exit $RETVAL
