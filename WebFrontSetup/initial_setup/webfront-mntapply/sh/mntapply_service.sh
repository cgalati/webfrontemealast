#!/bin/bash

if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi

status(){
cd ${WF_MNTAPPLY_HOMEDIR}/sh
if [ -f _mntapply.pid ]
then
        MNTPID=`cat _mntapply.pid`
        # il processo e' ancora in esecuzione?
        if ps -p $MNTPID >/dev/null 2>/dev/null
        then
                if [ "$FUNC" == "status" ]
                then
                   echo "MntApply active"
                fi
	        STATUS=1
        else
                if  [ "$FUNC" == "status" ]
                then
                   echo "MntApply not active"
                fi
	        STATUS=0
          rm -f _mntapply.pid
        fi
else
	if [ "$FUNC" == "status" ]
	then
           echo "MntApply not active"
        fi
	STATUS=0
fi
}

start() {

	#echo -n $"Starting WebFront-MntApply service: "
	echo "Starting WebFront-MntApply service: "
	cd ${WF_MNTAPPLY_HOMEDIR}/sh

        FUNC=start
        status
        if [ "$STATUS" -ne "0" ]
        then
           echo "WebFront-MntApply service already active"
           exit
        fi
	
 	/usr/bin/nohup ${WF_MNTAPPLY_HOMEDIR}/sh/mntapply.sh mntapply >/dev/null  2>&1 &
	sleep 2

        FUNC=status
        status
	
 	RETVAL=$?
	RETVAL=0
}

stop() {
	#echo -n $"Stopping WebFront-MntApply service: "
	echo "Stopping WebFront-MntApply service: "

  FUNC=stop

	cd ${WF_MNTAPPLY_HOMEDIR}/sh

  status 
  if [ "$STATUS" -ne "0" ]
  then
     if [ -f _mntapply.pid ]
     then
	      echo stop >${WF_MNTAPPLY_STOP_FLAG_FILE}

        TIMEOUT=20
        for ((i=1; i<=$TIMEOUT; i++))
        do
           sleep 1
           if [ ! -f ${WF_MNTAPPLY_STOP_FLAG_FILE} ]
           then
                 break
           fi
        done

        if [ -f ${WF_MNTAPPLY_STOP_FLAG_FILE} ]
        then
          if [ -f _mntapply.pid ]
          then
             MNTPID=`cat _mntapply.pid`
             kill -15 $MNTPID
             rm _mntapply.pid
          fi
        fi
        rm -f _mntapply.pid
        rm -f ${WF_MNTAPPLY_STOP_FLAG_FILE}
     fi
  fi

	sleep 2

        FUNC=status
        status

 	RETVAL=$?
	RETVAL=0

}

FUNC=$1


case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
	status
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
	echo "Usage: $0 {start|stop|status|restart}"
	exit 1
esac

exit $RETVAL
