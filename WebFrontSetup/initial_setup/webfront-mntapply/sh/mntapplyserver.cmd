@ECHO OFF

SET SLEEPSEC=300

TITLE WebFront-MntApply

CD "%ProgramFiles%\webfront-mntapply"

SET WORK_FILE=GMRECMNT.DAT
SET MNT_FILES=*MRECMNT*.DAT
SET DAT_FILES=PROM*.DAT
SET SELE_FILE=GMSELECT.DAT
SET OFFL_FILE=GMOFF%SRV%.DAT
SET WORK_DIR="%ProgramFiles%\webfront-mntapply"
SET FILE_DIR="%ProgramFiles%\webfront-mntapply\hoc"
SET SHEL_DIR="%ProgramFiles%\webfront-mntapply\sh"
SET SLEEPCMD="%ProgramFiles%\webfront-mntapply\bin\SLEEP.EXE"
SET PIF_DIR=C:\NCR\promointerface TODO
SET TIMEOUT=10
SET BACK_DIR="%ProgramFiles%\webfront-mntapply\backup"
SET MNTLOG="%ProgramFiles%\webfront-mntapply\logs\mntapply.log"
SET DBG_MODE=0
SET PARAM=-v
SET PROTOCOL=tcp
SET REG=%CLIENT_MNTAPPLY%



rem echo ........... >> %MNTLOG%
rem echo par 1: %1   >> %MNTLOG%
rem echo par 2: %2 >> %MNTLOG%
rem echo par 3: %3 >> %MNTLOG%
rem echo par 4: %4 >> %MNTLOG%
rem echo ......... >> %MNTLOG%
rem echo SRV = -%SRV%-  >> %MNTLOG%


REM ###############################################################
REM # Controllo che non sia chiamato direttamente
REM ###############################################################     

	IF NOT %1% == mntapply GOTO PARAM

REM ###############################################################
REM # Controllo se debug mode
REM ###############################################################

	IF NOT %2% == 1 SET DBG_MODE=0
	IF %DBG_MODE% == 0 SET PARAM=" "

REM ###############################################################
REM # Invocazione manuale (sincrona)
REM ###############################################################
	IF "%3" == "" GOTO BEGIN

	IF %DBG_MODE% == 1 echo RECORD a : 'A000060000000GMRECMNT.DAT                    ' >> %MNTLOG%
	echo A000060000000GMRECMNT.DAT                     >%WORK_DIR%/%SELE_FILE%

	IF %DBG_MODE% == 1 echo RECORD b : 'S0%SRV%00790000GMTRALOG.DAT    GMTRALOG.DAT    ' >> %MNTLOG%
	echo "S0%SRV%00790000GMTRALOG.DAT    GMTRALOG.DAT    " >>%WORK_DIR%/%SELE_FILE%

	IF EXIST %WORK_DIR%/%WORK_FILE% MOVE /Y %WORK_DIR%/%WORK_FILE% %WORK_DIR%/%WORK_FILE%.SAVTEMP
	TYPE %FILE_DIR%/%3% >> %WORK_DIR%/%WORK_FILE%
	IF EXIST %BACK_DIR% MOVE /Y %FILE_DIR%/%3% %BACK_DIR%/
	DEL /F /Q %FILE_DIR%/%3%
	REM 	SET STEP=1
	REM GOTO CREATE_GMSELECT
	CALL :CREATE_GMSELECT
	REM :STEP1	
	
	PUSHD %WORK_DIR%
	IF %DBG_MODE% == 1 echo Chiamo pludrv..... >> %MNTLOG%
	
	IF %DBG_MODE% == 1 echo Esecuzione comando %SRV_PATH%\bin\pludrv %PARAM% %SELE_FILE%... >> %MNTLOG%
	IF %DBG_MODE% == 1 echo BEGIN SELE_FILE: >> %MNTLOG%
	IF %DBG_MODE% == 1 TYPE %SELE_FILE% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo END   SELE_FILE: >> %MNTLOG%
	%SRV_PATH%\bin\pludrv %PARAM% %SELE_FILE% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo ...completata esecuzione comando %SRV_PATH%\bin\pludrv %PARAM% %SELE_FILE% >> %MNTLOG%

	REM SET STEP=2
	REM 	IF NOT ERRORLEVEL == 0 GOTO PLUDRV_ERROR
	IF NOT ERRORLEVEL == 0 CALL :PLUDRV_ERROR
	IF ERRORLEVEL == 0 CALL :PLUDRV_OK

	REM 	IF ERRORLEVEL == 0 echo Chiamata a pludrv eseguita con successo. >> %MNTLOG%

	REM :STEP2
	REM  IF ERRORLEVEL == 0 DEL /F /Q %WORK_DIR%\%WORK_FILE%
	REM   IF ERRORLEVEL == 0 DEL /F /Q %WORK_DIR%\%SELE_FILE%
	REM   SET STEP=0
	REM	cd -
  	POPD
  	
	IF EXIST %WORK_DIR%\%WORK_FILE%.SAVTEMP MOVE /Y %WORK_DIR%\%WORK_FILE%  
  	GOTO :END   
   

:BEGIN 
   	IF %DBG_MODE% == 1 GOTO PRINT_INFO
:BEGIN2                                 
   	IF EXIST STOP.CTL GOTO :STOP
   	TITLE MNTAPLY PROCESS
   	%SRV_PATH%\BIN\showwin.exe HIDE -i mntapply.ico
   	%SLEEPCMD% 5

   	IF %DBG_MODE% == 1 echo ...APPLICAZIONE FILE PROMOZIONALI %DAT_FILES%  >> %MNTLOG%
   	FOR %%i IN (%FILE_DIR%\%DAT_FILES%) DO CALL :SENDPROMO %%i %DBG_MODE%

   	IF %DBG_MODE% == 1 echo ...APPLICAZIONE FILE PROMOZIONALI DA ASARFRONT  >> %MNTLOG%
	  IF %DBG_MODE% == 1 echo %FILE_DIR%\DOEXPORT.DAT >> %MNTLOG%
   	IF EXIST %FILE_DIR%\DOEXPORT.DAT CALL :SENDASARPROMO %DBG_MODE%

   	IF %DBG_MODE% == 1 echo ...CONCATENAZIONE FILE DI MANTENANCE %MNT_FILES%  >> %MNTLOG%
   	FOR %%b IN (%FILE_DIR%\%MNT_FILES%) DO CALL :APPENDMNT %%b %DBG_MODE%

   	IF %DBG_MODE% == 1 echo ...APPLICAZIONE FILE DI MANTENANCE %WORK_FILE%  >> %MNTLOG%
   	IF EXIST %WORK_DIR%\%WORK_FILE% CALL :APPLYMNT %DBG_MODE%

   	IF %DBG_MODE% == 1 echo ...APPLICAZIONE FILE DI MANTENANCE %OFFL_FILE%  >> %MNTLOG%
   	IF EXIST %WORK_DIR%\%OFFL_FILE% CALL :APPLYMNT %DBG_MODE%
GOTO :BEGIN


:STOP
   	%SRV_PATH%\BIN\showwin.exe RESTORE
   	Echo Progrmma fermato dall'utente (STOP.CTL).
   	DEL STOP.CTL
   	%SLEEPCMD% 5
   	GOTO :END

REM *******************************************************
REM :SENDPROMO ASARFRONT BEGIN
REM *******************************************************
:SENDASARPROMO
	ECHO pippo
  	SET DBG_MODE=%1%

	PUSHD %SHEL_DIR%
  	call mntapply_env.bat
	POPD

  	PUSHD %PIF_DIR%
  	IF %DBG_MODE% == 1 echo chiamo asr_promoInterface con parametri >> %MNTLOG%    
  	IF EXIST %FILE_DIR%\DOEXPORT.DAT CALL ASR_PromoInterface.bat A -src 1
  	IF EXIST %FILE_DIR%\DOEXPORT.DAT CALL ASR_PromoInterface.bat A -src 4
  	POPD

  	IF EXIST %FILE_DIR%\DOEXPORT.DAT DEL /F /Q %FILE_DIR%\DOEXPORT.DAT

	%SLEEPCMD% 10
  	REM IF %DBG_MODE% == 1 echo copia file pmt in server\dp ... >> %MNTLOG%    
	REM FOR %%a IN (%PIF_DIR%\dp\*.PMT) COPY /F %%a %SRV_PATH%\DP

  	IF %DBG_MODE% == 1 echo cancellazioen file pmt da cartella PI ... >> %MNTLOG%    
	DEL /F /Q %PIF_DIR%\dp\*.PMT
	
  	IF %DBG_MODE% == 1 echo importo eventuali wrec per applicazione ... >> %MNTLOG%    
	IF EXIST %PIF_DIR%\hoc\WMRECMNT.DAT COPY /F %PIF_DIR%\hoc\WMRECMNT.DAT %FILE_DIR% 
	
	GOTO :EOF

REM *******************************************************
REM :SENDPROMO BEGIN
REM *******************************************************
:SENDPROMO
  	SET SENDPROMO1=%1%
  	SET DBG_MODE=%2%

	PUSHD %SHEL_DIR%
  	call mntapply_env.bat
	POPD

  	IF %DBG_MODE% == 1 echo applicazione PROMO.DAT >> %MNTLOG%
  	IF %DBG_MODE% == 1 echo copia %SENDPROMO1% %PIF_DIR%\inputfile >> %MNTLOG%
  	COPY /Y %SENDPROMO1% %PIF_DIR%\inputfile
  
  	IF %DBG_MODE% == 1 echo Esecuzione comando di spostamento file %SENDPROMO1% in %BACK_DIR%\ ... >> %MNTLOG%
  	IF EXIST %BACK_DIR% MOVE /Y %1%  %BACK_DIR%\
  
  	IF %DBG_MODE% == 1 echo ... completata esecuzione comando MOVE /Y %1% %BACK_DIR%\ .. >> %MNTLOG%
  	IF %DBG_MODE% == 1 echo Esecuzione comando DEL /F /Q %SENDPROMO1%... >> %MNTLOG%
  
  	DEL /F /Q %SENDPROMO1%
  	IF %DBG_MODE% == 1 echo ... completata esecuzione comando DEL /F /Q %SENDPROMO1%... >> %MNTLOG%

  	PUSHD %PIF_DIR%
  	IF %DBG_MODE% == 1 echo esecuzione asr_promoInterface ... >> %MNTLOG%  

  	IF EXIST inputfile\%DAT_FILES% CALL ASR_PromoInterface.bat
  	IF %DBG_MODE% == 1 echo asr_promoInterface chiamata correttamente... >> %MNTLOG%    
  	POPD

	%SLEEPCMD% 10
  	REM IF %DBG_MODE% == 1 echo copia file pmt in server\dp ... >> %MNTLOG%    
	REM FOR %%a IN (%PIF_DIR%\dp\*.PMT) COPY /F %%a %SRV_PATH%\DP

  	IF %DBG_MODE% == 1 echo cancellazioen file pmt da cartella PI ... >> %MNTLOG%    
	DEL /F /Q %PIF_DIR%\dp\*.PMT
	
  	IF %DBG_MODE% == 1 echo importo eventuali wrec per applicazione ... >> %MNTLOG%    
	IF EXIST %PIF_DIR%\hoc\WMRECMNT.DAT COPY /F %PIF_DIR%\hoc\WMRECMNT.DAT %FILE_DIR% 
	
	GOTO :EOF
REM *******************************************************
REM :SENDPROMO END
REM *******************************************************

REM *******************************************************
REM :APPENDMNT BEGIN
REM *******************************************************
:APPENDMNT  
	SET APPENDMNT1=%1%
	SET DBG_MODE = %2%

	PUSHD %SHEL_DIR%
  	call mntapply_env.bat
	POPD

	IF %DBG_MODE% == 1 echo eseguo comando TYPE su file %APPENDMNT1% >> %MNTLOG%
	TYPE %APPENDMNT1% >> %WORK_DIR%\%WORK_FILE%
	IF EXIST %BACK_DIR% MOVE /Y %APPENDMNT1% %BACK_DIR%\
	DEL /F /Q %APPENDMNT1%
	IF %DBG_MODE% == 1 echo ... completata esecuzione comando DEL /F /Q %APPENDMNT1% >> %MNTLOG%  
  	GOTO :EOF

REM *******************************************************
REM :APPENDMNT END
REM *******************************************************

REM *******************************************************
REM :APPLYMNT BEGIN
REM *******************************************************  
:APPLYMNT
	SET DBG_MODE = %1%
	CALL :CREATE_GMSELECT %DBG_MODE%
	call mntapply_env.bat

	IF %DBG_MODE% == 1 echo File found. >> %MNTLOG%
	IF %DBG_MODE% == 1 echo dir di lavoro...%WORK_DIR%\%WORK_FILE% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo file di select..%WORK_DIR%\%SELE_FILE% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo server..........%SRV_PATH% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo ... >> %MNTLOG%

	PUSHD %WORK_DIR%

	IF %DBG_MODE% == 1 echo Chiamo pludrv..... >> %MNTLOG%

	IF %DBG_MODE% == 1 echo BEGIN SELE_FILE: >> %MNTLOG%
  IF %DBG_MODE% == 1 TYPE %SELE_FILE% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo END   SELE_FILE: >> %MNTLOG%
	
	IF %DBG_MODE% == 1 echo Esecuzione comando %SRV_PATH%\bin\pludrv %PARAM% %SELE_FILE%... >> %MNTLOG%
	%SRV_PATH%\bin\pludrv %PARAM% %SELE_FILE% >> %MNTLOG%

	IF %DBG_MODE% == 1 echo ...completata esecuzione comando %SRV_PATH%\bin\pludrv %PARAM% %SELE_FILE% >> %MNTLOG%
	IF %DBG_MODE% == 1 echo Eseguito pludrv..... >> %MNTLOG%
	IF NOT ERRORLEVEL == 0 CALL :PLUDRV_ERROR %MNTLOG%
    	IF ERRORLEVEL == 0 CALL :PLUDRV_OK %DBG_MODE%
    	POPD 
REM *******************************************************
REM :APPLYMNT END
REM *******************************************************


REM *******************************************************
REM :SLEEP BEGIN
REM *******************************************************
:SLEEP
	IF %DBG_MODE% == 1 ECHO Attesa %1 secondi...
	%SLEEPCMD% %1
	IF %DBG_MODE% == 1 ECHO ...Fine Attesa
	GOTO :EOF

REM *******************************************************
REM :SLEEP END
REM *******************************************************

REM *******************************************************
REM :CREATE_GMSELECT BEGIN
REM *******************************************************
:CREATE_GMSELECT
	SET DBG_MODE = %1%
	call mntapply_env.bat

	IF %DBG_MODE% == 1 echo create_gmselect... >> %MNTLOG%

	IF %DBG_MODE% == 1 echo Record a: A000060000000GMRECMNT.DAT                     >>%MNTLOG%
	echo A000060000000GMRECMNT.DAT                    >%WORK_DIR%\%SELE_FILE%
	IF %DBG_MODE% == 1 echo Record b: S0%SRV%00790000GMTRALOG.DAT    GMTRALOG.DAT     >>%MNTLOG%
	echo S0%SRV%00790000GMTRALOG.DAT    GMTRALOG.DAT     >>%WORK_DIR%\%SELE_FILE% 
	GOTO :EOF
  
REM *******************************************************
REM :CREATE_GMSELECT END
REM *******************************************************
    
REM *******************************************************
REM :PLUDRV_ERROR BEGIN
REM *******************************************************
:PLUDRV_ERROR
    	SET MNTLOG = %1%
	echo .... >> %MNTLOG%
	echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! >> %MNTLOG%
	echo Chiamata a pludrv eseguita con ERRORE! >> %MNTLOG%
	echo pludrv error! >> %MNTLOG%
	echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! >> %MNTLOG%
	echo .... >> %MNTLOG%
	REM 		IF %STEP% == 2 GOTO STEP2
	GOTO :EOF

REM *******************************************************
REM :PLUDRV_ERROR END
REM ******************************************************* 

REM *******************************************************
REM :PLUDRV_OK BEGIN
REM *******************************************************
:PLUDRV_OK
    	SET DBG_MODE = %1%
    	call mntapply_env.bat
    	IF %DBG_MODE% == 1 echo Chiamata a pludrv eseguita con successo. >> %MNTLOG%
    	DEL /F /Q %WORK_DIR%\%WORK_FILE%
    	DEL /F /Q %WORK_DIR%\%WORK_FILE%
	GOTO :EOF

REM *******************************************************
REM :PLUDRV_OK END 
REM *******************************************************
    

:PRINT_INFO
	echo ******************************************* >> %MNTLOG%
	echo AS@R MAINTENANCE LOOP FOR AS@R F2.x WINDOWS >> %MNTLOG%
	echo ******************************************* >> %MNTLOG%
	echo server path.....%SRV_PATH% >> %MNTLOG%
	echo server num......%SRV% >> %MNTLOG%
	echo reg num.........%REG% >> %MNTLOG%
	echo dir di lavoro...%WORK_DIR% >> %MNTLOG%
	echo dir per file....%FILE_DIR% >> %MNTLOG%
	echo select file.....%SELE_FILE% >> %MNTLOG%
	echo ... >> %MNTLOG%
	echo Accepted files..%MNT_FILES% >> %MNTLOG%
	echo ... >> %MNTLOG%
	echo backup dir......%BACK_DIR% >> %MNTLOG%
	echo ... >> %MNTLOG%
  	GOTO BEGIN2
	
  
:PARAM
	echo .....
	echo You cannot start this program standalone: Start the program mntapply.sh 
	echo ... >> %MNTLOG%
	echo ... >> %MNTLOG%
	echo You should NOT start this program standalone: Start the program mntapply.sh instead >> %MNTLOG%
	echo ... >> %MNTLOG%
	echo ...

:END


