
##############################################################################
#   Read WebFront environment
##############################################################################
if [ -f /home/NCR/webfront/sh/webfront_setenv.sh ]
then
   cd /home/NCR/webfront/sh
   . ./webfront_setenv.sh >/dev/null
   cd -  >/dev/null
fi

##############################################################################
#   Needed for pludrv...
##############################################################################
export REG=${WF_ARSSERVER_CRTMNT}
export SRV=${WF_ARSSERVER_SRV}

##############################################################################
#   Environment variables
##############################################################################
TIMEOUT=10
MNTLOG=${WF_MNTAPPLY_LOGSDIR}/WebFront_MntApply.log
WORK_FILE=GMRECMNT.DAT
SELE_FILE=GMSELECT.DAT
OFFL_FILE=GMOFF${SRV}.DAT

echo "Start MntApply ..." >> $MNTLOG

if [ ${DBG_MODE} = "1" ]
then
	echo "*****************************************" >> $MNTLOG
	echo "WebFront MntApply for AS@R F3.x LINUX    " >> $MNTLOG
	echo "*****************************************" >> $MNTLOG
	echo "server path...............${WF_ARSSERVER_HOMEDIR}" >> $MNTLOG
	echo "server num................${SRV}" >> $MNTLOG
	echo "reg num...................${REG}" >> $MNTLOG
	echo "dir di lavoro.............${WF_MNTAPPLY_WORKDIR}" >> $MNTLOG
	echo "dir di input..............${WF_MNTAPPLY_MNTSRCDIR}" >> $MNTLOG
	echo "dir di backup.............${WF_MNTAPPLY_BACKDIR}" >> $MNTLOG
	echo "stop flag file............${WF_MNTAPPLY_STOP_FLAG_FILE}" >> $MNTLOG
	echo "mnt file mask.............${WF_MNTAPPLY_MNTFILES_MASK}" >> $MNTLOG
	echo "promo file mask...........${WF_MNTAPPLY_PROMOFILES_MASK}" >> $MNTLOG
	echo "select file...............$SELE_FILE" >> $MNTLOG
	echo "offline file..............$OFFL_FILE" >> $MNTLOG
	echo " " >> $MNTLOG   
fi                     

echo ' '    >> $MNTLOG 
echo ' '    >> $MNTLOG 
echo '****************************************************************'    >> $MNTLOG 
echo BEGIN of $0  >> $MNTLOG 
echo '****************************************************************'    >> $MNTLOG 
echo 'To enable debug:'  >> $MNTLOG
echo '   1) stop the WebFront-MntApply service'  >> $MNTLOG
echo '   2) create the file dbg_mode_enabled.flg into folder  '${WF_MNTAPPLY_HOMEDIR}/sh/ >> $MNTLOG
echo '   3) start again the WebFront-MntApply  service'   >> $MNTLOG
echo ' '    >> $MNTLOG 

if [ -f ${WF_MNTAPPLY_HOMEDIR}/sh/dbg_mode_enabled.flg ]
then
   DBG_MODE=1
   PARAM=-v
   env >>$MNTLOG
fi

echo "         " >> $MNTLOG
echo "par 1: $1" >> $MNTLOG
echo "par 2: $2" >> $MNTLOG
echo "par 3: $3" >> $MNTLOG
echo "par 4: $4" >> $MNTLOG
echo "         " >> $MNTLOG
echo "SRV = -${SRV}-"  >> $MNTLOG


# Attenzione! Verificare che la stringa di creazione del file select
# contenga il carattere ^M alla fine della riga
create_gmselect ()
{
	if [ ${DBG_MODE} = "1" ]
	then
		echo "create_gmselect..." >>$MNTLOG
	fi

    echo "Record a: 'A000060000000GMRECMNT.DAT                    '" >>$MNTLOG
    echo "A000060000000GMRECMNT.DAT                    ">${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
    echo "Record b: 'S0${SRV}00790000GMTRALOG.DAT    GMTRALOG.DAT    '" >>$MNTLOG
    echo "S0${SRV}00790000GMTRALOG.DAT    GMTRALOG.DAT    " >>${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
    unix2dos ${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
    chmod 777 ${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
}

###############################################################
# Controllo che non sia chiamato direttamente
###############################################################
if  [ "$1" != "mntapply" ]
then
   echo " "
   echo "You cannot start this program standalone: Start the program mntapply.sh" 
   echo " " >> $MNTLOG
   echo " " >> $MNTLOG
   echo "You should NOT start this program standalone: Start the program mntapply.sh instead" >> $MNTLOG
   echo " " >> $MNTLOG
   echo " "
#   exit
fi

###############################################################
# Invocazione manuale (sincrona)
###############################################################
if [ "$3" = "" ]
then
	MANUAL_MODE=0
else
	echo "RECORD a : 'A000060000000GMRECMNT.DAT                    '" >> $MNTLOG
	echo "A000060000000GMRECMNT.DAT                    " >${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
    	echo "RECORD b : 'S0${SRV}00790000GMTRALOG.DAT    GMTRALOG.DAT    '" >> $MNTLOG
    	echo "S0${SRV}00790000GMTRALOG.DAT    GMTRALOG.DAT    " >>${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
    	if [ -f ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE ]
    	then
    		mv -f ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE.SAVTEMP
    	fi
	cat ${WF_MNTAPPLY_MNTSRCDIR}/$3 >> ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE
	if [ -d ${WF_MNTAPPLY_BACKDIR} ]
	then
	    	mv ${WF_MNTAPPLY_MNTSRCDIR}/$3 ${WF_MNTAPPLY_BACKDIR}/
	else
	    	rm ${WF_MNTAPPLY_MNTSRCDIR}/$3
	fi
	create_gmselect
	cd ${WF_MNTAPPLY_WORKDIR}
	if [ ${DBG_MODE} = "1" ]
	then
		echo "Chiamo ${WF_ARSSERVER_PLUDRV_FULLPATH}..." >> $MNTLOG
	fi
	echo "Esecuzione comando ${WF_ARSSERVER_PLUDRV_FULLPATH} $PARAM $SELE_FILE..." >> $MNTLOG
        echo BEGIN SELE_FILE: >> $MNTLOG
        cat $SELE_FILE >> $MNTLOG
        echo END   SELE_FILE: >> $MNTLOG
	${WF_ARSSERVER_PLUDRV_FULLPATH} $PARAM $SELE_FILE >> $MNTLOG
	echo "...completata esecuzione comando ${WF_ARSSERVER_PLUDRV_FULLPATH} $PARAM $SELE_FILE" >> $MNTLOG
	if [ $? != 0 ]
	then
		echo " " >> $MNTLOG
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> $MNTLOG
		echo "Chiamata a ${WF_ARSSERVER_PLUDRV_FULLPATH} eseguita con ERRORE!" >> $MNTLOG
		echo "${WF_ARSSERVER_PLUDRV_FULLPATH} error!" >> $MNTLOG
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> $MNTLOG
		echo " " >> $MNTLOG
	fi
	if [ ${DBG_MODE} = "1" ]
	then
		echo "Chiamata a ${WF_ARSSERVER_PLUDRV_FULLPATH} eseguita con successo." >> $MNTLOG
	fi
	rm ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE
	rm ${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
	cd -
    	if [ -f ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE.SAVTEMP ]
    	then
    		mv -f ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE.SAVTEMP ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE
    	fi
	exit 
fi


echo $$ > ${WF_MNTAPPLY_HOMEDIR}/sh/_mntapply.pid

while true
do
	if [ ${DBG_MODE} = "1" ]
	then
		echo "waiting for $TIMEOUT sec..." >> $MNTLOG
	fi
  for ((i=1; i<=$TIMEOUT; i++))
  do
     sleep 1
     if [ -f ${WF_MNTAPPLY_STOP_FLAG_FILE} ]
     then
        echo Programma terminato da flag file ${WF_MNTAPPLY_STOP_FLAG_FILE} >> $MNTLOG
         echo Stop MntApply
         rm -f ${WF_MNTAPPLY_STOP_FLAG_FILE}
          echo '****************************************************************'    >> $MNTLOG 
          echo END   of $0  >> $MNTLOG 
          echo '****************************************************************'    >> $MNTLOG 
          echo ' '    >> $MNTLOG 
          echo ' '    >> $MNTLOG 
         exit 0
     fi
  done
	echo "dopo waiting for $TIMEOUT sec..." >> $MNTLOG


  # applicazione dei file PROMO.DAT
  # ---------------------------------------------------------
	if [ ${DBG_MODE} = "1" ]
	then
		echo "applicazione PROMO.DAT" >> $MNTLOG
	fi
	
  CALLPROMOIF=false
	for FILENAME in `ls ${WF_MNTAPPLY_MNTSRCDIR}/${WF_MNTAPPLY_PROMOFILES_MASK}`
	do
      CALLPROMOIF=true
	    chmod 666 $FILENAME
	    echo "cp $FILENAME ${WF_PROMO_INTERFACE_IN_DIR}" >> $MNTLOG
	    cp $FILENAME ${WF_PROMO_INTERFACE_IN_DIR}
	    if [ -d ${WF_MNTAPPLY_BACKDIR} ]
	    then
	    	echo "Esecuzione comando mv -f $FILENAME ${WF_MNTAPPLY_BACKDIR}/ ..." >> $MNTLOG
	    	mv $FILENAME ${WF_MNTAPPLY_BACKDIR}/
	    	echo "... completata esecuzione comando mv -f $FILENAME ${WF_MNTAPPLY_BACKDIR}/ " >> $MNTLOG
	    else
	    	echo "Esecuzione comando rm -f $FILENAME..." >> $MNTLOG
	    	rm $FILENAME
	    	echo "... completata esecuzione comando rm -f $FILENAME" >> $MNTLOG
	    fi
	done 2>/dev/null

	cd ${WF_PROMO_INTERFACE_HOMEDIR}
  if [ "${CALLPROMOIF}" == "true" ]
	then
		echo sh/ASR_PromoInterface.sh >>$MNTLOG
		sh/ASR_PromoInterface.sh
	fi
	cd -
	
	if [ -f ${WF_MNTAPPLY_MNTSRCDIR}/DOEXPORT.DAT ]
	then
	    	echo "Applicazione Promozioni da P.I." >> $MNTLOG
		echo cd ${WF_PROMO_INTERFACE_HOMEDIR} >>$MNTLOG
		cd ${WF_PROMO_INTERFACE_HOMEDIR}
		echo sh/ASR_PromoInterface.sh A -src 1 >>$MNTLOG
		sh/ASR_PromoInterface.sh A -src 1
		cd -
		rm ${WF_MNTAPPLY_MNTSRCDIR}/DOEXPORT.DAT
	fi

	sleep 10
	ls ${WF_PROMO_INTERFACE_PMT_OUT_DIR}/*.PMT  >> $MNTLOG
	for FILENAME in `ls ${WF_PROMO_INTERFACE_PMT_OUT_DIR}/*.PMT`
	do
		echo "cp $FILENAME ${WF_ARSSERVER_PMT_IN_DIR}" >> $MNTLOG
		chmod 666 $FILENAME
		cp $FILENAME ${WF_ARSSERVER_PMT_IN_DIR}
		echo "Esecuzione comando rm -f $FILENAME..." >> $MNTLOG
		rm $FILENAME
		echo "... completata esecuzione comando rm -f $FILENAME" >> $MNTLOG
	done 2>/dev/null

	if [ -f ${WF_PROMO_INTERFACE_MNT_OUT_DIR}/WMRECMNT.DAT ]
	then
		echo "cp ${WF_PROMO_INTERFACE_MNT_OUT_DIR}/WMRECMNT.DAT ${WF_MNTAPPLY_MNTSRCDIR}" >> $MNTLOG
		cp ${WF_PROMO_INTERFACE_MNT_OUT_DIR}/WMRECMNT.DAT ${WF_MNTAPPLY_MNTSRCDIR}
		echo "rm ${WF_PROMO_INTERFACE_MNT_OUT_DIR}/WMRECMNT.DAT"  >> $MNTLOG 
		rm ${WF_PROMO_INTERFACE_MNT_OUT_DIR}/WMRECMNT.DAT
	fi

        # append the new file, if there's an ol' file not processed
        # ---------------------------------------------------------
	for FILENAME in `ls ${WF_MNTAPPLY_MNTSRCDIR}/${WF_MNTAPPLY_MNTFILES_MASK}`
	do
	    echo "cat $FILENAME >> ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE" >> $MNTLOG
	    cat $FILENAME >> ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE
	    if [ -d ${WF_MNTAPPLY_BACKDIR} ]
	    then
	    	echo "Esecuzione comando mv -f $FILENAME ${WF_MNTAPPLY_BACKDIR}/ ..." >> $MNTLOG
	    	mv $FILENAME ${WF_MNTAPPLY_BACKDIR}/
	    	echo "... completata esecuzione comando mv -f $FILENAME ${WF_MNTAPPLY_BACKDIR}/ " >> $MNTLOG
	    else
	    	echo "Esecuzione comando rm -f $FILENAME..." >> $MNTLOG
	    	rm $FILENAME
	    	echo "... completata esecuzione comando rm -f $FILENAME" >> $MNTLOG
	    fi
	done 2>/dev/null

	if [ -f ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE ] || [ -f $WRK_DIR/$OFFL_FILE ]
	then
		create_gmselect
		if [ ${DBG_MODE} = "1" ]
		then
			echo "File found." >> $MNTLOG
			echo "dir di lavoro...${WF_MNTAPPLY_WORKDIR}/$WORK_FILE" >> $MNTLOG
			echo "file di select..${WF_MNTAPPLY_WORKDIR}/$SELE_FILE" >> $MNTLOG
			echo "server..........${WF_ARSSERVER_HOMEDIR}" >> $MNTLOG
			echo " " >> $MNTLOG
		fi
		cd ${WF_MNTAPPLY_WORKDIR}

		if [ ${DBG_MODE} = "1" ]
		then
		   echo "Chiamo ${WF_ARSSERVER_PLUDRV_FULLPATH}....." >> $MNTLOG
		fi

	        echo BEGIN SELE_FILE: >> $MNTLOG
        	cat $SELE_FILE >> $MNTLOG
	        echo END   SELE_FILE: >> $MNTLOG
		echo "Esecuzione comando ${WF_ARSSERVER_PLUDRV_FULLPATH} $PARAM $SELE_FILE..." >> $MNTLOG
		${WF_ARSSERVER_PLUDRV_FULLPATH} $PARAM $SELE_FILE >> $MNTLOG
		echo "...completata esecuzione comando ${WF_ARSSERVER_PLUDRV_FULLPATH} $PARAM $SELE_FILE" >> $MNTLOG

		if [ ${DBG_MODE} = "1" ]
		then
		   echo "Eseguito ${WF_ARSSERVER_PLUDRV_FULLPATH}....." >> $MNTLOG
		fi

                if [ $? != 0 ]
                then
		    if [ ${DBG_MODE} = "1" ]
		    then
                        echo "${WF_ARSSERVER_PLUDRV_FULLPATH} error" >> $MNTLOG
                        echo " " >> $MNTLOG
                    fi
                else
		    rm ${WF_MNTAPPLY_WORKDIR}/$WORK_FILE
                fi

		rm ${WF_MNTAPPLY_WORKDIR}/$SELE_FILE
		cd -
	else
		if [ ${DBG_MODE} = "1" ]
		then
                    echo "...nothing to do." >> $MNTLOG
                    echo " " >> $MNTLOG
                fi
	fi
done


