Attenzione, le modifiche ai file (png, css) non richiedono il restart dei servizi del server, devi però assicurarti che la pagina venga ricaricata dal browser.

X l’immagine con lo sfondo nella schermata di login:
   /home/NCR/webfront/resources/webfront/core/login/login_background.png

X l’immagine cone le version di component (pagina principale e pulsante “about):
    /home/NCR/webfront/resources/webfront/core/home/welcome-logo-info.png   

Per le barre (superiore e inferiore) per i titoli delle finester e per i menu, non ci sono immagini ma i colori nel file seguente:
   /home/NCR/webfront/resources/css/webfrontcore.css
Devi sostituire nel file tutte le occorrenze di #6FB644 (colore verde) con il colore desiderato (per esempio 6F0000)


P.S. ho visto che le seguenti immagini non sono utilizzate e possono quindi essere cancellate:
   /home/NCR/webfront/resources/webfront/core/login/background_.png
   /home/NCR/webfront/resources/webfront/core/login/login_background_.png
   /home/NCR/webfront/resources/webfront/core/login/webfront_splash.jpg
   /home/NCR/webfront/resources/webfront/core/home/webfront_splash.jpg
   /home/NCR/webfront/resources/webfront/core/home/welcome-logo.png
   /home/NCR/webfront/resources/webfront/core/home/downbar.png
   /home/NCR/webfront/resources/webfront/core/home/upbar.png
   /home/NCR/webfront/resources/webfront/common/downbar.png
   /home/NCR/webfront/resources/webfront/common/upbar.png

