package com.ncr.webfront.base.restservices.commandexecution;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.commandexecution.CommandServiceInterface;
import com.ncr.webfront.base.commandexecution.CommandServiceRestInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.commandexecution.Command;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/commandService")
public class CommandServiceRestServer implements CommandServiceRestInterface {


	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return Response.status(Response.Status.OK).entity(gson.toJson(commandService.checkService())).build();
	}
	
	
	@Override
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAll() {
		try {
			return RestRespond.with(commandService.getAll());
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getByName/{commandName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getByName(@PathParam("commandName") String commandName) {
		try {
			return RestRespond.with(commandService.getByName(commandName));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/execute/{commandName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object execute(@PathParam("commandName") String commandName) {
		try {
			commandService.execute(commandName);
			
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.OK_NO_ERROR));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.BUSINESS_LOGIC_ERROR, e.getMessage()));
		}
	}
	
	@Override
	@Path("/cancel/{commandName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object cancel(@PathParam("commandName") String commandName) {
		try {
			commandService.cancel(commandName);
			
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.OK_NO_ERROR));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.BUSINESS_LOGIC_ERROR, e.getMessage()));
		}
	}

	@Override
	@Path("/update/{commandName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object update(@PathParam("commandName") String commandName) {
		try {
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.OK_NO_ERROR, gson.toJson(commandService.update(commandName))));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.BUSINESS_LOGIC_ERROR, e.getMessage()));
		}
	}
	
	@Override
	@Path("/makeCommandAvailabileForExecution")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object makeCommandAvailabileForExecution(String commandJsonString) {
		try {
			commandService.makeCommandAvailabileForExecution(gson.fromJson(commandJsonString, Command.class));
			
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.OK_NO_ERROR));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(new ErrorObject(ErrorObject.ErrorCode.BUSINESS_LOGIC_ERROR, e.getMessage()));
		}
	}

	public void setCommandService(CommandServiceInterface commandService) {
		this.commandService = commandService;
	}

	private Gson gson = new Gson();
	private CommandServiceInterface commandService;
	private Logger logger = WebFrontLogger.getLogger(CommandServiceRestServer.class);
}
