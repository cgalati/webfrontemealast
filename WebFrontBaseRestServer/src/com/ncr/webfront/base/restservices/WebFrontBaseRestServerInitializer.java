package com.ncr.webfront.base.restservices;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;

public class WebFrontBaseRestServerInitializer extends ContextLoaderListener{

	@Override
   public void contextDestroyed(ServletContextEvent arg0) {
		super.contextDestroyed(arg0);
   }

	@Override
   public void contextInitialized(ServletContextEvent arg0) {
		WebFrontMappingProperties.changeWebFrontConfigByReadingItFromWar();
		super.contextInitialized(arg0);
   }
}
