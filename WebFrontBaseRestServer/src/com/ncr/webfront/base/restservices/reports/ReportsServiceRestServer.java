package com.ncr.webfront.base.restservices.reports;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posterminal.PosTerminal;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.reports.ReportsServiceInterface;
import com.ncr.webfront.base.reports.ReportsServiceRestInterface;
import com.ncr.webfront.base.restservices.VersionWebFrontBaseRestServer;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.common.RestUtils;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/reportsService")
public class ReportsServiceRestServer implements ReportsServiceRestInterface {


	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		VersionWebFrontBaseRestServer.logVersionOnce();
		return RestRespond.with(reportsService.checkService());
	}
	
	@Override
	@Path("/getFormats")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getFormats() {
		return RestRespond.with(reportsService.getFormats());
	}
	
	@Override
	@Path("/generatePosOperatorReport")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generatePosOperatorReport(String reportOptionsJsonString) {
		ReportOptions reportOptions = gson.fromJson(reportOptionsJsonString, ReportOptions.class);
		
		try {
			byte[] report = reportsService.generatePosOperatorReport(reportOptions);
			
			return RestRespond.with(report);
		} catch (Throwable t) {
			logger.error("", t);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Override
	@Path("/generateFiancialReport")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generateFiancialReport(String reportOptionsJsonString) {
		ReportOptions reportOptions = gson.fromJson(reportOptionsJsonString, ReportOptions.class);
		
		try {
			byte[] report = reportsService.generateFiancialReport(reportOptions);
			
			return RestRespond.with(report);
		} catch (Throwable t) {
			logger.error("", t);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Override
	@Path("/generateHourlyActivityReport")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generateHourlyActivityReport(String reportOptionsJsonString) {
		ReportOptions reportOptions = gson.fromJson(reportOptionsJsonString, ReportOptions.class);
		
		try {
			byte[] report = reportsService.generateHourlyActivityReport(reportOptions);
			
			return RestRespond.with(report);
		} catch (Throwable t) {
			logger.error("", t);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@Override
	@Path("/generateDepartmentReport")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generateDepartmentReport(String reportOptionsJsonString) {
		ReportOptions reportOptions = gson.fromJson(reportOptionsJsonString, ReportOptions.class);
		List<PosDepartment> posDepartments = new ArrayList<PosDepartment>();
		List<String> selectedDepartmentsCodes = new ArrayList<String>();
		for (PosDepartment selected : reportOptions.getPosDepartments()) {
			selectedDepartmentsCodes.add(selected.getCode());
		}
		for(PosDepartment department : posDepartmentService.getAll()) {
			if (selectedDepartmentsCodes.contains(department.getCode()) ||
					selectedDepartmentsCodes.contains(department.getTotal())) {
				posDepartments.add(department);
			}
		}
		reportOptions.setPosDepartments(posDepartments);
		try {
			byte[] report = reportsService.generateDepartmentReport(reportOptions);
			
			return RestRespond.with(report);
		} catch (Throwable t) {
			logger.error("", t);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Override
	@Path("/generatePosOperatorCashReport")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generatePosOperatorCashReport(String posOperatorCashReportJsonString) {
		String posOperatorsTerminalCodeMapJsonString = RestUtils.extractFromCustomJsonString(posOperatorCashReportJsonString, 0);
		Type posOperatorTerminalCodeMapType = new TypeToken<Map<PosOperator, String>>(){}.getType();
		Map<PosOperator, String> posOperatorsTerminalCodeMap = gson.fromJson(posOperatorsTerminalCodeMapJsonString, posOperatorTerminalCodeMapType);
				
		String reportFormatJsonString = RestUtils.extractFromCustomJsonString(posOperatorCashReportJsonString, 1);
		String reportFormat = gson.fromJson(reportFormatJsonString, String.class);

		try {
			byte[] report = reportsService.generatePosOperatorCashReport(posOperatorsTerminalCodeMap, reportFormat);
			
			return RestRespond.with(report);
		} catch (Throwable t) {
			logger.error("", t);
			
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Override
	@Path("/generateEodReports")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generateEodReports(String registerListJsonString) {
		Type registerListType = new TypeToken<List<String>>(){}.getType();
		List<String> registerList = gson.fromJson(registerListJsonString, registerListType);
		
		return RestRespond.with(reportsService.generateEodReports(registerList));
	}
	
	public void setReportsService(ReportsServiceInterface repoService) {
		this.reportsService = repoService;
	}
	
	public void setPosDepartmentService(PosDepartmentServiceInterface posDepartmentService) {
		this.posDepartmentService = posDepartmentService;
	}
	
	private Gson gson = new Gson();
	private ReportsServiceInterface reportsService;
	private PosDepartmentServiceInterface posDepartmentService;
	
	private static Logger logger = WebFrontLogger.getLogger(ReportsServiceRestServer.class);


}
