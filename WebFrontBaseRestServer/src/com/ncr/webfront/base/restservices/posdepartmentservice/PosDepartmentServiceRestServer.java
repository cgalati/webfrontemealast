package com.ncr.webfront.base.restservices.posdepartmentservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceRestInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

//TODO Error handling + logging !!

@Path("/posDepartmentService")
public class PosDepartmentServiceRestServer implements PosDepartmentServiceRestInterface {


	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		try {
			return Response.status(Response.Status.OK).entity(gson.toJson(departmentService.checkService())).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAll() {
		try {
			String jsonString = gson.toJson(departmentService.getAll());
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}

	}

	@Path("/getDepartmentsOfLevel/{level}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getDepartmentsOfLevel(@PathParam("level") int level) {
		try {
			String jsonString = gson.toJson(departmentService.getDepartmentsOfLevel(level));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/getByCode/{dptCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getByCode(@PathParam("dptCode") String dptCode) {
		try {
			String jsonString = gson.toJson(departmentService.getByCode(dptCode));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	@Path("/addDepartment")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDepartment(String departmentJsonString) {
		try {
			PosDepartment parsedDepartment = gson.fromJson(departmentJsonString, PosDepartment.class);
			return Response.status(Response.Status.OK).entity(gson.toJson(departmentService.addDepartment(parsedDepartment))).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

	@Path("/deleteDepartment/{departmentCodeToDelete}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteDepartment(@PathParam("departmentCodeToDelete") String departmentCodeToDelete) {
		try {
			String jsonString = gson.toJson(departmentService.deleteDepartment(departmentCodeToDelete));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/updateDepartment")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateDepartment(String departmentJsonString) {
		try {
			PosDepartment parsedDepartment = gson.fromJson(departmentJsonString, PosDepartment.class);
			return Response.status(Response.Status.OK).entity(gson.toJson(departmentService.updateDepartment(parsedDepartment))).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}
	
	@Path("/createDepartment")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createDepartment() {
		try {
			return Response.status(Response.Status.OK).entity(gson.toJson(departmentService.createDepartment())).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}


	public PosDepartmentServiceInterface getDepartmentService() {
		return departmentService;
	}

	public void setDepartmentService(PosDepartmentServiceInterface departmentService) {
		this.departmentService = departmentService;
	}

	private Logger logger = WebFrontLogger.getLogger(PosDepartmentServiceRestServer.class);
	private Gson gson = new Gson();
	private PosDepartmentServiceInterface departmentService = null;

}
