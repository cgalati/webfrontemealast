package com.ncr.webfront.base.restservices.invoicecreditnoteservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceRestInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSearchType;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/invoiceCreditNoteService")
public class InvoiceCreditNoteServiceRestServer implements InvoiceCreditNoteServiceRestInterface {
	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return RestRespond.with(invoiceCreditNoteService.checkService());
	}
	
	@Override
	@Path("/createDefaultCustomer")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createDefaultCustomer() {
		try {
			return RestRespond.with(invoiceCreditNoteService.createDefaultCustomer());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/addCustomer")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCustomer(String customerToAdd) {
		try {
			return RestRespond.with(invoiceCreditNoteService.addCustomer(gson.fromJson(customerToAdd, Customer.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getAllCustomers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCustomers() {
		try {
			return RestRespond.with(invoiceCreditNoteService.getAllCustomers());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getCustomersByName/{companyName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomersByName(@PathParam("companyName") String companyName) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getCustomersByName(companyName));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getCustomersByFiscalOrVatCode/{code}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomersByFiscalOrVatCode(@PathParam("code") String code) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getCustomersByFiscalOrVatCode(code));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getCustomerByFidelityCode/{fidelityCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerByFidelityCode(@PathParam("fidelityCode") String fidelityCode) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getCustomerByFidelityCode(fidelityCode));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/updateCustomer")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomer(String customerToUpdate) {
		try {
			invoiceCreditNoteService.updateCustomer(gson.fromJson(customerToUpdate, Customer.class));
			
			return RestRespond.with();
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/deleteCustomer")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCustomer(String customerToDelete) {
		try {
			invoiceCreditNoteService.deleteCustomer(gson.fromJson(customerToDelete, Customer.class));
			
			return RestRespond.with();
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getAllCustomerTypes")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllCustomerTypes() {
		try {
			return RestRespond.with(invoiceCreditNoteService.getAllCustomerTypes());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getAllExemptions")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllExemptions() {
		try {
			return RestRespond.with(invoiceCreditNoteService.getAllExemptions());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getCustomerValidationData")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCustomerValidationData() {
		try {
			return RestRespond.with(invoiceCreditNoteService.getCustomerValidationData());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/searchReceipts")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response searchReceipts(String receiptSearchTypeJsonString) {
		ReceiptSearchType parsedReceiptSearchType = gson.fromJson(receiptSearchTypeJsonString, ReceiptSearchType.class);	
		try {
			return RestRespond.with(invoiceCreditNoteService.searchReceipts(parsedReceiptSearchType));
		} catch (Exception e) {
			log.error("", e);		
			return RestRespond.with(e);
		}	
	}
	
	@Override
	@Path("/fillReceipt")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object fillReceipt(String receiptSearchTypeJsonString) {
		try {
			return RestRespond.with(invoiceCreditNoteService.fillReceipt(gson.fromJson(receiptSearchTypeJsonString, ReceiptSearchType.class)));
		} catch (Exception e) {
			log.error("", e);		
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/createInvoice")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object createInvoice(String receiptJsonString) {
		try {
			return RestRespond.with(invoiceCreditNoteService.createInvoice(gson.fromJson(receiptJsonString, Receipt.class)));
		} catch (Exception e) {
			log.error("", e);		
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/generateInvoice")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object generateInvoice(String invoiceJsonString) {
		try {
			return RestRespond.with(invoiceCreditNoteService.generateInvoice(gson.fromJson(invoiceJsonString, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/renderHtmlInvoice")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object renderHtmlInvoice(String invoiceJsonString) {
		try {
			return RestRespond.with(invoiceCreditNoteService.renderHtmlInvoice(gson.fromJson(invoiceJsonString, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/renderPdfInvoice")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object renderPdfInvoice(String invoiceJsonString) {
		try {
			return RestRespond.with(invoiceCreditNoteService.renderPdfInvoice(gson.fromJson(invoiceJsonString, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/saveInvoiceAndRenderPdf")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object saveInvoiceAndRenderPdf(String invoiceJsonString) {
		try {
			return RestRespond.with(invoiceCreditNoteService.saveInvoiceAndRenderPdf(gson.fromJson(invoiceJsonString, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/cancelInvoice")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object cancelInvoice(String invoiceJsonString) {
		try {
			invoiceCreditNoteService.cancelInvoice(gson.fromJson(invoiceJsonString, Document.class));
			
			return RestRespond.with();
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getInvoiceByNumber/{number}/{mayHaveCreditNote}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInvoiceByNumber(@PathParam("number") int number, @PathParam("mayHaveCreditNote") boolean mayHaveCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getInvoiceByNumber(number, mayHaveCreditNote));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getInvoicesByFiscalCode/{fiscalCode}/{mayHaveCreditNote}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInvoicesByFiscalCode(@PathParam("fiscalCode") String fiscalCode, @PathParam("mayHaveCreditNote") boolean mayHaveCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getInvoicesByFiscalCode(fiscalCode, mayHaveCreditNote));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getInvoicesByVatCode/{vatCode}/{mayHaveCreditNote}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getInvoicesByVatCode(@PathParam("vatCode") String vatCode, @PathParam("mayHaveCreditNote") boolean mayHaveCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getInvoicesByVatCode(vatCode, mayHaveCreditNote));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getLastPrintedInvoices/{number}/{mayHaveCreditNote}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getLastPrintedInvoices(@PathParam("number") int number, @PathParam("mayHaveCreditNote") boolean mayHaveCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getLastPrintedInvoices(number, mayHaveCreditNote));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/generateCreditNote")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Object generateCreditNote(String invoice) {
		try {
			return RestRespond.with(invoiceCreditNoteService.generateCreditNote(gson.fromJson(invoice, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/renderHtmlCreditNote")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Object renderHtmlCreditNote(String generatedCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.renderHtmlCreditNote(gson.fromJson(generatedCreditNote, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/renderPdfCreditNote")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Object renderPdfCreditNote(String generatedCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.renderPdfCreditNote(gson.fromJson(generatedCreditNote, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/saveCreditNoteAndRenderPdf")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Object saveCreditNoteAndRenderPdf(String generatedCreditNote) {
		try {
			return RestRespond.with(invoiceCreditNoteService.saveCreditNoteAndRenderPdf(gson.fromJson(generatedCreditNote, Document.class)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/cancelCreditNote")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Object cancelCreditNote(String creditNote) {
		try {
			invoiceCreditNoteService.cancelCreditNote(gson.fromJson(creditNote, Document.class));
			
			return RestRespond.with();
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/peekSequences")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object peekSequences() {
		try {
			return RestRespond.with(invoiceCreditNoteService.peekSequences());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/setSequence/{key}/{value}")
	@GET
	public Object setSequence(@PathParam("key") String key, @PathParam("value") int value) {
		try {
			invoiceCreditNoteService.setSequence(key, value);
			
			return RestRespond.with();
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getPrintedDocumentByNumber/{number}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPrintedDocumentByNumber(@PathParam("number") String number) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getPrintedDocumentByNumber(Integer.valueOf(number)));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getPrintedDocumentsByFiscalCode/{fiscalCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPrintedDocumentsByFiscalCode(@PathParam("fiscalCode") String fiscalCode) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getPrintedDocumentsByFiscalCode(fiscalCode));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getPrintedDocumentsByVatCode/{vatCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPrintedDocumentsByVatCode(@PathParam("vatCode") String vatCode) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getPrintedDocumentsByVatCode(vatCode));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getLastPrintedDocuments/{number}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getLastPrintedDocuments(@PathParam("number") int number) {
		try {
			return RestRespond.with(invoiceCreditNoteService.getLastPrintedDocuments(number));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	public void setInvoiceCreditNoteService(InvoiceCreditNoteServiceInterface invoiceCreditNoteService) {
		this.invoiceCreditNoteService = invoiceCreditNoteService;
	}
	
	private InvoiceCreditNoteServiceInterface invoiceCreditNoteService;
	private Gson gson = new Gson();
	
	private static final Logger log = WebFrontLogger.getLogger(InvoiceCreditNoteServiceRestServer.class);
}
