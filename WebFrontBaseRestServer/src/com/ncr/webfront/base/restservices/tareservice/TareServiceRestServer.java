package com.ncr.webfront.base.restservices.tareservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.base.tare.TareServiceInterface;
import com.ncr.webfront.base.tare.TareServiceRestInterface;

//TODO Error handling + logging !!
@Path("/tareService")
public class TareServiceRestServer implements TareServiceRestInterface {

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return RestRespond.with(tareService.checkService());
	}


	@Override
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAll() {
		return RestRespond.with(tareService.getAll());
	}

	@Override
	@Path("/getTareById/{tareId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getTareById(int tareId) {
		return RestRespond.with(tareService.getTareById(tareId));
	}
	
	public void setTareService(TareServiceInterface tareService) {
		this.tareService = tareService;
	}
	
	private TareServiceInterface tareService;

}
