package com.ncr.webfront.base.restservices.posstatusservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.posstatus.PosStatusServiceInterface;
import com.ncr.webfront.base.posstatus.PosStatusServiceRestInterface;
import com.ncr.webfront.base.restservices.VersionWebFrontBaseRestServer;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/posStatusService")
public class PosStatusServiceRestServer implements PosStatusServiceRestInterface {

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		VersionWebFrontBaseRestServer.logVersionOnce();
		return RestRespond.with(posStatusService.checkService());
	}

	
	@Path("/getAllPosStatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllPosStatus() {
		try {
			return RestRespond.with(posStatusService.getAllPosStatus());
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Path("/getPosStatus/{posCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPosStatus(@PathParam("posCode") String posCode) {
		try {
			return RestRespond.with(posStatusService.getPosStatus(posCode));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}

	public void setPosStatusService(PosStatusServiceInterface posStatusService) {
		this.posStatusService = posStatusService;
	}

	private PosStatusServiceInterface posStatusService = null;
	
	private static Logger logger = WebFrontLogger.getLogger(PosStatusServiceRestServer.class);
	
}
