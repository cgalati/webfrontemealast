package com.ncr.webfront.base.restservices.eodservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.eod.EodServiceInterface;
import com.ncr.webfront.base.eod.EodServiceRestInterface;
import com.ncr.webfront.base.restservices.VersionWebFrontBaseRestServer;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/eodService")
public class EodServiceRestServer implements EodServiceRestInterface {

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		VersionWebFrontBaseRestServer.logVersionOnce();
		return RestRespond.with(eodService.checkService());
	}

	
	@Path("/getAllPosStatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAllPosStatus() {
		try {
			return RestRespond.with(eodService.getAllPosStatus());
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Path("/getPosStatus/{posCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPosStatus(@PathParam("posCode") String posCode) {
		try {
			return RestRespond.with(eodService.getPosStatus(posCode));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Path("/getEodStatus")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getEodStatus() {
		try {
			return RestRespond.with(eodService.getEodStatus());
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Path("/getEodStatusHistory")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getEodStatusHistory() {
		try {
			return RestRespond.with(eodService.getEodStatusHistory());
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Path("/launchEodAllPos")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object launchEodAllPos() {
		return RestRespond.with(eodService.launchEodAllPos());
	}

	@Path("/launchEodServer")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object launchEodServer() {
		return RestRespond.with(eodService.launchEodServer());
	}
	
	@Path("/cancel")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object cancel() {
		eodService.cancel();
		
		return RestRespond.with();
	}

	@Path("/declareDefective/{posCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object declareDefective(@PathParam("posCode") String posCode) {
		return RestRespond.with(eodService.declareDefective(posCode));
	}
	
	public void setEodService(EodServiceInterface eodService) {
		this.eodService = eodService;
	}

	private EodServiceInterface eodService = null;
	
	private static Logger logger = WebFrontLogger.getLogger(EodServiceRestServer.class);

}
