package com.ncr.webfront.base.restservices.dbtestservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.dbtest.DbTestServiceInterface;
import com.ncr.webfront.base.plugins.dbtest.DbTestServiceImplementation;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/dbTestService")
public class DbTestServiceRestServer {
	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		logger.debug("Begin - End");
		return Response.status(Response.Status.OK).entity(gson.toJson(dbTestService.checkService())).build();
	}

	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAll() {
		logger.debug("Begin - End");
		return Response.status(Response.Status.OK).entity(gson.toJson(dbTestService.getAll())).build();
	}

	
	public DbTestServiceInterface getDbTestService() {
		return dbTestService;
	}

	public void setDbTestService(DbTestServiceInterface dbTestService) {
		this.dbTestService = dbTestService;
	}


	private Gson gson = new Gson();
	private DbTestServiceInterface dbTestService;
	private Logger logger = WebFrontLogger.getLogger(DbTestServiceRestServer.class);
}
