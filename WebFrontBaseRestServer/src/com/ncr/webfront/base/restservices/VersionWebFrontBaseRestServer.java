package com.ncr.webfront.base.restservices;

import com.ncr.webfront.core.utils.versioning.ComponentVersionInterface;
import com.ncr.webfront.core.utils.versioning.VersionManager;

public class VersionWebFrontBaseRestServer implements ComponentVersionInterface {

	private final static String VERSION_NUMBER = "0.18.0819";
	private final static String VERSION_DATE = "18/08/2018";
	private final static String COMPONENT_NAME = "WebFront EMEA RestServer";

	private static boolean versionLogged = false;

	@Override
	public String getVersionNumber() {
		return VERSION_NUMBER;
	}

	@Override
	public String getVersionDate() {
		return VERSION_DATE;
	}

	@Override
	public String getComponentName() {
		return COMPONENT_NAME;
	}

	@Override
	public String getComponentNameAndVersion() {
		return COMPONENT_NAME + " v. " + VERSION_NUMBER + " (" + VERSION_DATE + ")";
	}

	public static void logVersionOnce(){
		try{
			
		if (versionLogged){
			return;
		}
		versionLogged = true;
		VersionManager.addVersionOnlyForLogVersion(new VersionWebFrontBaseRestServer());
		VersionManager.logVersions();
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
}
