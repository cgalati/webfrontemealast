package com.ncr.webfront.base.restservices.posidcjrnservice;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.posidcjrn.PosIdcJrnServiceInterface;
import com.ncr.webfront.base.posidcjrn.entities.DataCollect;
import com.ncr.webfront.base.posidcjrn.entities.DataHeader;
import com.ncr.webfront.base.posidcjrn.entities.Journal;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;


//TODO Error handling + logging !!
@Path("/posIdcJrnService")
public class PosIdcJrnServiceRestServer {
	private Logger logger = WebFrontLogger.getLogger(PosIdcJrnServiceRestServer.class);
	private Gson gson = new Gson();
	private PosIdcJrnServiceInterface posIdcJrnService;
	
	
	public PosIdcJrnServiceInterface getPosIdcJrnService() {
		return posIdcJrnService;
	}

	public void setPosIdcJrnService(PosIdcJrnServiceInterface posIdcJrnService) {
		this.posIdcJrnService = posIdcJrnService;
	}

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return RestRespond.with(posIdcJrnService.checkService());
	}
	
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAll() {
		return RestRespond.with(posIdcJrnService.getAll());
	}
	
	@Path("/addIdcJrn")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Object addIdcJrn(String jsonRequest) {
		try {
			jsonRequest = splitJson(jsonRequest);
			
			//DataHeader data = gson.fromJson(jsonRequest, DataHeader.class);
			
			return RestRespond.with(posIdcJrnService.addIdcJrn(jsonRequest));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	private String splitJson(String json) {
		boolean find = true;
		logger.debug("json param: " + json);
		
		while (find) {
			int posBeg = json.indexOf("\"id\""); 
			
			if (posBeg >= 0) {							
				int posEnd = json.substring(posBeg).indexOf(",");
				
				json = json.substring(0, posBeg) + json.substring(posBeg + posEnd + 1);
			} else {
				posBeg = json.indexOf("\"transactionHeader\"");
				if (posBeg >= 0) {				
					int posEnd = json.substring(posBeg).indexOf("}");
					
					json = json.substring(0, posBeg) + json.substring(posBeg + posEnd);
				} else {
					posBeg = json.indexOf("\"line\"");
					if (posBeg >= 0) {				
						int posEnd = json.substring(posBeg).indexOf("\",");
						if (posEnd >= 0) {
							json = json.substring(0, posBeg + posEnd+1) + json.substring(posBeg + posEnd + 2);	
						} else {
							find = false;
						}						
					} else {
						find = false;
					}
				}
			}
		}
				
		logger.debug("json return: " + json);
		return json;
	}
	
	
	@Path("/getTransaction/{terminal}/{dateFrom}/{dateTo}/{store}/{transaction}/{customer}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getTransaction(@PathParam("terminal") String terminal, @PathParam("dateFrom") String dateFrom, 
			@PathParam("dateTo") String dateTo, @PathParam("store") String store, 
			@PathParam("transaction") String transaction, @PathParam("customer") String customer) {
		try {			
			return RestRespond.with(posIdcJrnService.getTransactionsByParams(terminal, dateFrom, dateTo, store, transaction, customer));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(e);
		}
	}

	@Path("/deleteIdcJrn/{id}/{table}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Object deleteIdcJrn(@PathParam("id") String id, @PathParam("table") String table) {
		try {
			return RestRespond.with(posIdcJrnService.deleteIdcJrn(Integer.parseInt(id), table));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
		
	@Path("/deleteAll/{id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Object deleteIdcJrn(@PathParam("id") String id) {
		try {
			return RestRespond.with(posIdcJrnService.deleteAll(Integer.parseInt(id)));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Path("/getLstIdc/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getLstIdc(@PathParam("id") String id) {
		try {			
			return RestRespond.with(posIdcJrnService.getLstIdc(Integer.parseInt(id)));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(e);
		}
	}

	@Path("/getLstJrn/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getLstJrn(@PathParam("id") String id) {
		try {			
			return RestRespond.with(posIdcJrnService.getLstJrn(Integer.parseInt(id)));
		} catch (Exception e) {
			logger.error("", e);
			return RestRespond.with(e);
		}
	}
	
	@Path("/addJrn")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object addJrn(String jsonJournal) {
		try {
			//Journal jrn = gson.fromJson(jsonJournal, Journal.class);
			
			return RestRespond.with(posIdcJrnService.addJrn(jsonJournal));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Path("/addIdc")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object addIdc(String jsonDataCollect) {
		try {
			//DataCollect idc = gson.fromJson(jsonDataCollect, DataCollect.class);
			
			return RestRespond.with(posIdcJrnService.addIdc(jsonDataCollect));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
		
	@Path("/updateFileName/{updateFileName}/{idHeader}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Object updateFileName(@PathParam("updateFileName") String updateFileName, @PathParam("idHeader") String idHeader) {
		try {
			return RestRespond.with(posIdcJrnService.updateFileName(updateFileName, Integer.parseInt(idHeader)));
		} catch (Exception e) {
			logger.error("", e);
			
			return RestRespond.with(e);
		}
	}
}
