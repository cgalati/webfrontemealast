package com.ncr.webfront.base.restservices.posidcjrnservice;

import org.apache.log4j.Logger;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.settings.WebFrontCoreAbstractSettings;

public class WebFrontEmeaSettings extends WebFrontCoreAbstractSettings {

	private final Logger logger = WebFrontLogger.getLogger(this.getClass().getName());
	private static WebFrontEmeaSettings instance;

	public static final int MAX_REGEOD_INDEXES = 10;

	public static synchronized WebFrontEmeaSettings getInstance() {
		if (instance == null) {
			instance = new WebFrontEmeaSettings("webfront.properties");
		}
		return instance;
	}

	protected WebFrontEmeaSettings(String propertyFileName) {
		super(propertyFileName);
	}

	// TODO test settings

	public String getTestString() {
		logger.debug("BEGIN");
		String value = getStringValue("test.string", "test");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public int getTestInt() {
		logger.debug("BEGIN");
		int value = getIntValue("test.int", 180);
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPathIdcDirectory() {
		logger.debug("BEGIN");
		String value = getStringValue("idc.files.directory", "C:\\HOCIDC");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getPathJrnDirectory() {
		logger.debug("BEGIN");
		String value = getStringValue("jrn.files.directory", "C:\\HOCJRN");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPathZipDirectory() {
		logger.debug("BEGIN");
		String value = getStringValue("zip.files.directory", "C:\\HOCJRNIDC");
		logger.debug("END (" + value + ")");
		return value;
	}


	public String getDeleteIdcJrnDays() {
		logger.debug("BEGIN");
		String value = getStringValue("delete.idcjrn.db.days", "90");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getDeleteHeadersDays() {
		logger.debug("BEGIN");
		String value = getStringValue("delete.headers.db.days", "360");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public static void dump() {
		Logger logger = WebFrontLogger.getLogger(WebFrontEmeaSettings.class);
		logger.info("BEGIN");

		WebFrontEmeaSettings instance = WebFrontEmeaSettings.getInstance();
		logger.info(" getTestString()                                  = \"" + instance.getTestString() + "\"");
		logger.info(" getTestInt()                                     = \"" + instance.getTestInt() + "\"");

		logger.info("END");
	}
	
	public String getHeadersFilterTitle() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.title.filters", "Search by filters");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getHeadersFilterStore() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.filter.store", "Store");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getHeadersFilterTerminal() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.filter.terminal", "Terminal");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersFilterReceipt() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.filter.receipt", "Receipt");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersFilterCustomer() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.filter.customer", "Customer");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersFilterDateFrom() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.filter.datefrom", "Date From");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersFilterDateTo() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.filter.dateto", "Date To");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getHeadersTableStore() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.store", "Store");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableTerminal() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.terminal", "Terminal");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableReceipt() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.receipt", "Receipt");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableDate() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.date", "Date");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableTime() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.time", "Time");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableTotal() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.total", "Total");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableCustomer() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.customer", "Customer");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersTableEod() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.eod", "Eod");
		logger.debug("END (" + value + ")");
		return value;
	}
	public String getHeadersTableFileName() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.filename", "File Name");
		logger.debug("END (" + value + ")");
		return value;
	}

	
	public String getHeadersButtonSearch() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.button.search", "Search");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getHeadersButtonExport() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.button.export", "Export");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getHeadersButtonPrint() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.print.jrn", "Print Journal");
		logger.debug("END (" + value + ")");
		return value;
	}	
	
	
	public String getMsgNoResultHeaders() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.noresult", "No result in the header list");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getMsgNoResultIdc() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.idc.noresult", "No result in the data collect list");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getMsgNoResultJrn() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.jrn.noresult", "No result in the journal list");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getTitleTableJrn() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.jrn", "Journal");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getTitleTableIdc() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.table.idc", "Data Collect");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getSuccessExportJrn() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.export.success.jrn", "the export of the journal was successful");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getNoSuccessExportJrn() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.export.nosuccess.jrn", "the export of the journal wasn't successful");
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getSuccessExportIdc() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.export.success.idc", "the export of the data collect was successful");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getNoSuccessExportIdc() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.export.nosuccess.idc", "the export of the data collect wasn't successful");
		logger.debug("END (" + value + ")");
		return value;
	}

	
	public String getPrinterName() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.printer.name", "");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPrinterSuccess() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.printer.success", "Print of Journal was successful");
		logger.debug("END (" + value + ")");
		return value;
	}

	public String getPrinterNoSuccess() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.printer.nosuccess", "Print of Journal wasn't successful");
		logger.debug("END (" + value + ")");
		return value;
	}
		
	public int getMaxTransaction() {
		logger.debug("BEGIN");
		int value = getIntValue("search.headers.max.rows", 100);
		logger.debug("END (" + value + ")");
		return value;
	}
	
	public String getMaxTransactionMessage() {
		logger.debug("BEGIN");
		String value = getStringValue("search.headers.max.rows.message", "Too many transaction");
		logger.debug("END (" + value + ")");
		return value;
	}
}
