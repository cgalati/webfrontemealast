package com.ncr.webfront.base.restservices.commonlistsservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.ncr.webfront.core.utils.commonlists.CommonListsRestInterface;
import com.ncr.webfront.core.utils.commonlists.CommonListsServiceInterface;
import com.ncr.webfront.core.utils.common.RestRespond;

@Path("/commonListsService")
public class CommonListsServiceRestServer implements CommonListsRestInterface {

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return Response.status(Response.Status.OK).entity(gson.toJson(commonListsService.checkService())).build();
	}

	
	@Override
	@Path("/getManualDiscountCodeList")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getManualDiscountCodeList() {
		return RestRespond.with(commonListsService.getManualDiscountCodeList());
	}

	@Override
	@Path("/getManualDiscountCodeById/{idManualDiscountCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getManualDiscountCodeById(@PathParam("idManualDiscountCode") int idManualDiscountCode) {
		return RestRespond.with(commonListsService.getManualDiscountCodeById(idManualDiscountCode));
	}

	@Override
	@Path("/getPackagingTypeList")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPackagingTypeList() {
		return RestRespond.with(commonListsService.getPackagingTypeList());
	}

	@Override
	@Path("/getPackagingTypeById/{idPackagingType}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPackagingTypeById(@PathParam("idPackagingType") String idPackagingType) {
		return RestRespond.with(commonListsService.getPackagingTypeById(idPackagingType));
	}

	@Override
	@Path("/getPrizeCodeList")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPrizeCodeList() {
		return RestRespond.with(commonListsService.getPrizeCodeList());
	}

	@Override
	@Path("/getPrizeCodeById/{idPrizeCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPrizeCodeById(@PathParam("idPrizeCode") int idPrizeCode) {
		return RestRespond.with(commonListsService.getPrizeCodeById(idPrizeCode));
	}

	@Override
	@Path("/getZeroPriceBehaviourList")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getZeroPriceBehaviourList() {
		return RestRespond.with(commonListsService.getZeroPriceBehaviourList());
	}

	@Override
	@Path("/getZeroPriceBehaviourById/{idZeroPriceBehaviour}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getZeroPriceBehaviourById(@PathParam("idZeroPriceBehaviour") int idZeroPriceBehaviour) {
		return RestRespond.with(commonListsService.getZeroPriceBehaviourById(idZeroPriceBehaviour));
	}

	@Override
	@Path("/getAutoDiscountTypeList")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAutoDiscountTypeList() {
		return RestRespond.with(commonListsService.getAutoDiscountTypeList());
	}

	@Override
	@Path("/getAutoDiscountTypeById/{idAutoDiscountType}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAutoDiscountTypeById(@PathParam("idAutoDiscountType") int idAutoDiscountType) {
		return RestRespond.with(commonListsService.getAutoDiscountTypeById(idAutoDiscountType));
	}

	@Override
	@Path("/getUpbOperationTypeList")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getUpbOperationTypeList() {
		return RestRespond.with(commonListsService.getUpbOperationTypeList());
	}

	@Override
	@Path("/getUpbOperationTypeById/{idUpbOperationType}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getUpbOperationTypeById(@PathParam("idUpbOperationType") int idUpbOperationType) {
		return RestRespond.with(commonListsService.getUpbOperationTypeById(idUpbOperationType));
	}
	
	public void setCommonListsService(CommonListsServiceInterface commonListsService) {
		this.commonListsService = commonListsService;
	}
	
	private Gson gson = new Gson();
	private CommonListsServiceInterface commonListsService;
}
