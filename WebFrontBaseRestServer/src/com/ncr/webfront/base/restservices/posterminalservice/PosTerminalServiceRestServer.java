package com.ncr.webfront.base.restservices.posterminalservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.posterminal.PosTerminalServiceInterface;
import com.ncr.webfront.base.posterminal.PosTerminalServiceRestInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

//TODO Error handling + logging !!
@Path("/posTerminalService")
public class PosTerminalServiceRestServer implements PosTerminalServiceRestInterface {


	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return Response.status(Response.Status.OK).entity(gson.toJson(terminalService.checkService())).build();
	}

	
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		try {
			String jsonString = gson.toJson(terminalService.getAll());
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/getByCode/{terminalCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByCode(@PathParam("terminalCode") String terminalCode) {
		try {
			String jsonString = gson.toJson(terminalService.getByCode(terminalCode));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	public PosTerminalServiceInterface getTerminalService() {
		return terminalService;
	}

	public void setTerminalService(PosTerminalServiceInterface terminalService) {
		this.terminalService = terminalService;
	}
	
	private Logger logger = WebFrontLogger.getLogger(PosTerminalServiceRestServer.class);
	private Gson gson = new Gson();
	private PosTerminalServiceInterface terminalService = null;
}
