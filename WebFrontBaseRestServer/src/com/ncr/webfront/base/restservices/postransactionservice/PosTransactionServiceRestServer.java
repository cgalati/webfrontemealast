package com.ncr.webfront.base.restservices.postransactionservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.postransaction.PosTransactionSearchCriteria;
import com.ncr.webfront.base.postransaction.PosTransactionServiceInterface;
import com.ncr.webfront.base.postransaction.PosTransactionServiceRestInterface;
import com.ncr.webfront.base.postransaction.PosTransactionSummary;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/posTransactionService")
public class PosTransactionServiceRestServer implements PosTransactionServiceRestInterface {

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		logger.info("BEGIN");
		Object response = null;
		try {
			response = Response.status(Response.Status.OK).entity(gson.toJson(posTransactionService.checkService())).build();
		} catch (Exception e) {
			logger.error("", e);
			response = RestRespond.with(e);
		}
		logger.info("END");
		return response;
	}

	@Path("/getAvailablePosTransactionDates")
	@GET
	public Object getAvailablePosTransactionDates() {
		logger.info("BEGIN");
		Object response = null;
		try {
			String jsonString = gson.toJson(posTransactionService.getAvailablePosTransactionDates());
			response = Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			response = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
		logger.info("END");
		return response;
	}

	@Override
	@Path("/getPosTransactionJournal")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object getPosTransactionJournal(String posTransactionSummaryJsonString) {
		logger.info("BEGIN");
		Object response = null;
		PosTransactionSummary posTransactionSummary = gson.fromJson(posTransactionSummaryJsonString, PosTransactionSummary.class);
		try {
			response = RestRespond.with(posTransactionService.getPosTransactionJournal(posTransactionSummary));
		} catch (Exception e) {
			logger.error("", e);
			response = RestRespond.with(e);
		}
		logger.info("END");
		return response;
	}

	@Override
	@Path("/searchTransactions")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Object searchTransactions(String posTransactionSearchCriteriaJsonString) {
		logger.info("BEGIN");
		Object response = null;
		PosTransactionSearchCriteria posTransactionSearchCriteria = gson.fromJson(posTransactionSearchCriteriaJsonString, PosTransactionSearchCriteria.class);
		try {
			response = RestRespond.with(posTransactionService.searchTransactions(posTransactionSearchCriteria));
		} catch (Exception e) {
			logger.error("", e);
			response = RestRespond.with(e);
		}
		logger.info("END");
		return response;
	}
	public void setPosTransactionService(PosTransactionServiceInterface posTransactionService) {
		this.posTransactionService = posTransactionService;
	}
	

	private Logger logger = WebFrontLogger.getLogger(PosTransactionServiceRestServer.class);
	private Gson gson = new Gson();
	private PosTransactionServiceInterface posTransactionService = null;
}
