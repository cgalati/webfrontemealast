package com.ncr.webfront.base.restservices.posoperatorservice;

import javax.ws.rs.GET;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.posoperator.PosOperatorServiceInterface;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.posoperator.PosOperatorServiceRestInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

//TODO Error handling + logging !!
@Path("/posOperatorMaintenance")
public class PosOperatorServiceRestServer implements
		PosOperatorServiceRestInterface {


	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		return Response.status(Response.Status.OK).entity(gson.toJson(operatorService.checkService())).build();
	}
	
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		try {
			String jsonString = gson.toJson(operatorService.getAll());
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/getByCode/{operatorCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByCode(@PathParam("operatorCode") String operatorCode) {
		try {
			String jsonString = gson.toJson(operatorService.getByCode(operatorCode));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	@Path("/searchByCode/{operatorCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByCode(@PathParam("operatorCode") String operatorCode) {
		String jsonString = gson.toJson(operatorService.searchByCode(operatorCode));
		return Response.status(Response.Status.OK).entity(jsonString).build();
	}

	@Path("/searchByName/{name}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByName(@PathParam("name") String name) {
		String jsonString = gson.toJson(operatorService.searchByName(name));
		return Response.status(Response.Status.OK).entity(jsonString).build();

	}

	@Path("/searchByName")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByName() {
		String jsonString = gson.toJson(operatorService.searchByName(""));
		return Response.status(Response.Status.OK).entity(jsonString).build();

	}
	
	@Path("/createDefaultCashier")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createDefaultCashier() {
		try {
			String jsonString = gson.toJson(operatorService.createCashierFromRole(""));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/createCashierFromRole/{role}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCashierFromRole(@PathParam("role") String role) {
		try {
			String jsonString = gson.toJson(operatorService.createCashierFromRole(role));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	@Path("/addPosOperator")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addPosOperator(String posOperatorJsonString) {
		try {
			PosOperator parsedPosOperator = gson.fromJson(posOperatorJsonString, PosOperator.class);
			return Response.status(Response.Status.OK).entity(gson.toJson(operatorService.addPosOperator(parsedPosOperator))).build();
		} catch (Exception j) {
		}
		return Response.status(Response.Status.BAD_REQUEST).build();

	}
	
	@Path("/deletePosOperator/{posOperatorCodeToDelete}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deletePosOperator(@PathParam("posOperatorCodeToDelete") String posOperatorCodeToDelete) {
		String jsonString = gson.toJson(operatorService.deletePosOperator(posOperatorCodeToDelete));
		return Response.status(Response.Status.OK).entity(jsonString).build();
	}

	@Path("/updatePosOperator")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updatePosOperator(String posOperatorJsonString) {
		try {
			PosOperator parsedPosOperator = gson.fromJson(posOperatorJsonString, PosOperator.class);
			return Response.status(Response.Status.OK).entity(gson.toJson(operatorService.updatePosOperator(parsedPosOperator))).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	@Path("/getPosOperatorValidationData")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
    public Object getPosOperatorValidationData() {
		return Response.status(Response.Status.OK).entity(gson.toJson(operatorService.getPosOperatorValidationData())).build();
    }
	
	@Path("/getPosOperatorRoles")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPosOperatorRoles() {
		try {
			String jsonString = gson.toJson(operatorService.getPosOperatorRoles());
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	@Path("/getPosOperatorTraineeRole")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getPosOperatorTraineeRole() {
		try {
			String jsonString = gson.toJson(operatorService.getPosOperatorTraineeRole());
			
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}
	
	public PosOperatorServiceInterface getOperatorService() {
		return operatorService;
	}

	public void setOperatorService(PosOperatorServiceInterface operatorService) {
		this.operatorService = operatorService;
	}
	
	private Logger logger = WebFrontLogger.getLogger(PosOperatorServiceRestServer.class);
	private Gson gson = new Gson();
	private PosOperatorServiceInterface operatorService = null;


	
}
