package com.ncr.webfront.base.restservices.articleservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.article.ArticleServiceRestInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

//TODO Error handling + logging !!
@Path("/articleMaintenance")
public class ArticleServiceRestServer implements ArticleServiceRestInterface {

	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		try {
			return Response.status(Response.Status.OK).entity(gson.toJson(articleService.checkService())).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/searchByPluCode/{pluCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByPluCode(@PathParam("pluCode") String pluCode) {
		try {
			String jsonString = gson.toJson(articleService.searchByPluCode(pluCode));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/searchByArticleCode/{articleCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByArticleCode(@PathParam("articleCode") String articleCode) {
		try {
			String jsonString = gson.toJson(articleService.searchByArticleCode(articleCode));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/searchByStartsWithArticleCode/{articleCode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByStartsWithArticleCode(@PathParam("articleCode") String articleCode) {
		try {
			String jsonString = gson.toJson(articleService.searchByStartsWithArticleCode(articleCode));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/searchByDescription/{description}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchByDescription(@PathParam("description") String description) {
		try {
			String jsonString = gson.toJson(articleService.searchByDescription(description));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/searchByDescription")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getArticleByDescription() {
		try {
			String jsonString = gson.toJson(articleService.searchByDescription(""));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/addArticle")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addArticle(String articleJsonString) {
		try {
			Article parsedArticle = gson.fromJson(articleJsonString, Article.class);
			return Response.status(Response.Status.OK).entity(gson.toJson(articleService.addArticle(parsedArticle))).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

	@Path("/deleteArticle/{articleCodeToDelete}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteArticle(@PathParam("articleCodeToDelete") String articleCodeToDelete) {
		try {
			String jsonString = gson.toJson(articleService.deleteArticle(articleCodeToDelete));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/updateArticle")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateArticle(String articleJsonString) {
		try {
			Article parsedArticle = gson.fromJson(articleJsonString, Article.class);
			return Response.status(Response.Status.OK).entity(gson.toJson(articleService.updateArticle(parsedArticle))).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

	@Path("/getCapabilities")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCapabilities() {
		try {
			String jsonString = gson.toJson(articleService.getCapabilities());
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/createArticle")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response createArticle() {
		try {
			return Response.status(Response.Status.OK).entity(gson.toJson(articleService.createArticle())).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	@Path("/getValidationData")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getValidationData() {
		try {
			return Response.status(Response.Status.OK).entity(gson.toJson(articleService.getValidationData())).build();
		} catch (Exception e) {
			logger.error("", e);

			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
	}

	public void setArticleService(ArticleServiceInterface articleService) {
		this.articleService = articleService;
	}

	public ArticleServiceInterface getArticleService() {
		return articleService;
	}

	private Logger logger = WebFrontLogger.getLogger(ArticleServiceRestServer.class);
	private ArticleServiceInterface articleService = null;
	private Gson gson = new Gson();

}
