package com.ncr.webfront.base.restservices.loginservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.login.LoginServiceInterface;
import com.ncr.webfront.base.login.LoginServiceRestInterface;
import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/loginService")
public class LoginServiceRestServer implements LoginServiceRestInterface {

	@Override
	@Path("/doLogin/{userId}/{password}")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Object doLogin(@PathParam("userId") String id, @PathParam("password") String password) {
		try {
			return RestRespond.with(loginService.doLogin(id, password));

		} catch (Exception e) {
			logger.error("", e);

			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/doLogout/{userToken}")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Object doLogout(@PathParam("userToken") long userToken) {
		try {
			return RestRespond.with(loginService.doLogout(userToken));
		} catch (Exception e) {
			logger.error("", e);

			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/isLoggedIn/{userToken}")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
   public Object isLoggedIn(@PathParam("userToken") long userToken) {
		try {
			return RestRespond.with(loginService.isLoggedIn(userToken));
		} catch (Exception e) {
			logger.error("", e);

			return RestRespond.with(e);
		}
   }


	
	@Override
	@Path("/getLoggedInUserData/{userToken}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
   public Object getLoggedInUserData(@PathParam("userToken") long userToken) {
		try {
			String jsonString = gson.toJson(loginService.getLoggedInUserData(userToken));
			return Response.status(Response.Status.OK).entity(jsonString).build();
		} catch (Exception e) {
			logger.error("", e);
			return Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(e.getMessage()).build();
		}
   }

	public void setLoginService(LoginServiceInterface loginService) {
		this.loginService = loginService;
	}

	private LoginServiceInterface loginService;
	private Gson gson = new Gson();
	private static final Logger logger = WebFrontLogger.getLogger(LoginServiceRestServer.class);

}
