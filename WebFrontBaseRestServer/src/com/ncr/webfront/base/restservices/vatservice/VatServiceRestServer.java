package com.ncr.webfront.base.restservices.vatservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.common.RestRespond;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.base.vat.VatServiceRestInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

@Path("/vatService")
public class VatServiceRestServer implements VatServiceRestInterface {
	
	@Path("/checkService")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object checkService() {
		try {
			return RestRespond.with(vatService.checkService());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	
	@Override
	@Path("/getAll")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getAll() {
		try {
			return RestRespond.with(vatService.getAll());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}

	@Override
	@Path("/getVatById/{vatId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getVatById(@PathParam("vatId") int vatId) {
		try {
			return RestRespond.with(vatService.getVatById(vatId));
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	@Override
	@Path("/getDefault")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object getDefault() {
		try {
			return RestRespond.with(vatService.getDefault());
		} catch (Exception e) {
			log.error("", e);
			
			return RestRespond.with(e);
		}
	}
	
	public void setVatService(VatServiceInterface vatService) {
		this.vatService = vatService;
	}
	
	private VatServiceInterface vatService;
	
	private static final Logger log = WebFrontLogger.getLogger(VatServiceRestServer.class);
}
