package com.ncr.webfront.base.plugins.reports.dataobject;

public class PosOperatorCashReportRow {

	private String posOperatorCode;
	private String posOperatorName;
	private String terminal;
	private int cash;
	
	public PosOperatorCashReportRow(String posOperatorCode, String posOperatorName, String terminal, int cash) {
		this.posOperatorCode = posOperatorCode;
		this.posOperatorName = posOperatorName;
		this.terminal = terminal;
		this.cash = cash;
	}
	
	public String getPosOperatorCode() {
		return posOperatorCode;
	}
	public void setPosOperatorCode(String posOperatorCode) {
		this.posOperatorCode = posOperatorCode;
	}
	public String getPosOperatorName() {
		return posOperatorName;
	}
	public void setPosOperatorName(String posOperatorName) {
		this.posOperatorName = posOperatorName;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public int getCash() {
		return cash;
	}
	public void setCash(int cash) {
		this.cash = cash;
	}	
}
