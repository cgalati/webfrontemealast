package com.ncr.webfront.base.plugins.reports.dataobject;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ReportHeader extends DataObject {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<header>\n");
		sb.append("<negozio>").append(storeCode).append("</negozio>\n");
		sb.append("<data>").append(date).append("</data>\n");
		sb.append("<formatted_date>").append(formattedDate).append("</formatted_date>\n");
		sb.append("<ora>").append(hour).append("</ora>\n");
		sb.append("<elementoreport>").append(type).append("</elementoreport>\n");
		sb.append("<filtro>").append(filter).append("</filtro>\n");
		sb.append("<titolo>").append(title).append("</titolo>\n");
		sb.append("<stream>").append(stream).append("</stream>\n");
		sb.append("<cod_cassiere>").append(posOperatorCode).append("</cod_cassiere>\n");
		sb.append("<cassiere>").append(posOperatorName).append("</cassiere>\n");
		sb.append("<cod_terminale>").append(posTerminalCode).append("</cod_terminale>\n");
		sb.append("<stato_terminale>").append(posTerminalStatus).append("</stato_terminale>\n");
		sb.append("<cod_ente>").append(entityCode).append("</cod_ente>\n");
		sb.append("<desc_ente>").append(entityName).append("</desc_ente>\n");
		sb.append("</header>\n");
		logger.info(String.format("[getXml] xml = [%s]", sb));

		return sb.toString();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFormattedDate() {
		return formattedDate;
	}

	public void setFormattedDate(String formattedDate) {
		this.formattedDate = formattedDate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getPosOperatorCode() {
		return posOperatorCode;
	}

	public void setPosOperatorCode(String posOperatorCode) {
		this.posOperatorCode = posOperatorCode;
	}

	public String getPosOperatorName() {
		return posOperatorName;
	}

	public void setPosOperatorName(String posOperatorName) {
		this.posOperatorName = posOperatorName;
	}

	public String getPosTerminalCode() {
		return posTerminalCode;
	}

	public void setPosTerminalCode(String posTerminalCode) {
		this.posTerminalCode = posTerminalCode;
	}

	public String getPosTerminalStatus() {
		return posTerminalStatus;
	}

	public void setPosTerminalStatus(String posterminalStatus) {
		this.posTerminalStatus = posterminalStatus;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	private Logger logger = WebFrontLogger.getLogger(ReportHeader.class);
	private String title;
	private String date;
	private String formattedDate;
	private String hour;
	private String stream;
	private String storeCode;
	private String type;
	private String filter;
	private String posOperatorCode;
	private String posOperatorName;
	private String posTerminalCode;
	private String posTerminalStatus;
	private String entityCode;
	private String entityName;

}
