package com.ncr.webfront.base.plugins.reports.dataobject;

import org.apache.commons.lang3.builder.ToStringBuilder;

abstract class DataObject {
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
