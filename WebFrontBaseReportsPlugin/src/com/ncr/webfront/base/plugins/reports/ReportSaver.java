package com.ncr.webfront.base.plugins.reports;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.manager.BaseReportManager;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

import org.apache.commons.io.FileUtils;

public class ReportSaver {
	private Logger logger = WebFrontLogger.getLogger(ReportSaver.class);
	public static final String reportPath = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + "eodreports";
	public static final String reportPathLast = reportPath + File.separator + "last";

	public ReportSaver() {
		createDirIfNotExist(reportPath);
		manageLastReportsDirectory();
	}

	public void saveReport(BaseReportManager manager, String reportFileName) {
		ReportRenderer reportRenderer = new ReportRenderer();
		try {
			byte[] reportBytes = reportRenderer.render(manager);
			FileOutputStream fos = new FileOutputStream(reportPathLast + File.separator + reportFileName);
			fos.write(reportBytes);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createDirIfNotExist(String path) {
		logger.info("BEGIN (" + path + ")");

		File dir = new File(path);
		if (!dir.exists()) {
			logger.info("Directory \"" + path + "\" does not exist. Creating it...");
			dir.mkdir();
			logger.info("Directory \"" + path + "\" Created.");
		} else {
			logger.info("Directory \"" + path + "\" exists.");
		}
		logger.info("END");
	}

	private void manageLastReportsDirectory() {
		logger.info("BEGIN");
		logger.info("reportPathLast = \"" + reportPathLast + "\"");

		File lastReportsDir = new File(reportPathLast);
		logger.info("lastReportsDir =\"" + lastReportsDir.getAbsolutePath() + "\"");
		if (!lastReportsDir.exists()) {
			logger.info("Directory lastReportsDir does not exist.");
		}

		if (lastReportsDir.exists()) {
			String lastModifiedTime = "unknowndate";
			try {
				logger.info("Directory lastReportsDir exists.");
				SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
				Path firstReport = lastReportsDir.toPath();
				BasicFileAttributes attributes = Files.readAttributes(firstReport, BasicFileAttributes.class);
				lastModifiedTime = formatter.format(attributes.lastModifiedTime().toMillis());
			} catch (Exception e) {
				logger.error("Exception getting timestamp from lastReportsDir!", e);
				lastModifiedTime = "unknowndate";
			}
			logger.info("lastModifiedTime = \"" + lastModifiedTime + "\"");
			File dateReportsDir = new File(reportPath + File.separator + lastModifiedTime);
			try {
				if (dateReportsDir.exists()) {
					for (int index = 1; index < 100; index++) {
						dateReportsDir = new File(reportPath + File.separator + lastModifiedTime + "." + index);
						if (!dateReportsDir.exists()) {
							break;
						}
					}
				}
			} catch (Exception e) {
				logger.error("Exception getting dateReportsDir!", e);
			}
			logger.info("dateReportsDir = \"" + dateReportsDir.getAbsolutePath() + "\"");
			try {
				logger.info("Calling FileUtils.moveDirectory (\"" + lastReportsDir + "\", \"" + dateReportsDir + "\")");
				FileUtils.moveDirectory(lastReportsDir, dateReportsDir);
				logger.info("Called Utils.moveDirectory (\"" + lastReportsDir + "\", \"" + dateReportsDir + "\")");
			} catch (IOException e) {
				logger.error("Exception CALLING FileUtils.moveDirectory!", e);
			}
		}
		try {
			logger.info("Creating directory \"" + lastReportsDir.getAbsolutePath() + "\"...");
			lastReportsDir.mkdir();
			logger.info("Directory \"" + lastReportsDir.getAbsolutePath() + "\" created.");
		} catch (Exception e) {
			logger.error("Exception!", e);
			e.printStackTrace();
		}
		logger.info("END");
	}

}
