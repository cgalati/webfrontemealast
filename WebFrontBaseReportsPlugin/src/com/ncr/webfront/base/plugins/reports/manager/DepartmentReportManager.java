package com.ncr.webfront.base.plugins.reports.manager;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.ncr.webfront.base.plugins.reports.ReportsMappingProperties;
import com.ncr.webfront.base.plugins.reports.dataobject.DepartmentReport;
import com.ncr.webfront.base.plugins.reports.dataobject.DepartmentReportGroup;
import com.ncr.webfront.base.plugins.reports.dataobject.DepartmentReportItem;
import com.ncr.webfront.base.plugins.reports.dataobject.ReportHeader;
import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class DepartmentReportManager extends BaseReportManager {
	public DepartmentReportManager(ReportOptions reportOptions) throws Exception {
		super(reportOptions);

		this.reportOptions = reportOptions;
		setGroupingsAndDepartments(reportOptions.getPosDepartments());
		setDepartmentsAndDescriptions(reportOptions.getPosDepartments());
	}

	@Override
	public String getJasperFileName() {
		return ReportsMappingProperties.getInstance().getProperty(ReportsMappingProperties.departmentJasperPath, true);
	}

	@Override
	public InputStream innerGeneration() throws SQLException {
		DepartmentReport report = new DepartmentReport();
		InputStream xmlOutput = null;
		
		String subTitle = "";
		logger.debug("getReportOptions().getSubTitle() =" + getReportOptions().getSubTitle());
		if ((getReportOptions().getSubTitle() != null) && (!getReportOptions().getSubTitle().isEmpty())) {
			logger.debug("Replacing subTitle with the subTitle from options...");
			subTitle = getReportOptions().getSubTitle();
		} else {
			if (!getPosTerminals().equals("ALL")) {
				subTitle = "TERMINALS: " + getPosTerminals();
			} else {
				subTitle = "TERMINALS: ALL";
			}
		}
		logger.debug("subTitle = " + subTitle);
		
		report.setHeader(createHeader("pdf", subTitle, "liv0", ""));
		report.setRowSet(createRowSetForDepartment(getReportDate(), getPosTerminals()));
		insertReportTotal(report, getReportDate(), getPosTerminals());
		report.processReport("pdf");
		logger.info(String.format("[generateJasperReport] report = [%s]", report));

		xmlOutput = processXmlOutput(report);
		logger.info(xmlOutput);
		return xmlOutput;
	}

	private ReportHeader createHeader(String stream, String posTerminalId, String level, String value) {
		ReportHeader head = loadHeader(stream);

		if (posTerminalId.equals("ALL")) {
			head.setType("Totale");
		} else {
			head.setType("Parziale Terminale");
		}
		head.setPosTerminalCode(posTerminalId);

		String title = "DEPARTMENT REPORT (26)";
		logger.debug("getReportOptions().getTitle() =" + getReportOptions().getTitle());
		if ((getReportOptions().getTitle() != null) && (!getReportOptions().getTitle().isEmpty())) {
			logger.debug("Replacing title with the title from options...");
			title = getReportOptions().getTitle();
		}
		logger.debug("title = "+ title);
		head.setTitle(title);
		
		head.setPosTerminalStatus("");
		head.setPosOperatorCode(level);
		head.setPosOperatorName(value);

		return head;
	}

	private List<DepartmentReportGroup> createRowSetForDepartment(String date, String posTerminalId)
			throws SQLException {
		List<DepartmentReportGroup> rowSet = new ArrayList<>();

		logger.info(String.format("[createRowsetForDepartment] groupingsAndDepartments = [%s]",
				groupingsAndDepartments));

		Set<String> keys = groupingsAndDepartments.keySet();
		boolean found = false;
		List<String> keyList = new ArrayList<>();
		for (String key : keys) {
			found = false;

			for (int i = 0; (i < keyList.size()) && !found; i++) {
				if (key.compareTo(keyList.get(i)) < 0) {
					keyList.add(i, key);

					found = true;
				}
			}

			if (!found) {
				keyList.add(key);
			}
		}
		logger.info("[createRowsetForDepartment] keyList = [%s] " + keyList);
		
		addDeletedDepartments(keyList, date, posTerminalId);
		
		logger.info("removed departments added");

		addMajorsToSelf(date, posTerminalId);
		
		logger.info("added majors to self");
		logger.info("[createRowsetForDepartment] keyList = [%s] " + keyList);
		Gson gson = new Gson();
		boolean detail = gson.fromJson(reportOptions.getAdditionalInfo(), Boolean.class);
		logger.info("[createRowsetForDepartment] detail " + detail);
		for (int j = 0; j < keyList.size(); j++) {
			logger.info("[createRowsetForDepartment] keyList.size() " +  keyList.size());
			List<DepartmentReportItem> departmentList = new ArrayList<>();
			
			DepartmentReportGroup group = new DepartmentReportGroup(detail);
			String key = keyList.get(j);
			logger.info(String.format("[createRowsetForDepartment] key = [%s]", key));
			List<String> departmentIdList = groupingsAndDepartments.get(key);
			logger.info(String.format("[createRowsetForDepartment] departmentIdList = [%s]",
					departmentIdList));

			group.setCode(key);
			group.setDescription(ReportsConstants.DESCRIZIONE_RAGGRUPPAMENTO);
			group.setNetDescription(ReportsConstants.DESCRIZIONE_NETTE);
			group.setSalesDescription(ReportsConstants.DESCRIZIONE_SCONTI);
			group.setDate(date);
			group.setDescription(departmentsAndDescriptions.get(key));
			
			if (departmentsAndDescriptions.get(key) == null) {
				logger.debug("descrizione nulla");
				continue;
			}
			
			for (int i = 0; i < departmentIdList.size(); i++) {
				DepartmentReportItem department = createDepartment(date, departmentIdList.get(i), posTerminalId);

				if (department != null) {
					department.setMajorCode(key);
					departmentList.add(department);
				}
			}
			if (departmentList.size() > 0) {
				group.setDepartments(departmentList);
				rowSet.add(group);
			}
		}

		return rowSet;
	}
	
	private void addMajorsToSelf(String date, String posTerminalId) throws SQLException {
		List<String> departments = getVendingDepartmentsAt(date);
		for (String department : departments) {
			if (createDepartment(date, department, posTerminalId) != null) {				
				for (String major : groupingsAndDepartments.keySet()) {
					if (major.equals(department)) {
						for (List<String> grouping : groupingsAndDepartments.values()) {
							if (grouping.contains(department)) {
								grouping.remove(department);
							}
						}
						List<String> deptList = groupingsAndDepartments.get(major);
						deptList.add(department);
					}
				}
			}
		}		
	}
	
	private void addDeletedDepartments(List<String> groupings, String date, String posTerminalId) throws SQLException {
		List<String> removedDepartments = getDeletedDepartmentsFor(date, posTerminalId);
		
		logger.info(String.format("removedDepartments [%s]", removedDepartments));
		
		groupingsAndDepartments.put("????", removedDepartments);
		groupings.add("????");
	}
	
	private List<String> getDeletedDepartmentsFor(String date, String posTerminalId) throws SQLException {
		List<String> removedDepartments = new ArrayList<>();
		List<String> departments = getVendingDepartmentsAt(date);
		
		for (String department : departments) {
			if (createDepartment(date, department, posTerminalId) != null) {
				boolean contained = false;
				
				for (List<String> grouping : groupingsAndDepartments.values()) {
					if (grouping.contains(department)) {
						contained = true;
					}
				}
				
				for (String major : groupingsAndDepartments.keySet()) {
					if (major.equals(department)) {
						contained = true;
					}
				}
				
				if (!contained) {
					logger.info(String.format("department [%s] was indeed removed", department));
					
					removedDepartments.add(department);
				}
			}
		}
		
		return removedDepartments;
	}
	
	private List<String> getVendingDepartmentsAt(String date) throws SQLException {
		List<String> departments = new ArrayList<>();
		//String query = String.format("select distinct dpt.department_number from IDC_%sDPT dpt where dpt.DDATE=to_date(%s,'yyyy-mm-dd')",
		//		getExtractionTable(), GeneralPurpose.oraFormatString(date));
		String query = String.format("select distinct dpt.department_number from IDC_%sDPT dpt where dpt.DDATE=convert(DATETIME, %s, 102)",
				getExtractionTable(), GeneralPurpose.oraFormatString(date));
		
		try (Statement statement = connection.createStatement()) {
			logger.info(String.format("query [%s]", query));

			try (ResultSet rs = statement.executeQuery(query)) {
				while (rs.next()) {
					if ((rs.getString("department_number") != null)) {
						departments.add(rs.getString("department_number"));
					}
				}
				logger.info(String.format("departments [%s]", departments));
			}
		} catch (SQLException e) {
			logger.error("", e);

			throw e;
		}
		
		return departments;
	}

	private DepartmentReportItem createDepartment(String date, String departmentCode, String posTerminalId)
			throws SQLException {
		StringBuilder queryFormat = new StringBuilder(
				"select sum(CASE WHEN dpt.totaltype=0 THEN dpt.customercounter ELSE 0 END) as clienti, sum(dpt.itemcounter) as articoli, ");
		StringBuilder queryEnd = new StringBuilder();
		DepartmentReportItem department = new DepartmentReportItem();
		List<Object> values = null;

		queryFormat.append(String.format("sum(dpt.amount) as importo from IDC_%sDPT", getExtractionTable()));
		queryFormat.append(String.format(" dpt where dpt.department_number=%s",
				GeneralPurpose.oraFormatString(departmentCode)));

		queryEnd.append(String.format(" and dpt.DDATE=convert(DATETIME, %s, 102) ", GeneralPurpose.oraFormatString(date)));
		//queryEnd.append(String.format(" and dpt.DDATE=to_date(%s,'yyyy-mm-dd') ", GeneralPurpose.oraFormatString(date)));
		if (!posTerminalId.equals("ALL")) {
			StringTokenizer ids = new StringTokenizer(posTerminalId, ",");

			queryEnd.append(" and (POSNUMBER=" + GeneralPurpose.oraFormatNumber(ids.nextToken()));
			while (ids.hasMoreTokens()) {
				queryEnd.append(" or POSNUMBER=" + GeneralPurpose.oraFormatNumber(ids.nextToken()));
			}
			queryEnd.append(")");
		}

		queryFormat.append(queryEnd);
		if (getStream().equals(ReportsConstants.STREAM_FLAT)) {
			queryFormat.append(" and dpt.checkernumber < 800 ");
		}
		queryFormat.append(" and totaltype != 1 and totaltype != 3");

		department.setCode(departmentCode);
		department.setNetDescription("Net Sales");
		department.setSalesDescription("Discounts");
		if (departmentsAndDescriptions.containsKey(departmentCode)) {
			department.setDescription(departmentsAndDescriptions.get(departmentCode));
		} else {
			department.setDescription("DEPARTMENT REMOVED");
		}
		logger.info("department.getCode()        = " + department.getCode());
		logger.info("department.getDescription() = " + department.getDescription());

		values = getDepartmentValue(queryFormat.toString());
		if (values != null) {
			department.setTotalAmount((float) values.get(0) / 100);
			department.setArticlesTotal((int) values.get(1));
			department.setCustomersTotal((int) values.get(2));
		} else {
			logger.info("[createDepartment] department not extracted with current query");
			logger.info("[createDepartment] END");

			return null;
		}

		queryFormat = new StringBuilder(
				"select sum(dpt.customercounter) as clienti, sum(dpt.itemcounter) as articoli, ");
		queryFormat.append(String.format("sum(dpt.amount) as importo from IDC_%sDPT", getExtractionTable()));
		queryFormat.append(String.format(" dpt where dpt.department_number=%s",
				GeneralPurpose.oraFormatString(departmentCode)));
				//GeneralPurpose.oraFormatString(departmentCode.replace('*', '0'))));
		queryFormat.append(queryEnd);

		values = getDepartmentValue(queryFormat.toString() + " and (totaltype = 1 or totaltype = 3)");
		if (values != null) {
			department.setSalesAmount((float) values.get(0) / 100);
			department.setArticlesSales((int) values.get(1));
			department.setCustomersSales((int) values.get(2));
		}

		values = getDepartmentValue(queryFormat.toString());
		if (values != null) {
			department.setNetAmount((float) values.get(0) / 100);
		}

		return department;
	}

	private List<Object> getDepartmentValue(String query) throws SQLException {
		List<Object> values = null;

		try (Statement statement = connection.createStatement()) {
			logger.info(String.format("[getDepartmentValue] query = [%s]", query));

			try (ResultSet rs = statement.executeQuery(query)) {
				while (rs.next()) {
					if ((rs.getString("importo") != null) && (rs.getString("articoli") != null)
							&& (rs.getString("clienti") != null)) {
						values = new ArrayList<>();

						values.add(rs.getFloat("importo"));
						values.add(rs.getInt("articoli"));
						values.add(rs.getInt("clienti"));
					}
				}
				logger.info(String.format("[getDepartmentValue] values = [%s]", values));
			}
		} catch (SQLException e) {
			logger.error("", e);

			throw e;
		}

		return values;
	}

	private void insertReportTotal(DepartmentReport report, String date, String posTerminalId) throws SQLException {
		//StringBuilder queryFormat = new StringBuilder(
		//		"SELECT sum(coalesce(customercounter,0)) as clienti FROM IDC_%sACT where DDATE=to_date(%s,'yyyy-mm-dd')");
		StringBuilder queryFormat = new StringBuilder(
				"SELECT sum(coalesce(customercounter,0)) as clienti FROM IDC_%sACT where DDATE=convert(DATETIME, %s,102)");

		if (!posTerminalId.equals("ALL")) {
			StringTokenizer ids = new StringTokenizer(posTerminalId, ",");

			queryFormat.append(" and (POSNUMBER=" + GeneralPurpose.oraFormatNumber(ids.nextToken()));
			while (ids.hasMoreTokens()) {
				queryFormat.append(" or POSNUMBER=" + GeneralPurpose.oraFormatNumber(ids.nextToken()));
			}
			queryFormat.append(")");
		}
		if (getStream().equals(ReportsConstants.STREAM_FLAT)) {
			queryFormat.append(" and checkernumber < 800");
		}

		try (Statement statement = connection.createStatement()) {
			String query = String.format(queryFormat.toString(), getExtractionTable(),
					GeneralPurpose.oraFormatString(date));
			logger.info(String.format("[insertReportTotal] query = [%s]", query));

			try (ResultSet rs = statement.executeQuery(query)) {
				while (rs.next()) {
					if ((rs.getString("clienti") != null)) {
						report.setCustomersTotal(rs.getInt("clienti"));
					}
				}
			}
		} catch (SQLException e) {
			logger.error("", e);

			throw e;
		}
	}

	public Map<String, List<String>> getGroupingsAndDepartments() {
		return groupingsAndDepartments;
	}

	private void setGroupingsAndDepartments(List<PosDepartment> posDepartments) {
		logger.debug("posDepartments size "+posDepartments.size());
		for (PosDepartment posDepartment : posDepartments) {
			String total = posDepartment.getTotal();

			if (!groupingsAndDepartments.containsKey(total)) {
				groupingsAndDepartments.put(total, new ArrayList<String>());
			}
			groupingsAndDepartments.get(total).add(posDepartment.getCode());
		}
	}

	public Map<String, String> getDepartmentsAndDescriptions() {
		return departmentsAndDescriptions;
	}

	private void setDepartmentsAndDescriptions(List<PosDepartment> posDepartments) {
		for (PosDepartment posDepartment : posDepartments) {
			String description = StringUtils.substring(posDepartment.getDescription(), 0, 15);

			departmentsAndDescriptions.put(posDepartment.getCode(), description);
		}
	}

	private Logger logger = WebFrontLogger.getLogger(DepartmentReportManager.class);
	private Map<String, List<String>> groupingsAndDepartments = new HashMap<>();
	private Map<String, String> departmentsAndDescriptions = new HashMap<>();
	private ReportOptions reportOptions;
}
