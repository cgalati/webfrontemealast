package com.ncr.webfront.base.plugins.reports.manager;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.dataobject.BaseReport;
import com.ncr.webfront.base.plugins.reports.dataobject.ReportHeader;
import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.WebFrontEnvParameters;
import com.ncr.webfront.core.utils.db.DbHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class BaseReportManager {
	public final static String DAILY_TABLE = "DAILY";
	public final static String CONSOLIDATED_TABLE = "EOD";
	private ReportOptions reportOptions;

	public BaseReportManager(ReportOptions reportOptions) throws Exception {
		this.reportOptions = reportOptions;
		logger.debug("getting the connection");

		connection = DbHelper.getConnection();

		logger.debug("connection with db esta" + "blished");

		setExtractionTable(reportOptions.isConsolidated());
		setReportDate(reportOptions.getDate());
		setPosOperators(reportOptions.getPosOperators());
		setPosTerminals(reportOptions.getPosTerminals());
		setReportFormat(reportOptions.getFormat());
	}

	public abstract String getJasperFileName();

	public InputStream generateJasperReport() throws SQLException {
		InputStream generated = null;

		try {
			generated = innerGeneration();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		finally {
			logger.debug("freeing a connection");

			connection.close();

			logger.debug("connecion freed");
		}

		return generated;
	}

	protected abstract InputStream innerGeneration() throws SQLException;

	public ReportHeader loadHeader(String stream) {
		ReportHeader head = new ReportHeader();

		head.setStoreCode(new WebFrontEnvParameters().getStoreCode());
		head.setType("Totale");
		head.setFilter(getReportDate());
		head.setTitle("Rapporto");
		head.setStream(stream);
		head.setDate(getReportDate());
		head.setFormattedDate(getFormattedReportDate());
		head.setHour(new SimpleDateFormat("hh:mm").format(new Date()));
		logger.info(String.format("[loadHeader] head = [%s]", head));

		return head;
	}

	protected List<String> getGroupsSummaryList() throws SQLException {
		List<String> list = new ArrayList<>();

		String queryFormat = "select description as des, flaginfo as fi from idc_financial_plan fin where fin.FINANCIALTOTALID = '%s'";
		int cursorId = ReportsConstants.FINTOTID_RIEPILOGO_BASEID;

		while (cursorId < ReportsConstants.FINTOTID_RIEPILOGO_MAXLIMIT) {
			String query = String.format(queryFormat, GeneralPurpose.padLeft(Integer.toString(cursorId), 4, "0"));
			logger.info(String.format("[getGroupsSummaryList] query = [%s]", query));

			try (Statement statement = connection.createStatement()) {
				try (ResultSet rs = statement.executeQuery(query)) {
					if (rs.next()) {
						String groupSummary = String.format("%d%s%d%s%s%s%s", cursorId, ReportsConstants.NCR_INFLIST_SEPARATOR, cursorId + 8,
								ReportsConstants.NCR_INFLIST_SEPARATOR, rs.getString("des"), ReportsConstants.NCR_INFLIST_SEPARATOR, rs.getString("fi"));

						list.add(groupSummary);
						logger.info(String.format("[getGroupsSummaryList] groupSummary = [%s]", groupSummary));
					}
				} catch (SQLException e) {
					logger.error("", e);

					throw e;
				}
			} catch (SQLException e) {
				logger.error("", e);

				throw e;
			}

			cursorId += ReportsConstants.FINTOTID_RIEPILOGO_STEP;
		}
		logger.info(String.format("[getGroupsSummaryList] list size = [%d]", list.size()));

		return list;
	}

	protected InputStream processXmlOutput(BaseReport<?> report) {
		return new ByteArrayInputStream(report.getXml().getBytes());
	}

	public String getExtractionTable() {
		return extractionTable;
	}

	private void setExtractionTable(boolean consolidated) {
		if (consolidated) {
			extractionTable = CONSOLIDATED_TABLE;
		} else {
			extractionTable = DAILY_TABLE;
		}
	}

	public String getReportDate() {
		return reportDate;
	}

	public String getFormattedReportDate() {
		return formattedReportDate;
	}

	private void setReportDate(Date reportDate) {
		this.reportDate = new SimpleDateFormat("yyyy-MM-dd").format(reportDate);
		this.formattedReportDate = new SimpleDateFormat("dd/MM/yyyy").format(reportDate);
	}

	public boolean isRimapped() {
		return false;
	}

	public int getFinTotIdCli() {
		return finTotIdCli;
	}

	public String getStream() {
		return ReportsConstants.STREAM_PDF;
	}

	public boolean isRealFinanziario() {
		return realFinanziario;
	}

	public String getPosTerminals() {
		return posTerminals;
	}

	private void setPosTerminals(List<String> posTerminals) {
		this.posTerminals = StringUtils.join(posTerminals, ",");
		if (getPosTerminals().isEmpty()) {
			this.posTerminals = "ALL";
		}
	}

	public String getPosOperators() {
		return posOperators;
	}

	private void setPosOperators(List<String> posOperators) {
		this.posOperators = StringUtils.join(posOperators, ",");
		if (getPosOperators().isEmpty()) {
			this.posOperators = "ALL";
		}
	}

	public String getReportFormat() {
		return reportFormat;
	}

	public void setReportFormat(String reportFormat) {
		this.reportFormat = reportFormat;
	}

	public ReportOptions getReportOptions() {
		return reportOptions;
	}

	private Logger logger = WebFrontLogger.getLogger(BaseReportManager.class);
	protected Connection connection;
	private String extractionTable;
	private String reportDate;
	private String formattedReportDate;
	private int finTotIdCli;
	private boolean realFinanziario;
	private String posTerminals;
	private String posOperators;
	private String reportFormat;

}
