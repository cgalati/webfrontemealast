package com.ncr.webfront.base.plugins.reports.dataobject;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class HourlyActivityReportRow extends DataObject {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<fascia codice='").append(getStrCodiceFasciaOraria()).append("' >\n");
		sb.append("<data>").append(this.getStrData()).append("</data>\n");
		sb.append("<ora>").append(this.getStrOra()).append("</ora>\n");
		sb.append("<importovenduto>").append(GeneralPurpose.formatNumber(Float.toString(getFltImporto()), "#,###.00"))
				.append("</importovenduto>\n");
		sb.append("<tassovenduto>")
				.append(GeneralPurpose.formatNumber(Float.toString(getFltTassoImporto()), "#,###.00"))
				.append("</tassovenduto>\n");
		sb.append("<quantitaarticoli>").append(getIntArticoli()).append("</quantitaarticoli>\n");
		sb.append("<tassoarticoli>")
				.append(GeneralPurpose.formatNumber(Float.toString(getFltTassoArticoli()), "#,###.00"))
				.append("</tassoarticoli>\n");
		sb.append("<quantitaclienti>").append(getIntClienti()).append("</quantitaclienti>\n");
		sb.append("<tassoclienti>")
				.append(GeneralPurpose.formatNumber(Float.toString(getFltTassoClienti()), "#,###.00"))
				.append("</tassoclienti>\n");
		sb.append("<statistichevenduto>")
				.append(GeneralPurpose.formatNumber(Float.toString(getFltVendutoSuClienti()), "#,###.00"))
				.append("</statistichevenduto>\n");
		sb.append("<statistichearticoli>")
				.append(GeneralPurpose.formatNumber(Float.toString(getFltArticoliSuClienti()), "#,###.00"))
				.append("</statistichearticoli>\n");
		sb.append("<statistichesubtot>")
				.append(GeneralPurpose.formatNumber(Float.toString(getFltSubTotale()), "#,###.00"))
				.append("</statistichesubtot>\n");
		sb.append("</fascia>\n");
		logger.trace(String.format("[getXml] sb = [%s]", sb));

		return sb.toString();
	}

	public String getStrData() {
		return strData;
	}

	public void setStrData(String strDataIn) {
		this.strData = strDataIn;
	}

	public String getStrOra() {
		return strOra;
	}

	public void setStrOra(String strOra) {
		this.strOra = strOra;
	}

	public float getFltImporto() {
		return fltImporto;
	}

	public void setFltImporto(float fltImporto) {
		this.fltImporto = fltImporto;
	}

	public float getFltTassoImporto() {
		return fltTassoImporto;
	}

	public void setFltTassoImporto(float fltTassoImporto) {
		this.fltTassoImporto = fltTassoImporto;
	}

	public int getIntArticoli() {
		return intArticoli;
	}

	public void setIntArticoli(int intArticoli) {
		this.intArticoli = intArticoli;
	}

	public float getFltTassoArticoli() {
		return fltTassoArticoli;
	}

	public void setFltTassoArticoli(float fltTassoArticoli) {
		this.fltTassoArticoli = fltTassoArticoli;
	}

	public int getIntClienti() {
		return intClienti;
	}

	public void setIntClienti(int intClienti) {
		this.intClienti = intClienti;
	}

	public float getFltTassoClienti() {
		return fltTassoClienti;
	}

	public void setFltTassoClienti(float fltTassoClienti) {
		this.fltTassoClienti = fltTassoClienti;
	}

	public float getFltVendutoSuClienti() {
		return fltVendutoSuClienti;
	}

	public void setFltVendutoSuClienti(float fltVendutoSuClienti) {
		this.fltVendutoSuClienti = fltVendutoSuClienti;
	}

	public float getFltArticoliSuClienti() {
		return fltArticoliSuClienti;
	}

	public void setFltArticoliSuClienti(float fltArticoliSuClienti) {
		this.fltArticoliSuClienti = fltArticoliSuClienti;
	}

	public float getFltSubTotale() {
		return fltSubTotale;
	}

	public void setFltSubTotale(float fltSubTotale) {
		this.fltSubTotale = fltSubTotale;
	}

	public String getStrCodiceFasciaOraria() {
		return strCodiceFasciaOraria;
	}

	public void setStrCodiceFasciaOraria(String strCodiceFasciaOraria) {
		this.strCodiceFasciaOraria = strCodiceFasciaOraria;
	}

	private Logger logger = WebFrontLogger.getLogger(HourlyActivityReportRow.class);
	private String strData;
	private String strOra;
	private int intClienti;
	private float fltTassoClienti;
	private float fltVendutoSuClienti;
	private float fltArticoliSuClienti;
	private float fltSubTotale;
	private float fltImporto;
	private float fltTassoImporto;
	private int intArticoli;
	private float fltTassoArticoli;
	private String strCodiceFasciaOraria;

}
