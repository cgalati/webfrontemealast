package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.List;

public abstract class BaseReport<T> extends DataObject {
	public abstract String getXml();

	public ReportHeader getHeader() {
		return header;
	}

	public void setHeader(ReportHeader header) {
		this.header = header;
	}

	public List<T> getRowSet() {
		return rowSet;
	}

	public void setRowSet(List<T> rowSet) {
		this.rowSet = rowSet;
	}

	private ReportHeader header;
	private List<T> rowSet;
}
