package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.ArrayList;
import java.util.List;

public class FinancialReportRow extends DataObject {
	public void processFlagInfo() {
		List<Boolean> flags = new ArrayList<>();

		char first = getStrFlagInfo().charAt(0);
		char second = getStrFlagInfo().charAt(1);

		if (first == '8') {
			setStrFlagInfo("00");
			first = getStrFlagInfo().charAt(0);
		}
		
		int number1 = Integer.decode(String.format("0x%c", first));
		int number2 = Integer.decode(String.format("0x%c", second));

		flags.add(new Boolean((number1 & 0x4) == 4));
		flags.add(new Boolean((number1 & 0x2) == 2));
		flags.add(new Boolean((number1 & 0x1) == 1));
		
		flags.add(new Boolean((number2 & 0x4) == 4));
		flags.add(new Boolean((number2 & 0x2) == 2));
		flags.add(new Boolean((number2 & 0x1) == 1));

		setFlags(flags);
	}

	public int getIntFinancialTotalId() {
		return intFinancialTotalId;
	}

	public void setIntFinancialTotalId(int intFinancialTotalId) {
		this.intFinancialTotalId = intFinancialTotalId;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getStrFlagInfo() {
		return strFlagInfo;
	}

	public void setStrFlagInfo(String strFlagInfo) {
		this.strFlagInfo = strFlagInfo;
	}

	public String getStrType() {
		return strType;
	}

	public void setStrType(String strType) {
		this.strType = strType;
	}

	public List<Boolean> getFlags() {
		return flags;
	}

	public void setFlags(List<Boolean> vctFlags) {
		this.flags = vctFlags;
	}

	private int intFinancialTotalId;
	private String strDescription;
	private String strFlagInfo;
	private String strType;
	private List<Boolean> flags;
}
