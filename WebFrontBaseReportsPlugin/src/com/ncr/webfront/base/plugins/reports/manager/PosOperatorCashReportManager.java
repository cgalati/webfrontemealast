package com.ncr.webfront.base.plugins.reports.manager;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.ReportsMappingProperties;
import com.ncr.webfront.base.plugins.reports.dataobject.PosOperatorCashReport;
import com.ncr.webfront.base.plugins.reports.dataobject.PosOperatorCashReportRow;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.core.utils.db.DbHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class PosOperatorCashReportManager {

	private final Logger logger = WebFrontLogger.getLogger(PosOperatorCashReportManager.class);
	private Connection connection;
	private Map<PosOperator, String> posOperatorsTerminalCodeMap;
	public static final String CASH_FINALCIAL_ID = "1101";   

	public PosOperatorCashReportManager(Map<PosOperator, String> posOperatorsTerminalCodeMap) throws Exception {
		connection = DbHelper.getConnection();
		this.posOperatorsTerminalCodeMap = posOperatorsTerminalCodeMap;
		
//		JasperCompileManager.compileReportToFile("d:\\Profiles\\ffanelli\\Desktop\\jasper reports\\pos-operator-cash-report.jrxml", "d:\\Profiles\\ffanelli\\Desktop\\jasper reports\\test.jasper");

	}

	public String getJasperFileName() {
		return ReportsMappingProperties.getInstance().getProperty(
				ReportsMappingProperties.posOperatorCashJasperPath, true);
	}

	private List<PosOperatorCashReportRow> createRowSet() {
		List<PosOperatorCashReportRow> rowSet = new ArrayList<PosOperatorCashReportRow>();
		boolean posOperatorCashIgnoreEmptyOperators = Boolean.valueOf(ReportsMappingProperties.getInstance().getProperty(ReportsMappingProperties.posOperatorCashIgnoreEmptyOperators));
		
		for ( PosOperator posOperator : posOperatorsTerminalCodeMap.keySet()) {
			PosOperatorCashReportRow posOperatorCashReportRow = getPosOperatorCashRow(posOperator);
			if (posOperatorCashReportRow != null) {
				if (posOperatorCashReportRow.getTerminal().isEmpty()) {
					posOperatorCashReportRow.setTerminal(" "); // empty string generates null value in jasper report
					if (posOperatorCashIgnoreEmptyOperators) {
						if (posOperatorCashReportRow.getCash() == 0) { // terminal code isEmpty so if cash equals 0 
							continue;								   // then ignore the current posOperator
						}
					}
				}
			}
			rowSet.add(posOperatorCashReportRow);
		}
		return rowSet;
	}

	private PosOperatorCashReportRow getPosOperatorCashRow(PosOperator posOperator) {
		PosOperatorCashReportRow posOperatorCashRow = null;

		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			statement = connection
					.prepareStatement("SELECT SUM(AMOUNT) AS CASH_SUM"
							+ " FROM "
							+ ReportsConstants.DAILY_TABELLA_CASSIERE
							+ " WHERE FINANCIALTOTALID = ? AND CHECKERNUMBER = ? ");

			statement.setString(1, CASH_FINALCIAL_ID);
			statement.setInt(2, Integer.parseInt(posOperator.getOperatorCode()));
			rs = statement.executeQuery();
			while (rs.next()) {
				posOperatorCashRow = new PosOperatorCashReportRow(
						posOperator.getOperatorCode(), posOperator.getName(),
						getPosOperatorTerminalCode(posOperator), rs.getInt("CASH_SUM"));
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
		
		return posOperatorCashRow;
	}

	public InputStream generateJasperReportFromXML() throws JRException, UnsupportedEncodingException {
		InputStream xmlOutput = null;
		PosOperatorCashReport posOperatorCashReport = new PosOperatorCashReport();
		posOperatorCashReport.setRowSet(createRowSet());
		xmlOutput =  new ByteArrayInputStream(posOperatorCashReport.getXml().getBytes("UTF-8"));
		return xmlOutput;
	}
	
	public String getPosOperatorTerminalCode(PosOperator posOperator) {
		return posOperatorsTerminalCodeMap.get(posOperator);
	}

}
