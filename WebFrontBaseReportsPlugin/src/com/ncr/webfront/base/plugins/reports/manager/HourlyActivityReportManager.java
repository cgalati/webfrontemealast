package com.ncr.webfront.base.plugins.reports.manager;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.ReportsMappingProperties;
import com.ncr.webfront.base.plugins.reports.dataobject.HourlyActivityReport;
import com.ncr.webfront.base.plugins.reports.dataobject.HourlyActivityReportRow;
import com.ncr.webfront.base.plugins.reports.dataobject.ReportHeader;
import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class HourlyActivityReportManager extends BaseReportManager {
	public HourlyActivityReportManager(ReportOptions reportOptions) throws Exception {
		super(reportOptions);
	}

	public String getJasperFileName() {
		return ReportsMappingProperties.getInstance().getProperty(ReportsMappingProperties.hourlyActivityJasperPath, true);
	}

	public InputStream innerGeneration() throws SQLException {
		HourlyActivityReport objReport = new HourlyActivityReport();
		
		String subTitle = "";
		logger.debug("getReportOptions().getSubTitle() =" + getReportOptions().getSubTitle());
		if ((getReportOptions().getSubTitle() != null) && (!getReportOptions().getSubTitle().isEmpty())) {
			logger.debug("Replacing subTitle with the subTitle from options...");
			subTitle = getReportOptions().getSubTitle();
		} else {
			if (!getPosTerminals().equals("ALL")) {
				subTitle = "TERMINALS: " + getPosTerminals();
			} else {
				subTitle = "TERMINALS: ALL";
			}
		}
		logger.debug("subTitle = " + subTitle);
		
		objReport.setHeader(createHeader("pdf", subTitle, getReportDate()));
		objReport.setRowSet(createRowsetForAttivitaOraria(getReportDate(), getPosTerminals()));
		objReport.processTotalSubtotalRates();

		InputStream xmlOutput = processXmlOutput(objReport);

		return xmlOutput;
	}

	private ReportHeader createHeader(String stream, String strCassaId, String strDate) throws SQLException {
		ReportHeader head = loadHeader(stream);

		String title = "HOURLY ACTIVITY REPORT (24)";
		logger.debug("getReportOptions().getTitle() =" + getReportOptions().getTitle());
		if ((getReportOptions().getTitle() != null) && (!getReportOptions().getTitle().isEmpty())) {
			logger.debug("Replacing title with the title from options...");
			title = getReportOptions().getTitle();
		}
		logger.debug("title = "+ title);
		head.setTitle(title);
		if (!strCassaId.equals("ALL")) {
			head.setType("Parziale Terminale");
		} else {
			head.setType("ALL");
		}
		head.setPosTerminalCode(strCassaId);
		head.setPosTerminalStatus("");
		head.setPosOperatorCode("");
		head.setPosOperatorName("");

		return head;
	}

	private List<HourlyActivityReportRow> createRowsetForAttivitaOraria(String strDate, String strCassaId)
			throws SQLException {
		List<HourlyActivityReportRow> vctRowset = new ArrayList<>();

		//String strPostQuery = " and mag.DDATE=to_date(" + GeneralPurpose.oraFormatString(strDate) + ",'yyyy-mm-dd') ";
		String strPostQuery = " and mag.DDATE=convert(DATETIME, " + GeneralPurpose.oraFormatString(strDate) + ", 102) ";

		String strQuerySuffix = "";
		if (!strCassaId.equals("ALL")) {
			StringTokenizer st = new StringTokenizer(strCassaId, ",");

			strQuerySuffix += " and (mag.POSNUMBER=" + GeneralPurpose.oraFormatNumber(st.nextToken());
			if (st.hasMoreTokens()) {
				strQuerySuffix += "or mag.POSNUMBER=" + GeneralPurpose.oraFormatNumber(st.nextToken());
			}
			strQuerySuffix += ")";
		}

		Statement objStatement = null;
		ResultSet objResultSet = null;

		// primo ciclo: recupero i codici di fascia oraria, con il relativo
		// orario di partenza
		//String strQuery = "select ana.HOURLYCD as code, substr(ana.STARTTIME,1,2)||':'||substr(ana.STARTTIME,3,2)"
		//		+ "as stime from idc_hourlyact_plan ana";
		String strQuery = "select ana.HOURLYCD as code, substring(ana.STARTTIME,1,2) + ':' + substring(ana.STARTTIME,3,2)"
				+ "as stime from idc_hourlyact_plan ana";

		List<String> vctHourlyCode = new ArrayList<>();
		List<String> vctStartTime = new ArrayList<>();

		objStatement = connection.createStatement();
		objResultSet = objStatement.executeQuery(strQuery.toString());
		logger.info("HourlyActivityReportManager:createRowsetForAttivitaOraria - eseguita query = " + strQuery.toString());
		while (objResultSet.next()) {
			vctHourlyCode.add(objResultSet.getString("code"));
			vctStartTime.add(objResultSet.getString("stime"));
		}

		objResultSet.close();
		objStatement.close();

		logger.info("HourlyActivityReportManager:createRowsetForAttivitaOraria - codici = " + vctHourlyCode.toString());
		logger.info("HourlyActivityReportManager:createRowsetForAttivitaOraria - starttime = " + vctStartTime.toString());

		String strTabella = "IDC_" + this.getExtractionTable() + "ACT";
		String strQueryPrefix = "select  round(mag.AMOUNT, 2) as importo, mag.ITEMCOUNTER as articoli, mag.CUSTOMERCOUNTER as clienti "
				+ "from " + strTabella + " mag where mag.HOURLYCD=";

		float acc_Importo = 0;
		int acc_Articoli = 0;
		int acc_Clienti = 0;
		HourlyActivityReportRow row = null;

		for (int i = 0; i < vctHourlyCode.size(); i++) {
			acc_Importo = 0;
			acc_Articoli = 0;
			acc_Clienti = 0;

			strQuery = strQueryPrefix + (String) vctHourlyCode.get(i);
			strQuery = strQuery + strPostQuery + strQuerySuffix;

			logger.info("HourlyActivityReportManager:createRowsetForAttivitaOraria - impostata query = "
					+ strQuery.toString());
			objStatement = connection.createStatement();
			objResultSet = objStatement.executeQuery(strQuery.toString());
			logger.info("HourlyActivityReportManager:createRowsetForAttivitaOraria - eseguita query = "
					+ strQuery.toString());

			boolean bExistRow = false;
			while (objResultSet.next()) {
				if ((objResultSet.getString("importo") != null) && (objResultSet.getString("articoli") != null)
						&& (objResultSet.getString("clienti") != null)) {
					bExistRow = true;
					acc_Importo += objResultSet.getFloat("importo");
					acc_Articoli += objResultSet.getFloat("articoli");
					acc_Clienti += objResultSet.getFloat("clienti");
				}
			}

			if (bExistRow) {
				row = new HourlyActivityReportRow();
				row.setStrData(strDate);
				row.setStrCodiceFasciaOraria((String) vctHourlyCode.get(i));
				row.setStrOra((String) vctStartTime.get(i));
				row.setFltImporto(acc_Importo / 100);
				row.setIntArticoli(acc_Articoli);
				row.setIntClienti(acc_Clienti);
				row.setFltVendutoSuClienti(row.getFltImporto() / (float) row.getIntClienti());
				row.setFltArticoliSuClienti((float) row.getIntArticoli() / (float) row.getIntClienti());

				vctRowset.add(row);
			}

			objResultSet.close();
			objStatement.close();
		}

		return vctRowset;
	}

	private Logger logger = WebFrontLogger.getLogger(HourlyActivityReportManager.class);
}
