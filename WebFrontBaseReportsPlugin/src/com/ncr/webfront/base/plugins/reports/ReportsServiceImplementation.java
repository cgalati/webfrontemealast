package com.ncr.webfront.base.plugins.reports;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.manager.DepartmentReportManager;
import com.ncr.webfront.base.plugins.reports.manager.EndOfDayReportManager;
import com.ncr.webfront.base.plugins.reports.manager.FinancialReportManager;
import com.ncr.webfront.base.plugins.reports.manager.HourlyActivityReportManager;
import com.ncr.webfront.base.plugins.reports.manager.PosOperatorCashReportManager;
import com.ncr.webfront.base.plugins.reports.manager.PosOperatorReportManager;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.posoperator.PosOperator;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.base.reports.ReportsServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.core.utils.db.DbHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
// +++AMZ  
public class ReportsServiceImplementation implements ReportsServiceInterface {
	public ReportsServiceImplementation (PosDepartmentServiceInterface posDepartmentService){
		// Eagerly loads (and saves) the reports.properties 
		ReportsMappingProperties.getInstance();
		this.posDepartmentService=posDepartmentService;
	}
	
	@Override
	public ErrorObject checkService() {
		try {
			DbHelper.getConnection().close();
			
			checkIfFileExists(ReportsMappingProperties.departmentJasperPath);
			checkIfFileExists(ReportsMappingProperties.financialJasperPath);
			checkIfFileExists(ReportsMappingProperties.hourlyActivityJasperPath);
			checkIfFileExists(ReportsMappingProperties.posOperatorJasperPath);
			checkIfFileExists(ReportsMappingProperties.posOperatorCashJasperPath);
		} catch (Exception e) {
			logger.error("", e);
			
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
		
		return new ErrorObject(ErrorCode.OK_NO_ERROR);
	}
	
	private void checkIfFileExists(String key) throws Exception {
		String path = ReportsMappingProperties.getInstance().getProperty(key, true);
		
		if (!new File(path).exists()) {
			throw new Exception(String.format("File %s inesistente", path));
		}
	}
	
	@Override
	public List<String> getFormats() {
		// +++AMZ -- restituisce i formati disponibili
		// ci mettiamo anche EXCEL e questo li fa apparire nella
		// lista 
		// AMZ-XLS#MOD -- aggiunto tipo EXCEL
		return Arrays.asList("HTML", "PDF","EXCEL", "CSV");
	}
	
	@Override
	public byte[] generatePosOperatorReport(ReportOptions reportOptions) {
		logger.info ("generatePosOperatorReport");
		try {
			return reportRenderer.render(new PosOperatorReportManager(reportOptions));
		} catch (Exception e) {
			logger.error("", e);
			
			return new byte[0];
		}
	}

	@Override
	public byte[] generateFiancialReport(ReportOptions reportOptions) {
		logger.info ("generateFiancialReport");
		try {
			return reportRenderer.render(new FinancialReportManager(reportOptions));
		} catch (Exception e) {
			logger.error("", e);
			
			return new byte[0];
		}
	}

	@Override
	public byte[] generateHourlyActivityReport(ReportOptions reportOptions) {
		logger.info ("generateHourlyActivityReport");
		try {
			return reportRenderer.render(new HourlyActivityReportManager(reportOptions));
		} catch (Exception e) {
			logger.error("", e);
			
			return new byte[0];
		}
	}

	@Override
	public byte[] generateDepartmentReport(ReportOptions reportOptions) {
		logger.info ("generateDepartmentReport");
		try {
			return reportRenderer.render(new DepartmentReportManager(reportOptions));
		} catch (Exception e) {
			logger.error("", e);
			
			return new byte[0];
		}
	}
	
	@Override
	public byte[] generatePosOperatorCashReport(Map<PosOperator, String> posOperatorsTerminalCodeMap, String reportFormat) {
		logger.info ("generatePosOperatorCashReport");
		try {
			return reportRenderer.render(new PosOperatorCashReportManager(posOperatorsTerminalCodeMap), reportFormat);
		} catch (Exception e) {
			logger.error("", e);
			
			return new byte[0];
		}
	}
	
	@Override
	public ErrorObject generateEodReports(List<String> registerList) {
		 Set<PosDepartment> selectedPosDepartmentList = new HashSet<>(posDepartmentService.getAll());
		EndOfDayReportManager eodReportsManager = new EndOfDayReportManager(registerList,new ArrayList<PosDepartment>(selectedPosDepartmentList));
//		eodReportsManager.generateEodReports();
		return null;
	}
	
	private Logger logger = WebFrontLogger.getLogger(ReportsServiceImplementation.class);
	private ReportRenderer reportRenderer = new ReportRenderer();
	private PosDepartmentServiceInterface posDepartmentService = null;

}
