package com.ncr.webfront.base.plugins.reports.dataobject;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class DepartmentReport extends BaseReport<DepartmentReportGroup> {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<report>\n");
		if (!paging) {
			sb.append("<pagina>\n");
			sb.append(getHeader().getXml());
			sb.append("<dati>\n");
			for (int i = 0; i < getRowSet().size(); i++) {
				DepartmentReportGroup group = getRowSet().get(i);

				sb.append(group.getXml());
			}
			sb.append("</dati>\n");
			sb.append(getXmlRowTotals());
			sb.append("</pagina>\n");
		}
		sb.append("</report>\n");
		logger.trace(String.format("[getXml] sb = [%s]", sb));

		return sb.toString();
	}

	private String getXmlRowTotals() {
		StringBuilder sb = new StringBuilder();

		sb.append("<totale>\n");
		sb.append("<importo>").append(GeneralPurpose.formatNumber(Float.toString(totalAmount), "#,###.00"))
				.append("</importo>\n");
		sb.append("<importo_esclusi>")
				.append(GeneralPurpose.formatNumber(Float.toString(amountTotalExcluded), "#,###.00"))
				.append("</importo_esclusi>\n");

		String strTotVenditeLorde = GeneralPurpose.formatNumber(
				Float.toString(totalAmount + amountTotalExcluded), "#,###.00");
		sb.append("<importo_lordo>").append(strTotVenditeLorde).append("</importo_lordo>\n");
		sb.append("<articoli>").append(articlesTotal).append("</articoli>\n");
		sb.append("<clienti>").append(customersTotal).append("</clienti>\n");
		sb.append("<scontrino_medio>")
				.append(GeneralPurpose.formatNumber(Float.toString(receiptAverage), "#,###.00"))
				.append("</scontrino_medio>\n");
		sb.append("</totale>\n");
		logger.trace(String.format("[getRigaTotaliXML] sb = [%s]", sb));

		return sb.toString();
	}

	public void processReport(String stream) {
		float accTotImporto = 0;
		float accTotImportoNette = 0;
		float accTotImportoSconti = 0;
		int accTotArticoli = 0;
		int accTotArticoliSconti = 0;
		int accTotClientiSconti = 0;
		float accTotImportoEsclusi = 0;

		for (int i = 0; i < getRowSet().size(); i++) {
			DepartmentReportGroup group = getRowSet().get(i);

			if (stream.equals(ReportsConstants.STREAM_FLAT)
					|| (!stream.equals(ReportsConstants.STREAM_FLAT) && !group.getCode().equals(
							ReportsConstants.MERCEOLOGICO_MAJOR_NOFOOD_7))) {
				group.calculateTotals();
				accTotImporto += group.getTotalAmount();
				accTotArticoli += group.getArticlesTotal();
				accTotImportoNette += group.getNetAmount();
				accTotImportoSconti += group.getSalesAmount();
				accTotArticoliSconti += group.getArticlesSales();
				accTotClientiSconti += group.getCustomersSales();
			} else {
				group.calculateTotals();
				accTotImportoEsclusi += group.getTotalAmount();
			}
		}
		setTotalAmount(accTotImportoNette);
		setArticlesTotal(accTotArticoli);
		setAmountTotalExcluded(accTotImportoEsclusi);
		setReceiptAverage(0);

		if (this.getCustomersTotal() != 0) {
			setReceiptAverage(getTotalAmount() / getCustomersTotal());
		}

		for (int i = 0; i < getRowSet().size(); i++) {
			DepartmentReportGroup group = getRowSet().get(i);

			group.calculateRates(accTotImporto, getArticlesTotal(), getCustomersTotal(), accTotImportoSconti,
					getTotalAmount(), accTotArticoliSconti, accTotClientiSconti);
		}
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float fltTotaleImporto) {
		this.totalAmount = fltTotaleImporto;
	}

	public int getArticlesTotal() {
		return articlesTotal;
	}

	public void setArticlesTotal(int intTotaleArticoli) {
		this.articlesTotal = intTotaleArticoli;
	}

	public int getCustomersTotal() {
		return customersTotal;
	}

	public void setCustomersTotal(int intTotaleClienti) {
		this.customersTotal = intTotaleClienti;
	}

	public float getReceiptAverage() {
		return receiptAverage;
	}

	public void setReceiptAverage(float fltScontrinoMedio) {
		this.receiptAverage = fltScontrinoMedio;
	}

	public int getReportNumber() {
		return reportNumber;
	}

	public void setReportNumber(int iNumReport) {
		this.reportNumber = iNumReport;
	}

	public boolean isPaging() {
		return paging;
	}

	public void setPaging(boolean flagPaging) {
		this.paging = flagPaging;
	}

	public float getAmountTotalExcluded() {
		return amountTotalExcluded;
	}

	public void setAmountTotalExcluded(float fltTotaleImportoEsclusi) {
		this.amountTotalExcluded = fltTotaleImportoEsclusi;
	}
	
	private Logger logger = WebFrontLogger.getLogger(DepartmentReport.class);
	private float totalAmount;
	private int articlesTotal;
	private int customersTotal;
	private float receiptAverage;
	private int reportNumber = 1;
	private boolean paging = false;
	private float amountTotalExcluded;

}
