package com.ncr.webfront.base.plugins.reports;

import java.io.File;
import java.util.Map;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvMetadataExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

import com.ncr.webfront.base.plugins.reports.manager.BaseReportManager;
import com.ncr.webfront.base.plugins.reports.manager.PosOperatorCashReportManager;
import com.ncr.webfront.core.utils.jasperreports.JasperReportsHelper;

public class ReportRenderer {
	public byte[] render(BaseReportManager manager) throws Exception {
		InputStream report = manager.generateJasperReport();

		return render(new JRXmlDataSource(report), manager.getJasperFileName(), manager.getReportFormat());
	}
	
	public byte[] render(PosOperatorCashReportManager posOperatorCashReportManager, String format) throws Exception {
		
		JRDataSource reportDS = new JRXmlDataSource(posOperatorCashReportManager.generateJasperReportFromXML(), "/report/posOperatorList/posOperator");
		
		return render(reportDS, posOperatorCashReportManager.getJasperFileName(), format);
	}

	private byte[] render(JRDataSource reportDS, String templatePath, String format) throws Exception {
        
		File temporaryFile = File.createTempFile("report", "");
		byte[] output = null;

		switch (format.toLowerCase()) {
		case "pdf":
			JasperRunManager.runReportToPdfFile(templatePath, temporaryFile.getAbsolutePath(), null, reportDS);
			output = read(temporaryFile);
			break;

		default:
		case "html":
			JasperRunManager.runReportToHtmlFile(templatePath, temporaryFile.getAbsolutePath(), null, reportDS);
			output = read(temporaryFile);
			output = JasperReportsHelper.fixImagesOnAHtmlReport(output);
			break;

		case "csv":
			Map csvParameters = new HashMap();
			String csvPrintFileName = JasperFillManager.fillReportToFile(templatePath, csvParameters, reportDS);
			JRCsvExporter csvExporter = new JRCsvExporter();
			csvExporter.setParameter(JRExporterParameter.INPUT_FILE_NAME, csvPrintFileName);
			csvExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, temporaryFile.getAbsolutePath());

			csvExporter.exportReport();
			output = read(temporaryFile);
			break;

		// AMZ-XLS#BEG
		case "excel":
			Map excelParameters = new HashMap();
			String excelPrintFileName = JasperFillManager.fillReportToFile(templatePath, excelParameters, reportDS);
			JRXlsExporter excelExporter = new JRXlsExporter();
			excelExporter.setParameter(JRExporterParameter.INPUT_FILE_NAME, excelPrintFileName);
			excelExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, temporaryFile.getAbsolutePath());

			excelExporter.exportReport();
			output = read(temporaryFile);
			break;
		// AMZ-XLS#END
		}

		return output;
	}

	private byte[] read(File temporaryOutput) throws IOException {
		byte[] output = null;

		try (InputStream stream = new FileInputStream(temporaryOutput)) {
			output = new byte[stream.available()];

			stream.read(output);
		}

		return output;
	}
}
