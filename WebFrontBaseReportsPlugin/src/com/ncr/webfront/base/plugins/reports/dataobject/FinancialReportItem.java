package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.List;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;

public class FinancialReportItem extends DataObject {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<elemento>\n");
		sb.append("<label>").append(getStrLabel()).append("</label>\n");
		sb.append("<tipo>").append(getStrTipo()).append("</tipo>\n");
		sb.append("<nome>").append(getStrNome()).append("</nome>\n");

		String strFinancialTotalId = "" + getIntFinancialTotalId();
		if (getIntFinancialTotalId() > 0) {
			strFinancialTotalId = strFinancialTotalId.substring(2);
		}
		sb.append("<totalid>").append(strFinancialTotalId).append("</totalid>\n");

		String strTempPerc = GeneralPurpose.formatNumber(Double.toString(getDblPerc()), "##0.##");
		sb.append("<perc>").append(strTempPerc).append("</perc>\n");

		if ((getFlags().get(ReportsConstants.FLAGINFO_PRINTTRANSACTION)).booleanValue()) {
			if ((getFlags().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getIntValore1() == 0)) {
				sb.append("<clienti>0</clienti>\n");
			} else {
				sb.append("<clienti>").append(getIntValore1()).append("</clienti>\n");
			}
		} else {
			sb.append("<clienti>0</clienti>\n");
		}

		if ((getFlags().get(ReportsConstants.FLAGINFO_PRINTITEM)).booleanValue()) {
			if ((getFlags().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getIntValore2() == 0)) {
				sb.append("<artic>0</artic>\n");
			} else {
				sb.append("<artic>").append(getIntValore2()).append("</artic>\n");
			}
		} else {
			sb.append("<artic>0</artic>\n");
		}

		if ((getFlags().get(ReportsConstants.FLAGINFO_PRINTAMOUNT)).booleanValue()) {
			if ((getFlags().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getDblValore3() == 0)) {
				sb.append("<importo>.00</importo>\n");
			} else if (getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)) {
				String strTemp = GeneralPurpose.formatNumber(Double.toString(getDblValore3()), "#####0");
				sb.append("<importo>").append(GeneralPurpose.secondToTimetable(new Integer(strTemp).intValue()))
						.append("</importo>\n");
			} else {
				sb.append("<importo>")
						.append(GeneralPurpose.formatNumber(Double.toString(getDblValore3()), "#,###.00"))
						.append("</importo>\n");
			}
		} else {
			sb.append("<importo>.00</importo>\n");
		}
		sb.append("</elemento>\n");

		return sb.toString();
	}

	public String getStrLabel() {
		return strLabel;
	}

	public void setStrLabel(String strLabel) {
		this.strLabel = strLabel;
	}


	public String getStrLabelClienti() {
		return strLabelClienti;
	}

	public void setStrLabelClienti(String strLabelClienti) {
		this.strLabelClienti = strLabelClienti;
	}

	public String getStrLabelArticoli() {
		return strLabelArticoli;
	}

	public void setStrLabelArticoli(String strLabelArticoli) {
		this.strLabelArticoli = strLabelArticoli;
	}

	public String getStrLabelImporto() {
		return strLabelImporto;
	}

	public void setStrLabelImporto(String strLabelImporto) {
		this.strLabelImporto = strLabelImporto;
	}

	public String getStrTipoRigaGruppo() {
		return strTipoRigaGruppo;
	}

	public void setStrTipoRigaGruppo(String strTipoRigaGruppoIn) {
		this.strTipoRigaGruppo = strTipoRigaGruppoIn;
	}

	public String getStrNumProg() {
		return strNumProg;
	}

	public void setStrNumProg(String strNumProg) {
		this.strNumProg = strNumProg;
	}

	public String getStrTipo() {
		return strTipo;
	}

	public void setStrTipo(String strTipo) {
		this.strTipo = strTipo;
	}

	public String getStrNome() {
		return strNome;
	}

	public void setStrNome(String strNome) {
		this.strNome = strNome;
	}

	public int getIntValore1() {
		return intValore1;
	}

	public void setIntValore1(int intValore1) {
		this.intValore1 = intValore1;
	}

	public int getIntValore2() {
		return intValore2;
	}

	public void setIntValore2(int intValore2) {
		this.intValore2 = intValore2;
	}

	public double getDblValore3() {
		return dblValore3;
	}

	public void setDblValore3(double dblValore3) {
		this.dblValore3 = dblValore3;
	}

	public String getStrCassa() {
		return strCassa;
	}

	public void setStrCassa(String strCassa) {
		this.strCassa = strCassa;
	}

	public String getStrCassiere() {
		return strCassiere;
	}

	public void setStrCassiere(String strCassiere) {
		this.strCassiere = strCassiere;
	}

	public int getIntFinancialTotalId() {
		return intFinancialTotalId;
	}

	public void setIntFinancialTotalId(int intFinancialTotalId) {
		this.intFinancialTotalId = intFinancialTotalId;
	}

	public List<Boolean> getFlags() {
		return flags;
	}

	public void setFlags(List<Boolean> vctFlags) {
		this.flags = vctFlags;
	}

	public String getStrGroupLabel() {
		return strGroupLabel;
	}

	public void setStrGroupLabel(String strGroupLabel) {
		this.strGroupLabel = strGroupLabel;
	}

	public double getDblPerc() {
		return dblPerc;
	}

	public void setDblPerc(double dblPerc) {
		this.dblPerc = dblPerc;
	}

	private String strLabel; // subreport key field
	private String strLabelClienti;
	private String strLabelArticoli;
	private String strLabelImporto;
	private String strTipoRigaGruppo = "";
	private String strNumProg;
	private String strTipo;
	private String strNome;
	private String strCassa;
	private String strCassiere;
	private int intFinancialTotalId;
	private List<Boolean> flags;
	private int intValore1 = 0;
	private int intValore2 = 0;
	private String strGroupLabel = "";
	private double dblValore3;
	private double dblPerc;
}
