package com.ncr.webfront.base.plugins.reports.manager;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.ReportsMappingProperties;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReport;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReportGroup;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class FinancialReportManager extends DetailedReportManager {
	public FinancialReportManager(ReportOptions reportOptions) throws Exception {
		super(reportOptions);
	}

	public String getJasperFileName() {
		return ReportsMappingProperties.getInstance().getProperty(ReportsMappingProperties.financialJasperPath, true);
	}

	public InputStream innerGeneration() throws SQLException {
		FinancialReport report = new FinancialReport();

		report.setRealFinanziario(isRealFinanziario());

		String title = "FINANCIAL REPORT (23)";
		logger.debug("getReportOptions().getTitle() =" + getReportOptions().getTitle());
		if ((getReportOptions().getTitle() != null) && (!getReportOptions().getTitle().isEmpty())) {
			logger.debug("Replacing title with the title from options...");
			title = getReportOptions().getTitle();
		}
		logger.debug("title = " + title);

		String subTitle = "";
		logger.debug("getReportOptions().getSubTitle() =" + getReportOptions().getSubTitle());
		if ((getReportOptions().getSubTitle() != null) && (!getReportOptions().getSubTitle().isEmpty())) {
			logger.debug("Replacing subTitle with the subTitle from options...");
			subTitle = getReportOptions().getSubTitle();
		} else {
			if (!getPosTerminals().equals("ALL")) {
				subTitle = "TERMINALS: " + getPosTerminals();
			} else {
				subTitle = "TERMINALS: ALL";
			}
		}
		logger.debug("subTitle = " + subTitle);

		report.setHeader(createHeader(title, "pdf", subTitle, ""));
		//report.setStrDataReport(getReportDate());
		report.setStrDataReport(getFormattedReportDate());
		report.setRowSet(createRowset(getReportDate(), getPosTerminals(), null));
		report.hourlyProductivity();
		report.setGruppiRiepilogo(createSummary(getReportDate(), getPosTerminals(), null, true));
		report.processMeanValues(getReportDate());

		List<FinancialReportGroup> singleElement = report.getRowSet();

		report.setRowSet(updateRowset(singleElement));

		return processXmlOutput(report);
	}

	private List<FinancialReportGroup> createRowset(String date, String terminal, String checker) throws SQLException {
		List<FinancialReportGroup> vctRowset = new ArrayList<>();

		// gruppo totale giornaliero
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_TOTGIORN_INF, ReportsConstants.FINTOTID_TOTGIORN_SUP,
				ReportsConstants.FIN_TOTGIORN_TOTLORDE_CODICE, ReportsConstants.FIN_TOTGIORN_LORDE_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date,
				terminal, checker, true));

		// gruppo totale lordo
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_TOTLORDE_INF, ReportsConstants.FINTOTID_TOTLORDE_SUP,
				ReportsConstants.FIN_TOTGIORN_TOTLORDE_CODICE, ReportsConstants.FIN_TOTGIORN_LORDE_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date,
				terminal, checker, true));

		// gruppo SCONTI ARTICOLI
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_SCONTIART_INF, ReportsConstants.FINTOTID_SCONTIART_SUP,
				ReportsConstants.FIN_SCONTIART_SCONTIAUTO_CODICE, ReportsConstants.FIN_SCONTIART_AUTO_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date,
				terminal, checker, true));

		// gruppo SCONTI AUTO
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_SCONTIAUTO_INF, ReportsConstants.FINTOTID_SCONTIAUTO_SUP,
				ReportsConstants.FIN_SCONTIART_SCONTIAUTO_CODICE, ReportsConstants.FIN_SCONTIART_AUTO_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date,
				terminal, checker, true));

		// gruppo IMPOSTA IVA
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_ALIQUOTE_INF, ReportsConstants.FINTOTID_ALIQUOTE_SUP,
				ReportsConstants.FIN_ALIQUOTE_LORDIIVA_CODICE, ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date,
				terminal, checker, true));

		// gruppo LORDIIVA
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_LORDIIVA_INF, ReportsConstants.FINTOTID_LORDIIVA_SUP,
				ReportsConstants.FIN_ALIQUOTE_LORDIIVA_CODICE, ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date,
				terminal, checker, true));

		// gruppo VENDMERC
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_VENDMERC_INF, ReportsConstants.FINTOTID_VENDMERC_SUP,
				ReportsConstants.FIN_VENDMERC_STAT_CODICE, ReportsConstants.FIN_VENDMERC_STAT_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date, terminal,
				checker, true));

		// gruppo STATISTICHE
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_STAT_INF, ReportsConstants.FINTOTID_STAT_SUP, ReportsConstants.FIN_VENDMERC_STAT_CODICE,
				ReportsConstants.FIN_VENDMERC_STAT_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date, terminal, checker, true));

		// gruppo PRODUTTIVITA'
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_PROD_INF, ReportsConstants.FINTOTID_PROD_SUP, ReportsConstants.FIN_PROD_MEDI_CODICE,
				ReportsConstants.FIN_PROD_MEDI_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date, terminal, checker, false));

		// gruppo VALORI MEDI
		FinancialReportGroup objGruppoValoriMedi = new FinancialReportGroup();
		objGruppoValoriMedi.setStrCodice(ReportsConstants.FIN_PROD_MEDI_CODICE);
		objGruppoValoriMedi.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objGruppoValoriMedi.setStrTipoRigaGruppo(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER);
		vctRowset.add(objGruppoValoriMedi);

		// gruppo TENDER MEDIA
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_TENDER_INF, ReportsConstants.FINTOTID_TENDER_SUP,
				ReportsConstants.FIN_TENDER_PUNTI_CODICE, ReportsConstants.FIN_TENDER_PUNTI_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date, terminal,
				checker, true));

		// gruppo PUNTI
		vctRowset.add(this.createGroupJasper(ReportsConstants.FINTOTID_PUNTI_INF, ReportsConstants.FINTOTID_PUNTI_SUP,
				ReportsConstants.FIN_TENDER_PUNTI_CODICE, ReportsConstants.FIN_TENDER_PUNTI_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date, terminal,
				checker, true));

		return vctRowset;
	}

	private Logger logger = WebFrontLogger.getLogger(FinancialReportManager.class);

}
