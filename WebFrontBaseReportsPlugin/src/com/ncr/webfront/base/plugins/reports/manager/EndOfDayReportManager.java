package com.ncr.webfront.base.plugins.reports.manager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ncr.webfront.base.plugins.reports.EodReportsMappingProperties;
import com.ncr.webfront.base.plugins.reports.ReportSaver;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.reports.EndOfDayReportProperties;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class EndOfDayReportManager {

	private Logger logger = WebFrontLogger.getLogger(EndOfDayReportManager.class);
	public static final String eodReportPropertiesPrefix = "eod.report";
	public static final int maxEodReportPropertiesEntries = 100;
	public static final int terminalCodeDigit = 3;
	public static final String eodReportPropertiesJsonFileName = "eod_reports_properties.json";

	private List<String> registerList;
	private List<EndOfDayReportProperties> eodReportPropertiesList;

	public EndOfDayReportManager(List<String> registerList,List<PosDepartment> listPosDepartment) {
		eodReportPropertiesList = new ArrayList<EndOfDayReportProperties>();
		this.registerList = registerList;
		readEodReportProperties();
		generateEodReports(listPosDepartment);
		writeEodReportPropertiesToJson();
	}

	public void generateEodReports(List<PosDepartment> listPosDepartment) {
		ReportSaver reportSaver = new ReportSaver();
		for (EndOfDayReportProperties endOfDayReportProperties : eodReportPropertiesList) {
			ReportOptions reportOptions = new ReportOptions();
			reportOptions.setFormat(endOfDayReportProperties.getFormat());
			reportOptions.setPosTerminals(endOfDayReportProperties.getRegisters());
			logger.debug("ALBTEMP - reportOptions.setPosTerminals -> " + endOfDayReportProperties.getRegisters());
			reportOptions.setDate(new Date());
			reportOptions.setTitle(endOfDayReportProperties.getTitle());
			reportOptions.setSubTitle(endOfDayReportProperties.getSubTitle());
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			reportOptions.setAdditionalInfo(gson.toJson(true).toString());
			logger.debug("ALBTEMP - reportOptions.gettPosDepartments size -> " + reportOptions.getPosDepartments().size());
			reportOptions.setPosDepartments(listPosDepartment);
			// financial report
			if (endOfDayReportProperties.getType().equalsIgnoreCase("financial")) {
				try {
					reportSaver.saveReport(new FinancialReportManager(reportOptions), endOfDayReportProperties.getFilename());
				} catch (Exception e) {
					logger.error(e);
				}
			}
			// hourly activity report
			else if (endOfDayReportProperties.getType().equalsIgnoreCase("hourly.activity")) {
				try {
					reportSaver.saveReport(new HourlyActivityReportManager(reportOptions), endOfDayReportProperties.getFilename());
				} catch (Exception e) {
					logger.error(e);
				}
			}
			// department report
			else if (endOfDayReportProperties.getType().equalsIgnoreCase("department")) {
				try {
					reportSaver.saveReport(new DepartmentReportManager(reportOptions), endOfDayReportProperties.getFilename());
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
	}

	private void readEodReportProperties() {
		EodReportsMappingProperties eodReportsMappingProperties = EodReportsMappingProperties.getInstance();

		for (int i = 1; i <= maxEodReportPropertiesEntries; i++) {
			// type, format, filename and title are mandatory field
			String eodReportType = eodReportsMappingProperties

			.getProperty(eodReportPropertiesPrefix + "." + i + ".type");
			if (eodReportType == null || eodReportType.isEmpty()) {
				logger.warn("property " + eodReportPropertiesPrefix + "." + i + ".type" + " is empty. Skipping record with index " + i);
				continue;
			}
			String eodReportFormat = eodReportsMappingProperties.getProperty(eodReportPropertiesPrefix + "." + i + ".format");
			if (eodReportFormat == null || eodReportFormat.isEmpty()) {
				logger.warn("property " + eodReportPropertiesPrefix + "." + i + ".format" + " is empty. Skipping record with index " + i);
			}

			String eodReportFilename = eodReportsMappingProperties.getProperty(eodReportPropertiesPrefix + "." + i + ".filename");
			if (eodReportFilename == null || eodReportFilename.isEmpty()) {
				logger.warn("property " + eodReportPropertiesPrefix + "." + i + ".filename" + " is empty. Skipping record with index " + i);
				continue;
			}

			String eodReportTitle = eodReportsMappingProperties.getProperty(eodReportPropertiesPrefix + "." + i + ".title");
			if (eodReportTitle== null || eodReportTitle.isEmpty()) {
				logger.warn("property " + eodReportPropertiesPrefix + "." + i + ".title" + " is empty. Skipping record with index " + i);
				continue;
			}

			String eodReportSummary = eodReportsMappingProperties.getProperty(eodReportPropertiesPrefix + "." + i + ".summary");
			boolean eodReportSummaryValue;
			if (eodReportSummary == null || eodReportSummary.isEmpty()) {
				eodReportSummaryValue = EodReportsMappingProperties.EOD_REPORT_SUMMARY_DEFAULT_VALUE;
			} else {
				eodReportSummaryValue = Boolean.valueOf(eodReportSummary);
			}

			String eodReportRegisters = eodReportsMappingProperties.getProperty(eodReportPropertiesPrefix + "." + i + ".registers");
			if (eodReportRegisters == null || eodReportRegisters.isEmpty()) {
				eodReportRegisters = EodReportsMappingProperties.EOD_REPORT_REGISTERS_DEFAULT_VALUE;
			}

			List<String> filteredPosTerminalList = filterRegisterList(eodReportRegisters);
			String eodReportSubTitle = eodReportsMappingProperties.getProperty(eodReportPropertiesPrefix + "." + i + ".subtitle");
			if (eodReportSubTitle== null || eodReportSubTitle.isEmpty()) {
				logger.debug("property " + eodReportPropertiesPrefix + "." + i + ".title" + " is empty.");
			}

			if (filteredPosTerminalList.size() > 0) {
				logger.debug("ALBTEMP - filteredPosTerminalList = " + filteredPosTerminalList);
				EndOfDayReportProperties endOfDayReportProperties = new EndOfDayReportProperties(eodReportType, eodReportFormat, eodReportFilename,
						eodReportTitle, eodReportSubTitle ,eodReportSummaryValue, filteredPosTerminalList);

				eodReportPropertiesList.add(endOfDayReportProperties);
			} else {
				logger.warn("property " + eodReportPropertiesPrefix + "." + i + ".registers" + " is empty. Skipping record with index " + i);
			}

		}
	}

	private List<String> filterRegisterList(String eodReportRegistersProperty) {
		List<String> filteredRegisterList = new ArrayList<String>();

		if (eodReportRegistersProperty.equalsIgnoreCase(EodReportsMappingProperties.EOD_REPORT_REGISTERS_DEFAULT_VALUE)) {
			filteredRegisterList.add(EodReportsMappingProperties.EOD_REPORT_REGISTERS_DEFAULT_VALUE);
			return filteredRegisterList;
		}

		String[] eodReportRegistersPropertySplitted = eodReportRegistersProperty.split(";");
		for (String registerProperty : eodReportRegistersPropertySplitted) {
			try {
				String[] fromToRegisters = registerProperty.split("-");
				if (fromToRegisters.length > 2) {
					throw new IllegalArgumentException();
				}

				// one register
				if (fromToRegisters.length == 1) {
					int terminalCode = Integer.parseInt(fromToRegisters[0]);
					String terminalCodeWithPrefix = prefixWithZeros(terminalCodeDigit, terminalCode);
					if (registerList.contains(terminalCodeWithPrefix)) {
						filteredRegisterList.add(terminalCodeWithPrefix);
					}
					continue;
				}

				// from-to registers
				int fromRegister = Integer.parseInt(fromToRegisters[0]);
				int toRegister = Integer.parseInt(fromToRegisters[1]);
				if (fromRegister > toRegister) {
					throw new IllegalArgumentException();
				}
				for (int i = fromRegister; i <= toRegister; i++) {
					String terminalCodeWithPrefix = prefixWithZeros(terminalCodeDigit, i);
					if (registerList.contains(terminalCodeWithPrefix)) {
						filteredRegisterList.add(terminalCodeWithPrefix);
					}
				}

			} catch (IllegalArgumentException e) {
				logger.error(e);
			}
		}
		return filteredRegisterList;
	}

	private void writeEodReportPropertiesToJson() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Type endOfDayReportPropertiesListType = new TypeToken<List<EndOfDayReportProperties>>() {
		}.getType();
		String posOperatorsTerminalCodeMapJsonString = gson.toJson(eodReportPropertiesList, endOfDayReportPropertiesListType);

		try {
			File eodReportPropertiesJsonFile = new File(ReportSaver.reportPathLast + File.separator + eodReportPropertiesJsonFileName);
			FileUtils.write(eodReportPropertiesJsonFile, posOperatorsTerminalCodeMapJsonString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String prefixWithZeros(int numDigit, int number) {
		if (numDigit <= 0 || numDigit >= 8) {
			return "" + number;
		}
		String formatString = "%0" + numDigit + "d";
		return String.format(formatString, number);
	}
}
