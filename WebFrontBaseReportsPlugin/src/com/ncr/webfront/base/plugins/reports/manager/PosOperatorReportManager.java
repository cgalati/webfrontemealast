package com.ncr.webfront.base.plugins.reports.manager;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.ReportsMappingProperties;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReport;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReportGroup;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class PosOperatorReportManager extends DetailedReportManager {
	public PosOperatorReportManager(ReportOptions reportOptions) throws Exception {
		super(reportOptions);
	}

	@Override
	public String getJasperFileName() {
		return ReportsMappingProperties.getInstance().getProperty(ReportsMappingProperties.posOperatorJasperPath, true);
	}

	@Override
	public InputStream innerGeneration() throws SQLException {
		FinancialReport report = new FinancialReport();

		String subTitle = "";
		logger.debug("getReportOptions().getSubTitle() =" + getReportOptions().getSubTitle());
		if ((getReportOptions().getSubTitle() != null) && (!getReportOptions().getSubTitle().isEmpty())) {
			logger.debug("Replacing subTitle with the subTitle from options...");
			subTitle = getReportOptions().getSubTitle();
		} else {
			if (!getPosTerminals().equals("ALL")) {
				subTitle = "TERMINALS: " + getPosTerminals();
			} else {
				subTitle = "TERMINALS: ALL";
			}
		}
		logger.debug("subTitle = " + subTitle);
		
		report.setRealFinanziario(isRealFinanziario());
		report.setHeader(createHeader("CHECKER REPORT (21)", "pdf", subTitle, getPosOperators()));
		report.setStrDataReport(getReportDate());
		report.setRowSet(createRowset(getReportDate(), getPosTerminals(), getPosOperators()));
		report.hourlyProductivity();
		report.setGruppiRiepilogo(createSummary(getReportDate(), getPosTerminals(), getPosOperators(), true));
		report.processMeanValues(getReportDate());

		List<FinancialReportGroup> singleElement = report.getRowSet();

		report.setRowSet(updateRowset(singleElement));
		
		return processXmlOutput(report);
	}

	protected List<FinancialReportGroup> createRowset(String date, String terminal, String checker) throws SQLException {
		FinancialReportGroup group = null;
		List<FinancialReportGroup> rowSet = new ArrayList<>();

		// gruppo totale giornaliero
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_TOTGIORN_INF, ReportsConstants.FINTOTID_TOTGIORN_SUP,
				ReportsConstants.FIN_TOTGIORN_TOTLORDE_CODICE, ReportsConstants.FIN_TOTGIORN_LORDE_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER,
				date, terminal, checker, true));

		// gruppo totale lordo
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_TOTLORDE_INF, ReportsConstants.FINTOTID_TOTLORDE_SUP,
				ReportsConstants.FIN_TOTGIORN_TOTLORDE_CODICE, ReportsConstants.FIN_TOTGIORN_LORDE_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER,
				date, terminal, checker, true));

		// gruppo SCONTI ARTICOLI
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_SCONTIART_INF, ReportsConstants.FINTOTID_SCONTIART_SUP,
				ReportsConstants.FIN_SCONTIART_SCONTIAUTO_CODICE, ReportsConstants.FIN_SCONTIART_AUTO_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER,
				date, terminal, checker, true));

		// gruppo SCONTI AUTO
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_SCONTIAUTO_INF, ReportsConstants.FINTOTID_SCONTIAUTO_SUP,
				ReportsConstants.FIN_SCONTIART_SCONTIAUTO_CODICE, ReportsConstants.FIN_SCONTIART_AUTO_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER,
				date, terminal, checker, true));

		// gruppo IMPOSTA IVA
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_ALIQUOTE_INF, ReportsConstants.FINTOTID_ALIQUOTE_SUP,
				ReportsConstants.FIN_ALIQUOTE_LORDIIVA_CODICE, ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER,
				date, terminal, checker, true));

		// gruppo LORDIIVA
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_LORDIIVA_INF, ReportsConstants.FINTOTID_LORDIIVA_SUP,
				ReportsConstants.FIN_ALIQUOTE_LORDIIVA_CODICE, ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER,
				date, terminal, checker, true));

		// gruppo VENDMERC
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_VENDMERC_INF, ReportsConstants.FINTOTID_VENDMERC_SUP,
				ReportsConstants.FIN_VENDMERC_STAT_CODICE, ReportsConstants.FIN_VENDMERC_STAT_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date,
				terminal, checker, true));

		// gruppo STATISTICHE
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_STAT_INF, ReportsConstants.FINTOTID_STAT_SUP,
				ReportsConstants.FIN_VENDMERC_STAT_CODICE, ReportsConstants.FIN_VENDMERC_STAT_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date,
				terminal, checker, true));

		// gruppo PRODUZIONE
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_PROD_INF, ReportsConstants.FINTOTID_PROD_SUP,
				ReportsConstants.FIN_PROD_MEDI_CODICE, ReportsConstants.FIN_PROD_MEDI_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date,
				terminal, checker, false));

		// gruppo VALORI MEDI
		FinancialReportGroup objGruppoValoriMedi = new FinancialReportGroup();
		objGruppoValoriMedi.setStrCodice(ReportsConstants.FIN_PROD_MEDI_CODICE);
		objGruppoValoriMedi.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objGruppoValoriMedi.setStrTipoRigaGruppo(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER);
		rowSet.add(objGruppoValoriMedi);

		// gruppo VALORI MEDI, lo creo empty, lo valorizza l'objReport
		//group = new FinancialReportGroup();
		//group.setStrCodice(ReportsConstants.FIN_PROD_MEDI_CODICE);
		//group.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		//group.setStrTipoRigaGruppo(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER);
		//group.setElements(new ArrayList<>());
		//rowSet.add(group);

		// gruppo TENDER MEDIA
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_TENDER_INF, ReportsConstants.FINTOTID_TENDER_SUP,
				ReportsConstants.FIN_TENDER_PUNTI_CODICE, ReportsConstants.FIN_TENDER_PUNTI_LABEL, ReportsConstants.TIPOGRUPPO_RIGASX_JASPER, date,
				terminal, checker, true));

		// gruppo PUNTI
		rowSet.add(this.createGroupJasper(ReportsConstants.FINTOTID_PUNTI_INF, ReportsConstants.FINTOTID_PUNTI_SUP,
				ReportsConstants.FIN_TENDER_PUNTI_CODICE, ReportsConstants.FIN_TENDER_PUNTI_LABEL, ReportsConstants.TIPOGRUPPO_RIGADX_JASPER, date,
				terminal, checker, true));

		return rowSet;
	}

	private Logger logger = WebFrontLogger.getLogger(PosOperatorReportManager.class);
}
