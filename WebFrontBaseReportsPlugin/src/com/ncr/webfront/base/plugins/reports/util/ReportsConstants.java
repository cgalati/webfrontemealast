package com.ncr.webfront.base.plugins.reports.util;

public interface ReportsConstants {
	public static final String NCR_INFLIST_NULL = " ";
	public static final String NCR_INFLIST_SEPARATOR = "\t";
	public static final String NCR_JSP_NUMBER_SEPARATOR = ".";

	public static final String NCR_OPTIONS_DISPLAY_DELIMITATOR = "&nbsp;";

	public static final String REPORT_DATE_FORMAT_BATCH_PARAMETER = "yyyyMMdd";
	public static final String REPORT_DATE_FORMAT_SHORT = "ddMMyy";
	public static final String REPORT_DATE_FORMAT_SHORT_ENG = "yyMMdd";
	public static final String REPORT_DATE_FORMAT_MANAGER = "yyyy-mm-dd";

	public static final String FINE_GIORNO = "FINE_GIORNO";
	public static final String DAILY_TABELLA_CASSIERE = "IDC_DAILYFIN";
	public static final String DAILY_TABELLA_FINANZIARIO = "IDC_DAILYFIN";
	public static final String DAILY_TABELLA_MERCEOLOGICO = "IDC_DAILYACT";

	public static final String RIMAPP_TIPO_CASSA_NORMALI = "'ON'";
	public static final String RIMAPP_TIPO_CASSA_OFFLINE = "'OFF'";
	public static final int RIMAPP_POSITION_IDASAR = 0;
	public static final int RIMAPP_POSITION_FLAGINFO = 1;
	public static final int RIMAPP_POSITION_ENTE = 2;
	public static final String RIMAPP_TOT_NO_ENTE = "0";
	public static final int RIMAPP_FLAGINFO_RPT21 = 0;
	public static final int RIMAPP_FLAGINFO_RPT23 = 1;
	public static final int RIMAPP_FLAGINFO_ARTICOLI = 2;
	public static final int RIMAPP_FLAGINFO_CLIENTI = 3;

	public static final String STREAM_HTML = "html";
	public static final String STREAM_PDF = "pdf";
	public static final String STREAM_FLAT = "flat";
	public static final String STREAM_TXT = "txt";
	public static final String STREAM_EXCEL = "excel";

	public static final String TIPOREPORT_ATTIVITAORARIA = "report_attivita_oraria";
	public static final String TIPOREPORT_CASSIERE = "report_cassiere";
	public static final String TIPOREPORT_FINANZIARIO = "report_finanziario";
	public static final String TIPOREPORT_MERCEOLOGICO = "report_merceologico";

	public static final String LIVELLO_STORICOARTICOLI_BATCH_NEGOZIO = "0";
	public static final String LIVELLO_STORICOARTICOLI_BATCH_REPARTO = "1";
	public static final String LIVELLO_STORICOARTICOLI_BATCH_SETTORE = "2";
	public static final String LIVELLO_STORICOARTICOLI_BATCH_FAMIGLIA = "3";
	public static final String LIVELLO_STORICOARTICOLI_BATCH_SOTTOFAMIGLIA = "4";
	public static final String LIVELLO_STORICOARTICOLI_BATCH_ARTICOLO = "5";
	public static final String DESC_STORICOARTICOLI_BATCH_NESSUNARTICOLO = "NESSUN ARTICOLO";

	public static final String TIPOREPORT_STAT_TRANSAZIONI_EXCEL = "stat_transazioni_excel";

	public static final String FOGLIO_STAT_TRANSAZIONI_CLIENTI = "STAT_CLIENTI";
	public static final String FOGLIO_STAT_TRANSAZIONI_FATTURATO = "STAT_FATTURATO";
	public static final String FOGLIO_STAT_TRANSAZIONI_ARTICOLI = "STAT_ARTICOLI";

	public static final String FOGLIO_STAT_TRANSAZIONI_SCONTRMEDIO = "STAT_SCONTRINO_MEDIO";
	public static final String FOGLIO_STAT_TRANSAZIONI_ARTICOLIMEDIO = "STAT_ARTICOLI_MEDIO";

	public static final String FOGLIO_STAT_TRANSAZIONI_CLIENTI_ANNUALE = "STAT_CLIENTI_ANNUALE";
	public static final String FOGLIO_STAT_TRANSAZIONI_FATTURATO_ANNUALE = "STAT_FATTURATO_ANNUALE";
	public static final String FOGLIO_STAT_TRANSAZIONI_ARTICOLI_ANNUALE = "STAT_ARTICOLI_ANNUALE";

	public static final String FOGLIO_STAT_TRANSAZIONI_SCONTRMEDIO_ANNUALE = "STAT_SCONTRINO_MEDIO_ANNUALE";
	public static final String FOGLIO_STAT_TRANSAZIONI_ARTICOLIMEDIO_ANNUALE = "STAT_ARTICOLI_MEDIO_ANNUALE";

	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_CLIENTI = 0;
	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_FATTURATO = 1;
	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_ARTICOLI = 2;

	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_SCONTRMEDIO = 3;
	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_ARTICOLIMEDIO = 4;

	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_CLIENTI_ANNUALE = 0;
	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_FATTURATO_ANNUALE = 1;
	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_ARTICOLI_ANNUALE = 2;

	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_SCONTRMEDIO_ANNUALE = 3;
	public static final int NUM_FOGLIO_STAT_TRANSAZIONI_ARTICOLIMEDIO_ANNUALE = 4;

	public static final String FILE_TRANSAZIONI = "TRANSAZIONI";
	public static final String FILE_TRANSAZIONI_TOT = "TRANSAZIONI_TOT";

	public static final String FORMAT_CELL_STRING = "STRING";
	public static final String FORMAT_CELL_NUMBER_QTA = "NUMBER_QTA";
	public static final String FORMAT_CELL_NUMBER_AMT = "NUMBER_AMT";

	public static final String SCANDPT_OUTFILE_FLAT = "SCAN_DPT";
	public static final String SCANDPT_XMLFILE = "SCAN_DPT.xml";
	public static final String TRA39_OUTFILE_FLAT = "TRA39";
	public static final String TRA39_XMLFILE = "TRA39.xml";
	public static final String RPT39_OUTFILE_FLAT = "RPT39";
	public static final String RPT39_XMLFILE = "RPT39.xml";
	public static final String FATTURE_OUTFILE_FLAT = "FATTURE";
	public static final String FATTURE_XMLFILE = "FATTURE.xml";
	public static final String ANACASSIERE_OUTFILE_FLAT = "CKRTBL";
	public static final String ANACASSIERE_XMLFILE = "CKRTBL.xml";
	public static final String CLIENTIFEDELI_OUTFILE_FLAT = "SH_DC_ASCII";
	public static final String CLIENTIFEDELI_XMLFILE = "SH_DC_ASCII.xml";
	public static final String TESSEREOFF_OUTFILE_FLAT = "SH_TESSERE_OFF";
	public static final String TESSEREOFF_XMLFILE = "SH_TESSERE_OFF.xml";
	public static final String SALDOPUNTI_OUTFILE_FLAT = "SH_SALDO_PUNTI";
	public static final String SALDOPUNTI_XMLFILE = "SH_SALDO_PUNTI.xml";
	public static final String ARTPROMO_OUTFILE_FLAT = "ART_PROMO";
	public static final String ARTPROMO_XMLFILE = "ART_PROMO.xml";
	public static final String BOLLE_OUTFILE_FLAT = "BOLLE_SEDE";
	public static final String BOLLE_XMLFILE = "BOLLE_SEDE.xml";
	public static final String CLIENTI_OUTFILE_FLAT = "CLIENTI_SEDE";
	public static final String CLIENTI_XMLFILE = "CLIENTI_SEDE.xml";
	public static final String VOLANTINO_OUTFILE_FLAT = "VELAB_DC";
	public static final String VOLANTINO_XMLFILE = "VELAB_DC.xml";
	public static final String CAMBIPREZZO_OUTFILE_FLAT = "CAMBIO_PREZZO";
	public static final String CAMBIPREZZO_XMLFILE = "CAMBIO_PREZZO.xml";
	public static final String TRANSAZIONI_OUTFILE_FLAT = "TRANSAZIONI";
	public static final String PUNTIARTICOLO_OUTFILE_FLAT = "SH_PUNTI_ARTICOLO";
	public static final String PUNTIARTICOLO_XMLFILE = "SH_PUNTI_ARTICOLO.xml";

	public static final String LOG_OUTFILE_FLAT = "SH_LOGFILE";

	public static final String PAGE_SIZE = "page_size";
	public static final String ORIENTATION = "orientation";
	public static final String MARGIN_TOP = "margin_top";
	public static final String MARGIN_BOTTOM = "margin_bottom";
	public static final String MARGIN_RIGHT = "margin_right";
	public static final String MARGIN_LEFT = "margin_left";

	public static final String DESCRIZIONE_RAGGRUPPAMENTO = "GROUPING DESCRIPTION";
	public static final String DESCRIZIONE_NETTE = "Net Sales";
	public static final String DESCRIZIONE_SCONTI = "Discounts";

	public static final String COMMON_XSL_FLAT = "general_flat.xsl";
	public static final String COMMON_XSL_FLAT_LIST = "general_flat_list.xsl";

	public static final String ATTIVITAORARIA_XMLFILE = "report_attivita_oraria.xml";
	public static final String ATTIVITAORARIA_XMLFILE_LIST = "report_attivita_oraria_list.xml";
	public static final String ATTIVITAORARIA_XSL_HTML = "report_attivita_oraria.xsl";
	public static final String ATTIVITAORARIA_XSL_HTML_LIST = "report_attivita_oraria_list.xsl";
	public static final String ATTIVITAORARIA_OUTFILE = "report_attivita_oraria";
	public static final String ATTIVITAORARIA_OUTFILE_LIST = "report_attivita_oraria";
	public static final String ATTIVITAORARIA_XSL_PDF = "report_attivita_orariafo.xsl";
	public static final String ATTIVITAORARIA_XSL_PDF_LIST = "report_attivita_orariafo_list.xsl";
	public static final String ATTIVITAORARIA_XSL_TXT = "report_attivita_orariatxt.xsl";
	public static final String ATTIVITAORARIA_XSL_TXT_LIST = "report_attivita_orariatxt_list.xsl";

	public static final String MERCEOLOGICO_TOTALTYPE_LORDO = "0";
	public static final String MERCEOLOGICO_TOTALTYPE_SCONTO = "1";
	public static final String MERCEOLOGICO_TOTALTYPE_RESO = "2";
	public static final String MERCEOLOGICO_TOTALTYPE_STORNO = "3";
	public static final String MERCEOLOGICO_TOTALTYPE_ANNULLO = "4";
	public static final String MERCEOLOGICO_XMLFILE = "report_merceologico.xml";
	public static final String MERCEOLOGICO_XMLFILE_LIST = "report_merceologico_list.xml";
	public static final String MERCEOLOGICO_XSL_HTML = "report_merceologico.xsl";
	public static final String MERCEOLOGICO_XSL_HTML_LIST = "report_merceologico_list.xsl";
	public static final String MERCEOLOGICO_XSL_PDF = "report_merceologicofo.xsl";
	public static final String MERCEOLOGICO_XSL_FLAT = COMMON_XSL_FLAT;
	public static final String MERCEOLOGICO_XSL_FLAT_LIST = COMMON_XSL_FLAT_LIST;
	public static final String MERCEOLOGICO_XSL_TXT = "report_merceologicotxt.xsl";
	public static final String MERCEOLOGICO_XSL_TXT_LIST = "report_merceologicotxt_list.xsl";
	public static final String MERCEOLOGICO_OUTFILE = "report_merceologico";
	public static final String MERCEOLOGICO_OUTFILE_LIST = "report_merceologico";
	public static final String MERCEOLOGICO_OUTFILE_FLAT = "RPT26";

	public static final String MERCEOLOGICO_LIVELLO_NEGOZIO = "liv0";
	public static final String MERCEOLOGICO_LIVELLO_TASTO = "liv1";
	public static final String MERCEOLOGICO_LIVELLO_REPARTO = "liv2";
	public static final String MERCEOLOGICO_CARATTERE_RAGGR = "*";
	public static final int MERCEOLOGICO_DIMENSIONE_LIVELLI = 4;
	public static final int MERCEOLOGICO_TASTO_NUM_CARAT_RAGGR = 1;
	public static final int MERCEOLOGICO_REPARTO_NUM_CARAT_RAGGR = 0;

	public static final String MERCEOLOGICO_MAJOR_FOOD_1 = "*001";
	public static final String MERCEOLOGICO_MAJOR_FOOD_2 = "*002";
	public static final String MERCEOLOGICO_MAJOR_FOOD_3 = "*003";

	public static final String MERCEOLOGICO_MAJOR_NOFOOD_4 = "*004";
	public static final String MERCEOLOGICO_MAJOR_NOFOOD_5 = "*005";
	public static final String MERCEOLOGICO_MAJOR_NOFOOD_6 = "*006";
	public static final String MERCEOLOGICO_MAJOR_NOFOOD_7 = "*007";
	public static final String MERCEOLOGICO_MAJOR_NOFOOD_9 = "*009";

	public static final String FINANZIARIO_XMLFILE = "report_finanziario.xml";
	public static final String FINANZIARIO_XMLFILE_LIST = "report_finanziario_list.xml";
	public static final String FINANZIARIO_XSL_HTML = "report_finanziario.xsl";
	public static final String FINANZIARIO_XSL_HTML_LIST = "report_finanziario_list.xsl";
	public static final String FINANZIARIO_ENTE_XSL_HTML = "report_finanziario_ente.xsl";
	public static final String FINANZIARIO_ENTE_XSL_HTML_LIST = "report_finanziario_ente_list.xsl";
	public static final String FINANZIARIO_OUTFILE = "report_finanziario";
	public static final String FINANZIARIO_OUTFILE_LIST = "report_finanziario";
	public static final String FINANZIARIO_XSL_PDF = "report_finanziariofo.xsl";
	public static final String FINANZIARIO_XSL_TXT = "report_finanziariotxt.xsl";
	public static final String FINANZIARIO_XSL_FLAT = COMMON_XSL_FLAT;
	public static final String FINANZIARIO_XSL_FLAT_LIST = COMMON_XSL_FLAT_LIST;
	public static final String FINANZIARIO_XSL_TXT_LIST = "report_finanziariotxt_list.xsl";
	public static final String FINANZIARIO_OUTFILE_FLAT = "RPT23";

	public static final String CASSIERE_XMLFILE = "report_cassiere.xml";
	public static final String CASSIERE_XMLFILE_LIST = "report_cassiere_list.xml";
	public static final String CASSIERE_XSL_HTML = "report_cassiere.xsl";
	public static final String CASSIERE_XSL_HTML_LIST = "report_cassiere_list.xsl";
	public static final String CASSIERE_OUTFILE = "report_cassiere";
	public static final String CASSIERE_OUTFILE_LIST = "report_cassiere";
	public static final String CASSIERE_XSL_PDF = "report_cassierefo.xsl";
	public static final String CASSIERE_XSL_TXT = "report_cassieretxt.xsl";
	public static final String CASSIERE_XSL_FLAT = COMMON_XSL_FLAT;
	public static final String CASSIERE_XSL_FLAT_LIST = COMMON_XSL_FLAT_LIST;
	public static final String CASSIERE_XSL_TXT_LIST = "report_cassieretxt_list.xsl";
	public static final String CASSIERE_OUTFILE_FLAT = "RPT21";
	public static final String CASSIERE_LIST_ALL = "Tutti";

	public static final String CASSIERE_ART_REP_XMLFILE = "report_cassiere_art_rep.xml";
	public static final String CASSIERE_ART_REP_XSL_HTML = "report_cassiere_art_rep.xsl";
	public static final String CASSIERE_ART_REP_OUTFILE = "report_cassiere_art_rep";
	public static final String CASSIERE_ART_REP_XSL_PDF = "report_cassiere_art_repfo.xsl";
	public static final String CASSIERE_ART_REP_XSL_TXT = "report_cassiere_art_reptxt.xsl";
	public static final String CASSIERE_ART_REP_XSL_FLAT = "report_cassiere_art_rep_flat.xsl";
	public static final String CASSIERE_ART_REP_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_MED_ORA_SEM_XMLFILE = "report_produttivita_med_ora_sem.xml";
	public static final String PRODUTTIVITA_MED_ORA_SEM_XSL_HTML = "report_produttivita_med_ora_sem.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SEM_4_1OUTFILE = "report_produttivita_med_ora_sem_4_1";
	public static final String PRODUTTIVITA_MED_ORA_SEM_4_2OUTFILE = "report_produttivita_med_ora_sem_4_2";
	public static final String PRODUTTIVITA_MED_ORA_SEM_4_3OUTFILE = "report_produttivita_med_ora_sem_4_3";
	public static final String PRODUTTIVITA_MED_ORA_SEM_XSL_PDF = "report_produttivita_med_ora_semfo.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SEM_XSL_TXT = "report_produttivita_med_ora_semtxt.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SEM_XSL_FLAT = "report_produttivita_med_ora_semflat.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SEM_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_MED_ORA_SCAS_XMLFILE = "report_produttivita_med_ora_scas.xml";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_XSL_HTML = "report_produttivita_med_ora_scas.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_OUTFILE = "report_produttivita_med_ora_scas";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_XSL_PDF = "report_produttivita_med_ora_scasfo.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_XSL_TXT = "report_produttivita_med_ora_scastxt.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_XSL_FLAT = "report_produttivita_med_ora_scasflat.xsl";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_CASSA_XMLFILE = "report_produttivita_cassa.xml";
	public static final String PRODUTTIVITA_CASSA_XSL_HTML = "report_produttivita_cassa.xsl";
	public static final String PRODUTTIVITA_CASSA_OUTFILE = "report_produttivita_cassa";
	public static final String PRODUTTIVITA_CASSA_XSL_PDF = "report_produttivita_cassafo.xsl";
	public static final String PRODUTTIVITA_CASSA_XSL_TXT = "report_produttivita_cassatxt.xsl";
	public static final String PRODUTTIVITA_CASSA_XSL_FLAT = "report_produttivita_cassaflat.xsl";
	public static final String PRODUTTIVITA_CASSA_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_MED_ORA_AS_XMLFILE = "report_produttivita_med_as.xml";
	public static final String PRODUTTIVITA_MED_ORA_AS_XSL_HTML = "report_produttivita_med_as.xsl";
	public static final String PRODUTTIVITA_MED_ORA_AS_OUTFILE = "report_produttivita_med_as";
	public static final String PRODUTTIVITA_MED_ORA_AS_XSL_PDF = "report_produttivita_med_asfo.xsl";
	public static final String PRODUTTIVITA_MED_ORA_AS_XSL_TXT = "report_produttivita_med_astxt.xsl";
	public static final String PRODUTTIVITA_MED_ORA_AS_XSL_FLAT = "report_produttivita_med_asflat.xsl";
	public static final String PRODUTTIVITA_MED_ORA_AS_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_MED_TTS_XMLFILE = "report_produttivita_med_tts.xml";
	public static final String PRODUTTIVITA_MED_TTS_XSL_HTML = "report_produttivita_med_tts.xsl";
	public static final String PRODUTTIVITA_MED_TTS_OUTFILE = "report_produttivita_med_tts";
	public static final String PRODUTTIVITA_MED_TTS_XSL_PDF = "report_produttivita_med_ttsfo.xsl";
	public static final String PRODUTTIVITA_MED_TTS_XSL_TXT = "report_produttivita_med_ttstxt.xsl";
	public static final String PRODUTTIVITA_MED_TTS_XSL_FLAT = "report_produttivita_med_ttsflat.xsl";
	public static final String PRODUTTIVITA_MED_TTS_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_TOC_XMLFILE = "report_produttivita_toc.xml";
	public static final String PRODUTTIVITA_TOC_XSL_HTML = "report_produttivita_toc.xsl";
	public static final String PRODUTTIVITA_TOC_OUTFILE = "report_produttivita_toc";
	public static final String PRODUTTIVITA_TOC_XSL_PDF = "report_produttivita_tocfo.xsl";
	public static final String PRODUTTIVITA_TOC_XSL_TXT = "report_produttivita_toctxt.xsl";
	public static final String PRODUTTIVITA_TOC_XSL_FLAT = "report_produttivita_tocflat.xsl";
	public static final String PRODUTTIVITA_TOC_OUTFILE_FLAT = "RPTXX";

	public static final String PRODUTTIVITA_RIGA_NORMALE = "N";
	public static final String PRODUTTIVITA_RIGA_SOMMA = "S";
	public static final String PRODUTTIVITA_RIGA_MEDIA = "M";
	public static final String PRODUTTIVITA_MED_ORA_SEM_VENDITE_A_TASTO_PLUNUM = "9999999999999";
	public static final int PRODUTTIVITA_MED_ORA_SEM_VENDITALORDA_TOTALTYPE = 0;
	public static final int PRODUTTIVITA_MED_ORA_SEM_SCONTO_TOTALTYPE = 1;
	public static final int PRODUTTIVITA_MED_ORA_SEM_RESO_TOTALTYPE = 2;
	public static final int PRODUTTIVITA_MED_ORA_SEM_STORNO_TOTALTYPE = 3;
	public static final int PRODUTTIVITA_MED_ORA_SEM_ANNULLO_TOTALTYPE = 4;
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TTOTALE = "901";
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TPAUSA = "902";
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TLOAN = "903";
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TSERVIZIO = "904";
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TSCANNER = "905";
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TPAGAM = "906";
	public static final String PRODUTTIVITA_FINANCIALTOTALID_TATTESA = "909";

	public static final int PRODUTTIVITA_ORAINIZIO = 9;
	public static final int PRODUTTIVITA_ORAFINE = 22;
	public static final String PRODUTTIVITA_MED_ORA_SCAS_TIPONORMALE = "NORMALE";
	public static final String PRODUTTIVITA_MED_ORA_SCAS_TIPOVELOCE = "VELOCE";

	public static final String PRODUTTIVITA_MED_TTS_FASCIA1 = " <=5 ";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA2 = " between 6 and 10 ";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA3 = " between 11 and 20 ";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA4 = " between 21 and 30 ";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA5 = " >30 ";

	public static final String PRODUTTIVITA_MED_TTS_FASCIA1_DESC = "SCONTRINI&#160;&#160;&#160;&#160;&lt;=5&#160;&#160;&#160;&#160;ARTICOLI";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA2_DESC = "SCONTRINI&#160;&#160;6-10&#160;&#160;&#160;&#160;&#160;&#160;ARTICOLI";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA3_DESC = "SCONTRINI&#160;11-20&#160;&#160;&#160;&#160;&#160;ARTICOLI";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA4_DESC = "SCONTRINI&#160;21-30&#160;&#160;&#160;&#160;&#160;ARTICOLI";
	public static final String PRODUTTIVITA_MED_TTS_FASCIA5_DESC = "SCONTRINI&#160;&#160;&#160;&#160;&gt;30&#160;&#160;&#160;&#160;ARTICOLI";

	public static final String EFTSTAMPA_XMLFILE = "report_eft_stampa.xml";
	public static final String EFTSTAMPA_XSL_HTML = "report_eft_stampa.xsl";
	public static final String EFTSTAMPA_OUTFILE = "report_eft_stampa";
	public static final String EFTSTAMPA_XSL_PDF = "report_eft_stampafo.xsl";
	public static final String EFTSTAMPA_XSL_TXT = "report_eft_stampatxt.xsl";
	public static final String EFTSTAMPA_XSL_FLAT = "report_eft_stampa_flat.xsl";
	public static final String EFTSTAMPA_OUTFILE_FLAT = "RPTXX";

	public static final String IVAPUNTUALE_XMLFILE = "report_iva_puntuale.xml";
	public static final String IVAPUNTUALE_XSL_HTML = "report_iva_puntuale.xsl";
	public static final String IVAPUNTUALE_OUTFILE = "report_iva_puntuale";
	public static final String IVAPUNTUALE_XSL_PDF = "report_iva_puntualefo.xsl";
	public static final String IVAPUNTUALE_XSL_TXT = "report_iva_puntualetxt.xsl";
	public static final String IVAPUNTUALE_XSL_FLAT = "report_iva_puntuale_flat.xsl";
	public static final String IVAPUNTUALE_OUTFILE_FLAT = "RPTXX";

	public static final String DETTAGLIOVERSAMENTI_XMLFILE = "report_dettaglio_versamenti.xml";
	public static final String DETTAGLIOVERSAMENTI_XSL_HTML = "report_dettaglio_versamenti.xsl";
	public static final String DETTAGLIOVERSAMENTI_OUTFILE = "report_dettaglio_versamenti";
	public static final String DETTAGLIOVERSAMENTI_XSL_PDF = "report_dettaglio_versamentifo.xsl";
	public static final String DETTAGLIOVERSAMENTI_XSL_TXT = "report_dettaglio_versamentitxt.xsl";
	public static final String DETTAGLIOVERSAMENTI_XSL_FLAT = "report_dettaglio_versamenti_flat.xsl";
	public static final String DETTAGLIOVERSAMENTI_OUTFILE_FLAT = "RPTXX";

	public static final String FONDOCASSA_XMLFILE = "report_fondo_cassa.xml";
	public static final String FONDOCASSA_XSL_HTML = "report_fondo_cassa.xsl";
	public static final String FONDOCASSA_OUTFILE = "report_fondo_cassa";
	public static final String FONDOCASSA_XSL_PDF = "report_fondo_cassafo.xsl";
	public static final String FONDOCASSA_XSL_TXT = "report_fondo_cassatxt.xsl";
	public static final String FONDOCASSA_XSL_FLAT = "report_fondo_cassa_flat.xsl";
	public static final String FONDOCASSA_OUTFILE_FLAT = "RPTXX";

	public static final String STORNICASSIERE_XMLFILE_TMP = "/tmp/storni_cassiere.xml";
	public static final String STORNICASSIERE_XMLFILE = "report_storni_cassiere.xml";
	public static final String STORNICASSIERE_XSL_HTML = "report_storni_cassiere.xsl";
	public static final String STORNICASSIERE_OUTFILE = "report_storni_cassiere";
	public static final String STORNICASSIERE_XSL_PDF = "report_storni_cassierefo.xsl";
	public static final String STORNICASSIERE_XSL_TXT = "report_storni_cassieretxt.xsl";
	public static final String STORNICASSIERE_XSL_FLAT = "report_storni_cassiere_flat.xsl";
	public static final String STORNICASSIERE_OUTFILE_FLAT = "RPTXX";
	public static final String ARTICOLIFINANZA_XMLFILE_TMP = "/tmp/articoli_finanza.xml";
	public static final String ARTICOLIFINANZA_XMLFILE = "report_articoli_finanza.xml";
	public static final String ARTICOLIFINANZA_XSL_HTML = "report_articoli_finanza.xsl";
	public static final String ARTICOLIFINANZA_OUTFILE = "report_articoli_finanza";
	public static final String ARTICOLIFINANZA_XSL_PDF = "report_articoli_finanzafo.xsl";
	public static final String ARTICOLIFINANZA_XSL_TXT = "report_articoli_finanzatxt.xsl";
	public static final String ARTICOLIFINANZA_XSL_FLAT = "report_articoli_finanza_flat.xsl";
	public static final String ARTICOLIFINANZA_OUTFILE_FLAT = "RPTXX";
	public static final String BOLLONICASSIERE_XMLFILE_TMP = "/tmp/bolloni_cassiere.xml";
	public static final String BOLLONICASSIERE_XMLFILE = "report_bolloni_cassiere.xml";
	public static final String BOLLONICASSIERE_XSL_HTML = "report_bolloni_cassiere.xsl";
	public static final String BOLLONICASSIERE_OUTFILE = "report_bolloni_cassiere";
	public static final String BOLLONICASSIERE_XSL_PDF = "report_bolloni_cassierefo.xsl";
	public static final String BOLLONICASSIERE_XSL_TXT = "report_bolloni_cassieretxt.xsl";
	public static final String BOLLONICASSIERE_XSL_FLAT = "report_bolloni_cassiere_flat.xsl";
	public static final String BOLLONICASSIERE_OUTFILE_FLAT = "RPTXX";

	public static final String APRICHIUDICASSIERI_XMLFILE = "report_aprichiudi_cassieri.xml";
	public static final String APRICHIUDICASSIERI_XSL_HTML = "report_aprichiudi_cassieri.xsl";
	public static final String APRICHIUDICASSIERI_OUTFILE = "report_aprichiudi_cassieri";
	public static final String APRICHIUDICASSIERI_XSL_PDF = "report_aprichiudi_cassierifo.xsl";
	public static final String APRICHIUDICASSIERI_XSL_TXT = "report_aprichiudi_cassieritxt.xsl";
	public static final String APRICHIUDICASSIERI_XSL_FLAT = "report_aprichiudi_cassieri_flat.xsl";
	public static final String APRICHIUDICASSIERI_OUTFILE_FLAT = "RPTXX";

	public static final String INCASSOSETT_XMLFILE = "report_incasso_settimanale.xml";
	public static final String INCASSOSETT_XSL_HTML = "report_incasso_settimanale.xsl";
	public static final String INCASSOSETT_LASER_XSL_HTML = "report_incasso_settimanale_laser.xsl";
	public static final String INCASSOSETT_OUTFILE = "report_incasso_settimanale";
	public static final String INCASSOSETT_XSL_PDF = "report_incasso_settimanalefo.xsl";
	public static final String INCASSOSETT_XSL_TXT = "report_incasso_settimanaletxt.xsl";
	public static final String INCASSOSETT_XSL_FLAT = "report_incasso_settimanale_flat.xsl";
	public static final String INCASSOSETT_OUTFILE_FLAT = "RPTXX";
	public static final String INCASSOSETT_FOOD_TOTDESC = "TOT. ALIM.";
	public static final String INCASSOSETT_NONFOOD_TOTDESC = "TOT. NON FOOD";
	public static final String INCASSOSETT_GIFT_TOTDESC = "TOT. GIFT";
	public static final String INCASSOSETT_GIFT_DPTDESC = "GIFT CARD          ";
	public static final String INCASSOSETT_TOTGEN_DESC = "TOTALE GENERALE";
	public static final String INCASSOSETT_TOTCLI_DESC = "TOTALE CLIENTI";
	public static final String INCASSOSETT_VIRTUALTASTO_GIORNO = "<tasto><descrizione>GIORNO</descrizione><reparto><html>41</html></reparto></tasto>\n";
	public static final String INCASSOSETT_WIDTHVALUES = "width_values";
	public static final String INCASSOSETT_MODE_REPORT = "mode_report";
	public static final String INCASSOSETT_MODE_REPORT_LASER = "laser";
	public static final String INCASSOSETT_MODE_REPORT_AGHI = "aghi";

	public static final String INCASSOSETT_MAJOR_FOOD_1 = "*001";
	public static final String INCASSOSETT_MAJOR_FOOD_2 = "*002";
	public static final String INCASSOSETT_MAJOR_FOOD_3 = "*003";

	public static final String INCASSOSETT_MAJOR_NOFOOD_4 = "*004";
	public static final String INCASSOSETT_MAJOR_NOFOOD_5 = "*005";
	public static final String INCASSOSETT_MAJOR_NOFOOD_6 = "*006";
	public static final String INCASSOSETT_MAJOR_NOFOOD_7 = "*007";
	public static final String INCASSOSETT_MAJOR_NOFOOD_9 = "*009";

	public static final String QUADRATURACASSIERI_XMLFILE = "report_quadratura_cassiere.xml";
	public static final String QUADRATURACASSIERI_XSL_HTML = "report_quadratura_cassiere.xsl";
	public static final String QUADRATURACASSIERI_XSL_TXT = "report_quadratura_cassieretxt.xsl";
	public static final String QUADRATURACASSIERI_OUTFILE = "report_quadratura_cassiere";

	public static final String STORICOARTICOLIBATCH_XMLFILE = "report_storicoarticoli_batch";
	public static final String STORICOARTICOLIBATCH_XMLFILE_LIST = "report_storicoarticoli_batch_list.xml";
	public static final String STORICOARTICOLIBATCH_XSL_HTML = "report_storicoarticoli_batch.xsl";
	public static final String STORICOARTICOLIBATCH_XSL_HTML_LIST = "report_storicoarticoli_batch_list.xsl";
	public static final String STORICOARTICOLIBATCH_XSL_PDF = "report_storicoarticoli_batchfo.xsl";
	public static final String STORICOARTICOLIBATCH_XSL_FLAT = COMMON_XSL_FLAT;
	public static final String STORICOARTICOLIBATCH_XSL_FLAT_LIST = COMMON_XSL_FLAT_LIST;
	public static final String STORICOARTICOLIBATCH_XSL_TXT = "report_storicoarticoli_batchtxt.xsl";
	public static final String STORICOARTICOLIBATCH_XSL_TXT_LIST = "report_storicoarticoli_batchtxt_list.xsl";
	public static final String STORICOARTICOLIBATCH_OUTFILE = "report_storicoarticoli_batch";
	public static final String STORICOARTICOLIBATCH_OUTFILE_LIST = "report_storicoarticoli_batch";

	public static final String VENDTIMEFUORISTD_XMLFILE = "report_vendtime_fuoristd";
	public static final String VENDTIMEFUORISTD_XMLFILE_LIST = "report_vendtime_fuoristd_list.xml";
	public static final String VENDTIMEFUORISTD_XSL_HTML = "report_vendtime_fuoristd.xsl";
	public static final String VENDTIMEFUORISTD_XSL_HTML_LIST = "report_vendtime_fuoristd_list.xsl";
	public static final String VENDTIMEFUORISTD_XSL_PDF = "report_vendtime_fuoristdfo.xsl";
	public static final String VENDTIMEFUORISTD_XSL_FLAT = COMMON_XSL_FLAT;
	public static final String VENDTIMEFUORISTD_XSL_FLAT_LIST = COMMON_XSL_FLAT_LIST;
	public static final String VENDTIMEFUORISTD_XSL_TXT = "report_vendtime_fuoristdtxt.xsl";
	public static final String VENDTIMEFUORISTD_XSL_TXT_LIST = "report_vendtime_fuoristdtxt_list.xsl";
	public static final String VENDTIMEFUORISTD_OUTFILE = "report_vendtime_fuoristd";
	public static final String VENDTIMEFUORISTD_OUTFILE_LIST = "report_vendtime_fuoristd";

	public static final String QUADRATURACASSIERI_TOTTENDER = "tot_tender";
	public static final String QUADRATURACASSIERI_FONDOCASSA = "fondo_cassa";
	public static final String QUADRATURACASSIERI_VERSAMENTI = "versamenti";
	public static final String QUADRATURACASSIERI_FONDOCASSAGS = "fondo_cassa_gs";
	public static final String QUADRATURACASSIERI_DISTVERSAMENTI = "dist_versamenti";
	public static final String QUADRATURACASSIERI_PAGAMENTIEFT = "pagamenti_eft";
	public static final String QUADRATURACASSIERI_ECCAMM = "ecc_amm";

	public static final int FLAGINFO_PRINTNOTZERO = 0;
	public static final int FLAGINFO_SUBTRACTSUBTOTAL = 1;
	public static final int FLAGINFO_ADDSUBTOTAL = 2;
	public static final int FLAGINFO_PRINTTRANSACTION = 3;
	public static final int FLAGINFO_PRINTITEM = 4;
	public static final int FLAGINFO_PRINTAMOUNT = 5;

	public static final int FINTOTID_TOTGIORN_INF = 101;
	public static final int FINTOTID_TOTGIORN_SUP = 109;
	public static final int FINTOTID_TOTLORDE_INF = 201;
	public static final int FINTOTID_TOTLORDE_SUP = 209;
	public static final int FINTOTID_SCONTIART_INF = 301;
	public static final int FINTOTID_SCONTIART_SUP = 399;
	public static final int FINTOTID_SCONTIAUTO_INF = 401;
	public static final int FINTOTID_SCONTIAUTO_SUP = 499;
	public static final int FINTOTID_PUNTI_INF = 831;
	public static final int FINTOTID_PUNTI_SUP = 839;
	public static final int FINTOTID_ALTREENT_INF = 501;
	public static final int FINTOTID_ALTREENT_SUP = 509;
	public static final int FINTOTID_PRELIEVI_INF = 601;
	public static final int FINTOTID_PRELIEVI_SUP = 609;

	public static final int FINTOTID_ALIQUOTE_INF = 711;
	public static final int FINTOTID_ALIQUOTE_SUP = 719;
	public static final int FINTOTID_LORDIIVA_INF = 701;
	public static final int FINTOTID_LORDIIVA_SUP = 709;
	public static final int FINTOTID_VENDMERC_INF = 801;
	public static final int FINTOTID_VENDMERC_SUP = 801;
	public static final int FINTOTID_STAT_INF = 802;
	public static final int FINTOTID_STAT_SUP = 810;
	public static final int FINTOTID_TRANRES_INF = 811;
	public static final int FINTOTID_TRANRES_SUP = 811;
	public static final int FINTOTID_PROMO_INF = 821;
	public static final int FINTOTID_PROMO_SUP = 829;
	public static final int FINTOTID_PROD_INF = 901;
	public static final int FINTOTID_PROD_SUP = 909;
	public static final int FINTOTID_TENDER_INF = 1001;
	public static final int FINTOTID_TENDER_SUP = 1009;
	public static final int FINTOTID_RIEPILOGO_INF = 100;
	public static final int FINTOTID_RIEPILOGO_SUP = 200;
	public static final int FINTOTID_RIEPILOGO_BASEID = 1101;
	public static final int FINTOTID_RIEPILOGO_MAXLIMIT = 5001;
	public static final int FINTOTID_RIEPILOGO_STEP = 100;
	public static final int FIN_RIEPILOGO_LIST_INFPOS = 0;
	public static final int FIN_RIEPILOGO_LIST_SUPPOS = 1;
	public static final int FIN_RIEPILOGO_LIST_DESCPOS = 2;
	public static final int FIN_RIEPILOGO_LIST_FLAGPOS = 3;

	public static final int RIM_FINTOTID_NUMERO_CLIENTI = 99000200;
	public static final int RIM_FINTOTID_INCASSI_VAL_STRAN_E3_ON = 20913;
	public static final int RIM_FINTOTID_TASSO_CAMB_VAL_STRAN_3_ON = 20923;
	public static final int RIM_FINTOTID_VERSAMENTI_VAL_STRAN_3_ON = 21204;
	public static final int RIM_FINTOTID_VERSAMENTI_VAL_STRAN_E3_ON = 21214;

	public static final int RIM_FINTOTID_INCASSI_VAL_STRAN_E3_OFF = 120913;
	public static final int RIM_FINTOTID_TASSO_CAMB_VAL_STRAN_3_OFF = 120923;
	public static final int RIM_FINTOTID_VERSAMENTI_VAL_STRAN_3_OFF = 121204;
	public static final int RIM_FINTOTID_VERSAMENTI_VAL_STRAN_E3_OFF = 121214;

	public static final String FIN_TOTGIORN_LABEL = "TOT. GIORNO";
	public static final String FIN_TOTLORDE_LABEL = "TOT. LORDO";
	public static final String FIN_SCONTIART_LABEL = "SCONTI ART.";
	public static final String FIN_SCONTIAUTO_LABEL = "SCONTI TRANS.";
	public static final String FIN_PUNTI_LABEL = "PUNTI";
	public static final String FIN_ARROT_LABEL = "ARROTONDAMENTI";
	public static final String FIN_ALTREENT_LABEL = "ALTRE ENTRATE";
	public static final String FIN_PRELIEVI_LABEL = "PRELIEVI";
	public static final String FIN_ALIQUOTE_LABEL = "IMPOSTA IVA";
	public static final String FIN_LORDIIVA_LABEL = "LORDI IVA";
	public static final String FIN_RESTI_LABEL = "RESTI";
	public static final String FIN_STAT_LABEL = "STATISTICHE";
	public static final String FIN_VENDMERC_LABEL = "VENDITE MERC.";
	public static final String FIN_TRANRES_LABEL = "TRAN_RESUME";
	public static final String FIN_PROMO_LABEL = "PROMOZIONI";
	public static final String FIN_REVISIONE_LABEL = "REVISIONE";
	public static final String FIN_CATEGORIE_LABEL = "CATEGORIE";
	public static final String FIN_PROD_LABEL = "PRODUTTIVITA";
	public static final String FIN_MEDI_LABEL = "VALORI MEDI";
	public static final String FIN_TENDER_LABEL = "TENDER MEDIA";
	public static final String FIN_RIEPILOGO_LABEL = "RIEPILOGO";
	public static final String FIN_TOTGIORN_CODICE = "tot_giornaliero";
	public static final String FIN_TOTLORDE_CODICE = "tot_lorde";
	public static final String FIN_SCONTIART_CODICE = "sconti_articoli";
	public static final String FIN_SCONTIAUTO_CODICE = "sconti_automatici";
	public static final String FIN_PUNTI_CODICE = "punti";
	public static final String FIN_ARROT_CODICE = "arrotondamenti";
	public static final String FIN_ALTREENT_CODICE = "altre_entrate";
	public static final String FIN_PRELIEVI_CODICE = "prelievi";
	public static final String FIN_ALIQUOTE_CODICE = "aliquote";
	public static final String FIN_LORDIIVA_CODICE = "lordi_iva";
	public static final String FIN_RESTI_CODICE = "resti";
	public static final String FIN_VENDMERC_CODICE = "vendite_merceologiche";
	public static final String FIN_STAT_CODICE = "statistiche";
	public static final String FIN_TRANRES_CODICE = "tran_resume";
	public static final String FIN_PROMO_CODICE = "promozioni";
	public static final String FIN_REVISIONE_CODICE = "revisione";
	public static final String FIN_CATEGORIE_CODICE = "categorie";
	public static final String FIN_PROD_CODICE = "produttivita";
	public static final String FIN_MEDI_CODICE = "valori_medi";
	public static final String FIN_TENDER_CODICE = "tender_media";
	public static final String FIN_RIEPILOGO_CODICE = "riepilogo";
	public static final String FIN_RIEPILOGO_GRUPPO_CODICE = "gruppo_riepilogo";

	public static final String TIPOGRUPPO_RIGASX_JASPER = "1";
	public static final String TIPOGRUPPO_RIGADX_JASPER = "2";

	public static final String FIN_TOTGIORN_TOTLORDE_CODICE = "tot_giornaliero_lorde";
	public static final String FIN_SCONTIART_SCONTIAUTO_CODICE = "sconti_articoli_automatici";
	public static final String FIN_ALIQUOTE_LORDIIVA_CODICE = "aliquote_lordi_iva";
	public static final String FIN_VENDMERC_STAT_CODICE = "vendite_merceologiche_statistiche";
	public static final String FIN_PROD_MEDI_CODICE = "produttivita_valori_medi";
	public static final String FIN_TENDER_PUNTI_CODICE = "tender_media_punti";

	public static final String FIN_TOTGIORN_LORDE_LABEL = "DAILY TOTALS-GROSS";
	public static final String FIN_SCONTIART_AUTO_LABEL = "ITEM-TRANS DISCOUNTS";
	public static final String FIN_ALIQUOTE_LORDIIVA_LABEL = "VAT-GROSS TAX";
	public static final String FIN_VENDMERC_STAT_LABEL = "DEPT SALES-STATISTICS";
	public static final String FIN_PROD_MEDI_LABEL = "PRODUCTIVITY-AVERAGES";
	public static final String FIN_TENDER_PUNTI_LABEL = "TENDER MEDIA-POINTS";

	public static final String FIN_GENERALE_ARTICOLI_LABEL = "Items";
	public static final String FIN_GENERALE_CLIENTI_LABEL = "Customers";
	public static final String FIN_GENERALE_IMPORTO_LABEL = "Amount";
	public static final String FIN_TEMPO_ARTICOLI_LABEL = "Items";
	public static final String FIN_TEMPO_CLIENTI_LABEL = "Customers";
	public static final String FIN_TEMPO_IMPORTO_LABEL = "Time";

	public static final String RIM_FIN_TASSO_CAMBIO_VALUTA_LABEL = "RIMAPP TASSO CAMBIO VALUTA STRANIERA";
	public static final String RIM_FIN_VERSAMENTI_VALUTA_LABEL = "RIMAPP VERSAMENTI VALUTA STRANIERA";
	public static final String RIM_FIN_VERSAMENTI_VALUTA_IN_EURO_LABEL = "RIMAPP VERSAMENTI VALUTA STRANIERA IN EURO";
	public static final String RIM_FIN_INCASSI_VALUTA_IN_EURO_LABEL = "RIMAPP INCASSI VALUTA STRANIERA IN EURO";
	public static final String RIM_FIN_NUMERO_CLIENTI_LABEL = "RIMAPP NUMERO CLIENTI";

	public static final String ARTPROMO_CODPROMO_SMVI = "SMVI";
	public static final String ARTPROMO_CODPROMO_SATI = "SATI";
	public static final String ARTPROMO_CODPROMO_SAVI = "SAVI";
	public static final String ARTPROMO_CODPROMO_SAPI = "SAPI";
	public static final String ARTPROMO_CODPROMO_SMVL = "SMVL";
	public static final String ARTPROMO_CODPROMO_PTVI = "PTVI";
	public static final String ARTPROMO_CODPROMO_PAVI = "PAVI";
	public static final String ARTPROMO_CODPROMO_STPI = "STPI";
	public static final String ARTPROMO_CODPROMO_RAPI = "RAPI";
	public static final String ARTPROMO_CODPROMO_RACI = "RACI";
	public static final String ARTPROMO_CODPROMO_RASI = "RASI";
	public static final String ARTPROMO_CODPROMO_SWVI = "SWVI";

	public static final String ARTPROMO_TIPOPROMO_M = "M";
	public static final String ARTPROMO_TIPOPROMO_T = "T";
	public static final String ARTPROMO_TIPOPROMO_V = "V";
	public static final String ARTPROMO_TIPOPROMO_S = "S";
	public static final String ARTPROMO_TIPOPROMO_Q = "Q";
	public static final String ARTPROMO_TIPOPROMO_P = "P";
	public static final String ARTPROMO_TIPOPROMO_W = "W";

	public static final String ARTPROMO_FLAGFIDELITY_VERO = "S";
	public static final String ARTPROMO_FLAGFIDELITY_FALSO = "N";

	public static final int MIN_CASSIERE_FASTLANE = 600;
	public static final int MAX_CASSIERE_FASTLANE = 699;

	public static final String TIPO_CASSA_ASAR_CODES_BARRIERA = "N";
	public static final String TIPO_CASSA_ASAR_CODES_HIFI = "H";
	public static final String TIPO_CASSA_ASAR_CODES_TEX = "T";
	public static final String TIPO_CASSA_ASAR_CODES_FASTLANE = "F";
	public static final String TIPO_CASSA_ASAR_CODES_FARMA = "P";
	public static final String TIPO_CASSA_ASAR_CODES_RAP = "R";

	public static final String TIPO_CASSA_ASAR_CODES_HIFI_TEX_FARMA = "HTP";
	public static final String TIPO_CASSA_ASAR_CODES_FASTLANE_RAP = "FR";
}
