package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.ArrayList;
import java.util.List;

public class FinancialReportGroup extends DataObject {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<").append(getStrCodice()).append(">\n");
		sb.append("<data>").append(getStrData()).append("</data>\n");
		sb.append("<label>").append(getStrLabel()).append("</label>\n");
		for (int i = 0; i < getElements().size(); i++) {
			FinancialReportItem objElemento = (FinancialReportItem) getElements().get(i);
			objElemento.setStrGroupLabel(getStrLabel());

			sb.append(objElemento.getXml());
		}
		sb.append("</").append(getStrCodice()).append(">\n");

		return sb.toString();
	}

	public String getXml_DOUBLE() {
		StringBuilder sb = new StringBuilder();

		sb.append("<").append(getStrCodice()).append(">\n");
		sb.append("<data>").append(getStrData()).append("</data>\n");
		sb.append("<label>").append(getStrLabel()).append("</label>\n");
		for (int i = 0; i < getElements().size(); i++) {
			FinancialReportDoubleElement objElementoDoppio = (FinancialReportDoubleElement) getElements().get(i);

			objElementoDoppio.setStrGroupLabel_1(getStrLabel());
			objElementoDoppio.setStrGroupLabel_2(getStrLabel());

			sb.append(objElementoDoppio.getXml());
		}
		sb.append("</").append(getStrCodice()).append(">\n");

		return sb.toString();
	}

	public void processProductivity() {
		double dblTotal = ((FinancialReportItem) getElements().get(0)).getDblValore3();
		for (int j = 0; j < getElements().size(); j++) {
			double dblCalcoloPerc = 0;
			FinancialReportItem objElem = (FinancialReportItem) getElements().get(j);

			if (objElem.getDblValore3() == 0 || dblTotal == 0) {
				dblCalcoloPerc = 0;
			} else {
				dblCalcoloPerc = (objElem.getDblValore3() * 100) / dblTotal;
			}
			objElem.setDblPerc(dblCalcoloPerc);
		}
	}

	public String getStrData() {
		return strData;
	}

	public void setStrData(String strDataIn) {
		this.strData = strDataIn;
	}

	public String getStrTipoRigaGruppo() {
		return strTipoRigaGruppo;
	}

	public void setStrTipoRigaGruppo(String strTipoRigaGruppoIn) {
		this.strTipoRigaGruppo = strTipoRigaGruppoIn;
	}

	public boolean isFlagElementoAperto() {
		return flagElementoAperto;
	}

	public void setFlagElementoAperto(boolean flagElementoApertoIn) {
		this.flagElementoAperto = flagElementoApertoIn;
	}

	public String getStrCodice() {
		return strCodice;
	}

	public void setStrCodice(String strCodice) {
		this.strCodice = strCodice;
	}

	public String getStrLabel() {
		return strLabel;
	}

	public void setStrLabel(String strLabel) {
		this.strLabel = strLabel;
	}

	public List getElements() {
		return elements;
	}

	public void setElements(List elements) {
		this.elements = elements;
	}

	public Integer getTotalIdCli() {
		return totalIdCli;
	}

	public void setTotalIdCli(Integer totalIdCli) {
		this.totalIdCli = totalIdCli;
	}

	private String strData; // campo chiave subreport
	private String strTipoRigaGruppo = "";
	boolean flagElementoAperto = false;
	private String strCodice = "";
	private String strLabel = "";
	private List elements = new ArrayList<>();
	private Integer totalIdCli;
}
