package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class DepartmentReportGroup extends DataObject {
	
	public DepartmentReportGroup(boolean detail) {
		this.detail = detail;
	}
	
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<raggruppamento codice='").append(code).append("' >\n");
		sb.append("<data>").append(date).append("</data>\n");
		sb.append("<major>").append(code).append("</major>\n");
		sb.append("<desc_major>").append(description).append("</desc_major>\n");
		if (detail) {
			for (int i = 0; i < getDepartments().size(); i++) {
				sb.append(getDepartments().get(i).getXml());
			}
		}
		sb.append("<importototale_major>").append(GeneralPurpose.formatNumber(Float.toString(totalAmount), "#,###.00"))
		.append("</importototale_major>\n");
		sb.append("<importototale>").append(GeneralPurpose.formatNumber(Float.toString(totalAmount), "#,###.00"))
		.append("</importototale>\n");
		sb.append("<tassoimportototale_major>")
				.append(GeneralPurpose.formatNumber(Float.toString(totalAmountRate), "#,###.00"))
				.append("</tassoimportototale_major>\n");
		sb.append("<articolitotale_major>").append(articlesTotal).append("</articolitotale_major>\n");
		sb.append("<tassoarticolitotale_major>")
				.append(GeneralPurpose.formatNumber(Float.toString(articlesTotalRate), "#,###.00"))
				.append("</tassoarticolitotale_major>\n");
		sb.append("<clientitotale_major>").append(customersTotal).append("</clientitotale_major>\n");
		sb.append("<tassoclientitotale_major>")
				.append(GeneralPurpose.formatNumber(Float.toString(customersTotalRate), "#,###.00"))
				.append("</tassoclientitotale_major>\n");
		sb.append("<scontidesc_major>").append(salesDescription).append("</scontidesc_major>\n");
		sb.append("<importosconti_major>").append(GeneralPurpose.formatNumber(Float.toString(salesAmount), "#,###.00"))
				.append("</importosconti_major>\n");
		sb.append("<tassoimportosconti_major>")
				.append(GeneralPurpose.formatNumber(Float.toString(salesAmountRate), "#,###.00"))
				.append("</tassoimportosconti_major>\n");
		sb.append("<articolisconti_major>").append(articlesSales).append("</articolisconti_major>\n");
		sb.append("<tassoarticolisconti_major>")
				.append(GeneralPurpose.formatNumber(Float.toString(articlesSalesRate), "#,###.00"))
				.append("</tassoarticolisconti_major>\n");
		sb.append("<clientisconti_major>").append(customersSales).append("</clientisconti_major>\n");
		sb.append("<tassoclientisconti_major>")
				.append(GeneralPurpose.formatNumber(Float.toString(customersSalesRate), "#,###.00"))
				.append("</tassoclientisconti_major>\n");
		sb.append("<nettedesc_major>").append(netDescription).append("</nettedesc_major>\n");
		sb.append("<importonette_major>").append(GeneralPurpose.formatNumber(Float.toString(netAmount), "#,###.00"))
				.append("</importonette_major>\n");
		sb.append("<tassoimportonette_major>").append(GeneralPurpose.formatNumber(Float.toString(netAmountRate), "#,###.00"))
				.append("</tassoimportonette_major>\n");
		sb.append("</raggruppamento>\n");
		logger.trace(String.format("[getXml] sb = [%s]", sb));

		return sb.toString();
	}

	public void calculateTotals() {
		float accTotImporto = 0;
		int accTotArticoli = 0;
		int accTotClienti = 0;
		float accScontoImporto = 0;
		int accScontoArticoli = 0;
		int accScontoClienti = 0;
		float accNetteImporto = 0;

		for (int i = 0; i < getDepartments().size(); i++) {
			DepartmentReportItem rep = getDepartments().get(i);

			accTotImporto += rep.getTotalAmount();
			accTotArticoli += rep.getArticlesTotal();
			accTotClienti += rep.getCustomersTotal();
			accScontoImporto += rep.getSalesAmount();
			accScontoArticoli += rep.getArticlesSales();
			accScontoClienti += rep.getCustomersSales();
			accNetteImporto += rep.getNetAmount();
		}

		setTotalAmount(accTotImporto);
		setArticlesTotal(accTotArticoli);
		setCustomersTotal(accTotClienti);
		setSalesAmount(accScontoImporto);
		setArticlesSales(accScontoArticoli);
		setCustomersSales(accScontoClienti);
		setNetAmount(accNetteImporto);
	}

	public void calculateRates(float fltTotImporto, float fltTotArticoli, float fltTotClienti,
			float fltTotImportoSconti, float fltTotImportoNetti, float fltTotArticoliSconti, float fltTotClientiSconti) {
		logger.info(String.format("[calculateRates] customersTotal = [%f], code = [%s]",
				fltTotClienti, code));

		setTotalAmountRate((getTotalAmount() / fltTotImporto) * 100);
		setSalesAmountRate((getSalesAmount() / fltTotImportoSconti) * 100);
		setNetAmountRate((getNetAmount() / fltTotImportoNetti) * 100);
		setArticlesTotalRate(((float) this.getArticlesTotal() / fltTotArticoli) * 100);
		setArticlesSalesRate(((float) this.getArticlesSales() / fltTotArticoliSconti) * 100);
		setCustomersTotalRate(((float) this.getCustomersTotal() / fltTotClienti) * 100);
		setCustomersSalesRate(((float) this.getCustomersSales() / fltTotClientiSconti) * 100);

		for (int i = 0; i < getDepartments().size(); i++) {
			DepartmentReportItem rep = getDepartments().get(i);

			rep.setTotalAmountRate((rep.getTotalAmount() / fltTotImporto) * 100);
			rep.setSalesAmountRate((rep.getSalesAmount() / fltTotImportoSconti) * 100);
			rep.setNetAmountRate((rep.getNetAmount() / fltTotImportoNetti) * 100);
			rep.setArticlesTotalRate(((float) rep.getArticlesTotal() / fltTotArticoli) * 100);
			rep.setArticlesSalesRate(((float) rep.getArticlesSales() / fltTotArticoliSconti) * 100);
			rep.setCustomersTotalRate(((float) rep.getCustomersTotal() / fltTotClienti) * 100);
			rep.setCustomersSalesRate(((float) rep.getCustomersSales() / fltTotClientiSconti) * 100);

			logger.info(String
					.format("[calculateRates] department: customerTotals = [%d], code = [%s], customersTotalRate = [%f]",
							rep.getCustomersTotal(), rep.getCode(), rep.getCustomersTotalRate()));
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		if (description != null) {
			this.description = description.replace("&", "&amp;");
		}
	}

	public String getSalesDescription() {
		return salesDescription;
	}

	public void setSalesDescription(String salesDescription) {
		this.salesDescription = salesDescription;
	}

	public String getNetDescription() {
		return netDescription;
	}

	public void setNetDescription(String netDescription) {
		this.netDescription = netDescription;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public float getTotalAmountRate() {
		return totalAmountRate;
	}

	public void setTotalAmountRate(float totalAmountRate) {
		this.totalAmountRate = totalAmountRate;
	}

	public int getArticlesTotal() {
		return articlesTotal;
	}

	public void setArticlesTotal(int articlesTotal) {
		this.articlesTotal = articlesTotal;
	}

	public int getCustomersTotal() {
		return customersTotal;
	}

	public void setCustomersTotal(int customersTotal) {
		this.customersTotal = customersTotal;
	}

	public float getArticlesTotalRate() {
		return articlesTotalRate;
	}

	public void setArticlesTotalRate(float articlesTotalRate) {
		this.articlesTotalRate = articlesTotalRate;
	}

	public float getCustomersTotalRate() {
		return customersTotalRate;
	}

	public void setCustomersTotalRate(float customersTotalRate) {
		this.customersTotalRate = customersTotalRate;
	}

	public float getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(float salesAmount) {
		this.salesAmount = salesAmount;
	}

	public float getSalesAmountRate() {
		return salesAmountRate;
	}

	public void setSalesAmountRate(float salesAmountRate) {
		this.salesAmountRate = salesAmountRate;
	}

	public float getArticlesSalesRate() {
		return articlesSalesRate;
	}

	public void setArticlesSalesRate(float articlesSalesRate) {
		this.articlesSalesRate = articlesSalesRate;
	}

	public float getCustomersSalesRate() {
		return customersSalesRate;
	}

	public void setCustomersSalesRate(float customersSalesRate) {
		this.customersSalesRate = customersSalesRate;
	}

	public int getArticlesSales() {
		return articlesSales;
	}

	public void setArticlesSales(int articlesSales) {
		this.articlesSales = articlesSales;
	}

	public int getCustomersSales() {
		return customersSales;
	}

	public void setCustomersSales(int customersSales) {
		this.customersSales = customersSales;
	}

	public float getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(float netAmount) {
		this.netAmount = netAmount;
	}

	public float getNetAmountRate() {
		return netAmountRate;
	}

	public void setNetAmountRate(float netAmountRate) {
		this.netAmountRate = netAmountRate;
	}

	public List<DepartmentReportItem> getDepartments() {
		return departments;
	}

	public void setDepartments(List<DepartmentReportItem> departments) {
		this.departments = departments;
	}

	private Logger logger = WebFrontLogger.getLogger(DepartmentReportGroup.class);
	private String date;
	private String code;
	private String description;
	private String salesDescription;
	private String netDescription;
	private float totalAmount;
	private float totalAmountRate;
	private int articlesTotal;
	private int customersTotal;
	private float articlesTotalRate;
	private float customersTotalRate;
	private float salesAmount;
	private float salesAmountRate;
	private float articlesSalesRate;
	private float customersSalesRate;
	private int articlesSales;
	private int customersSales;
	private float netAmount;
	private float netAmountRate;
	private List<DepartmentReportItem> departments;
	private boolean detail = true;

}
