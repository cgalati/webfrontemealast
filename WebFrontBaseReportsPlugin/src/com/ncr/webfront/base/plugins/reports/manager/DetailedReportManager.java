package com.ncr.webfront.base.plugins.reports.manager;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReportDoubleElement;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReportGroup;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReportItem;
import com.ncr.webfront.base.plugins.reports.dataobject.FinancialReportRow;
import com.ncr.webfront.base.plugins.reports.dataobject.ReportHeader;
import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.base.reports.ReportOptions;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class DetailedReportManager extends BaseReportManager {
	public DetailedReportManager(ReportOptions reportOptions) throws Exception {
		super(reportOptions);
	}

	@Override
	public abstract InputStream innerGeneration() throws SQLException;

	protected ReportHeader createHeader(String title, String stream, String strCassaId, String checker)
			throws SQLException {
		ReportHeader head = this.loadHeader(stream);
		head.setTitle(title);

		if (!strCassaId.equals("ALL")) {
			head.setType("Parziale Terminale");
		} else {
			head.setType("Totale");
		}
		head.setPosTerminalCode(strCassaId);

		head.setPosTerminalStatus("");
		head.setPosOperatorCode(checker);
		head.setPosOperatorName("");
		head.setEntityCode("");
		head.setEntityName("");

		return head;
	}

	protected List<FinancialReportGroup> updateRowset(List<FinancialReportGroup> vctSingleElement) throws SQLException {

		List<FinancialReportGroup> vctRowsetDoppio = new ArrayList<>();
		FinancialReportGroup objGruppoDoppio_1 = null;
		FinancialReportGroup objGruppoDoppio_2 = null;
		FinancialReportGroup objGruppoDoppio = null;

		int CONTA_TOTGIORN_LORDE_LABEL = 0;
		int CONTA_SCONTIART_AUTO_LABEL = 0;
		int CONTA_ALIQUOTE_LORDIIVA_LABEL = 0;
		int CONTA_VENDMERC_STAT_LABEL = 0;
		int CONTA_PROD_MEDI_LABEL = 0;
		int CONTA_TENDER_PUNTI_LABEL = 0;

		for (int i = 0; i < vctSingleElement.size(); i++) {
			FinancialReportGroup objGruppo = (FinancialReportGroup) vctSingleElement.get(i);

			if ((objGruppo.getStrLabel().equals(ReportsConstants.FIN_TOTGIORN_LORDE_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_SCONTIART_AUTO_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_VENDMERC_STAT_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL) || objGruppo.getStrLabel()
					.equals(ReportsConstants.FIN_TENDER_PUNTI_LABEL))
					&& objGruppo.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGASX_JASPER)) {

				objGruppoDoppio_1 = this.createGroupDoppioElemento_1_2(objGruppo);
				if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_TOTGIORN_LORDE_LABEL))
					CONTA_TOTGIORN_LORDE_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_SCONTIART_AUTO_LABEL))
					CONTA_SCONTIART_AUTO_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL))
					CONTA_ALIQUOTE_LORDIIVA_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_VENDMERC_STAT_LABEL))
					CONTA_VENDMERC_STAT_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL))
					CONTA_PROD_MEDI_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_TENDER_PUNTI_LABEL))
					CONTA_TENDER_PUNTI_LABEL++;

			} else if ((objGruppo.getStrLabel().equals(ReportsConstants.FIN_TOTGIORN_LORDE_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_SCONTIART_AUTO_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_VENDMERC_STAT_LABEL)
					|| objGruppo.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL) || objGruppo.getStrLabel()
					.equals(ReportsConstants.FIN_TENDER_PUNTI_LABEL))
					&& objGruppo.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER)) {
				objGruppoDoppio_2 = this.createGroupDoppioElemento_1_2(objGruppo);
				if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_TOTGIORN_LORDE_LABEL))
					CONTA_TOTGIORN_LORDE_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_SCONTIART_AUTO_LABEL))
					CONTA_SCONTIART_AUTO_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL))
					CONTA_ALIQUOTE_LORDIIVA_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_VENDMERC_STAT_LABEL))
					CONTA_VENDMERC_STAT_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL))
					CONTA_PROD_MEDI_LABEL++;
				else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_TENDER_PUNTI_LABEL))
					CONTA_TENDER_PUNTI_LABEL++;
			}

			if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_TOTGIORN_LORDE_LABEL)
					&& CONTA_TOTGIORN_LORDE_LABEL == 2) {
				objGruppoDoppio = this.createGroupDoppioElemento(objGruppoDoppio_1, objGruppoDoppio_2);
				vctRowsetDoppio.add(objGruppoDoppio);
			} else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_SCONTIART_AUTO_LABEL)
					&& CONTA_SCONTIART_AUTO_LABEL == 2) {
				objGruppoDoppio = this.createGroupDoppioElemento(objGruppoDoppio_1, objGruppoDoppio_2);
				vctRowsetDoppio.add(objGruppoDoppio);
			} else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_ALIQUOTE_LORDIIVA_LABEL)
					&& CONTA_ALIQUOTE_LORDIIVA_LABEL == 2) {
				objGruppoDoppio = this.createGroupDoppioElemento(objGruppoDoppio_1, objGruppoDoppio_2);
				vctRowsetDoppio.add(objGruppoDoppio);
			} else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_VENDMERC_STAT_LABEL)
					&& CONTA_VENDMERC_STAT_LABEL == 2) {
				objGruppoDoppio = this.createGroupDoppioElemento(objGruppoDoppio_1, objGruppoDoppio_2);
				vctRowsetDoppio.add(objGruppoDoppio);
			} else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)
					&& CONTA_PROD_MEDI_LABEL == 2) {
				objGruppoDoppio = this.createGroupDoppioElemento(objGruppoDoppio_1, objGruppoDoppio_2);
				vctRowsetDoppio.add(objGruppoDoppio);
			} else if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_TENDER_PUNTI_LABEL)
					&& CONTA_TENDER_PUNTI_LABEL == 2) {
				objGruppoDoppio = this.createGroupDoppioElemento(objGruppoDoppio_1, objGruppoDoppio_2);
				vctRowsetDoppio.add(objGruppoDoppio);
			}
		}

		return vctRowsetDoppio;
	}

	protected List<FinancialReportGroup> createSummary(String date, String terminal, String checker, boolean div)
			throws SQLException {
		List<FinancialReportGroup> listaRiepilogo = new ArrayList<>();

		List<String> gruppi = this.getGroupsSummaryList();
		for (int i = 0; i < gruppi.size(); i++) {
			List<String> infoGroup = GeneralPurpose.unpackListWithNullsConverted((String) gruppi.get(i),
					ReportsConstants.NCR_INFLIST_SEPARATOR, ReportsConstants.NCR_INFLIST_SEPARATOR);
			FinancialReportGroup group = this.createGroup(
					Integer.parseInt((String) infoGroup.get(ReportsConstants.FIN_RIEPILOGO_LIST_INFPOS)),
					Integer.parseInt((String) infoGroup.get(ReportsConstants.FIN_RIEPILOGO_LIST_SUPPOS)),
					ReportsConstants.FIN_RIEPILOGO_GRUPPO_CODICE,
					(String) infoGroup.get(ReportsConstants.FIN_RIEPILOGO_LIST_DESCPOS), date, terminal, checker, div);
			group.setStrData(date);
			listaRiepilogo.add(group);
		}

		return listaRiepilogo;
	}

	protected FinancialReportGroup createGroupJasper(int intIdInf, int intIdSup, String strCodice, String strLabel,
			String strTipoRiga, String strDate, String strCassaId, String strCassiereId, boolean flagDiv)
			throws SQLException {

		logger.info("BaseReportManager:createGroup - intIdInf=" + intIdInf);
		logger.info("BaseReportManager:createGroup - intIdSup=" + intIdSup);
		logger.info("BaseReportManager:createGroup - strCodice=" + strCodice);
		logger.info("BaseReportManager:createGroup - strDate=" + strDate);
		logger.info("BaseReportManager:createGroup - strLabel=" + strLabel);
		logger.info("BaseReportManager:createGroup - strCassaId=" + strCassaId);
		logger.info("BaseReportManager:createGroup - strCassiereId=" + strCassiereId);

		FinancialReportGroup objFinGroup = new FinancialReportGroup();
		objFinGroup.setStrLabel(strLabel);
		objFinGroup.setStrCodice(strCodice);
		objFinGroup.setStrData(strDate);
		objFinGroup.setStrTipoRigaGruppo(strTipoRiga);

		List<FinancialReportItem> vctFinElement = new ArrayList<>();
		List<FinancialReportRow> vctFinPlanRow = null;

		vctFinPlanRow = this.getFinancialPlanRows(intIdInf, intIdSup);

		for (int i = 0; i < vctFinPlanRow.size(); i++) {
			FinancialReportRow objFinPlanRow = vctFinPlanRow.get(i);

			String strElemType = "N";
			if (objFinPlanRow.getStrDescription().toUpperCase().trim().startsWith("SUBTOTAL")) {
				strElemType = "S";
			}
			if (objFinPlanRow.getStrType().equals("HID")) {
				strElemType = "HID";
			}

			List<Integer> vctFinTotId = new ArrayList<>();
			vctFinTotId.add(new Integer(objFinPlanRow.getIntFinancialTotalId()));
			FinancialReportItem objFinElement = this.createElement(vctFinTotId, strDate,
					objFinPlanRow.getStrDescription(), strElemType, strCassaId, strCassiereId, flagDiv);
			objFinElement.setStrLabel(strLabel);
			if (objFinElement.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)) {
				objFinElement.setStrLabelArticoli(ReportsConstants.FIN_TEMPO_ARTICOLI_LABEL);
				objFinElement.setStrLabelClienti(ReportsConstants.FIN_TEMPO_CLIENTI_LABEL);
				objFinElement.setStrLabelImporto(ReportsConstants.FIN_TEMPO_IMPORTO_LABEL);				
			} else {
				objFinElement.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
				objFinElement.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
				objFinElement.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);				
			}
			objFinElement.setStrTipoRigaGruppo(strTipoRiga);

			if (objFinElement != null) {
				objFinElement.setFlags(objFinPlanRow.getFlags());
				logger.info("BaseReportManager:createGroup - un nuovo elemento");
				vctFinElement.add(objFinElement);
			}
		}

		objFinGroup.setElements(vctFinElement);

		return objFinGroup;
	}

	private FinancialReportGroup createGroup(int intIdInf, int intIdSup, String strCodice, String strLabel,
			String strDate, String strCassaId, String strCassiereId, boolean flagDiv) throws SQLException {

		logger.info("BaseReportManager:createGroup - intIdInf=" + intIdInf);
		logger.info("BaseReportManager:createGroup - intIdSup=" + intIdSup);
		logger.info("BaseReportManager:createGroup - strCodice=" + strCodice);
		logger.info("BaseReportManager:createGroup - strDate=" + strDate);
		logger.info("BaseReportManager:createGroup - strLabel=" + strLabel);
		logger.info("BaseReportManager:createGroup - strCassaId=" + strCassaId);
		logger.info("BaseReportManager:createGroup - strCassiereId=" + strCassiereId);

		FinancialReportGroup objFinGroup = new FinancialReportGroup();
		objFinGroup.setStrLabel(strLabel);
		objFinGroup.setStrCodice(strCodice);
		objFinGroup.setStrData(strDate);

		List<FinancialReportItem> vctFinElement = new ArrayList<>();
		List<FinancialReportRow> vctFinPlanRow = null;

		vctFinPlanRow = this.getFinancialPlanRows(intIdInf, intIdSup);

		for (int i = 0; i < vctFinPlanRow.size(); i++) {
			FinancialReportRow objFinPlanRow = (FinancialReportRow) vctFinPlanRow.get(i);

			String strElemType = "N";
			if (objFinPlanRow.getStrDescription().toUpperCase().trim().startsWith("SUBTOTAL")) {

				strElemType = "S";
			}

			List<Integer> vctFinTotId = new ArrayList<>();
			vctFinTotId.add(new Integer(objFinPlanRow.getIntFinancialTotalId()));
			FinancialReportItem objFinElement = this.createElement(vctFinTotId, strDate,
					objFinPlanRow.getStrDescription(), strElemType, strCassaId, strCassiereId, flagDiv);
			objFinElement.setStrLabel(strLabel);
			if (objFinElement.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)) {
				objFinElement.setStrLabelArticoli(ReportsConstants.FIN_TEMPO_ARTICOLI_LABEL);
				objFinElement.setStrLabelClienti(ReportsConstants.FIN_TEMPO_CLIENTI_LABEL);
				objFinElement.setStrLabelImporto(ReportsConstants.FIN_TEMPO_IMPORTO_LABEL);				
			} else {
				objFinElement.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
				objFinElement.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
				objFinElement.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);				
			}
			if (objFinElement != null) {
				objFinElement.setFlags(objFinPlanRow.getFlags());
				logger.info("BaseReportManager:createGroup - un nuovo elemento");
				vctFinElement.add(objFinElement);
			}
		}

		objFinGroup.setElements(vctFinElement);

		return objFinGroup;
	}

	private List<FinancialReportRow> getFinancialPlanRows(int intInfTotalId, int intSupTotalId) throws SQLException {
		logger.info("BaseReportManager:getFinancialPlanRows - InfTotalId=" + intInfTotalId);
		logger.info("BaseReportManager:getFinancialPlanRows - SupTotalId=" + intSupTotalId);

		List<FinancialReportRow> list = new ArrayList<>();

		Statement objStatement = null;
		ResultSet objResultSet = null;

		String strQuery = "select fp.FINANCIALTOTALID as id, fp.DESCRIPTION as descr, "
				+ "fp.FLAGINFO as flag, fp.TYPE as tipo from IDC_FINANCIAL_PLAN fp " + "where fp.FINANCIALTOTALID >= '"
				+ GeneralPurpose.padLeft(Integer.toString(intInfTotalId), 4, "0") + "' and fp.FINANCIALTOTALID <= '"
				+ GeneralPurpose.padLeft(Integer.toString(intSupTotalId), 4, "0") + "' order by fp.FINANCIALTOTALID";
		logger.info("BaseReportManager:getFinancialPlanRows - eseguita query = " + strQuery);
		objStatement = connection.createStatement();
		objResultSet = objStatement.executeQuery(strQuery);

		while (objResultSet.next()) {
			FinancialReportRow row = new FinancialReportRow();
			row.setIntFinancialTotalId(Integer.parseInt(objResultSet.getString("id")));

			if ((objResultSet.getString("descr") != null) && (objResultSet.getString("descr").length() > 0)) {
				row.setStrDescription(objResultSet.getString("descr").trim());
			} else {
				row.setStrDescription("");
			}

			row.setStrFlagInfo(objResultSet.getString("flag"));

			if ((objResultSet.getString("tipo") != null) && (objResultSet.getString("tipo").length() > 0)) {
				row.setStrType(objResultSet.getString("tipo"));
			} else {
				row.setStrType("");
			}

			row.processFlagInfo();

			list.add(row);
			logger.info("BaseReportManager:getFinancialPlanRows - Aggiunta una riga!!");
		}

		objResultSet.close();
		objStatement.close();

		logger.info("BaseReportManager:getFinancialPlanRows - listarows" + list);
		return list;
	}

	private FinancialReportGroup createGroupDoppioElemento(FinancialReportGroup objGruppoDoppio1,
			FinancialReportGroup objGruppoDoppio2) throws SQLException {
		FinancialReportGroup objGruppoDoppio = new FinancialReportGroup();
		objGruppoDoppio.setStrLabel(objGruppoDoppio1.getStrLabel());
		objGruppoDoppio.setStrCodice(objGruppoDoppio1.getStrCodice());
		objGruppoDoppio.setStrData(objGruppoDoppio1.getStrData()); // campo
																	// chiave
																	// subreport

		List<FinancialReportDoubleElement> vctFinDoubleElement = new ArrayList<>();

		logger.info("FinancialReportManager: - createGroupDoppioElemento start 1 "
				+ objGruppoDoppio1.getElements().size());
		logger.info("FinancialReportManager: - createGroupDoppioElemento start 2 "
				+ objGruppoDoppio2.getElements().size());

		for (int i = 0, j = 0; i < objGruppoDoppio1.getElements().size() || j < objGruppoDoppio2.getElements().size(); i++, j++) {

			FinancialReportDoubleElement objDoubleElement = new FinancialReportDoubleElement();
			FinancialReportDoubleElement objDoubleElement_1 = null;
			FinancialReportDoubleElement objDoubleElement_2 = null;
			if (i < objGruppoDoppio1.getElements().size()) {
				objDoubleElement_1 = (FinancialReportDoubleElement) objGruppoDoppio1.getElements().get(i);

				logger.info("FinancialReportManager: - createGroupDoppioElemento start 1 getStrNome_1 "
						+ objDoubleElement_1.getStrNome_1());
				logger.info("FinancialReportManager: - createGroupDoppioElemento start 1 getStrNome_1 "
						+ objDoubleElement_1.getDblValore3_1());

				objDoubleElement.setStrLabel_1(objDoubleElement_1.getStrLabel_1());
				objDoubleElement.setStrLabelClienti_1(objDoubleElement_1.getStrLabelClienti_1());
				objDoubleElement.setStrLabelArticoli_1(objDoubleElement_1.getStrLabelArticoli_1());
				objDoubleElement.setStrLabelImporto_1(objDoubleElement_1.getStrLabelImporto_1());
				objDoubleElement.setStrNome_1(objDoubleElement_1.getStrNome_1());
				objDoubleElement.setStrTipo_1(objDoubleElement_1.getStrTipo_1());
				objDoubleElement.setIntFinancialTotalId_1(objDoubleElement_1.getIntFinancialTotalId_1());
				objDoubleElement.setIntValore1_1(objDoubleElement_1.getIntValore1_1());
				objDoubleElement.setIntValore2_1(objDoubleElement_1.getIntValore2_1());
				objDoubleElement.setDblValore3_1(objDoubleElement_1.getDblValore3_1());
				objDoubleElement.setStrGroupLabel_1(objDoubleElement_1.getStrGroupLabel_1());
				objDoubleElement.setVctFlags_1(objDoubleElement_1.getVctFlags_1());
				objDoubleElement.setDblPerc_1(objDoubleElement_1.getDblPerc_1());
			}

			if (j < objGruppoDoppio2.getElements().size()) {
				objDoubleElement_2 = (FinancialReportDoubleElement) objGruppoDoppio2.getElements().get(j);

				logger.info("FinancialReportManager: - createGroupDoppioElemento start 1 getStrNome_2 "
						+ objDoubleElement_2.getStrNome_2());
				logger.info("FinancialReportManager: - createGroupDoppioElemento start 1 getStrNome_2 "
						+ objDoubleElement_2.getDblValore3_2());

				objDoubleElement.setStrLabel_2(objDoubleElement_2.getStrLabel_2());
				objDoubleElement.setStrLabelClienti_2(objDoubleElement_2.getStrLabelClienti_2());
				objDoubleElement.setStrLabelArticoli_2(objDoubleElement_2.getStrLabelArticoli_2());
				objDoubleElement.setStrLabelImporto_2(objDoubleElement_2.getStrLabelImporto_2());
				objDoubleElement.setStrNome_2(objDoubleElement_2.getStrNome_2());
				objDoubleElement.setStrTipo_2(objDoubleElement_2.getStrTipo_2());
				objDoubleElement.setIntFinancialTotalId_2(objDoubleElement_2.getIntFinancialTotalId_2());
				objDoubleElement.setIntValore1_2(objDoubleElement_2.getIntValore1_2());
				objDoubleElement.setIntValore2_2(objDoubleElement_2.getIntValore2_2());
				objDoubleElement.setDblValore3_2(objDoubleElement_2.getDblValore3_2());
				objDoubleElement.setStrGroupLabel_2(objDoubleElement_2.getStrGroupLabel_2());
				objDoubleElement.setVctFlags_2(objDoubleElement_2.getVctFlags_2());
				objDoubleElement.setDblPerc_2(objDoubleElement_2.getDblPerc_2());
			}

			if (i < objGruppoDoppio1.getElements().size())
				objDoubleElement.setStrLabel(objDoubleElement_1.getStrLabel());
			else if (j < objGruppoDoppio2.getElements().size())
				objDoubleElement.setStrLabel(objDoubleElement_2.getStrLabel());

			logger.info("FinancialReportManager: - createGroupDoppioElemento add double elemento getStrLabel "
					+ objDoubleElement.getStrLabel());

			vctFinDoubleElement.add(objDoubleElement);
		}

		objGruppoDoppio.setElements(vctFinDoubleElement);

		return objGruppoDoppio;
	}

	private FinancialReportGroup createGroupDoppioElemento_1_2(FinancialReportGroup objGruppo) throws SQLException {

		logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto gruppo "
				+ objGruppo.getStrLabel());

		FinancialReportGroup objGruppoDoppio = new FinancialReportGroup();
		objGruppoDoppio.setStrLabel(objGruppo.getStrLabel());
		objGruppoDoppio.setStrCodice(objGruppo.getStrCodice());
		objGruppoDoppio.setStrData(objGruppo.getStrData()); // campo chiave
															// subreport

		List<FinancialReportDoubleElement> vctFinDoubleElement = new ArrayList<>();

		for (int j = 0; j < objGruppo.getElements().size(); j++) {
			FinancialReportItem objElement = (FinancialReportItem) objGruppo.getElements().get(j);
			
			FinancialReportDoubleElement objDoubleElement = new FinancialReportDoubleElement();

			if (objGruppo.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGASX_JASPER)) {
				objDoubleElement.setStrLabel(objElement.getStrLabel());
				objDoubleElement.setStrLabel_1(objElement.getStrLabel());
				objDoubleElement.setStrLabelArticoli_1(objElement.getStrLabelArticoli());
				objDoubleElement.setStrLabelClienti_1(objElement.getStrLabelClienti());
				objDoubleElement.setStrLabelImporto_1(objElement.getStrLabelImporto());
				objDoubleElement.setStrNome_1(objElement.getStrNome());
				objDoubleElement.setStrTipo_1(objElement.getStrTipo());
				objDoubleElement.setIntFinancialTotalId_1(objElement.getIntFinancialTotalId());
				objDoubleElement.setIntValore1_1(objElement.getIntValore1());
				objDoubleElement.setIntValore2_1(objElement.getIntValore2());
				objDoubleElement.setDblValore3_1(objElement.getDblValore3());
				objDoubleElement.setStrGroupLabel_1(objElement.getStrGroupLabel());
				objDoubleElement.setVctFlags_1(objElement.getFlags());
				objDoubleElement.setDblPerc_1(objElement.getDblPerc());

				logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto elemento 1 "
						+ objElement.getStrNome());
				logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto elemento 1 "
						+ objDoubleElement.getStrNome_1());
				logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto elemento 1 "
						+ objDoubleElement.getDblValore3_1());

			} else if (objGruppo.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER)) {
				objDoubleElement.setStrLabel(objElement.getStrLabel());
				objDoubleElement.setStrLabel_2(objElement.getStrLabel());
				objDoubleElement.setStrLabelArticoli_2(objElement.getStrLabelArticoli());
				objDoubleElement.setStrLabelClienti_2(objElement.getStrLabelClienti());
				objDoubleElement.setStrLabelImporto_2(objElement.getStrLabelImporto());
				objDoubleElement.setStrNome_2(objElement.getStrNome());
				objDoubleElement.setStrTipo_2(objElement.getStrTipo());
				objDoubleElement.setIntFinancialTotalId_2(objElement.getIntFinancialTotalId());
				objDoubleElement.setIntValore1_2(objElement.getIntValore1());
				objDoubleElement.setIntValore2_2(objElement.getIntValore2());
				objDoubleElement.setDblValore3_2(objElement.getDblValore3());
				objDoubleElement.setStrGroupLabel_2(objElement.getStrGroupLabel());
				objDoubleElement.setVctFlags_2(objElement.getFlags());
				objDoubleElement.setDblPerc_2(objElement.getDblPerc());

				logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto elemento 2 "
						+ objElement.getStrNome());
				logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto elemento 2 "
						+ objDoubleElement.getStrNome_2());
				logger.info("FinancialReportManager: - createGroupDoppioElemento_1_2 aggiunto elemento 2 "
						+ objDoubleElement.getDblValore3_2());
			}
			vctFinDoubleElement.add(objDoubleElement);
		}

		objGruppoDoppio.setElements(vctFinDoubleElement);

		logger.info("FinancialReportManager:createGroupDoppioElemento_1 - Stop "
				+ objGruppoDoppio.getElements().size());
		return objGruppoDoppio;
	}

	private FinancialReportItem createElement(List<Integer> vctFinTotId, String strDate, String strLabel,
			String strType, String strCassaId, String strCassiereId, boolean flagDiv) throws SQLException {
		final String CURRENT_METHOD = " createElement- ";
		logger.info(CURRENT_METHOD + "vctFinTotId="
				+ vctFinTotId.toString());
		logger.info(CURRENT_METHOD + "strDate=" + strDate);
		logger.info(CURRENT_METHOD + "strType=" + strType);
		logger.info(CURRENT_METHOD + "strLabel=" + strLabel);
		logger.info(CURRENT_METHOD + "strCassaId=" + strCassaId);
		logger.info(CURRENT_METHOD + "strCassiereId=" + strCassiereId);

		FinancialReportItem objFinElemen = null;

		Statement objStatement = null;
		ResultSet objResultSet = null;

		String strQuery = "";
		String strQuerySuffix = "";

		if (!strCassaId.equals("ALL")) {
			StringTokenizer sti = new StringTokenizer(strCassaId, ",");
			strQuerySuffix += " and (fin.POSNUMBER=" + GeneralPurpose.oraFormatNumber(sti.nextToken());
			while (sti.hasMoreTokens()) {
				strQuerySuffix += " or fin.POSNUMBER=" + GeneralPurpose.oraFormatNumber(sti.nextToken());
			}
			strQuerySuffix += ")";
		}

		if (strCassiereId != null && !strCassiereId.equals("ALL")) {// ///
			StringTokenizer stCassa = new StringTokenizer(strCassiereId, ",");
			strQuerySuffix += " and (fin.CHECKERNUMBER=" + GeneralPurpose.oraFormatNumber(stCassa.nextToken());
			while (stCassa.hasMoreTokens()) {
				strQuerySuffix += " or fin.CHECKERNUMBER=" + GeneralPurpose.oraFormatNumber(stCassa.nextToken());
			}
			strQuerySuffix += ")";

			// strQuerySuffix += " and fin.CHECKERNUMBER=" +
			// GeneralPurpose.oraFormatNumber(strCassiereId);
		}

		String strTabella = "IDC_" + this.getExtractionTable() + "FIN";
		StringBuffer sbWhereCond = new StringBuffer();
		sbWhereCond.append("where fin.FINANCIALTOTALID ");

		// cambia la where a seconda di = (un solo valore) oppure IN (più
		// valori)
		if (vctFinTotId.size() > 1) {
			sbWhereCond.append(" in (");

			for (int i = 0; i < vctFinTotId.size() - 1; i++) {
				sbWhereCond.append("'")
						.append(GeneralPurpose.padLeft(((Integer) vctFinTotId.get(i)).toString(), 4, "0")).append("',");
			}

			sbWhereCond
					.append("'")
					.append(GeneralPurpose.padLeft(((Integer) vctFinTotId.get(vctFinTotId.size() - 1)).toString(), 4,
							"0")).append("')");
		} else {
			sbWhereCond.append(" = '")
					.append(GeneralPurpose.padLeft(((Integer) vctFinTotId.get(0)).toString(), 4, "0")).append("'");
		}

		strQuery = "select coalesce(sum(coalesce(fin.CUSTOMERCOUNTER,0)),0) as valore1, "
				+ "coalesce(sum(coalesce(fin.ITEMCOUNTER,0)),0) as valore2, coalesce(sum(coalesce(fin.AMOUNT,0)),0) as valore3 from "
				//+ strTabella + " fin " + sbWhereCond.toString() + " and fin.DDATE=to_date(" +
				//GeneralPurpose.oraFormatString(strDate) + ",'yyyy-mm-dd') " + strQuerySuffix;
				+ strTabella + " fin " + sbWhereCond.toString() + " and fin.DDATE=convert(DATETIME, " +
				GeneralPurpose.oraFormatString(strDate) + ",102) " + strQuerySuffix;
				// clsNcrRapportiGeneralPurpose.oraFormatString(strDate) +
				// ",'dd/mm/yyyy') " + strQuerySuffix;

		if (this.getStream().equals(ReportsConstants.STREAM_FLAT)) {
			strQuery += " and fin.CHECKERNUMBER < 800";

		}
		logger.info(CURRENT_METHOD + "creata stringa query = "
				+ strQuery);

		objStatement = connection.createStatement();
		objResultSet = objStatement.executeQuery(strQuery);
		logger.info(CURRENT_METHOD + "eseguita query = " + strQuery);

		int count = 0;
		while (objResultSet.next()) {
			count++;

			objFinElemen = new FinancialReportItem();

			if (!this.isRimapped()) {
				objFinElemen.setIntFinancialTotalId(((Integer) vctFinTotId.get(0)).intValue());
			} else {
				objFinElemen.setIntFinancialTotalId(this.getFinTotIdCli());
			}

			objFinElemen.setStrNome(strLabel);
			if (strType.equals("TOT")) {
				objFinElemen.setStrTipo("S");
			} else {
				objFinElemen.setStrTipo("N");
			}
			if (strType.equals("HID")) {
				objFinElemen.setStrTipo("H");
			}

			objFinElemen.setStrNumProg(Integer.toString(count));

			objFinElemen.setStrCassa(strCassaId);
			objFinElemen.setStrCassiere(strCassiereId);
			objFinElemen.setIntValore1(objResultSet.getInt("valore1"));
			objFinElemen.setIntValore2(objResultSet.getInt("valore2"));
			if (flagDiv) {
				double dblTmp = objResultSet.getDouble("valore3");
				double dblTmpTot = dblTmp / 100;

				objFinElemen.setDblValore3(dblTmpTot);
			} else {
				double dblTmp = objResultSet.getDouble("valore3");

				objFinElemen.setDblValore3(dblTmp);
			}

			logger.info(CURRENT_METHOD + "istanziato un FinElemento!!");
		}

		objResultSet.close();
		objStatement.close();

		return objFinElemen;
	}
	
	private Logger logger = WebFrontLogger.getLogger(DetailedReportManager.class);
}
