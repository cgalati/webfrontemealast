package com.ncr.webfront.base.plugins.reports;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class ReportsMappingProperties {
	public static final String reportsPath = "reports.path";
	public static final String reportsPathDefault = "conf/reports" + File.separator + "jasper";
	
	public static final String posOperatorJasperPath = "pos.operator.jasper.path";
	public static final String posOperatorJasperPathDefault = reportsPathDefault + File.separatorChar + "pos-operator-report.jasper";

	public static final String posOperatorCashJasperPath = "pos.operator.cash.jasper.path";
	public static final String posOperatorCashJasperPathDefault = reportsPathDefault + File.separatorChar + "pos-operator-cash-report.jasper";

	public static final String financialJasperPath = "financial.reports.path";
	public static final String financialJasperPathDefault = reportsPathDefault + File.separatorChar + "financial-report.jasper";
	
	public static final String hourlyActivityJasperPath = "hourly.activity.reports.path";
	public static final String hourlyActivityJasperPathDefault = reportsPathDefault + File.separatorChar + "hourly-activity-report.jasper";
	
	public static final String departmentJasperPath = "department.reports.path";
	public static final String departmentJasperPathDefault = reportsPathDefault + File.separatorChar + "department-report.jasper";
	
	public static final String posOperatorCashIgnoreEmptyOperators = "pos.operator.cash.ignore.empty.operators";
	
	public static synchronized ReportsMappingProperties getInstance() {
		if (instance == null) {
			instance = new ReportsMappingProperties();
		}
		
		return instance;
	}
	
	private ReportsMappingProperties() {
		reports = PropertiesLoader.loadOrCreatePropertiesFile(reportsPluginPropertiesFileName, getReportsPropertiesMap());
	}
	
	private static Map<String, String> getReportsPropertiesMap() {
		Map<String, String> reportsPropertiesMap = new HashMap<>();
		
		reportsPropertiesMap.put(reportsPath, reportsPathDefault);
		reportsPropertiesMap.put(posOperatorJasperPath, posOperatorJasperPathDefault);
		reportsPropertiesMap.put(financialJasperPath, financialJasperPathDefault);
		reportsPropertiesMap.put(hourlyActivityJasperPath, hourlyActivityJasperPathDefault);
		reportsPropertiesMap.put(departmentJasperPath, departmentJasperPathDefault);
		
		return reportsPropertiesMap;
	}
	
	public String getProperty(String key) {
		return getProperty(key, false);
	}
	
	public String getProperty(String key, boolean prependWorkingDir) {
		String value = reports.getProperty(key);
		
		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}
		
		return value;
	}

	private  final String reportsPluginPropertiesFileName = "reports.properties";

	private Properties reports;
	
	private static ReportsMappingProperties instance;
}
