package com.ncr.webfront.base.plugins.reports.dataobject;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class HourlyActivityReport extends BaseReport<HourlyActivityReportRow> {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<report>\n");
		if (!isFlagPaging()) {
			sb.append("<pagina>\n");
			sb.append(getHeader().getXml());
			sb.append("<dati>\n");
			for (int i = 0; i < getRowSet().size(); i++) {
				HourlyActivityReportRow row = getRowSet().get(i);

				sb.append(row.getXml());
			}
			sb.append("</dati>\n");
			sb.append(getXmlRowTotals());
			sb.append("</pagina>\n");
		}
		sb.append("</report>\n");
		logger.trace(String.format("[getXml] sb = [%s]", sb));

		return sb.toString();
	}

	private String getXmlRowTotals() {
		StringBuilder sb = new StringBuilder();

		sb.append("<totale>\n");
		sb.append("<importo>").append(GeneralPurpose.formatNumber(Float.toString(fltTotaleImporto), "#,###.00"))
				.append("</importo>\n");
		sb.append("<articoli>").append(intTotaleArticoli).append("</articoli>\n");
		sb.append("<clienti>").append(intTotaleClienti).append("</clienti>\n");
		sb.append("<statistichevenduto>")
				.append(GeneralPurpose.formatNumber(Float.toString(fltMediaStatisticheVenduto), "#,###.00"))
				.append("</statistichevenduto>\n");
		sb.append("<statistichearticoli>")
				.append(GeneralPurpose.formatNumber(Float.toString(fltMediaStatisticheArticoli), "#,###.00"))
				.append("</statistichearticoli>\n");
		sb.append("</totale>\n");
		logger.trace(String.format("[getRigaTotaliXml] sb = [%s]", sb));

		return sb.toString();
	}

	public void processTotalSubtotalRates() {
		float acc_Importo = 0;
		int acc_Articoli = 0;
		int acc_Clienti = 0;

		// Calculates totals, subtotals and averages
		for (int i = 0; i < getRowSet().size(); i++) {
			HourlyActivityReportRow row = getRowSet().get(i);

			acc_Importo += row.getFltImporto();
			acc_Articoli += row.getIntArticoli();
			acc_Clienti += row.getIntClienti();

			row.setFltSubTotale(acc_Importo);
		}
		setFltTotaleImporto(acc_Importo);
		setIntTotaleArticoli(acc_Articoli);
		setIntTotaleClienti(acc_Clienti);
		setFltMediaStatisticheVenduto(getFltTotaleImporto() / (float) getIntTotaleClienti());
		setFltMediaStatisticheArticoli((float) getIntTotaleArticoli() / (float) getIntTotaleClienti());

		// Calculates rates
		for (int i = 0; i < getRowSet().size(); i++) {
			HourlyActivityReportRow row = getRowSet().get(i);

			row.setFltTassoImporto((row.getFltImporto() / getFltTotaleImporto()) * 100);
			row.setFltTassoArticoli(((float) row.getIntArticoli() / (float) getIntTotaleArticoli()) * 100);
			row.setFltTassoClienti(((float) row.getIntClienti() / (float) getIntTotaleClienti()) * 100);
		}
	}

	public float getFltTotaleImporto() {
		return fltTotaleImporto;
	}

	public void setFltTotaleImporto(float fltTotaleImporto) {
		this.fltTotaleImporto = fltTotaleImporto;
	}

	public int getIntTotaleArticoli() {
		return intTotaleArticoli;
	}

	public void setIntTotaleArticoli(int intTotaleArticoli) {
		this.intTotaleArticoli = intTotaleArticoli;
	}

	public int getIntTotaleClienti() {
		return intTotaleClienti;
	}

	public void setIntTotaleClienti(int intTotaleClienti) {
		this.intTotaleClienti = intTotaleClienti;
	}

	public float getFltMediaStatisticheVenduto() {
		return fltMediaStatisticheVenduto;
	}

	public void setFltMediaStatisticheVenduto(float fltMediaStatisticheVenduto) {
		this.fltMediaStatisticheVenduto = fltMediaStatisticheVenduto;
	}

	public float getFltMediaStatisticheArticoli() {
		return fltMediaStatisticheArticoli;
	}

	public void setFltMediaStatisticheArticoli(float fltMediaStatisticheArticoli) {
		this.fltMediaStatisticheArticoli = fltMediaStatisticheArticoli;
	}

	public boolean isFlagPaging() {
		return flagPaging;
	}

	public void setFlagPaging(boolean flagPaging) {
		this.flagPaging = flagPaging;
	}

	public int getINumReport() {
		return iNumReport;
	}

	public void setINumReport(int iNumReport) {
		this.iNumReport = iNumReport;
	}

	private Logger logger = WebFrontLogger.getLogger(HourlyActivityReport.class);
	private float fltTotaleImporto;
	private int intTotaleArticoli;
	private int intTotaleClienti;
	private float fltMediaStatisticheVenduto;
	private float fltMediaStatisticheArticoli;
	private boolean flagPaging = false;
	private int iNumReport = 1;

}
