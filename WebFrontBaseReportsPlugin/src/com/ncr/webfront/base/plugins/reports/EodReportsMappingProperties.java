package com.ncr.webfront.base.plugins.reports;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class EodReportsMappingProperties {

	private final String eodReportsPropertiesFileName = "eod_reports.properties";
	public static final boolean EOD_REPORT_SUMMARY_DEFAULT_VALUE = true; 
	public static final String EOD_REPORT_REGISTERS_DEFAULT_VALUE = "ALL";
	private Properties props;
	public static EodReportsMappingProperties instance;
	
	public static synchronized EodReportsMappingProperties getInstance() {
		if (instance == null) {
			instance = new EodReportsMappingProperties();
		}
		return instance;
	}
	
	private EodReportsMappingProperties() {
		props = PropertiesLoader.loadOrCreatePropertiesFile(eodReportsPropertiesFileName, EndOfDayReportsMappingPropertiesMap());
	}

	private Map<String, String> EndOfDayReportsMappingPropertiesMap() {
		Map<String, String> propertiesMap = new HashMap<>();

		return propertiesMap;
	}
	
	public String getProperty(String key) {
		return getProperty(key, false);
	}

	public String getProperty(String key, boolean prependWorkingDir) {
		String value = props.getProperty(key);

		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}

		return value;
	}
}
