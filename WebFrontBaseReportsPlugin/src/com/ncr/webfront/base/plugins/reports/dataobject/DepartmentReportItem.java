package com.ncr.webfront.base.plugins.reports.dataobject;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class DepartmentReportItem extends DataObject {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		if (code.equals(majorCode)) {
			code = " ";
		}
		sb.append("<reparto>\n");
		sb.append("<major>").append(majorCode).append("</major>\n");
		sb.append("<codice>").append(code).append("</codice>\n");
		sb.append("<descrizione>").append(GeneralPurpose.replaceString(description, " ", "&#160;"))
				.append("</descrizione>\n");
		sb.append("<importototale>").append(GeneralPurpose.formatNumber(Float.toString(totalAmount), "#,###.00"))
				.append("</importototale>\n");
		sb.append("<tassoimportototale>")
				.append(GeneralPurpose.formatNumber(Float.toString(totalAmountRate), "#,###.00"))
				.append("</tassoimportototale>\n");
		sb.append("<articolitotale>").append(articlesTotal).append("</articolitotale>\n");
		sb.append("<tassoarticolitotale>")
				.append(GeneralPurpose.formatNumber(Float.toString(articlesTotalRate), "#,###.00"))
				.append("</tassoarticolitotale>\n");
		sb.append("<clientitotale>").append(customersTotal).append("</clientitotale>\n");
		sb.append("<tassoclientitotale>")
				.append(GeneralPurpose.formatNumber(Float.toString(customersTotalRate), "#,###.00"))
				.append("</tassoclientitotale>\n");
		sb.append("<scontidesc>").append(salesDescription).append("</scontidesc>\n");
		sb.append("<importosconti>").append(GeneralPurpose.formatNumber(Float.toString(salesAmount), "#,###.00"))
				.append("</importosconti>\n");
		sb.append("<tassoimportosconti>")
				.append(GeneralPurpose.formatNumber(Float.toString(salesAmountRate), "#,###.00"))
				.append("</tassoimportosconti>\n");
		sb.append("<articolisconti>").append(articlesSales).append("</articolisconti>\n");
		sb.append("<tassoarticolisconti>")
				.append(GeneralPurpose.formatNumber(Float.toString(articlesSalesRate), "#,###.00"))
				.append("</tassoarticolisconti>\n");
		sb.append("<clientisconti>").append(customersSales).append("</clientisconti>\n");
		sb.append("<tassoclientisconti>")
				.append(GeneralPurpose.formatNumber(Float.toString(customersSalesRate), "#,###.00"))
				.append("</tassoclientisconti>\n");
		sb.append("<nettedesc>").append(netDescription).append("</nettedesc>\n");
		sb.append("<importonette>").append(GeneralPurpose.formatNumber(Float.toString(netAmount), "#,###.00"))
				.append("</importonette>\n");
		sb.append("<tassoimportonette>").append(GeneralPurpose.formatNumber(Float.toString(netAmountRate), "#,###.00"))
				.append("</tassoimportonette>\n");
		sb.append("</reparto>\n");
		logger.trace(String.format("[getXml] sb =  [%s]", sb));

		return sb.toString();
	}

	public String getMajorCode() {
		return majorCode;
	}

	public void setMajorCode(String majorCode) {
		this.majorCode = majorCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
		if (description != null) {
			this.description = description.replace("&", "&amp;");
		}
	}

	public String getSalesDescription() {
		return salesDescription;
	}

	public void setSalesDescription(String salesDescription) {
		this.salesDescription = salesDescription;
	}

	public String getNetDescription() {
		return netDescription;
	}

	public void setNetDescription(String netDescription) {
		this.netDescription = netDescription;
	}

	public float getTotalAmountRate() {
		return totalAmountRate;
	}

	public void setTotalAmountRate(float totalAmountRate) {
		this.totalAmountRate = totalAmountRate;
	}

	public int getArticlesTotal() {
		return articlesTotal;
	}

	public void setArticlesTotal(int articlesTotal) {
		this.articlesTotal = articlesTotal;
	}

	public int getCustomersTotal() {
		return customersTotal;
	}

	public void setCustomersTotal(int customersTotal) {
		this.customersTotal = customersTotal;
	}

	public float getArticlesTotalRate() {
		return articlesTotalRate;
	}

	public void setArticlesTotalRate(float articlesTotalRate) {
		this.articlesTotalRate = articlesTotalRate;
	}

	public float getCustomersTotalRate() {
		return customersTotalRate;
	}

	public void setCustomersTotalRate(float customersTotalRate) {
		this.customersTotalRate = customersTotalRate;
	}

	public float getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(float salesAmount) {
		this.salesAmount = salesAmount;
	}

	public float getSalesAmountRate() {
		return salesAmountRate;
	}

	public void setSalesAmountRate(float salesAmountRate) {
		this.salesAmountRate = salesAmountRate;
	}

	public float getArticlesSalesRate() {
		return articlesSalesRate;
	}

	public void setArticlesSalesRate(float articlesSalesRate) {
		this.articlesSalesRate = articlesSalesRate;
	}

	public float getCustomersSalesRate() {
		return customersSalesRate;
	}

	public void setCustomersSalesRate(float customersSalesRate) {
		this.customersSalesRate = customersSalesRate;
	}

	public int getArticlesSales() {
		return articlesSales;
	}

	public void setArticlesSales(int articlesSales) {
		this.articlesSales = articlesSales;
	}

	public int getCustomersSales() {
		return customersSales;
	}

	public void setCustomersSales(int customersSales) {
		this.customersSales = customersSales;
	}

	public float getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(float netAmount) {
		this.netAmount = netAmount;
	}

	public float getNetAmountRate() {
		return netAmountRate;
	}

	public void setNetAmountRate(float netAmountRate) {
		this.netAmountRate = netAmountRate;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	private Logger logger = WebFrontLogger.getLogger(DepartmentReportItem.class);
	private String majorCode;
	private String code;
	private String description;
	private String salesDescription;
	private String netDescription;
	private float totalAmountRate;
	private int articlesTotal;
	private int customersTotal;
	private float articlesTotalRate;
	private float customersTotalRate;
	private float salesAmount;
	private float salesAmountRate;
	private float articlesSalesRate;
	private float customersSalesRate;
	private int articlesSales;
	private int customersSales;
	private float netAmount;
	private float netAmountRate;
	private float totalAmount;

}
