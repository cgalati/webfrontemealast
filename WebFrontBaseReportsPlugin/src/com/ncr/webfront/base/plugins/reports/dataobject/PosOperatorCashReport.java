package com.ncr.webfront.base.plugins.reports.dataobject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.ncr.webfront.core.utils.WebFrontEnvParameters;
import com.ncr.webfront.core.utils.converter.CentsToStringCurrencyConverter;

public class PosOperatorCashReport extends BaseReport<PosOperatorCashReportRow> {

	private List<PosOperatorCashReportRow> rowSet;
	
	public String getXml() {
		StringBuilder xml = new StringBuilder();
		Date today = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		String storeCode =  new WebFrontEnvParameters().getStoreCode();
		CentsToStringCurrencyConverter centsToStringCurrencyConverter = new CentsToStringCurrencyConverter();

		xml.append("<report>\n");
		xml.append("<header>\n");
		xml.append("<store>").append(storeCode).append("</store>\n");
		xml.append("<date>").append(dateFormatter.format(today)).append("</date>\n");
		dateFormatter = new SimpleDateFormat("HH:mm");
		xml.append("<time>").append(dateFormatter.format(today)).append("</time>\n");
		xml.append("</header>\n");
		
		xml.append("<posOperatorList>\n");
		for ( PosOperatorCashReportRow posOperatorCashReportRow : rowSet) {
			xml.append("<posOperator>\n");
			xml.append("<posOperatorCode>").append(posOperatorCashReportRow.getPosOperatorCode()).append("</posOperatorCode>\n");
			xml.append("<posOperatorName>").append(posOperatorCashReportRow.getPosOperatorName()).append("</posOperatorName>\n");
			xml.append("<terminal>").append(posOperatorCashReportRow.getTerminal()).append("</terminal>\n");		
			xml.append("<cash>").append(centsToStringCurrencyConverter.convertCentsToString(posOperatorCashReportRow.getCash())).append("</cash>\n");
			xml.append("</posOperator>\n");
		}
		xml.append("</posOperatorList>\n");
		xml.append("</report>");	
		return xml.toString();
	}

	public List<PosOperatorCashReportRow> getRowSet() {
		return rowSet;
	}

	public void setRowSet(List<PosOperatorCashReportRow> rowSet) {
		this.rowSet = rowSet;
	}

	
}
