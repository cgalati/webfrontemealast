package com.ncr.webfront.base.plugins.reports.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class GeneralPurpose {
	private GeneralPurpose() {
	}

	public static String oraFormatNumber(String strIn) {
		if ((strIn == null) || (strIn.trim().length() == 0)) {
			strIn = "null";
		}
		return strIn;
	}

	/**
	 * <p>
	 * Member description: Converts null String in strNull value
	 * <p>
	 * Input Paramethers: strIn = String to test
	 * <p>
	 * Input Paramethers: strNull = value to return for null String
	 */
	public static String StringToNull(String strIn, String strNullConvertor) {
		if ((strIn.equals(strNullConvertor))) {
			return "";
		} else {
			return strIn;
		}
	}

	/**
	 * <p>
	 * Member description: packs with Separator strings of a Vector
	 * <p>
	 * Input Paramethers: strIn = Vector of values, Separator
	 * <p>
	 * Output Paramethers: strOut = String "Separator" separated
	 */
	public static List<String> unpackListWithNullsConverted(String strFieldsList, String strSeparator,
			String strNullConvertor) {
		String strTokenDelim = strSeparator;
		List<String> vctOut = new ArrayList<>();
		int intNumberTokens;

		StringTokenizer stringTokenized = new StringTokenizer(strFieldsList, strTokenDelim);

		intNumberTokens = stringTokenized.countTokens();

		if (intNumberTokens > 0) {
			while (stringTokenized.hasMoreTokens()) {
				String strToken = stringTokenized.nextToken();
				strToken = GeneralPurpose.StringToNull(strToken, strNullConvertor);
				vctOut.add(strToken);
			}

			return vctOut;
		}
		return null;
	}

	public static String taroccaApice(String old) {
		String current = "";

		if (old != null && (old.trim().length() > 0)) {
			for (int j = 0; j < old.length(); j++) {
				if (old.charAt(j) == '\'') {
					current += "''";
				} else {
					current += old.charAt(j);
				}
			}
		}

		return current;
	}

	public static String oraFormatString(String strIn) {
		String strOut = "null";
		if ((strIn != null) && (strIn.trim().length() > 0)) {
			strOut = "'" + GeneralPurpose.taroccaApice(strIn) + "'";
		}
		return strOut;
	}

	public static String padLeft(String stringa, int lunghezza, String carattere) {

		String tempString = stringa;
		while (tempString.length() < lunghezza) {
			tempString = carattere + tempString;
		}
		return tempString;

	}

	public static String replaceString(String strInitial, String strSearch, String strReplace) {

		String strResult = "";
		String strTemp = strInitial;
		boolean loop = true;

		while (loop) {
			int index = strTemp.indexOf(strSearch);

			if (index > 0) {
				strResult += strTemp.substring(0, index);
				strResult += strReplace;

				strTemp = strTemp.substring(index + strSearch.length(), strTemp.length());
			} else {
				loop = false;
			}
		}

		strResult += strTemp;

		return strResult;
	}

	public static String formatNumber(String sValore, String sIntFormat) {
		String myStrNumber;
		try {
			double myNumber = 0;
			if (sValore.trim().length() != 0) {
				myNumber = (double) Double.parseDouble(sValore);
			}
			NumberFormat myFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
			((DecimalFormat) myFormat).applyPattern(sIntFormat);
			myStrNumber = myFormat.format(myNumber).toString();
		} catch (Exception ecc) {
			myStrNumber = " ";
		}
		return (myStrNumber);
	}

	/**
	 * secondToTimetable
	 * 
	 * @param strDate
	 *            String
	 * @return String
	 */
	public static String secondToTimetable(int second) {
		logger.info("GeneralPurpose:secondToTimetable - second-in = " + second);
		String strResult = "";

		int ore = second / 3600;
		String strOre = padLeft("" + ore, 2, "0");

		int minuti = (second - (ore * 3600)) / 60;
		String strMinuti = padLeft("" + minuti, 2, "0");

		int secondi = second - ((ore * 3600) + (minuti * 60));
		String strSecondi = padLeft("" + secondi, 2, "0");

		strResult = strOre + ":" + strMinuti + ":" + strSecondi;

		logger.info("GeneralPurpose:secondToTimetable - date-out = " + strResult);
		return strResult;
	}

	private static Logger logger = WebFrontLogger.getLogger(GeneralPurpose.class);
}
