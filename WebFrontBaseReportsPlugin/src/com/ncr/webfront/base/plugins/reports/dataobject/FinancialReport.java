package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class FinancialReport extends BaseReport<FinancialReportGroup> {
	public static final int PAGE_SIZE = 66;

	public String getXml() {
		StringBuffer sb = new StringBuffer();

		sb.append("<report>\n");
		sb.append("<numero_report>").append(getINumReport()).append("</numero_report>\n");
		sb.append("<pagina>\n");
		sb.append(getHeader().getXml());
		sb.append("<dati>\n");

		for (int i = 0; i < getRowSet().size(); i++) {
			FinancialReportGroup group = (FinancialReportGroup) getRowSet().get(i);

			logger.info(String.format("[getXml] label = [%s], code = [%s]", group.getStrLabel(), group.getStrCodice()));

			if (group.getStrCodice().equals(ReportsConstants.FIN_RIEPILOGO_GRUPPO_CODICE))
				sb.append(group.getXml_DOUBLE()); // vecchia versione
			else
				sb.append(group.getXml_DOUBLE());
		}

		sb.append("<riepilogo>\n");

		for (int j = 0; j < getGruppiRiepilogo().size(); j++) {
			FinancialReportGroup group = (FinancialReportGroup) getGruppiRiepilogo().get(j);

			sb.append("<").append(group.getStrCodice()).append(">\n");
			sb.append("<data>").append(getStrDataReport()).append("</data>\n");
			sb.append("<label>").append(group.getStrLabel()).append("</label>\n");

			for (int i = 0; i < group.getElements().size(); i++) {
				FinancialReportItem objElemento = (FinancialReportItem) group.getElements().get(i);

				String strFinancialTotalId = "" + objElemento.getIntFinancialTotalId();
				if (strFinancialTotalId.substring(2).equals("01") || strFinancialTotalId.substring(2).equals("03")
						|| strFinancialTotalId.substring(2).equals("05") || strFinancialTotalId.substring(2).equals("09")) {
					sb.append(objElemento.getXml());
				}
			}
			sb.append("</").append(group.getStrCodice()).append(">\n");
		}

		sb.append("</riepilogo>\n");
		sb.append("</dati>\n");
		sb.append("</pagina>\n");
		sb.append("</report>\n");
		logger.trace(String.format("[getXml] sb = [%s]", sb));

		return sb.toString();
	}

	public void processMeanValues(String strDate) {
		double dblVendite = 0;
		double dblClienti = 0;
		double dblArticoli = 0;
		double dblTempoTot = 0;
		double dblTempoReg = 0;

		List<Boolean> flag = new ArrayList<>();
		flag.add(new Boolean("false"));
		flag.add(new Boolean("false"));
		flag.add(new Boolean("false"));
		flag.add(new Boolean("true"));
		flag.add(new Boolean("true"));
		flag.add(new Boolean("true"));

		int indexValoriMedi = 0;

		for (int i = 0; i < getRowSet().size(); i++) {
			FinancialReportGroup objCurrentGroup = (FinancialReportGroup) getRowSet().get(i);

			if (objCurrentGroup.getStrLabel().equals(ReportsConstants.FIN_VENDMERC_STAT_LABEL)
					&& objCurrentGroup.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGASX_JASPER)) {
				FinancialReportItem objCurrentElement = (FinancialReportItem) objCurrentGroup.getElements().get(0);
				dblVendite = objCurrentElement.getDblValore3();
				dblClienti = objCurrentElement.getIntValore1();
				dblArticoli = objCurrentElement.getIntValore2();
			} else if (objCurrentGroup.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)
					&& objCurrentGroup.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGASX_JASPER)) {

				for (int j = 0; j < objCurrentGroup.getElements().size(); j++) {
					FinancialReportItem objCurrentElement = (FinancialReportItem) objCurrentGroup.getElements().get(j);

					if (objCurrentElement.getIntFinancialTotalId() == Integer.parseInt(ReportsConstants.PRODUTTIVITA_FINANCIALTOTALID_TTOTALE)) {
						dblTempoTot = objCurrentElement.getDblValore3() / 3600;
					}

					if (objCurrentElement.getIntFinancialTotalId() == Integer.parseInt(ReportsConstants.PRODUTTIVITA_FINANCIALTOTALID_TSCANNER)) {
						dblTempoReg = objCurrentElement.getDblValore3() / 60;
					}
				}
			} else if (objCurrentGroup.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)
					&& objCurrentGroup.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER)) {
				indexValoriMedi = i;
			}
		}
		FinancialReportGroup objNewGroup = new FinancialReportGroup();

		objNewGroup.setStrCodice(ReportsConstants.FIN_PROD_MEDI_CODICE);
		objNewGroup.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objNewGroup.setStrData(strDate);
		objNewGroup.setStrTipoRigaGruppo(ReportsConstants.TIPOGRUPPO_RIGADX_JASPER);

		double dblTmp = 0;
		if (dblClienti != 0) {
			dblTmp = dblVendite / dblClienti;
		}
		FinancialReportItem objNewElemento = new FinancialReportItem();
		objNewElemento.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objNewElemento.setStrTipo("X");
		objNewElemento.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
		objNewElemento.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
		objNewElemento.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);
		objNewElemento.setStrNome("Average Sales/Customers");
		objNewElemento.setDblValore3(dblTmp);
		objNewElemento.setStrGroupLabel(objNewGroup.getStrLabel());
		objNewElemento.setFlags(flag);
		objNewGroup.getElements().add(objNewElemento);

		dblTmp = 0;
		if (dblArticoli != 0) {
			dblTmp = dblVendite / dblArticoli;
		}
		objNewElemento = new FinancialReportItem();
		objNewElemento.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objNewElemento.setStrTipo("X");
		objNewElemento.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
		objNewElemento.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
		objNewElemento.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);
		objNewElemento.setStrTipoRigaGruppo(objNewGroup.getStrTipoRigaGruppo());
		objNewElemento.setStrNome("Average Sales Price");
		objNewElemento.setDblValore3(dblTmp);
		objNewElemento.setStrGroupLabel(objNewGroup.getStrLabel());
		objNewElemento.setFlags(flag);
		objNewGroup.getElements().add(objNewElemento);

		dblTmp = 0;
		if (dblTempoTot != 0) {
			dblTmp = dblVendite / dblTempoTot;
		}
		objNewElemento = new FinancialReportItem();
		objNewElemento.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objNewElemento.setStrTipo("X");
		objNewElemento.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
		objNewElemento.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
		objNewElemento.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);
		objNewElemento.setStrTipoRigaGruppo(objNewGroup.getStrTipoRigaGruppo());
		objNewElemento.setStrNome("Average Sales/Hour");
		objNewElemento.setDblValore3(dblTmp);
		objNewElemento.setStrGroupLabel(objNewGroup.getStrLabel());
		objNewElemento.setFlags(flag);
		objNewGroup.getElements().add(objNewElemento);

		dblTmp = 0;
		if (dblClienti != 0) {
			dblTmp = dblArticoli / dblClienti;
		}
		objNewElemento = new FinancialReportItem();
		objNewElemento.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objNewElemento.setStrTipo("X");
		objNewElemento.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
		objNewElemento.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
		objNewElemento.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);
		objNewElemento.setStrTipoRigaGruppo(objNewGroup.getStrTipoRigaGruppo());
		objNewElemento.setStrNome("Items per Customer");
		objNewElemento.setDblValore3(dblTmp);
		objNewElemento.setStrGroupLabel(objNewGroup.getStrLabel());
		objNewElemento.setFlags(flag);
		objNewGroup.getElements().add(objNewElemento);

		dblTmp = 0;
		if (dblTempoReg != 0) {
			dblTmp = dblArticoli / dblTempoReg;
		}
		objNewElemento = new FinancialReportItem();
		objNewElemento.setStrLabel(ReportsConstants.FIN_PROD_MEDI_LABEL);
		objNewElemento.setStrTipo("X");
		objNewElemento.setStrLabelArticoli(ReportsConstants.FIN_GENERALE_ARTICOLI_LABEL);
		objNewElemento.setStrLabelClienti(ReportsConstants.FIN_GENERALE_CLIENTI_LABEL);
		objNewElemento.setStrLabelImporto(ReportsConstants.FIN_GENERALE_IMPORTO_LABEL);
		objNewElemento.setStrTipoRigaGruppo(objNewGroup.getStrTipoRigaGruppo());
		objNewElemento.setStrNome("Average Items/T.Reg");
		objNewElemento.setDblValore3(dblTmp);
		objNewElemento.setStrGroupLabel(objNewGroup.getStrLabel());
		objNewElemento.setFlags(flag);
		objNewGroup.getElements().add(objNewElemento);

		getRowSet().add(indexValoriMedi, objNewGroup);
	}

	public void hourlyProductivity() {
		for (int i = 0; i < getRowSet().size(); i++) {
			FinancialReportGroup objGruppo = (FinancialReportGroup) getRowSet().get(i);

			if (objGruppo.getStrLabel().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)
					&& objGruppo.getStrTipoRigaGruppo().equals(ReportsConstants.TIPOGRUPPO_RIGASX_JASPER)) {
				objGruppo.processProductivity();
			}
		}
	}

	public List<FinancialReportGroup> getGruppiRiepilogo() {
		return gruppiRiepilogo;
	}

	public void setGruppiRiepilogo(List<FinancialReportGroup> vctGruppiRiepilogo) {
		this.gruppiRiepilogo = vctGruppiRiepilogo;
	}

	public boolean isRealFinanziario() {
		return realFinanziario;
	}

	public void setRealFinanziario(boolean realFinanziario) {
		this.realFinanziario = realFinanziario;
	}

	public String getStrFiliale() {
		return strFiliale;
	}

	public void setStrFiliale(String strFiliale) {
		this.strFiliale = strFiliale;
	}

	public String getStrDataReport() {
		return strDataReport;
	}

	public void setStrDataReport(String strDataReport) {
		this.strDataReport = strDataReport;
	}

	public boolean isFlagList() {
		return flagList;
	}

	public void setFlagList(boolean flagList) {
		this.flagList = flagList;
	}

	public int getRowCounter() {
		return rowCounter;
	}

	public void setRowCounter(int rowCounter) {
		this.rowCounter = rowCounter;
	}

	public int getPageCounter() {
		return pageCounter;
	}

	public void setPageCounter(int pageCounter) {
		this.pageCounter = pageCounter;
	}

	public boolean isOffLine() {
		return offLine;
	}

	public void setOffLine(boolean offLine) {
		this.offLine = offLine;
	}

	public int getINumReport() {
		return iNumReport;
	}

	public void setINumReport(int iNumReport) {
		this.iNumReport = iNumReport;
	}

	private Logger logger = WebFrontLogger.getLogger(FinancialReport.class);
	private List<FinancialReportGroup> gruppiRiepilogo;
	private boolean realFinanziario;
	private String strFiliale;
	private String strDataReport;
	private boolean flagList;
	private int rowCounter = 0;
	private int pageCounter = 1;
	private int iNumReport = 1;
	private boolean offLine = false;

}
