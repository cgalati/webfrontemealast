package com.ncr.webfront.base.plugins.reports.dataobject;

import java.util.List;

import com.ncr.webfront.base.plugins.reports.util.GeneralPurpose;
import com.ncr.webfront.base.plugins.reports.util.ReportsConstants;

public class FinancialReportDoubleElement extends DataObject {
	public String getXml() {
		StringBuilder sb = new StringBuilder();

		sb.append("<elemento>\n");
		sb.append("<label>").append(getStrLabel()).append("</label>\n");
		sb.append(getXml_1());
		sb.append(getXml_2());
		sb.append("</elemento>\n");
		
		return sb.toString();
	}

	private String getXml_1() {
		StringBuilder sb = new StringBuilder();

		sb.append("<label_1>").append(getStrLabel_1()).append("</label_1>\n");
		sb.append("<label_clienti_1>").append(getStrLabelClienti_1()).append("</label_clienti_1>\n");
		sb.append("<label_articoli_1>").append(getStrLabelArticoli_1()).append("</label_articoli_1>\n");
		sb.append("<label_importo_1>").append(getStrLabelImporto_1()).append("</label_importo_1>\n");
		sb.append("<tipo_1>").append(getStrTipo_1()).append("</tipo_1>\n");
		if (getStrTipo_1() == null || getStrTipo_1().equals("H")) {
			sb.append("<nome_1></nome_1>\n");
			sb.append("<totalid_1></totalid_1>\n").append("<perc_1></perc_1>\n").append("<clienti_1></clienti_1>\n");
			sb.append("<artic_1></artic_1>\n").append("<importo_1></importo_1>\n");
			return sb.toString();
		}
		sb.append("<nome_1>").append(getStrNome_1()).append("</nome_1>\n");

		String strFinancialTotalId = "" + getIntFinancialTotalId_1();
		if (getIntFinancialTotalId_1() > 0) {
			strFinancialTotalId = strFinancialTotalId.substring(2);
		}
		sb.append("<totalid_1>").append(strFinancialTotalId).append("</totalid_1>\n");

		String strTempPerc = GeneralPurpose.formatNumber(Double.toString(getDblPerc_1()), "##0.##");
		sb.append("<perc_1>").append(strTempPerc).append("</perc_1>\n");

		if (((Boolean) getVctFlags_1().get(ReportsConstants.FLAGINFO_PRINTTRANSACTION)).booleanValue()) {
			if (((Boolean) getVctFlags_1().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getIntValore1_1() == 0)) {
				sb.append("<clienti_1>0</clienti_1>\n");
			} else {
				sb.append("<clienti_1>").append(getIntValore1_1()).append("</clienti_1>\n");
			}
		} else {
			sb.append("<clienti_1>0</clienti_1>\n");
		}

		if (((Boolean) getVctFlags_1().get(ReportsConstants.FLAGINFO_PRINTITEM)).booleanValue()) {
			if (((Boolean) getVctFlags_1().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getIntValore2_1() == 0)) {
				sb.append("<artic_1>0</artic_1>\n");
			} else {
				sb.append("<artic_1>").append(getIntValore2_1()).append("</artic_1>\n");
			}
		} else {
			sb.append("<artic_1>0</artic_1>\n");
		}

		if ((getVctFlags_1().get(ReportsConstants.FLAGINFO_PRINTAMOUNT)).booleanValue()) {
			if ((getVctFlags_1().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getDblValore3_1() == 0)) {
				sb.append("<importo_1>.00</importo_1>\n");
			} else if (getStrGroupLabel_1().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)) {
				String strTemp = GeneralPurpose.formatNumber(Double.toString(getDblValore3_1()), "#####0");
				sb.append("<importo_1>").append(GeneralPurpose.secondToTimetable(new Integer(strTemp).intValue())).append("</importo_1>\n");
			} else {
				sb.append("<importo_1>").append(GeneralPurpose.formatNumber(Double.toString(getDblValore3_1()), "#,###.00")).append("</importo_1>\n");
			}
		} else {
			sb.append("<importo_1>.00</importo_1>\n");
		}
		
		return sb.toString();
	}

	private String getXml_2() {
		StringBuilder sb = new StringBuilder();

		sb.append("<label_2>").append(getStrLabel_2()).append("</label_2>\n");
		sb.append("<label_clienti_2>").append(getStrLabelClienti_2()).append("</label_clienti_2>\n");
		sb.append("<label_articoli_2>").append(getStrLabelArticoli_2()).append("</label_articoli_2>\n");
		sb.append("<label_importo_2>").append(getStrLabelImporto_2()).append("</label_importo_2>\n");
		sb.append("<tipo_2>").append(getStrTipo_2()).append("</tipo_2>\n");
		if (getStrTipo_2() == null || getStrTipo_2().equals("H")) {
			sb.append("<nome_2></nome_2>\n");
			sb.append("<totalid_2></totalid_2>\n").append("<perc_2></perc_2>\n").append("<clienti_2></clienti_2>\n");
			sb.append("<artic_2></artic_2>\n").append("<importo_2></importo_2>\n");
			return sb.toString();
		}
		sb.append("<nome_2>").append(getStrNome_2()).append("</nome_2>\n");

		String strFinancialTotalId = "" + getIntFinancialTotalId_2();
		if (getIntFinancialTotalId_2() > 0) {
			strFinancialTotalId = strFinancialTotalId.substring(2);
		}
		sb.append("<totalid_2>").append(strFinancialTotalId).append("</totalid_2>\n");

		String strTempPerc = GeneralPurpose.formatNumber(Double.toString(getDblPerc_2()), "##0.##");
		sb.append("<perc_2>").append(strTempPerc).append("</perc_2>\n");

		if (((Boolean) getVctFlags_2().get(ReportsConstants.FLAGINFO_PRINTTRANSACTION)).booleanValue()) {
			if (((Boolean) getVctFlags_2().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getIntValore1_2() == 0)) {
				sb.append("<clienti_2>0</clienti_2>\n");
			} else {
				sb.append("<clienti_2>").append(getIntValore1_2()).append("</clienti_2>\n");
			}
		} else {
			sb.append("<clienti_2>0</clienti_2>\n");
		}

		if (((Boolean) getVctFlags_2().get(ReportsConstants.FLAGINFO_PRINTITEM)).booleanValue()) {
			if (((Boolean) getVctFlags_2().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getIntValore2_2() == 0)) {
				sb.append("<artic_2>0</artic_2>\n");
			} else {
				sb.append("<artic_2>").append(getIntValore2_2()).append("</artic_2>\n");
			}
		} else {
			sb.append("<artic_2>0</artic_2>\n");
		}

		if (((Boolean) getVctFlags_2().get(ReportsConstants.FLAGINFO_PRINTAMOUNT)).booleanValue()) {
			if (((Boolean) getVctFlags_2().get(ReportsConstants.FLAGINFO_PRINTNOTZERO)).booleanValue() && (getDblValore3_2() == 0)) {
				sb.append("<importo_2>.00</importo_2>\n");
			//} else if (getStrGroupLabel_2().equals(ReportsConstants.FIN_PROD_MEDI_LABEL)) {
			//	String strTemp = GeneralPurpose.formatNumber(Double.toString(getDblValore3_2()), "#####0");
			//	sb.append("<importo_2>").append(GeneralPurpose.secondToTimetable(new Integer(strTemp).intValue())).append("</importo_2>\n");
			} else {
				sb.append("<importo_2>").append(GeneralPurpose.formatNumber(Double.toString(getDblValore3_2()), "#,###.00")).append("</importo_2>\n");
			}
		} else {
			sb.append("<importo_2>.00</importo_2>\n");
		}
		
		return sb.toString();
	}
	
	public String getStrLabel() {
		return strLabel;
	}

	public void setStrLabel(String strLabel) {
		this.strLabel = strLabel;
	}

	public String getStrLabel_1() {
		return strLabel_1;
	}

	public void setStrLabel_1(String strLabelIn_1) {
		this.strLabel_1 = strLabelIn_1;
	}

	public String getStrLabel_2() {
		return strLabel_2;
	}

	public void setStrLabel_2(String strLabelIn_2) {
		this.strLabel_2 = strLabelIn_2;
	}
	
	public String getStrLabelClienti_1() {
		return strLabelClienti_1;
	}

	public void setStrLabelClienti_1(String strLabelClienti_1) {
		this.strLabelClienti_1 = strLabelClienti_1;
	}

	public String getStrLabelClienti_2() {
		return strLabelClienti_2;
	}

	public void setStrLabelClienti_2(String strLabelClienti_2) {
		this.strLabelClienti_2 = strLabelClienti_2;
	}

	public String getStrLabelArticoli_1() {
		return strLabelArticoli_1;
	}

	public void setStrLabelArticoli_1(String strLabelArticoli_1) {
		this.strLabelArticoli_1 = strLabelArticoli_1;
	}

	public String getStrLabelArticoli_2() {
		return strLabelArticoli_2;
	}

	public void setStrLabelArticoli_2(String strLabelArticoli_2) {
		this.strLabelArticoli_2 = strLabelArticoli_2;
	}

	public String getStrLabelImporto_1() {
		return strLabelImporto_1;
	}

	public void setStrLabelImporto_1(String strLabelImporto_1) {
		this.strLabelImporto_1 = strLabelImporto_1;
	}

	public String getStrLabelImporto_2() {
		return strLabelImporto_2;
	}

	public void setStrLabelImporto_2(String strLabelImporto_2) {
		this.strLabelImporto_2 = strLabelImporto_2;
	}

	public String getStrNumProg() {
		return strNumProg;
	}

	public void setStrNumProg(String strNumProg) {
		this.strNumProg = strNumProg;
	}

	public String getStrTipo_1() {
		return strTipo_1;
	}

	public void setStrTipo_1(String strTipoIn_1) {
		this.strTipo_1 = strTipoIn_1;
	}

	public String getStrTipo_2() {
		return strTipo_2;
	}

	public void setStrTipo_2(String strTipoIn_2) {
		this.strTipo_2 = strTipoIn_2;
	}

	public String getStrNome_1() {
		return strNome_1;
	}

	public void setStrNome_1(String strNomeIn_1) {
		this.strNome_1 = strNomeIn_1;
	}

	public String getStrNome_2() {
		return strNome_2;
	}

	public void setStrNome_2(String strNomeIn_2) {
		this.strNome_2 = strNomeIn_2;
	}

	public int getIntValore1_1() {
		return intValore1_1;
	}

	public void setIntValore1_1(int intValoreIn1_1) {
		this.intValore1_1 = intValoreIn1_1;
	}

	public int getIntValore1_2() {
		return intValore1_2;
	}

	public void setIntValore1_2(int intValoreIn1_2) {
		this.intValore1_2 = intValoreIn1_2;
	}

	public int getIntValore2_1() {
		return intValore2_1;
	}

	public void setIntValore2_1(int intValoreIn2_1) {
		this.intValore2_1 = intValoreIn2_1;
	}

	public int getIntValore2_2() {
		return intValore2_2;
	}

	public void setIntValore2_2(int intValoreIn2_2) {
		this.intValore2_2 = intValoreIn2_2;
	}

	public double getDblValore3_1() {
		return dblValore3_1;
	}

	public void setDblValore3_1(double dblValoreIn3_1) {
		this.dblValore3_1 = dblValoreIn3_1;
	}

	public double getDblValore3_2() {
		return dblValore3_2;
	}

	public void setDblValore3_2(double dblValoreIn3_2) {
		this.dblValore3_2 = dblValoreIn3_2;
	}

	public String getStrCassa() {
		return strCassa;
	}

	public void setStrCassa(String strCassa) {
		this.strCassa = strCassa;
	}

	public String getStrCassiere() {
		return strCassiere;
	}

	public void setStrCassiere(String strCassiere) {
		this.strCassiere = strCassiere;
	}

	public int getIntFinancialTotalId_1() {
		return intFinancialTotalId_1;
	}

	public void setIntFinancialTotalId_1(int intFinancialTotalIdIn_1) {
		this.intFinancialTotalId_1 = intFinancialTotalIdIn_1;
	}

	public int getIntFinancialTotalId_2() {
		return intFinancialTotalId_2;
	}

	public void setIntFinancialTotalId_2(int intFinancialTotalIdIn_2) {
		this.intFinancialTotalId_2 = intFinancialTotalIdIn_2;
	}

	public String getStrGroupLabel_1() {
		return strGroupLabel_1;
	}

	public void setStrGroupLabel_1(String strGroupLabelIn_1) {
		this.strGroupLabel_1 = strGroupLabelIn_1;
	}

	public String getStrGroupLabel_2() {
		return strGroupLabel_2;
	}

	public void setStrGroupLabel_2(String strGroupLabelIn_2) {
		this.strGroupLabel_2 = strGroupLabelIn_2;
	}

	public double getDblPerc_1() {
		return dblPerc_1;
	}

	public void setDblPerc_1(double dblPercIn_1) {
		this.dblPerc_1 = dblPercIn_1;
	}

	public double getDblPerc_2() {
		return dblPerc_2;
	}

	public void setDblPerc_2(double dblPercIn_2) {
		this.dblPerc_2 = dblPercIn_2;
	}

	public List<Boolean> getVctFlags_1() {
		return vctFlags_1;
	}

	public void setVctFlags_1(List<Boolean> vctFlagsIn_1) {
		this.vctFlags_1 = vctFlagsIn_1;
	}

	public List<Boolean> getVctFlags_2() {
		return vctFlags_2;
	}

	public void setVctFlags_2(List<Boolean> vctFlagsIn_2) {
		this.vctFlags_2 = vctFlagsIn_2;
	}
	
	private String strLabel;
	private String strLabel_1;
	private String strLabel_2;
	private String strLabelClienti_1;
	private String strLabelClienti_2;
	private String strLabelArticoli_1;
	private String strLabelArticoli_2;
	private String strLabelImporto_1;
	private String strLabelImporto_2;
	private String strNumProg;
	private String strTipo_1;
	private String strTipo_2;
	private String strNome_1;
	private String strNome_2;
	private String strCassa;
	private String strCassiere;
	private int intFinancialTotalId_1;
	private int intFinancialTotalId_2;
	private List<Boolean> vctFlags_1;
	private List<Boolean> vctFlags_2;
	private int intValore1_1 = 0;
	private int intValore2_1 = 0;

	private int intValore1_2 = 0;
	private int intValore2_2 = 0;

	private String strGroupLabel_1 = "";
	private String strGroupLabel_2 = "";
	private double dblValore3_1;
	private double dblValore3_2;
	private double dblPerc_1;
	private double dblPerc_2;
}
