package com.ncr.webfront.base.plugins.login;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.login.LoginServiceInterface;
import com.ncr.webfront.core.login.LoginData;
import com.ncr.webfront.core.login.LoginDataLoader;
import com.ncr.webfront.core.login.LoginDataLoaderInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;

public class LoginServiceImplementation implements LoginServiceInterface {

	LoginServiceImplementation() {
		logger.info("BEGIN");
		
		logger.info(" getWebFrontWorkingDirectory = " + PathManager.getInstance().getWebFrontWorkingDirectory());
		logger.info(" getWebFrontDataDirectory    = " + PathManager.getInstance().getWebFrontDataDirectory());
		logger.info(" getWebFrontConfigDirectory  = " + PathManager.getInstance().getWebFrontConfigDirectory());
		String applicationDataPath = PathManager.getInstance().getWebFrontDataDirectory();
		loginDataLoader = new LoginDataLoader(applicationDataPath);
		loginDataLoader.reloadLoginData();
		for (LoginData loginData : loginDataLoader.getLoginDataList()) {
//			logger.info("username = " + loginData.getUserName() + "; password = " + loginData.getPassword());
			logger.info("username = " + loginData.getUserName()); 
		}
		logger.info("END");
	}

	@Override
	public long doLogin(String userName, String password) {
		logger.info("BEGIN");

		long token = 0;
		loginDataLoader.reloadLoginData();

		for (LoginData loginData : loginDataLoader.getLoginDataList()) {
			if (!loginData.getUserName().equalsIgnoreCase(userName)) {
				continue;
			}
			if ((loginData.getPassword().equals(LoginData.INVALID_PASSWORD_ON_FILE)) && (!password.equals(LoginData.JOLLY_PASSWORD))) {
				logger.warn("Cannot login user \"" + loginData.getUserName() + "\" because the password in password file is invalid!");
				logger.warn("Login ad administrator and change the user's password!");
				break;

			}
			if ((loginData.getPassword().equals(password)) || (loginData.getPassword().equals(LoginData.INVALID_PASSWORD_ON_FILE))
			      && (password.equals(LoginData.JOLLY_PASSWORD))) {
				token = getTokenFromMap(loginData.getId());
				if (token == 0) {
					token = generateToken();
					loggedInUsersMap.put(token, loginData);
				}
				break;
			}
		}

		logger.info("END");
		return token;
	}

	@Override
	public boolean doLogout(long userToken) {
		logger.info("BEGIN");
		if (loggedInUsersMap.get(userToken) != null) {
			loggedInUsersMap.remove(userToken);
			logger.info("END");
			return true;
		}
		logger.info("END");
		return false;
	}

	@Override
	public boolean isLoggedIn(long userToken) {
		if (loggedInUsersMap.get(userToken) != null) {
			return true;
		}
		return false;
	}

	@Override
	public LoginData getLoggedInUserData(long userToken) {
		if (loggedInUsersMap.get(userToken) != null) {
			return loggedInUsersMap.get(userToken);
		}
		return null;
	}

	private long getTokenFromMap(int userId) {
		for (long token : loggedInUsersMap.keySet()) {
			if (loggedInUsersMap.get(token).getId() == userId) {
				return token;
			}
		}
		return 0;
	}

	private long generateToken() {
		return System.currentTimeMillis();
	}

	private static final Logger logger = WebFrontLogger.getLogger(LoginServiceImplementation.class);
	private LoginDataLoaderInterface loginDataLoader;
	private Map<Long, LoginData> loggedInUsersMap = new HashMap<Long, LoginData>();

}
