package com.ncr.webfront.base.plugins.invoicecreditnote.internal.invoice;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType.INVOICE_DOCUMENT_TYPE;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentDetail;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSale;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.BasicPersistence;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.SellAsDepartmentUtils;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.utils.fiscal.PriceHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class InvoiceCreator extends BasicPersistence {
	public InvoiceCreator(EntityManagerFactory factory, ArticleServiceInterface articleService, PosDepartmentServiceInterface departmentService, VatServiceInterface vatService) {
		this.factory = factory;
		this.articleService = articleService;
		this.departmentService = departmentService;
		this.vatService = vatService;
	}
	
	public Document execute(Receipt receipt) {
		Document invoice = new Document();

		invoice.setDocumentType(getDocumentTypeByType(INVOICE_DOCUMENT_TYPE));
		invoice.setReceiptDate(receipt.getDate());
		invoice.setReceiptNumber(receipt.getReceiptNumber());
		invoice.setReceiptStore(receipt.getStore());
		invoice.setReceiptTerminal(receipt.getPosTerminalCode());
		
		for (ReceiptSale item : receipt.getSaleReceiptItems()) {
			DocumentDetail detail = getDocumentDetailWithEan(invoice.getDocumentDetails(), item.getItemCode());
			
			log.debug(String.format("ean already inserted? [%b]", detail != null));
			
			invoice.getDocumentDetails().add(createDocumentDetail(item));
		}
		
		return invoice;
	}
	
	private DocumentDetail getDocumentDetailWithEan(List<DocumentDetail> details, String ean) {
		for (DocumentDetail detail : details) {
			if (detail.getItemCode().equals(ean)) {
				return detail;
			}
		}
		
		return null;
	}
	
	private DocumentDetail createDocumentDetail(ReceiptSale item) {
		DocumentDetail detail = new DocumentDetail();
		Article itemArticle = SellAsDepartmentUtils.getArticle(articleService, item.getItemCode());
		int vatRate = SellAsDepartmentUtils.getVatRate(vatService, itemArticle);
		
		try {
			PropertyUtils.copyProperties(detail, item);
		} catch (Exception e) {
			// Unreachable.
		}
		
		detail.setDepartment(SellAsDepartmentUtils.getDepartmentDescription(departmentService, itemArticle,
				item.getDepartmentId()));
		detail.setVatRate(vatRate);
		detail.setNetPrice(PriceHelper.nettify(detail.getPrice(), vatRate));
		detail.setNetDiscount(PriceHelper.nettify(detail.getDiscount(), vatRate));
		detail.setNetAmount(PriceHelper.nettify(detail.getAmount(), vatRate));
		detail.setDiscountExempt(SellAsDepartmentUtils.getDiscountExempt(itemArticle));
		
		log.debug(String.format("detail [%s], item [%s]", detail, item));
		
		return detail;
	}
	
	private ArticleServiceInterface articleService;
	private PosDepartmentServiceInterface departmentService;
	private VatServiceInterface vatService;
	
	private static final Logger log = WebFrontLogger.getLogger(InvoiceCreator.class);
}
