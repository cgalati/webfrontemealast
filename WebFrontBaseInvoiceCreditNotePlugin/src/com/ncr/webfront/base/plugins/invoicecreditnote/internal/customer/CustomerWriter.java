package com.ncr.webfront.base.plugins.invoicecreditnote.internal.customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerType;
import com.ncr.webfront.base.invoicecreditnote.customer.Exemption;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public abstract class CustomerWriter {
	public CustomerWriter(EntityManagerFactory factory, CustomerServiceInterface customerService) {
		this.factory = factory;
		this.customerService = customerService;
	}
	
	public Integer execute(Customer customerToWrite) {
		if (isNotValid(customerToWrite)) {
			throw new RuntimeException("Invaid data was entered");
		}
		if (alreadyExists(customerToWrite)) {
			throw new RuntimeException("TRN# or fiscal code already used");			
		}
		
		return perform(customerToWrite);
	}
	
	private boolean isNotValid(Customer customerToValidate) {
		for (WebFrontValidationData validation : customerService.getCustomerValidationData()) {
			if (!validation.validate(customerToValidate).isEmpty()) {
				return true;
			}
		}
		
		return false;
	}
	
	protected abstract boolean alreadyExists(final Customer customer);
	
	protected abstract Integer perform(Customer customerToWrite);
	
	protected void attachDetached(Customer customerToWrite, EntityManager manager) {
		customerToWrite.setCustomerType(manager.find(CustomerType.class, customerToWrite.getCustomerType().getId()));
		customerToWrite.setExemption(manager.find(Exemption.class, customerToWrite.getExemption().getId()));
	}
	
	protected EntityManagerFactory factory;
	private CustomerServiceInterface customerService;
}
