package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class DocumentCanceller extends BasicPersistence {
	public DocumentCanceller(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	public void execute(final Document document) {
		if (isCreated(document)) {
			DocumentStatus canceled = getDocumentStatusByStatus(CANCELED_DOCUMENT_STATUS);
			
			document.setDocumentStatus(canceled);
			HibernateHelper.transact(factory, new Visitor<Void, EntityManager>() {
				public Void visit(EntityManager manager) {
					manager.merge(document);
					
					return null;
				}
			});
		} else {
			log.debug(String.format("document not cancellable: status [%s]", document.getDocumentStatus()));
		}
	}
	
	private static boolean isCreated(Document document) {
		return (document.getDocumentStatus() != null) && document.getDocumentStatus().getStatus().equalsIgnoreCase(CREATED_DOCUMENT_STATUS);
	}
	
	private static final Logger log = WebFrontLogger.getLogger(DocumentCanceller.class);
}
