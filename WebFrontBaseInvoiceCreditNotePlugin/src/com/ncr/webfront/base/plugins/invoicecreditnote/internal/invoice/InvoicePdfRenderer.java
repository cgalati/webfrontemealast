package com.ncr.webfront.base.plugins.invoicecreditnote.internal.invoice;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.plugins.invoicecreditnote.InvoiceCreditNoteMappingProperties;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentPdfRenderer;

public class InvoicePdfRenderer extends DocumentPdfRenderer {
	protected InvoicePdfRenderer(Document generatedDocument) {
		super(InvoiceCreditNoteMappingProperties.invoiceJasper, generatedDocument);
	}
}
