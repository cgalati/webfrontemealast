package com.ncr.webfront.base.plugins.invoicecreditnote.internal;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.IdcDetail;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSale;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSearchType;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.SellAsDepartmentUtils;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class ReceiptFiller {
	public ReceiptFiller(EntityManagerFactory factory, ArticleServiceInterface articleService, PosDepartmentServiceInterface departmentService) {
		this.factory = factory;
		this.articleService = articleService;
		this.departmentService = departmentService;
	}
	
	public Receipt execute(final ReceiptSearchType receiptSearchType, Receipt receipt) {
		List<IdcDetail> details = HibernateHelper.transactList(factory, new Visitor<List<IdcDetail>, EntityManager>() {
			@Override
			public List<IdcDetail> visit(EntityManager manager) {
				TypedQuery<IdcDetail> q = manager.createQuery(
						"from IdcDetail where reg = :reg and trans = :trans and recordType = :recordType",
						IdcDetail.class);
				
				q.setParameter("reg", Integer.parseInt(receiptSearchType.getPosTerminalCode()));
				q.setParameter("trans", receiptSearchType.getReceiptNumber());
				q.setParameter("recordType", "S");
				
				return q.getResultList();
			}
		});
		
		log.debug(String.format("[%d] S records for reg [%s] and trans [%s]", details.size(),
				receiptSearchType.getPosTerminalCode(), receiptSearchType.getReceiptNumber()));
		
		for (IdcDetail detail : details) {
			addDetail(receipt.getSaleReceiptItems(), detail);
		}
		
		return receipt;
	}
	
	private void addDetail(List<ReceiptSale> details, IdcDetail detail) {
		ReceiptSale oldItem = null;
		
		for (ReceiptSale receiptDetail : details) {
			if (receiptDetail.getItemCode().equals(detail.getMisc().trim())) {
				oldItem = receiptDetail;
			}
		}
		
		ReceiptSale item = populateItem(detail);
		
		if (oldItem == null) {
			details.add(item);
		} else {
			// Collapses a IdcDetail's data into an already existing object with the same ean.
			log.debug(String.format("oldItem [%s], item [%s]", oldItem, item));
			
			oldItem.setAmount(oldItem.getAmount() + item.getAmount());
			oldItem.setDiscount(oldItem.getDiscount() + item.getDiscount());
			oldItem.setQuantity(oldItem.getQuantity() + item.getQuantity());
			
			log.debug(String.format("afterwards, item [%s]", item));
			
			if (oldItem.getQuantity() == 0) {
				log.debug("removing an item completly annulled or reversed");
				
				details.remove(oldItem);
			}
		}
	}
	
	private ReceiptSale populateItem(IdcDetail detail) {
		ReceiptSale item = new ReceiptSale();
		String itemCode = detail.getMisc().trim();
		log.debug("Invoice receip line: itemCode for article : " + itemCode);
		Article article = SellAsDepartmentUtils.getArticle(articleService, itemCode);
		int departmentId = detail.getUserNo();
		
		item.setSequenceNumber(detail.getSequenceNumber());
		item.setItemCode(itemCode);
		item.setDepartmentId(departmentId);
		item.setAmount(detail.getNetAmount2());
		item.setDescription(SellAsDepartmentUtils.getDepartmentDescription(departmentService, article, departmentId));
		item.setDiscount(detail.getItemDiscountAmount() + detail.getTransDiscountAmount());
		item.setMeasureUnit(SellAsDepartmentUtils.getPackagingType(article));
		item.setPrice(getPrice(detail.getData()));
		item.setQuantity(getQuantity(detail.getData()));
		item.setDecimal(SellAsDepartmentUtils.isDecimalQty(article, detail.getData()));
		item.setVatCode(detail.getVatCode());
		
		log.debug(String.format("item [%s]", item));
		
		return item;
	}
	
	private static int getPrice(String data) {
		String price = data.split("\\*")[1];
		
		log.debug(String.format("data [%s]", data));
		
		return Integer.parseInt(price);
	}
	
	private static double getQuantity(String data) {
		String quantity = data.split("\\*")[0];
		
		log.debug(String.format("data [%s]", data));
		
		if (quantity.charAt(5) == '.') {
			return Double.parseDouble(quantity);
		} else {
			return Double.parseDouble(quantity.substring(0, 5));
		}
	}
	
	private EntityManagerFactory factory;
	private ArticleServiceInterface articleService;
	private PosDepartmentServiceInterface departmentService;
	
	private static final Logger log = WebFrontLogger.getLogger(ReceiptFiller.class);
}
