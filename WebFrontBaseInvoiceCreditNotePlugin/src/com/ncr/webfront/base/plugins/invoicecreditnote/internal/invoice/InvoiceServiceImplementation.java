package com.ncr.webfront.base.plugins.invoicecreditnote.internal.invoice;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus.*;
import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType.*;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.IdcDetail;
import com.ncr.webfront.base.invoicecreditnote.invoice.InvoiceServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSearchType;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.ReceiptFiller;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.BasicPersistence;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentCanceller;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentSaverAndPdfRenderer;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class InvoiceServiceImplementation extends BasicPersistence implements InvoiceServiceInterface {
	public InvoiceServiceImplementation(EntityManagerFactory factory, ArticleServiceInterface articleService,
			PosDepartmentServiceInterface departmentService, VatServiceInterface vatService) {
		this.factory = factory;
		this.articleService = articleService;
		this.departmentService = departmentService;
		this.vatService = vatService;
	}

	@Override
	public List<Receipt> searchReceipts(ReceiptSearchType receiptSearchType) {
		
		 List<Receipt> returnReceipts = new ArrayList<Receipt>();
		 Receipt singleReceipt = new Receipt();
		 
		 final StringBuilder query = new StringBuilder("from IdcDetail where recordType = 'F' and recordCode = '100'");
		 final String filterQuery = "select receiptNumber from Document where documentStatus.status = '" + PRINTED_DOCUMENT_STATUS + "'";
		 
		 if (receiptSearchType.getPosTerminalCode() != null){	
			 query.append(" and reg = ").append(Integer.parseInt(receiptSearchType.getPosTerminalCode()));
		 }
		 
		 if (receiptSearchType.getReceiptNumber() != 0){
			 query.append(" and trans = ").append(receiptSearchType.getReceiptNumber().intValue());
		 }
		 
		 if (StringUtils.isNotBlank(receiptSearchType.getFidelityCode())) {
			 query.append(" and trim(misc) = '").append(receiptSearchType.getFidelityCode()).append("'");
		 }

		 log.info("searchReceipts - query = \""+ query.toString() + "\"");
		 List<IdcDetail> queryResults = HibernateHelper.transactList(factory, new Visitor<List<IdcDetail>, EntityManager>() {
			@Override
			public List<IdcDetail> visit(EntityManager manager) {
				List<IdcDetail> receipts = manager.createQuery(query.toString(), IdcDetail.class).getResultList();
				List<Integer> documentIds = manager.createQuery(filterQuery, Integer.class).getResultList();
				List<IdcDetail> filteredReceipts = new ArrayList<>();
				
				for (IdcDetail receipt : receipts) {
					//if (!documentIds.contains(receipt.getTrans())) {
						filteredReceipts.add(receipt);
					//}
				}
				
				return filteredReceipts;
			}
		 });
		 			
		 //mount results data
		 for (IdcDetail idcDetail:queryResults){
				 singleReceipt = new Receipt();
				 singleReceipt.setStore(idcDetail.getStore()+"");
				 singleReceipt.setPosTerminalCode(idcDetail.getReg()+"");
				 singleReceipt.setDate(idcDetail.getDdate());
				 singleReceipt.setReceiptAmount(Integer.parseInt(idcDetail.getData().substring(11)));
				 singleReceipt.setReceiptNumber(idcDetail.getTrans());
				 singleReceipt.setFidelityCode(idcDetail.getMisc().trim());
				 returnReceipts.add(singleReceipt);
		 }	 
		 return returnReceipts;	 
		
	}
	
	@Override
	public Receipt fillReceipt(final ReceiptSearchType receiptSearchType) {
		Receipt receipt = searchReceipts(receiptSearchType).get(0);
		
		return new ReceiptFiller(factory, articleService, departmentService).execute(receiptSearchType, receipt);
	}
	
	@Override
	public Document createInvoice(Receipt receipt) {
		return new InvoiceCreator(factory, articleService, departmentService, vatService).execute(receipt);
	}
	
	@Override
	public Document generateInvoice(Document invoice) {
		return new InvoiceGenerator(factory).execute(invoice);
	}
	
	@Override
	public byte[] renderHtmlInvoice(Document generatedInvoice) {
		return new InvoiceHtmlRenderer(generatedInvoice).execute();
	}
	
	@Override
	public byte[] renderPdfInvoice(Document generatedInvoice) {
		return new InvoicePdfRenderer(generatedInvoice).execute();
	}
	
	@Override
	public byte[] saveInvoiceAndRenderPdf(Document generatedInvoice) {
		return new DocumentSaverAndPdfRenderer(factory).execute(generatedInvoice, new InvoicePdfRenderer(generatedInvoice));
	}
	
	@Override
	public void cancelInvoice(Document invoice) {
		new DocumentCanceller(factory).execute(invoice);
	}
	
	@Override
	public Document getInvoiceByNumber(final int number, final boolean mayHaveCreditNote) {
		return HibernateHelper.transact(factory, new Visitor<Document, EntityManager>() {
			@Override
			public Document visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document invoice where documentNumber = :documentNumber and documentStatus.status = :status and documentType.type = :documentType"
						+ getCreditNoteCondition(mayHaveCreditNote),
						Document.class);
				
				q.setParameter("documentNumber", number);
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				q.setParameter("documentType", INVOICE_DOCUMENT_TYPE);
				
				return q.getSingleResult();
			}
		});
	}
	
	@Override
	public List<Document> getInvoicesByFiscalCode(final String fiscalCode, final boolean mayHaveCreditNote) {
		return HibernateHelper.transactList(factory, new Visitor<List<Document>, EntityManager>() {
			@Override
			public List<Document> visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document invoice where customer.fiscalCode = :fiscalCode and documentStatus.status = :status and documentType.type = :documentType"
						+ getCreditNoteCondition(mayHaveCreditNote),
						Document.class);
				
				q.setParameter("fiscalCode", fiscalCode);
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				q.setParameter("documentType", INVOICE_DOCUMENT_TYPE);
				
				return q.getResultList();
			}
		});
	}
	
	@Override
	public List<Document> getInvoicesByVatCode(final String vatCode, final boolean mayHaveCreditNote) {
		return HibernateHelper.transactList(factory, new Visitor<List<Document>, EntityManager>() {
			@Override
			public List<Document> visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document invoice where customer.vatCode = :vatCode and documentStatus.status = :status and documentType.type = :documentType"
						+ getCreditNoteCondition(mayHaveCreditNote),
						Document.class);
				
				q.setParameter("vatCode", vatCode);
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				q.setParameter("documentType", INVOICE_DOCUMENT_TYPE);
				
				return q.getResultList();
			}
		});
	}
	
	@Override
	public List<Document> getLastPrintedInvoices(final int number, final boolean mayHaveCreditNote) {
		return HibernateHelper.transactList(factory, new Visitor<List<Document>, EntityManager>() {
			@Override
			public List<Document> visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document invoice where documentStatus.status = :status and documentType.type = :documentType"
						+ getCreditNoteCondition(mayHaveCreditNote),
						Document.class);
				
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				q.setParameter("documentType", INVOICE_DOCUMENT_TYPE);
				q.setMaxResults(number);
				
				return q.getResultList();
			}
		});
	}
	
	private String getCreditNoteCondition(boolean mayHaveCreditNote) {
		if (mayHaveCreditNote) {
			return "";
		} else {
			return " and (select count(*) from Document creditNote where creditNote.referenced = invoice.id and creditNote.documentStatus.status = :status) = 0";
		}
	}
	
	private static final Logger log = WebFrontLogger.getLogger(InvoiceServiceImplementation.class);
	private ArticleServiceInterface articleService;
	private PosDepartmentServiceInterface departmentService;
	private VatServiceInterface vatService;
}
