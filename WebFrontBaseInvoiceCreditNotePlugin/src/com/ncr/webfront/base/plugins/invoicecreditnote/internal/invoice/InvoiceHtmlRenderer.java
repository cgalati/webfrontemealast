package com.ncr.webfront.base.plugins.invoicecreditnote.internal.invoice;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.plugins.invoicecreditnote.InvoiceCreditNoteMappingProperties;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentHtmlRenderer;

public class InvoiceHtmlRenderer extends DocumentHtmlRenderer {
	protected InvoiceHtmlRenderer(Document generatedDocument) {
		super(InvoiceCreditNoteMappingProperties.invoiceJasper, generatedDocument);
	}
}
