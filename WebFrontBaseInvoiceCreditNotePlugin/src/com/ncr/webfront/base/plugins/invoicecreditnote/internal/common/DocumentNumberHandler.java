package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.plugins.invoicecreditnote.InvoiceCreditNoteMappingProperties;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.sequence.SequenceProperties;

public abstract class DocumentNumberHandler extends BasicPersistence {
	protected void updateTentativeNumber(Document document) {
		updateNumber(document, new Visitor<Integer, String>() {
			@Override
			public Integer visit(String key) {
				return SequenceProperties.peekProperty(key);
			}
		});
	}
	
	protected void updateFinalNumber(Document document) {
		updateNumber(document, new Visitor<Integer, String>() {
			@Override
			public Integer visit(String key) {
				return SequenceProperties.getProperty(key);
			}
		});
	}
	
	private void updateNumber(Document document, Visitor<Integer, String> visitor) {
		String numberKey = document.getDocumentType().getType();
		int documentNumber = visitor.visit(numberKey);
		
		document.setDocumentNumber(documentNumber);
		updateCode(document);
	}
	
	private void updateCode(Document document) {
		String dateFormat = InvoiceCreditNoteMappingProperties.getInstance().getProperty(InvoiceCreditNoteMappingProperties.invoiceCodeDataFormat);
		String codeFormat = InvoiceCreditNoteMappingProperties.getInstance().getProperty(InvoiceCreditNoteMappingProperties.invoiceCodeFormat);
		
		String processedDate = new SimpleDateFormat(dateFormat).format(document.getCreationDate());
		
		String documentCode = String.format(codeFormat, document.getDocumentNumber());
		documentCode = documentCode.replace("STORE", document.getReceiptStore());
		documentCode = documentCode.replace("DATE", processedDate);
		
		log.debug(String.format("calculated code is [%s]", documentCode));
		
		document.setDocumentCode(documentCode);
	}
	
	private static final Logger log = WebFrontLogger.getLogger(DocumentNumberHandler.class);
}
