package com.ncr.webfront.base.plugins.invoicecreditnote.internal.invoice;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus.*;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentDetail;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentHelper;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;

public class InvoiceGenerator extends DocumentHelper {
	public InvoiceGenerator(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	public Document execute(final Document invoice) {
		Date now = new Date();
		int rate = invoice.getCustomer().getExemption().getRate();
		boolean facilitated = invoice.getCustomer().getExemption().isFacilitated();
		
		updateAmount(invoice, rate, facilitated);
		invoice.setCreationDate(now);
		invoice.setLastUpdate(now);
		
		updateTentativeNumber(invoice);
		
		invoice.setDocumentStatus(getDocumentStatusByStatus(CREATED_DOCUMENT_STATUS));
		
		HibernateHelper.transact(factory, new Visitor<Void, EntityManager>() {
			@Override
			public Void visit(EntityManager manager) {
				manager.persist(invoice);
				for (DocumentDetail detail : invoice.getDocumentDetails()) {
					detail.setDocumentId(invoice.getId());
					manager.persist(detail);
				}
				
				return null;
			}
		});
		
		return invoice;
	}
}
