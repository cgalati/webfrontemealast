package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;

public abstract class BasicPersistence {
	protected List<DocumentType> getAllDocumentTypes() {
		return HibernateHelper.transactList(factory, new Visitor<List<DocumentType>, EntityManager>() {
			@Override
			public List<DocumentType> visit(EntityManager manager) {
				TypedQuery<DocumentType> q = manager.createQuery("from DocumentType", DocumentType.class);
				
				return q.getResultList();
			}
		});
	}
	
	protected DocumentType getDocumentTypeByType(final String type) {
		return HibernateHelper.transact(factory, new Visitor<DocumentType, EntityManager>() {
			@Override
			public DocumentType visit(EntityManager manager) {
				TypedQuery<DocumentType> q = manager.createQuery("from DocumentType where type = :type", DocumentType.class);
				
				q.setParameter("type", type);
				
				return q.getSingleResult();
			}
		});
	}
	
	protected DocumentStatus getDocumentStatusByStatus(final String status) {
		return HibernateHelper.transact(factory, new Visitor<DocumentStatus, EntityManager>() {
			@Override
			public DocumentStatus visit(EntityManager manager) {
				TypedQuery<DocumentStatus> q = manager.createQuery("from DocumentStatus where status = :status", DocumentStatus.class);
				
				q.setParameter("status", status);
				
				return q.getSingleResult();
			}
		});
	}
	
	protected EntityManagerFactory factory;
}
