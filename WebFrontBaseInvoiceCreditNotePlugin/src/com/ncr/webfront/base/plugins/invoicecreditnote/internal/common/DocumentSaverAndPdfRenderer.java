package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus.*;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;

public class DocumentSaverAndPdfRenderer extends DocumentNumberHandler {
	public DocumentSaverAndPdfRenderer(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	public byte[] execute(final Document generatedDocument, DocumentPdfRenderer renderer) {
		generatedDocument.setLastUpdate(new Date());
		updateFinalNumber(generatedDocument);
		generatedDocument.setDocumentStatus(getDocumentStatusByStatus(PRINTED_DOCUMENT_STATUS));
		
		HibernateHelper.transact(factory, new Visitor<Void, EntityManager>() {
			@Override
			public Void visit(EntityManager manager) {
				manager.merge(generatedDocument);
				
				return null;
			}
		});
		
		return renderer.execute();
	}
}
