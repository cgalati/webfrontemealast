package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentDetail;
import com.ncr.webfront.core.utils.fiscal.PriceHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class DocumentHelper extends DocumentNumberHandler {
	protected void updateAmount(Document document, int vatRate, boolean facilitated) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal hundred = BigDecimal.valueOf(100);
		
		for (DocumentDetail detail : document.getDocumentDetails()) {
			if (facilitated && (detail.getVatRate() != (int) vatRate)) {
				BigDecimal netPrice = BigDecimal.valueOf(detail.getNetPrice());
				BigDecimal netDiscount = BigDecimal.valueOf(detail.getNetDiscount());
				BigDecimal ratePrecise = BigDecimal.valueOf(vatRate + 100).divide(hundred);
				
				BigDecimal newPrice = netPrice.multiply(ratePrecise);
				BigDecimal newDiscount = netDiscount.multiply(ratePrecise);
				BigDecimal newAmount = newPrice.multiply(BigDecimal.valueOf(detail.getQuantity())).add(newDiscount);
				
				log.debug(String.format("newPrice [%s], newDiscount [%s], newAmount [%s], ratePrecise [%s]", newPrice,
						newDiscount, newAmount, ratePrecise));
				
				detail.setPrice(PriceHelper.round(newPrice).intValue());
				detail.setDiscount(PriceHelper.round(newDiscount).intValue());
				detail.setAmount(PriceHelper.round(newAmount).intValue());
				detail.setVatRate((int) vatRate);
				
				totalAmount = totalAmount.add(newAmount);
			} else {
				totalAmount = totalAmount.add(BigDecimal.valueOf(detail.getAmount()));
			}
		}
		
		log.debug(String.format("totalAmount [%s]", totalAmount));
		
		document.setAmount(PriceHelper.round(totalAmount).intValue());
	}
	
	private static final Logger log = WebFrontLogger.getLogger(DocumentHelper.class);
}
