package com.ncr.webfront.base.plugins.invoicecreditnote.internal.customer;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerServiceInterface;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;

public class CustomerUpdater extends CustomerWriter {
	public CustomerUpdater(EntityManagerFactory factory, CustomerServiceInterface customerService) {
		super(factory, customerService);
	}
	
	@Override
	protected boolean alreadyExists(final Customer customer) {
		// It really and always already exists, but we are overwriting it (not creating a new one).
		return false;
	}
	
	@Override
	protected Integer perform(final Customer customerToAdd) {
		customerToAdd.setLastUpdate(new Date());
		
		HibernateHelper.transact(factory, new Visitor<Void, EntityManager>() {
			@Override
			public Void visit(EntityManager manager) {
				attachDetached(customerToAdd, manager);
				manager.merge(customerToAdd);
				
				return null;
			}
		});
		
		return customerToAdd.getId();
	}
}
