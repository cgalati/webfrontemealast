package com.ncr.webfront.base.plugins.invoicecreditnote.internal.creditnote;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.plugins.invoicecreditnote.InvoiceCreditNoteMappingProperties;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentPdfRenderer;

public class CreditNotePdfRenderer extends DocumentPdfRenderer {
	protected CreditNotePdfRenderer(Document generatedDocument) {
		super(InvoiceCreditNoteMappingProperties.creditNoteJasper, generatedDocument);
	}
}
