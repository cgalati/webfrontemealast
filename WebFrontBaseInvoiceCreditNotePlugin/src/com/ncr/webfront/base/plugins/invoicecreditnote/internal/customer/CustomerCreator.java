package com.ncr.webfront.base.plugins.invoicecreditnote.internal.customer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerServiceInterface;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;

public class CustomerCreator extends CustomerWriter {
	public CustomerCreator(EntityManagerFactory factory, CustomerServiceInterface customerService) {
		super(factory, customerService);
	}
	
	@Override
	protected boolean alreadyExists(final Customer customer) {
		return HibernateHelper.transact(factory, new Visitor<Long, EntityManager>() {
			@Override
			public Long visit(EntityManager manager) {
				String conditions = getConditionsOnCandidateKeys(customer);
				TypedQuery<Long> q = manager.createQuery(
						"select count(*) from Customer where " + conditions,
						Long.class);
				
				if (conditions.contains("vatCode")) {
					q.setParameter("vatCode", customer.getVatCode());
				}
				if (conditions.contains("fiscalCode")) {
					q.setParameter("fiscalCode", customer.getFiscalCode());
				}
				
				return q.getSingleResult();
			}
		}) > 0;
	}
	
	private String getConditionsOnCandidateKeys(Customer customer) {
		final List<String> conditions = new ArrayList<>();
		
		if (StringUtils.isNotBlank(customer.getVatCode())) {
			conditions.add("vatCode = :vatCode");
		}
		if (StringUtils.isNotBlank(customer.getFiscalCode())) {
			conditions.add("fiscalCode = :fiscalCode");
		}
		
		return StringUtils.join(conditions, " or ");
	}
	
	@Override
	protected Integer perform(final Customer customerToUpdate) {
		customerToUpdate.setLastUpdate(new Date());
		
		return HibernateHelper.transact(factory, new Visitor<Integer, EntityManager>() {
			@Override
			public Integer visit(EntityManager manager) {
				attachDetached(customerToUpdate, manager);
				manager.merge(customerToUpdate);
				
				return customerToUpdate.getId();
			}
		});
	}
}
