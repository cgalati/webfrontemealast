package com.ncr.webfront.base.plugins.invoicecreditnote;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.base.invoicecreditnote.sequence.SequenceServiceInterface;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.BasicPersistence;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.sequence.SequenceProperties;

public class SequenceServiceImplementation extends BasicPersistence implements SequenceServiceInterface {
	public SequenceServiceImplementation(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	@Override
	public List<MutablePair<DocumentType, Integer>> peekSequences() {
		List<DocumentType> documentTypes = getAllDocumentTypes();
		List<String> keys = new ArrayList<>();
		List<Integer> values = null;
		List<MutablePair<DocumentType, Integer>> sequences = new ArrayList<>();
		
		for (DocumentType documentType : documentTypes) {
			keys.add(documentType.getType());
		}
		
		log.debug(String.format("found [%d] document types", documentTypes.size()));
		
		values = SequenceProperties.peekProperties(keys);
		
		log.debug(String.format("peeked [%d] values", values.size()));
		
		for (int i = 0; i < keys.size(); i++) {
			MutablePair<DocumentType, Integer> sequence =
					new MutablePair<>(documentTypes.get(i), values.get(i));
			
			sequences.add(sequence);
		}
		
		return sequences;
	}

	@Override
	public void setSequence(String key, int value) {
		SequenceProperties.setProperty(key, value);
	}
	
	private static final Logger log = WebFrontLogger.getLogger(SequenceServiceImplementation.class);
}
