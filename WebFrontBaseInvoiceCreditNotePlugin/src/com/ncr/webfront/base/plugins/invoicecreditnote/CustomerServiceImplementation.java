package com.ncr.webfront.base.plugins.invoicecreditnote;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerType;
import com.ncr.webfront.base.invoicecreditnote.customer.Exemption;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.customer.CustomerCreator;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.customer.CustomerUpdater;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public class CustomerServiceImplementation implements CustomerServiceInterface {
	private static final String CUSTOMER_TYPE_COMPANY = "COMPANY";
	private static final String CUSTOMER_TYPE_PRIVATE = "PRIVATE";
	
	public CustomerServiceImplementation(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	@Override
	public Customer createDefaultCustomer() {
		Customer defaultCustomer = new Customer();
		
		defaultCustomer.getCustomerAddress().setCountry("Italia");
		defaultCustomer.setCustomerType(getAllCustomerTypes().get(0));
		defaultCustomer.setExemption(getAllExemptions().get(0));
		
		return defaultCustomer;
	}

	@Override
	public int addCustomer(final Customer customerToAdd) {
		return new CustomerCreator(factory, this).execute(customerToAdd);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return HibernateHelper.transactList(factory, new Visitor<List<Customer>, EntityManager>() {
			@Override
			public List<Customer> visit(EntityManager manager) {
				return manager.createQuery("from Customer", Customer.class).getResultList();
			}
		});
	}

	@Override
	public List<Customer> getCustomersByName(final String lastNameCompanyName) {
		return HibernateHelper.transactList(factory, new Visitor<List<Customer>, EntityManager>() {
			@Override
			public List<Customer> visit(EntityManager manager) {
				TypedQuery<Customer> q = manager.createQuery("from Customer where lower(lastNameCompanyName) like :lastNameCompanyName", Customer.class);
				
				q.setParameter("lastNameCompanyName", String.format("%%%s%%", lastNameCompanyName.toLowerCase()));
				
				return q.getResultList();
			}
		});
	}

	@Override
	public List<Customer> getCustomersByFiscalOrVatCode(final String code) {
		return HibernateHelper.transact(factory, new Visitor<List<Customer>, EntityManager>() {
			@Override
			public List<Customer> visit(EntityManager manager) {
				TypedQuery<Customer> q = manager.createQuery("from Customer where lower(fiscalCode) = :code or vatCode = :code", Customer.class);
				
				q.setParameter("code", code.toLowerCase());
				
				return q.getResultList();
			}
		});
	}
	
	@Override
	public Customer getCustomerByFidelityCode(final String fidelityCode) {
		List<Customer> customers = HibernateHelper.transactList(factory, new Visitor<List<Customer>, EntityManager>() {
			@Override
			public List<Customer> visit(EntityManager manager) {
				TypedQuery<Customer> q = manager.createQuery("from Customer where fidelityCode = :fidelityCode", Customer.class);
				
				q.setParameter("fidelityCode", fidelityCode);
				
				return q.getResultList();
			}
		});
		
		return !customers.isEmpty() ? customers.get(0) : null;
	}

	@Override
	public void updateCustomer(final Customer customerToUpdate) {
		new CustomerUpdater(factory, this).execute(customerToUpdate);
	}

	@Override
	public void deleteCustomer(final Customer customerToDelete) {
		HibernateHelper.transact(factory, new Visitor<Void, EntityManager>() {
			@Override
			public Void visit(EntityManager manager) {
				manager.remove(manager.merge(customerToDelete));
				
				return null;
			}
		});
	}

	@Override
	public List<CustomerType> getAllCustomerTypes() {
		return HibernateHelper.transactList(factory, new Visitor<List<CustomerType>, EntityManager>() {
			@Override
			public List<CustomerType> visit(EntityManager manager) {
				return manager.createQuery("from CustomerType", CustomerType.class).getResultList();
			}
		});
	}
	
	@Override
	public List<Exemption> getAllExemptions() {
		return HibernateHelper.transactList(factory, new Visitor<List<Exemption>, EntityManager>() {
			@Override
			public List<Exemption> visit(EntityManager manager) {
				return manager.createQuery("from Exemption", Exemption.class).getResultList();
			}
		});
	}

	@Override
	public List<WebFrontValidationData> getCustomerValidationData() {
		List<WebFrontValidationData> customerValidationData = new ArrayList<>();
		WebFrontValidationData company = new WebFrontValidationData("customerType.type").notBlank().is(CUSTOMER_TYPE_COMPANY);
		WebFrontValidationData notCompany = new WebFrontValidationData("customerType.type").notBlank().is(CUSTOMER_TYPE_PRIVATE);
		
		customerValidationData.add(new WebFrontValidationData("lastNameCompanyName").notBlank().length(1, 255));
		
		customerValidationData.add(new WebFrontValidationData("vatCode").notEmpty().number().length(15, 15).when(company));
		customerValidationData.add(new WebFrontValidationData("vatCode").length(0, 0).when(notCompany));
		
		customerValidationData.add(new WebFrontValidationData("fiscalCode").fiscalCode().when(company));
		customerValidationData.add(new WebFrontValidationData("fiscalCode").notBlank().fiscalCode().when(notCompany));
		
		customerValidationData.add(new WebFrontValidationData("fidelityCode").number().length(13, 13));
		customerValidationData.add(new WebFrontValidationData("customerType").notEmpty());
		
		customerValidationData.add(new WebFrontValidationData("customerAddress.street").notBlank().length(1, 255));
		customerValidationData.add(new WebFrontValidationData("customerAddress.city").notBlank().length(1, 100));
		customerValidationData.add(new WebFrontValidationData("customerAddress.country").notBlank().length(1, 100));
		customerValidationData.add(new WebFrontValidationData("customerAddress.postalCode").notEmpty().number().length(2, 5));
		
		return customerValidationData;
	}
	
	private EntityManagerFactory factory;
}
