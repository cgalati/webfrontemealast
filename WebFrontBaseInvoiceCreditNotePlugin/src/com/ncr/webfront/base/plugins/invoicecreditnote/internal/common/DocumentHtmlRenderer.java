package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.core.utils.jasperreports.JasperReportsHelper;

public class DocumentHtmlRenderer extends DocumentRenderer {
	protected DocumentHtmlRenderer(String jasperKey, Document generatedDocument) {
		super(jasperKey, generatedDocument);
	}

	public byte[] execute() {
		byte[] bytes = super.execute("html");
		
		return JasperReportsHelper.fixImagesOnAHtmlReport(bytes);
	}
	
	@Override
	protected void generate(String jasper, String output, JRDataSource dataSource) throws JRException {
		JasperRunManager.runReportToHtmlFile(jasper, output, null, dataSource);
	}
}
