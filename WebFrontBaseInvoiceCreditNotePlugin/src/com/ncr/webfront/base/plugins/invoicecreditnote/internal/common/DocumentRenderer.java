package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentDetail;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReportDocument;
import com.ncr.webfront.base.invoicecreditnote.invoice.VatDetail;
import com.ncr.webfront.base.plugins.invoicecreditnote.InvoiceCreditNoteMappingProperties;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public abstract class DocumentRenderer {
	protected DocumentRenderer(String jasperKey, Document generatedDocument) {
		this.jasper = InvoiceCreditNoteMappingProperties.getInstance().getProperty(jasperKey, generatedDocument.getCustomer(), true);
		this.generatedDocument = generatedDocument;
	}
	
	protected byte[] execute(String extension) {
		String dir = InvoiceCreditNoteMappingProperties.getInstance().getProperty(InvoiceCreditNoteMappingProperties.invoiceCreditNoteDir, true);
		
		String output = String.format("%s%s%d.%s", dir, File.separator, generatedDocument.getDocumentNumber(), extension);
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(getReportDecoratedDocument(generatedDocument)));
		
		log.debug(String.format("output [%s] for generatedDocument [%s]", output, generatedDocument));
		
		try {
			generate(jasper, output, dataSource);
			
			return read(output);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private ReportDocument getReportDecoratedDocument(Document source) {
		ReportDocument reportDocument = new ReportDocument();
		
		try {
			PropertyUtils.copyProperties(reportDocument, source);
		} catch (Exception e) {
			// Unreachable.
		}
		reportDocument.setVatCastle(getVatCastle(source));
		
		return reportDocument;
	}
	
	private List<VatDetail> getVatCastle(Document source) {
		List<VatDetail> vatDetails = new ArrayList<>();
		
		for (DocumentDetail documentDetail : source.getDocumentDetails()) {
			VatDetail vatDetail = getVatDetailByVatRate(vatDetails, documentDetail.getVatRate());			
						
			//vatDetail.setTaxable(vatDetail.getTaxable() + documentDetail.getNetAmount());
			//vatDetail.setVatAmount(vatDetail.getVatAmount() + documentDetail.getAmount() - documentDetail.getNetAmount());
			vatDetail.setGross(vatDetail.getGross() + documentDetail.getAmount());   //  totale lordo
		}
		
		for (VatDetail vatDetail : vatDetails) {  //per ogni vat
			double aliquota =  ((double) vatDetail.getVatRate() / (double) (vatDetail.getVatRate() + 100));
	        double ivaCalcolataSuArticolo = Math.ceil((double)(vatDetail.getGross() * aliquota));

	        vatDetail.setVatAmount((long)(ivaCalcolataSuArticolo));  
			vatDetail.setTaxable(vatDetail.getGross() - vatDetail.getVatAmount());
		}
		
		return vatDetails;
	}
	
	private VatDetail getVatDetailByVatRate(List<VatDetail> vatDetails, int vatRate) {
		for (VatDetail vatDetail : vatDetails) {
			if (vatDetail.getVatRate() == vatRate) {
				return vatDetail;
			}
		}
		
		vatDetails.add(new VatDetail(vatRate));
		
		return vatDetails.get(vatDetails.size() - 1);
	}
	
	protected abstract void generate(String jasper, String output, JRDataSource dataSource) throws JRException;
	
	private byte[] read(String fullPath) throws IOException {
		byte[] output = null;
		
		try (InputStream stream = new FileInputStream(fullPath)) {
			output = new byte[stream.available()];
			
			stream.read(output);
		}
		
		return output;
	}
	
	private String jasper;
	private Document generatedDocument;
	
	private static final Logger log = WebFrontLogger.getLogger(DocumentRenderer.class);
}
