package com.ncr.webfront.base.plugins.invoicecreditnote;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject;
import com.ncr.webfront.core.utils.commonobjects.ErrorObject.ErrorCode;
import com.ncr.webfront.base.invoicecreditnote.InvoiceCreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.base.invoicecreditnote.customer.CustomerType;
import com.ncr.webfront.base.invoicecreditnote.customer.Exemption;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType;
import com.ncr.webfront.base.invoicecreditnote.invoice.Receipt;
import com.ncr.webfront.base.invoicecreditnote.invoice.ReceiptSearchType;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.creditnote.CreditNoteServiceImplementation;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.invoice.InvoiceServiceImplementation;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.vat.VatServiceInterface;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;
import com.ncr.webfront.core.utils.validation.WebFrontValidationData;

public class InvoiceCreditNoteServiceImplementation implements InvoiceCreditNoteServiceInterface {
	@Override
	public ErrorObject checkService() {
		try {
			getFactory();

			return new ErrorObject(ErrorCode.OK_NO_ERROR);
		} catch (Exception e) {
			return new ErrorObject(ErrorCode.SETUP_ERROR, e.getLocalizedMessage());
		}
	}

	@Override
	public Customer createDefaultCustomer() {
		return new CustomerServiceImplementation(getFactory()).createDefaultCustomer();
	}

	@Override
	public int addCustomer(Customer customerToAdd) {
		return new CustomerServiceImplementation(getFactory()).addCustomer(customerToAdd);
	}

	@Override
	public List<Customer> getAllCustomers() {
		return new CustomerServiceImplementation(getFactory()).getAllCustomers();
	}

	@Override
	public List<Customer> getCustomersByName(String companyName) {
		return new CustomerServiceImplementation(getFactory()).getCustomersByName(companyName);
	}

	@Override
	public List<Customer> getCustomersByFiscalOrVatCode(String code) {
		return new CustomerServiceImplementation(getFactory()).getCustomersByFiscalOrVatCode(code);
	}

	@Override
	public Customer getCustomerByFidelityCode(String fidelityCode) {
		return new CustomerServiceImplementation(getFactory()).getCustomerByFidelityCode(fidelityCode);
	}

	@Override
	public void updateCustomer(Customer customerToUpdate) {
		new CustomerServiceImplementation(getFactory()).updateCustomer(customerToUpdate);
	}

	@Override
	public void deleteCustomer(Customer customerToDelete) {
		new CustomerServiceImplementation(getFactory()).deleteCustomer(customerToDelete);
	}

	@Override
	public List<CustomerType> getAllCustomerTypes() {
		return new CustomerServiceImplementation(getFactory()).getAllCustomerTypes();
	}

	@Override
	public List<Exemption> getAllExemptions() {
		return new CustomerServiceImplementation(getFactory()).getAllExemptions();
	}

	@Override
	public List<WebFrontValidationData> getCustomerValidationData() {
		return new CustomerServiceImplementation(getFactory()).getCustomerValidationData();
	}

	@Override
	public List<Receipt> searchReceipts(ReceiptSearchType receiptSearchType) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService)
				.searchReceipts(receiptSearchType);
	}

	@Override
	public Receipt fillReceipt(ReceiptSearchType receiptSearchType) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService)
				.fillReceipt(receiptSearchType);
	}

	@Override
	public Document createInvoice(Receipt receipt) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).createInvoice(receipt);
	}

	@Override
	public Document generateInvoice(Document invoice) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).generateInvoice(invoice);
	}

	@Override
	public byte[] renderHtmlInvoice(Document generatedInvoice) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService)
				.renderHtmlInvoice(generatedInvoice);
	}
	
	@Override
	public byte[] renderPdfInvoice(Document generatedInvoice) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService)
				.renderPdfInvoice(generatedInvoice);
	}

	@Override
	public byte[] saveInvoiceAndRenderPdf(Document generatedInvoice) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService)
				.saveInvoiceAndRenderPdf(generatedInvoice);
	}

	@Override
	public void cancelInvoice(Document invoice) {
		new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).cancelInvoice(invoice);
	}
	
	@Override
	public Document getInvoiceByNumber(int number, boolean mayHaveCreditNote) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).getInvoiceByNumber(number, mayHaveCreditNote);
	}
	
	@Override
	public List<Document> getInvoicesByFiscalCode(String fiscalCode, boolean mayHaveCreditNote) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).getInvoicesByFiscalCode(fiscalCode, mayHaveCreditNote);
	}
	
	@Override
	public List<Document> getInvoicesByVatCode(String vatCode, boolean mayHaveCreditNote) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).getInvoicesByVatCode(vatCode, mayHaveCreditNote);
	}
	
	@Override
	public List<Document> getLastPrintedInvoices(int number, boolean mayHaveCreditNote) {
		return new InvoiceServiceImplementation(getFactory(), articleService, departmentService, vatService).getLastPrintedInvoices(number, mayHaveCreditNote);
	}
	
	@Override
	public Document generateCreditNote(Document invoice) {
		return new CreditNoteServiceImplementation(getFactory()).generateCreditNote(invoice);
	}

	@Override
	public byte[] renderHtmlCreditNote(Document generatedCreditNote) {
		return new CreditNoteServiceImplementation(getFactory()).renderHtmlCreditNote(generatedCreditNote);
	}
	
	@Override
	public byte[] renderPdfCreditNote(Document generatedCreditNote) {
		return new CreditNoteServiceImplementation(getFactory()).renderPdfCreditNote(generatedCreditNote);
	}

	@Override
	public byte[] saveCreditNoteAndRenderPdf(Document generatedCreditNote) {
		return new CreditNoteServiceImplementation(getFactory()).saveCreditNoteAndRenderPdf(generatedCreditNote);
	}

	@Override
	public void cancelCreditNote(Document creditNote) {
		new CreditNoteServiceImplementation(getFactory()).cancelCreditNote(creditNote);
	}

	@Override
	public List<MutablePair<DocumentType, Integer>> peekSequences() {
		return new SequenceServiceImplementation(getFactory()).peekSequences();
	}

	@Override
	public void setSequence(String key, int value) {
		new SequenceServiceImplementation(getFactory()).setSequence(key, value);
	}
	
	@Override
	public Document getPrintedDocumentByNumber(final int number) {
		return HibernateHelper.transact(factory, new Visitor<Document, EntityManager>() {
			@Override
			public Document visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document where documentNumber = :documentNumber and documentStatus.status = :status", Document.class);
				
				q.setParameter("documentNumber", number);
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				
				return q.getSingleResult();
			}
		});
	}
	
	@Override
	public List<Document> getPrintedDocumentsByFiscalCode(final String fiscalCode) {
		return HibernateHelper.transactList(factory, new Visitor<List<Document>, EntityManager>() {
			@Override
			public List<Document> visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document where customer.fiscalCode = :fiscalCode and documentStatus.status = :status", Document.class);
				
				q.setParameter("fiscalCode", fiscalCode);
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				
				return q.getResultList();
			}
		});
	}
	
	@Override
	public List<Document> getPrintedDocumentsByVatCode(final String vatCode) {
		return HibernateHelper.transactList(factory, new Visitor<List<Document>, EntityManager>() {
			@Override
			public List<Document> visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document where customer.vatCode = :vatCode and documentStatus.status = :status", Document.class);
				
				q.setParameter("vatCode", vatCode);
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				
				return q.getResultList();
			}
		});
	}
	
	@Override
	public List<Document> getLastPrintedDocuments(final int number) {
		return HibernateHelper.transactList(factory, new Visitor<List<Document>, EntityManager>() {
			@Override
			public List<Document> visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery(
						"from Document where documentStatus.status = :status", Document.class);
				
				q.setParameter("status", PRINTED_DOCUMENT_STATUS);
				q.setMaxResults(number);
				
				return q.getResultList();
			}
		});
	}

	private synchronized EntityManagerFactory getFactory() {
		try {
			if (factory == null) {
				factory = Persistence.createEntityManagerFactory(WebFrontMappingProperties.webFrontPersistenceUnitName);
			}

			return factory;
		} catch (Exception e) {
			log.error("", e);

			throw e;
		}
	}

	public void setArticleService(ArticleServiceInterface articleService) {
		this.articleService = articleService;
	}
	
	public void setDepartmentService(PosDepartmentServiceInterface departmentService) {
		this.departmentService = departmentService;
	}

	public void setVatService(VatServiceInterface vatService) {
		this.vatService = vatService;
	}

	private EntityManagerFactory factory;
	private ArticleServiceInterface articleService;
	private PosDepartmentServiceInterface departmentService;
	private VatServiceInterface vatService;

	private static final Logger log = WebFrontLogger.getLogger(InvoiceCreditNoteServiceImplementation.class);
}
