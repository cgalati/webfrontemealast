package com.ncr.webfront.base.plugins.invoicecreditnote.internal.creditnote;

import javax.persistence.EntityManagerFactory;

import com.ncr.webfront.base.invoicecreditnote.creditnote.CreditNoteServiceInterface;
import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentCanceller;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentSaverAndPdfRenderer;

public class CreditNoteServiceImplementation implements CreditNoteServiceInterface {
	public CreditNoteServiceImplementation(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	@Override
	public Document generateCreditNote(Document invoice) {
		return new CreditNoteGenerator(factory).execute(invoice);
	}

	@Override
	public byte[] renderHtmlCreditNote(Document generatedCreditNote) {
		return new CreditNoteHtmlRenderer(generatedCreditNote).execute();
	}
	
	@Override
	public byte[] renderPdfCreditNote(Document generatedCreditNote) {
		return new CreditNotePdfRenderer(generatedCreditNote).execute();
	}

	@Override
	public byte[] saveCreditNoteAndRenderPdf(Document generatedCreditNote) {
		return new DocumentSaverAndPdfRenderer(factory).execute(generatedCreditNote, new CreditNotePdfRenderer(generatedCreditNote));
	}

	@Override
	public void cancelCreditNote(Document creditNote) {
		new DocumentCanceller(factory).execute(creditNote);
	}
	
	private EntityManagerFactory factory;
}
