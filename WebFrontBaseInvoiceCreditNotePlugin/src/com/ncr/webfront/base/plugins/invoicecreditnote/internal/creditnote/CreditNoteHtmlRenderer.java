package com.ncr.webfront.base.plugins.invoicecreditnote.internal.creditnote;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.plugins.invoicecreditnote.InvoiceCreditNoteMappingProperties;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentHtmlRenderer;

public class CreditNoteHtmlRenderer extends DocumentHtmlRenderer {
	protected CreditNoteHtmlRenderer(Document generatedDocument) {
		super(InvoiceCreditNoteMappingProperties.creditNoteJasper, generatedDocument);
	}
}
