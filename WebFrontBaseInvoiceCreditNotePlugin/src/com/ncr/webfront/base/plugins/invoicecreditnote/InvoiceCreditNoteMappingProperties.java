package com.ncr.webfront.base.plugins.invoicecreditnote;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import com.ncr.webfront.base.invoicecreditnote.customer.Customer;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.PropertiesLoader;

public class InvoiceCreditNoteMappingProperties {
	public static final String invoiceCreditNoteDir = "invoicecreditnote.dir";
	public static final String invoiceCreditNoteDirDefault = "conf/invoicecreditnote";
	
	public static final String jasperDir = "jasper.dir";
	public static final String jasperDirDefault = invoiceCreditNoteDirDefault + File.separator + "jasper";
	
	public static final String invoiceJasper = "invoice.jasper";
	public static final String invoiceJasperDefault = jasperDirDefault + File.separator + "invoicecreditnote.jasper";
	
	public static final String creditNoteJasper = "creditnote.jasper";
	public static final String creditNoteJasperDefault = jasperDirDefault + File.separator + "invoicecreditnote.jasper";
	
	public static final String invoiceCodeFormat = "invoice.code.format";
	public static final String invoiceCodeFormatDefault = "%s-%d";
	
	public static final String invoiceCodeDataFormat = "invoice.code.data.format";
	public static final String invoiceCodeDataFormatDefault = "yyyy";
	
	public static synchronized InvoiceCreditNoteMappingProperties getInstance() {
		if (instance == null) {
			instance = new InvoiceCreditNoteMappingProperties();
		}
		
		return instance;
	}
	
	private InvoiceCreditNoteMappingProperties() {
		invoiceCreditNote = PropertiesLoader.loadOrCreatePropertiesFile(invoiceCreditNotePluginPropertiesFileName, getPropertiesMap());
	}
	
	
	private static Map<String, String> getPropertiesMap() {
		Map<String, String> propertiesMap = new HashMap<>();
		
		propertiesMap.put(invoiceCreditNoteDir, invoiceCreditNoteDirDefault);
		propertiesMap.put(jasperDir, jasperDirDefault);
		propertiesMap.put(invoiceJasper + ".company", invoiceJasperDefault);
		propertiesMap.put(invoiceJasper + ".private", invoiceJasperDefault);
		propertiesMap.put(creditNoteJasper + ".company", creditNoteJasperDefault);
		propertiesMap.put(creditNoteJasper + ".private", creditNoteJasperDefault);
		propertiesMap.put(invoiceCodeFormat, invoiceCodeFormatDefault);
		propertiesMap.put(invoiceCodeDataFormat, invoiceCodeDataFormatDefault);
		
		return propertiesMap;
	}
	
	public String getProperty(String key) {
		return getProperty(key, false);
	}
	
	public String getProperty(String key, Customer customer, boolean prependWorkingDir) {
		return getProperty(key + "." + customer.getCustomerType().getType().toLowerCase(), prependWorkingDir);
	}
	
	public String getProperty(String key, boolean prependWorkingDir) {
		String value = invoiceCreditNote.getProperty(key);
		
		if (prependWorkingDir) {
			value = PathManager.getInstance().getWebFrontWorkingDirectory() + File.separator + value;
		}
		
		return value;
	}
	
	private final String invoiceCreditNotePluginPropertiesFileName = "invoicecreditnote.properties";
	private Properties invoiceCreditNote;
	
	private static InvoiceCreditNoteMappingProperties instance;
}
