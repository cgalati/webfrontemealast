package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;

public abstract class DocumentPdfRenderer extends DocumentRenderer {
	protected DocumentPdfRenderer(String jasperKey, Document generatedDocument) {
		super(jasperKey, generatedDocument);
	}

	public byte[] execute() {
		return super.execute("pdf");
	}
	
	@Override
	protected void generate(String jasper, String output, JRDataSource dataSource) throws JRException {
		JasperRunManager.runReportToPdfFile(jasper, output, null, dataSource);
	}
}
