package com.ncr.webfront.base.plugins.invoicecreditnote.internal.common;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import com.ncr.webfront.base.article.Article;
import com.ncr.webfront.base.article.ArticleServiceInterface;
import com.ncr.webfront.base.posdepartment.PosDepartment;
import com.ncr.webfront.base.posdepartment.PosDepartmentServiceInterface;
import com.ncr.webfront.base.vat.VatServiceInterface;

/**
 * A util class that takes into account the possibility that an item can be sold as a department ("vendita a reparto").
 * If and only if the item is backed by an article, the standard operations and behaviour takes place.
 */
public class SellAsDepartmentUtils {
	private SellAsDepartmentUtils() {
	}
	
	public static Article getArticle(ArticleServiceInterface articleService, String itemCode) {
		List<Article> articles = articleService.searchByArticleCode(itemCode);
		
		if (!CollectionUtils.isEmpty(articles)) {
			return articles.get(0);
		} else {
			return null;
		}
	}
	
	public static boolean getDiscountExempt(Article article) {
		if (article != null) {
			return (article.getAutomDiscountType() & 0x1) == 0;
		} else {
			return false;
		}
	}
	
	public static String getPackagingType(Article article) {
		return (article != null) ? article.getPackagingType() : "";
	}
	
	public static int getVatRate(VatServiceInterface vatService, Article article) {
		//TODO Verify - there was an exception using default VAT (705) from ars.properties - the properties was containing 705 but we expected 4 (i.e. 705-701)  
		int vatId = (article != null) ? article.getVatId() : vatService.getDefault().getVatId();
		
		return vatService.getVatById(vatId).getRate();
	}
	
	public static boolean isDecimalQty(Article article, String idcDetailData) {
		return (article != null) ? article.isDecimalQty() : idcDetailData.contains(".");
	}
	
	public static String getDepartmentDescription(PosDepartmentServiceInterface departmentService, Article article,
			int departmentId) {
		if (article != null) {
			return article.getDescription();
		} else {
			PosDepartment department = departmentService.getByCode(Integer.toString(departmentId));
			
			return (department != null) ? department.getDescription() : "Reparto " + departmentId;
		}
	}
}
