package com.ncr.webfront.base.plugins.invoicecreditnote.internal.creditnote;

import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentType.*;
import static com.ncr.webfront.base.invoicecreditnote.invoice.DocumentStatus.*;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.ncr.webfront.base.invoicecreditnote.invoice.Document;
import com.ncr.webfront.base.invoicecreditnote.invoice.DocumentDetail;
import com.ncr.webfront.base.plugins.invoicecreditnote.internal.common.DocumentHelper;
import com.ncr.webfront.core.utils.Visitor;
import com.ncr.webfront.core.utils.db.HibernateHelper;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;

public class CreditNoteGenerator extends DocumentHelper {
	public CreditNoteGenerator(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	public Document execute(final Document invoice) {
		final Document creditNote = generate(invoice);
		
		HibernateHelper.transact(factory, new Visitor<Void, EntityManager>() {
			@Override
			public Void visit(EntityManager manager) {
				TypedQuery<Document> q = manager.createQuery("from Document where id = :id", Document.class);
				
				q.setParameter("id", invoice.getId());
				creditNote.setReferenced(q.getSingleResult());
				creditNote.setCustomer(creditNote.getReferenced().getCustomer());
				
				manager.persist(creditNote);
				for (DocumentDetail detail : creditNote.getDocumentDetails()) {
					detail.setDocumentId(creditNote.getId());
					manager.persist(detail);
				}
				
				return null;
			}
		});
		
		return creditNote;
	}
	
	private Document generate(Document invoice) {
		Document creditNote = new Document();
		Date now = new Date();
		int rate = invoice.getCustomer().getExemption().getRate();
		boolean facilitated = invoice.getCustomer().getExemption().isFacilitated();
		
		try {
			PropertyUtils.copyProperties(creditNote, invoice);
		} catch (Exception e) {
			log.error("", e);
			
			// Unreachable.
		}
		creditNote.setId(0);
		
		log.debug(String.format("creditNote <- invoice [%s]", creditNote));
		
		creditNote.setDocumentDetails(new ArrayList<DocumentDetail>());
		for (DocumentDetail invoiceDetail : invoice.getDocumentDetails()) {
			DocumentDetail creditNoteDetail = new DocumentDetail();
			
			try {
				PropertyUtils.copyProperties(creditNoteDetail, invoiceDetail);
			} catch (Exception e) {
				log.error("", e);
				
				// Unreachable.
			}
			creditNoteDetail.setId(0);
			
			creditNote.getDocumentDetails().add(creditNoteDetail);
		}
		
		log.debug("creditNote.documentDetails <- invoice.documentDetails");
		
		creditNote.setDocumentType(getDocumentTypeByType(CREDITNOTE_DOCUMENT_TYPE));
		creditNote.setDocumentStatus(getDocumentStatusByStatus(CREATED_DOCUMENT_STATUS));
		creditNote.setCreationDate(now);
		creditNote.setLastUpdate(now);
		
		log.debug("updating amount and number...");
		
		updateAmount(creditNote, rate, facilitated);
		updateTentativeNumber(creditNote);
		
		log.debug("updated");
		
		return creditNote;
	}
	
	private static final Logger log = WebFrontLogger.getLogger(CreditNoteGenerator.class);
}
