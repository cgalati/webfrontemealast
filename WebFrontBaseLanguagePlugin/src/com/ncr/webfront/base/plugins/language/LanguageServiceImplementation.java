package com.ncr.webfront.base.plugins.language;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.ncr.webfront.base.language.LabelsMap;
import com.ncr.webfront.base.language.LanguageServiceInterface;
import com.ncr.webfront.core.utils.logging.WebFrontLogger;
import com.ncr.webfront.core.utils.paths.PathManager;
import com.ncr.webfront.core.utils.propertiesmapping.WebFrontMappingProperties;

public class LanguageServiceImplementation implements LanguageServiceInterface {

	public LanguageServiceImplementation (){
		logger.info("BEGIN");
		logger.info("END");
	}
	
	@Override
	public LabelsMap load(Locale locale, String serviceName) {
		logger.info("BEGIN");
		logger.info("locale=" + locale);
		logger.info("serviceName="+serviceName);

		LabelsMap labelsMap = null;

		if (locale != null) {
			labelsMap = load(serviceName + "_" + locale);
		}
		if (labelsMap == null || labelsMap.isEmpty()) {
			labelsMap = load(serviceName);
		}
		logger.info("END");
		return labelsMap;

	}
	@Override
	public LabelsMap load(String serviceName) {
		logger.info("BEGIN");
		logger.info("serviceName="+serviceName);

		LabelsMap labelsMap = new LabelsMap();

		String labelsFilePath = PathManager.getInstance().getWebFrontLabelsDirectory(WebFrontMappingProperties.getInstance().getForcedTextsLocale());
		String labelsFileFullNameAndPath = labelsFilePath + File.separator + serviceName + ".properties";

		logger.info("file: " + labelsFileFullNameAndPath);

		File file = new File(labelsFileFullNameAndPath);
		if (!file.exists()) {
			file = null;
			logger.info("END (null)");
			return null;
		}
		file = null;

		Properties prop = new Properties();
		try {
			FileInputStream in = new FileInputStream(labelsFileFullNameAndPath);
			prop.load(in);
			in.close();
		} catch (IOException e) {
			logger.error("Exception!", e);
			e.printStackTrace();
		}
		logger.info("prop.keySet().size() = " + prop.keySet().size() + "");

		if (prop != null) {
			for (final String name : prop.stringPropertyNames())
				labelsMap.put(name, prop.getProperty(name));
		}

		logger.info("END (map)");
		return labelsMap;
	}

	private static final Logger logger = WebFrontLogger.getLogger(LanguageServiceImplementation.class);
}
